#!/usr/bin/env bash

function run() {
    (cd src && php bin/console $@)
}

function previewDiff() {
    run doctrine:cache:clear-metadata
    echo 'Diff preview:'
    run doctrine:schema:update --dump-sql
    read -p 'Generate migration (y/n)?: ' isApproved
    if [ ! $isApproved == 'y' ]; then
        echo 'Bye'
        exit 0
    fi

    run doctrine:migrations:diff -n

    read -p 'Apply it migration (y/n)?: ' isApproved
    if [ ! $isApproved == 'y' ]; then
        echo 'Bye'
        exit 0
    fi

    run doctrine:migrations:migrate -n
}

function deploy() {
    #runs symfony commands
    #clear caches
    run cache:clear --env=prod --no-debug
    run doctrine:migrations:migrate -n
    run background:supervisor:build
    run background:cron:build
    (cd src/frontend && npm i)
    (cd src/frontend && npm run build)
    (cd src/websocket && npm i)
    run payment:rate:update
    run doctrine:cache:clear-result
    run cache:warmup --env=prod --no-debug
    run shop:cache:warm
}

args="${@:2}"

case $1 in
    diff)
        previewDiff
        ;;
    entity)
        run doctrine:generate:entities $args
        ;;
    clear-metadata)
        run doctrine:cache:clear-metadata
        ;;
    migrate)
        run doctrine:migrations:migrate $args
        ;;
    deploy)
        deploy
        ;;
    *)
        run $@
        ;;
esac