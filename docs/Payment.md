Links
-----
[Референс по адресу](https://github.com/bitcoin/bips/blob/master/bip-0021.mediawiki#Transfer_amountsize)  
[Доки к апи blockchain.info](https://blockchain.info/ru/api)

Twig
----

Переменные и функции в twig

```
{{ currency }} - объект текущей валюты в системе, выведет код
{{ currency.name }} - название
{{ currency.icon }} - иконка валюты
```
Конвертация
```
{{ '245'|convertTo(fromCurrency=USD, toCurrency=BTC) }}
```
*fromCurrency* - необязательное значение, по умолчанию USD, из какой валюты конвертировать  
*toCurrency* - необязательное значение, в какую валюту конвертировать, если не указано, то используется текущая выбранная в системе  
в качестве валют можно использовать как объект Payment\Component\Currency так и код валюты