События
=======
Кастомные события в системе
|Name|EventClass|Description|
|----|----|----|
|payment.success|\Payment\Component\PaymentEvent|Платеж успешно прошел верификацию|
|order.status.change|\Order\Component\OrderStatusEvent|Заказ изменил статус в системе|
|import.product.update_price|\Catalog\Import\Component\ProductUpdateEvent|Обновление цены|
|import.product.update_available|\Catalog\Import\Component\ProductUpdateEvent|Обновление видимости|
|withdrawal.change_status|\Referral\Event\WithdrawalEvent|Списание бонусов сменило статус|