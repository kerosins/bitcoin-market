Frontend
========

Twig Widgets
============
Configurable block of products
------------------------------
Использование
```twig
{{ itemBlock('configurableProducts', {
    title: 'More products from this seller',
    supplier: product.supplier,
    category: category.id,
    price: product.price.value
}) }}
```

Параметры:
- **title** - заголовок блока, если false или null, то заголовок не отображается
- **supplier** - id продавца, если используется, то выборка будет отфильтрована по этому продавцу
- **category** - id категории, если используется, то выборка будет ограничена этой категорией
- **priceDiapason** - диапазон цены в процентах
- **price** - цена, от этого значения считается диапазон цены, если не установлена, диапазон в фильтрации не используется

Note: 
Выборка ограничена 20-ю продуктами


Tips and Tricks
===============

Модальные окна
--------------
data-toggle="ajax-modal
когда происходит клик на элемент вызывается модальное окно 
с контентом запрашиваемым по ajax по url из __*data-url*__ параметра

##### Пример:
```html
<button type="button" class="btn btn-default" data-toggle="ajax-modal" data-url="/some-ajax-url">
    Open modal
</button>
```

Алерт
-----
Для того что бы вызвать стилизованный алерт, можно изпользовать следующий код:
```javascript
$(document).trigger('alertModal.btshop', {
    message: 'Some alert message' //Текст алерта
});
```

Модалка подтверждения
---------------------
```html
.js-link-require-confirmation
```