Catalog\Import
======
Run Catalog\Import EPN's Price List
---------------------------
**Временное решенеие**
Загрузить прайс с epn, распаковать и положить в любую директорию
http://files.epn.bz/?offer=aliexpress&size=l&r=t1
Например:
```bash
curl -O -J -L 'http://files.epn.bz/?offer=aliexpress&size=l&r=t1'
```
```bash
#remove illegal chars
sed 's/[[:cntrl:]]//g' alidump.yml > alidump.yml.fixed
#import 
./btshop.sh import:xml -t epn -p {path_to_yml_dir}/alidump.yml.fixed
#load description for new goods
./btshop.sh import:load:description
#rearrange suppliers
./btshop.sh import:rearrange_suppliers
#populate index
./btshop.sh fos:elastica:populate --no-debug --no-reset --no-delete
``` 