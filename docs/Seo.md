Заметки по алиасам 
==================
Пока наброски больше для себя  
для генерации ссылки, использовать:
- Для относительных ссылок:
```twig
{{ relativePageAlias('product', 1) }}
```
- Для абсолютных:
```twig
{{ pageAlias('product', 1) }}
```
Для разобраться, смотреть:
 - \Seo\Service\AliasRouteManager - сохранение алиасов, их поиск
 - \Seo\Routing\AliasListener - Резолв роутов и редирект на главный алиасов по необходимости
 - \Seo\Component\PageAliasContractInterface - Интерфейс для сущностей, к которым нужно привязывать алиас
 
Todo: нужно погонять под нагрузкой, профилировать, найти и устранить узкие места