Background Bundle
===================
Выполнение задач в фоновом режиме

Websockets
----------
Todo: описать потом принцип и примеры отправки сообщений  

Supervisor
----------
### Настройка
Создать конфиг супервизора:
```bash
php bin/console background:supervisor:build
```
Скопировать конфигурацию в supervisor:
```bash
yes | cp -rf supervisor.conf /etc/supervisor/conf.d/zenith.conf
```
Перезапусить supervisor
```bash
service supervisor restart
```

Background Task Workers
-----------------------
### Workflow
Для того что бы запустить сервер воркеров нужно выполнить в консоли
```bash
php bin/console background:worker:server
```
Для создания воркера необходимо:
 - реализовать интерфейс \Background\Worker\TaskWorkerInterface
 - зарегистрировать сервис в worker.yml с тегом background.task_worker и указать id, который 
 будет использоваться при создании задачи 
 ```{ name: 'background.task_worker', id: 'cmd_wrap_worker' }```
  
В коде задачу можно создать следующим образом:
```php
$mng = $this->getContainer()->get(\Background\Worker\WorkerServer::class);

$promise = $mng->addTask('example_worker', [
    'some param name' => 'some param value'
], true); //if need wait for task set true, otherwise false

//if flag for wait set true, then
$promise->then(function ($output) {
    //string output from worker
});
```
### How it works
TODO описать, что к чему
