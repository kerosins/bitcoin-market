Important
=========

Использовать **!** вместо **?** при jsonb запросах

Доступы
=======
[Доступы](docs/LogPass.md)

!IMPORTANT
==========
При изменении типа поля на кастомный тип Doctrine не добавляет комментарий в БД.  
После чего при каждом diff схемы будет пытаться это поле обновить.  
В этом случае необходимо в миграцию руками добавить следующий код:  
```php
//method up
$this->addSql('COMMENT ON COLUMN <Имя таблицы>.<Имя поля> IS \'(DC2Type:<Тип в Doctrine>)\'');
//method down
$this->addSql('COMMENT ON COLUMN <Имя таблицы>.<Имя поля> IS NULL');
```

Структура
=======
**/docker** - конфиги образов докера  
**/src** - код приложения  
**/src/app** - конфиги приложения  
**/src/app/Migrations** - Миграции  
**/src/src** - бандлы

Бандлы
======
**Shop** каталог, заказы, публичная часть  
**Cp** панель управления, админка  
**Kerosin** общие классы, обработчики событий, новые базовые типы форм  
**Payment** криптовалюты, оплата и все что с этим связано  
**ProductImport** загрузка продуктов из вне
**Background** Обработка задач в фоне. [Докуменация](docs/Background.md)

Статусы
----
СHECKOUT (10) - Заказ на стадии заполнения информации о доставке, проверке  
PAYMENT_PROCESS (20) - Ожидание оплаты  
PROCESS (30) - Сборка заказа, проще говоря, выкуп у китайцев и оформление отправки заказчику  
SHIPPING (40) - Заказ идет покупателю  
DONE (50) - Заказ завершен  
CANCEL (60) - Заказ отменен, истекло время оплаты

Frontend
========
[Docs](docs/Frontend.md)

### Js Хелперы

jQuery
------
Надо описать, памятка
```javascript
(function ($) {
    
    $(function () {
        //DOM is ready here
    });
    
} (window.internalJQuery));
```

Console
=======

в файл *~/.bashrc* добавить следующее:
```bash
if [ -f ~/.symfony_helpers ]; then
    . ~/.symfony_helpers
fi
```
в ~/.symfony_helpers:
```bash
btshop_path=/mnt/hgfs/work/btshop/bitcoin-market/src #путь до проекта
bin_program='php bin/console'

function smf {
    (cd $btshop_path && $bin_program $@)
}

function btshop {
args="${@:2}"

    case $1 in
        diff)
            smf doctrine:migrations:diff $args
            ;;
        migrate)
            smf doctrine:migrations:migrate $args
            ;;
        entity)
            smf doctrine:generate:entities $args
            ;;
        *)
            smf $@
            ;;
    esac
}
```
Cron jobs
---------
```bash
*/2 * * * * (cd src && php bin/console payment:expire) # проверка платежей, Not used
* */12 * * * (cd src && php bin/console order:check_end_date) #отправка сообщений о скором окончании времени обработки заказа
* */12 * * * (cd src && php bin/console order:accept_process) #подтверждение заказов в статусе shipping по прошествии 60 дней
```