<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180304223739 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE order_delivery_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE order_delivery_info_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE order_delivery_info (id INT NOT NULL, order_id INT DEFAULT NULL, country_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, state JSON NOT NULL, city JSON NOT NULL, zip VARCHAR(255) NOT NULL, address TEXT NOT NULL, apartment VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B776B11F8D9F6D38 ON order_delivery_info (order_id)');
        $this->addSql('CREATE INDEX IDX_B776B11FF92F3E70 ON order_delivery_info (country_id)');
        $this->addSql('COMMENT ON COLUMN order_delivery_info.state IS \'(DC2Type:geo_object)\'');
        $this->addSql('COMMENT ON COLUMN order_delivery_info.city IS \'(DC2Type:geo_object)\'');
        $this->addSql('ALTER TABLE order_delivery_info ADD CONSTRAINT FK_B776B11F8D9F6D38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_delivery_info ADD CONSTRAINT FK_B776B11FF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE order_delivery');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE order_delivery_info_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE order_delivery_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE order_delivery (id INT NOT NULL, state_id INT DEFAULT NULL, county_id INT DEFAULT NULL, city_id INT DEFAULT NULL, order_id INT DEFAULT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, address TEXT NOT NULL, apartment VARCHAR(255) DEFAULT NULL, zip INT NOT NULL, email TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_d6790ea18d9f6d38 ON order_delivery (order_id)');
        $this->addSql('CREATE INDEX idx_d6790ea15d83cc1 ON order_delivery (state_id)');
        $this->addSql('CREATE INDEX idx_d6790ea18bac62af ON order_delivery (city_id)');
        $this->addSql('CREATE INDEX idx_d6790ea185e73f45 ON order_delivery (county_id)');
        $this->addSql('ALTER TABLE order_delivery ADD CONSTRAINT fk_d6790ea15d83cc1 FOREIGN KEY (state_id) REFERENCES address_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_delivery ADD CONSTRAINT fk_d6790ea185e73f45 FOREIGN KEY (county_id) REFERENCES address_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_delivery ADD CONSTRAINT fk_d6790ea18bac62af FOREIGN KEY (city_id) REFERENCES address_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_delivery ADD CONSTRAINT fk_d6790ea18d9f6d38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE order_delivery_info');
    }
}
