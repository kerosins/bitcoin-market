<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180305003616 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('TRUNCATE address_object RESTART IDENTITY CASCADE');

        $this->addSql('CREATE SEQUENCE zip_code_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('DROP INDEX uniq_a1ace158421d9546');
        $this->addSql('ALTER TABLE zip_code ADD code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE zip_code RENAME COLUMN zip TO id');

        $this->addSql('ALTER TABLE zip_code DROP CONSTRAINT zip_code_pkey');
        $this->addSql('ALTER TABLE zip_code ADD PRIMARY KEY (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
    }
}
