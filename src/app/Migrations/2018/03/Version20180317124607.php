<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180317124607 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE address_object DROP CONSTRAINT FK_93F42E53F92F3E70');
        $this->addSql('ALTER TABLE address_object ADD CONSTRAINT FK_93F42E53F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE currency DROP CONSTRAINT fk_6956883f68afd1a0');
        $this->addSql('DROP INDEX idx_6956883f68afd1a0');
        $this->addSql('ALTER TABLE currency ADD exchange VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE currency DROP exchange_id');
        $this->addSql('ALTER TABLE payment DROP CONSTRAINT fk_6d28840d68afd1a0');
        $this->addSql('DROP INDEX idx_6d28840d68afd1a0');
        $this->addSql('ALTER TABLE payment ADD exchange VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE payment ADD rate NUMERIC(10, 5) DEFAULT NULL');
        $this->addSql('ALTER TABLE payment ADD email TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment ADD status INT NOT NULL');
        $this->addSql('ALTER TABLE payment DROP exchange_id');
        $this->addSql('ALTER TABLE payment DROP is_done');
        $this->addSql('ALTER TABLE payment DROP is_success');
        $this->addSql('ALTER TABLE payment ALTER external_transaction_id DROP NOT NULL');
        $this->addSql('ALTER TABLE payment ALTER err_message DROP NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE currency ADD exchange_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE currency DROP exchange');
        $this->addSql('ALTER TABLE currency ADD CONSTRAINT fk_6956883f68afd1a0 FOREIGN KEY (exchange_id) REFERENCES exchange (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_6956883f68afd1a0 ON currency (exchange_id)');
        $this->addSql('ALTER TABLE payment ADD exchange_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment ADD is_done BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE payment ADD is_success BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE payment DROP exchange');
        $this->addSql('ALTER TABLE payment DROP rate');
        $this->addSql('ALTER TABLE payment DROP email');
        $this->addSql('ALTER TABLE payment DROP status');
        $this->addSql('ALTER TABLE payment ALTER external_transaction_id SET NOT NULL');
        $this->addSql('ALTER TABLE payment ALTER err_message SET NOT NULL');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT fk_6d28840d68afd1a0 FOREIGN KEY (exchange_id) REFERENCES exchange (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_6d28840d68afd1a0 ON payment (exchange_id)');
        $this->addSql('ALTER TABLE address_object DROP CONSTRAINT fk_93f42e53f92f3e70');
        $this->addSql('ALTER TABLE address_object ADD CONSTRAINT fk_93f42e53f92f3e70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
