<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181204201144 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE referral_purchase ADD referral_session_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE referral_purchase ADD CONSTRAINT FK_B343749A709FE444 FOREIGN KEY (referral_session_id) REFERENCES referral_session (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_B343749A709FE444 ON referral_purchase (referral_session_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE referral_purchase DROP CONSTRAINT FK_B343749A709FE444');
        $this->addSql('DROP INDEX IDX_B343749A709FE444');
        $this->addSql('ALTER TABLE referral_purchase DROP referral_session_id');
    }
}
