<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181221183917 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
    }

    public function postUp(Schema $schema)
    {
        $dataDir = $this->container->getParameter('data_dir');
        $usersFile = new \SplFileObject($dataDir . '/users.ldj');
        $usersFile->setFlags(\SplFileObject::SKIP_EMPTY);

        while (!$usersFile->eof()) {
            $row = $usersFile->current();
            if (!$row) {
                continue;
            }
            $row = json_decode($row, true);

            $id = $this->connection->executeQuery("SELECT nextval('fos_user_id_seq')")->fetch(FetchMode::NUMERIC);
            $row['data']['id'] = $id[0];

            $this->connection->insert('fos_user', $row['data'], $row['types']);
            $usersFile->next();
        }
    }
}
