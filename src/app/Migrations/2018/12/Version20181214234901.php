<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181214234901 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE dispute DROP CONSTRAINT FK_3C925007C63651C0');
        $this->addSql('ALTER TABLE dispute ADD CONSTRAINT FK_3C925007C63651C0 FOREIGN KEY (order_position_id) REFERENCES order_position (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_message DROP CONSTRAINT FK_40C799342130303A');
        $this->addSql('ALTER TABLE order_message DROP CONSTRAINT FK_40C7993429F6EE60');
        $this->addSql('ALTER TABLE order_message ADD CONSTRAINT FK_40C799342130303A FOREIGN KEY (from_user_id) REFERENCES fos_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_message ADD CONSTRAINT FK_40C7993429F6EE60 FOREIGN KEY (to_user_id) REFERENCES fos_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_tbl DROP CONSTRAINT FK_FC2D72A8A76ED395');
        $this->addSql('ALTER TABLE order_tbl ADD CONSTRAINT FK_FC2D72A8A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_position DROP CONSTRAINT FK_A7D406448D9F6D38');
        $this->addSql('ALTER TABLE order_position ADD CONSTRAINT FK_A7D406448D9F6D38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_status DROP CONSTRAINT FK_B88F75C98D9F6D38');
        $this->addSql('ALTER TABLE order_status ADD CONSTRAINT FK_B88F75C98D9F6D38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX status_type_idx ON order_status (type)');
        $this->addSql('ALTER TABLE user_delivery_info DROP CONSTRAINT FK_F49B549DA76ED395');
        $this->addSql('ALTER TABLE user_delivery_info ADD CONSTRAINT FK_F49B549DA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE dispute DROP CONSTRAINT fk_3c925007c63651c0');
        $this->addSql('ALTER TABLE dispute ADD CONSTRAINT fk_3c925007c63651c0 FOREIGN KEY (order_position_id) REFERENCES order_position (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_message DROP CONSTRAINT fk_40c799342130303a');
        $this->addSql('ALTER TABLE order_message DROP CONSTRAINT fk_40c7993429f6ee60');
        $this->addSql('ALTER TABLE order_message ADD CONSTRAINT fk_40c799342130303a FOREIGN KEY (from_user_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_message ADD CONSTRAINT fk_40c7993429f6ee60 FOREIGN KEY (to_user_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_position DROP CONSTRAINT fk_a7d406448d9f6d38');
        $this->addSql('ALTER TABLE order_position ADD CONSTRAINT fk_a7d406448d9f6d38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_status DROP CONSTRAINT fk_b88f75c98d9f6d38');
        $this->addSql('DROP INDEX status_type_idx');
        $this->addSql('ALTER TABLE order_status ADD CONSTRAINT fk_b88f75c98d9f6d38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_delivery_info DROP CONSTRAINT fk_f49b549da76ed395');
        $this->addSql('ALTER TABLE user_delivery_info ADD CONSTRAINT fk_f49b549da76ed395 FOREIGN KEY (user_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_tbl DROP CONSTRAINT fk_fc2d72a8a76ed395');
        $this->addSql('ALTER TABLE order_tbl ADD CONSTRAINT fk_fc2d72a8a76ed395 FOREIGN KEY (user_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
