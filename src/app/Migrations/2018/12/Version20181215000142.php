<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181215000142 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE fos_user DROP CONSTRAINT FK_957A6479F92F3E70');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A6479F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE referral_program DROP CONSTRAINT FK_CBCD2BA2A76ED395');
        $this->addSql('ALTER TABLE referral_program ADD CONSTRAINT FK_CBCD2BA2A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE referral_purchase DROP CONSTRAINT FK_B343749A3EB8070A');
        $this->addSql('ALTER TABLE referral_purchase DROP CONSTRAINT FK_B343749A8D9F6D38');
        $this->addSql('ALTER TABLE referral_purchase ADD CONSTRAINT FK_B343749A3EB8070A FOREIGN KEY (program_id) REFERENCES referral_program (code) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE referral_purchase ADD CONSTRAINT FK_B343749A8D9F6D38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE referral_program DROP CONSTRAINT fk_cbcd2ba2a76ed395');
        $this->addSql('ALTER TABLE referral_program ADD CONSTRAINT fk_cbcd2ba2a76ed395 FOREIGN KEY (user_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE referral_purchase DROP CONSTRAINT fk_b343749a3eb8070a');
        $this->addSql('ALTER TABLE referral_purchase DROP CONSTRAINT fk_b343749a8d9f6d38');
        $this->addSql('ALTER TABLE referral_purchase ADD CONSTRAINT fk_b343749a3eb8070a FOREIGN KEY (program_id) REFERENCES referral_program (code) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE referral_purchase ADD CONSTRAINT fk_b343749a8d9f6d38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE fos_user DROP CONSTRAINT fk_957a6479f92f3e70');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT fk_957a6479f92f3e70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
