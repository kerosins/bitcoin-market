<?php declare(strict_types=1);

namespace Migrations;

use Content\ItemBlock\BlockType\ManualProducts\ManualProductsBlock;
use Content\ORM\Entity\ItemBlock;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181217192304 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
    }

    public function postUp(Schema $schema)
    {
        $itemBlocks = $this->connection->createQueryBuilder()
            ->select('ib.*')
            ->from('item_block', 'ib')
            ->andWhere('ib.type = :type')
            ->setParameter('type', ManualProductsBlock::NAME)
            ->execute()
            ->fetchAll();

        foreach ($itemBlocks as $block) {
            $params = json_decode($block['params'], true);
            $counter = 0;
            $params['products'] = array_map(function ($id) use (&$counter) {
                return [
                    'id' => $id,
                    'position' => $counter++
                ];
            }, $params['products']);

            $this->connection->update(
                'item_block',
                ['params' => json_encode($params)],
                ['id' => $block['id']]
            );
        }
    }
}
