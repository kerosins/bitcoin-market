<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180214201218 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE country ADD code VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE country ADD allow_autocomplete BOOLEAN NOT NULL DEFAULT \'f\'');
        $this->addSql('ALTER TABLE user_delivery_info ADD country_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_delivery_info ADD CONSTRAINT FK_F49B549DF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F49B549DF92F3E70 ON user_delivery_info (country_id)');
        $this->addSql('COMMENT ON COLUMN country.flag IS \'(DC2Type:image)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE country DROP code');
        $this->addSql('ALTER TABLE country DROP allow_autocomplete');
        $this->addSql('ALTER TABLE user_delivery_info DROP CONSTRAINT FK_F49B549DF92F3E70');
        $this->addSql('DROP INDEX IDX_F49B549DF92F3E70');
        $this->addSql('ALTER TABLE user_delivery_info DROP country_id');
        $this->addSql('COMMENT ON COLUMN country.flag IS NULL');
    }

    public function postUp(Schema $schema)
    {
        $countries = [
            ['title' => 'United States', 'code' => 'USA'],
            ['title' => 'Åland Islands', 'code' => ''],
            ['title' => 'Albania', 'code' => 'ALB'],
            ['title' => 'Andorra', 'code' => 'AND'],
            ['title' => 'Austria', 'code' => 'AUT'],
            ['title' => 'Belgium', 'code' => 'BEL'],
            ['title' => 'Bulgaria', 'code' => 'BUL'],
            ['title' => 'Bosnia and Herzegovina', 'code' => 'BIH'],
            ['title' => 'Belarus', 'code' => 'BLR'],
            ['title' => 'Switzerland', 'code' => 'SUI'],
            ['title' => 'Cyprus', 'code' => 'CYP'],
            ['title' => 'Czechia', 'code' => 'CZE'],
            ['title' => 'Germany', 'code' => 'GER'],
            ['title' => 'Denmark', 'code' => 'DEN'],
            ['title' => 'Spain', 'code' => 'ESP'],
            ['title' => 'Estonia', 'code' => 'EST'],
            ['title' => 'Finland', 'code' => 'FIN'],
            ['title' => 'France', 'code' => 'FRA'],
            ['title' => 'Faroe Islands', 'code' => ''],
            ['title' => 'United Kingdom', 'code' => 'GBR'],
            ['title' => 'Guernsey', 'code' => ''],
            ['title' => 'Gibraltar', 'code' => ''],
            ['title' => 'Greece', 'code' => 'GRE'],
            ['title' => 'Croatia', 'code' => 'CRO'],
            ['title' => 'Hungary', 'code' => 'HUN'],
            ['title' => 'Isle of Man', 'code' => ''],
            ['title' => 'Ireland', 'code' => 'IRL'],
            ['title' => 'Iceland', 'code' => 'ISL'],
            ['title' => 'Italy', 'code' => 'ITA'],
            ['title' => 'Jersey', 'code' => ''],
            ['title' => 'Kosovo', 'code' => 'KOS'],
            ['title' => 'Liechtenstein', 'code' => 'LIE'],
            ['title' => 'Lithuania', 'code' => 'LTU'],
            ['title' => 'Luxembourg', 'code' => 'LUX'],
            ['title' => 'Latvia', 'code' => 'LAT'],
            ['title' => 'Monaco', 'code' => 'MON'],
            ['title' => 'Moldova', 'code' => 'MDA'],
            ['title' => 'Macedonia', 'code' => 'MKD'],
            ['title' => 'Malta', 'code' => 'MLT'],
            ['title' => 'Montenegro', 'code' => 'MNE'],
            ['title' => 'Netherlands', 'code' => 'NED'],
            ['title' => 'Norway', 'code' => 'NOR'],
            ['title' => 'Poland', 'code' => 'POL'],
            ['title' => 'Portugal', 'code' => 'POR'],
            ['title' => 'Romania', 'code' => 'ROU'],
            ['title' => 'Russia', 'code' => 'RUS'],
            ['title' => 'Svalbard and Jan Mayen', 'code' => ''],
            ['title' => 'San Marino', 'code' => 'SMR'],
            ['title' => 'Serbia', 'code' => 'SRB'],
            ['title' => 'Slovakia', 'code' => 'SVK'],
            ['title' => 'Slovenia', 'code' => 'SLO'],
            ['title' => 'Sweden', 'code' => 'SWE'],
            ['title' => 'Ukraine', 'code' => 'UKR'],
            ['title' => 'Vatican City', 'code' => '']
        ];

        foreach ($countries as $country) {
            $id = $this->connection->executeQuery("SELECT nextval('country_id_seq')")->fetch(\PDO::FETCH_NUM);
            $country['id'] = $id[0];
            $country['created_at'] = $country['updated_at'] = date('Y-m-d H:i:s');
            $country['allow_autocomplete'] = 'f';
            $this->connection->insert('country', $country);
        }
    }
}
