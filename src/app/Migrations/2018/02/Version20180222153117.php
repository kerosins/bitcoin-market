<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180222153117 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE mapping_category_alias_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE external_categories_map_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE external_products_map_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE mapping_category_alias (id INT NOT NULL, category_id INT DEFAULT NULL, aliases TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B889B0FF12469DE2 ON mapping_category_alias (category_id)');
        $this->addSql('CREATE TABLE external_categories_map (id INT NOT NULL, category_id INT DEFAULT NULL, external_id INT NOT NULL, source INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BCB7EEF512469DE2 ON external_categories_map (category_id)');
        $this->addSql('CREATE TABLE external_products_map (id INT NOT NULL, product_id INT DEFAULT NULL, external_id INT NOT NULL, source VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_991670374584665A ON external_products_map (product_id)');
        $this->addSql('CREATE INDEX search_idx ON external_products_map (external_id, source)');
        $this->addSql('ALTER TABLE mapping_category_alias ADD CONSTRAINT FK_B889B0FF12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE external_categories_map ADD CONSTRAINT FK_BCB7EEF512469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE external_products_map ADD CONSTRAINT FK_991670374584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE country ALTER allow_autocomplete DROP DEFAULT');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE mapping_category_alias_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE external_categories_map_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE external_products_map_id_seq CASCADE');
        $this->addSql('DROP TABLE mapping_category_alias');
        $this->addSql('DROP TABLE external_categories_map');
        $this->addSql('DROP TABLE external_products_map');
        $this->addSql('ALTER TABLE country ALTER allow_autocomplete SET DEFAULT \'false\'');
    }
}
