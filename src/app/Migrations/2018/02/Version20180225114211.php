<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use User\Entity\User;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180225114211 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {   // this up() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema)
    {
        $id = $this->connection->executeQuery("SELECT nextval('fos_user_id_seq')")->fetch(Query::HYDRATE_SINGLE_SCALAR);

        $this->connection->insert(
            'fos_user',
            [
                'id' => $id['nextval'],
                'username' => 'system',
                'username_canonical' => 'system',
                'email' => 'test@test.com2',
                'email_canonical' => 'test@test.com',
                'enabled' => true,
                'password' => '$13$ZlQtJOY9S0zcXaq0XEwk6.Sf6cg9.XjySgejZBPDRzkvHVc6I.cIS',
                'roles' => 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}',
                'country_id' => 6
            ]
        );
    }

    public function postDown(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->remove(
            $em->getRepository('UserBundle:User')->findOneBy(['username' => User::SYSTEM_USERNAME])
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
    }
}
