<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180224232317 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE external_categories_map ALTER external_id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE external_categories_map ALTER external_id DROP DEFAULT');
        $this->addSql('ALTER TABLE external_categories_map ALTER source TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE external_categories_map ALTER source DROP DEFAULT');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE external_categories_map ALTER external_id TYPE INT');
        $this->addSql('ALTER TABLE external_categories_map ALTER external_id DROP DEFAULT');
        $this->addSql('ALTER TABLE external_categories_map ALTER source TYPE INT');
        $this->addSql('ALTER TABLE external_categories_map ALTER source DROP DEFAULT');
    }
}
