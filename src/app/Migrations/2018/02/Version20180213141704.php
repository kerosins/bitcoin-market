<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180213141704 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE fos_user ADD country_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A6479F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_957A6479F92F3E70 ON fos_user (country_id)');
        $this->addSql('ALTER TABLE user_delivery_info DROP CONSTRAINT fk_f49b549d5d83cc1');
        $this->addSql('ALTER TABLE user_delivery_info DROP CONSTRAINT fk_f49b549d8bac62af');
        $this->addSql('ALTER TABLE user_delivery_info DROP CONSTRAINT fk_f49b549da1ace158');
        $this->addSql('DROP INDEX idx_f49b549d5d83cc1');
        $this->addSql('DROP INDEX idx_f49b549d8bac62af');
        $this->addSql('DROP INDEX idx_f49b549da1ace158');
        $this->addSql('ALTER TABLE user_delivery_info ADD state JSON NOT NULL');
        $this->addSql('ALTER TABLE user_delivery_info ADD city JSON NOT NULL');
        $this->addSql('ALTER TABLE user_delivery_info DROP state_id');
        $this->addSql('ALTER TABLE user_delivery_info DROP city_id');
        $this->addSql('ALTER TABLE user_delivery_info DROP zip_code');
        $this->addSql('ALTER TABLE user_delivery_info ALTER apartment DROP NOT NULL');
        $this->addSql('ALTER TABLE user_delivery_info RENAME COLUMN country TO zip');
        $this->addSql('COMMENT ON COLUMN user_delivery_info.state IS \'(DC2Type:geo_object)\'');
        $this->addSql('COMMENT ON COLUMN user_delivery_info.city IS \'(DC2Type:geo_object)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE fos_user DROP CONSTRAINT FK_957A6479F92F3E70');
        $this->addSql('DROP INDEX IDX_957A6479F92F3E70');
        $this->addSql('ALTER TABLE fos_user DROP country_id');
        $this->addSql('ALTER TABLE user_delivery_info ADD state_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_delivery_info ADD city_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_delivery_info ADD zip_code INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_delivery_info DROP state');
        $this->addSql('ALTER TABLE user_delivery_info DROP city');
        $this->addSql('ALTER TABLE user_delivery_info ALTER apartment SET NOT NULL');
        $this->addSql('ALTER TABLE user_delivery_info RENAME COLUMN zip TO country');
        $this->addSql('ALTER TABLE user_delivery_info ADD CONSTRAINT fk_f49b549d5d83cc1 FOREIGN KEY (state_id) REFERENCES address_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_delivery_info ADD CONSTRAINT fk_f49b549d8bac62af FOREIGN KEY (city_id) REFERENCES address_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_delivery_info ADD CONSTRAINT fk_f49b549da1ace158 FOREIGN KEY (zip_code) REFERENCES zip_code (zip) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_f49b549d5d83cc1 ON user_delivery_info (state_id)');
        $this->addSql('CREATE INDEX idx_f49b549d8bac62af ON user_delivery_info (city_id)');
        $this->addSql('CREATE INDEX idx_f49b549da1ace158 ON user_delivery_info (zip_code)');
    }
}
