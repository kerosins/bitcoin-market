<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180521215959 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE currency_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE currency_rate_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE currency_rate (id INT NOT NULL, rate NUMERIC(10, 7) NOT NULL, currency VARCHAR(255) NOT NULL, exchange VARCHAR(255) NOT NULL, is_best BOOLEAN NOT NULL, is_active BOOLEAN NOT NULL, difference NUMERIC(10, 3) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('DROP TABLE currency');
        $this->addSql('ALTER TABLE order_product ADD inner_currency VARCHAR(255) DEFAULT \'USD\'');
        $this->addSql('ALTER TABLE order_product ALTER inner_currency DROP DEFAULT');
        $this->addSql('ALTER TABLE order_product ALTER inner_currency SET NOT NULL');

        $this->addSql('ALTER TABLE order_product ADD price_scale NUMERIC(10, 2) DEFAULT 0.00');
        $this->addSql('ALTER TABLE order_product ALTER price_scale DROP DEFAULT');
        $this->addSql('ALTER TABLE order_product ALTER price_scale SET NOT NULL');

        $this->addSql('ALTER TABLE order_product ALTER currency DROP NOT NULL');
        $this->addSql('ALTER TABLE order_product ALTER public_price DROP NOT NULL');
        $this->addSql('ALTER TABLE order_product ALTER public_rate_value DROP NOT NULL');

        $this->addSql('ALTER TABLE znt_account ADD is_main_system BOOLEAN DEFAULT \'f\'');
        $this->addSql('ALTER TABLE znt_account ALTER is_main_system DROP DEFAULT');
        $this->addSql('ALTER TABLE znt_account ALTER is_main_system SET NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE currency_rate_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE currency_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE currency (id INT NOT NULL, rate NUMERIC(10, 7) NOT NULL, currency VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, is_best BOOLEAN NOT NULL, is_active BOOLEAN NOT NULL, exchange VARCHAR(255) NOT NULL, difference NUMERIC(10, 3) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('DROP TABLE currency_rate');
        $this->addSql('ALTER TABLE order_product DROP inner_currency');
        $this->addSql('ALTER TABLE order_product DROP price_scale');
        $this->addSql('ALTER TABLE order_product ALTER public_price SET NOT NULL');
        $this->addSql('ALTER TABLE order_product ALTER public_rate_value SET NOT NULL');
        $this->addSql('ALTER TABLE order_product ALTER currency SET NOT NULL');
        $this->addSql('ALTER TABLE znt_account DROP is_main_system');
    }
}
