<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180521225104 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE znt_transaction_id_seq CASCADE');
        $this->addSql('ALTER TABLE znt_transaction DROP CONSTRAINT FK_DCD419A641AC638A');
        $this->addSql('ALTER TABLE znt_transaction ALTER id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE znt_transaction ALTER prev_transaction_id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE znt_transaction ADD CONSTRAINT FK_DCD419A641AC638A FOREIGN KEY (prev_transaction_id) REFERENCES znt_transaction (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE znt_transaction_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('ALTER TABLE znt_transaction ALTER id TYPE INT');
        $this->addSql('ALTER TABLE znt_transaction ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE znt_transaction ALTER prev_transaction_id TYPE INT');
        $this->addSql('ALTER TABLE znt_transaction ALTER prev_transaction_id DROP DEFAULT');
    }
}
