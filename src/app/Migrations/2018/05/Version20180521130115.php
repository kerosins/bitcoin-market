<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180521130115 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE order_payment (order_id INT NOT NULL, payment_id INT NOT NULL, PRIMARY KEY(order_id, payment_id))');
        $this->addSql('CREATE INDEX IDX_9B522D468D9F6D38 ON order_payment (order_id)');
        $this->addSql('CREATE INDEX IDX_9B522D464C3A3BB ON order_payment (payment_id)');
        $this->addSql('ALTER TABLE order_payment ADD CONSTRAINT FK_9B522D468D9F6D38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_payment ADD CONSTRAINT FK_9B522D464C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE fos_user ADD account_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A64799B6B5FBA FOREIGN KEY (account_id) REFERENCES znt_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_957A64799B6B5FBA ON fos_user (account_id)');
        $this->addSql('ALTER TABLE order_tbl DROP CONSTRAINT fk_fc2d72a84c3a3bb');
        $this->addSql('DROP INDEX uniq_fc2d72a84c3a3bb');
        $this->addSql('ALTER TABLE order_tbl DROP payment_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE order_payment');
        $this->addSql('ALTER TABLE fos_user DROP CONSTRAINT FK_957A64799B6B5FBA');
        $this->addSql('DROP INDEX UNIQ_957A64799B6B5FBA');
        $this->addSql('ALTER TABLE fos_user DROP account_id');
        $this->addSql('ALTER TABLE order_tbl ADD payment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_tbl ADD CONSTRAINT fk_fc2d72a84c3a3bb FOREIGN KEY (payment_id) REFERENCES payment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_fc2d72a84c3a3bb ON order_tbl (payment_id)');
    }
}
