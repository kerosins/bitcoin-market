<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180520004008 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE znt_transaction ADD prev_transaction_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE znt_transaction ADD CONSTRAINT FK_DCD419A641AC638A FOREIGN KEY (prev_transaction_id) REFERENCES znt_transaction (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DCD419A641AC638A ON znt_transaction (prev_transaction_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE znt_transaction DROP CONSTRAINT FK_DCD419A641AC638A');
        $this->addSql('DROP INDEX UNIQ_DCD419A641AC638A');
        $this->addSql('ALTER TABLE znt_transaction DROP prev_transaction_id');
    }
}
