<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180520002501 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE znt_account_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE znt_transaction_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE znt_account (id INT NOT NULL, is_bot BOOLEAN NOT NULL, description VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE znt_transaction (id INT NOT NULL, from_account_id INT DEFAULT NULL, to_account_id INT DEFAULT NULL, amount NUMERIC(10, 5) NOT NULL, hash VARCHAR(255) DEFAULT NULL, previous_hash VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DCD419A6B0CF99BD ON znt_transaction (from_account_id)');
        $this->addSql('CREATE INDEX IDX_DCD419A6BC58BDC7 ON znt_transaction (to_account_id)');
        $this->addSql('ALTER TABLE znt_transaction ADD CONSTRAINT FK_DCD419A6B0CF99BD FOREIGN KEY (from_account_id) REFERENCES znt_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE znt_transaction ADD CONSTRAINT FK_DCD419A6BC58BDC7 FOREIGN KEY (to_account_id) REFERENCES znt_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE znt_transaction DROP CONSTRAINT FK_DCD419A6B0CF99BD');
        $this->addSql('ALTER TABLE znt_transaction DROP CONSTRAINT FK_DCD419A6BC58BDC7');
        $this->addSql('DROP SEQUENCE znt_account_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE znt_transaction_id_seq CASCADE');
        $this->addSql('DROP TABLE znt_account');
        $this->addSql('DROP TABLE znt_transaction');
    }
}
