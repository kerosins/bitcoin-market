<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180504150251 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE purchase_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE referral_purchase_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE referral_purchase (id INT NOT NULL, program_id VARCHAR(255) DEFAULT NULL, order_id INT DEFAULT NULL, amount NUMERIC(10, 0) NOT NULL, currency VARCHAR(255) NOT NULL, rate NUMERIC(5, 0) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B343749A3EB8070A ON referral_purchase (program_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B343749A8D9F6D38 ON referral_purchase (order_id)');
        $this->addSql('ALTER TABLE referral_purchase ADD CONSTRAINT FK_B343749A3EB8070A FOREIGN KEY (program_id) REFERENCES referral_program (code) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE referral_purchase ADD CONSTRAINT FK_B343749A8D9F6D38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE purchase');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE referral_purchase_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE purchase_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE purchase (id INT NOT NULL, program_id VARCHAR(255) DEFAULT NULL, order_id INT DEFAULT NULL, amount NUMERIC(10, 0) NOT NULL, currency VARCHAR(255) NOT NULL, rate NUMERIC(5, 0) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_6117d13b8d9f6d38 ON purchase (order_id)');
        $this->addSql('CREATE INDEX idx_6117d13b3eb8070a ON purchase (program_id)');
        $this->addSql('ALTER TABLE purchase ADD CONSTRAINT fk_6117d13b3eb8070a FOREIGN KEY (program_id) REFERENCES referral_program (code) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE purchase ADD CONSTRAINT fk_6117d13b8d9f6d38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE referral_purchase');
    }
}
