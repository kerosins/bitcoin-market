<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180423135253 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE page_alias ADD is_main BOOLEAN DEFAULT \'f\'');
        $this->addSql('ALTER TABLE page_alias ALTER is_main DROP DEFAULT');
        $this->addSql('ALTER TABLE page_alias ALTER is_main SET NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_87ED9442841CB121 ON page_alias (uri)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX UNIQ_87ED9442841CB121');
        $this->addSql('ALTER TABLE page_alias DROP is_main');
    }
}
