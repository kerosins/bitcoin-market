<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180822194645 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE dispute_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE order_message_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE dispute (id INT NOT NULL, order_position_id INT DEFAULT NULL, status INT NOT NULL, refund_amount NUMERIC(20, 8) DEFAULT NULL, decline_reason TEXT DEFAULT NULL, is_processing_refund BOOLEAN DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3C925007C63651C0 ON dispute (order_position_id)');
        $this->addSql('CREATE TABLE order_message (id INT NOT NULL, from_user_id INT DEFAULT NULL, to_user_id INT DEFAULT NULL, order_id INT DEFAULT NULL, dispute_id INT DEFAULT NULL, from_display_name VARCHAR(255) NOT NULL, to_display_name VARCHAR(255) NOT NULL, message TEXT NOT NULL, attachments JSON DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, target VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_40C799342130303A ON order_message (from_user_id)');
        $this->addSql('CREATE INDEX IDX_40C7993429F6EE60 ON order_message (to_user_id)');
        $this->addSql('CREATE INDEX IDX_40C799348D9F6D38 ON order_message (order_id)');
        $this->addSql('CREATE INDEX IDX_40C79934C7B47CB5 ON order_message (dispute_id)');
        $this->addSql('COMMENT ON COLUMN order_message.attachments IS \'(DC2Type:image)\'');
        $this->addSql('ALTER TABLE dispute ADD CONSTRAINT FK_3C925007C63651C0 FOREIGN KEY (order_position_id) REFERENCES order_product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_message ADD CONSTRAINT FK_40C799342130303A FOREIGN KEY (from_user_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_message ADD CONSTRAINT FK_40C7993429F6EE60 FOREIGN KEY (to_user_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_message ADD CONSTRAINT FK_40C799348D9F6D38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_message ADD CONSTRAINT FK_40C79934C7B47CB5 FOREIGN KEY (dispute_id) REFERENCES dispute (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE order_message DROP CONSTRAINT FK_40C79934C7B47CB5');
        $this->addSql('DROP SEQUENCE dispute_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE order_message_id_seq CASCADE');
        $this->addSql('DROP TABLE dispute');
        $this->addSql('DROP TABLE order_message');
    }
}
