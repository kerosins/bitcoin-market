<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180801135751 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE paybear_payment ADD address VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE paybear_payment ADD invoice VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE paybear_payment ADD callback_uri VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE paybear_payment ADD amount NUMERIC(10, 9) NOT NULL');
        $this->addSql('ALTER TABLE paybear_payment ADD currency VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE paybear_payment ADD block JSON NOT NULL');
        $this->addSql('ALTER TABLE paybear_payment ADD transaction JSON NOT NULL');
        $this->addSql('ALTER TABLE paybear_payment ADD confirmation_number INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE paybear_payment DROP address');
        $this->addSql('ALTER TABLE paybear_payment DROP invoice');
        $this->addSql('ALTER TABLE paybear_payment DROP callback_uri');
        $this->addSql('ALTER TABLE paybear_payment DROP amount');
        $this->addSql('ALTER TABLE paybear_payment DROP currency');
        $this->addSql('ALTER TABLE paybear_payment DROP block');
        $this->addSql('ALTER TABLE paybear_payment DROP transaction');
        $this->addSql('ALTER TABLE paybear_payment DROP confirmation_number');
    }
}
