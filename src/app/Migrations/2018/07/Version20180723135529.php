<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180723135529 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE item_block_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE item_block (id INT NOT NULL, title VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, params JSON NOT NULL, enable BOOLEAN NOT NULL, display_in_main BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE product ALTER amount_items DROP DEFAULT');
        $this->addSql('ALTER TABLE product ALTER amount_orders DROP DEFAULT');
        $this->addSql('ALTER TABLE product ALTER amount_wish_lists DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE item_block_id_seq CASCADE');
        $this->addSql('DROP TABLE item_block');
        $this->addSql('ALTER TABLE product ALTER amount_items SET DEFAULT 0');
        $this->addSql('ALTER TABLE product ALTER amount_orders SET DEFAULT 0');
        $this->addSql('ALTER TABLE product ALTER amount_wish_lists SET DEFAULT 0');
    }
}
