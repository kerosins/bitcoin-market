<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180707234306 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE product ADD amount_items INT DEFAULT 0');
        $this->addSql('ALTER TABLE product ADD amount_orders INT DEFAULT 0');
        $this->addSql('ALTER TABLE product ADD amount_wish_lists INT DEFAULT 0');
        $this->addSql('ALTER TABLE product ADD seller_name VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product DROP amount_items');
        $this->addSql('ALTER TABLE product DROP amount_orders');
        $this->addSql('ALTER TABLE product DROP amount_wish_lists');
        $this->addSql('ALTER TABLE product DROP seller_name');
    }
}
