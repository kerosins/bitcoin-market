<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180625202517 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE kraken_transaction DROP CONSTRAINT fk_6d589326712520f3');
        $this->addSql('DROP SEQUENCE kraken_transaction_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE kraken_wallet_address_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE referral_withdrawal_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE referral_withdrawal (id INT NOT NULL, user_id INT DEFAULT NULL, amount NUMERIC(10, 2) NOT NULL, comment TEXT DEFAULT NULL, status INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7C8D33DEA76ED395 ON referral_withdrawal (user_id)');
        $this->addSql('ALTER TABLE referral_withdrawal ADD CONSTRAINT FK_7C8D33DEA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE kraken_wallet_address');
        $this->addSql('DROP TABLE kraken_transaction');

        $this->addSql('ALTER TABLE referral_purchase ADD amount_bonuses NUMERIC(10, 2) DEFAULT 0.0');
        $this->addSql('ALTER TABLE referral_purchase ALTER amount_bonuses DROP DEFAULT');
        $this->addSql('ALTER TABLE referral_purchase ALTER amount_bonuses SET NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE referral_withdrawal_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE kraken_transaction_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE kraken_wallet_address_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE kraken_wallet_address (id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE kraken_transaction (id INT NOT NULL, wallet_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_6d589326712520f3 ON kraken_transaction (wallet_id)');
        $this->addSql('ALTER TABLE kraken_transaction ADD CONSTRAINT fk_6d589326712520f3 FOREIGN KEY (wallet_id) REFERENCES kraken_wallet_address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE referral_withdrawal');
        $this->addSql('ALTER TABLE referral_purchase DROP amount_bonuses');
    }
}
