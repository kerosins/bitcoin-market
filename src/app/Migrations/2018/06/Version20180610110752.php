<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180610110752 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('COMMENT ON COLUMN assembly_facet.data IS \'(DC2Type:json_array)\'');
        $this->addSql('COMMENT ON COLUMN facet_category.props IS \'(DC2Type:json_array)\'');
        $this->addSql('COMMENT ON COLUMN offer.data IS \'(DC2Type:json_array)\'');
        $this->addSql('COMMENT ON COLUMN progress_model.data IS \'(DC2Type:json_array)\'');
        $this->addSql('COMMENT ON COLUMN progress_model.result IS \'(DC2Type:json_array)\'');
        $this->addSql('COMMENT ON COLUMN browser_data.headers IS \'(DC2Type:json_array)\'');
        $this->addSql('COMMENT ON COLUMN browser_data.location IS \'(DC2Type:json_array)\'');
        $this->addSql('ALTER TABLE order_product ADD shipment_tax DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE order_product DROP price_scale');
        $this->addSql('ALTER TABLE order_product ALTER public_price TYPE NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE order_product RENAME COLUMN public_rate_value TO currency_rate');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE order_product ADD price_scale NUMERIC(10, 2) NOT NULL');
        $this->addSql('ALTER TABLE order_product DROP shipment_tax');
        $this->addSql('ALTER TABLE order_product ALTER public_price TYPE NUMERIC(10, 5)');
        $this->addSql('ALTER TABLE order_product RENAME COLUMN currency_rate TO public_rate_value');
        $this->addSql('COMMENT ON COLUMN facet_category.props IS NULL');
        $this->addSql('COMMENT ON COLUMN assembly_facet.data IS NULL');
        $this->addSql('COMMENT ON COLUMN progress_model.data IS NULL');
        $this->addSql('COMMENT ON COLUMN progress_model.result IS NULL');
        $this->addSql('COMMENT ON COLUMN offer.data IS NULL');
        $this->addSql('COMMENT ON COLUMN browser_data.headers IS NULL');
        $this->addSql('COMMENT ON COLUMN browser_data.location IS NULL');
    }
}
