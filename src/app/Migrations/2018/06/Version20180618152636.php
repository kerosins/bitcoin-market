<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180618152636 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE user_wish_product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE kraken_transaction_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE kraken_wallet_address_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE user_wish_product (id INT NOT NULL, user_id INT NOT NULL, product_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B8B75D35A76ED395 ON user_wish_product (user_id)');
        $this->addSql('CREATE INDEX IDX_B8B75D354584665A ON user_wish_product (product_id)');
        $this->addSql('CREATE TABLE kraken_transaction (id INT NOT NULL, wallet_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6D589326712520F3 ON kraken_transaction (wallet_id)');
        $this->addSql('CREATE TABLE kraken_wallet_address (id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE user_wish_product ADD CONSTRAINT FK_B8B75D35A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_wish_product ADD CONSTRAINT FK_B8B75D354584665A FOREIGN KEY (product_id) REFERENCES fos_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kraken_transaction ADD CONSTRAINT FK_6D589326712520F3 FOREIGN KEY (wallet_id) REFERENCES kraken_wallet_address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE kraken_transaction DROP CONSTRAINT FK_6D589326712520F3');
        $this->addSql('DROP SEQUENCE user_wish_product_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE kraken_transaction_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE kraken_wallet_address_id_seq CASCADE');
        $this->addSql('DROP TABLE user_wish_product');
        $this->addSql('DROP TABLE kraken_transaction');
        $this->addSql('DROP TABLE kraken_wallet_address');
    }
}
