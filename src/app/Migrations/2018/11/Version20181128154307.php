<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181128154307 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE referral_program ADD referral_settings JSON DEFAULT \'[]\'');
        $this->addSql('ALTER TABLE referral_program ALTER referral_settings DROP DEFAULT ');
        $this->addSql('ALTER TABLE referral_program ALTER referral_settings SET NOT NULL');
        $this->addSql('COMMENT ON COLUMN referral_program.referral_settings IS \'(DC2Type:referral_settings)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE referral_program ADD settings JSON DEFAULT \'[]\'');
        $this->addSql('ALTER TABLE referral_program DROP referral_settings');
        $this->addSql('COMMENT ON COLUMN referral_program.settings IS \'(DC2Type:referral_settings)\'');
    }
}
