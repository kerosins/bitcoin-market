<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181127161146 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE delivery_fee_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE ali_express_delivery_fee_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE ali_express_delivery_fee (id INT NOT NULL, product_id INT DEFAULT NULL, country_id INT DEFAULT NULL, quantity INT NOT NULL, cost NUMERIC(10, 2) NOT NULL, commit_day INT NOT NULL, full_response TEXT DEFAULT NULL, selected_company JSON DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C871748F4584665A ON ali_express_delivery_fee (product_id)');
        $this->addSql('CREATE INDEX IDX_C871748FF92F3E70 ON ali_express_delivery_fee (country_id)');
        $this->addSql('ALTER TABLE ali_express_delivery_fee ADD CONSTRAINT FK_C871748F4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ali_express_delivery_fee ADD CONSTRAINT FK_C871748FF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE delivery_fee');
        $this->addSql('ALTER TABLE order_position ADD ali_express_delivery_fee_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_position ADD CONSTRAINT FK_A7D40644E079641D FOREIGN KEY (ali_express_delivery_fee_id) REFERENCES ali_express_delivery_fee (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A7D40644E079641D ON order_position (ali_express_delivery_fee_id)');
        $this->addSql('ALTER TABLE supplier ADD internal BOOLEAN DEFAULT \'t\'');
        $this->addSql('ALTER TABLE supplier ALTER internal DROP DEFAULT');
        $this->addSql('ALTER TABLE supplier ALTER internal SET NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE order_position DROP CONSTRAINT FK_A7D40644E079641D');
        $this->addSql('DROP SEQUENCE ali_express_delivery_fee_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE delivery_fee_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE delivery_fee (id INT NOT NULL, product_id INT DEFAULT NULL, country_id INT DEFAULT NULL, quantity INT NOT NULL, cost NUMERIC(10, 2) NOT NULL, commit_day INT NOT NULL, full_response TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, selected_company JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_b1e60f11f92f3e70 ON delivery_fee (country_id)');
        $this->addSql('CREATE INDEX idx_b1e60f114584665a ON delivery_fee (product_id)');
        $this->addSql('ALTER TABLE delivery_fee ADD CONSTRAINT fk_b1e60f11f92f3e70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE delivery_fee ADD CONSTRAINT fk_b1e60f114584665a FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE ali_express_delivery_fee');
        $this->addSql('DROP INDEX IDX_A7D40644E079641D');
        $this->addSql('ALTER TABLE order_position DROP ali_express_delivery_fee_id');
        $this->addSql('ALTER TABLE supplier DROP internal');
    }
}
