<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181117185344 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("
            INSERT INTO slug (slug, id, target, target_id, weight, is_main, created_at)
            SELECT 
                array_to_string(regexp_matches(pa.uri, '(?<=\/)([[:alnum:]\-]*)(?=\-(\d+))'), '') as slug,
                pa.id, 
                pa.target, 
                pa.target_id, 
                pa.weight, 
                pa.is_main, 
                pa.created_at
            FROM page_alias pa
        ");

        $this->addSql("SELECT setval('slug_id_seq', nextval('page_alias_id_seq'))");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('TRUNCATE TABLE slug');
        $this->addSql("SELECT setval('slug_id_seq', 1)");
    }
}
