<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181023162636 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE dispute DROP CONSTRAINT fk_3c925007c63651c0');
        $this->addSql('DROP SEQUENCE order_product_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE delivery_fee_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE order_position_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE delivery_fee (id INT NOT NULL, product_id INT DEFAULT NULL, country_id INT DEFAULT NULL, quantity INT NOT NULL, cost NUMERIC(10, 2) NOT NULL, commit_day INT NOT NULL, full_response TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B1E60F114584665A ON delivery_fee (product_id)');
        $this->addSql('CREATE INDEX IDX_B1E60F11F92F3E70 ON delivery_fee (country_id)');
        $this->addSql('CREATE TABLE order_position (id INT NOT NULL, product_id INT DEFAULT NULL, order_id INT DEFAULT NULL, public_price NUMERIC(10, 2) DEFAULT NULL, currency_rate NUMERIC(10, 7) DEFAULT NULL, inner_price NUMERIC(10, 2) NOT NULL, inner_currency VARCHAR(255) NOT NULL, quantity INT NOT NULL, currency VARCHAR(255) DEFAULT NULL, status INT NOT NULL, comment TEXT DEFAULT NULL, shipment_tax DOUBLE PRECISION DEFAULT NULL, track_code VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A7D406444584665A ON order_position (product_id)');
        $this->addSql('CREATE INDEX IDX_A7D406448D9F6D38 ON order_position (order_id)');
        $this->addSql('ALTER TABLE delivery_fee ADD CONSTRAINT FK_B1E60F114584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE delivery_fee ADD CONSTRAINT FK_B1E60F11F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_position ADD CONSTRAINT FK_A7D406444584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_position ADD CONSTRAINT FK_A7D406448D9F6D38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE order_product');
        $this->addSql('ALTER TABLE country ADD ali_express_code VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE dispute DROP CONSTRAINT FK_3C925007C63651C0');
        $this->addSql('DROP SEQUENCE delivery_fee_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE order_position_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE order_product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE order_product (id INT NOT NULL, product_id INT DEFAULT NULL, order_id INT DEFAULT NULL, inner_price NUMERIC(10, 2) NOT NULL, quantity INT NOT NULL, currency VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, public_price NUMERIC(10, 2) DEFAULT NULL, currency_rate NUMERIC(10, 7) DEFAULT NULL, status INT NOT NULL, comment TEXT DEFAULT NULL, track_code VARCHAR(255) DEFAULT NULL, inner_currency VARCHAR(255) NOT NULL, shipment_tax DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_2530ade64584665a ON order_product (product_id)');
        $this->addSql('CREATE INDEX idx_2530ade68d9f6d38 ON order_product (order_id)');
        $this->addSql('ALTER TABLE order_product ADD CONSTRAINT fk_2530ade64584665a FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_product ADD CONSTRAINT fk_2530ade68d9f6d38 FOREIGN KEY (order_id) REFERENCES order_tbl (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE delivery_fee');
        $this->addSql('DROP TABLE order_position');
        $this->addSql('ALTER TABLE country DROP ali_express_code');
        $this->addSql('ALTER TABLE dispute DROP CONSTRAINT fk_3c925007c63651c0');
        $this->addSql('ALTER TABLE dispute ADD CONSTRAINT fk_3c925007c63651c0 FOREIGN KEY (order_position_id) REFERENCES order_product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
