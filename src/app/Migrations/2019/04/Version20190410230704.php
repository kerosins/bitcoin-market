<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190410230704 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
        $this->connection->insert(
            'system_setting',
            [
                'name' => 'check_product_available',
                'title' => 'Check Product Available',
                'value' => 't',
                'type' => 'bool',
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
                'enabled' => 't',
            ],
            [
                'created_at' => 'datetime',
                'updated_at' => 'datetime'
            ]
        );
    }
}
