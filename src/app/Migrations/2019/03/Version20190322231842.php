<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190322231842 extends AbstractMigration
{
    private $proxies = [
        ['194.58.92.55', 'user01', 'Grjc$9dsmFRnd7GL', 'f'],
        ['194.58.118.14', 'user02', 'BwTp9#nDF7kLS', 'f'],
        ['80.78.248.216', 'user03', 'MusQ0pD6Vj3nSD', 'f'],
        ['178.21.10.232', 'user04', 'VesWmdXSDFO43nf', 'f'],
        ['89.108.71.196', 'user05', 'RjSqo5n3mRLSp', 'f'],
        ['37.140.199.103', 'user06', 'NqlDrh58AkEm40', 'f'],
        ['194.58.98.30', 'user07', 'HQmp83bfRksl23', 't'],
        ['151.248.117.179', 'user08', 'NAw95bfRgnpasj5', 't'],
        ['151.248.117.188', 'user09', 'Ekmf5jd0dOPsj1aiRt', 't']
    ];

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE http_proxy_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE http_proxy (id INT NOT NULL, ip VARCHAR(255) NOT NULL, username VARCHAR(255) DEFAULT NULL, password VARCHAR(255) DEFAULT NULL, is_banned BOOLEAN NOT NULL, only_runtime BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE category ADD is_auto_update BOOLEAN DEFAULT \'f\'');
        $this->addSql('ALTER TABLE category ALTER is_auto_update DROP DEFAULT');
        $this->addSql('ALTER TABLE category ALTER is_auto_update SET NOT NULL');
    }

    public function postUp(Schema $schema)
    {
        foreach ($this->proxies as $proxy) {
            $values = array_combine(['ip', 'username', 'password', 'only_runtime'], $proxy);
            $values['is_banned'] = 'f';
            $values['created_at'] = $values['updated_at'] = new \DateTime();
            $id = $this->connection->fetchColumn('SELECT nextval(\'http_proxy_id_seq\')');
            $values['id'] = $id;

            $this->connection->insert('http_proxy', $values, ['created_at' => 'datetime', 'updated_at' => 'datetime']);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE http_proxy_id_seq CASCADE');
        $this->addSql('DROP TABLE http_proxy');
        $this->addSql('ALTER TABLE category DROP is_auto_update');
    }
}
