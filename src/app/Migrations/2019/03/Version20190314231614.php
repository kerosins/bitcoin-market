<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190314231614 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE balance_change_log ADD withdrawal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE balance_change_log ADD CONSTRAINT FK_6B90CFA697D393B FOREIGN KEY (withdrawal_id) REFERENCES referral_withdrawal (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_6B90CFA697D393B ON balance_change_log (withdrawal_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE balance_change_log DROP CONSTRAINT FK_6B90CFA697D393B');
        $this->addSql('DROP INDEX IDX_6B90CFA697D393B');
        $this->addSql('ALTER TABLE balance_change_log DROP withdrawal_id');
    }
}
