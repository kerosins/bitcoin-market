<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190308231324 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE balance_change_log_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE balance_change_log (id INT NOT NULL, user_id INT DEFAULT NULL, transaction_hash VARCHAR(255) DEFAULT NULL, amount NUMERIC(15, 5) NOT NULL, status INT NOT NULL, comment TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6B90CFAA76ED395 ON balance_change_log (user_id)');
        $this->addSql('CREATE INDEX IDX_6B90CFA33FB5C19 ON balance_change_log (transaction_hash)');
        $this->addSql('ALTER TABLE balance_change_log ADD CONSTRAINT FK_6B90CFAA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE balance_change_log ADD CONSTRAINT FK_6B90CFA33FB5C19 FOREIGN KEY (transaction_hash) REFERENCES znt_transaction (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE fos_user DROP CONSTRAINT FK_957A64799B6B5FBA');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A64799B6B5FBA FOREIGN KEY (account_id) REFERENCES znt_account (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE balance_change_log_id_seq CASCADE');
        $this->addSql('DROP TABLE balance_change_log');
        $this->addSql('ALTER TABLE fos_user DROP CONSTRAINT fk_957a64799b6b5fba');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT fk_957a64799b6b5fba FOREIGN KEY (account_id) REFERENCES znt_account (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
