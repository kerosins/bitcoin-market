<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190213072930 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE fos_user DROP CONSTRAINT FK_957A64799B6B5FBA');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A64799B6B5FBA FOREIGN KEY (account_id) REFERENCES znt_account (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE fos_user DROP CONSTRAINT fk_957a64799b6b5fba');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT fk_957a64799b6b5fba FOREIGN KEY (account_id) REFERENCES znt_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
