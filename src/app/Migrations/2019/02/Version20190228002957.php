<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190228002957 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE INDEX idx_product_status ON product (status)');
        $this->addSql('CREATE INDEX idx_product_external_source ON product (external_source, external_id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP INDEX idx_product_status');
        $this->addSql('DROP INDEX idx_product_external_source');
    }


    public function postUp(Schema $schema)
    {
        $this->connection->executeQuery("
            UPDATE product AS p 
            SET
                source = 'epn_yml',
                external_source = 'aliexpress',
                external_id = e.external_id,
                status = CASE WHEN available = 't' THEN 0 ELSE 1 END
            FROM external_products_map e 
            WHERE p.id = e.product_id 
        ");
    }
}
