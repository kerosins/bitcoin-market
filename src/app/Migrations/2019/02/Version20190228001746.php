<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190228001746 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE product ADD source VARCHAR(255) DEFAULT \'epn_yml\'');
        $this->addSql('ALTER TABLE product ALTER source DROP DEFAULT');
        $this->addSql('ALTER TABLE product ALTER source SET NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP source');
    }
}
