<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190116152631 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX search_idx');
        $this->addSql('ALTER TABLE external_products_map ADD count_failure_availability INT DEFAULT 0');
        $this->addSql('ALTER TABLE external_products_map ADD deleted BOOLEAN DEFAULT \'f\'');

        $this->addSql('ALTER TABLE external_products_map ALTER deleted DROP DEFAULT');
        $this->addSql('ALTER TABLE external_products_map ALTER count_failure_availability DROP DEFAULT');

        $this->addSql('ALTER TABLE external_products_map ALTER deleted SET NOT NULL');
        $this->addSql('ALTER TABLE external_products_map ALTER count_failure_availability SET NOT NULL');

        $this->addSql('CREATE INDEX search_idx ON external_products_map (external_id, source, deleted)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX search_idx');
        $this->addSql('ALTER TABLE external_products_map DROP count_failure_availability');
        $this->addSql('ALTER TABLE external_products_map DROP deleted');
        $this->addSql('CREATE INDEX search_idx ON external_products_map (external_id, source)');
    }
}
