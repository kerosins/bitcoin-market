<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171208192051 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE route_alias_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_product_favorite_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE route_alias (id INT NOT NULL, alias VARCHAR(255) NOT NULL, ref_id INT NOT NULL, entity VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user_product_favorite (id INT NOT NULL, product_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DE60A6934584665A ON user_product_favorite (product_id)');
        $this->addSql('CREATE INDEX IDX_DE60A693A76ED395 ON user_product_favorite (user_id)');
        $this->addSql('ALTER TABLE user_product_favorite ADD CONSTRAINT FK_DE60A6934584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_product_favorite ADD CONSTRAINT FK_DE60A693A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD meta_title TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD meta_description TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD meta_keywords TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE product DROP main_image');
        $this->addSql('ALTER TABLE product ADD main_image JSON NOT NULL DEFAULT \'{}\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE route_alias_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_product_favorite_id_seq CASCADE');
        $this->addSql('DROP TABLE route_alias');
        $this->addSql('DROP TABLE user_product_favorite');
        $this->addSql('ALTER TABLE product DROP meta_title');
        $this->addSql('ALTER TABLE product DROP meta_description');
        $this->addSql('ALTER TABLE product DROP meta_keywords');
        $this->addSql('ALTER TABLE product DROP main_image');
        $this->addSql('ALTER TABLE product ADD main_image VARCHAR(255)');
    }
}
