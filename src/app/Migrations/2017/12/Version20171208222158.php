<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171208222158 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE category ADD meta_title TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE category ADD meta_description TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE category ADD meta_keywords TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD active BOOLEAN DEFAULT \'t\'');
        $this->addSql('ALTER TABLE product ALTER active DROP DEFAULT');
        $this->addSql('ALTER TABLE product ALTER active SET NOT NULL');
        $this->addSql('ALTER TABLE product ALTER images TYPE JSON');
        $this->addSql('ALTER TABLE product ALTER images DROP DEFAULT');
        $this->addSql('ALTER TABLE product ALTER main_image TYPE JSON');
        $this->addSql('ALTER TABLE product ALTER main_image DROP DEFAULT');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE category DROP meta_title');
        $this->addSql('ALTER TABLE category DROP meta_description');
        $this->addSql('ALTER TABLE category DROP meta_keywords');
        $this->addSql('ALTER TABLE product DROP active');
        $this->addSql('ALTER TABLE product ALTER images TYPE JSON');
        $this->addSql('ALTER TABLE product ALTER images DROP DEFAULT');
        $this->addSql('ALTER TABLE product ALTER main_image TYPE JSON');
        $this->addSql('ALTER TABLE product ALTER main_image SET DEFAULT \'{}\'');
    }
}
