<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170711141600 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE category_facet_category (category_id INT NOT NULL, facet_category_id INT NOT NULL, PRIMARY KEY(category_id, facet_category_id))');
        $this->addSql('CREATE INDEX IDX_D8B914D112469DE2 ON category_facet_category (category_id)');
        $this->addSql('CREATE INDEX IDX_D8B914D19C493C12 ON category_facet_category (facet_category_id)');
        $this->addSql('ALTER TABLE category_facet_category ADD CONSTRAINT FK_D8B914D112469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE category_facet_category ADD CONSTRAINT FK_D8B914D19C493C12 FOREIGN KEY (facet_category_id) REFERENCES facet_category (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE facet_category DROP CONSTRAINT fk_39a957ec12469de2');
        $this->addSql('DROP INDEX idx_39a957ec12469de2');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE category_facet_category');
        $this->addSql('ALTER TABLE facet_category ADD CONSTRAINT fk_39a957ec12469de2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_39a957ec12469de2 ON facet_category (category_id)');
    }
}
