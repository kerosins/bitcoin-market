<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170726003529 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE zip_code DROP CONSTRAINT FK_A1ACE158B96C6A63');
        $this->addSql('ALTER TABLE zip_code ADD CONSTRAINT FK_A1ACE158B96C6A63 FOREIGN KEY (address_object_id) REFERENCES address_object (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE zip_code DROP CONSTRAINT fk_a1ace158b96c6a63');
        $this->addSql('ALTER TABLE zip_code ADD CONSTRAINT fk_a1ace158b96c6a63 FOREIGN KEY (address_object_id) REFERENCES address_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
