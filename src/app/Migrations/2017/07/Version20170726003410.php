<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170726003410 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE address_object DROP CONSTRAINT FK_93F42E53727ACA70');
        $this->addSql('ALTER TABLE address_object ADD CONSTRAINT FK_93F42E53727ACA70 FOREIGN KEY (parent_id) REFERENCES address_object (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE address_object DROP CONSTRAINT fk_93f42e53727aca70');
        $this->addSql('ALTER TABLE address_object ADD CONSTRAINT fk_93f42e53727aca70 FOREIGN KEY (parent_id) REFERENCES address_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
