<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170728235707 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE order_delivery_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE order_delivery (id INT NOT NULL, order_id INT DEFAULT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, state VARCHAR(255) DEFAULT NULL, county VARCHAR(255) DEFAULT NULL, city VARCHAR(255) NOT NULL, address TEXT NOT NULL, apartment VARCHAR(255) NOT NULL, zip INT NOT NULL, email TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D6790EA18D9F6D38 ON order_delivery (order_id)');
        $this->addSql('ALTER TABLE order_delivery ADD CONSTRAINT FK_D6790EA18D9F6D38 FOREIGN KEY (order_id) REFERENCES "order" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_product ADD public_price NUMERIC(10, 5) NOT NULL');
        $this->addSql('ALTER TABLE order_product ADD public_rate_value NUMERIC(10, 7) NOT NULL');
        $this->addSql('ALTER TABLE order_product RENAME COLUMN price TO inner_price');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE order_delivery_id_seq CASCADE');
        $this->addSql('DROP TABLE order_delivery');
        $this->addSql('ALTER TABLE order_product DROP public_price');
        $this->addSql('ALTER TABLE order_product DROP public_rate_value');
        $this->addSql('ALTER TABLE order_product RENAME COLUMN inner_price TO price');
    }
}
