<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170711104200 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE assembly_facet_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE assembly_facet (id INT NOT NULL, category_id INT DEFAULT NULL, facet_id INT DEFAULT NULL, data JSON NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_707B9F5512469DE2 ON assembly_facet (category_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_707B9F55FC889F24 ON assembly_facet (facet_id)');
        $this->addSql('ALTER TABLE assembly_facet ADD CONSTRAINT FK_707B9F5512469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE assembly_facet ADD CONSTRAINT FK_707B9F55FC889F24 FOREIGN KEY (facet_id) REFERENCES facet_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE assembly_facet_id_seq CASCADE');
        $this->addSql('DROP TABLE assembly_facet');
    }
}
