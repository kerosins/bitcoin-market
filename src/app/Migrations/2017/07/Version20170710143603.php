<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170710143603 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE facet_category_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE facet_list_value_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_parameter_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE facet_category (id INT NOT NULL, category_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, type SMALLINT NOT NULL, sort INT NOT NULL, props JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_39A957EC12469DE2 ON facet_category (category_id)');
        $this->addSql('CREATE TABLE facet_list_value (id INT NOT NULL, title VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE product_parameter (id INT NOT NULL, product_id INT DEFAULT NULL, facet_id INT DEFAULT NULL, value INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4437279D4584665A ON product_parameter (product_id)');
        $this->addSql('CREATE INDEX IDX_4437279DFC889F24 ON product_parameter (facet_id)');
        $this->addSql('ALTER TABLE facet_category ADD CONSTRAINT FK_39A957EC12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_parameter ADD CONSTRAINT FK_4437279D4584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_parameter ADD CONSTRAINT FK_4437279DFC889F24 FOREIGN KEY (facet_id) REFERENCES facet_category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE fos_user ADD first_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE fos_user ADD last_name VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product_parameter DROP CONSTRAINT FK_4437279DFC889F24');
        $this->addSql('DROP SEQUENCE facet_category_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE facet_list_value_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_parameter_id_seq CASCADE');
        $this->addSql('DROP TABLE facet_category');
        $this->addSql('DROP TABLE facet_list_value');
        $this->addSql('DROP TABLE product_parameter');
        $this->addSql('ALTER TABLE fos_user DROP first_name');
        $this->addSql('ALTER TABLE fos_user DROP last_name');
    }
}
