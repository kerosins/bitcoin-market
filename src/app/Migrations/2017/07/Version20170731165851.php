<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170731165851 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE order_delivery ADD state_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_delivery ADD county_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_delivery ADD city_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_delivery DROP state');
        $this->addSql('ALTER TABLE order_delivery DROP county');
        $this->addSql('ALTER TABLE order_delivery DROP city');
        $this->addSql('ALTER TABLE order_delivery ADD CONSTRAINT FK_D6790EA15D83CC1 FOREIGN KEY (state_id) REFERENCES address_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_delivery ADD CONSTRAINT FK_D6790EA185E73F45 FOREIGN KEY (county_id) REFERENCES address_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_delivery ADD CONSTRAINT FK_D6790EA18BAC62AF FOREIGN KEY (city_id) REFERENCES address_object (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D6790EA15D83CC1 ON order_delivery (state_id)');
        $this->addSql('CREATE INDEX IDX_D6790EA185E73F45 ON order_delivery (county_id)');
        $this->addSql('CREATE INDEX IDX_D6790EA18BAC62AF ON order_delivery (city_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE order_delivery DROP CONSTRAINT FK_D6790EA15D83CC1');
        $this->addSql('ALTER TABLE order_delivery DROP CONSTRAINT FK_D6790EA185E73F45');
        $this->addSql('ALTER TABLE order_delivery DROP CONSTRAINT FK_D6790EA18BAC62AF');
        $this->addSql('DROP INDEX IDX_D6790EA15D83CC1');
        $this->addSql('DROP INDEX IDX_D6790EA185E73F45');
        $this->addSql('DROP INDEX IDX_D6790EA18BAC62AF');
        $this->addSql('ALTER TABLE order_delivery ADD state VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_delivery ADD county VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_delivery ADD city VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE order_delivery DROP state_id');
        $this->addSql('ALTER TABLE order_delivery DROP county_id');
        $this->addSql('ALTER TABLE order_delivery DROP city_id');
    }
}
