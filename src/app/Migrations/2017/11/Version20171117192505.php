<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171117192505 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_A1ACE158421D9546 ON zip_code (zip)');
        $this->addSql('ALTER TABLE user_delivery_info ADD zip_code INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_delivery_info DROP zip');
        $this->addSql('ALTER TABLE user_delivery_info ADD CONSTRAINT FK_F49B549DA1ACE158 FOREIGN KEY (zip_code) REFERENCES zip_code (zip) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F49B549DA1ACE158 ON user_delivery_info (zip_code)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX UNIQ_A1ACE158421D9546');
        $this->addSql('ALTER TABLE user_delivery_info DROP CONSTRAINT FK_F49B549DA1ACE158');
        $this->addSql('DROP INDEX IDX_F49B549DA1ACE158');
        $this->addSql('ALTER TABLE user_delivery_info ADD zip VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user_delivery_info DROP zip_code');
    }
}
