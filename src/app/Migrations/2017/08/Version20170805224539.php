<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170805224539 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE payment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE payment (id INT NOT NULL, exchange_id INT DEFAULT NULL, external_transaction_id VARCHAR(255) NOT NULL, currency VARCHAR(255) NOT NULL, amount NUMERIC(10, 7) NOT NULL, is_done BOOLEAN NOT NULL, is_success BOOLEAN NOT NULL, err_message TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6D28840D68AFD1A0 ON payment (exchange_id)');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840D68AFD1A0 FOREIGN KEY (exchange_id) REFERENCES exchange (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_tbl ADD payment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_tbl ADD CONSTRAINT FK_FC2D72A84C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FC2D72A84C3A3BB ON order_tbl (payment_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE order_tbl DROP CONSTRAINT FK_FC2D72A84C3A3BB');
        $this->addSql('DROP SEQUENCE payment_id_seq CASCADE');
        $this->addSql('DROP TABLE payment');
        $this->addSql('DROP INDEX UNIQ_FC2D72A84C3A3BB');
        $this->addSql('ALTER TABLE order_tbl DROP payment_id');
    }
}
