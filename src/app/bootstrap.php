<?php

define('DS', DIRECTORY_SEPARATOR);

$envFile = __DIR__ . '/config/.env';

if (file_exists($envFile)) {
    $dotenv = new \Symfony\Component\Dotenv\Dotenv();
    $dotenv->load($envFile);
}