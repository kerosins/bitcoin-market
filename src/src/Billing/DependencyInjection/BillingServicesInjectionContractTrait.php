<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Billing\DependencyInjection;

use Billing\Service\AccountManager;
use Billing\Service\TransactionManager;

trait BillingServicesInjectionContractTrait
{
    /**
     * @var AccountManager
     */
    private $accountManager;

    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * @param AccountManager $accountManager
     */
    public function setAccountManager(AccountManager $accountManager)
    {
        $this->accountManager = $accountManager;
    }

    /**
     * @param TransactionManager $transactionManager
     */
    public function setTransactionManager(TransactionManager $transactionManager)
    {
        $this->transactionManager = $transactionManager;
    }


}