<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Billing\DependencyInjection;

use Billing\Service\AccountManager;
use Billing\Service\TransactionManager;

interface BillingServicesInjectionContract
{
    /**
     * sets an account manager
     *
     * @param AccountManager $accountManager
     * @return mixed
     */
    public function setAccountManager(AccountManager $accountManager);

    /**
     * sets a transaction manager
     *
     * @param TransactionManager $transactionManager
     * @return mixed
     */
    public function setTransactionManager(TransactionManager $transactionManager);
}