<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Billing\DependencyInjection;


use Billing\Service\AccountManager;
use Billing\Service\TransactionManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class BillingServicesInjectionCompiler implements CompilerPassInterface
{
    public const
        TAG = 'billing_services_injection',
        INTERFACE_NAME = BillingServicesInjectionContract::class;

    public function process(ContainerBuilder $container)
    {
        if (!$container->has(AccountManager::class) && !$container->has(TransactionManager::class)) {
            return;
        }

        $accountManagerReference = new Reference(AccountManager::class);
        $transactionManagerDefinition = new Reference(TransactionManager::class);

        $taggedServices = $container->findTaggedServiceIds(self::TAG);
        foreach ($taggedServices as $service => $tag) {
            $definition = $container->getDefinition($service);

            $definition->addMethodCall('setAccountManager', [$accountManagerReference]);
            $definition->addMethodCall('setTransactionManager', [$transactionManagerDefinition]);
        }
    }
}