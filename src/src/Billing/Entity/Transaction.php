<?php
/**
 * @author Kondaurov
 */

namespace Billing\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;

/**
 * Class Transaction
 * @package Billing\Entity
 *
 * @ORM\Entity(repositoryClass="Billing\Repository\TransactionRepository")
 * @ORM\Table(name="znt_transaction")
 * @ORM\HasLifecycleCallbacks()
 */
class Transaction
{
    use BaseMapping;
    use UpdateTimestamps;

    public const
        STATUS_DONE = 0,
        STATUS_PENDING = 1,
        STATUS_PROCESS = 2,
        STATUS_CANCEL = 3,
        STATUS_HOLD = 4;

    public static $statusString = [
        self::STATUS_DONE => 'Done',
        self::STATUS_PENDING => 'Pending',
        self::STATUS_PROCESS => 'Process',
        self::STATUS_CANCEL => 'Cancel',
        self::STATUS_HOLD => 'Hold',
    ];
    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Billing\Entity\Account", cascade={"persist"})
     * @ORM\JoinColumn(name="from_account_id", referencedColumnName="id")
     */
    private $fromAccount;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Billing\Entity\Account", cascade={"persist"})
     * @ORM\JoinColumn(name="to_account_id", referencedColumnName="id")
     */
    private $toAccount;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=15, scale=5)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $hash;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $previousHash;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * @var Transaction
     *
     * @ORM\ManyToOne(targetEntity="Billing\Entity\Transaction", cascade={"persist"})
     * @ORM\JoinColumn(name="prev_transaction_id", referencedColumnName="id", nullable=true)
     */
    private $previousTransaction;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Billing\Doctrine\TransactionIdGenerator")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt = null)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return Account|null
     */
    public function getFromAccount()
    {
        return $this->fromAccount;
    }

    /**
     * @param Account $fromAccount
     *
     * @return Transaction
     */
    public function setFromAccount(Account $fromAccount): Transaction
    {
        $this->fromAccount = $fromAccount;
        return $this;
    }

    /**
     * @return Account
     */
    public function getToAccount(): ?Account
    {
        return $this->toAccount;
    }

    /**
     * @param Account $toAccount
     *
     * @return Transaction
     */
    public function setToAccount(Account $toAccount): Transaction
    {
        $this->toAccount = $toAccount;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     *
     * @return Transaction
     */
    public function setAmount(float $amount): Transaction
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     *
     * @return Transaction
     */
    public function setHash(string $hash): Transaction
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Transaction
     */
    public function setDescription(string $description): Transaction
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getPreviousHash(): ?string
    {
        return $this->previousHash;
    }

    /**
     * @param string $previousHash
     *
     * @return Transaction
     */
    public function setPreviousHash(string $previousHash): Transaction
    {
        $this->previousHash = $previousHash;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return Transaction
     */
    public function setStatus(int $status): Transaction
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Transaction
     */
    public function getPreviousTransaction(): ?Transaction
    {
        return $this->previousTransaction;
    }

    /**
     * @param Transaction $previousTransaction
     *
     * @return Transaction
     */
    public function setPreviousTransaction(Transaction $previousTransaction): Transaction
    {
        $this->previousTransaction = $previousTransaction;
        return $this;
    }

    public function getFormattedStatus()
    {
        return self::$statusString[$this->status];
    }

    /**
     * @return string
     */
    public function getVisibleCode(): string
    {
        $code = sprintf('ZT%s', substr($this->id, 0, 13));
        return strtoupper($code);
    }
}