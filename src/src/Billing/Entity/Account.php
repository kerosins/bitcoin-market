<?php
/**
 * @author Kondaurov
 */

namespace Billing\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;
use User\Entity\User;

/**
 * Class Account
 * @package Billing\Entity
 *
 * @ORM\Entity(repositoryClass="Billing\Repository\AccountRepository")
 * @ORM\Table(name="znt_account")
 * @ORM\HasLifecycleCallbacks()
 */
class Account
{
    use BaseMapping;
    use UpdateTimestamps;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isBot = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isMainSystem = false;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * @var User
     */
    private $user;

    /**
     * @var float
     */
    private $balance;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return bool
     */
    public function isBot(): ?bool
    {
        return $this->isBot;
    }

    /**
     * @param bool $isBot
     *
     * @return Account
     */
    public function setIsBot(bool $isBot): Account
    {
        $this->isBot = $isBot;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Account
     */
    public function setDescription(string $description): Account
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMainSystem(): ?bool
    {
        return $this->isMainSystem;
    }

    /**
     * @param bool $isMainSystem
     */
    public function setIsMainSystem(bool $isMainSystem)
    {
        $this->isMainSystem = $isMainSystem;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return float
     */
    public function getBalance(): ?float
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     * @return Account
     */
    public function setBalance(float $balance): Account
    {
        $this->balance = $balance;
        return $this;
    }


}