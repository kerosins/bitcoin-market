<?php
/**
 * @author Kondaurov
 */

namespace Billing\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

class TransactionIdGenerator extends AbstractIdGenerator
{
    /**
     * @inheritdoc
     *
     * @param EntityManager $em
     * @param \Doctrine\ORM\Mapping\Entity $entity
     *
     * @return mixed|string
     */
    public function generate(EntityManager $em, $entity)
    {
        return bin2hex(sha1(time(), uniqid()));
    }
}