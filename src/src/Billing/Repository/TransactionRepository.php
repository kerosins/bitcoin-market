<?php
/**
 * @author Kondaurov
 */

namespace Billing\Repository;


use Billing\Entity\Transaction;
use Kerosin\Doctrine\ORM\EntityRepository;

/**
 * Class TransactionRepository
 * @package Billing\Repository
 *
 * @method Transaction findOneByHash(string $hash)
 */
class TransactionRepository extends EntityRepository
{
    /**
     * @param int $id
     *
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getStatusOfTransactionById(string $id)
    {
        return $this->createQueryBuilder('t')
            ->select('t.status')
            ->andWhere('t.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return Transaction|null
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findFirstPendingTransaction()
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.status = :status')
            ->andWhere('t.hash IS NULL')
            ->andWhere('t.previousHash IS NULL')
            ->setParameter('status', Transaction::STATUS_PENDING)
            ->orderBy('t.createdAt', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $account
     *
     * @return float
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getTotalDeficitByAccount($account)
    {
        return $this->createQueryBuilder('t')
            ->select('SUM(t.amount)')
            ->andWhere('t.status IN (:status)')
            ->andWhere('t.fromAccount = :from')
            ->setParameters([
                'status' => [ Transaction::STATUS_DONE, Transaction::STATUS_HOLD ],
                'from' => $account
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param $account
     *
     * @return float
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getTotalProfitByAccount($account)
    {
        return $this->createQueryBuilder('t')
            ->select('SUM(t.amount)')
            ->andWhere('t.status IN (:status)')
            ->andWhere('t.toAccount = :from')
            ->setParameters([
                'status' => [ Transaction::STATUS_DONE, Transaction::STATUS_HOLD ],
                'from' => $account
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Find a last completed transaction
     *
     * @return Transaction
     */
    public function findLastDoneTransaction()
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.status = :status')
            ->setParameter('status', Transaction::STATUS_DONE)
            ->orderBy('t.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByVisibleCode(string $code)
    {

    }
}