<?php
/**
 * @author Kondaurov
 */

namespace Billing\Repository;


use Billing\Entity\Account;
use Kerosin\Doctrine\ORM\EntityRepository;

/**
 * Class AccountRepository
 * @package Billing\Repository
 *
 * @method Account|null find($id, $lockMode = null, $lockVersion = null)
 */
class AccountRepository extends EntityRepository
{

}