<?php
/**
 * @author Kondaurov
 */

namespace Billing\Service;

use Billing\Entity\Account;
use Billing\Entity\Transaction;
use Billing\Event\TransactionEvent;
use Billing\Exception\TransactionException;
use Billing\Repository\AccountRepository;
use Billing\Repository\TransactionRepository;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Payment\Entity\CurrencyRate;
use Payment\Entity\Payment;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class TransactionManager implements EntityManagerContract
{
    use EntityManagerContractTrait;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TransactionRepository
     */
    private $transactionRepo;

    /**
     * @var AccountRepository
     */
    private $accountRepo;

    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * add transaction to processor
     *
     * @param Account|int $from - account or its ID
     * @param Account|int $to - account or its ID
     * @param float $amount
     * @param string $description
     *
     * @return bool|Transaction - Transaction entity if transaction added, false otherwise
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(
        $from,
        $to,
        float $amount,
        string $description = ''
    ) {
        try {
            $transaction = $this->createTransaction($from, $to, $amount, $description);
        } catch (TransactionException $e) {
            $this->logger->error($e->getMessage(), [
                'trace' => $e->getTraceAsString()
            ]);
            return false;
        }

        $transaction->setStatus(Transaction::STATUS_PENDING);
        $this->flushTransaction($transaction);

        return $transaction;
    }

    /**
     * Add hold transaction
     *
     * @param Account|int $from
     * @param Account|int $to
     * @param float $amount
     * @param string $description
     *
     * @return Transaction|false
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addHold($from, $to, float $amount, string $description = '')
    {
        try {
            $transaction = $this->createTransaction($from, $to, $amount, $description);
        } catch (TransactionException $e) {
            $this->logger->error($e->getMessage(), [
                'trace' => $e->getTraceAsString()
            ]);
            return false;
        }
        $transaction->setStatus(Transaction::STATUS_HOLD);

        $this->flushTransaction($transaction);

        return $transaction;
    }

    /**
     * Release transaction
     *
     * @param string|Transaction $transaction
     *
     * @return bool
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function releaseTransaction($transaction)
    {
        if (!$transaction instanceof Transaction) {
            $transaction = $this->entityManager->find(Transaction::class, $transaction);
        }

        if ($transaction->getStatus() !== Transaction::STATUS_HOLD) {
            $this->logger->warning('Try release not hold transaction', [
                'transaction' => $transaction->getId()
            ]);
            return false;
        }

        $transaction->setStatus(Transaction::STATUS_PENDING);
        $this->flushTransaction($transaction);

        return true;
    }

    /**
     * Cancel a transaction
     *
     * @param string|Transaction $transaction
     *
     * @return bool
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function cancelTransaction($transaction)
    {
        if (!$transaction instanceof Transaction) {
            $transaction = $this->entityManager->find(Transaction::class, $transaction);
        }

        if (!in_array($transaction->getStatus(), [Transaction::STATUS_PENDING, Transaction::STATUS_HOLD])) {
            $this->logger->warning('Try cancel already processed transaction', [
                'transaction' => $transaction->getId()
            ]);
            return false;
        }

        $transaction->setStatus(Transaction::STATUS_CANCEL);
        $this->flushTransaction($transaction);

        return true;
    }

    /**
     * @param Account|int $from
     * @param Account|int $to
     * @param float $amount
     * @param string $description
     *
     * @return Transaction|bool
     */
    private function createTransaction(
        $from,
        $to,
        float $amount,
        string $description
    ) {
        $transaction = new Transaction();

        if (!$from instanceof Account) {
            /** @var Account $from */
            $from = $this->getAccountRepository()->find($from);
            if (!$from) {
                throw new TransactionException();
            }
        }
        $transaction->setFromAccount($from);

        if (!$to instanceof Account) {
            /** @var Account $to */
            $to = $this->getAccountRepository()->find($to);
            if (!$to) {
                throw new TransactionException();
            }
        }
        $transaction->setToAccount($to);

        if ($amount <= 0) {
            $this->logger->error('Invalid amount in transaction', [
                'from' => $from,
                'to' => $to
            ]);
            throw new TransactionException();
        }

        $transaction->setAmount($amount);
        $transaction->setDescription($description);

        return $transaction;
    }

    /**
     * Checks if transaction was verified
     *
     * @param string $id
     *
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isVerifiedById(string $id)
    {
        return Transaction::STATUS_DONE === $this->getTransactionRepository()->getStatusOfTransactionById($id);
    }

    /**
     * Generates hash for transaction
     *
     * @param Transaction $transaction
     * @param string $previousHash
     *
     * @return string
     */
    public function generateHash(Transaction $transaction, string $previousHash)
    {
        $hashString =
            ($transaction->getFromAccount() ? $transaction->getFromAccount()->getId() : null) .
            $transaction->getToAccount()->getId() .
            $transaction->getAmount() .
            $transaction->getId() .
            $previousHash;

        return hash('sha256', $hashString);
    }

    /**
     * @param Transaction $transaction
     *
     * @return bool
     */
    public function verifyTransaction(Transaction $transaction)
    {
        $previousByHash = $this->getTransactionRepository()->findOneByHash($transaction->getPreviousHash());

        if (!$previousByHash) {
            $this->logger->error(
                "Transaction verify process failed. Transaction with hash {$transaction->getPreviousHash()} not found",
                ['transaction_id' => $transaction->getId()]
            );
            throw new TransactionException();
        }

        return $previousByHash->getId() === $transaction->getPreviousTransaction()->getId();
    }

    /**
     * @param $id
     * @return null|Transaction
     */
    public function getTransaction($id)
    {
        return $this->getTransactionRepository()->find($id);
    }

    /**
     * It kostyl'
     */
    public function transmitTransactionToPayment(Transaction $transaction)
    {
        $payment = new Payment();
        $payment->setExternalTransactionId($transaction->getId());
        $payment->setCurrency(CurrencyRate::ZBC);
        $payment->setAmount($transaction->getAmount());

        return $payment;
    }

    /**
     * @return \Billing\Repository\TransactionRepository
     */
    private function getTransactionRepository()
    {
        if (!$this->transactionRepo) {
            $this->transactionRepo = $this->entityManager->getRepository(Transaction::class);
        }

        return $this->transactionRepo;
    }

    /**
     * @return \Billing\Repository\AccountRepository
     */
    private function getAccountRepository()
    {
        if (!$this->accountRepo) {
            $this->accountRepo = $this->entityManager->getRepository(Account::class);
        }

        return $this->accountRepo;
    }

    /**
     * Flush transaction to DB
     *
     * @param Transaction $transaction
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function flushTransaction(Transaction $transaction)
    {
        $this->entityManager->persist($transaction);
        $this->entityManager->flush($transaction);
    }
}