<?php
/**
 * @author Kondaurov
 */

namespace Billing\Service;

use Billing\Entity\Account;
use Billing\Entity\Transaction;
use Billing\Repository\AccountRepository;
use Billing\Repository\TransactionRepository;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Psr\Log\LoggerInterface;

class AccountManager implements EntityManagerContract
{
    use EntityManagerContractTrait;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var AccountRepository
     */
    private $accountRepository;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Create an new account
     *
     * @param bool $isBot
     * @param string $description
     */
    public function create(bool $isBot = false, string $description = '')
    {
        $account = new Account();
        $account->setIsBot($isBot);
        $account->setDescription($description);

        $this->entityManager->persist($account);
        $this->entityManager->flush($account);

        return $account;
    }

    /**
     * Gets a repository
     *
     * @return TransactionRepository
     */
    private function getTransactionRepository()
    {
        if (!$this->transactionRepository) {
            $this->transactionRepository = $this->entityManager->getRepository(Transaction::class);
        }

        return $this->transactionRepository;
    }

    /**
     * @param Account|int $account
     *
     * @return float
     */
    public function balance($account)
    {
        if (!$account) {
            return 0;
        }

        $deficit = $this->getTransactionRepository()->getTotalDeficitByAccount($account) ?? 0;
        $profit = $this->getTransactionRepository()->getTotalProfitByAccount($account) ?? 0;

        return bcsub($profit, $deficit, 5);
    }

    /**
     * @todo mb to repository?
     * @return Account
     */
    public function getSystemAccount()
    {
        return $this->entityManager->getRepository(Account::class)->createQueryBuilder('a')
            ->andWhere('a.isMainSystem = :isMain')
            ->setParameter('isMain', true)
            ->getQuery()
            ->getOneOrNullResult();
    }
}