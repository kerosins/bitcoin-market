<?php
/**
 * @author Kondaurov
 */

namespace Billing\Service;

use Billing\Entity\Transaction;
use Billing\Repository\TransactionRepository;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\Doctrine\ORM\Persister;
use Psr\Log\LoggerInterface;

class TransactionProcessor implements EntityManagerContract
{
    use EntityManagerContractTrait;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var AccountManager
     */
    private $accountManager;

    /**
     * @var Persister
     */
    private $persister;

    /**
     * @var TransactionManager
     */
    private $transactionManager;

    public function __construct(
        AccountManager $accountManager,
        TransactionManager $transactionManager,
        LoggerInterface $logger,
        Persister $persister
    ) {
        $this->accountManager = $accountManager;
        $this->logger = $logger;
        $this->persister = $persister;
        $this->transactionManager = $transactionManager;
    }

    /**
     * Runs a processor
     */
    public function run()
    {
        while (true) {
            $transaction = $this->getTransactionRepository()->findFirstPendingTransaction();

            if (!$transaction) {
                sleep(15);
                continue;
            }

            //verify transaction
            $senderBalance = $this->accountManager->balance($transaction->getFromAccount());
            if ($senderBalance < $transaction->getAmount()) {
                $this->logger->error('Balance for transaction doesn\'t enough', [
                    'transaction' => [
                        'id' => $transaction->getId(),
                        'from_account' => $transaction->getFromAccount()->getId(),
                        'balance_account' => $senderBalance,
                        'transaction_amount' => $transaction->getAmount()
                    ]
                ]);

                $transaction->setStatus(Transaction::STATUS_CANCEL);

                $this->entityManager->persist($transaction);
                $this->entityManager->flush();
                $this->entityManager->clear();

                continue;
            }

            $lastDoneTransaction = $this->getTransactionRepository()->findLastDoneTransaction();
            $lastHash = $lastDoneTransaction->getHash();
            $hash = $this->transactionManager->generateHash($transaction, $lastHash);

            $transaction->setHash($hash);
            $transaction->setPreviousHash($lastHash);
            $transaction->setPreviousTransaction($lastDoneTransaction);
            $transaction->setStatus(Transaction::STATUS_DONE);

            $this->entityManager->persist($transaction);
            $this->entityManager->flush();

            $this->entityManager->clear();
        }
    }

    /**
     * Gets transaction repository
     *
     * @return TransactionRepository
     */
    private function getTransactionRepository()
    {
        if (!$this->transactionRepository) {
            $this->transactionRepository = $this->entityManager->getRepository(Transaction::class);
        }

        return $this->transactionRepository;
    }
}