<?php
/**
 * @author Kondaurov
 */

namespace Billing\Exception;

use Billing\Entity\Transaction;
use Throwable;

class TransactionException extends \RuntimeException
{
}