<?php
/**
 * @author Kondaurov
 */

namespace Billing;


use Billing\DependencyInjection\BillingServicesInjectionCompiler;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class BillingBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new BillingServicesInjectionCompiler());
        $container
            ->registerForAutoconfiguration(BillingServicesInjectionCompiler::INTERFACE_NAME)
            ->addTag(BillingServicesInjectionCompiler::TAG);
    }
}