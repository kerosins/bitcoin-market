<?php
/**
 * @author Kondaurov
 */

namespace Billing\Command;


use Billing\Service\TransactionProcessor;
use Kerosin\Component\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProcessorCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('billing:processor')
            ->setDescription('Run a processor for handle transactions');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get(TransactionProcessor::class)->run();
    }
}