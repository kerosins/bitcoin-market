<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Billing\Command;

use Billing\Service\AccountManager;
use Kerosin\Component\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use User\Entity\User;

class CreateAccountCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('billing:account:create')
            ->addArgument('user', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $accountManager = $this->getContainer()->get(AccountManager::class);

        $user = $this->getEntityManager()->find(User::class, $input->getArgument('user'));
        if (!$user) {
            $output->writeln('User with id #' . $input->getArgument('user') . ' not found');
            return;
        }

        if ($user->getBillingAccount()) {
            $output->writeln('User already have account');
        }

        $account = $accountManager->create(false, 'Account for user with id #' . $user->getId());

        $user->setBillingAccount($account);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();

        $output->writeln('Created');
    }
}