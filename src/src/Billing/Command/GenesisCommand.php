<?php
/**
 * @author Kondaurov
 */

namespace Billing\Command;


use Billing\Entity\Account;
use Billing\Entity\Transaction;
use Billing\Service\AccountManager;
use Billing\Service\TransactionManager;
use Kerosin\Component\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenesisCommand
 * @package Billing\Command
 *
 * todo it must be delete in production
 */
class GenesisCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('billing:genesis')
            ->setDescription('Initialize billing and add genesis transaction');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Initialize billing');

        //drop transactions and accounts first
        $entityManager = $this->getEntityManager();
        $entityManager->getConnection()->executeQuery('DELETE FROM znt_transaction t');
        $entityManager->getConnection()->executeQuery('DELETE FROM znt_account a');

        $output->writeln('Create a system account');

        $systemAccount = $this->getContainer()
            ->get(AccountManager::class)
            ->create(false, "It's a system account");



        $systemAccount = $entityManager->find(Account::class, $systemAccount->getId());
        $systemAccount->setIsMainSystem(true);
        $entityManager->persist($systemAccount);

        $output->writeln('Add a genesis transaction');
        $genesis = new Transaction();
        $genesis
            ->setToAccount($systemAccount)
            ->setAmount(1000000000)
            ->setDescription('Genesis transaction')
            ->setStatus(Transaction::STATUS_DONE)
        ;

        $entityManager->persist($genesis);
        $entityManager->flush();

        $previousHash = '0';
        $hash = $this->getContainer()->get(TransactionManager::class)->generateHash($genesis, $previousHash);
        $genesis
            ->setHash($hash)
            ->setPreviousHash($hash)
            ->setPreviousTransaction($genesis)
        ;

        $entityManager->persist($genesis);
        $entityManager->flush();
    }
}