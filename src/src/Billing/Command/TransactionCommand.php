<?php
/**
 * @author Kondaurov
 */

namespace Billing\Command;

use Billing\Event\TransactionEvent;
use Billing\Service\TransactionManager;
use Kerosin\Component\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use User\Entity\BalanceChangeLog;
use User\Entity\User;
use User\Event\BalanceEvent;
use User\Repository\UserRepository;

class TransactionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('billing:transaction')
            ->addArgument('from', InputArgument::REQUIRED, 'From account')
            ->addArgument('to', InputArgument::REQUIRED, 'To account')
            ->addArgument('amount', InputArgument::REQUIRED, 'Amount')
            ->setDescription('Create a transaction manually');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $transactionManager = $this->getContainer()->get(TransactionManager::class);
        $result = $transactionManager->add(
            $input->getArgument('from'),
            $input->getArgument('to'),
            $input->getArgument('amount'),
            'Manually added tokens'
        );

        $userRepository = $this->getContainer()->get(UserRepository::class);
        /** @var User $user */
        $user = $userRepository->find($result->getToAccount());

        if (!$user) {
            return;
        }

        $this->getContainer()->get('event_dispatcher')->dispatch(
            BalanceEvent::NAME,
            new BalanceEvent(
                $result,
                $user,
                BalanceChangeLog::TYPE_DEPOSIT,
                BalanceChangeLog::STATUS_DEPOSIT,
                'Manually added tokens'
            )
        );

        if (!$result) {
            $output->writeln('Transaction was not added');
            return;
        }

        $output->writeln('Transaction was added');
    }
}