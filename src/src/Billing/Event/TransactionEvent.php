<?php
/**
 * @author Kondaurov
 */

namespace Billing\Event;


use Billing\Entity\Transaction;
use Symfony\Component\EventDispatcher\Event;

/**
 * @deprecated
 *
 * Class TransactionEvent
 * @package Billing\Event
 */
class TransactionEvent extends Event
{
    public const NAME = 'billing.transaction';

    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * @var int
     */
    private $status;

    /**
     * TransactionEvent constructor.
     *
     * @param Transaction $transaction
     * @param int $status - status from BalanceChangeLog
     */
    public function __construct(
        Transaction $transaction,
        int $status
    ) {
        $this->transaction = $transaction;
        $this->status = $status;
    }

    /**
     * @return Transaction
     */
    public function getTransaction(): Transaction
    {
        return $this->transaction;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
}