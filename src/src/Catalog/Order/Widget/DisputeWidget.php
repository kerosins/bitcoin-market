<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Order\Widget;

use Kerosin\Component\Widget;
use Catalog\ORM\Entity\Dispute;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderPosition;
use Catalog\Order\Service\DisputeManager;

class DisputeWidget extends Widget
{
    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * @inheritdoc
     *
     * @return array|\Twig_Filter[]
     */
    public function getFilters()
    {
        return [
            new \Twig_Filter('canOpenDispute', [$this, 'canOpenDisputeFilter']),
            new \Twig_Filter('hasDisputeForPosition', [$this, 'hasDisputeForPositionFilter']),
            new \Twig_Filter('hasDisputesInOrder', [$this, 'hasDisputesInOrderFilter']),
            new \Twig_Filter('canCloseByUser', [$this, 'canCloseByUserFilter'])
        ];
    }

    public function canOpenDisputeFilter(OrderPosition $orderProduct)
    {
        return $this->container->get(DisputeManager::class)->canOpen($orderProduct);
    }

    public function hasDisputeForPositionFilter(OrderPosition $orderProduct)
    {
        return $orderProduct->getDisputes()->count() > 0;
    }

    public function hasDisputesInOrderFilter(Order $order)
    {
        return $this->getDisputeManager()->countDisputesOfOrder($order) > 0;
    }

    public function canCloseByUserFilter(Dispute $dispute)
    {
        return $dispute->getStatus() === Dispute::STATUS_OPEN;
    }

    private function getDisputeManager()
    {
        if (!$this->disputeManager) {
            $this->disputeManager = $this->container->get(DisputeManager::class);
        }

        return $this->disputeManager;
    }
}