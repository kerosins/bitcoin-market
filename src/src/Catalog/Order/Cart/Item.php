<?php
/**
 * @author Kerosin
 */

namespace Catalog\Order\Cart;
use Catalog\ORM\Entity\OrderPosition;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Entity\ProductPrice;

/**
 * is not real entity
 *
 * Class CartItem
 * @package Shop\Entity
 */
class Item
{

    /**
     * @var Product
     */
    private $product;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var float
     */
    private $totalPrice;

    public function __construct(Product $product = null, $quantity = null)
    {
        $this->product = $product;
        $this->quantity = $quantity;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }

    public function getTotalPrice()
    {
        if (!$this->totalPrice) {
            $this->totalPrice = bcmul($this->product->getPublicPrice(), $this->quantity, ProductPrice::SCALE);
        }

        return $this->totalPrice;
    }

    public function toArray()
    {
        return [
            'product' => $this->product instanceof Product ? $this->product->getId() : null,
            'quantity' => $this->quantity
        ];
    }

    /**
     * Convert data to real doctrine entity
     *
     * @return OrderPosition
     */
    public function toOrderPositionEntity()
    {
        $entity = new OrderPosition();
        $entity->setProduct($this->getProduct());
        $entity->setQuantity($this->getQuantity());

        $productPrice = $this->getProduct()->getPrice();
        $entity->setInnerPrice($productPrice->getValue());
        $entity->setInnerCurrency('USD');
        $entity->setPublicPrice($this->getProduct()->getPublicPrice());

        return $entity;
    }
}