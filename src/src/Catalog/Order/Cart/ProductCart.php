<?php
/**
 * @author kerosin
 */

namespace Catalog\Order\Cart;


use Catalog\Order\Exception\DeliveryCompanyNotFoundException;
use Catalog\Order\Service\TotalAmountCalculator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Catalog\Order\Component\TotalPrice;
use Payment\Provider\CurrencyProvider;
use Predis\Client;
use Predis\Transaction\MultiExec;
use Catalog\Order\Cart\Item;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Entity\ProductPrice;
use Shop\Exception\UnknownCartHashException;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Class ProductCart
 * @package Shop\Service
 */
class ProductCart
{
    /**
     * @var ArrayCollection
     */
    private $items;

    /**
     * Redis connection
     *
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $cartHash;

    /**
     * @var string
     */
    private $cookieName;

    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * Flag which shows that cart is ready
     *
     * @var bool
     */
    private $ready = false;

    /**
     * @var CurrencyProvider
     */
    private $currencyProvider;

    /**
     * @var TotalAmountCalculator
     */
    private $taxCalculator;

    private $currencyScale;

    public function __construct(
        Client $client,
        ObjectManager $manager,
        RequestStack $requestStack,
        CurrencyProvider $currencyProvider,
        TotalAmountCalculator $taxCalculator,
        $currencyScale,
        $cookieName
    ) {
        $this->client = $client;
        $this->manager = $manager;
        $this->requestStack = $requestStack;
        $this->items = new ArrayCollection();
        $this->currencyProvider = $currencyProvider;
        $this->taxCalculator = $taxCalculator;

        $this->currencyScale = $currencyScale;
        $this->cookieName = $cookieName;
    }

    /**
     * Save cart to redis after destruction
     */
    public function __destruct()
    {
        if ($this->items->count()) {
            $this->flush();
        }
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->getRequestType() !== HttpKernelInterface::MASTER_REQUEST) {
            return;
        }

        $request = $event->getRequest();
        $hash = $request->cookies->get($this->cookieName);
        if (!$hash) {
            $hash = md5(uniqid(rand(), true));
        }

        $this->init($hash);
    }

    /**
     * Handler of Kernel event
     *
     * @param FilterResponseEvent $event
     * @return null|void
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (!$this->cartHash) {
            return null;
        }

        $response = $event->getResponse();
        $request = $event->getRequest();

        if ($request->cookies->has($this->cookieName)) {
            return;
        }

        $cookie = new Cookie($this->cookieName, $this->cartHash);
        $response->headers->setCookie($cookie);
    }

    /**
     * @param string $key - if key is null, use key from Cookie
     * @param bool $force
     */
    public function init($hash = null, $force = false)
    {
        if ($this->ready && !$force) {
            return;
        }

        if ($hash) {
            $this->setCartHash($hash);
        }

        if (!$this->getCartHash()) {
            throw new UnknownCartHashException();
        }

        $hash = $this->getCartHash();

        $items = $this->client->smembers($hash);
        $this->removeAll();

        $repo = $this->manager->getRepository(Product::class);
        foreach ($items as $item) {
            $item = $this->client->hgetall($item);
            $item['product'] = $repo->find($item['product']);
            $cartItem = new Item(
                $item['product'],
                $item['quantity']
            );

            $this->items->add($cartItem);
        }

        $this->ready = true;
    }

    /**
     * @return mixed
     */
    public function getCartHash()
    {
        return $this->cartHash;
    }

    /**
     * @param mixed $cartHash
     */
    public function setCartHash($cartHash)
    {
        $this->cartHash = $cartHash;
    }

    /**
     * Remove all items from cart
     */
    public function removeAll()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * Add item to cart, if item already exist, then increase quantity
     *
     * @param Product|int $product
     * @param $quantity
     */
    public function add($product, $quantity)
    {
        $id = $product instanceof Product ? $product->getId() : $product;
        if (($cartItem = $this->getByProductId($id)) === null) {
            $cartItem = new Item(
                $product,
                $quantity
            );

            return $this->items->add($cartItem);
        }

        if ($quantity == 1) {
            $quantity += $cartItem->getQuantity();
        }

        $cartItem->setQuantity($quantity);

        return true;
    }

    /**
     * Remove item from cart, if quantity is not null, then decrease
     *
     * @param Product|int $product
     * @param $quantity
     *
     * @return bool
     */
    public function remove($product, $quantity = null)
    {
        $id = $product instanceof Product ? $product->getId() : $product;
        $cartItem = $this->getByProductId($id);
        if ($cartItem) {
            if ($quantity && ($cartItem->getQuantity() - $quantity) > 0) {
                $cartItem->setQuantity($cartItem->getQuantity() - $quantity);
            } else {
                $this->items->removeElement($cartItem);
            }

            return true;
        }

        return false;
    }

    /**
     * Update quantity
     *
     * @param Product $product
     * @param $quantity
     */
    public function updateQuantity($product, $quantity = 0)
    {
        $id = $product instanceof Product ? $product->getId() : $product;
        $cartItem = $this->getByProductId($id);

        if (!$cartItem) {
            return false;
        }

        if (!$quantity) {
            $this->items->removeElement($cartItem);
        } else {
            $cartItem->setQuantity($quantity);
        }

        return true;
    }

    /**
     * find CartItem by product id
     *
     * @param $id
     *
     * @return Item|null
     */
    private function getByProductId($id)
    {
        $products = $this->items->filter(function (Item $item) use ($id) {
            return $item->getProduct()->getId() == $id;
        });

        if ($products->count() == 1) {
            return $products->first();
        }

        return null;
    }

    /**
     * @param $productId
     *
     * @return ArrayCollection|Item
     */
    public function get($productId = null)
    {
        if ($productId) {
            return $this->getByProductId($productId);
        }

        return $this->items;
    }

    /**
     * Save cart to redis
     */
    public function flush()
    {
        $hash = $this->getCartHash();
        if (!$hash) {
            return;
        }

        $oldKeys = $this->client->smembers($hash);

        $transaction = $this->client->transaction();
        try {
            $transaction->del([$hash]);
            $items = [];

            //remove old items
            if ($oldKeys) {
                $transaction->del($oldKeys);
            }

            foreach ($this->items as $item) {
                $itemKey = $this->flushItem($transaction, $item);
                array_push($items, $itemKey);
            }

            if (count($items) > 0) {
                $transaction->sadd($hash, $items);
            }
            $transaction->execute();
        } catch (\RuntimeException|\Exception $e) {
            $transaction->discard();
            var_dump($e);
        }
    }

    /**
     * @param MultiExec $transaction
     * @param Item $cartItem
     *
     * @return string
     */
    private function flushItem(MultiExec $transaction, Item $cartItem)
    {
        $hash = $this->getCartHash();
        $key = $hash . ':' . $cartItem->getProduct()->getId();

        foreach ($cartItem->toArray() as $name => $value) {
            $transaction->hset($key, $name, $value);
        }

        return $key;
    }

    /**
     * Get total price of products in cart
     *
     * @return float
     */
    public function getTotalPrice(): TotalPrice
    {
        $subTotal = 0;
        $deliveryFee = 0;

        foreach ($this->get() as $item) {
            /** @var Item $item */
            $totalPrice = $this->taxCalculator->calculatePrice($item->getProduct(), null, $item->getQuantity());
            $subTotal += $totalPrice->getSubTotal();
            $deliveryFee += $totalPrice->getDeliveryTax();
        }

        return (new TotalPrice())
            ->setDeliveryTax($deliveryFee)
            ->setSubTotal($subTotal)
            ->setTotal(bcadd($deliveryFee, $subTotal, $this->currencyScale));
    }
}