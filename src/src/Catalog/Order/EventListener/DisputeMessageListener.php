<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Order\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Catalog\ORM\Entity\Message\DisputeMessage;

/**
 * @todo implement for send mails and badges
 *
 * Class DisputeMessageListener
 * @package Order\EventListener
 */
class DisputeMessageListener
{
    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getObject();

        if (!$entity instanceof DisputeMessage) {
            return false;
        }
    }
}