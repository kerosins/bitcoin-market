<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Order\EventListener;


use Billing\DependencyInjection\BillingServicesInjectionContract;
use Billing\DependencyInjection\BillingServicesInjectionContractTrait;
use Catalog\Order\Service\OrderManager;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderStatus;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\DependencyInjection\Contract\EventDispatcherContract;
use Kerosin\DependencyInjection\Contract\EventDispatcherContractTrait;
use Payment\Component\PaymentEvent;
use Payment\Entity\CurrencyRate;
use Payment\Entity\Payment;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PaymentSubscriber implements
    EventSubscriberInterface,
    EntityManagerContract,
    EventDispatcherContract,
    BillingServicesInjectionContract
{
    use EntityManagerContractTrait, EventDispatcherContractTrait, BillingServicesInjectionContractTrait;

    /**
     * @var OrderManager
     */
    private $orderManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        OrderManager $orderManager,
        LoggerInterface $logger
    ) {
        $this->orderManager = $orderManager;
        $this->logger = $logger;
    }

    public static function getSubscribedEvents()
    {
        return [ PaymentEvent::EVENT_NAME_SUCCESS => 'onPaymentSuccess' ];
    }

    /**
     * Updates status of order to 'process' when all of payments are done
     *
     * @param PaymentEvent $event
     */
    public function onPaymentSuccess(PaymentEvent $event)
    {
        $payment = $event->getPayment();
        $order = $this->entityManager->getRepository(Order::class)->findOneByPayment($event->getPayment());
        if (!$order) {
            return;
        }

        $isZntPayment = $payment->getCurrency() === CurrencyRate::ZBC;
        $outerPayments = $order->getRealPayments();

        $this->entityManager->beginTransaction();
        try {
            $isDone = false;
            if ($isZntPayment) {
                if ($outerPayments->count() === 0) {
                    $isDone = true;
                } else {
                    $isDone = !$outerPayments->exists(function ($index, Payment $payment) {
                        return $payment->getStatus() !== Payment::PAYMENT_SUCCEEDED;
                    });
                }
            } else {
                if ($order->getPaymentsByZnt()->count() > 0) {
                    foreach ($order->getPaymentsByZnt() as $payment) {
                        /** @var Payment $payment */
                        $this->transactionManager->releaseTransaction($payment->getExternalTransactionId());
                        $payment->setStatus(Payment::PAYMENT_PROCESS);
                        $this->entityManager->persist($payment);
                        $this->entityManager->flush($payment);
                    }
                } else {
                    $isDone = !$outerPayments->exists(function ($index, Payment $payment) {
                        return $payment->getStatus() !== Payment::PAYMENT_SUCCEEDED;
                    });
                }
            }

            if ($isDone) {
                $this->orderManager->updateStatus($order, OrderStatus::PROCESS);
            }

            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [
                'trace' => $e->getTraceAsString()
            ]);
            $this->entityManager->rollback();
        }
    }
}