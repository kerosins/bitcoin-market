<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Order\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Catalog\Order\Event\OrderStatusEvent;
use Catalog\ORM\Entity\Order;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Listens 'postUpdate' event and broadcast 'order.status.change'
 *
 * Class OrderStatusListener
 * @package Order\EventListener
 */
class OrderStatusListener
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();

        if (!$entity instanceof Order) {
            return;
        }

        $changeSet = $eventArgs->getEntityManager()->getUnitOfWork()->getEntityChangeSet($entity);
        if (isset($changeSet['currentStatus'])) {
            [$old, $new] = $changeSet['currentStatus'];

            $event = (new OrderStatusEvent())
                ->setOrder($entity)
                ->setOldStatus($old->getType())
                ->setNewStatus($new->getType());

            $this->dispatcher->dispatch(OrderStatusEvent::EVENT_NAME, $event);
        }
    }
}