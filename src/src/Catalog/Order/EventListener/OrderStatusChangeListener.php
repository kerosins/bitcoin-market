<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Order\EventListener;


use Catalog\Order\Delivery\AliExpressCalculator;
use Catalog\Order\Service\TotalAmountCalculator;
use Catalog\ORM\Entity\OrderPosition;
use Doctrine\ORM\EntityManagerInterface;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\Mailer\CustomMailer;
use Catalog\Order\Event\OrderStatusEvent;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderStatus;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderStatusChangeListener implements EntityManagerContract, EventSubscriberInterface
{
    use EntityManagerContractTrait;
    /**
     * @var CustomMailer
     */
    private $mailer;

    /**
     * @var int End of life of order interval, in days
     */
    private $eolInterval;

    /**
     * @var AliExpressCalculator
     */
    private $calculator;

    public function __construct(
        EntityManagerInterface $entityManager,
        CustomMailer $mailer,
        AliExpressCalculator $calculator,
        int $eolInterval
    ) {
        $this->mailer = $mailer;
        $this->eolInterval = $eolInterval;
        $this->calculator = $calculator;
        $this->setEntityManager($entityManager);
    }

    public static function getSubscribedEvents()
    {
        return [
            OrderStatusEvent::EVENT_NAME => [
                ['onPaymentWait'],
                ['onOrderStatusChange']
            ]
        ];
    }

    public function onPaymentWait(OrderStatusEvent $event)
    {
        if (OrderStatus::PAYMENT_WAIT !== $event->getNewStatus()) {
            return;
        }

        $order = $event->getOrder();

        if (!$order->getIsSentMail()) {
            $this->mailer->orderReceived($order);
            $order->setIsSentMail(true);
            $this->entityManager->persist($order);
            $this->entityManager->flush($order);
        }

        $order->getOrderProducts()->map(function (OrderPosition $position) use ($order) {
            if (!$position->getProduct()->getSupplier()->getInternal()) {
                return $position;
            }

            $fee = $this->calculator->calculatePosition($position, $order->getDelivery()->getCountry());
            $position->setAliExpressDeliveryFee($fee);
            $this->entityManager->persist($position);
            return $position;
        });

        $this->entityManager->flush($order->getOrderProducts()->toArray());
    }


    /**
     * @param OrderStatusEvent $event
     */
    public function onOrderStatusChange(OrderStatusEvent $event)
    {
        $status = $event->getNewStatus();
        /** @var Order $order */
        $order = $event->getOrder();

        //sends emails
        switch ($status) {
            case OrderStatus::PROCESS:
                $paymentDate = new \DateTime();
                $order->setPaymentDate($paymentDate);
                $eolDate = clone $paymentDate;
                $eolDate->add(new \DateInterval("P{$this->eolInterval}D"));
                $order->setEolDate($eolDate);

                $this->entityManager->persist($order);
                $this->entityManager->flush($order);

                $this->mailer->orderProcess($order);
                $this->mailer->sendNotifyEmailToModeratorAboutOrder($order);
                break;
            case OrderStatus::SHIPPING:
                $this->mailer->orderShipped($order);
                break;
        }
    }
}