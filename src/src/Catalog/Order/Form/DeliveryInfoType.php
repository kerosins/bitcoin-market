<?php
/**
 * @author Kerosin
 */

namespace Catalog\Order\Form;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Kerosin\Component\GeoObject;
use Kerosin\Component\Validator\OnlyLetters;
use Kerosin\Form\Type\AddressObjectType;
use Kerosin\Form\Type\AutoCompleteType;
use Catalog\ORM\Entity\DeliveryInfo;
use Shop\Entity\AddressObject;
use Shop\Entity\Country;
use Shop\Entity\ZipCode;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class DeliveryInfoType extends AbstractType
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var array
     */
    private $options;

    /**
     * @var DeliveryInfo
     */
    protected $entity;

    public function __construct(ObjectManager $entityManager, RouterInterface $router)
    {
        $this->em = $entityManager;
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->options = $options;

        $builder
            ->add('firstName', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new OnlyLetters( )
                ]
            ])
            ->add('lastName', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new OnlyLetters(),
                ]
            ])
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'choice_label' => 'title',
                'query_builder' => function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('c')
                        ->orderBy('c.title','ASC');
                },
                'choice_attr' => function (Country $val) {
                    return [
                        'data-autocomplete' => (int)$val->getAllowAutocomplete()
                    ];
                },
                'attr' => [
                    'class' => 'delivery-info-country-field'
                ],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('address', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ]
            ])
            ->add('apartment', TextType::class, [
                'required' => false
            ])
            ->add('stateSimple', TextType::class, [
                'label' => 'State/Province',
                'required' => false,
                'attr' => [
                    'data-field-type' => 'simple',
                ]
            ])
            ->add('stateAutocomplete', AddressObjectType::class, [
                'autocomplete_url' => $this->router->generate($options['state_autocomplete_route']),
                'required' => false,
                'label' => 'State/Province',
                'attr' => [
                    'placeholder' => 'Select State',
                    'data-field-type' => 'autocomplete',
                    'class' => 'delivery-info-state-field',
                    'data-data-handler' => 'dataForStateSelect',
                    'data-type' => AddressObject::STATE_TYPE
                ]

            ])
            ->add('citySimple', TextType::class, [
                'label' => 'City',
                'required' => false,
                'attr' => [
                    'data-field-type' => 'simple'
                ]
            ])
            ->add('cityAutocomplete', AddressObjectType::class, [
                'autocomplete_url' => $this->router->generate($options['city_autocomplete_route']),
                'label' => 'City',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Select City',
                    'class' => 'delivery-info-city-field',
                    'data-data-handler' => 'dataForCitySelect',
                    'data-field-type' => 'autocomplete',
                    'data-type' => AddressObject::CITY_TYPE
                ]
            ])
            ->add('zipSimple', TextType::class, [
                'label' => 'Zip',
                'required' => false,
                'attr' => [
                    'data-field-type' => 'simple'
                ]
            ])
            ->add('zipAutocomplete', AutoCompleteType::class, [
                'entity_class' => ZipCode::class,
                'required' => false,
                'ajax_url' => $this->router->generate($options['zip_autocomplete_route']),
                'value_attr' => 'id',
                'text_attr' => 'code',
                'js_prepare_query' => 'dataForZipSelect',
                'attr' => [
                    'placeholder' => 'Select Zip',
                    'class' => 'delivery-info-zip-field',
                    'data-field-type' => 'autocomplete',
                ]
            ]);

        $builder->addModelTransformer(new CallbackTransformer(
            [$this, 'toForm'],
            [$this, 'toEntity']
        ));
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'class' => 'js-delivery-form'
            ],
            'data_class' => null,
            'state_autocomplete_route' => '',
            'city_autocomplete_route' => '',
            'zip_autocomplete_route' => '',
            'constraints' => new Callback([
                'callback' => [$this, 'validate']
            ])
        ]);

        $resolver->addAllowedTypes('state_autocomplete_route', 'string');
        $resolver->addAllowedTypes('city_autocomplete_route', 'string');
        $resolver->addAllowedTypes('zip_autocomplete_route', 'string');
    }

    /**
     * Transform data to form
     *
     * @param $value
     * @return array
     */
    public function toForm($value)
    {
        if (is_array($value) || !$value instanceof DeliveryInfo) {
            return $value;
        }

        $this->entity = $value;

        $autocomplete = $value->getCountry() && $value->getCountry()->getAllowAutocomplete();

        $stateSimple = $value->getState() ? $value->getState()->getTitle() : null;
        $citySimple = $value->getCity() ? $value->getCity()->getTitle() : null;
        $zipSimple = $value->getZip();

        $stateAutocomplete = null;
        $cityAutocomplete = null;
        $zipAutocomplete = null;

        if ($autocomplete) {
            $addressRepo = $this->em->getRepository('ShopBundle:AddressObject');

            if ($value->getState() && $value->getState()->getId()) {
                $stateAutocomplete = $addressRepo->find($value->getState()->getId());
            }

            if ($value->getCity() && $value->getCity()->getId()) {
                $cityAutocomplete = $addressRepo->find($value->getCity()->getId());
            }

            if ($autocomplete && $value->getZip()) {
                $zipAutocomplete = $this->em
                    ->getRepository('ShopBundle:ZipCode')
                    ->findOneByCodeAndCountry($value->getZip(), $value->getCountry());
            }
        }

        return [
            'id' => $value->getFirstName(),
            'firstName' => $value->getFirstName(),
            'lastName' => $value->getLastName(),
            'country' => $value->getCountry(),
            'address' => $value->getAddress(),
            'apartment' => $value->getApartment(),
            'stateSimple' => $stateSimple,
            'stateAutocomplete' => $stateAutocomplete,
            'citySimple' => $citySimple,
            'cityAutocomplete' => $cityAutocomplete,
            'zipSimple' => $zipSimple,
            'zipAutocomplete' => $zipAutocomplete
        ];
    }

    /**
     * Transform data from form to entity
     *
     * @param $value
     * @return mixed
     */
    public function toEntity($value)
    {
        $accessor = new PropertyAccessor();

        $country = $value['country'];
        $autocomplete = $country instanceof Country && $country->getAllowAutocomplete();

        $entity = $this->entity;
        $entity
            ->setCountry($accessor->getValue($value, '[country]'))
            ->setFirstName($accessor->getValue($value, '[firstName]'))
            ->setLastName($accessor->getValue($value, '[lastName]'))
            ->setAddress($accessor->getValue($value, '[address]'))
            ->setApartment($accessor->getValue($value, '[apartment]'))
        ;

        if ($autocomplete) {
            $state = $accessor->getValue($value, '[stateAutocomplete]');
            $city = $accessor->getValue($value, '[cityAutocomplete]');

            /** @var ZipCode $zip */
            $zip = $accessor->getValue($value, '[zipAutocomplete]');
            if ($zip) {
                $zip = $accessor->getValue($zip, 'code');
            }
        } else {
            $state = GeoObject::fromArray([
                'title' => $accessor->getValue($value, '[stateSimple]')
            ]);
            $city = GeoObject::fromArray([
                'title' => $accessor->getValue($value, '[citySimple]')
            ]);
            $zip = $accessor->getValue($value, '[zipSimple]');
        }

        $entity
            ->setState($state)
            ->setCity($city)
            ->setZip($zip);

        return $entity;
    }

    /**
     * Validate a form
     *
     * @param DeliveryInfo $data
     */
    public function validate($data, ExecutionContextInterface $context)
    {
        $country = $data->getCountry();
        $isAutocomplete = $country instanceof Country && $country->getAllowAutocomplete();

        $accessor = new PropertyAccessor();
        $noBlankAttributes = ['state', 'city', 'zip'];
        $breakValidation = false;

        foreach ($noBlankAttributes as $attribute) {
            $value = $accessor->getValue($data, $attribute);

            if (!$value || ($value instanceof GeoObject && !$value->getTitle())) {
                $breakValidation = true;
                $context
                    ->buildViolation('%attribute% is required')
                    ->setParameter('%attribute%', ucfirst($attribute))
                    ->atPath('[' . $attribute . ($isAutocomplete ? 'Autocomplete' : 'Simple') . ']')
                    ->addViolation();
            }
        }

        if ($isAutocomplete && !$breakValidation) {
            $this->validateAutocomplete($data, $context);
        }
    }

    /**
     * Validate a
     *
     * @param DeliveryInfo $data
     * @param ExecutionContextInterface $context
     */
    private function validateAutocomplete($data, ExecutionContextInterface $context)
    {
        $state = $this->getAddressObject($data->getState()->getId());
        $country = $data->getCountry();

        if (!$state ||
            $state->getType() !== AddressObject::STATE_TYPE ||
            $state->getCountry()->getId() !== $country->getId()
        ) {
            $this->createViolation($context, '[stateAutocomplete]', 'Invalid State|Region');
        }

        $city = $this->getAddressObject($data->getCity()->getId());
        /** @var ZipCod|string $zip */
        $zip = $data->getZip();
        if (
            !$city ||
            $city->getType() !== AddressObject::CITY_TYPE ||
            $city->getParent()->getParentId() !== $state->getId() ||
            ($zip instanceof ZipCode && $zip->getAddressObject()->getId() !== $city->getId())
        ) {
            $this->createViolation($context, '[cityAutocomplete]', 'Invalid City');
        }
    }

    /**
     * Get address object by id
     *
     * @param $id
     *
     * @return AddressObject|null
     */
    private function getAddressObject($id)
    {
        return $this->em->getRepository('ShopBundle:AddressObject')->find($id);
    }

    private function createViolation(ExecutionContextInterface $context, $attr, $message, array $params = [])
    {
        $context->buildViolation($message, $params)
            ->atPath($attr)
            ->addViolation();
    }
}