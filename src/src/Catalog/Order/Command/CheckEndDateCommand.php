<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Order\Command;


use Doctrine\ORM\Query;
use Kerosin\Component\ContainerAwareCommand;
use Kerosin\Mailer\CustomMailer;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderStatus;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckEndDateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('order:check_end_date')
            ->setDescription('Check an Interval until the End of Order, and send mail about it');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repo = $this->getEntityManager()->getRepository(Order::class);
        $eol = $this->getContainer()->getParameter('order_ttl_interval');
        $critical = (int)($eol / 10);
        /** @var CustomMailer $mailer */
        $mailer = $this->getContainer()->get(CustomMailer::class);
        $logger = $this->getContainer()->get('logger.public');

        //remind users about non-accepted orders
        $nonAcceptedOrders = $repo->findByCriticalDateAndStatus($eol, OrderStatus::SHIPPING);

        foreach ($nonAcceptedOrders as $order) {
            /** @var Order $order */

            $message = 'Sent mail about soon end of order';
            $output->writeln($message);
            $logger->info($message, [
                'id' => $order->getId()
            ]);

            $order->setIsSentRemindMail(true);

            $this->getEntityManager()->persist($order);
            $this->getEntityManager()->flush($order);

            $mailer->orderRemind($order);
        }

        //remind moderators about non processed orders
        $nonProcessedItems = $repo->findByCriticalDateAndStatus($eol, OrderStatus::PROCESS);
        foreach ($nonProcessedItems as $order) {
            $mailer->sendNotifyEmailToModeratorAboutOrder($order);
        }
    }
}