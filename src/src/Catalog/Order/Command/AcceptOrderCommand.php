<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Order\Command;

use Kerosin\Component\ContainerAwareCommand;
use Kerosin\DependencyInjection\Contract\LoggerContract;
use Kerosin\DependencyInjection\Contract\LoggerContractTrait;
use Kerosin\Mailer\CustomMailer;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderStatus;
use Catalog\Order\Service\OrderManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use User\Service\SystemUser;

class AcceptOrderCommand extends ContainerAwareCommand implements LoggerContract
{
    use LoggerContractTrait;

    protected function configure()
    {
        $this
            ->setName('order:accept_process')
            ->setDescription('Command accepts orders')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mailer = $this->getContainer()->get(CustomMailer::class);
        $orderManager = $this->getContainer()->get(OrderManager::class);
        $customMailer = $this->getContainer()->get(CustomMailer::class);

        $orderQuery = $this->getEntityManager()->getRepository(Order::class)->createQueryBuilder('o');
        $orderQuery
            ->innerJoin('o.currentStatus', 's')
            ->andWhere('s.type = :status')
            ->andWhere('o.eolDate IS NOT NULL and o.eolDate < CURRENT_DATE()')
            ->setParameters([
                'status' => OrderStatus::SHIPPING
            ])
        ;

        /** @var Order[] $orders */
        $orders = $orderQuery->getQuery()->getResult();
        $this->logger->info('Found ' . count($orders) . ' orders for processing');

        $systemUser = $this->getContainer()->get(SystemUser::class)->getSystemUser();

        foreach ($orders as $order) {
            $success = $orderManager->accept($order, $systemUser);
            if ($success) {
                $this->logger->info('Updated order', [
                    'id' => $order->getId()
                ]);
                $mailer->orderExpirationEnd($order);
            } else {
                $this->logger->warning('Failed update order', [
                    'id' => $order->getId()
                ]);
            }
        }

        $this->logger->info('Complete');
    }
}