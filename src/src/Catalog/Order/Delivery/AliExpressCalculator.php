<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Order\Delivery;

use Catalog\Import\AliExpress\Delivery\DeliveryRequestPerformer;
use Catalog\ORM\Entity\AliExpressDeliveryFee;
use Catalog\ORM\Entity\OrderPosition;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Repository\AliExpressDeliveryFeeRepository;
use Psr\Log\LoggerInterface;
use Shop\Entity\Country;

class AliExpressCalculator
{
    /**
     * @var int
     */
    private $expire;

    /**
     * @var AliExpressDeliveryFeeRepository
     */
    private $repository;

    /**
     * @var int
     */
    private $fallbackCost = 30;

    /**
     * @var int
     */
    private $fallbackCommitDay = 21;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DeliveryRequestPerformer
     */
    private $deliveryRequestPerformer;

    /**
     * AliExpressCalculator constructor.
     * @param AliExpressDeliveryFeeRepository $repository
     * @param DeliveryRequestPerformer $deliveryRequestPerformer
     * @param LoggerInterface $logger
     * @param int $expire
     * @param int $fallbackCost
     * @param int $fallbackCommitDay
     */
    public function __construct(
        AliExpressDeliveryFeeRepository $repository,
        DeliveryRequestPerformer $deliveryRequestPerformer,
        LoggerInterface $logger,
        int $expire,
        int $fallbackCost,
        int $fallbackCommitDay
    ) {
        $this->repository = $repository;
        $this->logger = $logger;
        $this->expire = $expire;
        $this->fallbackCost = $fallbackCost;
        $this->fallbackCommitDay = $fallbackCommitDay;
        $this->deliveryRequestPerformer = $deliveryRequestPerformer;
    }

    /**
     * @param Product $product
     * @param Country $country
     * @param int $quantity
     *
     * @return AliExpressDeliveryFee
     */
    public function calculateProduct(Product $product, Country $country, int $quantity = 1)
    {
        $fee = $this->repository->findByProductAndCountry($product, $country, $quantity, $this->expire);
        if ($fee) {
            return $fee;
        }

        $fee = new AliExpressDeliveryFee();
        $fee->setProduct($product)->setCountry($country)->setQuantity($quantity);

        try {
            $data = $this->deliveryRequestPerformer->sendRequestForCalculate($product, $country, $quantity);
            $fee->setCost((float)$data['price'])
                ->setCommitDay((int)$data['commitDay'])
                ->setSelectedCompany($data['selected'])
                ->setFullResponse($data['response']);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [
                'exception' => [
                    'class' => get_class($e),
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString()
                ],
                'api_response' => $data ?? []
            ]);
            $fee->setCost($this->fallbackCost)->setCommitDay($this->fallbackCommitDay);
        }

        $this->repository->save($fee);
        return $fee;
    }

    /**
     * Calculate order position
     *
     * @param OrderPosition $position
     * @param Country $country
     * @return AliExpressDeliveryFee
     */
    public function calculatePosition(OrderPosition $position, Country $country)
    {
        return $this->calculateProduct($position->getProduct(), $country, $position->getQuantity());
    }
}