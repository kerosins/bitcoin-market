<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Order\Event;


use Catalog\ORM\Entity\Order;
use Symfony\Component\EventDispatcher\Event;

class OrderEvent extends Event
{
    public const
        ORDER_PLACED = 'catalog.order.placed',
        ORDER_CANCELED = 'catalog.order.canceled',
        ORDER_IN_PROCESS = 'catalog.order.in_process',
        ORDER_DONE = 'catalog.order.done',
        ORDER_CHANGE_STATUS = 'catalog.order.change_status';

    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return Order
     */
    public function getOrder(): ?Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }
}