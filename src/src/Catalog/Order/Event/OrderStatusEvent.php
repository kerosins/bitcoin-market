<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Order\Event;

use Catalog\ORM\Entity\Order;
use Symfony\Component\EventDispatcher\Event;

class OrderStatusEvent extends Event
{
    public const EVENT_NAME = 'order.status.change';

    /**
     * @var Order
     */
    private $order;

    /**
     * @var int
     */
    private $oldStatus;

    /**
     * @var int
     */
    private $newStatus;

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return OrderStatusEvent
     */
    public function setOrder(Order $order): OrderStatusEvent
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return int
     */
    public function getOldStatus(): ?int
    {
        return $this->oldStatus;
    }

    /**
     * @param int $oldStatus
     * @return OrderStatusEvent
     */
    public function setOldStatus(int $oldStatus): OrderStatusEvent
    {
        $this->oldStatus = $oldStatus;
        return $this;
    }

    /**
     * @return int
     */
    public function getNewStatus(): ?int
    {
        return $this->newStatus;
    }

    /**
     * @param int $newStatus
     * @return OrderStatusEvent
     */
    public function setNewStatus(int $newStatus): OrderStatusEvent
    {
        $this->newStatus = $newStatus;
        return $this;
    }
}