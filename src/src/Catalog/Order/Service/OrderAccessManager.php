<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Order\Service;


use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderStatus;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use User\Entity\User;

class OrderAccessManager implements EntityManagerContract
{
    use EntityManagerContractTrait;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Checks that order available for view
     *
     * @param Order $order
     *
     * @return bool
     */
    public function canView(Order $order)
    {
        if ($order->getCurrentStatus()->getType() === OrderStatus::CHECKOUT) {
            return false;
        }

        $currentUser = $this->tokenStorage->getToken();
        if ($currentUser) {
            $currentUser = $currentUser->getUser();
            $currentUser = $currentUser instanceof User ? $currentUser : null;
        }

        if ($order->getUser()->isEnabled() &&
            (!$currentUser || $currentUser->getId() !== $order->getUser()->getId())
        ) {
            return false;
        }

        return true;
    }

    public function canApproveBySign()
    {

    }
}