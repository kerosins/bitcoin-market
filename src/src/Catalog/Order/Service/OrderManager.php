<?php
/**
 * @author Kerosin
 */

namespace Catalog\Order\Service;

use Billing\DependencyInjection\BillingServicesInjectionContract;
use Billing\DependencyInjection\BillingServicesInjectionContractTrait;
use Catalog\Order\Cart\ProductCart;
use Catalog\Order\Event\OrderEvent;
use Doctrine\Common\Collections\ArrayCollection;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\DependencyInjection\Contract\EventDispatcherContract;
use Kerosin\DependencyInjection\Contract\EventDispatcherContractTrait;
use Kerosin\Mailer\CustomMailer;
use Catalog\ORM\Entity\BrowserData;
use Catalog\ORM\Entity\OrderDeliveryInfo;
use Catalog\ORM\Entity\OrderPosition;
use Catalog\Order\Exception\OrderManagerException;
use Payment\Component\PaymentEvent;
use Payment\Entity\CurrencyRate;
use Payment\Entity\Payment;
use Payment\Provider\CurrencyProvider;
use Payment\Service\PaymentManager;
use Psr\Log\LoggerInterface;
use Catalog\Order\Cart\Item;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderStatus;
use Catalog\ORM\Repository\OrderRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use User\Entity\User;
use User\Service\IpGeoLocation;

class OrderManager implements EntityManagerContract, BillingServicesInjectionContract, EventDispatcherContract
{
    use EntityManagerContractTrait,
        BillingServicesInjectionContractTrait,
        EventDispatcherContractTrait;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var IpGeoLocation
     */
    private $geoLocation;

    /**
     * @var int
     */
    private $eolOrderInterval;

    /**
     * @var CustomMailer
     */
    private $mailer;

    /**
     * @var CurrencyProvider
     */
    private $currencyProvider;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TotalAmountCalculator
     */
    private $taxCalculator;

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
        UserPasswordEncoderInterface $encoder,
        PaymentManager $paymentManager,
        IpGeoLocation $geoLocation,
        CurrencyProvider $currencyProvider,
        LoggerInterface $logger,
        TotalAmountCalculator $taxCalculator,
        SessionInterface $session,
        $eolOrderInterval
    ) {
        $this->encoder = $encoder;
        $this->paymentManager = $paymentManager;
        $this->geoLocation = $geoLocation;
        $this->currencyProvider = $currencyProvider;
        $this->logger = $logger;
        $this->taxCalculator = $taxCalculator;
        $this->session = $session;

        $this->eolOrderInterval = $eolOrderInterval;
    }

    /**
     * Prepare order for checkout
     *
     * @param ProductCart $cart
     * @param User $user
     *
     * @return Order
     */
    public function prepareOrderFromCart(ProductCart $cart, User $user = null)
    {
        $this->entityManager->beginTransaction();
        try {
            $orderHash = $this->session->get('preOrder');
            $order = null;
            if ($orderHash) {
                $order = $this->entityManager->getRepository(Order::class)->findOneByHash($orderHash);
            }

            if (!$order) {
                $order = new Order();

                $code = $this->generateCode();
                $order->setCode($code);
                $order->setHash(base64_encode($code));

                //set status
                $status = new OrderStatus(OrderStatus::CHECKOUT);
                $order->setCurrentStatus($status);
                $this->entityManager->persist($status);
            }

            $order->setUser($user);
            $this->attachProducts($order, $cart->get());

            $this->entityManager->persist($order);
            $this->entityManager->flush();

            $this->session->set('preOrder', $order->getHash());

            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }

        return $order;
    }

    /**
     * Removes order hash from session
     */
    public function releasePreparedOrder()
    {
        $this->session->remove('preOrder');
    }

    /**
     * @param Order $order
     * @param ArrayCollection|Item[] $cartProducts
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function attachProducts(Order $order, $cartProducts)
    {
        //calculate difference between order and cart
        $currentProducts = $order->getOrderProducts()->map(function (OrderPosition $item) {
            return md5($item->getProduct()->getId() . $item->getTotalPublicPrice());
        })->toArray();

        $newProducts = $cartProducts->map(function (Item $item) {
            return md5($item->getProduct()->getId() . $item->getTotalPrice());
        })->toArray();

        $diff1 = array_diff($newProducts, $currentProducts);
        $diff2 = array_diff($currentProducts, $newProducts);
        if (count($diff1) === 0 && count($diff2) === 0) {
            return;
        }

        foreach ($order->getOrderProducts() as $orderPosition) {
            $this->entityManager->remove($orderPosition);
        }

        /** @var Item $cartProduct */
        foreach ($cartProducts as $cartProduct) {
            $orderPosition = $cartProduct->toOrderPositionEntity();
            $order->addOrderProduct($orderPosition);
            $this->entityManager->persist($orderPosition);
        }
    }

    /**
     * Create an new order
     *
     * @param OrderDeliveryInfo $data
     * @param ProductCart $cart
     * @param User|null $user
     */
    public function create(OrderDeliveryInfo $info, ProductCart $cart, User $user = null)
    {
        $this->entityManager->beginTransaction();

        try {
            $order = $this->prepareOrderFromCart($cart, $user);
            $order->setDelivery($info);

            $order->getOrderProducts()->map(function (OrderPosition $orderPosition) use ($order) {
                $totalPrice = $this->taxCalculator->calculatePriceByPosition($orderPosition, $order->getDelivery()->getCountry());

                $orderPosition
                    ->setShipmentTax($totalPrice->getDeliveryTax())
                    ->setPublicPrice($totalPrice->getSubTotal());

                return $orderPosition;
            });

            $this->entityManager->persist($info);
            $this->entityManager->persist($order);
            $this->entityManager->flush();

            $this->eventDispatcher->dispatch(OrderEvent::ORDER_PLACED, new OrderEvent($order));

            $this->entityManager->commit();
        } catch (\Exception $exception) {
            $this->entityManager->rollback();
            throw $exception;
        }

        return $order;
    }

    /**
     * @param Order $order - Candidate for accept
     * @param User $user - User, who tries accept that order
     *
     * @return bool - true if order has been successfuly accepted, false otherwise
     */
    public function accept(Order $order, User $user)
    {
        if ($order->getCurrentStatus()->getType() !== OrderStatus::SHIPPING) {
            return false;
        }

        if ($user->hasRole(User::ROLE_SUPER_ADMIN) || $order->getUser()->getId() === $user->getId()) {
            $this->updateStatus($order, OrderStatus::DONE);
            $this->eventDispatcher->dispatch(OrderEvent::ORDER_DONE, new OrderEvent($order));
            return true;
        }

        return false;
    }

    /**
     * Cancel order
     *
     * @param Order $order
     * @throws OrderManagerException
     *
     * @return void
     */
    private function cancelOrder(Order $order)
    {
        $status = new OrderStatus();
        $status->setType(OrderStatus::CANCEL);

        $order->setCurrentStatus($status);

        $this->entityManager->persist($order);
        $this->entityManager->persist($status);

        $this->eventDispatcher->dispatch(OrderEvent::ORDER_CANCELED, new OrderEvent($order));

        $this->entityManager->flush();
    }

    /**
     * Cancel order by user
     *
     * @param Order $order
     * @throws OrderManagerException
     *
     * @return void
     */
    public function cancelByUser(Order $order)
    {
        //checks status
        $this->entityManager->beginTransaction();
        try {
            if ($order->getCurrentStatus()->getType() !== OrderStatus::PAYMENT_WAIT) {
                throw new OrderManagerException();
            }

            if (($payments = $order->getPayment()) !== null) {
                $payments->map(function (Payment $payment) {
                    if (!in_array($payment->getStatus(), [Payment::PAYMENT_CANCELED, Payment::PAYMENT_DECLINED])) {
                        $this->paymentManager->cancelPayment($payment);
                    }
                });

            }

            $order->setComment('Cancel by user');
            $this->cancelOrder($order);
            $this->mailer->orderCancel($order);

            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
        }

    }

    /**
     * Shortcut for update an order status
     *
     * @param Order $order
     * @param int $status
     */
    public function updateStatus(Order $order, $status)
    {
        $orderStatus = new OrderStatus();
        $orderStatus->setType($status);
        $order->setCurrentStatus($orderStatus);

        $this->entityManager->persist($order);
        $this->entityManager->persist($orderStatus);
        $this->entityManager->flush();
    }

    /**
     * generate an order unique code
     *
     * @return string
     */
    private function generateCode()
    {
        $code = strtoupper(uniqid('ZO', false));
        if (!$this->orderRepo) {
            $this->orderRepo = $this->entityManager->getRepository(Order::class);
        }

        if ($this->orderRepo->existByCode($code)) {
            return $this->generateCode();
        }

        return $code;
    }

    /**
     * Attach browser data to order
     *
     * @param Order $order
     * @param Request $request
     *
     * @return BrowserData
     */
    public function attachUserBrowserData(Order $order, Request $request)
    {
        if ($order->getBrowserData()) {
            return $order->getBrowserData();
        }

        $browserData = new BrowserData();
        $browserData->setOrder($order);

        $timezone = $request->cookies->get('timezoneOffset') ?? 0;
        $timezone = timezone_name_from_abbr("", $timezone * 60, false);

        $headers = [];
        foreach ($request->headers->getIterator() as $name => $value) {
            $headers[$name] = reset($value);
        }

        $location = $this->geoLocation->whereis($request->getClientIp());

        $browserData->setIp($request->getClientIp())
                    ->setUserAgent($headers['user-agent'])
                    ->setLanguage($headers['accept-language'])
                    ->setTimezone($timezone)
                    ->setHeaders($headers)
                    ->setLocation($location)
        ;

        $this->entityManager->persist($browserData);
        $this->entityManager->flush($browserData);

        return $browserData;
    }

    /**
     * Check is interval until end of order is critical (Less than T/10)
     *
     * @param \DateTime $endDate
     */
    public function isCriticalIntervalUntilEnd(\DateTime $endDate)
    {
        $currentDate = new \DateTime();
        if ($endDate <= $currentDate) {
            return true;
        }

        $interval = $endDate->diff($currentDate)->days;
        $orderLifeTime = $this->eolOrderInterval;
        $criticalLifeTime = (int)floor($orderLifeTime / 10);

        return $interval < $criticalLifeTime;
    }

    /**
     * Calculates public price with uses bonus (ZNT)
     *
     * @param Order $order
     * @param string $currency
     * @param float $bonus
     */
    public function calculateTotalPublicPriceWithBonus(Order $order, string $currency, float $bonus = 0)
    {
        $totalInnerInZnt = $this->currencyProvider->convertToBestRate(
            $order->getTotalPublicPrice(),
            CurrencyRate::ZBC);
        $totalInnerInZnt = bcsub($totalInnerInZnt, $bonus, CurrencyProvider::EXTERNAL_CURRENCY_PRECISION);
        $reverse = $this->currencyProvider->convertToUsd($totalInnerInZnt, CurrencyRate::ZBC);

        return $this->currencyProvider->convertToBestRate($reverse, $currency);
    }

    /**
     * @param Order $order
     * @param string $currency
     * @param float $bonus
     * @return null|Payment
     */
    public function createPaymentForOrder(Order $order, string $currency, float $bonus = 0): ?Payment
    {
        $this->entityManager->beginTransaction();

        try {
            $totalAmount = $this->taxCalculator->calculatePriceByOrder($order);
            if ($bonus) {
                //find account
                $account = $order->getUser()->getBillingAccount();

                if ($account && $this->accountManager->balance($account) >= $bonus) {
                    $system = $this->accountManager->getSystemAccount();
                    //create hold transaction and transmit it to payment
                    $id = $this->transactionManager->addHold($account, $system, $bonus);
                    $payment = $this->paymentManager->createPaymentFromBonusTransaction(
                        $this->transactionManager->getTransaction($id)
                    );
                    $order->addPayment($payment);
                    $this->entityManager->persist($payment);

                    //recalculate total
                    $bonusInUsd = $this->currencyProvider->convertToUsd($bonus, CurrencyRate::ZBC);
                    $totalAmount->sub($bonusInUsd);

                    if ($totalAmount->getTotal() === 0) {
                        //paid by ZNT only
                        $this->transactionManager->releaseTransaction($id);
                        $payment->setStatus(Payment::PAYMENT_PROCESS);
                        $this->updateStatus($order, OrderStatus::PAYMENT_PROCESS);

                        $this->entityManager->persist($order);
                        $this->entityManager->flush();
                        $this->entityManager->commit();
                        return $payment;
                    }
                }
            }

            $payment = $this->paymentManager->createPayment(
                $currency,
                $totalAmount->getTotal(),
                $order->getDelivery()->getEmail()
            );
            $order->addPayment($payment);

            $this->updateStatus($order, OrderStatus::PAYMENT_WAIT);

            $this->entityManager->persist($order);
            $this->entityManager->flush($order);
            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->logger->error('Fail attach payment to order', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            $this->entityManager->rollback();
            throw $e;
        }
        return $payment;
    }

    /**
     * Checks if allow chat for specified the Order
     *
     * @param Order $order
     * @return bool
     */
    public function isAllowChat(Order $order)
    {
        return !in_array($order->getCurrentStatus()->getType(), [
            OrderStatus::DONE,
            OrderStatus::CANCEL
        ]);
    }
}