<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Order\Service;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;
use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Catalog\ORM\Entity\Dispute;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderStatus;
use Symfony\Component\HttpFoundation\RequestStack;
use User\Entity\User;

class OrderProvider extends BaseProvider
{
    public static $visibleStatuses = [
        OrderStatus::PAYMENT_PROCESS,
        OrderStatus::PAYMENT_WAIT,
        OrderStatus::PAYMENT_SUCCESS,
        OrderStatus::PROCESS,
        OrderStatus::SHIPPING,
        OrderStatus::DONE,
    ];

    /**
     * @var string
     */
    private $code;

    /**
     * @var User|int
     */
    private $client;

    /**
     * @var Collection
     */
    private $status = [];

    private $hasDispute = false;

    /**
     * sort's Column
     *
     * @var string
     */
    private $sortColumn = 'createdAt';

    /**
     * @var string
     */
    private $direction = 'DESC';

    public function __construct(ObjectManager $entityManager, PaginatorInterface $paginator, RequestStack $requestStack)
    {
        $this->status = new ArrayCollection($this->status);
        parent::__construct($entityManager, $paginator, $requestStack);
    }

    /**
     * @inheritdoc
     */
    public function search(): PaginationInterface
    {
        $builder = $this->em->getRepository(Order::class)->createQueryBuilder('o');

        $builder
            ->andWhere('o.currentStatus is not null')
            ->orderBy('o.' . $this->sortColumn, $this->direction);

        if ($this->client) {
            $builder
                ->andWhere('o.user = :client')
                ->setParameter('client', $this->client)
            ;
        }

        if ($this->code) {
            $builder->andWhere('lower(o.code) LIKE :code')
                ->setParameter('code', '%' . strtolower($this->code) . '%');
        }

        if ($this->status->count()) {
            $builder
                ->innerJoin('o.currentStatus', 's')
                ->andWhere('s.type IN (:status)')
                ->setParameter('status', $this->status)
            ;
        }

        if ($this->hasDispute) {
            $builder
                ->innerJoin('o.orderProducts', 'p')
                ->innerJoin('p.disputes', 'd')
                ->where('d.status = :disputeStatus')
                ->setParameter('disputeStatus', Dispute::STATUS_OPEN)
            ;
        }

        return $this->buildPaginationResult($builder->getQuery());
    }

    /**
     * @return int|User
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param int|User $client
     * @return OrderProvider
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return OrderProvider
     */
    public function setCode(string $code = null): OrderProvider
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return array
     */
    public function getStatus()
    {
        return $this->status->toArray();
    }

    /**
     * @param array $status
     * @return $this
     */
    public function setStatus(array $status)
    {
        $this->status = new ArrayCollection($status);
        return $this;
    }

    /**
     * @param int $status
     */
    public function addStatus($status)
    {
        if (is_array($status)) {
            foreach ($status as $item) {
                $this->addStatus($item);
            }
        } else {
            $this->status->add($status);
        }

        return $this;
    }

    /**
     * @param $status
     *
     * @return static
     */
    public function removeStatus($status)
    {
        $this->status->removeElement($status);
        return $this;
    }

    /**
     * Drop all statuses from filter
     *
     * @return $this
     */
    public function clearStatuses()
    {
        $this->status->clear();
        return $this;
    }

    public function setSortColumn(string $column)
    {
        if (in_array($column, ['createdAt', 'paymentDate'])) {
            $column = 'createdAt';
        }

        $this->sortColumn = $column;
        return $this;
    }

    public function setDirection(string $direction)
    {
        if (in_array($direction, ['desc', 'asc'])) {
            $direction = 'desc';
        }
        $this->direction = $direction;
    }

    /**
     * alias for getHasDispute
     *
     * @return bool|null
     */
    public function hasDispute()
    {
        return $this->getHasDispute();
    }

    /**
     * @return bool
     */
    public function getHasDispute(): ?bool
    {
        return $this->hasDispute;
    }

    /**
     * @param bool $hasDispute
     * @return OrderProvider
     */
    public function setHasDispute(bool $hasDispute): OrderProvider
    {
        $this->hasDispute = $hasDispute;
        return $this;
    }
}