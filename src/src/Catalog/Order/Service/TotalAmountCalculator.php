<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Order\Service;

use Catalog\Order\Component\TotalPrice;
use Catalog\Order\Delivery\AliExpressCalculator;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderPosition;
use Catalog\ORM\Entity\Product;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Payment\Provider\CurrencyProvider;
use Shop\Entity\Country;
use User\Service\UserCountry;

/**
 * Class TotalAmountCalculator
 * @package Order\Service
 *
 * todo привести в порядок
 */
class TotalAmountCalculator implements EntityManagerContract
{
    use EntityManagerContractTrait;

    /**
     * @var float
     */
    private $tax;

    /**
     * @var CurrencyProvider
     */
    private $currencyProvider;

    /**
     * @var UserCountry
     */
    private $userCountry;
    /**
     * @var AliExpressCalculator
     */
    private $aliExpressCalculator;

    /**
     * @var int
     */
    private $fiatScale;

    public function __construct(
        CurrencyProvider $currencyProvider,
        UserCountry $userCountry,
        AliExpressCalculator $aliExpressCalculator,
        int $fiatScale
    ) {
        $this->currencyProvider = $currencyProvider;
        $this->userCountry = $userCountry;
        $this->aliExpressCalculator = $aliExpressCalculator;
        $this->fiatScale = $fiatScale;
    }

    /**
     * Calculate price with shipment tax
     *
     * @param float $price
     *
     * @return float
     */
    public function calculateTotalPriceWithTax(float $price)
    {
        return $price < $this->limitWithoutTax ? bcadd($price, $this->tax, 2) : $price;
    }

    /**
     * @param float $price
     *
     * @return float
     */
    public function calculateTaxByTotalPrice(float $price)
    {
        return bcsub(
            $this->calculateTotalPriceWithTax($price),
            $price,
            CurrencyProvider::INTERNAL_CURRENCY_PRECISION
        );
    }

    /**
     * Calculates price and tex
     *
     * @param \Catalog\ORM\Entity\Product $product
     * @param Country $country
     * @param int $quantity
     *
     * @return TotalPrice
     */
    public function calculatePrice(Product $product, Country $country = null, int $quantity = 1): TotalPrice
    {
        if (!$country) {
            $country = $this->userCountry->getDefaultCountry();
        }

        $totalPrice = new TotalPrice();
        $fee = $this->aliExpressCalculator->calculateProduct($product, $country, $quantity);

        $totalPrice
            ->setSubTotal(bcmul($product->getPublicPrice(), $quantity, $this->fiatScale))
            ->setDeliveryTax($fee->getCost())
            ->setTotal(bcadd($totalPrice->getSubTotal(), $totalPrice->getDeliveryTax(), $this->fiatScale))
        ;

        return $totalPrice;
    }

    /**
     * Calculates total price for position
     *
     * @param OrderPosition $position
     * @param Country $country
     * @return TotalPrice
     */
    public function calculatePriceByPosition(OrderPosition $position, Country $country)
    {
        if ($position->getShipmentTax()) {
            $subTotal = bcmul($position->getPublicPrice(), $position->getQuantity(), $this->fiatScale);
            $fee = $position->getShipmentTax();
            $total = bcadd($subTotal, $fee, $this->fiatScale);

            return (new TotalPrice($subTotal, $fee, $total));
        }

        return $this->calculatePrice($position->getProduct(), $country, $position->getQuantity());
    }

    /**
     * Calculates total price of all positions in order
     *
     * @param Order $order
     * @return TotalPrice
     */
    public function calculatePriceByOrder(Order $order)
    {
        $country = $order->getDelivery()->getCountry();
        $totals = array_map(function (OrderPosition $position) use ($country) {
            return $this->calculatePriceByPosition($position, $country);
        }, $order->getOrderProducts()->toArray());

        return $this->sum(...$totals);
    }

    /**
     * Sums total prices to one
     *
     * @param TotalPrice ...$totals
     */
    public function sum(TotalPrice ...$totals): TotalPrice
    {
        return new TotalPrice(
        //subtotal
            array_reduce($totals, function ($result, TotalPrice $price) {
                return bcadd($price->getSubTotal(), $result, $this->fiatScale);
            }, 0.0),
            //delivery tax
            array_reduce($totals, function ($result, TotalPrice $price) {
                return bcadd($price->getDeliveryTax(), $result, $this->fiatScale);
            }, 0.0),
            //total
            array_reduce($totals, function ($result, TotalPrice $price) {
                return bcadd($price->getTotal(), $result, $this->fiatScale);
            }, 0.0)
        );
    }

    /**
     * Subtracts second operand from first
     *
     * @param $first
     * @param $second
     */
    public function sub($first, $second)
    {
        //todo implements
    }
}