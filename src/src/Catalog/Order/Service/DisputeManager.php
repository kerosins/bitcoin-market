<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Order\Service;


use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\DependencyInjection\Contract\TaggedCacheContract;
use Kerosin\DependencyInjection\Contract\TaggedCacheContractTrait;
use Kerosin\Mailer\CustomMailer;
use Catalog\ORM\Entity\Dispute;
use Catalog\ORM\Entity\Message\DisputeMessage;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderPosition;
use Catalog\ORM\Entity\OrderStatus;
use Catalog\Order\Exception\DisputeException;
use User\Entity\User;

class DisputeManager implements EntityManagerContract, TaggedCacheContract
{
    use EntityManagerContractTrait, TaggedCacheContractTrait;

    /**
     * @var CustomMailer
     */
    private $customMailer;

    public function __construct(CustomMailer $customMailer)
    {
        $this->customMailer = $customMailer;
    }

    /**
     * Opens a new dispute
     *
     * @param OrderPosition $orderPosition
     *
     * @return Dispute
     */
    public function open(OrderPosition $orderPosition, User $user)
    {
        if ($user->getId() !== $orderPosition->getOrder()->getUser()->getId()) {
            throw new DisputeException('User who opens dispute must be same that created order');
        }

        if (!$this->canOpen($orderPosition)) {
            throw new DisputeException('Dispute can\'t open for that order');
        }

        $dispute = new Dispute();
        $orderPosition->addDispute($dispute);

        $this->taggedCache->invalidateTags(['order.' . $orderPosition->getOrder()->getId() . '.dispute.size']);

        $this->entityManager->persist($dispute);
        $this->entityManager->flush($dispute);

        $this->customMailer->disputeOpen($dispute);
        $this->customMailer->disputeOpenModerator($dispute);

        return $dispute;
    }

    /**
     * Checks statuses of order
     *
     * @param OrderPosition $orderPosition
     *
     * @return bool
     */
    public function canOpen(OrderPosition $orderPosition)
    {
        return $orderPosition->getOrder()->hasStatus([OrderStatus::PROCESS, OrderStatus::SHIPPING])
            && $orderPosition->getDisputes()->count() == 0;
    }

    /**
     * Checks if user can view this dispute
     *
     * @param Dispute $dispute
     * @param User $user
     *
     * @return bool
     */
    public function canView(Dispute $dispute, User $user)
    {
        return $dispute->getOrderPosition()->getOrder()->getUser()->getId() === $user->getId();
    }

    /**
     * Checks if chat allow fot specified user
     *
     * @param Dispute $dispute
     * @return bool
     */
    public function isAllowChat(Dispute $dispute)
    {
        return $dispute->getStatus() === Dispute::STATUS_OPEN || $dispute->getIsProcessingRefund();
    }

    /**
     * @param Order|int $order
     */
    public function countDisputesOfOrder($order)
    {
        $order = $order instanceof Order ? $order->getId() : $order;
        $cacheItem = $this->taggedCache->getItem('order.' . $order . '.dispute.size');
        if ($cacheItem->isHit()) {
            return $cacheItem->get();
        }

        $count = $this->entityManager->getRepository(Dispute::class)
            ->createQueryBuilder('d')
            ->select('COUNT(d.id)')
            ->innerJoin('d.orderPosition', 'p')
            ->andWhere('p.order = :order')
            ->setParameter('order', $order)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        $cacheItem->set($count);
        $cacheItem->tag('order.' . $order . '.dispute.size');
        $this->taggedCache->save($cacheItem);

        return $count;
    }

    /**
     * Cancels dispute by user
     *
     * @param Dispute $dispute
     * @param User $user
     *
     * @return bool
     */
    public function closeDisputeByUser(Dispute $dispute, User $user)
    {
        if ($dispute->getOrderPosition()->getOrder()->getUser()->getId() !== $user->getId()
            || $dispute->getStatus() !== Dispute::STATUS_OPEN
        ) {
            return false;
        }

        $dispute->setStatus(Dispute::STATUS_CLOSE_BY_CUSTOMER);
        $this->entityManager->persist($dispute);
        $this->entityManager->flush($dispute);

        $this->customMailer->disputeCancelByUser($dispute);
        $this->customMailer->disputeCancelByUserToModerators($dispute);

        return true;
    }

    /**
     * Set new status to dispute
     *
     * @param Dispute $dispute
     * @param int $status
     */
    public function setStatus(Dispute $dispute, int $status)
    {
        //todo implement
    }
}