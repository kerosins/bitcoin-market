<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Order\Component;


use Catalog\ORM\Entity\DeliveryInfo;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class DeliveryInfoTransmitter
{
    private static $ignoredProperties = ['id', 'created_at', 'updated_at'];

    public static function transmit(DeliveryInfo $from, DeliveryInfo $to)
    {
        $propertyAccessor = new PropertyAccessor();
        $reflect = new \ReflectionClass($from);

        $properties = $reflect->getProperties(\ReflectionProperty::IS_PROTECTED);
        foreach ($properties as $property) {
            if (in_array($property->getName(), self::$ignoredProperties)) {
                continue;
            }

            if (
                $propertyAccessor->isReadable($from, $property->getName()) &&
                $propertyAccessor->isWritable($from, $property->getName()) &&
                $propertyAccessor->isReadable($to, $property->getName()) &&
                $propertyAccessor->isWritable($to, $property->getName())
            ) {
                $value = $propertyAccessor->getValue($from, $property->getName());
                $propertyAccessor->setValue($to, $property->getName(), $value);
            }
        }
    }
}