<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Order\Component;

use Catalog\Order\Service\TotalAmountCalculator;
use Payment\Provider\CurrencyProvider;

class TotalPrice
{
    /**
     * @var float
     */
    private $subTotal;

    /**
     * @var float
     */
    private $deliveryTax;

    /**
     * @var float
     */
    private $total;

    public function __construct(float $subTotal = 0.0, float $deliveryTax = 0.0, float $total = 0.0)
    {
        $this->subTotal = $subTotal;
        $this->deliveryTax = $deliveryTax;
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getSubTotal(): ?float
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     *
     * @return TotalPrice
     */
    public function setSubTotal(float $subTotal): TotalPrice
    {
        $this->subTotal = $subTotal;
        return $this;
    }

    /**
     * @return float
     */
    public function getDeliveryTax(): ?float
    {
        return $this->deliveryTax;
    }

    /**
     * @param float $deliveryTax
     *
     * @return TotalPrice
     */
    public function setDeliveryTax(float $deliveryTax): TotalPrice
    {
        $this->deliveryTax = $deliveryTax;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotal(): ?float
    {
        return $this->total;
    }

    /**
     * @param float $total
     *
     * @return TotalPrice
     */
    public function setTotal(float $total): TotalPrice
    {
        $this->total = $total;
        return $this;
    }

    /**
     * Creates object from array
     *
     * @param $values
     *
     * @return TotalPrice
     */
    public static function fromArray($values)
    {
        if (!in_array(array_keys($values), ['sub_total', 'delivery_tax', 'total'])) {
            throw new \RuntimeException('One or more of required keys are undefined');
        }

        return (new self())
            ->setSubTotal($values['sub_total'])
            ->setDeliveryTax($values['delivery_tax'])
            ->setTotal($values['total']);
    }

    /**
     * Converts to array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'sub_total' => $this->getSubTotal(),
            'delivery_tax' => $this->getDeliveryTax(),
            'total' => $this->getTotal()
        ];
    }

    /**
     * Subs an amount
     *
     * @param float $value
     */
    public function sub(float $value)
    {
        if ($value === 0.0) {
            return;
        }

        if ($value > $this->total) {
            throw new \RuntimeException('Value must be lesser than total amount');
        }

        if ($value === $this->total) {
            $this->total = $this->subTotal = $this->deliveryTax = 0;
            return;
        }

        $result = (float)bcsub($this->total, $value, CurrencyProvider::INTERNAL_CURRENCY_PRECISION);
        $this->total = $result;

        if ($value > $this->subTotal) {
            $remainder = (float)bcsub($value, $this->subTotal, CurrencyProvider::INTERNAL_CURRENCY_PRECISION);
            $this->deliveryTax = (float)bcsub($this->deliveryTax, $remainder, CurrencyProvider::INTERNAL_CURRENCY_PRECISION);
            $this->subTotal = 0;
        } else {
            $this->subTotal = (float)bcsub($this->subTotal, $value, CurrencyProvider::INTERNAL_CURRENCY_PRECISION);
        }
    }
}