<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Command;


use Catalog\ORM\Entity\Category;
use Kerosin\Component\ContainerAwareCommand;
use Shop\Provider\CategoryProvider;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class CategoryToggleCommand extends ContainerAwareCommand
{
    private $switchValues = ['enable' => true, 'disable' => false];

    protected function configure()
    {
        $this->setName('catalog:category:toggle')
            ->addArgument('switch', InputArgument::OPTIONAL, 'Switch category enable', 'enable')
            ->addOption('id', null, InputOption::VALUE_OPTIONAL, 'Id of category')
            ->addOption('title', 't', InputOption::VALUE_OPTIONAL, 'Like filter by title')
            ->setDescription('Switches categories enable|disable in catalog')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $isEnable = $input->getArgument('switch');
        if (!array_key_exists($isEnable, $this->switchValues)) {
            $output->writeln(
                sprintf(
                    "<error>Unknown value '%s' for argument 'switch', allowed: %s</error>",
                    $isEnable,
                    implode(', ', array_keys($this->switchValues))
                )
            );
            exit(1);
        }

        //finds categories
        $entityManager = $this->getEntityManager();
        $qb = $entityManager->getRepository(Category::class)->createQueryBuilder('c');

        $id = (int)$input->getOption('id');
        if ($id) {
            $qb->andWhere('c.id = :id')->setParameter('id', $id);
        }

        $title = (string)$input->getOption('title');
        if ($title) {
            $qb->andWhere('lower(c.title) like :title')->setParameter('title', '%' . strtolower($title) . '%');
        }

        $categories = $qb->getQuery()->getResult();
        $output->writeln(
            sprintf('Found %d categories:', count($categories))
        );

        foreach ($categories as $category) {
            $output->writeln(
                sprintf('<info>%s</info> <comment>%d</comment> (%s)',
                    $category->getTitle(),
                    $category->getId(),
                    array_search($category->getEnabled(), $this->switchValues)
                )
            );
        }

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion(sprintf('%s those categories?', ucfirst($isEnable)));

        if (!$helper->ask($input, $output, $question)) {
            $output->writeln('Bye');
            exit;
        }

        foreach ($categories as $category) {
            /** @var Category $category  */
            $category->setEnabled($this->switchValues[$isEnable]);
            $entityManager->persist($category);
        }

        $entityManager->flush();

        $this->getContainer()->get('app.cache')->invalidateTags([CategoryProvider::CACHE_TAG]);

        $output->writeln('<info>Changed!</info>');
        $output->writeln('<comment>Note: reindex catalog and categories for provide consistence in catalog</comment>');
    }
}