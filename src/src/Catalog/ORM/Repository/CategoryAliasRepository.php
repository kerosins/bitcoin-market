<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Repository;

use Kerosin\Doctrine\ORM\EntityRepository;
use Catalog\ORM\Entity\Category;

class CategoryAliasRepository extends EntityRepository
{
    /**
     * @param $title
     * @return Category
     */
    public function findCategoryByAlias($title)
    {
        $title = strtolower($title);

        $alias = $this->createQueryBuilder('ca')
            ->andWhere('lower(ca.aliases) LIKE :alias')
            ->setParameter('alias', "%|" . $title . "|%")
            ->getQuery()
            ->getResult();

        if ($alias) {
            return $alias->getCategory();
        }

        $category = $this->getEntityManager()->getRepository(\Catalog\ORM\Entity\Category::class)->createQueryBuilder('c')
            ->andWhere('lower(c.title) = :title')
            ->setParameter('title', $title)
            ->getQuery()
            ->getResult();

        return $category;
    }
}