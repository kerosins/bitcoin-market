<?php
/**
 * ProductRepository class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Catalog\ORM\Repository;

use Doctrine\ORM\NativeQuery;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Kerosin\Component\Image;
use Kerosin\Doctrine\ORM\EntityRepository;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\Product;

class ProductRepository extends EntityRepository
{
    /**
     * Find products with unloaded images
     *
     * @return NativeQuery
     */
    public function findByUnloadedImage()
    {
        $sql = "SELECT p.* FROM product p WHERE p.main_image ->> 'src' IS NULL";

        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata($this->getClassName(), 'p');

        return $this->getEntityManager()->createNativeQuery($sql, $rsm);
    }

    /**
     * @param $id
     * @param Image $mainImage
     */
    public function updateMainImageById($id, Image $mainImage)
    {
        $mainImage = json_encode($mainImage);

        $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->update(Product::class, 'p')
            ->set('p.mainImage', ':image')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->setParameter('image', $mainImage)
            ->getQuery()
            ->execute()
        ;

    }

    /**
     * @param Category|Category[]|int|int[] $category
     *
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function existByCategory($category)
    {
        if (!is_array($category)) {
            $category = [$category];
        }

        $entity = $this->createQueryBuilder('p')
            ->select('p.id')
            ->innerJoin('p.categories', 'c')
            ->andWhere('c IN (:category) and p.status = :status')
            ->setParameter('status', Product::STATUS_ACTIVE)
            ->setParameter('category', $category)
            ->setMaxResults(1)
            ->getQuery()
            ->setHydrationMode(Query::HYDRATE_SINGLE_SCALAR)
            ->getOneOrNullResult();

        return $entity !== null;
    }

    public function getCount()
    {
        return $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param array $ids
     *
     * @return Product[]
     */
    public function findUnavailableProducts(array $ids)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.status != :status')
            ->andWhere('p.id IN (:ids)')
            ->setParameters([
                'status' => Product::STATUS_ACTIVE,
                'ids' => $ids
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $term
     * @return Product[]
     */
    public function findByLikeTitleOrId($term, $limit = 20)
    {
        $term = strtolower($term);
        $id = (int)$term;
        $term = '%' . $term . '%';

        return $this->createQueryBuilder('p')
            ->andWhere('lower(p.title) LIKE :title or p.id = :id')
            ->andWhere('p.status = :status')
            ->setParameters([
                'title' => $term,
                'id' => $id,
                'status' => Product::STATUS_ACTIVE
            ])
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * This builder uses for indexing process
     *
     * @param $alias
     * @param null $indexBy
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function createQueryBuilderForElastic($alias, $indexBy = null)
    {
        $builder = parent::createQueryBuilder($alias, $indexBy);
        $builder->innerJoin($alias . '.categories', 'c')
            ->andWhere("c.enabled = :enabled AND {$alias}.status = :active_status")
            ->setParameter('enabled', true)
            ->setParameter('active_status', Product::STATUS_ACTIVE);

        return $builder;
    }

    /**
     * @param string $alias
     * @param null $indexBy
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function createQueryBuilderForActiveProducts(string $alias, $indexBy = null)
    {
        $builder = parent::createQueryBuilder($alias, $indexBy);
        $builder
            ->andWhere("{$alias}.status = :active_status")
            ->setParameter('active_status', Product::STATUS_ACTIVE);

        return $builder;
    }

    /**
     * @param $supplier
     * @param int $limit
     */
    public function findBySupplier($supplier, $limit = 100)
    {
        $this->createQueryBuilder('p')
            ->andWhere('p.supplier = :supplier')
            ->setParameter('supplier', $supplier)
            ->getQuery()
            ->getResult();
    }

    /**
     * Finds by external identifiers
     *
     * @param string $externalSource
     * @param string $externalId
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByExternalIdentifiers(string $externalSource, string $externalId)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.externalSource = :source')
            ->andWhere('p.externalId = :external_id')
            ->setParameters([
                'source' => $externalSource,
                'external_id' => $externalId
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }
}