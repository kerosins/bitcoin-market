<?php
/**
 * @author Kondaurov
 */

namespace Catalog\ORM\Repository;

use Catalog\ORM\Entity\AliExpressPage;
use Kerosin\Doctrine\ORM\EntityRepository;

/**
 * Class AliExpressPageRepository
 * @package Catalog\ORM\Repository
 *
 * @method AliExpressPage|null findOneByUri(string $uri)
 */
class AliExpressPageRepository extends EntityRepository
{
    /**
     * Find all pages by uris
     *
     * @param string[] $uris
     *
     * @return AliExpressPage[]
     */
    public function findByUriList(array $uris)
    {
        $pages = $this->createQueryBuilder('aep')
            ->where('aep.uri IN (:uris)')
            ->setParameter('uris', $uris)
            ->getQuery()
            ->getResult();

        return $pages;
    }

    /**
     * Insert or update page
     *
     * @param string $uri
     * @param string $html
     * @param bool $flush
     */
    public function upsert(string $uri, string $html, bool $flush = true): void
    {
        $entity = $this->findOneByUri($uri);
        if (!$entity) {
            $entity = (new AliExpressPage())
                ->setUri($uri);
        }

        $entity->setHtml($html);

        $this->getEntityManager()->persist($entity);
        if ($flush) {
            $this->getEntityManager()->flush($entity);
        }
    }
}