<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Repository;


use Catalog\ORM\Entity\CanonicalCategory;
use Catalog\ORM\Entity\ExternalCategoriesMap;
use Kerosin\Doctrine\ORM\EntityRepository;
use Catalog\ORM\Entity\Category;

/**
 * Class ExternalCategoriesMapRepository
 * @package Catalog\ORM\Repository
 *
 * @method ExternalCategoriesMap findOneByExternalId(string $id)
 */
class ExternalCategoriesMapRepository extends EntityRepository
{

    public function createIfNotExist(CanonicalCategory $canonicalCategory, Category $category)
    {
        $exist = $this->createQueryBuilder('ecm')
            ->select('count(ecm.id)')
            ->andWhere('ecm.externalId = :external')
            ->andWhere('ecm.source = :source')
            ->setParameter('external', $canonicalCategory->getId())
            ->setParameter('source', $canonicalCategory->getSource())
            ->getQuery()
            ->getSingleScalarResult();

        if (!$exist) {
            $item = new ExternalCategoriesMap();
            $item->setSource($canonicalCategory->getSource())
                ->setExternalId($canonicalCategory->getId())
                ->setCategory($category);

            $this->getEntityManager()->persist($item);
            $this->getEntityManager()->flush($item);
        }
    }

}