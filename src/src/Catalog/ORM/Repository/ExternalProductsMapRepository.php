<?php

namespace Catalog\ORM\Repository;

use Catalog\ORM\Entity\Product;
use Kerosin\Doctrine\ORM\EntityRepository;

class ExternalProductsMapRepository extends EntityRepository
{
    /**
     * @param Product|int $product
     *
     * @return int|null
     */
    public function findExternalIdByProduct($product)
    {
        return $this->createQueryBuilder('epm')
            ->select('epm.externalId')
            ->andWhere('epm.product = :product')
            ->setParameter('product', $product)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
