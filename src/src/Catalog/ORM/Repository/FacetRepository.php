<?php
/**
 * FacetRepository class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Catalog\ORM\Repository;

use Kerosin\Doctrine\ORM\EntityRepository;

class FacetRepository extends EntityRepository
{

}