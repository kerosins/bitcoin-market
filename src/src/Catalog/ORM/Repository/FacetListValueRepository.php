<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Repository;

use Kerosin\Doctrine\ORM\EntityRepository;
use Catalog\ORM\Entity\FacetListValue;

/**
 * Class FacetListValueRepository
 * @package Shop\Repository
 *
 * @method findOneByTitle(string $title)
 */
class FacetListValueRepository extends EntityRepository
{
    /**
     * @param $title
     *
     * @return FacetListValue
     */
    public function createIfNotExistsByTitle($title) : FacetListValue
    {
        $em = $this->getEntityManager();

        $isset = $this->findOneByTitle($title);
        if ($isset) {
            return $isset;
        }

        $new = new FacetListValue();
        $new->setTitle($title);

        $em->persist($new);
        $em->flush($new);

        return $new;
    }
}