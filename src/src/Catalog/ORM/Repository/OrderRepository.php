<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Repository;

use Catalog\ORM\Entity\Order;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Kerosin\Doctrine\ORM\EntityRepository;
use Payment\Entity\Payment;
use User\Entity\User;

/**
 * Class OrderRepository
 * @package Shop\Repository
 *
 * @method \Catalog\ORM\Entity\Order|null findOneByHash(string $hash)
 */
class OrderRepository extends EntityRepository
{

    /**
     * Find orders by delivery email
     *
     * @param string $email
     * @return array
     */
    public function findByDeliveryEmailAndEmptyUser(string $email)
    {
        $builder = $this->createQueryBuilder('o');

        $builder->innerJoin('o.delivery', 'd')
                ->andWhere('o.user is null')
                ->andWhere('d.email = :email')
                ->setParameter('email', $email);

        return $builder->getQuery()->getResult();
    }

    /**
     * Check by code if order exist
     *
     * @param $code
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function existByCode($code)
    {
        return (bool)$this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->andWhere('o.code = :code')
            ->setParameter('code', $code)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    /**
     * @param $payment
     *
     * @return Order|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByPayment($payment)
    {
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata($this->getClassName(), 'o');

        $paymentId = $payment;
        if ($payment instanceof Payment) {
            $paymentId = $payment->getId();
        }

        $sql = "
          SELECT o.* FROM order_tbl o
          INNER JOIN order_payment op ON o.id = op.order_id
          WHERE op.payment_id = :payment
        ";

        return $this
            ->getEntityManager()
            ->createNativeQuery($sql, $rsm)
            ->setParameter('payment', $paymentId)
            ->getOneOrNullResult();
    }

    /**
     * Finds orders with are present payment
     * @return Order[]
     */
    public function findByPayments($payments)
    {
        return $this->createQueryBuilder('o')
            ->where('o.payment IN (:payments)')
            ->setParameter('payments', $payments)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Returns count of user's orders
     *
     * @param User|int $user
     *
     * @return int
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCountOrdersByUser($user)
    {
        return $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->where('o.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param $status
     *
     * @return Order[]
     */
    public function findByCriticalDateAndStatus(int $daysUntilEnd, $status)
    {
        $qb = $this->createQueryBuilder('o');

        $qb->where('DATE_DIFF(o.eolDate, CURRENT_DATE()) <= :critical')
           ->innerJoin('o.currentStatus', 'c')
           ->andWhere('c.type = :status')
           ->andWhere('o.isSentRemindMail = :isSent OR o.isSentRemindMail IS NULL')
           ->setParameters([
               'critical' => $daysUntilEnd,
               'status' => $status,
               'isSent' => false
           ])
        ;

        return $qb->getQuery()->getResult();
    }
}