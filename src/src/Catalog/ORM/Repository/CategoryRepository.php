<?php
/**
 * CategoryRepository class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Catalog\ORM\Repository;

use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\Source;
use Doctrine\ORM\Query;
use Kerosin\Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    public function createQueryBuilderForElastic($alias, $indexBy = null)
    {
        return parent::createQueryBuilder($alias, $indexBy)->orderBy($alias . '.id', 'ASC');
    }

    /**
     * Find all child categories
     *
     * @param null $parent
     * @return array
     */
    public function findCategories($parent = null, $hydration = Query::HYDRATE_OBJECT)
    {
        $builder =  $this->createQueryBuilder('c')
            ->select('c', 'children')
            ->leftJoin('c.children', 'children')
            ->orderBy('c.order', 'desc');

        if ($parent) {
            $builder->andWhere('c.parent = :parent')->setParameter('parent', (int)$parent);
        } else {
            $builder->andWhere('c.parent is null');
        }

        return $builder->getQuery()->getResult($hydration);
    }

    /**
     * @param $id
     * @return array
     */
    public function findAllParents($id)
    {
        $parents = [];

        $current = $this->find($id);
        while (true) {
            array_push($parents, $current);

            if (!$current->getParent()) {
                break;
            }

            $current = $current->getParent();
        }

        return array_reverse($parents);
    }

    /**
     * @param $ids
     * @return array
     */
    public function findByInIds($ids)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }

    /**
     * Get by name, non case sensitive
     *
     * @param $name
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByTitleNonCaseSensitive($name)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('lower(c.title) = :title')
            ->setParameter(':title', strtolower($name))
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $ids
     * @param string $source
     *
     * @return Category[]
     */
    public function findByExternalId($ids, $source = Source::EPN_YML)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->join('c.externalReferences', 'e')
            ->andWhere('e.externalId IN (:ids)')
            ->andWhere('e.source = :source')
            ->setParameters([
                'ids' => $ids,
                'source' => $source
            ]);

        return $qb->getQuery()->getResult();
    }
}