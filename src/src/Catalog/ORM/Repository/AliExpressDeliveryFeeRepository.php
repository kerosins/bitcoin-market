<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\ORM\Repository;


use Catalog\ORM\Entity\AliExpressDeliveryFee;
use Kerosin\Doctrine\ORM\EntityRepository;
use Shop\Entity\Country;
use Catalog\ORM\Entity\Product;

class AliExpressDeliveryFeeRepository extends EntityRepository
{
    /**
     * @param Product|int $product
     * @param Country|int $country
     * @param int $quantity
     * @param int $expire
     *
     * @return AliExpressDeliveryFee|null
     */
    public function findByProductAndCountry(
        $product,
        $country,
        int $quantity = 1,
        int $expire = 3600
    ) {
        return $this->createQueryBuilder('df')
            ->andWhere('df.product = :product AND df.country = :country')
            ->andWhere('df.quantity = :quantity AND df.createdAt >= :expire')
            ->setParameters([
                'product' => $product,
                'country' => $country,
                'quantity' => $quantity,
                'expire' => (new \DateTime())->sub(new \DateInterval('PT' . $expire . 'S'))
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }
}