<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\ORM\Repository;

use Kerosin\Doctrine\ORM\EntityRepository;
use Catalog\ORM\Entity\Dispute;
use Catalog\ORM\Entity\OrderPosition;

/**
 * Class DisputeRepository
 * @package Order\Repository
 *
 * @method Dispute findOneByOrderPosition(OrderPosition|int $orderProduct)
 */
class DisputeRepository extends EntityRepository
{

}