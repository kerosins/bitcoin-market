<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Repository;


use Kerosin\Doctrine\ORM\EntityRepository;
use Catalog\ORM\Entity\AssemblyFacet;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\FacetCategory;
use Catalog\ORM\Entity\ProductParameter;

/**
 * Class AssemblyFacetRepository
 * @package Shop\Repository
 *
 * @method findByCategory(Category $category)
 */
class AssemblyFacetRepository extends EntityRepository
{
    /**
     * @param $facet
     * @param $categoryId
     *
     * @return \Catalog\ORM\Entity\AssemblyFacet|null
     */
    public function findByFacetAndCategory(FacetCategory $facet, Category $category)
    {
        $builder = $this->createQueryBuilder('af')
            ->andWhere('af.facet = :facet')
            ->andWhere('af.category = :category')
            ->setParameters([
                'facet' => $facet,
                'category' => $category
            ])
        ;

        return $builder->getQuery()->getOneOrNullResult();
    }
}