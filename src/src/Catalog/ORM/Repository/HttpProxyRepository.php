<?php
/**
 * @author Kondaurov
 */

namespace Catalog\ORM\Repository;


use Catalog\ORM\Entity\HttpProxy;
use Kerosin\Doctrine\ORM\EntityRepository;

/**
 * Class HttpProxyRepository
 * @package Catalog\ORM\Repository
 */
class HttpProxyRepository extends EntityRepository
{
    /**
     * @param HttpProxy $entity
     * @param bool $flush
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveDetached($entity, bool $flush = true)
    {
        /** @var HttpProxy $realEntity */
        $realEntity = $this->find($entity->getId());
        $realEntity->setIsBanned($entity->getIsBanned());
        $this->save($realEntity, $flush);
    }
}