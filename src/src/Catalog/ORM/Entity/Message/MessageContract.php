<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\ORM\Entity\Message;

/**
 * Provides necessary methods for chatting between users
 *
 * Interface MessageContract
 * @package Order\Entity\Message
 */
interface MessageContract
{
    /**
     * Adds message to chat
     *
     * @param BaseMessage $message
     * @return mixed
     */
    public function addMessage(BaseMessage $message);

    /**
     * Removes message from chat
     *
     * @param BaseMessage $message
     * @return mixed
     */
    public function removeMessage(BaseMessage $message);

    /**
     * Gets are chat's messages
     *
     * @return mixed
     */
    public function getMessages();
}