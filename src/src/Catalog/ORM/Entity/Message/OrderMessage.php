<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\ORM\Entity\Message;
use Doctrine\ORM\Mapping as ORM;
use Catalog\ORM\Entity\Order;

/**
 * Class OrderMessage
 * @package Order\Entity\Message
 *
 * @ORM\Entity()
 */
class OrderMessage extends BaseMessage
{
    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\Order", inversedBy="messages", cascade={"remove"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    public function allowAttachments(): bool
    {
        return false;
    }

    /**
     * @return Order
     */
    public function getOrder(): ?Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return OrderMessage
     */
    public function setOrder(Order $order): OrderMessage
    {
        $this->order = $order;
        return $this;
    }
}