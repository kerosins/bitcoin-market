<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\ORM\Entity\Message;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use User\Entity\User;

/**
 * Class BaseMessage
 * @package Order\Entity\Message
 *
 * @ORM\Entity()
 * @ORM\Table(name="order_message")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="target", type="string")
 * @ORM\DiscriminatorMap({
 *     "order" = "Catalog\ORM\Entity\Message\OrderMessage",
 *     "dispute" = "Catalog\ORM\Entity\Message\DisputeMessage"
 * })
 */
abstract class BaseMessage
{
    public const
        TYPE_ORDER = 'order',
        TYPE_DISPUTE = 'dispute';

    /**
     * @return bool
     */
    abstract public function allowAttachments(): bool;

    use BaseMapping;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User", cascade={"remove"})
     * @ORM\JoinColumn(name="from_user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $from;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $fromDisplayName;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User", cascade={"remove"})
     * @ORM\JoinColumn(name="to_user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $to;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $toDisplayName;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @var array
     *
     * @ORM\Column(type="image_array", nullable=true)
     */
    private $attachments;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return User
     */
    public function getFrom(): ?User
    {
        return $this->from;
    }

    /**
     * @param User $from
     * @return BaseMessage
     */
    public function setFrom(User $from): BaseMessage
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return string
     */
    public function getFromDisplayName(): ?string
    {
        return $this->fromDisplayName;
    }

    /**
     * @param string $fromDisplayName
     * @return BaseMessage
     */
    public function setFromDisplayName(string $fromDisplayName): BaseMessage
    {
        $this->fromDisplayName = $fromDisplayName;
        return $this;
    }

    /**
     * @return User
     */
    public function getTo(): ?User
    {
        return $this->to;
    }

    /**
     * @param User $to
     * @return BaseMessage
     */
    public function setTo(User $to): BaseMessage
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return string
     */
    public function getToDisplayName(): ?string
    {
        return $this->toDisplayName;
    }

    /**
     * @param string $toDisplayName
     * @return BaseMessage
     */
    public function setToDisplayName(string $toDisplayName): BaseMessage
    {
        $this->toDisplayName = $toDisplayName;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return BaseMessage
     */
    public function setMessage(string $message): BaseMessage
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return array
     */
    public function getAttachments(): ?array
    {
        return $this->attachments;
    }

    /**
     * @param array $attachments
     * @return BaseMessage
     */
    public function setAttachments(array $attachments): BaseMessage
    {
        $this->attachments = $attachments;
        return $this;
    }
}