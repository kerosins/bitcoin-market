<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\ORM\Entity\Message;
use Doctrine\ORM\Mapping as ORM;
use Catalog\ORM\Entity\Dispute;

/**
 * Class DisputeMessage
 * @package Order\Entity\Message
 *
 * @ORM\Entity()
 */
class DisputeMessage extends BaseMessage
{
    /**
     * @var Dispute
     *
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\Dispute", inversedBy="messages", cascade={"remove"})
     * @ORM\JoinColumn(name="dispute_id", referencedColumnName="id")
     */
    private $dispute;

    public function allowAttachments(): bool
    {
        return true;
    }

    /**
     * @return Dispute
     */
    public function getDispute(): ?Dispute
    {
        return $this->dispute;
    }

    /**
     * @param Dispute $dispute
     * @return DisputeMessage
     */
    public function setDispute(Dispute $dispute): DisputeMessage
    {
        $this->dispute = $dispute;
        return $this;
    }
}