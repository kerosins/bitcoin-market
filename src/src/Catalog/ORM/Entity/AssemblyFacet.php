<?php
/**
 * AssemblyFacet class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Catalog\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;

/**
 * Class AssemblyFacet
 * @package Shop\Entity
 *
 * @ORM\Entity(repositoryClass="Catalog\ORM\Repository\AssemblyFacetRepository")
 * @ORM\Table(name="assembly_facet")
 * @ORM\HasLifecycleCallbacks
 */
class AssemblyFacet
{
    use BaseMapping;

    /**
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\FacetCategory")
     * @ORM\JoinColumn(name="facet_id", referencedColumnName="id")
     */
    private $facet;

    /**
     *
     * @ORM\Column(type="json_array")
     */
    private $data;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Set data
     *
     * @param array $data
     *
     * @return AssemblyFacet
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return AssemblyFacet
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set category
     *
     * @param \Catalog\ORM\Entity\Category $category
     *
     * @return AssemblyFacet
     */
    public function setCategory(\Catalog\ORM\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Catalog\ORM\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set facet
     *
     * @param \Catalog\ORM\Entity\FacetCategory $facet
     *
     * @return AssemblyFacet
     */
    public function setFacet(\Catalog\ORM\Entity\FacetCategory $facet = null)
    {
        $this->facet = $facet;

        return $this;
    }

    /**
     * Get facet
     *
     * @return \Catalog\ORM\Entity\FacetCategory
     */
    public function getFacet()
    {
        return $this->facet;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return AssemblyFacet
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function onUpdate()
    {
        $this->setUpdateAt(new \DateTime());
    }
}
