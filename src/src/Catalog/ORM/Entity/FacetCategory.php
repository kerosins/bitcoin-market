<?php
/**
 * FacetCategory class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Catalog\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class FacetCategory
 * @package Shop\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="facet_category")
 */
class FacetCategory
{
    public const LIST = 1;

    public const RANGE = 2;

    public const BOOL = 3;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToMany(targetEntity="Catalog\ORM\Entity\Category", mappedBy="facets")
     * @ORM\JoinTable(
     *     joinColumns={@ORM\JoinColumn(onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(onDelete="CASCADE")}
     * )
     */
    private $categories;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $sort = 0;

    /**
     * @ORM\Column(type="json_array")
     */
    private $props = [];

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return FacetCategory
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return FacetCategory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return FacetCategory
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return FacetCategory
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set props
     *
     * @param array $props
     *
     * @return FacetCategory
     */
    public function setProps($props)
    {
        $this->props = $props;

        return $this;
    }

    /**
     * Get props
     *
     * @return array
     */
    public function getProps()
    {
        return $this->props;
    }

    /**
     * Set assembly
     *
     * @param \Catalog\ORM\Entity\AssemblyFacet $assembly
     *
     * @return FacetCategory
     */
    public function setAssembly(\Catalog\ORM\Entity\AssemblyFacet $assembly = null)
    {
        $this->assembly = $assembly;

        return $this;
    }

    /**
     * Get assembly
     *
     * @return \Catalog\ORM\Entity\AssemblyFacet
     */
    public function getAssembly()
    {
        return $this->assembly;
    }

    /**
     * Add category
     *
     * @param \Catalog\ORM\Entity\Category $category
     *
     * @return FacetCategory
     */
    public function addCategory(\Catalog\ORM\Entity\Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \Catalog\ORM\Entity\Category $category
     */
    public function removeCategory(\Catalog\ORM\Entity\Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }
}
