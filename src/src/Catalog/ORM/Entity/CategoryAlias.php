<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\ORM\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;

/**
 * Class CategoryAlias
 * @package Catalog\ORM\Entity
 *
 * @ORM\Entity(repositoryClass="Catalog\ORM\Repository\CategoryAliasRepository")
 * @ORM\Table(name="mapping_category_alias")
 */
class CategoryAlias
{
    public const DELIMITER = '|';

    use BaseMapping;

    /**
     * @ORM\OneToOne(targetEntity="Catalog\ORM\Entity\Category", inversedBy="mappingAlias")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @ORM\Column(type="text")
     */
    private $aliases;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Set aliases
     *
     * @param string $aliases
     *
     * @return CategoryAlias
     */
    public function setAliases($aliases)
    {
        $this->aliases = $aliases;

        return $this;
    }

    /**
     * Get aliases
     *
     * @return string
     */
    public function getAliases()
    {
        return $this->aliases;
    }

    /**
     * Set category
     *
     * @param \Catalog\ORM\Entity\Category $category
     *
     * @return CategoryAlias
     */
    public function setCategory(\Catalog\ORM\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Catalog\ORM\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }
}
