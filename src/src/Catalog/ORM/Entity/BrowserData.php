<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\ORM\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;

/**
 * Class BrowserData
 * @package Order\Entity
 *
 * @ORM\Entity()
 * @ORM\Table()
 */
class BrowserData
{
    use BaseMapping;

    /**
     * @var Order
     *
     * @ORM\OneToOne(targetEntity="Catalog\ORM\Entity\Order", inversedBy="browserData")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $order;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $userAgent;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $timezone;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $ip;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     */
    private $headers;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $location;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }


    /**
     * Set userAgent
     *
     * @param string $userAgent
     *
     * @return BrowserData
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Get userAgent
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     *
     * @return BrowserData
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return BrowserData
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return BrowserData
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set order
     *
     * @param \Catalog\ORM\Entity\Order $order
     *
     * @return BrowserData
     */
    public function setOrder(\Catalog\ORM\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Catalog\ORM\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set headers
     *
     * @param array $headers
     *
     * @return BrowserData
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * Get headers
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }



    /**
     * Set location
     *
     * @param array $location
     *
     * @return BrowserData
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return array
     */
    public function getLocation()
    {
        return $this->location;
    }
}
