<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\ORM\Entity;

/**
 * Class AliExpressProduct
 * @package Catalog\ORM\Entity
 *
 * This not real entity
 */
class AliExpressProduct
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var bool
     */
    private $available;

    /**
     * @var float
     */
    private $price;

    /**
     * @var int
     */
    private $items;

    public static function create(bool $available, float $price, int $items)
    {
        return (new self)
            ->setItems($items)
            ->setPrice($price)
            ->setAvailable($available);
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return AliExpressProduct
     */
    public function setId(int $id): AliExpressProduct
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailable(): ?bool
    {
        return $this->available;
    }

    /**
     * @param bool $available
     * @return AliExpressProduct
     */
    public function setAvailable(bool $available): AliExpressProduct
    {
        $this->available = $available;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return AliExpressProduct
     */
    public function setPrice(float $price): AliExpressProduct
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getItems(): ?int
    {
        return $this->items;
    }

    /**
     * @param int $items
     * @return AliExpressProduct
     */
    public function setItems(int $items): AliExpressProduct
    {
        $this->items = $items;
        return $this;
    }

}