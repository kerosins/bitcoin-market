<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\ORM\Entity;


use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;
use Shop\Entity\Country;
use Catalog\ORM\Entity\Product;

/**
 * Class DeliveryCost
 * @package Order\Entity
 *
 * @ORM\Entity(repositoryClass="Catalog\ORM\Repository\AliExpressDeliveryFeeRepository")
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class AliExpressDeliveryFee
{
    use BaseMapping, UpdateTimestamps;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\Product", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $product;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Shop\Entity\Country", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $country;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $quantity = 1;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $cost;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $commitDay;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $fullResponse;

    /**
     * @var array
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $selectedCompany;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return Product
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return AliExpressDeliveryFee
     */
    public function setProduct(Product $product): AliExpressDeliveryFee
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return Country
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * @param Country $country
     * @return AliExpressDeliveryFee
     */
    public function setCountry(Country $country): AliExpressDeliveryFee
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return float
     */
    public function getCost(): ?float
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     * @return AliExpressDeliveryFee
     */
    public function setCost(float $cost): AliExpressDeliveryFee
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return int
     */
    public function getCommitDay(): ?int
    {
        return $this->commitDay;
    }

    /**
     * @param int $commitDay
     * @return AliExpressDeliveryFee
     */
    public function setCommitDay(int $commitDay): AliExpressDeliveryFee
    {
        $this->commitDay = $commitDay;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullResponse(): ?string
    {
        return $this->fullResponse;
    }

    /**
     * @param string $fullResponse
     * @return AliExpressDeliveryFee
     */
    public function setFullResponse(string $fullResponse = null): AliExpressDeliveryFee
    {
        $this->fullResponse = $fullResponse;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return AliExpressDeliveryFee
     */
    public function setQuantity(int $quantity = 0): AliExpressDeliveryFee
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return array
     */
    public function getSelectedCompany(): ?array
    {
        return $this->selectedCompany;
    }

    /**
     * @param array $selectedCompany
     *
     * @return static
     */
    public function setSelectedCompany(array $selectedCompany = []): AliExpressDeliveryFee
    {
        $this->selectedCompany = $selectedCompany;
        return $this;
    }


}