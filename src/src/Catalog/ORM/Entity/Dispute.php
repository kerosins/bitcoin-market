<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\ORM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;
use Catalog\ORM\Entity\Message\BaseMessage;
use Catalog\ORM\Entity\Message\DisputeMessage;
use Catalog\ORM\Entity\Message\MessageContract;
use Catalog\Order\Exception\DisputeException;

/**
 * Class Dispute
 * @package Order\Entity
 *
 * @ORM\Entity(repositoryClass="Catalog\ORM\Repository\DisputeRepository")
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class Dispute implements MessageContract
{
    public const
        STATUS_OPEN = 10,
        STATUS_CLOSE_BY_CUSTOMER = 20,
        STATUS_DECLINE = 30,
        STATUS_FULL_REFUND = 40,
        STATUS_PARTIAL_REFUND = 50;

    public static $statusLabel = [
        self::STATUS_OPEN => 'Open',
        self::STATUS_CLOSE_BY_CUSTOMER => 'Closed by Customer',
        self::STATUS_DECLINE => 'Declined',
        self::STATUS_FULL_REFUND => 'Fully Refunded',
        self::STATUS_PARTIAL_REFUND => 'Partially Refunded'
    ];

    use BaseMapping;
    use UpdateTimestamps;

    /**
     * @var OrderPosition
     *
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\OrderPosition", inversedBy="disputes")
     * @ORM\JoinColumn(name="order_position_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $orderPosition;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $status = self::STATUS_OPEN;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=20, scale=8, nullable=true)
     */
    private $refundAmount;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $declineReason;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isProcessingRefund = false;

    /**
     * @var Collection|DisputeMessage[]
     *
     * @ORM\OneToMany(targetEntity="Catalog\ORM\Entity\Message\DisputeMessage", mappedBy="dispute")
     */
    private $messages;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();

        $this->messages = new ArrayCollection();
    }

    /**
     * @return OrderPosition
     */
    public function getOrderPosition(): ?OrderPosition
    {
        return $this->orderPosition;
    }

    /**
     * @param OrderPosition $orderPosition
     *
     * @return Dispute
     */
    public function setOrderPosition(OrderPosition $orderPosition): Dispute
    {
        $this->orderPosition = $orderPosition;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Dispute
     */
    public function setStatus(int $status): Dispute
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return float
     */
    public function getRefundAmount(): ?float
    {
        return $this->refundAmount;
    }

    /**
     * @param float $refundAmount
     * @return Dispute
     */
    public function setRefundAmount(float $refundAmount): Dispute
    {
        $this->refundAmount = $refundAmount;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeclineReason(): ?string
    {
        return $this->declineReason;
    }

    /**
     * @param string $declineReason
     * @return Dispute
     */
    public function setDeclineReason(string $declineReason): Dispute
    {
        $this->declineReason = $declineReason;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsProcessingRefund(): ?bool
    {
        return $this->isProcessingRefund;
    }

    /**
     * @param bool $isProcessingRefund
     * @return Dispute
     */
    public function setIsProcessingRefund(bool $isProcessingRefund): Dispute
    {
        $this->isProcessingRefund = $isProcessingRefund;
        return $this;
    }

    /**
     * Gets a readable status
     *
     * @return mixed|string
     */
    public function getReadableStatus()
    {
        return self::$statusLabel[$this->status] ?? 'Unknown Status';
    }

    /**
     * @param DisputeMessage $message
     * @return static
     */
    public function addMessage(BaseMessage $message)
    {
        $message->setDispute($this);
        $this->messages->add($message);

        return $this;
    }

    /**
     * @param DisputeMessage $message
     * @return mixed|void
     */
    public function removeMessage(BaseMessage $message)
    {
        throw new DisputeException("Can't remove message from dispute");
    }

    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Gets currency of order
     */
    public function getCurrency()
    {
        return $this->getOrderPosition()->getOrder()->getCurrency();
    }

    /**
     * @return bool
     */
    public function getIsClose()
    {
        return (
            in_array($this->status, [
                Dispute::STATUS_DECLINE,
                Dispute::STATUS_CLOSE_BY_CUSTOMER
            ])
            || (
                in_array($this->status, [
                    Dispute::STATUS_PARTIAL_REFUND,
                    Dispute::STATUS_FULL_REFUND
                ])
                && !$this->isProcessingRefund
            )
        );
    }
}