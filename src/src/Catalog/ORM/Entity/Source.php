<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Entity;


class Source
{
    /**
     * @deprecated
     */
    public const WALMART = 'walmart';

    /**
     * external sources
     */
    public const
        EXTERNAL_SOURCE_ALIEXPRESS = 'aliexpress';

    public const
        EPN_YML = 'epn_yml',
        MANUAL = 'manual';

    public static function getSourcesList()
    {
        return [
            'Epn' => self::EPN_YML,
            'Manual' => self::MANUAL
        ];
    }

    public static function getExternalSourcesList()
    {
        return [
            'AliExpress' => self::EXTERNAL_SOURCE_ALIEXPRESS
        ];
    }
}