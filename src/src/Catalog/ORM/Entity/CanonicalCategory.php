<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Entity;


class CanonicalCategory
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $parent;

    /**
     * @var string
     */
    private $source;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CanonicalCategory
     */
    public function setId($id): CanonicalCategory
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return CanonicalCategory
     */
    public function setTitle(string $title): CanonicalCategory
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param int $parent
     * @return CanonicalCategory
     */
    public function setParent($parent): CanonicalCategory
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param int $source
     * @return CanonicalCategory
     */
    public function setSource(string $source): CanonicalCategory
    {
        $this->source = $source;
        return $this;
    }

}