<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Kerosin\Doctrine\ORM\BaseMapping;

/**
 * Class RemoteProductMap
 * @package Catalog\ORM\Entity
 *
 * @deprecated
 *
 * @ORM\Entity(repositoryClass="Catalog\ORM\Repository\ExternalProductsMapRepository")
 * @ORM\Table(indexes={
 *     @Index(name="search_idx", columns={"external_id", "source", "deleted"})
 * })
 */
class ExternalProductsMap
{
    use BaseMapping;

    public const TABLE = 'external_products_map';

    /**
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\Product", cascade={"remove", "persist"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $product;

    /**
     * @ORM\Column(type="string")
     */
    private $externalId;

    /**
     * @ORM\Column(type="string")
     */
    private $source;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $countFailureAvailability = 0;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $deleted = false;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Set externalId
     *
     * @param string $externalId
     *
     * @return ExternalProductsMap
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set source
     *
     * @param string $source
     *
     * @return ExternalProductsMap
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set product
     *
     * @param \Catalog\ORM\Entity\Product $product
     *
     * @return ExternalProductsMap
     */
    public function setProduct(\Catalog\ORM\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Catalog\ORM\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getCountFailureAvailability(): ?int
    {
        return $this->countFailureAvailability;
    }

    /**
     * @param int $countFailureAvailability
     * @return ExternalProductsMap
     */
    public function setCountFailureAvailability(int $countFailureAvailability): self
    {
        $this->countFailureAvailability = $countFailureAvailability;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted(): ?bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     * @return ExternalProductsMap
     */
    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;
        return $this;
    }
}
