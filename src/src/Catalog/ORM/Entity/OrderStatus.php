<?php
/**
 * @author kerosin
 */

namespace Catalog\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;

/**
 * Class OrderStatus
 * @package Shop\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="order_status", indexes={@ORM\Index(name="status_type_idx", columns={"type"})})
 */
class OrderStatus
{
    public const
        CHECKOUT = 10,
        PAYMENT_WAIT = 15,
        PAYMENT_EXPIRED = 18,
        PAYMENT_PROCESS = 20,
        PAYMENT_SUCCESS = 25,
        PROCESS = 30,
        SHIPPING = 40,
        DELIVERED = 45,
        DONE = 50,
        CANCEL = 60
    ;

    public static $statusString = [
        self::PAYMENT_WAIT => 'Awaiting payment',
        self::PAYMENT_EXPIRED => 'Payment has expired',
        self::PAYMENT_PROCESS => 'Payment pending',
        self::PAYMENT_SUCCESS => 'Payment successful',
        self::PROCESS => 'Processing',
        self::SHIPPING => 'Order is shipped',
        self::DELIVERED => 'Order delivered',
        self::DONE => 'Order done',
        self::CANCEL => 'Order cancelled',
    ];

    use BaseMapping;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\Order", inversedBy="historyStatuses")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $order;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment = '';

    public function __construct($type = null)
    {
        if ($type) {
            $this->type = $type;
        }
        $this->createdAt = $this->updatedAt = new \DateTime();
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return OrderStatus
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return OrderStatus
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return OrderStatus
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set order
     *
     * @param \Catalog\ORM\Entity\Order $order
     *
     * @return OrderStatus
     */
    public function setOrder(\Catalog\ORM\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Catalog\ORM\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Get status string
     *
     * @return mixed|string
     */
    public function getReadableStatus()
    {
        return static::$statusString[$this->type] ?? 'Unknown';
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return OrderStatus
     */
    public function setComment(string $comment): self
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * Check if status isn't close status
     *
     * @return bool
     */
    public function isOpened()
    {
        return !in_array($this->type, [static::CANCEL, static::DONE, static::PAYMENT_EXPIRED]);
    }
}
