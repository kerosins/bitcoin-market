<?php
/**
 * @author Kondaurov
 */
namespace Catalog\ORM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Kerosin\Component\Image;
use Kerosin\Doctrine\ORM\MetaTagsMapping;
use Content\Seo\SeoDataProviderInterface;
use Content\SlugRoute\Sluggable;
use Kerosin\Doctrine\ORM\UpdateTimestamps;
use Shop\Entity\Seller;

/**
 * Class Product
 * @package Shop\Entity
 *
 * @ORM\Entity(repositoryClass="Catalog\ORM\Repository\ProductRepository")
 * @ORM\Table(
 *     name="product",
 *     indexes={
 *         @Index(name="idx_product_status", columns={"status"}),
 *         @Index(name="idx_product_external_source", columns={"external_source", "external_id"})
 *     }
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class Product implements SeoDataProviderInterface, Sluggable
{
    use MetaTagsMapping, UpdateTimestamps;

    /**
     * constants of statuses
     */
    public const
        STATUS_ACTIVE = 0,
        STATUS_UNAVAILABLE = 1,
        STATUS_ARCHIVED = 2,
        STATUS_DELETED = 3,
        STATUS_NEW = 4;

    public static $readableStatuses = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_UNAVAILABLE => 'Unavailable',
        self::STATUS_ARCHIVED => 'Archived',
        self::STATUS_DELETED => 'Deleted',
        self::STATUS_NEW => 'New'
    ];

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $shortDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="image_array")
     */
    private $images = [];

    /**
     * @var Image
     *
     * @ORM\Column(type="image")
     */
    private $mainImage;

    /**
     * @ORM\ManyToMany(targetEntity="Catalog\ORM\Entity\Category")
     * @ORM\JoinTable(name="category_product",
     *     joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private $categories;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sort;

    /**
     * @ORM\OneToMany(targetEntity="Catalog\ORM\Entity\ProductParameter", mappedBy="product", cascade={"remove"})
     */
    private $parameters;

    /**
     * @ORM\OneToOne(targetEntity="Catalog\ORM\Entity\ProductPrice", mappedBy="product", cascade={"remove"})
     */
    private $price;

    /**
     * @var Seller
     *
     * Owner of goods
     *
     * @ORM\ManyToOne(targetEntity="Shop\Entity\Seller", inversedBy="products")
     * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id")
     */
    private $seller;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $supplierId;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $available = true;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $amountItems = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $amountOrders = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $amountWishLists = 0;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $sellerName;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $status = self::STATUS_ACTIVE;

    //VIRTUAL PROPERTIES

    /**
     * @var float
     */
    private $publicPrice;

    /**
     * If entity must be deleted from db physical
     *
     * @var bool
     */
    private $forceDelete = false;

    /**
     * @var Collection|\Content\ORM\Entity\Slug[]
     */
    private $slugs;

    /**
     * IMPORT BASED COLUMNS
     */

    /**
     * Where is from create product
     *
     * @var string
     * @ORM\Column(type="string")
     */
    private $source;

    /**
     * Where is from sync product
     *
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $externalSource;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $externalLink;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $externalId;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $availabilityFailureCount = 0;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();

        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->parameters = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = trim($title);
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    /**
     * @param string $shortDescription
     *
     * @return Product
     */
    public function setShortDescription(string $shortDescription): Product
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Product
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set images
     *
     * @param array $images
     *
     * @return Product
     */
    public function setImages($images)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Get images
     *
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return Product
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Add category
     *
     * @param \Catalog\ORM\Entity\Category $category
     *
     * @return Product
     */
    public function addCategory(\Catalog\ORM\Entity\Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \Catalog\ORM\Entity\Category $category
     */
    public function removeCategory(\Catalog\ORM\Entity\Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add parameter
     *
     * @param \Catalog\ORM\Entity\ProductParameter $parameter
     *
     * @return Product
     */
    public function addParameter(\Catalog\ORM\Entity\ProductParameter $parameter)
    {
        $this->parameters[] = $parameter;
        $parameter->setProduct($this);

        return $this;
    }

    /**
     * Remove parameter
     *
     * @param \Catalog\ORM\Entity\ProductParameter $parameter
     */
    public function removeParameter(\Catalog\ORM\Entity\ProductParameter $parameter)
    {
        $this->parameters->removeElement($parameter);
    }

    /**
     * Get parameters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Set actual price
     *
     * @param \Catalog\ORM\Entity\ProductPrice $price
     *
     * @return Product
     */
    public function setPrice(\Catalog\ORM\Entity\ProductPrice $price = null)
    {
        $this->price = $price;
        if ($price !== null) {
            $price->setProduct($this);
        }
        return $this;
    }

    /**
     * Get price
     *
     * @return \Catalog\ORM\Entity\ProductPrice
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Provide total price from ProductPrice entity
     *
     * @return float|null
     */
    public function getPublicPrice()
    {
        if ($this->publicPrice !== null) {
            return $this->publicPrice;
        }


        if (!$this->getPrice()) {
            $innerPrice = 0.0;
        } else {
            $innerPrice = $this->getPrice()->getTotalPrice();
        }

        $this->publicPrice = $innerPrice;
        return $this->publicPrice;
    }

    /**
     * @return Image|null
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    /**
     * @param Image $mainImage
     * @return $this
     */
    public function setMainImage(Image $mainImage)
    {
        $this->mainImage = $mainImage;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->status === Product::STATUS_ACTIVE;
    }

    public function setForceDelete($delete)
    {
        $this->forceDelete = $delete;
        return $this;
    }

    public function getForceDelete()
    {
        return $this->forceDelete;
    }

    /**
     * Set externalLink
     *
     * @param string $externalLink
     *
     * @return Product
     */
    public function setExternalLink($externalLink)
    {
        $this->externalLink = $externalLink;

        return $this;
    }

    /**
     * Get externalLink
     *
     * @return string
     */
    public function getExternalLink()
    {
        return urldecode($this->externalLink);//todo it temp
    }

    /**
     * Set supplier
     *
     * @param \Shop\Entity\Seller $seller
     *
     * @return Product
     */
    public function setSeller(\Shop\Entity\Seller $seller = null)
    {
        $this->seller = $seller;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \Shop\Entity\Seller
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @return bool
     */
    public function getAvailable(): ?bool
    {
        return $this->available;
    }

    /**
     * @param bool $available
     * @return Product
     */
    public function setAvailable(bool $available): Product
    {
        $this->available = $available;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountItems(): ?int
    {
        return $this->amountItems;
    }

    /**
     * @param int $amountItems
     *
     * @return Product
     */
    public function setAmountItems(int $amountItems): Product
    {
        $this->amountItems = $amountItems;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountOrders(): ?int
    {
        return $this->amountOrders;
    }

    /**
     * @param int $amountOrders
     *
     * @return Product
     */
    public function setAmountOrders(int $amountOrders): Product
    {
        $this->amountOrders = $amountOrders;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountWishLists(): ?int
    {
        return $this->amountWishLists;
    }

    /**
     * @param int $amountWishLists
     *
     * @return Product
     */
    public function setAmountWishLists(int $amountWishLists): Product
    {
        $this->amountWishLists = $amountWishLists;
        return $this;
    }

    /**
     * @return string
     */
    public function getSellerName(): ?string
    {
        return $this->sellerName ?? $this->seller->getName();
    }

    /**
     * @param string $sellerName
     *
     * @return Product
     */
    public function setSellerName(string $sellerName = null): Product
    {
        $this->sellerName = $sellerName;
        return $this;
    }

    /**
     * @return int
     */
    public function getSupplierId(): ?int
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplierId
     *
     * @return Product
     */
    public function setSupplierId(int $supplierId): Product
    {
        $this->supplierId = $supplierId;
        return $this;
    }

    public function getSlugType(): string
    {
        return \Content\ORM\Entity\Slug::TARGET_PRODUCT;
    }

    public function getSlugAttribute(): string
    {
        return 'title';
    }

    public function getIdentifier(): int
    {
        return $this->id;
    }

    public function getSlugs()
    {
        return $this->slugs;
    }

    public function setSlugs($slugs)
    {
        $this->slugs = new ArrayCollection($slugs);
    }

    public function addSlug(\Content\ORM\Entity\Slug $slug)
    {
        if (!$this->slugs instanceof Collection) {
            $this->slugs = new ArrayCollection();
        }

        $this->slugs->add($slug);
        return $this;
    }

    /**
     * fixme
     *
     * @return Seller
     */
    public function getSupplier()
    {
        return $this->seller;
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return Product
     */
    public function setStatus(int $status): Product
    {
        $this->status = $status;
        return $this;
    }

    public function getReadableStatus(): string
    {
        return self::$readableStatuses[$this->status] ?? 'Unknown';
    }

    /**
     * Toggle status between active and unactive
     *
     * @param bool $isActive
     *
     * @return Product
     */
    public function toggleStatus(bool $isActive): Product
    {
        $this->status = $isActive ? self::STATUS_ACTIVE : self::STATUS_UNAVAILABLE;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string $source
     *
     * @return Product
     */
    public function setSource(string $source = null): Product
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     *
     * @return Product
     */
    public function setExternalId(string $externalId): Product
    {
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * @return int
     */
    public function getAvailabilityFailureCount(): ?int
    {
        return $this->availabilityFailureCount;
    }

    /**
     * @param int $availabilityFailureCount
     *
     * @return Product
     */
    public function setAvailabilityFailureCount(int $availabilityFailureCount): Product
    {
        $this->availabilityFailureCount = $availabilityFailureCount;
        return $this;
    }

    /**
     * @return string
     */
    public function getExternalSource(): ?string
    {
        return $this->externalSource;
    }

    /**
     * @param string $externalSource
     *
     * @return Product
     */
    public function setExternalSource(string $externalSource = null): Product
    {
        $this->externalSource = $externalSource;
        return $this;
    }

    /**
     * @return int
     */
    public function getDeliveryInterval(): int
    {
        return 14 + ((int)substr($this->getId(), -1, 1));
    }
}
