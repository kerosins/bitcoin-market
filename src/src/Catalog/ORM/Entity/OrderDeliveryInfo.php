<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use User\Entity\UserDeliveryInfo;

/**
 * Class OrderDeliveryInfo
 * @package Order\Entity
 *
 * @ORM\Table(name="order_delivery_info")
 * @ORM\Entity()
 *
 * @ORM\HasLifecycleCallbacks()
 */
class OrderDeliveryInfo extends DeliveryInfo
{
    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\Order", inversedBy="delivery")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $order;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * Set order
     *
     * @param \Catalog\ORM\Entity\Order $order
     *
     * @return OrderDeliveryInfo
     */
    public function setOrder(\Catalog\ORM\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Catalog\ORM\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return OrderDeliveryInfo
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}
