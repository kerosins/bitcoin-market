<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Catalog\Order\Component\TotalPrice;
use Catalog\ORM\Entity\Message\BaseMessage;
use Catalog\ORM\Entity\Message\MessageContract;
use Catalog\ORM\Entity\Message\OrderMessage;
use Catalog\Order\Exception\OrderManagerException;
use Payment\Component\Currency;
use Payment\Entity\CurrencyRate;
use Payment\Entity\Payment;
use Catalog\ORM\Entity\ProductPrice;

/**
 * Class Order
 * @package Shop\Entity
 *
 * @ORM\Entity(repositoryClass="Catalog\ORM\Repository\OrderRepository")
 * @ORM\Table(name="order_tbl")
 * @ORM\HasLifecycleCallbacks
 */
class Order implements MessageContract
{
    use BaseMapping;

    /**
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @ORM\Column(type="string")
     */
    private $hash;

    /**
     * @ORM\OneToMany(targetEntity="Catalog\ORM\Entity\OrderPosition", mappedBy="order", cascade={"remove"})
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $orderProducts;

    /**
     * @ORM\OneToOne(targetEntity="Catalog\ORM\Entity\OrderStatus")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    private $currentStatus;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Catalog\ORM\Entity\OrderStatus", mappedBy="order", cascade={"remove"})
     */
    private $historyStatuses;

    /**
     * @ORM\OneToOne(targetEntity="Catalog\ORM\Entity\OrderDeliveryInfo", mappedBy="order", cascade={"remove"})
     */
    private $delivery;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Payment\Entity\Payment")
     * @ORM\JoinTable(name="order_payment",
     *     joinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="payment_id", referencedColumnName="id")}
     * )
     */
    private $payment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $paymentDate;

    /**
     * @var BrowserData
     *
     * @ORM\OneToOne(targetEntity="Catalog\ORM\Entity\BrowserData", mappedBy="order")
     */
    private $browserData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $eolDate;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isSentMail = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isSentRemindMail = false;

    /**
     * @var TotalPrice
     */
    private $totalPrice;

    /**
     * @var Collection|OrderMessage[]
     *
     * @ORM\OneToMany(targetEntity="Catalog\ORM\Entity\Message\OrderMessage", mappedBy="order")
     */
    private $messages;

    /**
     * @var Payment|null
     */
    private $mainPayment;

    /**
     * @var Currency|null
     */
    private $currency;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = $this->updatedAt = new \DateTime();
        $this->orderProducts = new ArrayCollection();
        $this->payment = new ArrayCollection();

        $this->messages = new ArrayCollection();
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Order
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Order
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Order
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set hash
     *
     * @param string $hash
     *
     * @return Order
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Order
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \User\Entity\User $user
     *
     * @return Order
     */
    public function setUser(\User\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add orderProduct
     *
     * @param \Catalog\ORM\Entity\OrderPosition $orderProduct
     *
     * @return Order
     */
    public function addOrderProduct(\Catalog\ORM\Entity\OrderPosition $orderProduct)
    {
        $this->orderProducts[] = $orderProduct;
        $orderProduct->setOrder($this);
        return $this;
    }

    /**
     * Remove orderProduct
     *
     * @param \Catalog\ORM\Entity\OrderPosition $orderProduct
     */
    public function removeOrderProduct(\Catalog\ORM\Entity\OrderPosition $orderProduct)
    {
        $this->orderProducts->removeElement($orderProduct);
    }

    /**
     * Get orderProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderProducts()
    {
        return $this->orderProducts;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function onCreateUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set currentStatus
     *
     * @param \Catalog\ORM\Entity\OrderStatus $currentStatus
     *
     * @return Order
     */
    public function setCurrentStatus(\Catalog\ORM\Entity\OrderStatus $currentStatus = null)
    {
        $this->currentStatus = $currentStatus;
        if ($currentStatus) {
            $currentStatus->setOrder($this);
            $this->addHistoryStatus($currentStatus);
        }
        return $this;
    }

    /**
     * Get currentStatus
     *
     * @return \Catalog\ORM\Entity\OrderStatus
     */
    public function getCurrentStatus()
    {
        return $this->currentStatus;
    }

    /**
     * Add historyStatus
     *
     * @param \Catalog\ORM\Entity\OrderStatus $historyStatus
     *
     * @return Order
     */
    public function addHistoryStatus(\Catalog\ORM\Entity\OrderStatus $historyStatus)
    {
        $this->historyStatuses[] = $historyStatus;
        $historyStatus->setOrder($this);
        return $this;
    }

    /**
     * Remove historyStatus
     *
     * @param \Catalog\ORM\Entity\OrderStatus $historyStatus
     */
    public function removeHistoryStatus(\Catalog\ORM\Entity\OrderStatus $historyStatus)
    {
        $this->historyStatuses->removeElement($historyStatus);
    }

    /**
     * Get historyStatuses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistoryStatuses()
    {
        return $this->historyStatuses;
    }

    /**
     * Set delivery
     *
     * @param \Catalog\ORM\Entity\OrderDeliveryInfo $delivery
     *
     * @return Order
     */
    public function setDelivery(\Catalog\ORM\Entity\OrderDeliveryInfo $delivery = null)
    {
        $this->delivery = $delivery;
        if ($delivery) {
            $delivery->setOrder($this);
        }
        return $this;
    }

    /**
     * Get delivery
     *
     * @return \Catalog\ORM\Entity\OrderDeliveryInfo
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Provide main currency of order
     *
     * @return string
     */
    public function getCurrency()
    {
        if (!$this->currency && $this->getMainPayment()) {
            $mainPayment = $this->getMainPayment();
            $currency = (new Currency())
                ->setRate($mainPayment->getRate())
                ->setCode($mainPayment->getCurrency());
            $this->currency = $currency;
        }

        return $this->currency;
    }

    /**
     * Set browserData
     *
     * @param \Catalog\ORM\Entity\BrowserData $browserData
     *
     * @return Order
     */
    public function setBrowserData(\Catalog\ORM\Entity\BrowserData $browserData = null)
    {
        $this->browserData = $browserData;

        return $this;
    }

    /**
     * Get browserData
     *
     * @return \Catalog\ORM\Entity\BrowserData
     */
    public function getBrowserData()
    {
        return $this->browserData;
    }

    /**
     * @return \DateTime|null
     */
    public function getEolDate()
    {
        return $this->eolDate;
    }

    /**
     * @param \DateTime $eolDate
     * @return Order
     */
    public function setEolDate(\DateTime $eolDate): Order
    {
        $this->eolDate = $eolDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @param \DateTime $paymentDate
     * @return Order
     */
    public function setPaymentDate(\DateTime $paymentDate = null)
    {
        $this->paymentDate = $paymentDate;
        return $this;
    }

    /**
     * Set isSentMail
     *
     * @param boolean $isSentMail
     *
     * @return Order
     */
    public function setIsSentMail($isSentMail)
    {
        $this->isSentMail = $isSentMail;

        return $this;
    }

    /**
     * Get isSentMail
     *
     * @return boolean
     */
    public function getIsSentMail()
    {
        return $this->isSentMail;
    }

    /**
     * @return bool
     */
    public function getIsSentRemindMail(): ?bool
    {
        return $this->isSentRemindMail;
    }

    /**
     * @param bool $isSentRemindMail
     * @return Order
     */
    public function setIsSentRemindMail(bool $isSentRemindMail): Order
    {
        $this->isSentRemindMail = $isSentRemindMail;
        return $this;
    }

    /**
     * Add payment
     *
     * @param \Payment\Entity\Payment $payment
     *
     * @return Order
     */
    public function addPayment(\Payment\Entity\Payment $payment)
    {
        $this->payment[] = $payment;

        return $this;
    }

    /**
     * Remove payment
     *
     * @param \Payment\Entity\Payment $payment
     */
    public function removePayment(\Payment\Entity\Payment $payment)
    {
        $this->payment->removeElement($payment);
    }

    /**
     * Get payment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @return Collection
     */
    public function getPaymentsByZnt()
    {
        return $this->payment->filter(function (Payment $payment) {
            return $payment->getCurrency() === CurrencyRate::ZBC;
        });
    }

    /**
     * @return Collection|Payment[]
     */
    public function getRealPayments()
    {
        return $this->payment->filter(function (Payment $payment) {
            return $payment->getCurrency() !== CurrencyRate::ZBC &&
                $payment->getStatus() !== Payment::PAYMENT_CANCELED;
        });
    }

    /**
     * Gets first payment in crypto if exists or bonuses
     * @return Payment
     */
    public function getMainPayment()
    {
        if (!$this->mainPayment) {
            $this->mainPayment = $this->getRealPayments()->count() ? $this->getRealPayments()->first() : $this->getPaymentsByZnt()->first();
        }

        return $this->mainPayment;
    }

    /**
     * @return TotalPrice
     */
    public function getTotalPrice()
    {
        if ($this->totalPrice) {
            return $this->totalPrice;
        }

        $total = new TotalPrice();

        $products = $this->getOrderProducts();

        $amounts = $products->map(function (OrderPosition $orderPosition) {
            return [
                'sub_total' => $orderPosition->getTotalPublicPrice(),
                'tax' => $orderPosition->getShipmentTax(),
                'total' => $orderPosition->getTotalPriceWithTax()
            ];
        })->toArray();

        $total->setTotal(array_sum(array_column($amounts, 'total')));
        $total->setSubTotal(array_sum(array_column($amounts, 'sub_total')));
        $total->setDeliveryTax(array_sum(array_column($amounts, 'tax')));

        return $total;
    }

    /**
     * @deprecated
     *
     * Get total inner price
     */
    public function getTotalInnerPrice()
    {
        $price = 0.0;

        foreach ($this->orderProducts as $item) {
            /** @var OrderPosition $item */
            $price = bcadd($price, $item->getInnerPrice(), ProductPrice::SCALE);
        }

        return $price;
    }

    /**
     * @deprecated
     *
     * Gets total inner price with scale
     *
     * @return float
     */
    public function getTotalPublicPrice()
    {
        $price = 0.0;

        foreach ($this->orderProducts as $product) {
            /** @var OrderPosition $product */
            $price = bcadd($price, $product->getTotalPublicPrice(), ProductPrice::SCALE);
        }

        return $price;
    }

    /**
     * Checks if current status equal specified
     *
     * @param int|int[] $status
     */
    public function hasStatus($status)
    {
        $status = (array)$status;
        return in_array($this->getCurrentStatus()->getType(), $status);
    }


    /**
     * @param OrderMessage $message
     * @return static
     */
    public function addMessage(BaseMessage $message)
    {
        $message->setOrder($this);
        $this->messages->add($message);

        return $this;
    }

    /**
     * @param OrderMessage $message
     * @return mixed|void
     */
    public function removeMessage(BaseMessage $message)
    {
        throw new OrderManagerException("Can't remove message from order");
    }

    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * todo may be move to DisputeManager and use cache
     *
     * Provide list of disputes
     *
     * @return Dispute[]|array
     */
    public function getDisputes()
    {
        $disputes = [];
        $positionWithDisputes = $this->orderProducts->filter(function (OrderPosition $orderProduct) {
            return $orderProduct->getDisputes()->count() > 0;
        });

        foreach ($positionWithDisputes as $position) {
            $disputes[] = $position->getDisputes()->first();
        }

        return $disputes;
    }
}
