<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Entity;

use Kerosin\Component\Image;

/**
 * This like Entity, but doesn't real entity
 *
 * Class CanonicalProduct
 * @package Catalog\ORM\Entity
 */
class CanonicalProduct
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var CanonicalCategory
     */
    private $category;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $price = 0.0;

    /**
     * @var float
     */
    private $scale = 0.0;

    /**
     * @var Image
     */
    private $mainImage;

    /**
     * @var array
     */
    private $images;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $source;

    /**
     * @var string
     */
    private $externalSource;

    /**
     * @var bool
     */
    private $available = true;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return CanonicalProduct
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return CanonicalProduct
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return CanonicalCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return CanonicalProduct
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return CanonicalProduct
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return (float)$this->price;
    }

    /**
     * @param float $price
     * @return CanonicalProduct
     */
    public function setPrice($price): CanonicalProduct
    {
        $this->price = (float)$price;
        return $this;
    }

    /**
     * @return float
     */
    public function getScale(): float
    {
        return $this->scale;
    }

    /**
     * @param float $scale
     * @return CanonicalProduct
     */
    public function setScale(float $scale): CanonicalProduct
    {
        $this->scale = $scale;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    /**
     * @param mixed $mainImage
     * @return CanonicalProduct
     */
    public function setMainImage($mainImage)
    {
        $this->mainImage = $mainImage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param mixed $images
     * @return CanonicalProduct
     */
    public function setImages($images)
    {
        $this->images = $images;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return CanonicalProduct
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return CanonicalProduct
     */
    public function setSource(string $source): CanonicalProduct
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvailable()
    {
        return $this->available;
    }

    /**
     * @param mixed $available
     * @return CanonicalProduct
     */
    public function setAvailable($available)
    {
        $this->available = $available;
        return $this;
    }

    /**
     * @return string
     */
    public function getExternalSource(): ?string
    {
        return $this->externalSource;
    }

    /**
     * @param string $externalSource
     *
     * @return CanonicalProduct
     */
    public function setExternalSource(string $externalSource = null): CanonicalProduct
    {
        $this->externalSource = $externalSource;
        return $this;
    }


}