<?php
/**
 * @author kerosin
 */

namespace Catalog\ORM\Entity;


use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;

/**
 * Class ProductPrice
 * @package Shop\Entity
 *
 * @ORM\Entity(repositoryClass="Catalog\ORM\Repository\ProductPriceRepository")
 * @ORM\Table(name="product_price")
 */
class ProductPrice
{
    /**
     * @deprecated
     */
    public const SCALE = 2;

    use BaseMapping;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $value;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $scale;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive = false;

    /**
     * @ORM\OneToOne(targetEntity="Catalog\ORM\Entity\Product", inversedBy="price")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $product;

    public function __construct()
    {
        $this->createdAt = new \DateTime;
    }

    /**
     * Set markup
     *
     * @param string $scale
     *
     * @return ProductPrice
     */
    public function setScale($scale)
    {
        $this->scale = $scale;

        return $this;
    }

    /**
     * Get markup
     *
     * @return string
     */
    public function getScale()
    {
        return $this->scale;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return ProductPrice
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ProductPrice
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set product
     *
     * @param \Catalog\ORM\Entity\Product $product
     *
     * @return ProductPrice
     */
    public function setProduct(\Catalog\ORM\Entity\Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Catalog\ORM\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return ProductPrice
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get a total product price with scale
     *
     * @return float
     */
    public function getTotalPrice()
    {
        if (!$this->value && !$this->scale) {
            return 0.00;
        }

        $base = $this->value;
        $markup = bcdiv($this->scale, 100, self::SCALE);
        $markup = bcmul($base, $markup, self::SCALE);
        $price = bcadd($base, $markup, self::SCALE);

        return $price;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    public function getCurrency()
    {
        return 'USD';
    }
}
