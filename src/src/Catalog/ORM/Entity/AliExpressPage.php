<?php
/**
 * @author Kondaurov
 */

namespace Catalog\ORM\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Kerosin\Doctrine\ORM\BaseMapping;

/**
 * Class AliPage
 * @package Catalog\ORM\Entity
 *
 * @ORM\Entity(repositoryClass="Catalog\ORM\Repository\AliExpressPageRepository")
 * @ORM\Table(indexes={@Index(name="uri_idx", columns={"uri"})})
 */
class AliExpressPage
{
    use BaseMapping;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $uri;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $html;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getUri(): ?string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     *
     * @return self
     */
    public function setUri(string $uri): self
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * @return string
     */
    public function getHtml(): ?string
    {
        return $this->html;
    }

    /**
     * @param string $html
     *
     * @return self
     */
    public function setHtml(string $html): self
    {
        $this->html = $html;
        return $this;
    }
}