<?php
/**
 * @author Kondaurov
 */

namespace Catalog\ORM\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Catalog\ORM\Entity\CategoryAlias;
use Doctrine\ORM\Mapping\Index;
use Kerosin\Doctrine\ORM\MetaTagsMapping;
use Content\Seo\SeoDataProviderInterface;
use Content\ORM\Entity\Slug;
use Content\SlugRoute\Sluggable;

/**
 * Class Category
 * @package Shop\Entity
 *
 * @ORM\Entity(repositoryClass="Catalog\ORM\Repository\CategoryRepository")
 * @ORM\Table(
 *     name="category"
 * )
 */
class Category implements SeoDataProviderInterface, Sluggable
{
    use MetaTagsMapping;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="integer", nullable=true, name="sort")
     */
    private $order;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;


    /**
     * @ORM\OneToMany(targetEntity="Catalog\ORM\Entity\Category", mappedBy="parent", cascade={"remove"})
     * @ORM\JoinColumn(name="id", referencedColumnName="parent_id", onDelete="CASCADE")
     */
    private $children;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @ORM\ManyToMany(targetEntity="Catalog\ORM\Entity\FacetCategory", inversedBy="categories", cascade={"remove"})
     * @ORM\JoinTable(
     *     joinColumns={@ORM\JoinColumn(onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(onDelete="CASCADE")}
     * )
     */
    private $facets;

    /**
     * @ORM\OneToOne(targetEntity="Catalog\ORM\Entity\CategoryAlias", mappedBy="category")
     */
    private $mappingAlias;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $enabled = true;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isAutoUpdate = false;

    /**
     * @var ArrayCollection|null
     */
    private $slugs;

    /**
     * @var bool
     */
    private $issetProduct;

    /**
     * @var int
     */
    private $depth;

    /**
     * @var ExternalCategoriesMap[]
     *
     * @ORM\OneToMany(targetEntity="Catalog\ORM\Entity\ExternalCategoriesMap", mappedBy="category")
     */
    private $externalReferences;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return Category
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Category
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set parent
     *
     * @param Category $parent
     *
     * @return Category
     */
    public function setParent(Category $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->externalReferences = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    /**
     * Add child
     *
     * @param \Catalog\ORM\Entity\Category $child
     *
     * @return Category
     */
    public function addChild(\Catalog\ORM\Entity\Category $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Catalog\ORM\Entity\Category $child
     */
    public function removeChild(\Catalog\ORM\Entity\Category $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    public function setChildren(array $children)
    {
        $this->children = new ArrayCollection($children);
        return $this;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     *
     * @return Category
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Add facet
     *
     * @param \Catalog\ORM\Entity\FacetCategory $facet
     *
     * @return Category
     */
    public function addFacet(\Catalog\ORM\Entity\FacetCategory $facet)
    {
        $this->facets[] = $facet;

        return $this;
    }

    /**
     * Remove facet
     *
     * @param \Catalog\ORM\Entity\FacetCategory $facet
     */
    public function removeFacet(\Catalog\ORM\Entity\FacetCategory $facet)
    {
        $this->facets->removeElement($facet);
    }

    /**
     * Get facets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFacets()
    {
        return $this->facets;
    }

    /**
     * @inheritdoc
     */
    public function getMetaTitle()
    {
        return $this->metaTitle ?? $this->title;
    }

    /**
     * Set mappingAlias
     *
     * @param \Catalog\ORM\Entity\CategoryAlias $mappingAlias
     *
     * @return Category
     */
    public function setMappingAlias(\Catalog\ORM\Entity\CategoryAlias $mappingAlias = null)
    {
        $this->mappingAlias = $mappingAlias;

        return $this;
    }

    /**
     * Get mappingAlias
     *
     * @return \Catalog\ORM\Entity\CategoryAlias
     */
    public function getMappingAlias()
    {
        if (!$this->mappingAlias) {
            $this->mappingAlias = new CategoryAlias();
            $this->mappingAlias->setCategory($this);
        }

        return $this->mappingAlias;
    }

    /**
     * @return bool
     */
    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): Category
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsAutoUpdate(): ?bool
    {
        return $this->isAutoUpdate;
    }

    /**
     * @param bool $isAutoUpdate
     *
     * @return Category
     */
    public function setIsAutoUpdate(bool $isAutoUpdate): Category
    {
        $this->isAutoUpdate = $isAutoUpdate;
        return $this;
    }

    /**
     * @return ExternalCategoriesMap[]
     */
    public function getExternalReferences()
    {
        return $this->externalReferences;
    }

    /**
     * @param ExternalCategoriesMap[] $externalReferences
     *
     * @return Category
     */
    public function setExternalReferences(array $externalReferences): Category
    {
        $this->externalReferences = new ArrayCollection($externalReferences);
        return $this;
    }

    /**
     * @param ExternalCategoriesMap $externalReference
     *
     * @return Category
     */
    public function addExternalReference(ExternalCategoriesMap $externalReference): Category
    {
        $this->externalReferences[] = $externalReference;
        return $this;
    }

    public function hasExternalReferenceById($id, $source = Source::EPN_YML)
    {
        foreach ($this->getExternalReferences() as $externalReference) {
            if ($externalReference->getExternalId() == $id && $externalReference->getSource() == $source) {
                return true;
            }
        }

        return false;
    }

    /**
     * Interface's methods
     */

    public function getDepth()
    {
        return $this->depth;
    }

    public function setDepth($depth = 0)
    {
        $this->depth = $depth;
        return $this->depth;
    }

    /**
     * @return bool
     */
    public function issetProducts(): ?bool
    {
        return $this->issetProduct === true;
    }

    /**
     * @param bool $issetProduct
     */
    public function setIssetProducts(bool $issetProduct)
    {
        $this->issetProduct = $issetProduct;
        return $this;
    }

    public function getSlugType(): string
    {
        return \Content\ORM\Entity\Slug::TARGET_CATEGORY;
    }

    public function getSlugAttribute(): string
    {
        return 'title';
    }

    public function getIdentifier(): int
    {
        return $this->id;
    }

    public function getSlugs()
    {
        return $this->slugs;
    }

    public function setSlugs($slugs)
    {
        $this->slugs = new ArrayCollection($slugs);
    }

    public function addSlug(\Content\ORM\Entity\Slug $slug)
    {
        if (!$this->slugs instanceof Collection) {
            $this->slugs = new ArrayCollection();
        }

        $this->slugs->add($slug);
        return $this;
    }


}