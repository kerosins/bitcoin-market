<?php
/**
 * @author Kondaurov
 */

namespace Catalog\ORM\Entity;


use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;

/**
 * Class ProductFile
 * @package Catalog\ORM\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="product_yml_file")
 * @ORM\HasLifecycleCallbacks()
 */
class ProductYmlFile
{
    use BaseMapping, UpdateTimestamps;

    /**
     * Statuses
     */
    public const
        STATUS_DONE = 0,
        STATUS_PROCESS = 1,
        STATUS_CANCELED = 2;

    /**
     * Transformers
     */
    public const
        TRANSFORMER_EPN = 'epn';

    /**
     * Source path
     *
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $initiallyPath;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $sourcePath;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $transformer;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="file_offset")
     */
    private $offset = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $status = self::STATUS_PROCESS;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getInitiallyPath(): ?string
    {
        return $this->initiallyPath;
    }

    /**
     * @param string $initiallyPath
     *
     * @return ProductYmlFile
     */
    public function setInitiallyPath(string $initiallyPath): ProductYmlFile
    {
        $this->initiallyPath = $initiallyPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getSourcePath(): ?string
    {
        return $this->sourcePath;
    }

    /**
     * @param string $sourcePath
     *
     * @return ProductYmlFile
     */
    public function setSourcePath(string $sourcePath = null): ProductYmlFile
    {
        $this->sourcePath = $sourcePath;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransformer(): ?string
    {
        return $this->transformer;
    }

    /**
     * @param string $transformer
     *
     * @return ProductYmlFile
     */
    public function setTransformer(string $transformer = null): ProductYmlFile
    {
        $this->transformer = $transformer;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffset(): ?int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     *
     * @return ProductYmlFile
     */
    public function setOffset(int $offset = 0): ProductYmlFile
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return ProductYmlFile
     */
    public function setStatus(int $status): ProductYmlFile
    {
        $this->status = $status;
        return $this;
    }
}