<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;

/**
 * Class ExternalCategoriesMap
 * @package Catalog\ORM\Entity
 *
 * @ORM\Entity(repositoryClass="Catalog\ORM\Repository\ExternalCategoriesMapRepository")
 * @ORM\Table()
 */
class ExternalCategoriesMap
{
    use BaseMapping;

    /**
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\Category", cascade={"remove", "persist"}, inversedBy="externalReferences")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @ORM\Column(type="string")
     */
    private $externalId;

    /**
     * @ORM\Column(type="string")
     */
    private $source;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Set externalId
     *
     * @param integer $externalId
     *
     * @return ExternalCategoriesMap
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return integer
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set source
     *
     * @param integer $source
     *
     * @return ExternalCategoriesMap
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return integer
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set category
     *
     * @param \Catalog\ORM\Entity\Category $category
     *
     * @return ExternalCategoriesMap
     */
    public function setCategory(\Catalog\ORM\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Catalog\ORM\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }
}
