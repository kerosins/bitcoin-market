<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Catalog\Order\Component\TotalPrice;
use Catalog\ORM\Entity\OrderStatus;
use Catalog\ORM\Entity\ProductPrice;

/**
 * Class OrderProduct
 * @package Shop\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="order_position")
 */
class OrderPosition
{
    public const
        STATUS_PROCESS = 0,
        STATUS_SHIPPING = 10,

        STATUS_DONE = 20,
        STATUS_CANCEL = 30;

    public static $statusString = [
        self::STATUS_PROCESS => 'Processing',
        self::STATUS_SHIPPING => 'Shipped',
        self::STATUS_DONE => 'Done',
        self::STATUS_CANCEL => 'Cancelled'
    ];

    use BaseMapping;

    /**
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\Order", inversedBy="orderProducts")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $order;

    /**
     * Price at which sell product in cryptocurrency
     *
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    private $publicPrice = 0.00;

    /**
     * @ORM\Column(type="decimal", scale=7, nullable=true)
     */
    private $currencyRate;

    /**
     * Inner price at which buy product from supplier (USD)
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $innerPrice = 0.00;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $innerCurrency = 'USD';

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity = 1;

    /**
     * @ORM\Column(type="integer")
     */
    private $status = self::STATUS_PROCESS;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true, scale=2)
     */
    private $shipmentTax = 0.00;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $trackCode;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Catalog\ORM\Entity\Dispute", mappedBy="orderPosition")
     */
    private $disputes;

    /**
     * @var AliExpressDeliveryFee
     *
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\AliExpressDeliveryFee")
     * @ORM\JoinColumn(onDelete="SET NULL", nullable=true)
     */
    private $aliExpressDeliveryFee;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->disputes = new ArrayCollection();
    }


    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return OrderPosition
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return OrderPosition
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set product
     *
     * @param \Catalog\ORM\Entity\Product $product
     *
     * @return OrderPosition
     */
    public function setProduct(\Catalog\ORM\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Catalog\ORM\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set order
     *
     * @param \Catalog\ORM\Entity\Order $order
     *
     * @return OrderPosition
     */
    public function setOrder(\Catalog\ORM\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Catalog\ORM\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set publicPrice
     *
     * @param string $publicPrice
     *
     * @return OrderPosition
     */
    public function setPublicPrice($publicPrice)
    {
        $this->publicPrice = $publicPrice;

        return $this;
    }

    /**
     * Get publicPrice
     *
     * @return float
     */
    public function getPublicPrice()
    {
        return $this->publicPrice;
    }

    /**
     * Set publicRateValue
     *
     * @param string $currencyRate
     *
     * @return OrderPosition
     */
    public function setCurrencyRate($currencyRate)
    {
        $this->currencyRate = $currencyRate;

        return $this;
    }

    /**
     * Get publicRateValue
     *
     * @return string
     */
    public function getCurrencyRate()
    {
        return $this->currencyRate;
    }

    /**
     * Set innerPrice
     *
     * @param string $innerPrice
     *
     * @return OrderPosition
     */
    public function setInnerPrice($innerPrice)
    {
        $this->innerPrice = $innerPrice;

        return $this;
    }

    /**
     * Get innerPrice
     *
     * @return float
     */
    public function getInnerPrice()
    {
        return $this->innerPrice;
    }

    /**
     * @param string|null $status
     * @return $this
     */
    public function setStatus($status = null)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed|string
     */
    public function getReadableStatus()
    {
        return static::$statusString[$this->status];
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return OrderPosition
     */
    public function setComment(string $comment = null): OrderPosition
    {
        $this->comment = $comment;
        return $this;
    }



    /**
     * Set trackCode
     *
     * @param string $trackCode
     *
     * @return OrderPosition
     */
    public function setTrackCode($trackCode)
    {
        $this->trackCode = $trackCode;

        return $this;
    }

    /**
     * Get trackCode
     *
     * @return string
     */
    public function getTrackCode()
    {
        return $this->trackCode;
    }

    /**
     * @return string
     */
    public function getInnerCurrency(): ?string
    {
        return $this->innerCurrency;
    }

    /**
     * @param string $innerCurrency
     *
     * @return OrderPosition
     */
    public function setInnerCurrency(string $innerCurrency): OrderPosition
    {
        $this->innerCurrency = $innerCurrency;
        return $this;
    }

    /**
     * Gets public price of position without tax
     *
     * @return float
     */
    public function getTotalPublicPrice()
    {
        return bcmul($this->publicPrice, $this->quantity, ProductPrice::SCALE);
    }

    /**
     * @return float
     */
    public function getTotalPriceWithTax()
    {
        return bcadd($this->getTotalPublicPrice(), $this->shipmentTax, ProductPrice::SCALE);
    }

    /**
     * @return float
     */
    public function getShipmentTax(): ?float
    {
        return $this->shipmentTax;
    }

    /**
     * @param float $shipmentTax
     *
     * @return OrderPosition
     */
    public function setShipmentTax(float $shipmentTax): OrderPosition
    {
        $this->shipmentTax = $shipmentTax;
        return $this;
    }

    /**
     * Add dispute.
     *
     * @param \Catalog\ORM\Entity\Dispute $dispute
     *
     * @return OrderPosition
     */
    public function addDispute(\Catalog\ORM\Entity\Dispute $dispute)
    {
        $this->disputes[] = $dispute;
        $dispute->setOrderPosition($this);
        return $this;
    }

    /**
     * Remove dispute.
     *
     * @param \Catalog\ORM\Entity\Dispute $dispute
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDispute(\Catalog\ORM\Entity\Dispute $dispute)
    {
        return $this->disputes->removeElement($dispute);
    }

    /**
     * Get disputes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDisputes()
    {
        return $this->disputes;
    }

    /**
     * @return AliExpressDeliveryFee|null
     */
    public function getAliExpressDeliveryFee()
    {
        return $this->aliExpressDeliveryFee;
    }

    /**
     * @param AliExpressDeliveryFee $aliExpressDeliveryFee
     * @return OrderPosition
     */
    public function setAliExpressDeliveryFee(AliExpressDeliveryFee $aliExpressDeliveryFee = null): OrderPosition
    {
        $this->aliExpressDeliveryFee = $aliExpressDeliveryFee;
        return $this;
    }
}
