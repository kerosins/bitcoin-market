<?php
/**
 * @author Kondaurov
 */

namespace Catalog\ORM\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;

/**
 * Class HttpProxy
 * @package Catalog\ORM\Entity
 *
 * @ORM\Entity(repositoryClass="Catalog\ORM\Repository\HttpProxyRepository")
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class HttpProxy
{
    use BaseMapping, UpdateTimestamps;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $ip;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $username;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;

    /**
     * @var ?int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $port;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isBanned = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $onlyRuntime = false;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     *
     * @return HttpProxy
     */
    public function setIp(string $ip): HttpProxy
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return HttpProxy
     */
    public function setUsername(string $username): HttpProxy
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return HttpProxy
     */
    public function setPassword(string $password): HttpProxy
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsBanned(): ?bool
    {
        return $this->isBanned;
    }

    /**
     * @param bool $isBanned
     *
     * @return HttpProxy
     */
    public function setIsBanned(bool $isBanned): HttpProxy
    {
        $this->isBanned = $isBanned;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsOnlyRuntime(): ?bool
    {
        return $this->onlyRuntime;
    }

    /**
     * @param bool $onlyRuntime
     *
     * @return HttpProxy
     */
    public function setOnlyRuntime(bool $onlyRuntime): HttpProxy
    {
        $this->onlyRuntime = $onlyRuntime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param mixed $port
     *
     * @return HttpProxy
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return string
     */
    public function getProxyString($schema = 'http'): string
    {
        $proxy = sprintf(
            '%s://%s:%s@%s',
            $schema,
            $this->username,
            $this->password,
            $this->ip
        );

        if ($this->port) {
            $proxy .= ':' . $this->port;
        }

        return $proxy;
    }
}