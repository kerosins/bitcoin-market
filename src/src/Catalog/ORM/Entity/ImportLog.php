<?php
/**
 * @author Kerosin
 */

namespace Catalog\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;

/**
 * Class ImportLog
 * @package Catalog\ORM\Entity
 *
 * @ORM\Entity()
 * @ORM\Table()
 *
 */
class ImportLog
{
    public const STATUS_COMPLETED = 0;

    public const STATUS_IN_PROGRESS = 1;

    public const STATUS_ERROR = 2;

    use BaseMapping;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $finishedAt;

    /**
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\Column(type="string")
     */
    private $source;

    /**
     * @ORM\Column(type="string")
     */
    private $sourceUri;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status = self::STATUS_IN_PROGRESS;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $total = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $success = 0;

    /**
     * @ORM\Column(type="string")
     */
    private $logFile;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Set finishedAt
     *
     * @param \DateTime $finishedAt
     *
     * @return ImportLog
     */
    public function setFinishedAt($finishedAt)
    {
        $this->finishedAt = $finishedAt;

        return $this;
    }

    /**
     * Get finishedAt
     *
     * @return \DateTime
     */
    public function getFinishedAt()
    {
        return $this->finishedAt;
    }

    /**
     * Set source
     *
     * @param string $source
     *
     * @return ImportLog
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set sourceUri
     *
     * @param string $sourceUri
     *
     * @return ImportLog
     */
    public function setSourceUri($sourceUri)
    {
        $this->sourceUri = $sourceUri;

        return $this;
    }

    /**
     * Get sourceUri
     *
     * @return string
     */
    public function getSourceUri()
    {
        return $this->sourceUri;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return ImportLog
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set logFile
     *
     * @param string $logFile
     *
     * @return ImportLog
     */
    public function setLogFile($logFile)
    {
        $this->logFile = $logFile;

        return $this;
    }

    /**
     * Get logFile
     *
     * @return string
     */
    public function getLogFile()
    {
        return $this->logFile;
    }

    /**
     * Set user
     *
     * @param \User\Entity\User $user
     *
     * @return ImportLog
     */
    public function setUser(\User\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set total
     *
     * @param integer $total
     *
     * @return ImportLog
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return integer
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set success
     *
     * @param integer $success
     *
     * @return ImportLog
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get success
     *
     * @return integer
     */
    public function getSuccess()
    {
        return $this->success;
    }
}
