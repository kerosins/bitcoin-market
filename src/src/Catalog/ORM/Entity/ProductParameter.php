<?php
/**
 * ProductParameter class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Catalog\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;

/**
 * Class ProductParameter
 * @package Shop\Entity
 *
 * @ORM\Entity(repositoryClass="Catalog\ORM\Repository\ProductParameterRepository")
 * @ORM\Table(name="product_parameter")
 */
class ProductParameter
{
    use BaseMapping;

    /**
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\Product", inversedBy="parameters")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\FacetCategory")
     * @ORM\JoinColumn(name="facet_id", referencedColumnName="id")
     */
    private $facet;

    /**
     * @ORM\Column(type="integer")
     */
    private $value;

    /**
     * @ORM\Column(type="integer")
     */
    private $productId;

    private $toDelete = false;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return ProductParameter
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ProductParameter
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set product
     *
     * @param \Catalog\ORM\Entity\Product $product
     *
     * @return ProductParameter
     */
    public function setProduct(\Catalog\ORM\Entity\Product $product = null)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * Get product
     *
     * @return \Catalog\ORM\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set facet
     *
     * @param \Catalog\ORM\Entity\FacetCategory $facet
     *
     * @return ProductParameter
     */
    public function setFacet(\Catalog\ORM\Entity\FacetCategory $facet = null)
    {
        $this->facet = $facet;
        return $this;
    }

    /**
     * Get facet
     *
     * @return \Catalog\ORM\Entity\FacetCategory
     */
    public function getFacet()
    {
        return $this->facet;
    }

    public function markToDelete()
    {
        $this->toDelete = true;
    }

    public function needDelete()
    {
        return $this->toDelete;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     *
     * @return ProductParameter
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer
     */
    public function getProductId()
    {
        return $this->productId;
    }
}
