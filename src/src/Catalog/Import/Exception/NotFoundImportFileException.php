<?php
/**
 * @author Kerosin
 */

namespace Catalog\Import\Exception;


class NotFoundImportFileException extends \Exception
{
    public function __construct($filePath)
    {
        $message = "{$filePath} not found";
        parent::__construct($message, 0, null);
    }
}