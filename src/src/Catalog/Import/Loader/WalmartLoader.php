<?php
/**
 * @author Kerosin
 */

namespace Catalog\Import\Loader;

use GuzzleHttp\Client;
use Catalog\ORM\Entity\CanonicalCategory;
use Catalog\ORM\Entity\CanonicalProduct;
use Catalog\ORM\Entity\Source;
use Catalog\Import\Exception\LoaderException;
use Kerosin\Component\Image;

class WalmartLoader implements LoaderInterface
{
    /**
     * Uri to walmart api
     *
     * @var string
     */
    private $apiUri;

    /**
     * Walmart api key
     *
     * @var string
     */
    private $apiKey;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var \Predis\Client
     */
    private $redis;

    /**
     * @var CanonicalCategory[]
     */
    private $categories;

    /**
     * @var array
     */
    private $options = [];

    public function __construct(
        string $uri,
        string $key,
        \Predis\Client $redis
    ) {
        $this->apiUri = $uri;
        $this->apiKey = $key;
        $this->redis = $redis;
    }


    /**
     * @param array $options
     * - category - ID of walmart category
     * - flushState - flush previous downloads
     */
    public function run(array $options = [])
    {
        $this->client = new Client(['base_uri' => $this->apiUri]);
        $this->options = $options;
        $this->loadCategories();
    }

    /**
     * @param array $data
     * @return CanonicalProduct
     */
    private function toCanonicalProduct(array $data)
    {
        $product = new CanonicalProduct();

        $url = $data['productUrl'];
        $url = explode('?l=', $url);
        $url = end($url);
        $url = urldecode($url);

        $image = new Image($data['largeImage'] ?? '');

        $product
            ->setId($data['itemId'])
            ->setSource(Source::WALMART)
            ->setDescription($data['shortDescription'] ?? '')
            ->setTitle($data['name'])
            ->setMainImage($image)
            ->setCategory($this->categories[$data['categoryNode']])
            ->setUrl($url);

        return $product;
    }

    public function products()
    {
        if (!$this->client) {
            throw new LoaderException('Loader was not started');
        }

        $response = null;
        $category = $this->options['category'] ?? null;

        //generate redis key
        $key = Source::WALMART . ':import';
        if ($category) {
            $key .= ':' . $category;
        }

        //try load previous state
        if (isset($this->options['flushState']) && $this->options['flushState'] !== true) {
            if (($savedUri = $this->redis->get($key)) !== null) {
                $response = $this->request($savedUri, false);
            }
        } else {
            $this->redis->del([$key]);
        }

        //if previous state has been not found or state was flushed
        if (!$response) {
            $params = $category ? ['category' => $category] : [];
            $response = $this->request('/v1/paginated/items', $params);
        }

        if (!$response) {
            return;
        }

        $nextPage = $response['nextPage'];
        $items = $response['items'];

        while (($item = array_shift($items)) !== null) {
            if ($this->isValidProduct($item)) {
                yield $this->toCanonicalProduct($item);
            }

            if (!$items && $nextPage) {
                echo "Request to $nextPage" . PHP_EOL;

                $this->redis->set($key, $nextPage);

                $response = $this->request($nextPage, false);
                if (!$response) {
                    return;
                }

                $items = $response['items'];
                $nextPage = $response['nextPage'] ?? null;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function clear()
    {
        $this->client = null;
        $this->options = [];
        $this->categories = [];
    }

    /**
     * Check if external product is valid
     *
     * @param array $product
     * @return bool
     */
    private function isValidProduct(array $product)
    {
        //check if product category is valid
        if (!isset($product['categoryNode']) || !isset($this->categories[$product['categoryNode']])) {
            return false;
        }

        return true;
    }

    /**
     * Load walmart categories
     *
     * @return void
     */
    private function loadCategories()
    {
        $response = $this->request('/v1/taxonomy');

        $populate = function ($category) use (&$populate) {
            $canonical = new CanonicalCategory();
            $canonical
                ->setId($category['id'])
                ->setTitle($category['name'])
                ->setSource(Source::WALMART)
            ;

            $this->categories[$category['id']] = $canonical;

            if (!empty($category['children'])) {
                foreach ($category['children'] as $child) {
                    $populate($child);
                }
            }
        };

        foreach ($response['categories'] as $category) {
            $populate($category);
        }
    }

    /**
     * @param string $path
     * @param array|bool $params
     * @return array
     */
    private function request(string $path, $params = [])
    {
        if (is_array($params) || $params === null) {
            $params = array_merge([
                'apiKey' => $this->apiKey,
                'format' => 'json'
            ], $params);
        }

        $response = $this->client->request(
            'GET',
            $path,
            ($params !== false ? ['query' => $params] : [])
        );

        $body = (string)$response->getBody();
        return json_decode($body, true);
    }

    public function __destruct()
    {
        $this->clear();
    }
}