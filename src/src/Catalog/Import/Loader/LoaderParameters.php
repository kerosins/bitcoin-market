<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Import\Loader;


class LoaderParameters
{
    /**
     * Path to import file
     *
     * @var string
     */
    private $filePath;

    /**
     * Transformer of imported file
     *
     * @var string
     */
    private $transformer;

    /**
     * @var int
     */
    private $productFileId;

    /**
     * Skip data from table
     *
     * @var bool
     */
    private $force;

    public function __construct(
        string $filePath = null,
        string $transformer = null,
        int $productFileId = null,
        bool $force = null
    ) {
        $this->filePath = $filePath;
        $this->transformer = $transformer;
        $this->productFileId = $productFileId;
        $this->force = $force;
    }

    /**
     * @return string
     */
    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @param string $filePath
     *
     * @return LoaderParameters
     */
    public function setFilePath(?string $filePath): LoaderParameters
    {
        $this->filePath = $filePath;
        return $this;
    }

    /**
     * @return string
     */
    public function getTransformer(): ?string
    {
        return $this->transformer;
    }

    /**
     * @param string $transformer
     *
     * @return LoaderParameters
     */
    public function setTransformer(?string $transformer): LoaderParameters
    {
        $this->transformer = $transformer;
        return $this;
    }

    /**
     * @return int
     */
    public function getProductFileId(): ?int
    {
        return $this->productFileId;
    }

    /**
     * @param int $productFileId
     *
     * @return LoaderParameters
     */
    public function setProductFileId(?int $productFileId): LoaderParameters
    {
        $this->productFileId = $productFileId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isForce(): ?bool
    {
        return $this->force;
    }

    /**
     * @param bool $force
     *
     * @return LoaderParameters
     */
    public function setForce(?bool $force): LoaderParameters
    {
        $this->force = (bool)$force;
        return $this;
    }


}