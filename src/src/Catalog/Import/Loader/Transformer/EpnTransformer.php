<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Import\Loader\Transformer;


use Catalog\ORM\Entity\CanonicalCategory;
use Catalog\ORM\Entity\CanonicalProduct;
use Catalog\ORM\Entity\Source;
use Kerosin\Component\Image;

class EpnTransformer extends BaseTransformer
{
    public const NAME = 'epn';

    public function toCanonical(array $data): CanonicalProduct
    {
        $canonical = new CanonicalProduct();

        $category = $this->categories->filter(function (CanonicalCategory $category) use ($data) {
            return $category->getId() == $data['categoryId'];
        });

        $category = $category->first();
        $canonical->setCategory($category);

        $title = trim($data['name'] ?? '');
        $title = str_replace(['\r\n', '\r', '\n', '\b'], '', $title);

        $canonical->setId($data['id']);
        $canonical->setTitle($title);

        if (!empty($data['picture'])) {
            $image = new Image();
            $image->setUrl($data['picture']);
            $image
                ->setAlt($title)
                ->setTitle($title);

            $canonical->setMainImage($image);
        }
        $canonical
            ->setPrice($data['price'] ?? 0.0)
            ->setUrl(
                sprintf('https://www.aliexpress.com/item/deep-link/%d.html', $data['id'])
            )
            ->setSource(Source::EPN_YML)
            ->setExternalSource(Source::EXTERNAL_SOURCE_ALIEXPRESS)
            ->setAvailable($data['available']);
        return $canonical;
    }

    public function getName(): string
    {
        return 'epn';
    }
}