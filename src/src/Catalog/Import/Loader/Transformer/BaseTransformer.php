<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Import\Loader\Transformer;


use Doctrine\Common\Collections\Collection;
use Catalog\ORM\Entity\CanonicalCategory;
use Catalog\ORM\Entity\CanonicalProduct;

abstract class BaseTransformer
{
    /**
     * @var Collection|CanonicalCategory[]
     */
    protected $categories;

    public function setCategories(Collection $categories)
    {
        $this->categories = $categories;
        return $categories;
    }

    abstract public function toCanonical(array $data) : CanonicalProduct;

    abstract public function getName() : string;
}