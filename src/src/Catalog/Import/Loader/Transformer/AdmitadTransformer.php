<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Import\Loader\Transformer;


use Catalog\ORM\Entity\CanonicalCategory;
use Catalog\ORM\Entity\CanonicalProduct;
use Catalog\ORM\Entity\Source;
use Kerosin\Component\Image;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class AdmitadTransformer extends BaseTransformer
{
    /**
     * @var PropertyAccessor
     */
    private $accessor;

    /**
     * Map of key in xml
     * @var array xml-key => canonical-key
     */
    private static $map = [
        'id' => 'id',
        'categoryId' => 'category',
        'title' => 'title',
        'image' => 'mainImage',
        'url' => 'url',
        'price' => 'price'
    ];

    public function __construct()
    {
        $this->accessor = new PropertyAccessor();
    }

    public function toCanonical(array $data): CanonicalProduct
    {
        $canonical = new CanonicalProduct();

        foreach ($data as $key => $value) {
            $canonicalProperty = $this->accessor->getValue(static::$map, "[$key]");
            $value = trim($value);
            if ($canonicalProperty) {

                switch ($canonicalProperty) {
                    case 'category':
                        $category = $this->categories->filter(function (CanonicalCategory $category) use ($value) {
                            return $category->getId() == $value;
                        });
                        $value = $category->first();
                        break;
                    case 'url':
                        $value = explode('&ulp=', $value);
                        $value = urldecode($value[1]);
                        break;
                    case 'mainImage':
                        $image = new Image();
                        $image->setUrl($value);

                        $value = $image;
                        break;
                }

                $this->accessor->setValue($canonical, $canonicalProperty, $value);
            }
        }

        $canonical->setSource(Source::EPN_YML);

        return $canonical;
    }
}