<?php
/**
 * @author Kerosin
 */

namespace Catalog\Import\Loader;


use Catalog\ORM\Entity\ProductYmlFile;
use Doctrine\Common\Collections\ArrayCollection;
use Catalog\ORM\Entity\CanonicalCategory;
use Catalog\ORM\Entity\CanonicalProduct;
use Catalog\ORM\Entity\Source;
use Catalog\Import\Exception\NotFoundImportFileException;
use Catalog\Import\Loader\Transformer\AdmitadTransformer;
use Catalog\Import\Loader\Transformer\BaseTransformer;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class XmlLoader implements LoaderInterface
{
    /**
     * @var \XMLReader
     */
    private $reader;

    /**
     * @var ArrayCollection
     */
    private $categories;

    /**
     * @var PropertyAccessor
     */
    private $accessor;

    /**
     * @var BaseTransformer
     */
    private $transformer;

    /**
     * Minimal product price
     */
    private const MIN_PRODUCT_PRICE = 100;

    /**
     * @var int
     */
    private $offset = 0;

    /**
     * @var ProductYmlFile
     */
    private $productYmlFile;

    public function run()
    {
        $productFile = $this->productYmlFile;
        $this->offset = $this->productYmlFile->getOffset();

        $accessor = new PropertyAccessor();
        $this->accessor = $accessor;

        $filePath = $productFile->getSourcePath();

        if (!file_exists($filePath)) {
            throw new NotFoundImportFileException($filePath);
        }

        $this->reader = new \XMLReader();
        $this->reader->open($filePath);

        $this->loadCategories();

        //moves to offset
        $destinationOffset = $productFile->getOffset();
        if (0 === $destinationOffset) {
            return;
        }

        while ($destinationOffset > 0 && $this->reader->read()) {
            if (self::isEndElement($this->reader, 'offer')) {
                $destinationOffset--;
            }
        }
    }

    /**
     * @return \Generator
     */
    public function products()
    {
        $reader = $this->reader;
        try {
            while ($reader->read()) {
                if (self::isBeginElement($reader, 'offer')) {
                    $data = [];
                    $data['id'] = $reader->getAttribute('id');
                    $data['available'] = $reader->getAttribute('available') ?? true;

                    while ($reader->read() && !self::isEndElement($reader, 'offer')) {
                        $eData = self::readElement($reader);
                        $data[$eData['key']] = $eData['value'];
                        $reader->read();
                    }

                    $canonical = $this->transformer->toCanonical($data);

                    $this->offset++;
                    $this->productYmlFile->setOffset($this->offset);
                    if ($this->isAllowed($canonical)) {
                        yield $canonical;
                    }
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @return BaseTransformer
     */
    public function getTransformer(): ?BaseTransformer
    {
        return $this->transformer;
    }

    /**
     * @param BaseTransformer $transformer
     * @return XmlLoader
     */
    public function setTransformer(BaseTransformer $transformer)
    {
        $this->transformer = $transformer;
        return $this;
    }

    /**
     * @inheritdoc
     *
     * @param ProductYmlFile $productYmlFile
     *
     * @return $this
     */
    public function setProductFile(ProductYmlFile $productYmlFile)
    {
        $this->productYmlFile = $productYmlFile;
        return $this;
    }

    /**
     * @return ProductYmlFile
     */
    public function getProductFile(): ProductYmlFile
    {
        return $this->productYmlFile;
    }

    /**
     * Read are categories from xml
     *
     * @return void
     */
    private function loadCategories()
    {
        $reader = $this->reader;
        $categories = [];

        while ($reader->read()) {
            if (static::isBeginElement($reader, 'category')) {
                $name = $reader->getAttribute('en');
                $id = $reader->getAttribute('id');
                $parentId = $reader->getAttribute('parentId') ?? 0;

                if (!$name) {
                    $reader->read();
                    $name = $reader->value;
                }

                $categories[$id] = [
                    'name' => $name,
                    'parent' => $parentId
                ];
            }

            if (static::isEndElement($reader, 'categories')) {
                break;
            }
        }

        $this->categories = array_map(function ($item, $id) {
            $category = new CanonicalCategory();
            $category->setId($id);
            $category->setTitle($item['name']);
            $category->setParent($item['parent']);
            $category->setSource(Source::EPN_YML);

            return $category;
        }, $categories, array_keys($categories));

        foreach ($this->categories as $category) {
            /** @var CanonicalCategory $category */
            $parent = $category->getParent();
            if ($parent) {
                $parent = array_filter($this->categories, function (CanonicalCategory $category) use ($parent) {
                    return $category->getId() === $parent;
                });

                $category->setParent(reset($parent));
            }
        }

        $this->categories = new ArrayCollection($this->categories);
        $this->transformer->setCategories($this->categories);
    }

    /**
     * Checks if it product allowed for follow keep
     *
     * @param CanonicalProduct $product
     *
     * @return bool
     */
    private function isAllowed(CanonicalProduct $product): bool
    {
        return $product->getPrice() >= self::MIN_PRODUCT_PRICE;
    }

    /**
     * Check if is start element
     *
     * @param \XMLReader $reader
     * @param $elementName
     * @return bool
     */
    private static function isBeginElement(\XMLReader $reader, $elementName): bool
    {
        return \XMLReader::ELEMENT === $reader->nodeType && $elementName === $reader->name;
    }

    /**
     * Check if is end element
     *
     * @param \XMLReader $reader
     * @param $elementName
     * @return bool
     */
    private static function isEndElement(\XMLReader $reader, $elementName): bool
    {
        return \XMLReader::END_ELEMENT === $reader->nodeType && $elementName === $reader->name;
    }

    /**
     * @param \XMLReader $reader
     *
     * @return array
     */
    private static function readElement(\XMLReader $reader): array
    {
        $key = '';
        $value = '';

        do {
            if ($reader->nodeType === \XMLReader::ELEMENT) {
                $key = $reader->name;
            } elseif ($reader->nodeType === \XMLReader::TEXT) {
                $value = $reader->value;
            } elseif ($reader->nodeType === \XMLReader::END_ELEMENT) {
                break;
            }
        } while ($reader->read());

        return ['key' => $key, 'value' => $value];
    }
}