<?php
/**
 * @author Kerosin
 */

namespace Catalog\Import\Loader;

use Catalog\Import\Loader\Transformer\BaseTransformer;
use Catalog\ORM\Entity\ProductYmlFile;

interface LoaderInterface
{
    /**
     * Initialize and run load
     * @return mixed
     */
    public function run();

    /**
     * load products
     *
     * @return \Generator
     */
    public function products();

    /**
     * @param ProductYmlFile $productYmlFile
     *
     * @return $this
     */
    public function setProductFile(ProductYmlFile $productYmlFile);

    /**
     * @return ProductYmlFile
     */
    public function getProductFile(): ProductYmlFile;

    /**
     * @return BaseTransformer|null
     */
    public function getTransformer(): ?BaseTransformer;

    /**
     * @param BaseTransformer $transformer
     *
     * @return $this
     */
    public function setTransformer(BaseTransformer $transformer);
}