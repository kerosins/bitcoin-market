<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Import\Loader;

use Catalog\Import\Exception\LoaderFactoryException;
use Catalog\Import\Loader\Transformer\BaseTransformer;
use Catalog\ORM\Entity\ProductYmlFile;
use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Doctrine\ORM\EntityRepository;

class LoaderFactory
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var array
     */
    private $transformers;

    /**
     * @var array
     */
    private $loaders;

    /**
     * @var string
     */
    private $dataPath;

    public function __construct(
        EntityManagerInterface $entityManager,
        string $dataPath,
        array $transformers,
        array $loaders
    )
    {
        $this->entityManager = $entityManager;
        $this->dataPath = $dataPath;
        $this->transformers = $transformers;
        $this->loaders = $loaders;
    }

    /**
     * Builds loader
     *
     * @param LoaderParameters $parameters
     *
     * @return LoaderInterface
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function build(LoaderParameters $parameters)
    {
        $productFile = $this->prepareFileObject($parameters);

        $transformerName = $productFile->getTransformer() ?? $parameters->getTransformer();
        $transformer = $this->selectTransformer($transformerName);
        $productFile->setTransformer($transformerName);

        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(ProductYmlFile::class);
        $repository->save($productFile);

        $loader = $this->selectLoader($productFile->getSourcePath());
        $loader
            ->setTransformer($transformer)
            ->setProductFile($productFile);

        return $loader;
    }

    /**
     * Prepare a product file
     *
     * @param LoaderParameters $parameters
     *
     * @return ProductYmlFile
     */
    private function prepareFileObject(LoaderParameters $parameters): ProductYmlFile
    {
        if (!$parameters->getTransformer()) {
            throw new LoaderFactoryException('Transformer is required parameter');
        }

        //if filePath exists, then create record and load from file path
        $filePath = $parameters->getFilePath();
        $force = $parameters->isForce();
        if ($filePath) {
            if (!file_exists($filePath)) {
                throw new LoaderFactoryException('Import file not found');
            }

            //moves file
            $extension = pathinfo($filePath, PATHINFO_EXTENSION);
            $fileHash = md5_file($filePath);
            $sourcePath = $this->dataPath . DS . $fileHash . '.' . $extension;

            $iteration = 1;
            while (file_exists($sourcePath)) {
                $sourcePath = $this->dataPath . DS . $fileHash . '_' . $iteration . '.' . $extension;
                $iteration++;
            }

            rename($filePath, $sourcePath);

            $importFile = (new ProductYmlFile())
                ->setInitiallyPath($filePath)
                ->setSourcePath($sourcePath);

            return $importFile;
        }

        //if exists id of import-file then load state from db
        $productFileId = $parameters->getProductFileId();
        $importFile = $this->entityManager->find(ProductYmlFile::class, $productFileId);
        if (!$importFile) {
            throw new LoaderFactoryException("Record for file with id #$productFileId not found");
        }

        //checks if file was moved to data_dir
        $movePath = $importFile->getSourcePath();
        if (!file_exists($movePath)) {
            throw new LoaderFactoryException('Moved import file not found in data directory');
        }

        if ($force) {
            $importFile
                ->setOffset(0)
                ->setTransformer(null)
                ->setStatus(ProductYmlFile::STATUS_PROCESS);
        }

        return $importFile;
    }

    /**
     * Selects data transformer to canonical
     *
     * @return BaseTransformer
     */
    private function selectTransformer($name): BaseTransformer
    {
        $transformer = array_filter($this->transformers, function (BaseTransformer $transformer) use ($name) {
            return $transformer->getName() === $name;
        });

        $transformer = reset($transformer);
        if (!$transformer) {
            throw new LoaderFactoryException('Unknown loader');
        }

        return $transformer;
    }

    /**
     * Implement select logic
     *
     * @return LoaderInterface
     */
    private function selectLoader(string $filePath): LoaderInterface
    {
        $loaderClassName = reset($this->loaders);
        return new $loaderClassName;
    }
}