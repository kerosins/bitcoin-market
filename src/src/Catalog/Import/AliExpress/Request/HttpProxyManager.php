<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Import\AliExpress\Request;


use Catalog\Import\AliExpress\Exception\OutOfProxyException;
use Catalog\ORM\Entity\HttpProxy;
use Catalog\ORM\Repository\HttpProxyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;
use Symfony\Component\HttpKernel\CacheClearer\CacheClearerInterface;

class HttpProxyManager implements CacheClearerInterface
{
    private const CACHE_TAG = 'zb-import-proxy-list';

    /**
     * @var TagAwareAdapterInterface
     */
    private $tagAwareAdapter;

    /**
     * @var HttpProxyRepository
     */
    private $httpProxyRepository;

    /**
     * @var bool
     */
    private $isCli;

    /**
     * @var ArrayCollection
     */
    private $proxies;

    public function __construct(
        TagAwareAdapterInterface $tagAwareAdapter,
        HttpProxyRepository $httpProxyRepository,
        bool $isCli
    ) {
        $this->tagAwareAdapter = $tagAwareAdapter;
        $this->httpProxyRepository = $httpProxyRepository;
        $this->isCli = $isCli;
        $this->proxies = new ArrayCollection();
    }

    /**
     * @return HttpProxy
     *
     * @throws OutOfProxyException
     */
    public function getProxy()
    {
        $cacheItem = $this->tagAwareAdapter->getItem('zb-proxy-current');
        if ($cacheItem->isHit()) {
            return $cacheItem->get();
        }

        $proxy = $this
            ->getProxyList()
            ->filter(function (HttpProxy $httpProxy) {
                if ($httpProxy->getIsBanned()) {
                    return false;
                }

                if ($this->isCli) {
                    return !$httpProxy->getIsOnlyRuntime();
                }

                return $httpProxy->getIsOnlyRuntime();
            })
            ->first();

        if (!$proxy) {
            throw new OutOfProxyException();
        }

        $cacheItem->set($proxy);
        $cacheItem->tag(self::CACHE_TAG);
        $this->tagAwareAdapter->save($cacheItem);

        return $proxy;
    }

    /**
     * @param HttpProxy $httpProxy
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function disable(HttpProxy $httpProxy)
    {
        $httpProxy->setIsBanned(true);
        $this->httpProxyRepository->saveDetached($httpProxy);
        $this->invalidate();
    }

    /**
     * @return void
     */
    public function invalidate()
    {
        $this->tagAwareAdapter->invalidateTags([self::CACHE_TAG]);
    }

    /**
     * @inheritdoc
     * @param string $cacheDir
     */
    public function clear($cacheDir)
    {
        $this->invalidate();
    }


    /**
     * gets list of proxies
     *
     * @return ArrayCollection
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function getProxyList()
    {
        if ($this->proxies->count()) {
            return $this->proxies;
        }

        $cacheItem = $this->tagAwareAdapter->getItem('zb-import-proxy-list');
        if (!$cacheItem->isHit()) {
            $proxies = $this->httpProxyRepository->findBy([
                'isBanned' => false
            ]);
            $cacheItem->set($proxies);
            $cacheItem->tag(self::CACHE_TAG);
            $this->tagAwareAdapter->save($cacheItem);
        }

        $this->proxies = new ArrayCollection($cacheItem->get());
        return $this->proxies;
    }
}