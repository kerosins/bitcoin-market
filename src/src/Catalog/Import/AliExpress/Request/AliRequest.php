<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Import\AliExpress\Request;

use GuzzleHttp\Psr7\Response;

class AliRequest
{
    /**
     * @var string
     */
    private $uid;

    /**
     * @var string
     */
    private $uri;

    /**
     * @var int
     */
    private $attemptCount = 0;

    /**
     * @var array
     */
    private $options = [];

    /**
     * @var Response
     */
    private $response;

    /**
     * @var bool
     */
    private $isPerformed = false;

    /**
     * @var array
     */
    private $cookies = [];

    /**
     * @var callable
     */
    private $promise;

    public function __construct(string $uri, array $cookies, array $options = [])
    {
        $this->uri = $uri;
        $this->options = $options;
        $this->cookies = $cookies;
        $this->uid = uniqid('', true);
    }

    public function getCookies()
    {
        return $this->cookies;
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function addRequestOptions(array $options)
    {
        array_merge($this->options, $options);
        return $this;
    }

    /**
     * @return string
     */
    public function getUid(): ?string
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getUri(): ?string
    {
        return $this->uri;
    }

    /**
     * @return int
     */
    public function getAttemptCount(): ?int
    {
        return $this->attemptCount;
    }

    /**
     * @return AliRequest
     */
    public function increaseAttemptCount(): AliRequest
    {
        $this->attemptCount++;
        return $this;
    }

    /**
     * @return Response
     */
    public function getResponse(): ?Response
    {
        return $this->response;
    }

    /**
     * @param Response $response
     *
     * @return AliRequest
     */
    public function setResponse(Response $response): AliRequest
    {
        $this->response = $response;
        $this->isPerformed = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPerformed(): ?bool
    {
        return $this->isPerformed;
    }

    /**
     * @return array
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @return callable
     */
    public function getPromise(): ?callable
    {
        return $this->promise;
    }

    /**
     * @param callable $promise
     *
     * @return AliRequest
     */
    public function setPromise(?callable $promise): AliRequest
    {
        $this->promise = $promise;
        return $this;
    }
}