<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Import\AliExpress\Request;


use GuzzleHttp\Psr7\Response;

class ResponseChecker
{
    public const
        PRODUCT_STRATEGY = 'product',
        DELIVERY_STRATEGY = 'delivery';

    /**
     * @var int
     */
    private $strategy;

    /**
     * ResponseChecker constructor.
     *
     * @param int $strategy
     */
    public function __construct(string $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * @param Response $request
     */
    public function check(Response $response): bool
    {
        switch ($this->strategy) {
            case self::PRODUCT_STRATEGY:
                return $this->checkStrategyProduct($response);
            case self::DELIVERY_STRATEGY:
                return $this->checkStrategyDelivery($response);
        }

        return false;
    }

    public function checkStrategyProduct(Response $response)
    {
        //todo implement
        return true;
    }

    public function checkStrategyDelivery(Response $response)
    {
        return 200 === $response->getStatusCode();
    }
}