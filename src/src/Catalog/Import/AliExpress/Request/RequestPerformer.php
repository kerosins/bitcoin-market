<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Import\AliExpress\Request;

use Catalog\Import\AliExpress\Exception\OutOfProxyException;
use Catalog\Import\AliExpress\Exception\ExceedAttemptsException;
use Catalog\ORM\Entity\HttpProxy;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;

class RequestPerformer
{
    /**
     * @var Client
     */
    private $httpClient;

    private const
        MAIN_PAGE = 'https://m.aliexpress.com',
        SLEEP_TIMEOUT_INTERVAL = [20, 40],
        FIRST_ATTEMPT = 1,
        SECOND_ATTEMPT = 2;

    public const ALIEXPRESS_COM = '.aliexpress.com';
    public const CACHE_TAG = 'zb.request_performer';

    /**
     * array of default cookies
     *
     * @var array
     */
    protected $defaultCookies = [];

    /**
     * @var array
     */
    private $requestOptions = [
        RequestOptions::HEADERS => [
            'user-agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36'
        ]
    ];

    /**
     * @var int
     */
    private $concurrency = 3;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var bool
     */
    private $cliEnvironment;

    /**
     * @var HttpProxyManager
     */
    private $proxyManager;

    /**
     * @var HttpProxy
     */
    private $currentProxy;

    /**
     * @var CookieJar
     */
    private $cookieJar;

    /**
     * @var TagAwareAdapterInterface
     */
    private $tagAwareAdapter;

    public function __construct(
        LoggerInterface $logger,
        HttpProxyManager $proxyManager,
        TagAwareAdapterInterface $tagAwareAdapter,
        bool $cliEnvironment
    ) {
        $this->logger = $logger;
        $this->proxyManager = $proxyManager;
        $this->tagAwareAdapter = $tagAwareAdapter;
        $this->cliEnvironment = $cliEnvironment;

        $this->httpClient = new Client();
    }

    /**
     * @param AliRequest[] $requests
     * @param callable $checker
     *
     * @return AliRequest[]
     *
     * @throws ExceedAttemptsException
     */
    public function perform(array $requests, callable $checker)
    {
        $attempt = 0;
        $countRequests = count($requests);
        $successRequests = [];

        while ($attempt < 2) {
            try {
                //change cookies and proxy if previously requests were failed
                if (self::FIRST_ATTEMPT === $attempt) {
                    $this->clearCookies();
                } elseif (self::SECOND_ATTEMPT === $attempt) {
                    $this->clearCookies();
                    $this->proxyManager->disable($this->currentProxy);
                    $this->currentProxy = null;
                }

                if ($attempt > 0) {
                    sleep(rand(...self::SLEEP_TIMEOUT_INTERVAL));
                }

                $this->attemptRequests($requests, $checker);

                //separate successfully and failed requests
                $successRequests[] = array_filter(
                    $requests,
                    function (AliRequest $request) {
                        return $request->isPerformed();
                    }
                );

                $requests = array_filter(
                    $requests,
                    function (AliRequest $request) {
                        return !$request->isPerformed();
                    }
                );

                if (!$requests) {
                    break;
                }
            } catch (\Catalog\Import\AliExpress\Exception\RequestException $e) {
                $this->logger->error('Ali Request is failed', [
                    'attempt' => $attempt
                ]);
                $attempt++;
            } catch (OutOfProxyException $e) {
                $this->logger->error('Run out of proxy');
                break;
            }
        }

        $successRequests = array_merge(...$successRequests);

        if ($countRequests !== count($successRequests)) {
            if ($this->cliEnvironment) {
                throw new ExceedAttemptsException();
            } else {
                $this->logger->error(ExceedAttemptsException::class);
            }
        }

        return $successRequests;
    }

    /**
     * Attempts requests
     *
     * @param AliRequest[] $requests
     * @param callable $checker
     */
    private function attemptRequests($requests, callable $checker)
    {
        $promises = array_map(function (AliRequest $request) {
            $this->makePromise($request);
            return $request->getPromise();
        }, $requests);

        $config = [
            'concurrency' => $this->concurrency,
            'fulfilled' => function ($cortege) use ($checker) {
                /** @var AliRequest $request */
                /** @var Response $response */
                [$request, $response] = $cortege;

                $isSuccess = call_user_func($checker, $response);

                if ($isSuccess) {
                    $request->setResponse($response);
                } else {
                    throw new \Catalog\Import\AliExpress\Exception\RequestException();
                }
            }
        ];

        $pool = new Pool($this->httpClient, $promises, $config);
        $pool->promise()->wait();
    }

    /**
     * makes request promise
     *
     * @param AliRequest $request
     */
    private function makePromise(AliRequest $request)
    {
        $promise = function ($requestOptions) use ($request) {
            $cookieJar = $this->getCookies();

            if ($request->getCookies()) {
                foreach ($request->getCookies() as $cookie) {
                    $cookieJar->setCookie(
                        new SetCookie($cookie)
                    );
                }
            }

            $options = array_merge(
                $requestOptions,
                $this->requestOptions,
                $request->getOptions(),
                [
                    RequestOptions::EXPECT => true,
                    RequestOptions::COOKIES => $cookieJar,
                    RequestOptions::PROXY => $this->getCurrentProxy()->getProxyString()
                ]
            );

            return $this->httpClient
                ->getAsync($request->getUri(), $options)
                ->then(
                    function (Response $response) use ($request) {
                        return [$request, $response];
                    },
                    function (RequestException $err) use ($request) {
                        return [$request, $err->getResponse()];
                    }
                );
        };
        $request->setPromise($promise);
    }

    /**
     * Clears cookies
     */
    private function clearCookies()
    {
        $this->tagAwareAdapter->invalidateTags([self::CACHE_TAG]);
        $this->cookieJar = null;
    }

    /**
     * Sends request to aliexpress and fill out cookie jar
     *
     * @return CookieJar
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function getCookies()
    {
        if ($this->cookieJar) {
            return $this->cookieJar;
        }

        $cacheItem = $this->tagAwareAdapter->getItem('zb.request_performer.cookies');
        if ($cacheItem->isHit() && 1 != 1) {
            $this->cookieJar = new CookieJar(false, $cacheItem->get());
            return $this->cookieJar;
        }

        try {
            $mCookie = new CookieJar(false, $this->defaultCookies);
            $options = array_merge($this->requestOptions, [
                RequestOptions::PROXY => $this->getCurrentProxy()->getProxyString(),
                RequestOptions::COOKIES => $mCookie
            ]);
            $response = $this->httpClient->get(self::MAIN_PAGE, $options);
        } catch (OutOfProxyException $e) {
            throw $e;
        } catch (\Throwable $exception) {
            $this->logger->error('Curl error', [
                'exception' => [
                    'message' => $exception->getMessage(),
                    'stack' => $exception->getTraceAsString()
                ]
            ]);
            $this->proxyManager->disable($this->currentProxy);
            $this->currentProxy = null;

            return $this->getCookies();
        }


        $headers = $response->getHeaders();
        $cookies = $headers['Set-Cookie'] ?? [];

        $cookieJar = new CookieJar();

        //set cookies from main response
        foreach ($cookies as $cookie) {
            $cookie = SetCookie::fromString($cookie);
            if (!$cookie->getDomain()) {
                $cookie->setDomain('' . self::ALIEXPRESS_COM . '');
            }

            $cookieJar->setCookie($cookie);
        }

        $cacheItem->set($cookieJar->toArray());
        $cacheItem->tag([self::CACHE_TAG]);
        $this->tagAwareAdapter->save($cacheItem);

        $this->cookieJar = $cookieJar;

        return $this->cookieJar;
    }

    /**
     * Gets a current cookie
     *
     * @return HttpProxy
     * @throws OutOfProxyException
     */
    private function getCurrentProxy()
    {
        if (!$this->currentProxy) {
            $this->currentProxy = $this->proxyManager->getProxy();
        }

        return $this->currentProxy;
    }
}