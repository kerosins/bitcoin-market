<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Import\AliExpress\Product;

use Catalog\Import\AliExpress\Request\AliRequest;
use Catalog\Import\AliExpress\Request\RequestPerformer;
use Catalog\Import\AliExpress\Request\ResponseChecker;

/**
 * Class ProductRequestPerformer
 * @package Catalog\Import\AliExpress\Request
 */
class ProductRequestPerformer
{
    /**
     * @var RequestPerformer
     */
    private $requestPerformer;

    /**
     * @var ResponseChecker
     */
    private $responseChecker;

    /**
     * @var array
     */
    protected $cookies = [
        [
            'Name' => 'aep_usuc_f',
            'Value' => 'site=glo&c_tp=USD&region=US&b_locale=en_US',
            'Domain' => RequestPerformer::ALIEXPRESS_COM
        ]
    ];

    public function __construct(
        RequestPerformer $requestPerformer,
        ResponseChecker $responseChecker
    ) {
        $this->requestPerformer = $requestPerformer;
        $this->responseChecker = $responseChecker;
    }

    /**
     * Gets are products pages
     *
     * @param array $ids
     *
     * @return array
     * @throws \Catalog\Import\AliExpress\Exception\ExceedAttemptsException
     */
    public function getProductPages(array $ids)
    {
        $requests = array_map(
            function ($id) {
                return new AliRequest(
                    "https://www.aliexpress.com/item/{$id}.html",
                    $this->cookies
                );
            },
            $ids
        );

        return $this->requestPerformer->perform(
            $requests,
            [$this->responseChecker, 'check']
        );
    }

    /**
     * gets product
     *
     * @param array $urls
     *
     * @return array
     * @throws \Catalog\Import\AliExpress\Exception\ExceedAttemptsException
     */
    public function getDescriptionPages(array $urls)
    {
        $requests = array_map(
            function ($url) {
                return new AliRequest(
                    $url,
                    $this->cookies
                );
            },
            $urls
        );

        return $this->requestPerformer->perform(
            $requests,
            function () { return true; }//dummy check
        );
    }
}