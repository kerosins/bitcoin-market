<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Import\AliExpress\Product;


use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Entity\ProductPrice;
use Catalog\ORM\Entity\Source;
use Catalog\ORM\Repository\CategoryRepository;
use Content\SlugRoute\Service\SlugCreator;
use Doctrine\ORM\EntityManagerInterface;
use Shop\Entity\Seller;
use Shop\Repository\SellerRepository;

class SimpleSaver
{

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var SellerRepository
     */
    private $sellerRepository;

    /**
     * @var int
     */
    private $priceScale;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var SlugCreator
     */
    private $slugCreator;

    public function __construct(
        CategoryRepository $categoryRepository,
        SellerRepository $sellerRepository,
        EntityManagerInterface $entityManager,
        SlugCreator $slugCreator,
        int $priceScale
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->sellerRepository = $sellerRepository;
        $this->entityManager = $entityManager;
        $this->priceScale = $priceScale;
        $this->slugCreator = $slugCreator;
    }

    public function saveOne(AliExpressProduct $product)
    {
        $ids = $this->saveMany([$product]);
        return reset($ids);
    }

    /**
     * @param array|AliExpressProduct[] $aliExpressProducts
     *
     * @return array
     */
    public function saveMany(array $aliExpressProducts): array
    {
        $categories = $this->findCategories($aliExpressProducts);
        $sellers = $this->findSellers($aliExpressProducts);

        $ids = [];

        foreach ($aliExpressProducts as $aliExpressProduct) {
            $product = new Product();

            $images = $aliExpressProduct->getImages();
            $mainImage = array_shift($images);

            $product
                ->setTitle($aliExpressProduct->getTitle())
                ->setShortDescription($aliExpressProduct->getShortDescription())
                ->setDescription($aliExpressProduct->getDescription())
                ->setMainImage($mainImage)
                ->setImages($images)
                ->setAmountOrders($aliExpressProduct->getAmountItems())
                ->setAmountWishLists($aliExpressProduct->getAmountWishLists())
                ->setSource(Source::MANUAL)
                ->setExternalSource(Source::EXTERNAL_SOURCE_ALIEXPRESS)
                ->setExternalId($aliExpressProduct->getId())
            ;

            //set category and sellers
            if (isset($categories[$aliExpressProduct->getCategoryId()])) {
                $product->addCategory($categories[$aliExpressProduct->getCategoryId()]);
            }

            if (!isset($sellers[$aliExpressProduct->getSellerName()])) {
                $seller = (new Seller())
                    ->setName($aliExpressProduct->getSellerName())
                    ->setInternal(false);

                $this->sellerRepository->save($seller);
                $sellers[$seller->getName()] = $seller;
            }
            $product->setSeller($sellers[$aliExpressProduct->getSellerName()]);

            //set price
            $price = new ProductPrice();
            $price
                ->setValue($aliExpressProduct->getPrice())
                ->setIsActive(true)
                ->setProduct($product)
                ->setScale($this->priceScale);

            //SEO
            $product
                ->setMetaDescription($product->getTitle())
                ->setMetaTitle($product->getTitle())
                ->setMetaKeywords($product->getTitle());

            //save
            $this->entityManager->persist($product);
            $this->entityManager->persist($price);

            //slug
            $this->slugCreator->generate($product);

            $this->entityManager->flush();
            $ids[] = $product->getId();
        }

        return $ids;
    }

    /**
     * @param array|int[] $products
     *
     * @return array|Category[]
     */
    private function findCategories(array $products): array
    {
        $ids = array_map(function (AliExpressProduct $product) {
            return $product->getCategoryId();
        }, $products);

        $categories = $this->categoryRepository->findByExternalId($ids);
        $result = [];
        foreach ($categories as $category) {
            foreach ($ids as $referenceId) {
                if ($category->hasExternalReferenceById($referenceId)) {
                    $result[$referenceId] = $category;
                }
            }
        }

        return $result;
    }

    /**
     * @param array|string[] $products
     *
     * @return array|Seller[]
     */
    private function findSellers(array $products)
    {
        $names = array_map(function (AliExpressProduct $product) {
            return $product->getSellerName();
        }, $products);

        /** @var Seller[] $sellers */
        $sellers = $this->sellerRepository->findByNames($names);

        $result = [];
        foreach ($sellers as $seller) {
            $result[$seller->getName()] = $seller;
        }

        return $result;
    }
}