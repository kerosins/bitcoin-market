<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Import\AliExpress\Product;


use Kerosin\Component\Image;

class AliExpressProduct
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $categoryId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var float
     */
    private $price;

    /**
     * @var Image[]
     */
    private $images;

    /**
     * @var string
     */
    private $shortDescription;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $descriptionUrl;

    /**
     * @var string
     */
    private $sellerName;

    /**
     * @var int
     */
    private $amountItems;

    /**
     * @var int
     */
    private $amountWishLists;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return AliExpressProduct
     */
    public function setId(int $id): AliExpressProduct
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     *
     * @return AliExpressProduct
     */
    public function setCategoryId(int $categoryId): AliExpressProduct
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return AliExpressProduct
     */
    public function setTitle(string $title): AliExpressProduct
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return AliExpressProduct
     */
    public function setPrice(float $price): AliExpressProduct
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return Image[]
     */
    public function getImages(): ?array
    {
        return $this->images;
    }

    /**
     * @param Image[] $images
     *
     * @return AliExpressProduct
     */
    public function setImages(array $images): AliExpressProduct
    {
        $this->images = $images;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    /**
     * @param string $shortDescription
     *
     * @return AliExpressProduct
     */
    public function setShortDescription(string $shortDescription): AliExpressProduct
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return AliExpressProduct
     */
    public function setDescription(string $description): AliExpressProduct
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescriptionUrl(): ?string
    {
        return $this->descriptionUrl;
    }

    /**
     * @param string $descriptionUrl
     *
     * @return AliExpressProduct
     */
    public function setDescriptionUrl(string $descriptionUrl)
    {
        $this->descriptionUrl = $descriptionUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getSellerName(): ?string
    {
        return $this->sellerName;
    }

    /**
     * @param string $sellerName
     *
     * @return AliExpressProduct
     */
    public function setSellerName(string $sellerName): AliExpressProduct
    {
        $this->sellerName = $sellerName;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountItems(): ?int
    {
        return $this->amountItems;
    }

    /**
     * @param int $amountItems
     *
     * @return AliExpressProduct
     */
    public function setAmountItems(int $amountItems): AliExpressProduct
    {
        $this->amountItems = $amountItems;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountWishLists(): ?int
    {
        return $this->amountWishLists;
    }

    /**
     * @param int $amountWishLists
     *
     * @return AliExpressProduct
     */
    public function setAmountWishLists(int $amountWishLists): AliExpressProduct
    {
        $this->amountWishLists = $amountWishLists;
        return $this;
    }
}