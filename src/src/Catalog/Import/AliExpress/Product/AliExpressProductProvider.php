<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Import\AliExpress\Product;

use Catalog\Import\AliExpress\Exception\RequestException;
use Catalog\Import\AliExpress\Request\AliRequest;
use Catalog\Import\AliExpress\Request\RequestPerformer;
use Kerosin\Component\Image;

class AliExpressProductProvider
{
    /**
     * @var array
     */
    protected $defaultCookies = [
        [
            'Name' => 'aep_usuc_f',
            'Value' => 'site=glo&c_tp=USD&region=US&b_locale=en_US',
            'Domain' => RequestPerformer::ALIEXPRESS_COM
        ]
    ];

    /**
     * @var RequestPerformer
     */
    private $requestPerformer;

    /**
     * AliExpressProductProvider constructor.
     *
     * @param ProductRequestPerformer $requestPerformer
     */
    public function __construct(
        ProductRequestPerformer $requestPerformer
    ) {
        $this->requestPerformer = $requestPerformer;
    }

    /**
     * @param array $ids
     *
     * @return AliExpressProduct[]
     */
    public function get(array $ids)
    {
        $responses = $this->requestPerformer->getProductPages($ids);

        /** @var AliExpressProduct[] $products */
        $products = array_map([$this, 'transformProductPage'], $responses);

        $descriptionUrls = array_map(function (AliExpressProduct $product) {
            return $product->getDescriptionUrl();
        }, $products);
        /** @var AliRequest[] $responses */
        $responses = $this->requestPerformer->getDescriptionPages($descriptionUrls);
        $descriptions = [];

        foreach ($responses as $response) {
            $descriptions[$response->getUri()] = $this->sanitizeDescriptions(
                (string)$response->getResponse()->getBody()
            );
        }

        foreach ($products as $product) {
            $product->setDescription(
                $descriptions[$product->getDescriptionUrl()]
            );
        }

        return $products;
    }

    /**
     * @param $id
     *
     * @return AliExpressProduct
     */
    public function getOne($id)
    {
        $result = $this->get([$id]);
        return reset($result);
    }

    /**
     * @param AliRequest $request
     *
     * @return AliExpressProduct
     */
    public function transformProductPage(AliRequest $request): AliExpressProduct
    {
        $productJson = $this->getProductJson((string)$request->getResponse()->getBody());
        if (!$productJson) {
            throw new RequestException("Product's json is empty");
        }

        $product = new AliExpressProduct();
        $getValueByPath = function ($path, $default = null) use ($productJson) {
            $keys = explode('.', $path);
            $result = $productJson;
            foreach ($keys as $key) {
                if (!isset($result[$key])) {
                    return $default;
                }

                $result = $result[$key];
            }

            return $result;
        };

        $title = $getValueByPath('titleModule.subject');
        $images = array_map(
            function ($src) use ($title) {
                return Image::createFromArray([
                    'url' => $src,
                    'title' => $title,
                    'alt' => $title
                ]);
            },
            $getValueByPath('imageModule.imagePathList')
        );

        $properties = $getValueByPath('specsModule.props');
        $properties = array_map(
            function ($property) {
                $result = "<li class='description-item'><span class='ellipsis'>{$property['attrName']}</span><span class='ellipsis'>{$property['attrValue']}</span></li>";
                return $result;
            },
            $properties
        );
        $properties = implode('', $properties);

        $product
            ->setId($getValueByPath('actionModule.productId'))
            ->setCategoryId($getValueByPath('actionModule.categoryId'))
            ->setTitle($title)
            ->setImages($images)
            ->setPrice($getValueByPath('priceModule.maxAmount.value'))
            ->setDescriptionUrl($getValueByPath('descriptionModule.descriptionUrl'))
            ->setSellerName($getValueByPath('storeModule.storeName'))
            ->setAmountItems($getValueByPath('actionModule.totalAvailQuantity'))
            ->setAmountWishLists($getValueByPath('actionModule.itemWishedCount'))
            ->setShortDescription($properties);

        return $product;
    }

    /**
     * Cleans description's html
     *
     * @param $html
     *
     * @return false|string
     */
    private function sanitizeDescriptions($html)
    {
        $html = mb_eregi_replace("<!doctype(.*?)>", '', $html);
        // strip out comments
        $html = mb_eregi_replace("<!--(.*?)-->", '', $html);
        // strip out cdata
        $html = mb_eregi_replace("<!\[CDATA\[(.*?)\]\]>", '', $html);
        // strip out <script> tags
        $html = mb_eregi_replace("<\s*script[^>]*[^/]>(.*?)<\s*/\s*script\s*>", '', $html);
        $html = mb_eregi_replace("<\s*script\s*>(.*?)<\s*/\s*script\s*>", '', $html);
        // strip out <style> tags
        $html = mb_eregi_replace("<\s*style[^>]*[^/]>(.*?)<\s*/\s*style\s*>", '', $html);
        $html = mb_eregi_replace("<\s*style\s*>(.*?)<\s*/\s*style\s*>", '', $html);
        $html = mb_eregi_replace("<link(.*?)>", '', $html);

        return $html;
    }

    /**
     * @param $html
     *
     * @return array
     */
    private function getProductJson($html): array
    {
        $str = 'window.runParams = {';
        $position = strpos($html, $str);
        if ($position === false) {
            return [];
        }

        $endCharSeq = '};';
        $endPosition = strrpos($html, '};');
        while ($endPosition !== false && $endPosition > $position) {
            $html = substr($html, 0, $endPosition);
            $endPosition = strrpos($html, $endCharSeq);
        }

        $json = substr($html, $position + strlen($str));
        preg_match('/(?<=data\:)(.*)(?=\}\})/mi', $json, $matches);
        $json = $matches[0] . '}}';

        $json = json_decode($json, true);
        return $json;
    }
}