<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Import\AliExpress;


use PHPHtmlParser\Dom;
use Psr\Log\LoggerInterface;

class Transformer
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var array
     */
    private static $sellerBlockMapping = [
        'p' => 'sellerName',
        '.sumary-items span' => 'items',
        '.sumary-orders span' => 'orders',
        '.sumary-wishlist span' => 'wishList'
    ];

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function mainPage(string $html)
    {
        $dom = new Dom();
        $dom->loadStr($html, [
            'cleanupInput' => false
        ]);

        //checks 404
        $error = $dom->find('.ms-error .error-info');
        if (count($error)) {
            return null;
        }

        /** @var Dom\Collection $iFrame */
        $iFrame = $dom->find('amp-iframe');
        $result = [
            'title' => '',
            'descriptionUrl' => $iFrame->count() ? $iFrame->getAttribute('src') : null,
            'shortDescription' => '',
            'images' => [],
            'sellerName' => '',
            'items' => 0,
            'orders' => 0,
            'wishList' => 0,
            'price' => null
        ];

        //title
        $result['title'] = trim($this->getHtmlBySelector($dom, 'h1.detail-description'));
        //short description
        $result['shortDescription'] = $this->getHtmlBySelector($dom, '.detail-description-wrap .description-list');

        //images
        $images = $dom->find('#sku-carousel amp-img')->toArray();
        if (count($images)) {
            $result['images'] = array_map(function (Dom\AbstractNode $node) {
                return $node->getAttribute('src');
            }, $images);
        }

        //seller name, orders, etc
        $storeBlock = $dom->find('.stroe-info');
        if (count($storeBlock)) {
            /** @var Dom\HtmlNode $storeBlock */
            $storeBlock = $storeBlock[0];
            foreach (self::$sellerBlockMapping as $selector => $key) {
                $cnt = $storeBlock->find($selector);
                if (count($cnt) > 0) {
                    $result[$key] = $key === 'sellerName' ? $cnt[0]->text() : (int)$cnt[0]->text();
                } else {
                    $result[$key] = $key === 'sellerName' ? '' : 0;
                }
            }
        } else {
            $this->logger->warning('Store block not found');
        }

        $price = $dom->find('.current-price')->text();
        $result['price'] = $this->sanitizePrice($price);

        unset($dom);
        return $result;
    }

    public function justPriceAndAvailable(string $html): array
    {
        $dom = new Dom();
        $dom->loadStr($html, []);

        $available = true;
        $price = 0.0;
        $items = 0;

        $result = [
            'available' => &$available,
            'price' => &$price,
            'items' => &$items
        ];

        //checks if available
        if (!count($dom->find('.buy-now .buy-now-click'))) {
            $available = false;
            return $result;
        }

        $priceBox = $dom->find('.current-price');
        if (count($priceBox)) {
            $price = $this->sanitizePrice($priceBox->text());
        }

        $itemsBlock = $dom->find('.stroe-info .sumary-items span');
        if (count($itemsBlock)) {
            $items = (int)($itemsBlock[0])->text();
        }

        return $result;
    }

    /**
     * @param $price
     *
     * @return float
     */
    private function sanitizePrice($price)
    {
        $price = preg_replace('/[^0-9\.\-]+/', '', $price);
        $price = explode('-', $price);
        $price = end($price);
        $price = (float)$price;

        return $price;
    }

    /**
     * @param Dom $dom
     * @param string $selector
     * @return string
     */
    private function getHtmlBySelector(Dom $dom, string $selector): string
    {
        /** @var Dom\AbstractNode $elements */
        $elements = $dom->find($selector);
        return count($elements) ? $elements->innerHtml() : '';
    }

    /**
     * Clean description from styles and scripts, and some trash
     *
     * @param $html
     *
     * @return string
     */
    public function description($html)
    {
        $html = mb_eregi_replace("<!doctype(.*?)>", '', $html);
        // strip out comments
        $html = mb_eregi_replace("<!--(.*?)-->", '', $html);
        // strip out cdata
        $html = mb_eregi_replace("<!\[CDATA\[(.*?)\]\]>", '', $html);
        // strip out <script> tags
        $html = mb_eregi_replace("<\s*script[^>]*[^/]>(.*?)<\s*/\s*script\s*>", '', $html);
        $html = mb_eregi_replace("<\s*script\s*>(.*?)<\s*/\s*script\s*>", '', $html);
        // strip out <style> tags
        $html = mb_eregi_replace("<\s*style[^>]*[^/]>(.*?)<\s*/\s*style\s*>", '', $html);
        $html = mb_eregi_replace("<\s*style\s*>(.*?)<\s*/\s*style\s*>", '', $html);
        $html = mb_eregi_replace("<link(.*?)>", '', $html);

        return $html;
    }
}