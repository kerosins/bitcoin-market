<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Import\AliExpress\Delivery;

use Catalog\Import\AliExpress\Exception\ExceedAttemptsException;
use Catalog\Import\AliExpress\Exception\RequestException;
use Catalog\Import\AliExpress\Request\AliRequest;
use Catalog\Import\AliExpress\Request\RequestPerformer;
use Catalog\Import\AliExpress\Request\ResponseChecker;
use Catalog\Order\Exception\DeliveryCompanyNotFoundException;
use Catalog\ORM\Entity\Product;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;
use Shop\Entity\Country;

class DeliveryRequestPerformer
{
    /**
     * @var RequestPerformer
     */
    private $requestPerformer;

    /**
     * @var string
     */
    private $domain;
    /**
     * @var string
     */
    private $defaultCountryCode;
    /**
     * @var ResponseChecker
     */
    private $responseChecker;

    /**
     * DeliveryRequestPerformer constructor.
     *
     * @param RequestPerformer $requestPerformer
     * @param ResponseChecker $responseChecker
     * @param string $defaultCountryCode
     * @param string $domain
     */
    public function __construct(
        RequestPerformer $requestPerformer,
        ResponseChecker $responseChecker,
        string $defaultCountryCode,
        string $domain
    ) {
        $this->requestPerformer = $requestPerformer;
        $this->defaultCountryCode = $defaultCountryCode;
        $this->domain = $domain;
        $this->responseChecker = $responseChecker;
    }

    /**
     * @todo use DTO
     *
     * @param Product $product
     * @param Country $country
     * @param int $qty
     *
     * @return array
     */
    public function sendRequestForCalculate(Product $product, Country $country, int $qty = 1)
    {
        $externalId = $product->getExternalId();

        if (!$externalId) {
            return new \RuntimeException('External id is empty');
        }

        $uri = sprintf('https://m.%s/api/products/%d/fees', $this->domain, $externalId);
        $referer = sprintf('https://m.%s/item/%d.html', $this->domain, $externalId);

        $request = new AliRequest(
            $uri,
            [],
            [
                RequestOptions::QUERY => [
                    'count' => $qty,
                    'country' => $country->getAliExpressCode() ?? $this->defaultCountryCode,
                    'sendGoodsCountry' => 'CN'
                ],
                RequestOptions::HEADERS => [
                    'Referer' => $referer,
                ]
            ]
        );

        $response = $this->requestPerformer->perform([$request], [$this->responseChecker, 'check']);
        $response = (reset($response))->getResponse();

        $dataJson = (string)$response->getBody();
        $dataJson = json_decode($dataJson, true);
        $dataJson = $dataJson['data']['freightResult'] ?? null;

        if (!$dataJson) {
            throw new RequestException('Delivery response is empty');
        }

        $companies = array_filter($dataJson, function ($item) {
            return !empty($item['commitDay']);
        });

        if (!$companies) {
            throw new DeliveryCompanyNotFoundException();
        }

        $companies = array_map(function ($company) {
            $time = $company['time'];
            $time = explode('-', $time);
            $time = (array_sum($time) / count($time));

            $company['time'] = round($time);

            return $company;
        }, $companies);

        usort($companies, function ($a, $b) {
            $days = (int)$a['time'] <=> (int)$b['time'];
            return $days === 0 ? $a['freightAmount']['value'] <=> $b['freightAmount']['value'] : $days;
        });
        $selected = reset($companies);
        if (!$selected) {
            throw new DeliveryCompanyNotFoundException('Delivery company is not select for product #' . $product->getId());
        }

        return [
            'commitDay' => $selected['time'],
            'price' => $selected['freightAmount']['value'],
            'selected' => $selected,
            'response' => json_encode($dataJson)
        ];
    }
}