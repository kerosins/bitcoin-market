<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Import\AliExpress;
use Catalog\ORM\Entity\AliExpressProduct;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\DependencyInjection\Contract\TaggedCacheContract;
use Kerosin\DependencyInjection\Contract\TaggedCacheContractTrait;

/**
 * Class AliExpressProductProvider
 * @package Catalog\Import\Service
 *
 * Higher abstraction in front of AliExpressPageProvider, that uses in web
 */
class AliExpressProductProvider implements TaggedCacheContract, EntityManagerContract
{
    use TaggedCacheContractTrait;
    use EntityManagerContractTrait;

    /**
     * @var AliExpressPageProvider
     */
    private $aliExpressPageProvider;

    /**
     * @var int
     */
    private $expire;

    /**
     * @var Transformer
     */
    private $transformer;

    public function __construct(
        AliExpressPageProvider $provider,
        Transformer $transformer,
        int $expire = null
    ) {
        $this->aliExpressPageProvider = $provider;
        $this->transformer = $transformer;
        $this->expire = $expire;
    }

    /**
     * Gets aliexpress products
     *
     * @param $ids - id, or string of ids separated by comma, or array
     *
     * @return AliExpressProduct[] - where keys are ids of products
     */
    public function search($ids)
    {
        $pool = [];
        if (is_string($ids)) {
            $ids = explode(',', $ids);
            $ids = array_map('trim', $ids);
        }

        foreach ($ids as $id) {
            $pool[$id] = $this->aliExpressPageProvider->prepareUriFromId($id);
        }

        $result = [];
        //checks cache
        $pool = array_filter($pool, function ($id) use (&$result) {
            $cache = $this->taggedCache->getItem('ali_express_product_' . $id);
            if ($cache->get() !== null) {
                $result[$id] = $cache->get();
                return false;
            }

            return true;
        }, ARRAY_FILTER_USE_KEY);

        $this->aliExpressPageProvider->search(
            $pool,
            [
                AliExpressPageProvider::OPTION_USE_DB => false,
                AliExpressPageProvider::OPTION_PAGE_TRANSFORMER => function ($html, $uri) use ($pool, &$result) {
                    $data = $this->transformer->justPriceAndAvailable($html);
                    $product = AliExpressProduct::create(
                        $data['available'],
                        $data['price'],
                        $data['items']
                    );

                    $id = array_search($uri, $pool);
                    $product->setId($id);

                    $cache = $this->taggedCache->getItem('ali_express_product_' . $id);
                    $cache->set($product);
                    $cache->expiresAfter($this->expire);
                    $this->taggedCache->save($cache);

                    $result[$id] = $product;
                }
            ]
        );

        return $result;
    }

    /**
     * Provides a full info from aliexpress
     *
     * @param int[] $id
     * @return array|mixed|null
     */
    public function provideFullInfo($id)
    {
        $url = $this->aliExpressPageProvider->prepareUriFromId($id);
        $product = $this->aliExpressPageProvider->search(
            [$url],
            [
                AliExpressPageProvider::OPTION_PAGE_TRANSFORMER => [$this->transformer, 'mainPage']
            ]
        );

        $product = reset($product);

        if ($product === null) {
            return null;
        }

        $product['mainImage'] = '';
        $product['url'] = str_replace('/m.', '', $url);
        if ($product['images']) {
            $product['mainImage'] = array_shift($product['images']);
        }

        if (empty($product['descriptionUrl'])) {
            return $product;
        }

        $description = $this->aliExpressPageProvider->search(
            [$product['descriptionUrl']],
            [
                AliExpressPageProvider::OPTION_PAGE_TRANSFORMER => [$this->transformer, 'description']
            ]
        );

        $product['description'] = reset($description);

        return $product;
    }
}