<?php
/**
 * @author Kerosin
 */

namespace Catalog\Import\Service;

use Catalog\Import\Exception\FailedKeepException;
use Catalog\Import\Exception\UnknownCategoryException;
use Catalog\Import\Resolver\CategoryResolver;
use Catalog\ORM\Entity\CanonicalProduct;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\ExternalCategoriesMap;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Entity\ProductPrice;
use Catalog\ORM\Repository\ProductRepository;
use Content\ORM\Entity\Slug;
use Content\SlugRoute\Service\SlugCreator;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Kerosin\Component\Image;
use Kerosin\Doctrine\ORM\Persister;
use Kerosin\Entity\BusinessLog;
use Psr\Log\LoggerInterface;
use Shop\Entity\Seller;

class ProductKeeper
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var CategoryResolver
     */
    private $resolver;

    private $categories = [];
    private $suppliers = [];

    /**
     * @var Persister
     */
    private $persister;

    /**
     * @var \Content\SlugRoute\Service\SlugCreator
     */
    private $slugCreator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var int
     */
    private $priceAdvance;

    public function __construct(
        ObjectManager $em,
        CategoryResolver $resolver,
        Persister $persister,
        SlugCreator $slugCreator,
        LoggerInterface $logger,
        int $priceAdvance
    ) {
        $this->em = $em;
        $this->resolver = $resolver;
        $this->persister = $persister;
        $this->slugCreator = $slugCreator;
        $this->logger = $logger;

        $this->priceAdvance = $priceAdvance;

        $this->init();
    }

    /**
     * Init a keeper
     */
    public function init()
    {
        $this->persister->registerEntityForDetach([
            Product::class,
            ProductPrice::class,
            ExternalCategoriesMap::class,
            Slug::class,
            BusinessLog::class
        ]);

        $this->persister->disableDoctrineLogger();
        $this->persister->disableElasticPopulate();
    }

    /**
     * Find product if it exist
     *
     * @param CanonicalProduct $product
     *
     * @return \Catalog\ORM\Entity\Product|null
     */
    private function findProduct(CanonicalProduct $product)
    {
        return $this->getProductRepository()
            ->findByExternalIdentifiers(
                $product->getExternalSource(),
                $product->getId()
            );
    }

    /**
     * Save a pack of products
     *
     * @param CanonicalProduct[] $canonicalProducts
     *
     * @return int[] - array of ids
     *
     * @throws FailedKeepException
     */
    public function keep(array $canonicalProducts)
    {
        $productIds = [];

        try {
            /** @var Product[] $products */
            $products = [];
            foreach ($canonicalProducts as $canonical) {
                $product = $this->findProduct($canonical);

                try {
                    $products[] = $product ?
                        $this->persistAsExisting($canonical, $product) :
                        $this->persistAsNew($canonical);
                } catch (UnknownCategoryException $e) {
                    $this->logger->error($e->getMessage());
                }
            }

            $this->persister->flush(false);

            foreach ($products as $item) {
                $productIds[] = $item->getId();
                $alias = $this->slugCreator->generateIfNotExists($item, true, false);
                if ($alias) {
                    $this->persister->persist($alias);
                }
            }

            $this->persister->flush();
        } catch (\Exception $e) {
            if ($e->getPrevious()) {
                var_dump($e->getPrevious()->getMessage());
                var_dump($e->getPrevious()->getTraceAsString());
            }
            $exception = new FailedKeepException("", 0, $e);

            $this->logger->error('Failed keep pack of products', [
                'trace' => $e->getPrevious() ? $e->getPrevious()->getTraceAsString() : $e->getTraceAsString()
            ]);
            throw $exception;
        }

        return $productIds;
    }

    /**
     * Create a new product
     *
     * @param CanonicalProduct $product
     * @throws UnknownCategoryException
     *
     * @return Product;
     */
    private function persistAsNew(CanonicalProduct $product)
    {
        try {
            /** @var \Catalog\ORM\Entity\Category[] $categories */
            $categories = $this->resolveCategory($product);
        } catch (UnknownCategoryException $exception) {
            $message = "Category not found for '{$product->getCategory()->getTitle()}'";
            $data = [
                'category' => [
                    'title' => $product->getCategory()->getTitle(),
                    'id' => $product->getCategory()->getId()
                ],
                'product' => [
                    'title' => $product->getTitle(),
                    'id' => $product->getId()
                ]
            ];
            $this->logger->error($message, $data);

            throw $exception;
        }

        $entity = new Product();

        //set a category
        foreach ($categories as $category) {
            $entity->addCategory($category);
        }

        //title and main image
        /** @var Image $mainImage */
        $mainImage = $product->getMainImage();
        if (!$mainImage instanceof Image) {
            $mainImage = new Image();
        }
        $title = $product->getTitle();
        $mainImage->setTitle($title)->setAlt($title);
        $entity->setMainImage($mainImage)->setTitle($title);

        //price
        $productPrice = new ProductPrice();
        $productPrice->setValue($product->getPrice());
        $productPrice->setScale($this->priceAdvance);
        $entity->setPrice($productPrice);

        //temporary seller
        $supplier = $this->getSupplier();
        $entity->setSeller($supplier);

        //set source data and status new
        $entity
            ->setSource($product->getSource())
            ->setExternalSource($product->getExternalSource())
            ->setExternalId($product->getId())
            ->setExternalLink($product->getUrl())
            ->setStatus(Product::STATUS_NEW);

        $this->persister->persist([$entity, $productPrice]);
        return $entity;
    }

    /**
     * Find differences and update a product
     *
     * @param CanonicalProduct $product
     * @param \Catalog\ORM\Entity\Product $ourProduct
     *
     * @return Product
     */
    private function persistAsExisting(CanonicalProduct $product, Product $ourProduct)
    {
        //todo implement update only differences

        //skip new products
        if ($ourProduct->getStatus() === Product::STATUS_NEW) {
            return $ourProduct;
        }

        $productPrice = $ourProduct->getPrice();
        $productPrice->setValue($product->getPrice());
        $ourProduct
            ->setTitle($product->getTitle())
            ->toggleStatus($product->getAvailable());

        $this->persister->persist([$ourProduct, $productPrice]);
        return $ourProduct;
    }

    /**
     * Get a supplier
     *
     * @return Seller
     */
    private function getSupplier()
    {
        if (!$this->suppliers) {
            $suppliers = $this->em->getRepository(Seller::class)->findAll();
            $this->suppliers = $suppliers;
        }

        $countSupplies = count($this->suppliers);

        return $this->suppliers[random_int(0, $countSupplies - 1)];
    }

    /**
     * @param CanonicalProduct $product
     * @return Category[]
     */
    private function resolveCategory(CanonicalProduct $product)
    {
        $category = $product->getCategory();
        if (empty($this->categories[$category->getId()])) {
            try {
                /** @var \Catalog\ORM\Entity\Category $category */
                $ourCategories = $this->resolver->resolve($product->getCategory());
            } catch (UnknownCategoryException $exception) {
                //log here
                throw $exception;
            }

            foreach ($ourCategories as $ourCategory) {
                $this
                    ->em
                    ->getRepository(ExternalCategoriesMap::class)
                    ->createIfNotExist(
                        $product->getCategory(),
                        $ourCategory
                    );

                $this->categories[$category->getId()] = $ourCategory;
            }

            return $ourCategories;
        }

        return [$this->categories[$category->getId()]];
    }

    /**
     * @return \Catalog\ORM\Repository\ProductRepository
     */
    private function getProductRepository()
    {
        if (!$this->productRepository) {
            $this->productRepository = $this->em->getRepository(Product::class);
        }

        return $this->productRepository;
    }
}