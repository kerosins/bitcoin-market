<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Import\Service;

use Catalog\Import\Component\ProductUpdateEvent;
use Catalog\Import\Exception\EpnException;
use Catalog\Order\Cart\Item;
use Catalog\Order\Cart\ProductCart;
use Catalog\ORM\Entity\AliExpressProduct;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderPosition;
use Catalog\ORM\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Entity\SystemSetting;
use Kerosin\SystemSettings\SettingsManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ProductAvailableProvider
 * @package Catalog\Import\Service
 */
class ProductAvailableProvider {


    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var int
     */
    private $minAmountItems;

    /**
     * @var SettingsManager
     */
    private $settingsManager;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TagAwareAdapterInterface
     */
    private $taggedCache;

    /**
     * @var EpnAvailableRequestPerformer
     */
    private $epnAvailableRequestPerformer;

    public function __construct(
        EntityManagerInterface $entityManager,
        TagAwareAdapterInterface $taggedCache,
        SettingsManager $settingsManager,
        EventDispatcherInterface $eventDispatcher,
        EpnAvailableRequestPerformer $epnAvailableRequestPerformer,
        LoggerInterface $logger,
        int $minAmountItems
    ) {
        $this->entityManager = $entityManager;
        $this->taggedCache = $taggedCache;
        $this->settingsManager = $settingsManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->epnAvailableRequestPerformer = $epnAvailableRequestPerformer;
        $this->logger = $logger;
        $this->minAmountItems = $minAmountItems;
    }

    /**
     * @param \Catalog\ORM\Entity\Product $product
     *
     * @return bool - true if available, false otherwise
     */
    public function checkProduct(Product $product): bool
    {
        $result = $this->checkForUnavailable([$product]);
        if ($result) {
            $product->toggleStatus(false);
            $this->entityManager->persist($product);
            $this->entityManager->flush($product);

            return false;
        }

        return true;
    }

    /**
     * Checks cart for unavailable products and remove them
     *
     * @param ProductCart $cart
     *
     * @return \Catalog\ORM\Entity\Product[] - of unavailable products
     */
    public function checkCart(ProductCart $cart): array
    {
        $items = $cart->get()->toArray();
        if (!$items) {
            return [];
        }

        $products = array_map(function (Item $item) {
            return $item->getProduct();
        }, $items);

        $unavailableProducts = $this->checkForUnavailable($products);
        foreach ($unavailableProducts as $unavailableProduct) {
            $cart->remove($unavailableProduct);
        }

        return $unavailableProducts;
    }

    /**
     * Checks order for unavailable products and remove them
     *
     * @param Order $order
     *
     * @return \Catalog\ORM\Entity\Product[]
     */
    public function checkOrder(Order $order)
    {
        $orderProducts = $order->getOrderProducts();
        $products = array_map(function (OrderPosition $orderProduct) {
            return $orderProduct->getProduct();
        }, $orderProducts->toArray());

        $unavailableProducts = $this->checkForUnavailable($products);

        if ($unavailableProducts) {
            $this->entityManager->beginTransaction();
            try {
                $unavailableIds = array_map(function (Product $product) {
                    return $product->getId();
                }, $unavailableProducts);

                $orderProducts->forAll(function (OrderPosition $orderProduct) use ($unavailableIds) {
                    if (in_array($orderProduct->getProduct()->getId(), $unavailableIds)) {
                        $this->entityManager->remove($orderProduct);
                    }
                });

                $this->entityManager->flush();
                $this->entityManager->commit();
            } catch (\Exception $e) {
                $this->entityManager->rollback();
                throw $e;
            }
        }

        return $unavailableProducts;
    }

    /**
     * @fixme temp code
     *
     * Checks array of products for unavailable
     *
     * @internal
     *
     * @param \Catalog\ORM\Entity\Product[] $products
     */
    public function checkForUnavailable($products)
    {
        if (!$this->settingsManager->getValue(SystemSetting::CHECK_PRODUCT_AVAILABLE)) {
            return [];
        }

        /** @var Product[] $indexedProducts */
        $indexedProducts = [];

        foreach ($products as $product) {
            $indexedProducts[$product->getExternalId()] = $product;
        }

        try {
            $results = $this->epnAvailableRequestPerformer->perform(array_keys($indexedProducts));
        } catch (EpnException $e) {
            $this->logger->error($e->getMessage(), [
                'trace' => $e->getTraceAsString()
            ]);

            $this->settingsManager->setValue(SystemSetting::CHECK_PRODUCT_AVAILABLE, false);
            return [];
        }

        $unavailable = [];
        $hasChanges = false;
        foreach ($results as $id => $result) {
            $product = $indexedProducts[$id];
            $isAvailable = !isset($result['error']);

            if ($product->isActive() !== $isAvailable) {
                $product->toggleStatus($isAvailable);

                $event = new ProductUpdateEvent();
                $event->setProduct($product);
                $event->setProductAvailable($isAvailable);
                $this->eventDispatcher->dispatch(ProductUpdateEvent::EVENT_NAME_AVAILABLE, $event);

                $this->entityManager->persist($product);
                $hasChanges = true;
            }

            if (!$isAvailable) {
                $unavailable[] = $product;
            }
        }

        if ($hasChanges) {
            $this->entityManager->flush();
        }

        return $unavailable;
    }

    /**
     * @param \Catalog\ORM\Entity\Product $product
     * @param AliExpressProduct $aliExpressProduct
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function sync(Product $product, AliExpressProduct $aliExpressProduct)
    {
        $hasChanges = false;

        if ($product->isActive()
            && (!$aliExpressProduct->isAvailable() || $this->minAmountItems >= $aliExpressProduct->getItems())
        ) {
            $hasChanges = true;
            $product->setStatus(Product::STATUS_UNAVAILABLE);

            $event = new ProductUpdateEvent();
            $event->setProduct($product);
            $event->setProductAvailable(false);

            $this->eventDispatcher->dispatch(ProductUpdateEvent::EVENT_NAME_AVAILABLE, $event);
        }

//        if ($product->getPrice()->getValue() !== $aliExpressProduct->getPrice()) {
//            $hasChanges = true;
//            $price = $product->getPrice();
//            $price->setValue($aliExpressProduct->getPrice());
//            $this->entityManager->persist($price);
//
//            $event = new ProductUpdateEvent();
//            $event->setProduct($product);
//            $event->setNewPrice($price->getValue());
//
//            $this->eventDispatcher->dispatch(ProductUpdateEvent::EVENT_NAME_PRICE, $event);
//        }

        if ($hasChanges) {
            $this->entityManager->persist($product);
            $this->entityManager->flush();
        }
    }
}