<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Import\Service;

use Catalog\Import\Exception\EpnException;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class EpnAvailableRequestPerformer
{
    private const API_URL = 'http://api.epn.bz/json';

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $deepHash;

    /**
     * @var Client
     */
    private $client;

    public function __construct(
        string $apiKey,
        string $deepHash
    ) {
        $this->apiKey = $apiKey;
        $this->deepHash = $deepHash;
        $this->client = new Client();
    }

    /**
     * Send request to API EPN
     *
     * @param int[] $ids
     *
     * @return array
     *
     * @throws EpnException
     */
    public function perform(array $ids)
    {
        $requests = array_map(function ($id) {
            return [
                'action' => 'offer_info',
                'id' => $id,
                'lang' => 'en',
                'currency' => 'USD'
            ];
        }, $ids);
        $requests = array_combine($ids, $requests);

        $data = [
            'user_api_key' => $this->apiKey,
            'user_hash' => $this->deepHash,
            'api_version' => 2,
            'requests' => $requests
        ];

        $res = $this->client->post(self::API_URL, [
            RequestOptions::JSON => $data,
            RequestOptions::HEADERS => [
                'Content-Type' => 'text/plain'
            ],
            RequestOptions::HTTP_ERRORS => false
        ]);

        if ($res->getStatusCode() !== 200) {
            throw new EpnException('API EPN is down');
        }

        $responseData =  json_decode((string)$res->getBody(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new EpnException('EPN sent is bad response');
        }

        return $responseData['results'];
    }
}