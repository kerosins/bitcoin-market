<?php
/**
 * @author Kerosin
 */

namespace Catalog\Import\Resolver;


use Catalog\ORM\Entity\CanonicalCategory;
use Catalog\Import\Exception\UnknownCategoryException;
use Catalog\ORM\Entity\Category;

abstract class CategoryResolver
{
    /**
     * @var CategoryResolver
     */
    private $fallback;

    /**
     * You must call fallback if category not resolved
     *
     * @param CanonicalCategory $category
     *
     * @throws UnknownCategoryException
     * @return \Catalog\ORM\Entity\Category[]
     */
    abstract public function resolve(CanonicalCategory $category);

    public function setFallback(CategoryResolver $resolver)
    {
        $this->fallback = $resolver;
        return $this;
    }

    /**
     * @param CanonicalCategory $category
     *
     * @throws UnknownCategoryException
     * @return \Catalog\ORM\Entity\Category[]
     */
    protected function fallback(CanonicalCategory $category)
    {
        if (!$this->fallback) {
            throw new UnknownCategoryException();
        }

        return $this->fallback->resolve($category);
    }
}