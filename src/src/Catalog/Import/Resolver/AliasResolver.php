<?php
/**
 * @author Kerosin
 */

namespace Catalog\Import\Resolver;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Catalog\ORM\Entity\CanonicalCategory;
use Catalog\ORM\Entity\CategoryAlias;

class AliasResolver extends CategoryResolver
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(ObjectManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function resolve(CanonicalCategory $category)
    {
        $entities = $this->em->getRepository(CategoryAlias::class)->findCategoryByAlias($category->getTitle());
        return $entities ?? $this->fallback($category);
    }
}