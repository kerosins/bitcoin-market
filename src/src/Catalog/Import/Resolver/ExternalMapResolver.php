<?php
/**
 * @author Kerosin
 */

namespace Catalog\Import\Resolver;


use Catalog\ORM\Entity\ExternalCategoriesMap;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Catalog\ORM\Entity\CanonicalCategory;

class ExternalMapResolver extends CategoryResolver
{
    private $em;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * @inheritdoc
     */
    public function resolve(CanonicalCategory $category)
    {
        $ourCategory = $this->em->getRepository(ExternalCategoriesMap::class)->findOneBy([
            'externalId' => $category->getId(),
            'source' => $category->getSource()
        ]);

        return $ourCategory ? [$ourCategory->getCategory()] : $this->fallback($category);
    }

}