<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Import\Component;


use Catalog\ORM\Entity\Product;
use Symfony\Component\EventDispatcher\Event;

class ProductUpdateEvent extends Event
{
    public const EVENT_NAME_AVAILABLE = 'import.product.update_available';

    public const EVENT_NAME_PRICE = 'import.product.update_price';

    /**
     * @var \Catalog\ORM\Entity\Product
     */
    private $product;

    /**
     * @var float
     */
    private $newPrice;

    /**
     * @var bool
     */
    private $productAvailable = true;

    /**
     * @return \Catalog\ORM\Entity\Product
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param \Catalog\ORM\Entity\Product $product
     */
    public function setProduct(Product $product): self
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return float
     */
    public function getNewPrice(): ?float
    {
        return $this->newPrice;
    }

    /**
     * @param float $newPrice
     */
    public function setNewPrice(float $newPrice): self
    {
        $this->newPrice = $newPrice;
        return $this;
    }

    /**
     * @return bool
     */
    public function isProductAvailable(): ?bool
    {
        return $this->productAvailable;
    }

    /**
     * @param bool $productAvailable
     */
    public function setProductAvailable(bool $productAvailable): self
    {
        $this->productAvailable = $productAvailable;
        return $this;
    }
}