<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Import\Command;


use Catalog\Import\AliExpress\AliExpressProductProvider;
use Catalog\ORM\Entity\AliExpressProduct;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Entity\Source;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Predis\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Lock\Factory;

class CheckAvailabilityCommand extends Command
{
    private const ITEMS_PACK_SIZE = 200;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Factory
     */
    private $lockFactory;

    /**
     * @var Client
     */
    private $redisClient;

    /**
     * @var AliExpressProductProvider
     */
    private $aliExpressProductProvider;

    private $redisKey = 'import:products:check_availability';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var int
     */
    private $maxAvailabilityFailureCount;

    public function __construct(
        ?string $name = null,
        LoggerInterface $logger,
        Factory $lockFactory,
        Client $redisClient,
        EntityManagerInterface $entityManager,
        int $maxAvailabilityFailureCount
    ) {
        parent::__construct($name);
        $this->logger = $logger;
        $this->lockFactory = $lockFactory;
        $this->redisClient = $redisClient;
        $this->entityManager = $entityManager;
        $this->maxAvailabilityFailureCount = $maxAvailabilityFailureCount;
    }

    protected function configure()
    {
        $this
            ->setName('import:products:check_availability')
            ->setDescription('Checks availability all products or specified product')
            ->addOption('product', 'p', InputOption::VALUE_OPTIONAL, 'Id of specified product')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Refresh previous results if they exist')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $mutex = $this->lockFactory->createLock('check_availability.lock');

        if (!$mutex->acquire()) {
            $this->logger->error('Process already running');
        }

        $keys = $this->redisClient->keys($this->redisKey);

        if ($input->getOption('force') || !$keys) {
            $this->redisClient->del([$this->redisKey]);
            $this->prepareProductList();
        }

//        exit;
        $this->process();
    }

    /**
     * Prepares list of products for check
     *
     * @return void
     */
    private function prepareProductList()
    {
        $this->output->writeln('Prepare list of products for check');
        $repository = $this->entityManager->getRepository(Product::class);
        $qb = $repository->createQueryBuilder('p')
            ->innerJoin('p.categories', 'c')
            ->andWhere('c.enabled = :is_enabled')
            ->andWhere('p.status = :active_status')
            ->andWhere('p.externalSource = :external_source')
            ->setParameter('external_source', Source::EXTERNAL_SOURCE_ALIEXPRESS)
            ->setParameter('active_status', Product::STATUS_ACTIVE)
            ->setParameter('is_enabled', true);
        $countQuery = clone $qb;

        $qb->select('p.id');
        $iterator = $qb->getQuery()->setHydrationMode(Query::HYDRATE_SCALAR)->iterate();

        $count = $countQuery->select('COUNT(p.id)')->getQuery()->getSingleScalarResult();

        $c = 0;
        $progressBar = new ProgressBar($this->output, $count);

        foreach ($iterator as $item) {
            $id = reset($item)['id'];
            $this->redisClient->sadd($this->redisKey, [$id]);
            $c++;

            if ($c > 1000) {
                $progressBar->advance(1000);
                $c = 0;
            }
        }

        $progressBar->finish();
        unset($progressBar);
    }

    /**
     * Checks available
     */
    private function process()
    {
        $this->output->writeln('Process check');
        $progressBar = new ProgressBar($this->output, $this->redisClient->scard($this->redisKey));
        $step = self::ITEMS_PACK_SIZE;

        $getIds = function ($step) {
            $result = [];
            for ($i = 0; $i < $step; $i++) {
                $id = $this->redisClient->spop($this->redisKey);
                if (!$id) {
                    return null;
                }
                $result[] = $id;
            }

            $result = array_filter($result);

            return $result;
        };

        $qb = $this->entityManager->createQueryBuilder()
            ->select('p')
            ->from(Product::class, 'p')
            ->andWhere('p.id IN (:ids)')
            ->orderBy('p.id', 'DESC');

        $ids = $getIds($step);

        while ($ids) {
            $mapProducts = $qb->setParameter('ids', $ids)->getQuery()->getResult();
            $ids = array_map(
                function (Product $item) {
                    return $item->getExternalId();
                },
                $mapProducts
            );

            $aliProducts = $this->aliExpressProductProvider->search($ids);
            /** @var Product[] $products */
            $products = $this->diff($aliProducts, $mapProducts);

            if ($products) {
                foreach ($products as $product) {
                    $this->entityManager->persist($product);
                }
                $this->entityManager->flush();
            }

            $progressBar->advance($step);
            $ids = $getIds($step);

            $this->entityManager->clear();
        }
    }

    /**
     * @param AliExpressProduct[] $aliProducts
     * @param Product[] $map
     *
     * @return Product[] - list of products those have diff
     */
    private function diff($aliProducts, $map): array
    {
        $diff = [];

        //builds pairs
        foreach ($map as $item) {
            foreach ($aliProducts as $aliProduct) {
                if ($aliProduct->getId() == $item->getExternalId()
                    && $this->diffPair($item, $aliProduct)
                ) {
                    $diff[] = $item;
                    continue 2;
                }
            }
        }

        return $diff;
    }

    /**
     * Checks diff between oir and aliexpress product
     *
     * @param Product $product
     * @param AliExpressProduct $aliExpressProduct
     * @return bool
     */
    private function diffPair(Product $product, AliExpressProduct $aliExpressProduct)
    {
        $logParams = [
            'ali_id' => $aliExpressProduct->getId(),
            'product_id' => $product->getExternalId(),
            'inner_availability' => $product->getStatus() === Product::STATUS_ACTIVE,
            'outer_availability' => $aliExpressProduct->isAvailable()
        ];

        if ($aliExpressProduct->isAvailable() !== $product->isActive()) {
            $this->logger->info('Found diff between aliexpress and our product', $logParams);
            $product->toggleStatus($aliExpressProduct->isAvailable());
            $product->setAmountItems($aliExpressProduct->getItems() ?? 0);

            return true;
        }

        $this->logger->info('Product availability are identical', $logParams);
        return false;
    }
}