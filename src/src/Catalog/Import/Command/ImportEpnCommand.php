<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Import\Command;

use Background\Worker\TaskCreator;
use Kerosin\Component\ContainerAwareCommand;
use Psr\Log\LoggerInterface;
use React\EventLoop\Factory;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Lock\Lock;

class ImportEpnCommand extends ContainerAwareCommand
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var bool
     */
    private $isLocked = false;

    /**
     * @var Lock
     */
    private $mutex;

    protected function configure()
    {
        $this
            ->setName('import:epn')
            ->setDescription('Catalog\Import xml from EPN service');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mutex = $this->getContainer()->get('app.flock.factory')->createLock('import_xml.lock');
//        $mutex = new LockHandler('import_xml.lock', $this->getContainer()->getParameter('data_dir'));
        if (!$mutex->acquire()) {
            $output->writeln('Catalog\Import process already running. Try later or kill current import');
            return;
        }

        $this->isLocked = true;
        $this->mutex = $mutex;

        $container = $this->getContainer();

        $this->logger = $container->get('monolog.logger.import');
        $this->logger->info('Catalog\Import process (EPN) has been started');

        //temp part
        $filePath = $container->getParameter('data_dir') . '/alidump_short.yml';
        //end temp part


        $taskCreator = $this->getContainer()->get(TaskCreator::class);
        $reactEventLoop = Factory::create();

        $taskCreator->addTaskWithPromise($reactEventLoop,
            'cmd_wrap_worker',
            [
                'cmd' => 'import:xml',
                'params' => [
                    'filePath' => $filePath,
                    'transformer' => ImportXmlCommand::TRANSFORMER_EPN
                ]
            ]
        )->then(function () use ($output) {
            $message = 'All files has been imported';

            $this->logger->info($message);
            $output->writeln($message);
        });

        $reactEventLoop->run();
    }
}