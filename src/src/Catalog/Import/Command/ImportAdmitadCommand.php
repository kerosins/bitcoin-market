<?php
/**
 * ImportAliCommand class file.
 */

namespace Catalog\Import\Command;

use Background\Worker\TaskCreator;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use Psr\Log\LoggerInterface;
use React\EventLoop\Factory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\LockHandler;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Lock\Lock;

class ImportAdmitadCommand extends ContainerAwareCommand
{
    private $xmlUris = [
        'http://export.admitad.com/ru/webmaster/websites/444976/products/export_adv_products/?user=xRay_TFB&code=8001933de9&feed_id=14107&format=xml&fcid=6115',
        'http://export.admitad.com/ru/webmaster/websites/444976/products/export_adv_products/?user=xRay_TFB&code=8001933de9&feed_id=14267&format=xml&fcid=6115',
        'http://export.admitad.com/ru/webmaster/websites/444976/products/export_adv_products/?user=xRay_TFB&code=8001933de9&feed_id=14280&format=xml&fcid=6115',
        'http://export.admitad.com/ru/webmaster/websites/444976/products/export_adv_products/?user=xRay_TFB&code=8001933de9&feed_id=14281&format=xml&fcid=6115',
        'http://export.admitad.com/ru/webmaster/websites/444976/products/export_adv_products/?user=xRay_TFB&code=8001933de9&feed_id=14282&format=xml&fcid=6115',
        'http://export.admitad.com/ru/webmaster/websites/444976/products/export_adv_products/?user=xRay_TFB&code=8001933de9&feed_id=14283&format=xml&fcid=6115',
        'http://export.admitad.com/ru/webmaster/websites/444976/products/export_adv_products/?user=xRay_TFB&code=8001933de9&feed_id=14284&format=xml&fcid=6115',
        'http://export.admitad.com/ru/webmaster/websites/444976/products/export_adv_products/?user=xRay_TFB&code=8001933de9&feed_id=14285&format=xml&fcid=6115',
        'http://export.admitad.com/ru/webmaster/websites/444976/products/export_adv_products/?user=xRay_TFB&code=8001933de9&feed_id=15830&format=xml&fcid=6115'
    ];

    protected function configure()
    {
        $this->setName('import:admitad')
            ->setDescription('Catalog\Import all xml files from admitad');
    }

    /**
     * @var LoggerInterface
     */
    private $logger;

    private $isLocked = false;

    /**
     * @var Lock
     */
    private $mutex;

    /**
     * Loading and parsing xml from Admitad
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $mutex = $this->getContainer()->get('app.flock.factory')->createLock('import_xml.lock');
//        $mutex = new LockHandler('import_xml.lock', $this->getContainer()->getParameter('data_dir'));
        if (!$mutex->acquire()) {
            $output->writeln('Catalog\Import process already running. Try later or kill current import');
            return;
        }

        $this->isLocked = true;
        $this->mutex = $mutex;

        $container = $this->getContainer();

        $this->logger = $container->get('monolog.logger.import');
        $this->logger->info('Catalog\Import process has been started');

        $dataDir = $container->getParameter('data_dir');
        $dataDir .= '/goods/admitad';

        //find old import and delete they
        $output->writeln('Clean of old price\'s lists');
        $filesIterator = new \FilesystemIterator($dataDir);
        foreach ($filesIterator as $fileInfo) {
            /** @var SplFileInfo $fileInfo */
            $extension = explode('.', $fileInfo->getFilename());
            $extension = end($extension);
            $extension = strtolower($extension);

            if (in_array($extension, ['xml', 'yml'])) {
                unlink($fileInfo->getPathname());
            }
        }

        $output->writeln('Load new files from Admitad');

        //load new import files
        $client = new Client();
        $fileNames = [];

        //create requests
        //todo нужно запускать импорт сразу после загрузки первого же файла
        $requests = (function () use ($dataDir, $client, &$fileNames, $output) {
            foreach ($this->xmlUris as $uri) {
                $fileName = $dataDir . '/' . md5($uri) . '.xml';
                $fileNames[] = $fileName;

                yield function ($poolOpts) use (
                    $client,
                    $uri,
                    $fileName,
                    $output
                ) {
                    $ch = fopen($fileName, 'w+');

                    $poolOpts = (array)$poolOpts;
                    $poolOpts['sink'] = $ch;

                    return $client->getAsync($uri, $poolOpts)->then(
                        function () use ($fileName, $output) {
                            $message = "File {$fileName} was loaded";
                            $output->writeln($message);
                            $this->logger->info($message);
                        }
                    );
                };
            }
        })();

        $pool = new Pool($client, $requests, ['concurrency' => 3]);
        $pool->promise()->wait();

        //import
        $output->writeln('All files has been loaded, starting parsing them');

        $taskCreator = $this->getContainer()->get(TaskCreator::class);
        $reactEventLoop = Factory::create();

        $tasks = array_map(function ($filePath) use ($taskCreator, $reactEventLoop) {
            return $taskCreator->addTaskWithPromise(
                $reactEventLoop,
                'cmd_wrap_worker',
                [
                    'cmd' => 'import:xml',
                    'params' => [
                        'filePath' => $filePath,
                        'transformer' => ImportXmlCommand::TRANSFORMER_ADMITAD
                    ]
                ]
            );
        }, $fileNames);

        $reactEventLoop->run();

        \React\Promise\all($tasks)
            ->done(function () use ($output) {
                $message = 'All files has been imported';
                $this->logger->info($message);
                $output->writeln($message);
            });
    }

    public function __destruct()
    {
        if ($this->isLocked && $this->mutex) {
            $this->mutex->release();
        }
    }
}