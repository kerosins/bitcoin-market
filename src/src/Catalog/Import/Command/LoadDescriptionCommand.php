<?php
/**
 * @author Kondaurov
 */
namespace Catalog\Import\Command;

use Catalog\Import\AliExpress\AliExpressPageProvider;
use Catalog\ORM\Entity\AliExpressPage;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Entity\ProductPrice;
use Catalog\ORM\Entity\Source;
use Doctrine\ORM\Query;
use Kerosin\Component\ContainerAwareCommand;
use Kerosin\Component\Image;
use Kerosin\DependencyInjection\Contract\LoggerContract;
use Kerosin\DependencyInjection\Contract\LoggerContractTrait;
use Kerosin\Doctrine\ORM\Persister;
use PHPHtmlParser\Dom;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadDescriptionCommand extends ContainerAwareCommand implements LoggerContract
{
    use LoggerContractTrait;

    protected function configure()
    {
        $this
            ->setName('import:load:description')
            ->addOption(
                'sizeBlock',
                's',
                InputOption::VALUE_OPTIONAL,
                'Amount of products per block',
                50
            );
    }

    private static $sellerBlockMapping = [
        'p' => 'sellerName',
        '.sumary-items span' => 'items',
        '.sumary-orders span' => 'orders',
        '.sumary-wishlist span' => 'wishList'
    ];

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Query $productQuery */
        $container = $this->getContainer();

        $persister = $container->get(Persister::class);
        $persister->registerEntityForDetach([
            Product::class,
            ProductPrice::class,
            AliExpressPage::class
        ]);
        $persister->disableDoctrineLogger();
        $persister->disableElasticPopulate();
        $size = (int)$input->getOption('sizeBlock');

        $logger = $this->logger;

        $pageProvider = $container->get(AliExpressPageProvider::class);

        $products = $this->getProducts($size);
        while (count($products)) {
            $begin = microtime(true);

            $urisMap = array_map(function (Product $product) {
                return [
                    'uri' => str_replace('www.', 'm.', $product->getExternalLink()),
                    'product' => $product
                ];
            }, $products);
            $uris = array_column($urisMap, 'uri');

            //parses main page of product
            $responses = $pageProvider->search($uris, [
                AliExpressPageProvider::OPTION_PAGE_TRANSFORMER => [$this, 'transformMainPage']
            ]);

            $disabledItems = array_filter($responses, function ($response) {
                return $response === null;
            });
            $logger->info('Count of disabled products ' . count($disabledItems));

            $responses = array_filter($responses, function ($response) {
                return $response !== null;
            });
            $logger->info('Count of enabled products ' . count($responses));

            $secondPageUris = array_column($responses, 'descriptionUrl');
            $secondPageUris = array_filter($secondPageUris);

            $secondResponses = $pageProvider->search(
                $secondPageUris,
                [
                    AliExpressPageProvider::OPTION_PAGE_TRANSFORMER => [$this, 'cleanDescription']
                ]
            );
            //merges data to one array and save
            foreach ($responses as &$response) {
                $response['description'] = isset($secondResponses[$response['descriptionUrl']]) ? $secondResponses[$response['descriptionUrl']] : '';
            }

            foreach ($urisMap as $map) {
                /** @var Product $product */
                $product = $map['product'];
                $uri = $map['uri'];

                if (array_key_exists($uri, $disabledItems)) {
                    $product->toggleStatus(false);
                    $persister->persist($product);
                    continue;
                }

                if (!isset($responses[$uri])) {
                    $this->logger->error('Unknown uri!', [
                        'uri' => $uri,
                    /*    'disabled' => array_keys($disabledItems),
                        'enabled' => array_keys($responses),
                        'original' => $uris*/
                    ]);
                    exit;
                }

                $response = $responses[$uri];
                $product->setShortDescription($response['shortDescription']);
                $product->setDescription($response['description']);

                $product->setSellerName($response['sellerName']);

                $product->setAmountOrders($response['orders']);
                $product->setAmountItems($response['items']);
                $product->setAmountWishLists($response['wishList']);

                $productPrice = $product->getPrice();
                if ($response['price']) {
                    $productPrice->setValue($response['price']);
                    $persister->persist($productPrice);
                }

                $product->setImages(array_map(
                    function ($image) use ($product) {
                        return (new Image())
                            ->setUrl($image)
                            ->setAlt($product->getTitle())
                            ->setTitle($product->getTitle());
                    }, $response['images']
                ));

                $persister->persist($product);
            }

            $persister->flush();

            $end = microtime(true) - $begin;
            $logger->info('Processed ' . $size . ' items, time: ' . $end);

            $products = $this->getProducts($size);
        }
//        $output->writeln('Time: ' . $end);
    }

    /**
     * @param $size
     *
     * @return Product[]
     */
    private function getProducts($size)
    {
        $products = $this
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository(Product::class)
            ->createQueryBuilder('p')
            ->andWhere('p.status = :status')
            ->andWhere('p.externalSource = :externalSource')
            ->setParameter('status', Product::STATUS_NEW)
            ->setParameter('externalSource', Source::EXTERNAL_SOURCE_ALIEXPRESS)
            ->setMaxResults($size)
            ->getQuery()
            ->getResult();

        $ids = array_map(function (Product $product) { return $product->getId(); }, $products);
        $this->logger->debug('Products', [implode(', ', $ids)]);

        return $products;
    }

    public function transformMainPage($html)
    {
        $dom = new Dom();
        $dom->loadStr($html, [
            'cleanupInput' => false
        ]);

        //checks 404
        $error = $dom->find('.ms-error .error-info');
        if (count($error)) {
            return null;
        }

        /** @var Dom\Collection $iFrame */
        $iFrame = $dom->find('amp-iframe');
        $result = [
            'descriptionUrl' => $iFrame->count() ? $iFrame->getAttribute('src') : null,
            'shortDescription' => '',
            'images' => [],
            'sellerName' => '',
            'items' => 0,
            'orders' => 0,
            'wishList' => 0,
            'price' => null
        ];

        //short description
        $shortDescription = $dom->find('.detail-description-wrap .description-list');
        if (count($shortDescription)) {
            $result['shortDescription'] = $shortDescription->innerHtml;
        }

        //images
        $images = $dom->find('#sku-carousel amp-img')->toArray();
        if (count($images)) {
            $result['images'] = array_map(function (Dom\AbstractNode $node) {
                return $node->getAttribute('src');
            }, $images);
        }

        //seller name, orders, etc
        $storeBlock = $dom->find('.stroe-info');
        if (count($storeBlock)) {
            /** @var Dom\HtmlNode $storeBlock */
            $storeBlock = $storeBlock[0];
            foreach (self::$sellerBlockMapping as $selector => $key) {
                $cnt = $storeBlock->find($selector);
                if (count($cnt) > 0) {
                    $result[$key] = $key === 'sellerName' ? $cnt[0]->text() : (int)$cnt[0]->text();
                } else {
                    $result[$key] = $key === 'sellerName' ? '' : 0;
                }
            }
        } else {
            $this->logger->warning('Store block not found');
        }

        $price = $dom->find('.current-price')->text();
        $result['price'] = AliExpressPageProvider::sanitizePrice($price);

        unset($dom);
        return $result;
    }

    /**
     * Clean description from styles and scripts, and some trash
     *
     * @param $html
     *
     * @return string
     */
    public function cleanDescription($html)
    {
        $html = mb_eregi_replace("<!doctype(.*?)>", '', $html);
        // strip out comments
        $html = mb_eregi_replace("<!--(.*?)-->", '', $html);
        // strip out cdata
        $html = mb_eregi_replace("<!\[CDATA\[(.*?)\]\]>", '', $html);
        // strip out <script> tags
        $html = mb_eregi_replace("<\s*script[^>]*[^/]>(.*?)<\s*/\s*script\s*>", '', $html);
        $html = mb_eregi_replace("<\s*script\s*>(.*?)<\s*/\s*script\s*>", '', $html);
        // strip out <style> tags
        $html = mb_eregi_replace("<\s*style[^>]*[^/]>(.*?)<\s*/\s*style\s*>", '', $html);
        $html = mb_eregi_replace("<\s*style\s*>(.*?)<\s*/\s*style\s*>", '', $html);
        $html = mb_eregi_replace("<link(.*?)>", '', $html);

        return $html;
    }
}