<?php
/**
 * @author Kondaurov
 */

namespace Catalog\Import\Command;


use Catalog\Import\Exception\EpnException;
use Catalog\Import\Service\EpnAvailableRequestPerformer;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Entity\Source;
use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Entity\SystemSetting;
use Kerosin\SystemSettings\SettingsManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EpnApiTestCommand extends Command
{
    private $name = 'catalog:import:epn_api_health_test';

    /**
     * @var SettingsManager
     */
    private $settingsManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var EpnAvailableRequestPerformer
     */
    private $epnAvailableRequestPerformer;

    public function __construct(
        EntityManagerInterface $entityManager,
        EpnAvailableRequestPerformer $epnAvailableRequestPerformer,
        SettingsManager $settingsManager,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->epnAvailableRequestPerformer = $epnAvailableRequestPerformer;
        $this->settingsManager = $settingsManager;
        $this->logger = $logger;
        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName($this->name)
            ->setDescription('Checks if EPN API is down');
    }

    /**
     * @inheritdoc
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Kerosin\SystemSettings\Exception\SystemSettingException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Begin check EPN API health');

        /** @var Product $product */
        $product = $this->entityManager->getRepository(Product::class)->createQueryBuilder('p')
            ->andWhere('p.status = :status and p.externalSource = :extSource')
            ->setParameter('status', Product::STATUS_ACTIVE)
            ->setParameter('extSource', Source::EXTERNAL_SOURCE_ALIEXPRESS)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        $currentSettingAvailableCheck = $this->settingsManager->getValue(SystemSetting::CHECK_PRODUCT_AVAILABLE);

        try {
            $this->epnAvailableRequestPerformer->perform([$product->getExternalId()]);
        } catch (EpnException $e) {
            $output->writeln('EPN API is down');
            $this->logger->error($e->getMessage(), [
                'trace' => $e->getTraceAsString()
            ]);
            if ($currentSettingAvailableCheck) {
                $this->settingsManager->setValue(SystemSetting::CHECK_PRODUCT_AVAILABLE, false);
            }

            return;
        }

        $output->writeln('EPN API is available');

        if (!$currentSettingAvailableCheck) {
            $this->settingsManager->setValue(SystemSetting::CHECK_PRODUCT_AVAILABLE, true);
        }
    }
}