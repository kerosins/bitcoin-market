<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Catalog\Import\Command;

use Doctrine\ORM\Query;
use Kerosin\Component\ContainerAwareCommand;
use Kerosin\Doctrine\ORM\Persister;
use Catalog\ORM\Entity\Product;
use Shop\Entity\Seller;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RearrangeSuppliersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('import:rearrange_suppliers')
            ->setDescription('Rearrange suppliers from Aliexpress');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $begin = microtime(true);
        $container = $this->getContainer();

        $persister = $container->get(Persister::class);
        $persister->disableElasticPopulate();

        $logger = $container->get('logger.public');
        $logger->info('Start');

        $em = $this->getEntityManager();
        $productClass = Product::class;
        $productRepo = $this->getEntityManager()->getRepository($productClass);

        $supplierClass = Seller::class;

        $dql = "
SELECT p.sellerName FROM {$productClass} p
  LEFT JOIN {$supplierClass} s WITH LOWER(s.name) = LOWER(p.sellerName)
WHERE s.name IS NULL AND p.sellerName IS NOT NULL 
GROUP BY p.sellerName
ORDER BY p.sellerName ASC";

        $sellers = $em->createQuery($dql)->getResult(Query::HYDRATE_ARRAY);
        $sellers = array_column($sellers, 'sellerName');

        $logger->info('Got untouched suppliers', [
            'count' => count($sellers)
        ]);

        while (count($sellers) > 0) {
            $seller = array_shift($sellers);
            $seller = trim($seller);
            $em->beginTransaction();

            try {
                $sBegin = microtime(true);

                $supplier = new Seller();
                $supplier->setName($seller);
                $supplier->setInternal(true);
                $em->persist($supplier);

                $products = $productRepo->findBy(['sellerName' => $seller]);

                foreach ($products as $product) {
                    /** @var \Catalog\ORM\Entity\Product $product */
                    $product->setSeller($supplier);
                    $product->setSellerName(null);
                    if ($product->getStatus() === Product::STATUS_NEW) {
                        $product->setStatus(Product::STATUS_ACTIVE);
                    }
                    $em->persist($product);
                }

                $em->flush();
                $em->commit();
                $em->clear();

                $logger->info("Rearange products for supplier", [
                    'name' => $supplier->getName(),
                    'countProducts' => count($products),
                    'time' => (microtime(true) - $sBegin)
                ]);
            } catch (\Exception $e) {
                $em->rollback();
                $logger->error($e->getMessage(), [
                    'trace' => $e->getTraceAsString()
                ]);
            }

            $em->clear();
        }

        $logger->info('Done', [
            'time' => (microtime(true) - $begin)
        ]);
    }
}