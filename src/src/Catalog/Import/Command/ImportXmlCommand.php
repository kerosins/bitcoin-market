<?php
/**
 * @author Kerosin
 */

namespace Catalog\Import\Command;

use Catalog\Import\Loader\LoaderFactory;
use Catalog\Import\Loader\LoaderParameters;
use Catalog\Import\Loader\Transformer\AdmitadTransformer;
use Catalog\Import\Loader\Transformer\EpnTransformer;
use Catalog\Import\Service\ProductKeeper;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Entity\ProductYmlFile;
use Catalog\ORM\Entity\Source;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Monolog\Handler\StreamHandler;
use Predis\Client;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * todo sed 's/[[:cntrl:]]//g' data/alidump.yml > data/alidump.yml.fixed
 *
 * Class ImportXmlCommand
 * @package Catalog\Import\Command
 */
class ImportXmlCommand extends ContainerAwareCommand
{
    public const
        TRANSFORMER_ADMITAD = 'admitad',
        TRANSFORMER_EPN = 'epn';

    private static $transformers = [
        self::TRANSFORMER_ADMITAD => AdmitadTransformer::class,
        self::TRANSFORMER_EPN => EpnTransformer::class
    ];

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ProductKeeper
     */
    private $productKeeper;

    /**
     * @var LoaderFactory
     */
    private $loaderFactory;

    /**
     * @var Logger|LoggerInterface
     */
    private $logger;

    /**
     * @var Client
     */
    private $redis;

    /**
     * @var string
     */
    private $logsDir;

    /**
     * @var OutputInterface
     */
    private $output;

    public function __construct(
        ?string $name = null,
        EntityManagerInterface $entityManager,
        ProductKeeper $productKeeper,
        LoaderFactory $loaderFactory,
        LoggerInterface $logger,
        Client $redis,
        string $logsDir
    ) {
        parent::__construct($name);

        $this->entityManager = $entityManager;
        $this->productKeeper = $productKeeper;
        $this->loaderFactory = $loaderFactory;
        $this->logger = $logger;
        $this->redis = $redis;
        $this->logsDir = $logsDir;
    }

    protected function configure()
    {
        $this->setName('import:xml')
            ->addOption('filePath', 'p', InputOption::VALUE_OPTIONAL, 'Path to xml file')
            ->addOption('fileId', 'i', InputOption::VALUE_OPTIONAL, 'Id of import file')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force upload (refresh state)')
            ->addOption('blockSize', 's', InputOption::VALUE_OPTIONAL, 'Size of block for insert|update', 100)
            ->addOption('transformer', 't', InputOption::VALUE_REQUIRED, 'Transformer\'s Name');

        $this->setDescription('Run a Catalog\Import XML(like yml) file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        $filePath = $input->getOption('filePath');
        $fileId = $input->getOption('fileId');

        if (!$filePath && !$fileId) {
            throw new \RuntimeException('filePath or fileId options required');
        }

        $loaderParameters = new LoaderParameters();
        $loaderParameters
            ->setTransformer($input->getOption('transformer'))
            ->setFilePath($filePath)
            ->setProductFileId($fileId)
            ->setForce($input->getOption('force'));

        $loader = $this->loaderFactory->build($loaderParameters);
        $loader->run();

        //prepare logger
        $this->prepareLogger($loader->getProductFile()->getId());

        $this->logger->info('Import begin from offset ' . $loader->getProductFile()->getOffset());

        //prepare keeper
        $productKeeper = $this->getContainer()->get(ProductKeeper::class);
        $size = $input->getOption('blockSize');

        $canonical = [];
        $this->preProcessImport($loader->getProductFile(), $input->getOption('force'));
        //load active products to db
        $cacheKey = $this->buildCacheKey($loader->getProductFile()->getId());

        //todo fixme dirty hack for save products
        $flush = function ($canonical) use ($productKeeper, $loader, $cacheKey) {
            $ids = $productKeeper->keep($canonical);
            $this->redis->srem($cacheKey, $ids);

            $productYmlFile = $loader->getProductFile();
            $this->entityManager->persist($productYmlFile);
            $this->entityManager->flush($productYmlFile);
        };

        //process file
        $this->logger->info("Begin process file from {$loader->getProductFile()->getOffset()} node position");
        $pb = new ProgressBar($this->output);
        $pb->start();
        try {
            foreach ($loader->products() as $product) {
                if (!$product) {
                    break;
                }

                $canonical[] = $product;

                if (count($canonical) === $size) {
                    $flush($canonical);
                    $canonical = [];
                    $pb->advance($size);
                }
            }

            if ($canonical) {
                $flush($canonical);
            }

            $pb->finish();
            $this->output->writeln('');

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [
                'trace' => $e->getTraceAsString()
            ]);

            exit(1);
        }

        $this->postProcessImport($loader->getProductFile());
    }

    /**
     * Collects a list of active products
     *
     * @param ProductYmlFile $productYmlFile
     * @param bool $force
     */
    private function preProcessImport(ProductYmlFile $productYmlFile, bool $force = false)
    {
        $this->logger->info('Prepare list of active products');

        $cacheKey = $this->buildCacheKey($productYmlFile->getId());
        if (!$force && $this->redis->exists($cacheKey)) {
            $this->logger->info('List of products already exists');
            return;
        }

        $this->redis->del([$cacheKey]);

        $qb = $this->entityManager
            ->getRepository(Product::class)
            ->createQueryBuilderForActiveProducts('p')
            ->andWhere('p.source = :source AND p.externalSource = :external_source')
            ->setParameter('source', Source::EPN_YML)
            ->setParameter('external_source', Source::EXTERNAL_SOURCE_ALIEXPRESS)
            ->select('p.id')
        ;

        $countQb = clone $qb;
        $countQuery = $countQb
            ->select('COUNT(p.id)')
            ->getQuery()
            ->setHydrationMode(Query::HYDRATE_SINGLE_SCALAR);

        $countProducts = $countQuery->getResult();
        $countProducts = $countProducts[0][1];

        $productIds = [];
        $size = 1000;
        $iterable = $qb->getQuery()->setHydrationMode(Query::HYDRATE_SCALAR)->iterate();

        $this->logger->info("Found {$countProducts} active products");
        $progressBar = new ProgressBar($this->output, $countProducts);
        $progressBar->start();
        $progressBar->setMessage('Prepare list...');

        foreach ($iterable as $product) {
            $productIds[] = reset($product)['id'];

            if (count($productIds) > $size) {
                $this->redis->sadd(
                    $cacheKey,
                    $productIds
                );

                $productIds = [];
                $progressBar->advance($size);
            }
        }

        if ($productIds) {
            $this->redis->sadd(
                $cacheKey,
                $productIds
            );
        }

        $this->entityManager->clear();

        $progressBar->finish();
        $this->output->writeln('');

        $this->logger->info('List of active products has been filled', [
            'count' => $countProducts
        ]);
    }

    /**
     * Set status = unavailable for all products from list
     *
     * @param ProductYmlFile $productFile
     */
    private function postProcessImport(ProductYmlFile $productFile)
    {
        $query = $this->entityManager->getRepository(Product::class)
            ->createQueryBuilder('p')
            ->andWhere('p.id IN (:ids)')
            ->getQuery();

        $count = $this->redis->scard($this->buildCacheKey($productFile->getId()));
        $this->logger->info("Set for {$count} products unavailable status");

        $progressBar = new ProgressBar($this->output, $count);
        $progressBar->start();

        foreach ($this->iterateUnavailableList($productFile) as $ids) {
            /** @var Product[] $products */
            $products = $query->setParameter('ids', $ids)->getResult();

            foreach ($products as $product) {
                $product->setStatus(Product::STATUS_UNAVAILABLE);
            }

            $this->entityManager->flush();
            $this->entityManager->clear();

            $progressBar->advance(1000);
        }

        $progressBar->finish();

        $productFile->setStatus(ProductYmlFile::STATUS_DONE);
        $this->entityManager->flush();
    }

    /**
     * Iterates are unavailable ids from redis list
     *
     * @param ProductYmlFile $productYmlFile
     * @param int $batchSize
     *
     * @return \Generator
     */
    private function iterateUnavailableList(ProductYmlFile $productYmlFile, int $batchSize = 1000)
    {
        $cacheKey = $this->buildCacheKey($productYmlFile->getId());
        $counter = 0;
        $ids = [];
        while (($id = $this->redis->spop($cacheKey)) !== null) {
            $ids[] = $id;
            $counter++;

            if ($counter > $batchSize) {
                $counter = 0;
                yield $ids;
            }
        }

        if ($ids) {
            yield $ids;
        }
    }

    /**
     * builds a key
     *
     * @param int $id
     *
     * @return string
     */
    private function buildCacheKey(int $id)
    {
        return 'import:products:' . $id;
    }

    /**
     * prepare logger
     *
     * @param int $productFileId
     *
     * @throws \Exception
     */
    private function prepareLogger(int $productFileId)
    {
        $logPath = $this->logsDir . DS . 'import';
        $logFileName = "import-log-{$productFileId}.log.txt";

        $this->logger->pushHandler(new StreamHandler($logPath . DS . $logFileName, Logger::DEBUG, false));
        $this->logger->pushHandler(new StreamHandler('php://stdout', Logger::DEBUG, false));
    }
}