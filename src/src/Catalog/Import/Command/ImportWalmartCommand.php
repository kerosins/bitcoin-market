<?php
/**
 * @author Kerosin
 */

namespace Catalog\Import\Command;


use Catalog\Import\Loader\WalmartLoader;
use Catalog\Import\Service\ProductKeeper;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * todo not implemented
 *
 * Class ImportWalmartCommand
 * @package Catalog\Import\Command
 */
class ImportWalmartCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('import:walmart')
            ->addOption('category', 'c', InputOption::VALUE_OPTIONAL, 'Category ID')
            ->addOption('flushState', 's', InputOption::VALUE_NONE, 'Flush previous state of load')
            ->setDescription('Catalog\Import goods from walmart');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $loader = $this->getContainer()->get(WalmartLoader::class);
        $category = $input->getOption('category');
        $flushState = (bool)$input->getOption('flushState');

        $loader->run(['category' => $category, 'flushState' => $flushState]);

        $keeper = $this->getContainer()->get(ProductKeeper::class);

        $size = 100;
        $canonical = [];

        foreach ($loader->products() as $product) {
            $canonical[] = $product;

            if (count($canonical) === $size) {
                $keeper->keep($canonical);
                $canonical = [];
            }
        }

        $keeper->keep($canonical);
    }
}