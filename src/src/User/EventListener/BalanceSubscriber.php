<?php
/**
 * @author Kondaurov
 */

namespace User\EventListener;


use Kerosin\Mailer\CustomMailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use User\Entity\BalanceChangeLog;
use User\Event\BalanceEvent;
use User\Repository\BalanceChangeLogRepository;

class BalanceSubscriber implements EventSubscriberInterface
{
    /**
     * @var BalanceChangeLogRepository
     */
    private $changeLogRepository;

    /**
     * BalanceSubscriber constructor.
     *
     * @param BalanceChangeLogRepository $changeLogRepository
     */
    public function __construct(
        BalanceChangeLogRepository $changeLogRepository
    ) {
        $this->changeLogRepository = $changeLogRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            BalanceEvent::NAME => 'onChangeBalance'
        ];
    }

    /**
     * @param BalanceEvent $event
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onChangeBalance(BalanceEvent $event)
    {
        $changeLog = (new BalanceChangeLog())
            ->setUser($event->getUser())
            ->setTransactionHash($event->getTransaction())
            ->setAmount($event->getTransaction()->getAmount())
            ->setStatus($event->getStatus())
            ->setType($event->getType())
            ->setComment($event->getComment());

        if ($event->getWithdrawal()) {
            $changeLog->setWithdrawal($event->getWithdrawal());
        }

        $this->changeLogRepository->save($changeLog);
    }
}