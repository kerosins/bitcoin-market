<?php
/**
 * @author Kondaurov
 */
namespace User\Entity;

use Billing\Entity\Account;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Referral\Entity\ReferralProgram;
use Shop\Entity\Country;

/**
 * Class User
 * @package Shop\Entity
 *
 * @ORM\Entity(repositoryClass="User\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    public const ROLE_USER = 'ROLE_USER';

    public const ROLE_ADMIN = 'ROLE_ADMIN';

    public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    public const ROLE_FAKE_USER = 'ROLE_FAKE_USER';

    public const SYSTEM_USERNAME = 'system';

    public static $readableRoles = [
        self::ROLE_USER => 'User',
        self::ROLE_FAKE_USER => 'Fake user',
        self::ROLE_ADMIN => 'Admin',
        self::ROLE_SUPER_ADMIN => 'Super Admin'
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Country|null
     *
     * @ORM\ManyToOne(targetEntity="Shop\Entity\Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $country;

    /**
     * @var ReferralProgram
     *
     * @ORM\OneToOne(targetEntity="Referral\Entity\ReferralProgram", mappedBy="user")
         */
    private $referralProgram;

    /**
     * @var Account
     *
     * @ORM\OneToOne(targetEntity="Billing\Entity\Account", cascade={"persist"})
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $billingAccount;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $timezone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        parent::__construct();
        $this->createdAt = new \DateTime();
    }

    /**
     * Set country
     *
     * @param \Shop\Entity\Country $country
     *
     * @return User
     */
    public function setCountry(\Shop\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Shop\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return ReferralProgram
     */
    public function getReferralProgram(): ?ReferralProgram
    {
        return $this->referralProgram;
    }

    /**
     * @param ReferralProgram $referralProgram
     */
    public function setReferralProgram(ReferralProgram $referralProgram)
    {
        $this->referralProgram = $referralProgram;
        return $this;
    }

    /**
     * @return Account
     */
    public function getBillingAccount(): ?Account
    {
        return $this->billingAccount;
    }

    /**
     * @param Account $billingAccount
     *
     * @return User
     */
    public function setBillingAccount(Account $billingAccount): User
    {
        $this->billingAccount = $billingAccount;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     *
     * @return User
     */
    public function setTimezone(string $timezone): self
    {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getReadableRoles()
    {
        return array_map(function (string $role) {
            return self::$readableRoles[$role] ?? 'Unknown Role';
        }, $this->getRoles());
    }
}
