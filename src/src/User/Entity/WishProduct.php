<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace User\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;
use Catalog\ORM\Entity\Product;

/**
 * Class WishProduct
 * @package User\Entity
 *
 * @ORM\Entity(repositoryClass="User\Repository\WishProductRepository")
 * @ORM\Table(name="user_wish_product")
 *
 * @ORM\HasLifecycleCallbacks()
 */
class WishProduct
{
    use BaseMapping;
    use UpdateTimestamps;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $user;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Catalog\ORM\Entity\Product", cascade={"persist"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $product;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return WishProduct
     */
    public function setUser(User $user): WishProduct
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param \Catalog\ORM\Entity\Product $product
     *
     * @return WishProduct
     */
    public function setProduct(Product $product): WishProduct
    {
        $this->product = $product;
        return $this;
    }
}