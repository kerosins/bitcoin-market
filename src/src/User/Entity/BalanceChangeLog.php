<?php
/**
 * @author Kondaurov
 */

namespace User\Entity;
use Billing\Entity\Transaction;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Referral\Entity\ReferralWithdrawal;

/**
 * Class BalanceChangeLog
 * @package User\Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="User\Repository\BalanceChangeLogRepository")
 */
class BalanceChangeLog
{
    use BaseMapping;

    /**
     * Constants of statuses
     */
    public const
        STATUS_DEPOSIT = 0,
        STATUS_WITHDRAWAL_REQUEST = 1,
        STATUS_WITHDRAWAL_ACCEPT = 2,
        STATUS_WITHDRAWAL_PROCESS = 3,
        STATUS_WITHDRAWAL_CANCEL = 4;

    public const
        TYPE_DEPOSIT = 0,
        TYPE_WITHDRAWAL = 1;

    public static $readableType = [
        self::TYPE_DEPOSIT => 'Deposit',
        self::TYPE_WITHDRAWAL => 'Withdrawal'
    ];

    public static $readableStatus = [
        self::STATUS_DEPOSIT => 'Deposit Processed',
        self::STATUS_WITHDRAWAL_REQUEST => 'Withdrawal Request',
        self::STATUS_WITHDRAWAL_ACCEPT => 'Withdrawal Accepted',
        self::STATUS_WITHDRAWAL_PROCESS => 'Withdrawal Processed',
        self::STATUS_WITHDRAWAL_CANCEL => 'Withdrawal Cancelled'
    ];

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var Transaction
     *
     * @ORM\ManyToOne(targetEntity="Billing\Entity\Transaction")
     * @ORM\JoinColumn(name="transaction_hash", referencedColumnName="id", onDelete="CASCADE")
     */
    private $transactionHash;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=15, scale=5)
     */
    private $amount;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @var ReferralWithdrawal
     * @ORM\ManyToOne(targetEntity="Referral\Entity\ReferralWithdrawal")
     * @ORM\JoinColumn(name="withdrawal_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $withdrawal;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return BalanceChangeLog
     */
    public function setUser(User $user): BalanceChangeLog
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Transaction
     */
    public function getTransactionHash(): Transaction
    {
        return $this->transactionHash;
    }

    /**
     * @param Transaction $transactionHash
     *
     * @return BalanceChangeLog
     */
    public function setTransactionHash(Transaction $transactionHash): BalanceChangeLog
    {
        $this->transactionHash = $transactionHash;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     *
     * @return BalanceChangeLog
     */
    public function setAmount(float $amount): BalanceChangeLog
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return BalanceChangeLog
     */
    public function setStatus(int $status): BalanceChangeLog
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param ?string $comment
     *
     * @return BalanceChangeLog
     */
    public function setComment(?string $comment): BalanceChangeLog
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return string
     */
    public function getReadableStatus()
    {
        return self::$readableStatus[$this->status] ?? 'Unknown';
    }

    /**
     * @return int
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return BalanceChangeLog
     */
    public function setType(int $type): BalanceChangeLog
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getReadableType(): string
    {
        return self::$readableType[$this->type] ?? 'Unknown';
    }

    /**
     * @return ReferralWithdrawal
     */
    public function getWithdrawal(): ?ReferralWithdrawal
    {
        return $this->withdrawal;
    }

    /**
     * @param ReferralWithdrawal $withdrawal
     *
     * @return BalanceChangeLog
     */
    public function setWithdrawal(ReferralWithdrawal $withdrawal): BalanceChangeLog
    {
        $this->withdrawal = $withdrawal;
        return $this;
    }
}