<?php
/**
 * @author kerosin
 */

namespace User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kerosin\Component\GeoObject;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;
use Catalog\ORM\Entity\DeliveryInfo;
use Shop\Entity\Country;
use Shop\Entity\ZipCode;

/**
 * Class UserAddress
 * @package Shop\Entity
 *
 * @ORM\Entity(repositoryClass="User\Repository\UserDeliveryInfoRepository")
 * @ORM\Table(name="user_delivery_info")
 *
 * @ORM\HasLifecycleCallbacks()
 */
class UserDeliveryInfo extends DeliveryInfo
{
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    public function getName()
    {
        return sprintf(
            "%s, %s: %s, %s, %s",
            $this->getFirstName(),
            $this->getLastName(),
            $this->getState()->getTitle(),
            $this->getCity()->getTitle(),
            $this->getAddress()
        );
    }

    /**
     * Set user
     *
     * @param \User\Entity\User $user
     *
     * @return UserDeliveryInfo
     */
    public function setUser(\User\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'country' => [
                'id' => $this->getCountry()->getId(),
                'title' => $this->getCountry()->getTitle(),
            ],
            'state' => $this->getState(),
            'city' => $this->getCity(),
            'zip' => $this->getZip(),
            'address' => $this->getAddress(),
            'apartment' => $this->getApartment()
        ];

        return $data;
    }
}
