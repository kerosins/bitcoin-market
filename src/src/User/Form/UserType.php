<?php
/**
 * UserType class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace User\Form;

use Doctrine\ORM\EntityRepository;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\Form\DataTransformer\ArrayToStringTransformer;
use Shop\Entity\Country;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Context\ExecutionContext;
use User\Component\Validator\UniqueEmail;
use User\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * TODO Add validation here
 *
 * Class UserType
 * @package User\Form
 */
class UserType extends AbstractType implements EntityManagerContract
{
    use EntityManagerContractTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Regex([
                        'pattern' => '/^[[:alnum:]]+$/',
                        'message' => 'Username can contains only following chars a-Z,0-9'
                    ])
                ]
            ])
            ->add('email', EmailType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Email()
                ]
            ])
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'choice_label' => 'title',
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('c')
                        ->orderBy('c.title', 'ASC');
                }
            ])
            ->add('plainPassword', RepeatedType::class, [
                'first_options' => [
                    'label' => 'Password',
                    'attr' => [
                        'autocomplete' => 'off'
                    ]
                ],
                'second_options' => [
                    'label' => 'Repeat Password',
                    'attr' => [
                        'autocomplete' => 'off'
                    ]
                ],
                'required' => !$options['admin_mode'],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^[[:alnum:][:punct:]]+$/',
                        'message' => 'Password contains invalid characters',
                    ]),
                    new Length([
                        'min' => 6
                    ])
                ]
            ])
        ;

        if ($options['admin_mode']) {
            $builder->add('enabled');
            $builder->add('roles', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'choices' => [
                    'User' => User::ROLE_USER,
                    'Admin' => User::ROLE_ADMIN,
                ]
            ]);

//            $builder->get('roles')->addModelTransformer(new ArrayToStringTransformer(','));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'admin_mode' => false,
            'constraints' => new Callback([
                'callback' => [$this, 'validate']
            ]),
            'attr' => [
                'autocomplete' => 'off'
            ]
        ]);
        $resolver->setAllowedTypes('admin_mode', 'bool');
    }

    public function validate(User $user, ExecutionContext $context)
    {
        //validates email, username
        $repo = $this->entityManager->getRepository(User::class);
        $id = $user->getId();

        if ($repo->existUserWithEmail($user->getEmail(), $id)) {
            $context
                ->buildViolation('This email already used')
                ->atPath('email')
                ->addViolation()
            ;
        }

        if ($repo->existByUsername($user->getUsername(), $id)) {
            $context
                ->buildViolation('This username already used')
                ->atPath('username')
                ->addViolation();
        }
    }
}