<?php
/**
 * @author Kondaurov
 */

namespace User\Form\Filter;


use Kerosin\Form\BaseFormFilter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use User\Entity\User;
use User\Provider\UserProvider;

class UserFilterForm extends BaseFormFilter
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('query', TextType::class, [
                'label' => 'Username, Email or Id',
                'required' => false
            ])
            ->add('role', ChoiceType::class, [
                'label' => 'Role',
                'required' => false,
                'choices' => array_flip(User::$readableRoles)
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', UserProvider::class);
    }
}