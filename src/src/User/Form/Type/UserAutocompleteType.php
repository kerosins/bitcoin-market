<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace User\Form\Type;


use Kerosin\Form\Type\AutoCompleteType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use User\Entity\User;

/**
 * Class UserAutocompleteType
 * @package User\Form\Type
 *
 * Provides functionality for input with users autocomplete
 */
class UserAutocompleteType extends AbstractType
{
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('entity_class', User::class)
            ->setDefault('ajax_url', $this->router->generate('cp.userAutocomplete'))
            ->setDefault('text_attr', 'username');
        ;
    }

    public function getParent()
    {
        return AutoCompleteType::class;
    }
}