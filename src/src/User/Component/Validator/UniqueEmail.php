<?php
/**
 * @author Kerosin
 */

namespace User\Component\Validator;

use Symfony\Component\Validator\Constraint;

class UniqueEmail extends Constraint
{
    public $message = 'User with %email% already exists';
}