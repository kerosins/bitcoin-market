<?php
/**
 * UserController class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace User\Controller;

use Billing\Service\AccountManager;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Kerosin\Component\AutocompleteTransformer;
use Kerosin\Controller\BaseController;
use Kerosin\Mailer\CustomMailer;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Referral\Service\ReferralCreator;
use Referral\Service\ReferralManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use User\Entity\User;
use User\Form\Filter\UserFilterForm;
use User\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use User\Provider\UserProvider;
use User\Service\SystemUser;

class UserController extends BaseController
{
    public function registrationAction(
        Request $request,
        CustomMailer $customMailer,
        UserPasswordEncoderInterface $encoder,
        AccountManager $accountManager
    ) {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        $entityManager = $this->getEm();

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->addRole(User::ROLE_USER)->setEnabled(true);

            $entityManager->beginTransaction();

            try {
                $billingAccount = $accountManager->create();

                $user->setBillingAccount($billingAccount);

                $this->flushEntities($user);
                $customMailer->newUser($user);
                $entityManager->commit();
            } catch (\Exception $e) {
                $entityManager->rollback();
                throw $e;
            }

            return $this->redirectToRoute('shop.user.success_registration');
        }

        return $this->render('@User/Registration/registration.twig', [
            'form' => $form->createView()
        ]);
    }

    public function successRegistrationAction()
    {
        return $this->render('@User/Registration/success_register.twig');
    }

    //cp actions

    public function listAction(
        UserProvider $provider,
        Request $request
    ) {
        $this->addBreadCrumbsFromArray([
            ['Users', $this->generateUrl('cp.user.index')]
        ]);

        $filterForm = $this->createForm(UserFilterForm::class, $provider);
        $filterForm->handleRequest($request);

        $provider->setSortBy($request->get('sortBy'))
                ->setDirection($request->get('direction'));

        return $this->render('@User/User/list.twig', [
            'users' => $provider->search(),
            'filterForm' => $filterForm->createView(),
            'sortKeys' => $provider->getSortKeys()
        ]);
    }

    /**
     * Create or edit in Control Panel
     *
     * @param EntityManager $em
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createEditAction(
        ObjectManager $em,
        Request $request,
        UserPasswordEncoderInterface $encoder,
        SystemUser $systemUser,
        $id = null
    ) {
        if ($id) {
            $entity = $this->getUserById($id);
            if (!$entity) {
                throw new NotFoundHttpException();
            }

        } else {
            $entity = new User();
        }

        $form = $this->createForm(UserType::class, $entity, ['admin_mode' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($systemUser->getSystemUser()->getId() === $entity->getId()) {
                throw new \RuntimeException('"System User" is lock for any updates');
            }

            if ($entity->getPlainPassword()) {
                $password = $encoder->encodePassword($entity, $entity->getPlainPassword());
                $entity->setPassword($password);
            }

            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('cp.user.index');
        }

        return $this->render('@User/User/form.twig', [
            'isCreate' => !(bool)$id,
            'user' => $entity,
            'form' => $form->createView()
        ]);
    }

    public function deleteAction(ObjectManager $em, $id)
    {
        $entity = $this->getUserById($id);
        if (!$entity) {
            throw new NotFoundHttpException();
        }

        $em->remove($entity);
        return $this->redirectToRoute('cp.user.index');
    }

    public function autocompleteAction(Request $request, PaginatorInterface $paginator)
    {
        $builder = $this->getEm()->getRepository(User::class)->createQueryBuilder('u');

        $builder
            ->andWhere('u.enabled = :enabled')
            ->setParameter('enabled', true)
            ->orderBy('u.username', 'ASC');

        $query = $request->get('q');
        if ($query) {
            $builder
                ->andWhere('lower(u.username) LIKE :username')
                ->setParameter('username', '%' . mb_strtolower($query) . '%');
        }

        $paginated = $paginator->paginate(
            $builder->getQuery(),
            $request->get('page') ?? 1
        );

        return $this->json(
            AutocompleteTransformer::fromPaginatedResult($paginated, 'id', 'username')
        );
    }

    /**
     * Get an User
     * @param $id
     * @return null|User
     */
    private function getUserById($id)
    {
        return $this->get('doctrine.orm.entity_manager')->getRepository('UserBundle:User')->find($id);
    }
}