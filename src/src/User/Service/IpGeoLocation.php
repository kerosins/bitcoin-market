<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace User\Service;


use GuzzleHttp\Client;

/**
 * Use http://ip-api.com for locations
 *
 * Class IpLocation
 * @package User\Service
 */
class IpGeoLocation
{
    /**
     * @param $ip
     *
     * @return array
     */
    public function whereis($ip)
    {
        $client = new Client(['base_uri' => 'http://ip-api.com/json/']);

        $response = $client->get($ip);

        $body = (string)$response->getBody();
        return json_decode($body, true);
    }
}