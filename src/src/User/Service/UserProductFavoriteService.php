<?php
/**
 * @author Kerosin
 */

namespace User\Service;

use Catalog\ORM\Entity\Product;

class UserProductFavoriteService
{

    public function __construct()
    {
    }

    public function init()
    {
    }

    public function refresh()
    {
    }

    /**
     * Add product to favorite
     *
     * @param \Catalog\ORM\Entity\Product|int $product
     */
    public function add($product)
    {
    }

    /**
     * Remove product from favorites
     *
     * @param \Catalog\ORM\Entity\Product|int $product
     */
    public function remove($product)
    {
    }

    /**
     * Has product in favorites
     *
     * @param \Catalog\ORM\Entity\Product|int
     */
    public function hasFavorite($product)
    {
    }

    private function buildCollection()
    {
    }
}