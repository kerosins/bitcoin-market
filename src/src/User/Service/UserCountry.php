<?php
/**
 * @author Kerosin
 */

namespace User\Service;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Shop\Entity\Country;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use User\Entity\User;

class UserCountry
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var string
     */
    private $sessionKey;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        Session $session,
        ObjectManager $entityManager,
        string $sessionKey = 'user_country'
    ) {
       $this->session = $session;
       $this->tokenStorage = $tokenStorage;
       $this->entityManager = $entityManager;
       $this->sessionKey = $sessionKey;
    }

    /**
     * Set a country to session
     *
     * @param Country $country
     */
    public function set(Country $country)
    {
        $this->session->set($this->sessionKey, $country);
    }

    /**
     * Get an user country
     *
     * @return Country
     */
    public function get()
    {
        /** @var Country $country */
        $country = $this->session->get($this->sessionKey);
        if (!$country) {
            $user = $this->tokenStorage->getToken()->getUser();
            $country = $user instanceof User && $user->getCountry() ? $user->getCountry() : $this->getDefaultCountry();
            $this->set($country);
        }

        $country = $this->entityManager->merge($country);

        return $country;
    }

    /**
     * Unset country from session
     */
    public function clear()
    {
        $this->session->remove($this->sessionKey);
    }

    /**
     * Get default Country
     *
     * @return Country
     */
    public function getDefaultCountry()
    {
        return $this->entityManager->getRepository(Country::class)
            ->createQueryBuilder('c')
            ->andWhere('lower(c.code) = :default')
            ->setParameter('default', 'usa')
            ->getQuery()
            ->getOneOrNullResult();
    }
}