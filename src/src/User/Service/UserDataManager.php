<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace User\Service;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Catalog\Order\Component\DeliveryInfoTransmitter;
use Catalog\ORM\Entity\OrderDeliveryInfo;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use User\Entity\User;
use User\Entity\UserDeliveryInfo;
use User\Exception\UserManagerException;

/**
 * Manages of user data, e.g delivery info
 *
 * Class UserDataManager
 * @package User\Service
 */
class UserDataManager
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var \User\Repository\UserRepository
     */
    private $userRepo;

    /**
     * @var \User\Repository\UserDeliveryInfoRepository
     */
    private $userDeliveryRepo;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(
        ObjectManager $entityManager,
        UserPasswordEncoderInterface $encoder
    ) {
        $this->em = $entityManager;
        $this->encoder = $encoder;
        $this->userRepo = $this->em->getRepository('UserBundle:User');
    }

    /**
     * Lazy getter UserRepository
     *
     * @return \User\Repository\UserRepository
     */
    private function getUserRepo()
    {
        if (!$this->userRepo) {
            $this->userRepo = $this->em->getRepository('UserBundle:User');
        }

        return $this->userRepo;
    }

    /**
     * Lazy getter UserDeliveryInfoRepository
     *
     * @return \User\Repository\UserDeliveryInfoRepository
     */
    private function getUserDeliveryRepo()
    {
        if (!$this->userDeliveryRepo) {
            $this->userDeliveryRepo = $this->em->getRepository('UserBundle:UserDeliveryInfo');
        }

        return $this->userDeliveryRepo;
    }

    /**
     * Creates a new delivery info from order
     */
    public function createDeliveryInfoFromOrder(User $user, OrderDeliveryInfo $info)
    {
        $userInfo = new UserDeliveryInfo();

        DeliveryInfoTransmitter::transmit($info, $userInfo);
        $userInfo->setUser($user);

        $this->em->persist($userInfo);
        $this->em->flush($userInfo);
    }

    /**
     * Create a disabled user account with the random password
     *
     * @param User $user
     * @throws UserManagerException
     *
     * @return User
     */
    public function createHiddenAccountIfNotExists(User $user)
    {
        if ($user->getId() && $this->em->contains($user)) {
            throw new UserManagerException('User already exists');
        }

        $existedUser = $this->userRepo->findOneByEmail($user->getEmail());
        if ($existedUser) {
            if ($existedUser->isEnabled()) {
                throw new UserManagerException("User with email {$existedUser->getEmail()} already exist and he isn't hidden");
            }

            return $existedUser;
        }

        if ($user->getUsername() === null) {
            $user->setUsername(
                $this->createUsernameViaEmail($user->getEmail())
            );
        }

        $user->addRole(User::ROLE_DEFAULT);
        $user->setEnabled(false);

        $password = $this->encoder->encodePassword($user, uniqid('', false));
        $user->setPassword($password);

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * todo implement
     *
     * Enable previous created user
     *
     * @param User $user
     */
    public function enableHiddenUser(User $user)
    {

    }

    /**
     * Automatic create username via email
     *
     * @param string $email
     * @return string - auto generated username
     */
    private function createUsernameViaEmail($email)
    {
        $username = explode('@', $email);
        $username = reset($username);

        $username = uniqid($username, false);

        if ($this->userRepo->existByUsername($username)) {
            return $this->createUsernameViaEmail($email);
        }

        return $username;
    }
}