<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace User\Service;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\DependencyInjection\Contract\TaggedCacheContract;
use Kerosin\DependencyInjection\Contract\TaggedCacheContractTrait;
use Catalog\ORM\Entity\Product;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use User\Entity\User;
use User\Entity\WishProduct;
use User\Repository\WishProductRepository;

/**
 * Class WishProductManager
 * @package User\Service
 *
 * @todo add cache
 */
class WishProductManager implements EntityManagerContract, TaggedCacheContract
{
    use EntityManagerContractTrait, TaggedCacheContractTrait;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var User|null
     */
    private $user;

    /**
     * @var Collection
     */
    private $list;

    /**
     * @var WishProductRepository
     */
    private $wishRepo;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return null|User
     */
    private function getUser()
    {
        if (($token = $this->tokenStorage->getToken()) === null) {
            throw new \RuntimeException('This client must be authenticated');
        }

        if (!$this->user) {
            $this->user = $token->getUser();
        }

        return $this->user;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|WishProductRepository
     */
    private function getWishListRepo()
    {
        if (!$this->wishRepo) {
            $this->wishRepo = $this->entityManager->getRepository(WishProduct::class);
        }

        return $this->wishRepo;
    }

    /**
     * @param \Catalog\ORM\Entity\Product $product
     */
    public function add(Product $product, bool $flush = true)
    {
        if ($this->has($product)) {
            return false;
        }

        $wish = new WishProduct();

        $wish->setUser($this->getUser())
            ->setProduct($product);

        $this->entityManager->persist($wish);

        if ($flush) {
            $this->entityManager->flush();
        }

        $this->invalidate();

        return true;
    }

    /**
     * @param \Catalog\ORM\Entity\Product|int $product
     */
    public function drop($product)
    {
        $this->getWishListRepo()->deleteByUserAndProduct($this->getUser(), $product);
        $this->invalidate();
    }

    /**
     * @param \Catalog\ORM\Entity\Product|int $product
     */
    public function has($product)
    {
        if ($product instanceof Product) {
            $product = $product->getId();
        }

        return $this
                ->getList()
                ->filter(function (WishProduct $wishProduct) use ($product) {
                    return $wishProduct->getProduct()->getId() === $product;
                })
                ->count() > 0;
    }

    /**
     * Gets list of wish
     *
     * @return Collection|mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getList()
    {
        //exist in property
        if ($this->list) {
            return $this->list;
        }

        $user = $this->getUser();
        if (!$user instanceof User) {
            return new ArrayCollection();
        }

        $key = 'wish_list_user_' . $user->getId();

        $cacheItem = $this->taggedCache->getItem($key);

        //exist in cache
        if ($cacheItem->get() !== null) {
            $this->list = $cacheItem->get();
            return $this->list;
        }

        //find in db and set to cache
        $list = $this->getWishListRepo()->findByUser($user);
        $list = new ArrayCollection($list);

        $cacheItem->set($list);
        $cacheItem->tag($this->buildTags($user->getId()));

        $this->list = $list;
        return $this->list;
    }

    /**
     * @param int $userId
     * @return array
     */
    private function buildTags(int $userId)
    {
        return ['wish_list_user_' . $userId];
    }

    /**
     * invalidates wishlist for current user
     *
     * @return bool
     */
    public function invalidate()
    {
        $this->list = null;
        return $this->taggedCache->invalidateTags(
            $this->buildTags(
                $this->getUser()->getId()
            )
        );
    }

    /**
     * Invalidates for specified user
     *
     * @param User|int $user
     *
     * @return bool
     */
    public function invalidateForUser($user)
    {
        $user = ($user instanceof User) ? $user->getId() : $user;

        return $this->taggedCache->invalidateTags(
            $this->buildTags(
                $user
            )
        );
    }
}