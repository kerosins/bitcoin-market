<?php
/**
 * @author Kerosin
 */

namespace User\Service;

use Doctrine\Common\Persistence\ObjectManager;
use User\Entity\User;

class SystemUser
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var SystemUser
     */
    private $systemUser;

    public function __construct(ObjectManager $manager)
    {
        $this->em = $manager;
    }

    /**
     * @return User
     */
    public function getSystemUser()
    {
        if (!$this->systemUser) {
            $this->systemUser = $this
                ->em
                ->getRepository('UserBundle:User')
                ->findOneBy(['username' => User::SYSTEM_USERNAME])
            ;
        }

        return $this->systemUser;
    }
}