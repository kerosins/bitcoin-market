<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace User\Service;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\DependencyInjection\Contract\TaggedCacheContract;
use Kerosin\DependencyInjection\Contract\TaggedCacheContractTrait;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use User\Entity\User;

/**
 * Provide count something for specified user
 *
 * Class BadgeProvider
 * @package User\Service
 */
class BadgeManager implements TaggedCacheContract, EntityManagerContract
{
    use TaggedCacheContractTrait, EntityManagerContractTrait;

    public const
        TYPE_ORDER_MESSAGE = 'order_message',
        TYPE_DISPUTE_MESSAGE = 'dispute_message';

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Gets a count
     *
     * @param string $type
     * @param User $user
     * @return int|mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getCount(string $type, User $user)
    {
        $key = sprintf(
            'badge_%s_user_%d',
            strtolower($type),
            $user->getId()
        );
        $badge = $this->taggedCache->getItem($key);

        if (!$badge->isHit()) {
            return 0;
        }
        $result = $badge->get();
        $this->taggedCache->deleteItem($key);

        return $result;
    }

    public function incForUser(User $user, string $type, int $amount = 1)
    {
        $key = sprintf(
            'badge_%s_user_%d',
            strtolower($type),
            $user->getId()
        );
        $badge = $this->taggedCache->getItem($key);

        if (!$badge->isHit()) {
            $badge->set($amount);
        } else {
            $badge->set($badge->get() + $amount);
        }

        $this->taggedCache->save($badge);
    }

    /**
     * @param string $type
     * @param string $role
     */
    public function incForRole(string $role, string $type, int $amount = 1)
    {
        $users = $this->entityManager->getRepository(User::class)->findByRole($role);

        foreach ($users as $user) {
            $this->incForUser($user, $type, $amount);
        }
    }
}