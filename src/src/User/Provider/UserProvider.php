<?php
/**
 * @author Kondaurov
 */

namespace User\Provider;


use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;
use User\Entity\User;

class UserProvider extends BaseProvider
{
    /**
     * @var string
     */
    private $query;

    /**
     * @var string
     */
    private $role;

    private $sortBy = 'id';

    private $direction = SORT_ASC;

    private static $availableSortKeys = ['id', 'username', 'createdAt'];

    private static $sortByMap = [
        'id' => 'u.id',
        'username' => 'u.username',
        'createdAt' => 'u.createdAt'
    ];

    public function search(): PaginationInterface
    {
        $qb = $this->em->getRepository(User::class)->createQueryBuilder('u');

        if ($this->query) {
            $query = strtolower($this->query);
            $id = (int)$this->query;
            $qb
                ->andWhere('lower(u.username) like :query OR lower(u.email) like :query OR u.id = :id')
                ->setParameter('query', "%{$query}%")
                ->setParameter('id', $id);
        }

        if ($this->role) {//dirty hack
            if ($this->role !== User::ROLE_USER) {
                $qb->andWhere('lower(u.roles) like :role')
                    ->setParameter('role', '%' . strtolower($this->role) . '%');
            } else {
                $qb->andWhere('u.roles = :role_user')
                    ->setParameter('role_user', 'a:0:{}');
            }
        }

        $qb->orderBy(
            self::$sortByMap[$this->sortBy],
            ($this->direction === SORT_ASC ? 'ASC' : 'DESC')
        );

        return $this->buildPaginationResult($qb->getQuery());
    }

    /**
     * @return string
     */
    public function getQuery(): ?string
    {
        return $this->query;
    }

    /**
     * @param string $query
     *
     * @return UserProvider
     */
    public function setQuery(string $query): UserProvider
    {
        $this->query = $query;
        return $this;
    }

    /**
     * @return string
     */
    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * @param string $role
     *
     * @return UserProvider
     */
    public function setRole(string $role): UserProvider
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSortBy()
    {
        return $this->sortBy;
    }

    /**
     * @param mixed $sortBy
     *
     * @return UserProvider
     */
    public function setSortBy($sortBy = null)
    {
        $this->sortBy = in_array($sortBy, self::$availableSortKeys) ? $sortBy : 'id';
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param mixed $direction
     *
     * @return UserProvider
     */
    public function setDirection($direction = null)
    {
        $this->direction = in_array($direction, [SORT_ASC, SORT_DESC]) ? (int)$direction : SORT_ASC;
        return $this;
    }

    public function getSortKeys()
    {
        $keys = [];
        foreach (self::$availableSortKeys as $sortKey) {
            $keys[$sortKey] = [
                'key' => $sortKey,
                'direction' => ($this->sortBy === $sortKey ? $this->direction : SORT_DESC)
            ];
        }

        return $keys;
    }
}