<?php
/**
 * @author Kondaurov
 */

namespace User\Doctrine\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Referral\Entity\ReferralProgram;
use Referral\Service\ReferralCreator;
use User\Entity\User;

/**
 * Class UserCreateListener
 * @package User\Doctrine\EventListener
 *
 */
class UserCreateListener
{
    /**
     * @var ReferralCreator
     */
    private $referralCreator;

    public function __construct(ReferralCreator $creator)
    {
        $this->referralCreator = $creator;
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     */
    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();

        if (!$entity instanceof User) {
            return;
        }

        $this->referralCreator->attachReferralProgram($entity, true);
    }
}