<?php
/**
 * @author Kondaurov
 */

namespace User\Event;

use Billing\Entity\Transaction;
use Referral\Entity\ReferralWithdrawal;
use Symfony\Component\EventDispatcher\Event;
use User\Entity\User;

class BalanceEvent extends Event
{

    public const NAME = 'user.change_balance';

    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * @var User
     */
    private $user;

    /**
     * @var int
     */
    private $type;

    /**
     * @var int
     */
    private $status;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var ReferralWithdrawal
     */
    private $withdrawal;

    public function __construct(
        Transaction $transaction,
        User $user,
        int $type,
        int $status,
        string $comment
    )
    {
        $this->transaction = $transaction;
        $this->user = $user;
        $this->type = $type;
        $this->status = $status;
        $this->comment = $comment;
    }

    /**
     * @return Transaction
     */
    public function getTransaction(): ?Transaction
    {
        return $this->transaction;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return ReferralWithdrawal
     */
    public function getWithdrawal(): ?ReferralWithdrawal
    {
        return $this->withdrawal;
    }

    /**
     * @param ReferralWithdrawal $withdrawal
     *
     * @return BalanceEvent
     */
    public function setWithdrawal(ReferralWithdrawal $withdrawal): BalanceEvent
    {
        $this->withdrawal = $withdrawal;
        return $this;
    }
}