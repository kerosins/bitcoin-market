<?php
/**
 * UserRepository class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace User\Repository;

use Billing\Entity\Account;
use Doctrine\ORM\Query;
use Kerosin\Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use User\Entity\User;

/**
 * Class UserRepository
 * @package User\Repository
 *
 * @method \User\Entity\User findOneByEmail(string $email)
 * @method User findOneByBillingAccount(Account|int $account)
 */
class UserRepository extends EntityRepository implements UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.username = :username or u.email = :email')
            ->setParameters([
                'username' => $username,
                'email' => $username
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function findAllQuery()
    {
        return $this->createQueryBuilder('u')
            ->getQuery();
    }

    /**
     * Check by username if user is exist
     *
     * @param string $username
     * @param int|null $excludeId
     *
     * @return bool
     */
    public function existByUsername($username, int $id = null)
    {
        $query = $this->createQueryBuilder('u')
            ->andWhere('lower(u.username) = :username')
            ->setParameter('username', strtolower($username))
            ->setMaxResults(1);

        if ($id) {
            $query->andWhere('u.id != :exclude')->setParameter('exclude', $id);
        }

        return (bool)$query->getQuery()->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

	/**
     * Check if user with presented email exist
     *
     * @param string $email
     * @param int|null $excludeId
     *
     * @return bool
     */
    public function existUserWithEmail($email, int $excludeId = null)
    {
        $query = $this->createQueryBuilder('u')
            ->andWhere('lower(u.email) = :email')
            ->andWhere('u.enabled = :enabled')
            ->setParameter('enabled', true)
            ->setParameter('email', strtolower($email))
            ->setMaxResults(1)
        ;

        if ($excludeId) {
            $query->andWhere('u.id != :exclude')->setParameter('exclude', $excludeId);
        }

        return (bool)$query->getQuery()->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    /**
     * Finds users by their role
     *
     * @param string $role
     *
     * @return User[]
     */
    public function findByRole(string $role)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('lower(u.roles) like :role')
            ->setParameter('role', '%' . strtolower($role) . '%')
            ->andWhere('u.enabled = :enable')
            ->setParameter('enable', true)
            ->getQuery()
            ->getResult();
    }
}