<?php
/**
 * @author Kondaurov
 */

namespace User\Repository;

use Kerosin\Doctrine\ORM\EntityRepository;
use User\Entity\User;

class BalanceChangeLogRepository extends EntityRepository
{
    /**
     * @param User $user
     *
     * @return \Doctrine\ORM\Query
     */
    public function createByUserQuery(User $user)
    {
        return $this->createQueryBuilder('bcl')
            ->andWhere('bcl.user = :user')
            ->setParameter('user', $user)
            ->orderBy('bcl.createdAt', 'DESC')
            ->getQuery();
    }
}