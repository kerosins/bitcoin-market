<?php
/**
 * @Author Dmitry Kondaurov
 */
namespace User\Repository;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Query;
use Kerosin\Doctrine\ORM\EntityRepository;
use Test\Fixture\Entity\Shop\Product;
use User\Entity\User;
use User\Entity\WishProduct;

/**
 * Class WishProductRepository
 * @package User\Repository
 *
 * @method Collection findByUser(User|int $user)
 */
class WishProductRepository extends EntityRepository
{

    /**
     * @param User|int $user
     * @param Product|int $product
     * @return bool
     */
    public function existByUserAndProduct($user, $product)
    {
        return null !== $this->createQueryBuilder('wp')
            ->select('wp.id')
            ->andWhere('wp.product = :product and wp.user = :user')
            ->setParameters([
                'product' => $product,
                'user' => $user
            ])
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    /**
     * Deletes by user and product
     *
     * @param $user
     * @param $product
     *
     * @return mixed
     */
    public function deleteByUserAndProduct($user, $product)
    {
        return $this->createQueryBuilder('wp')
            ->delete(WishProduct::class, 'wp')
            ->andWhere('wp.product = :product AND wp.user = :user')
            ->setParameters([
                'product' => $product,
                'user' => $user
            ])
            ->getQuery()
            ->execute();
    }
}