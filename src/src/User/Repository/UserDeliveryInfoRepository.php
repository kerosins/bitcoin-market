<?php
/**
 * @author Kerosin
 */

namespace User\Repository;

use Kerosin\Doctrine\ORM\EntityRepository;

/**
 * Class UserDeliveryInfoRepository
 * @package User\Repository
 *
 * @method findByUser(\User\Entity\User $user)
 */
class UserDeliveryInfoRepository extends EntityRepository
{
    public function buildFindByUserQuery($user)
    {
        return $this->createQueryBuilder('udi')
            ->andWhere('udi.user = :user')
            ->setParameter('user', $user)
            ->orderBy('udi.createdAt', 'DESC')
            ->getQuery();
    }
}