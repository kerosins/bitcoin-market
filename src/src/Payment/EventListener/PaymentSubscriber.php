<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Payment\EventListener;


use Payment\Component\PaymentEvent;
use Payment\Repository\PaymentRepository;
use Payment\Service\PaymentManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PaymentSubscriber implements EventSubscriberInterface
{
    /**
     * @var PaymentRepository
     */
    private $paymentRepository;
    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * PaymentSubscriber constructor.
     * @param PaymentRepository $paymentRepository
     * @param PaymentManager $paymentManager
     */
    public function __construct(
        PaymentRepository $paymentRepository,
        PaymentManager $paymentManager
    ) {
        $this->paymentRepository = $paymentRepository;
        $this->paymentManager = $paymentManager;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            PaymentEvent::EVENT_NAME_EXPIRE => 'onPaymentExpire'
        ];
    }

    /**
     * @param PaymentEvent $event
     * @throws \Payment\Exception\PaymentManagerException
     */
    public function onPaymentExpire(PaymentEvent $event)
    {
        $payment = $event->getPayment();
        $this->paymentManager->cancelPayment($payment);
    }
}