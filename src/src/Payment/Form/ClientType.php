<?php
/**
 * @author Kerosin
 */

namespace Payment\Form;

use Doctrine\Common\Persistence\ObjectManager;
use Payment\Provider\ExchangeClientProvider;
use Payment\Provider\ExchangeCriteria;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    /** @var ObjectManager */
    private $em;

    /** @var ExchangeClientProvider  */
    private $exchangeProvider;

    public function __construct(ObjectManager $manager, ExchangeClientProvider $provider)
    {
        $this->em = $manager;
        $this->exchangeProvider = $provider;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $clients = $this->exchangeProvider->search(new ExchangeCriteria(['onlyActive' => false]));
        $choices = [];
        foreach ($clients as $meta) {
            /** @var \Payment\Exchange\ClientMeta $meta */
            $choices[$meta->getClassName()] = $meta->getClassName();
        }

        $resolver->setDefaults([
            'choices' => $choices,
            'expanded' => true
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}