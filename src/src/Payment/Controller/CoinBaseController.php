<?php
/**
 * @author Kondaurov
 */

namespace Payment\Controller;


use Kerosin\Controller\BaseController;
use Payment\Component\Exchange\Client\CoinBase\CoinBaseWebHook;
use Payment\Exception\PaymentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CoinBaseController extends BaseController
{
    public function webhookAction(CoinBaseWebHook $coinBaseWebHook, Request $request)
    {
        $logger = $this->get('monolog.logger.business');
        $env = $this->getParameter('kernel.environment');

        try {
            if ($env === 'dev') {
                $coinBaseWebHook->log($request);
            }
            $coinBaseWebHook->process($request);
        } catch (PaymentException $e) {
            $logger->error($e->getMessage());
        }

        return new Response('Ok');
    }
}