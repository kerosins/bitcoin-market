<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Payment\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Controller\BaseController;
use Payment\Entity\PaybearPayment;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PaybearController extends BaseController
{
    public function callbackAction(Request $request, EntityManagerInterface $entityManager)
    {
        /** @var LoggerInterface $logger */
        $logger = $this->get('monolog.logger.business');
        $content = $request->getContent();

        $response = new Response();

        if (!$content) {
            $logger->error('Empty request to paybear callback');
            return $response;
        }

        $logger->info('Request to PayBear callback', [
            'request' => $content
        ]);

        $json = json_decode($content, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            $logger->error('Invalid json in request to paybear callback', [
                'request_body' => $content,
                'error_number' => json_last_error(),
                'error_message' => json_last_error_msg()
            ]);
            return $response;
        }

        try {
            $this->validateRequestData($json);
        } catch (\RuntimeException $e) {
            $logger->error($e->getMessage());
            return $response;
        }

        $repo = $entityManager->getRepository(PaybearPayment::class);
        $payment = $repo->findOneBy([
            'invoice' => $json['invoice'],
            'isDone' => false
        ]);

        if (!$payment) {
            $logger->error('Payment is already done or not found');
            $response->setContent($json['invoice']);
            return $response;
        }

        //checks request data
        $diff = $this->diff($payment, $json);
        if ($diff) {
            $logger->error('Isset difference between request and known data in paybear callback', [
                'diff' => $diff
            ]);
            return $response;
        }

        $payment->setConfirmationNumber($json['confirmations']);
        $payment->setBlock($json['block']);
        $payment->setTransaction($json['inTransaction']);

        $payment->setIsDone(($json['confirmations'] >= $json['maxConfirmations']));

        $entityManager->persist($payment);
        $entityManager->flush();

        if ($payment->getIsDone()) {
            $response->setContent($payment->getInvoice());
        }

        return $response;
    }

    /**
     * todo may be implement use Form
     *
     * @param array $data
     * @throws \RuntimeException
     */
    private function validateRequestData(array $data)
    {
        $requiredKeys = [
            'invoice',
            'confirmations',
            'maxConfirmations',
            'blockchain',
            'block',
            'inTransaction'
        ];

        foreach ($requiredKeys as $key) {
            if (!array_key_exists($key, $data)) {
                throw new \RuntimeException("Required key '{$key}' is undefined");
            }
        }
    }

    /**
     * Checks diff between known data and request
     *
     * @param PaybearPayment $payment
     * @param $requestData
     * @return array
     */
    private function diff(PaybearPayment $payment, $requestData)
    {
        $diff = [];

        $addDiff = function ($key, $entity, $request) use (&$diff) {
            $diff[$key] = [
                'entity' => $entity,
                'request' => $request
            ];
        };

        if (strcasecmp($payment->getCurrency(), $requestData['blockchain']) !== 0) {
            $addDiff('currency', $payment->getCurrency(), $requestData['blockchain']);
        }

        $requestAmount = (float)((int)($requestData['inTransaction']['amount']) / pow(10, $requestData['inTransaction']['exp']));

        if (!isset($requestData['inTransaction']['amount'])
            || $requestAmount !== $payment->getAmount()
        ) {
            $addDiff('amount', $payment->getAmount(), $requestAmount);
        }

        return $diff;
    }
}