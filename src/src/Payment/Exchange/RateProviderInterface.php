<?php
/**
 * @author kerosin
 */

namespace Payment\Exchange;

/**
 * Provide rates of currencies USD\{CryptCurrency}
 *
 * Interface RateProviderInterface
 * @package Currency
 */
interface RateProviderInterface extends ClientInterface
{
    public function getRate(array $currencies = []) : array;
}