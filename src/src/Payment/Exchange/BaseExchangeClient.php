<?php
/**
 * @author kerosin
 */

namespace Payment\Exchange;


use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;

/**
 * Class BaseExchangeClient
 * @package Payment\Exchange
 *
 * @deprecated
 */
class BaseExchangeClient
{
    protected $url;

    /**
     * @var Client
     */
    private $client;

    /**
     * Get an exchange name
     *
     * @var string
     */
    private $name;

    /**
     * @param $url
     * @param array $query
     */
    protected function request($url, $query = [], $jsonResponse = true)
    {
        $response = $this->getHttpClient()->get($url, [
            'query' => $query
        ]);

        if ($jsonResponse) {
            return json_decode($response->getBody(), true);
        }


        return $response->getBody();
    }

    /**
     * Get guzzle
     *
     * @return Client
     */
    protected function getHttpClient()
    {
        if (!$this->client) {
            $this->client = new Client(['base_uri' => $this->url]);
        }

        return $this->client;
    }

    /**
     * Get exchange's name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return static
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

}