<?php
/**
 * @author kerosin
 */
namespace Payment\Exchange;

use Payment\Entity\Payment;

interface PaymentProviderInterface extends ClientInterface
{
    /**
     * Create transaction on exchange service
     *
     * @param Payment $payment
     * @return \Payment\Entity\Payment
     */
    public function processPayment(Payment $payment) : Payment;

    /**
     * Return a status of payment
     *
     * @return mixed
     */
    public function getPaymentStatus(string $transactionId);
}