<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Payment\Exchange;


interface ClientInterface
{
    /**
     * Return array of supported currencies
     *
     * @return array
     */
    public function getCurrencies(): array;

    /**
     * Sets an exchange's system name
     *
     * @param string $name
     * @return mixed
     */
    public function setName(string $name);

    /**
     * Gets an exchange's name
     *
     * @return string
     */
    public function getName(): string;
}