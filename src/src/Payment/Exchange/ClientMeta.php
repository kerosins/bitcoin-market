<?php
/**
 * @author kerosin
 */

namespace Payment\Exchange;


use Payment\Exchange\BaseExchangeClient;
use Payment\Entity\Exchange;
use Payment\Exception\UnknownExchangeClientException;
use Payment\Exchange\PaymentProviderInterface;
use Payment\Exchange\RateProviderInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class ClientMeta
{
    private static $interfaceDescription = [
        RateProviderInterface::class => 'Provide are currency rates',
        PaymentProviderInterface::class => 'Provide payments'
    ];

    private $features = [];

    private $currencies = [];

    /**
     * @var \Payment\Exchange\BaseExchangeClient
     */
    private $className = null;

    /**
     * @var \ReflectionClass
     */
    private $reflection;

    /**
     * @var \Payment\Exchange\BaseExchangeClient
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
        $this->className = get_class($client);
        try {
            $this->reflection = new \ReflectionClass($this->className);
            $this->fillMeta();
        } catch (\RuntimeException|\Exception $e) {
            throw new UnknownExchangeClientException($this->className, $e->getCode(), $e);
        }
    }

    /**
     * Fill meta data about client
     */
    private function fillMeta()
    {
        $this->currencies = $this->client->getCurrencies();

        $interfaces = [];
        foreach ($this->reflection->getInterfaces() as $interfaceName) {
            $interfaces[] = $interfaceName->getName();
        }

        $features = array_map(function ($interface) {
            return array_key_exists($interface, self::$interfaceDescription) ? self::$interfaceDescription[$interface] : null;
        }, $interfaces);

        $features = array_combine($interfaces, $features);

        $features = array_filter($features, function ($description) {
            return $description !== null;
        });

        $this->features = $features;
    }

    /**
     * Check if client has feature
     *
     * @param string $interface
     * @return bool
     */
    public function hasFeature(string $interface) : bool
    {
        return array_key_exists($interface, $this->features);
    }

    /**
     * Check if client has features
     *
     * @param array $interfaces
     * @return bool
     */
    public function hasFeatures(array $interfaces) : bool
    {
        foreach ($interfaces as $interface) {
            if (!$this->hasFeature($interface)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Check if client works with the currency
     *
     * @param string $currency
     * @return bool
     */
    public function hasCurrency(string $currency) : bool
    {
        return in_array($currency, $this->currencies);
    }

    /**
     * Check if client works with are currencies
     *
     * @param array $currencies
     * @return bool
     */
    public function hasCurrencies(array $currencies) : bool
    {
        foreach ($currencies as $currency) {
            if ($this->hasCurrency($currency)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getFeatures(): array
    {
        return $this->features;
    }

    /**
     * @return array
     */
    public function getCurrencies(): array
    {
        return $this->currencies;
    }

    /**
     * @return null
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * Get a client
     *
     * @return \Payment\Exchange\BaseExchangeClient
     */
    public function getClient()
    {
        return $this->client;
    }

}