<?php
/**
 * @author kerosin
 */

namespace Payment\Provider;


use Symfony\Component\PropertyAccess\PropertyAccessor;

class ExchangeCriteria
{
    /**
     * @var array
     */
    private $features = [];

    /**
     * @var array
     */
    private $currencies = [];

    /**
     * @var array
     */
    private $excludeClients = [];

    /**
     * @var bool
     */
    private $onlyActive = true;

    /**
     * ExchangeCriteria constructor.
     * @param array $criteria
     */
    public function __construct($criteria = [])
    {
        $accessor = new PropertyAccessor();
        foreach ($criteria as $name => $value) {
            if ($accessor->isWritable($this, $name)) {
                $accessor->setValue($this, $name, $value);
            }
        }
    }

    /**
     * @return array
     */
    public function getFeatures(): array
    {
        return $this->features;
    }

    /**
     * @param array $features
     */
    public function setFeatures(array $features)
    {
        $this->features = $features;
        return $this;
    }

    /**
     * @return array
     */
    public function getCurrencies(): array
    {
        return $this->currencies;
    }

    /**
     * @param array $currencies
     */
    public function setCurrencies(array $currencies)
    {
        $this->currencies = $currencies;
        return $this;
    }

    /**
     * @return array
     */
    public function getExcludeClients(): array
    {
        return $this->excludeClients;
    }

    /**
     * @param array $excludeClients
     */
    public function setExcludeClients(array $excludeClients)
    {
        $this->excludeClients = $excludeClients;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOnlyActive(): bool
    {
        return $this->onlyActive;
    }

    /**
     * @param bool $onlyActive
     */
    public function setOnlyActive(bool $onlyActive)
    {
        $this->onlyActive = $onlyActive;
        return $this;
    }
}