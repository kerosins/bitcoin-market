<?php
/**
 * @author kerosin
 */

namespace Payment\Provider;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Payment\Exchange\ClientInterface;
use Payment\Exchange\ClientMeta;
use Payment\Exchange\BaseExchangeClient;
use Payment\Entity\Exchange;

class ExchangeClientProvider
{
    /**
     * @var ObjectManager
     */
    private $em;

    private $clients = [];

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param ClientInterface $client
     * @return $this
     */
    public function addExchangeClient(ClientInterface $client)
    {
        $this->clients[$client->getName()] = $client;
        return $this;
    }

    /**
     * @param ExchangeCriteria $criteria
     *
     * @return ArrayCollection
     */
    public function search(ExchangeCriteria $criteria = null)
    {
        $clients = array_map(function ($client) {
            return new ClientMeta($client);
        }, $this->clients);

        /** @var ArrayCollection $clients */
        $clients = new ArrayCollection($clients);

        if (!$criteria) {
            $criteria = new ExchangeCriteria();
        }

        //filter by criteria
        $clients = $clients->filter(function (ClientMeta $clientMeta) use ($criteria) {
            $interfaces = $criteria->getFeatures();
            $currencies = $criteria->getCurrencies();
            $excludeClients = $criteria->getExcludeClients();

            if ($interfaces && !$clientMeta->hasFeatures($interfaces)) {
                return false;
            }

            if ($currencies && !$clientMeta->hasCurrencies($currencies)) {
                return false;
            }

            if ($excludeClients && in_array($clientMeta->getClassName(), $excludeClients)) {
                return false;
            }

            return true;
        });

        return $clients;
    }

    /**
     * Get exchange by name
     * @param string $name
     */
    public function getExchangeByName(string $name)
    {
        $exchange = array_filter($this->clients, function (ClientInterface $client) use ($name) {
            return $client->getName() === $name;
        });

        if (count($exchange) > 0) {
            return reset($exchange);
        }

        return null;
    }
}