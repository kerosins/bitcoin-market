<?php
/**
 * @author kerosin
 */

namespace Payment\Provider;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Kerosin\Component\ArrayHelper;
use Payment\Component\Currency;
use Payment\Exchange\RateProviderInterface;
use Payment\Entity\CurrencyRate;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CurrencyProvider
{
    /**
     * Change to crypto_currency_scale, fiat_currency_scale parameters
     *
     * @deprecated
     */
    public const
        EXTERNAL_CURRENCY_PRECISION = 5,
        INTERNAL_CURRENCY_PRECISION = 2;

    /**
     * @var TagAwareAdapter
     */
    private $cache;

    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var Session
     */
    private $session;

    private $sessionCurrencyKey = 'currency';

    /**
     * Current selected currency
     *
     * @var Currency
     */
    private $currentCurrency;

    /**
     * @var ExchangeClientProvider
     */
    private $exchangeProvider;

    public const
        MAPPING_NAME = 'name',
        MAPPING_CODE = 'code',
        MAPPING_ICON = 'icon',
        MAPPING_VISIBLE = 'visible',
        MAPPING_ACTIVE = 'active';

    /**
     * Scale crypt-currency
     *
     * @var int
     */
    private $cryptoScale;

    /**
     * Scale fiat currency
     *
     * @var int
     */
    private $fiatScale;

    /**
     * @var Collection
     */
    private $currencies;

    /**
     * CurrencyProvider constructor.
     *
     * @param EntityManager $entityManager
     * @param TagAwareAdapter $adapter
     * @param Session $session
     * @param ExchangeClientProvider $exchangeClientProvider
     *
     * fixme
     */
    public function __construct(
        ObjectManager $entityManager,
        TagAwareAdapterInterface $adapter,
        SessionInterface $session,
        ExchangeClientProvider $exchangeClientProvider,
        int $cryptoScale,
        int $fiatScale
    ) {
        $this->cache = $adapter;
        $this->em = $entityManager;
        $this->session = $session;
        $this->exchangeProvider = $exchangeClientProvider;

        $this->cryptoScale = $cryptoScale;
        $this->fiatScale = $fiatScale;

        $this->currencies = new ArrayCollection();

        $this->currencies->set(
            'USD',
            (new Currency())
                ->setIsActive(false)
                ->setVisible(false)
                ->setRate(1)
                ->setCode('USD')
                ->setName('USD')
                ->setIsFiat(true)
        );
    }


    /**
     * Get best currency rate
     *
     * @param string $currency
     * @return float
     */
    public function getBestForSale(string $currency)
    {
        $rate = $this->cache->getItem("rate_{$currency}");
        if (!$rate->get()) {
            $entity = $this->em->getRepository(CurrencyRate::class)->findBestRateForCurrency($currency);

            if (!$entity) {
                $currencyRate = 1;
            } else {
                $currencyRate = $entity->getRate();
            }

            $rate->set($currencyRate);
            $rate->tag('best_rate_' . $currency);

            $this->cache->save($rate);
        }

        return $rate->get();
    }

    /**
     * Update are rates of currencies
     *
     * @param $currencies
     */
    public function updateRates(array $currencies)
    {
        $criteria = new ExchangeCriteria();
        $criteria->setCurrencies($currencies);
        $criteria->setFeatures([RateProviderInterface::class]);

        $clientMetas = $this->exchangeProvider->search($criteria);
        $entities = $this->em->getRepository(CurrencyRate::class)->findByCurrencies($currencies);

        $this->em->beginTransaction();

        /** @var CurrencyRate[]|null $bestRate */
        $bestRate = [];
        $result = [];

        try {
            foreach ($clientMetas as $meta) {
                /** @var \Payment\Exchange\BaseExchangeClient|\Payment\Exchange\RateProviderInterface $client */
                $client = $meta->getClient();
                $rates = $client->getRate($currencies);

                foreach ($rates as $currency => $rate) {
                    /** @var CurrencyRate $entity */
                    $entity = ArrayHelper::arraySearch($entities, function (CurrencyRate $entity) use ($client, $currency) {
                        return $entity->getCurrency() === $currency && $client->getName() === $entity->getExchange();
                    });

                    if (!$entity) {
                        $entity = new CurrencyRate();
                        $entity->setExchange($client->getName());
                        $entity->setCurrency($currency);
                        $entity->setIsActive(true);
                        $entity->setDifference(0);
                    } else {
                        $oldRate = $entity->getRate();
                        $difference = bcmul($rate, 100, 10);
                        $difference = bcdiv($difference, $oldRate, 10);

                        $difference = abs(bcsub(100, $difference, 3));
                        $entity->setDifference($difference);
                    }

                    $entity->setRate($rate);

                    if (!isset($bestRate[$currency]) || $bestRate[$currency]->getRate() > $entity->getRate()) {
                        if (isset($bestRate[$currency])) {
                            $bestRate[$currency]->setIsBest(false);
                        }

                        $entity->setIsBest(true);
                        $bestRate[$currency] = $entity;
                    } else {
                        $entity->setIsBest(false);
                    }

                    $result[] = $entity;
                    $this->em->persist($entity);
                }
            }

            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }

        foreach ($currencies as $currency) {
            self::invalidate($currency);
        }

        return $result;
    }

    /**
     * Convert amount from USD to selected currency
     *
     * @deprecated use CurrencyConverter instead of this method
     *
     * @param float $price
     * @param string $currency
     * @return float
     */
    public function convertToBestRate($price, $currency)
    {
        $rate = $this->getBestForSale($currency);
        return bcmul($price, $rate, $this->cryptoScale);
    }

    /**
     * Convert price to USD
     *
     * @deprecated use CurrencyConverter instead of this method
     *
     * @param $price
     * @param $currency
     *
     * @return string
     */
    public function convertToUsd($price, $currency)
    {
        $rate = $this->getBestForSale($currency);
        return bcdiv($price, $rate, $this->fiatScale);
    }

    /**
     * Invalidate cache for rate
     *
     * @param $currency
     */
    public function invalidate($currency)
    {
        $this->cache->invalidateTags(['best_rate_' . $currency]);
    }

    /**
     * get a current selected currency
     *
     * @return Currency
     */
    public function getCurrentCurrency()
    {
        if (!$this->currentCurrency) {
            $currency = $this->session->get($this->sessionCurrencyKey);
            $currentCurrency = $currency ? $this->currencies->get($currency) : $this->getDefaultCurrency();

            if (!$currentCurrency) {
                throw new \RuntimeException("Unknown currency {$currency}");
            }

            $this->currentCurrency = $currentCurrency;
        }

        return $this->currentCurrency;
    }

    /**
     * set a current currency
     *
     * @param $currency
     * @return $this
     * @throws \Exception
     */
    public function setCurrentCurrency($currencyCode)
    {
        $isset = false;
        $this->currencies->forAll(function ($key, Currency $currency) use ($currencyCode, &$isset) {
            $currency->setIsActive(false);
            if ($key === $currencyCode
                && $currency->getVisible()
                && !$currency->isFiat()
            ) {
                $isset = true;
                $currency->setIsActive(true);
            }

            return true;
        });

        if (!$isset) {
            throw new \RuntimeException("Currency with code {$currencyCode} is not present or not allowed for select in system");
        }

        $this->session->set($this->sessionCurrencyKey, $currencyCode);
        return $this;
    }

    /**
     * get list of currencies present on system
     *
     * @return Collection|Currency[]
     */
    public function getCurrenciesList()
    {
        return $this->currencies;
    }

    /**
     * gets list of allowed currencies
     *
     * @return Collection|Currency[]
     */
    public function getAllowedCurrenciesList()
    {
        return $this->currencies->filter(function (Currency $currency) {
            return $currency->getVisible() && !$currency->isFiat();
        });
    }

    /**
     * convert via USD rate
     *
     * @deprecated
     *
     * @param $amount
     * @param $fromCurrency
     * @param $toCurrency
     *
     * @return float
     */
    public function convert($amount, $fromCurrency, $toCurrency)
    {
        $amount = $this->convertToUsd($amount, $fromCurrency);
        return $this->convertToBestRate($amount, $toCurrency);
    }

    /**
     * Convert by specified rate
     *
     * @deprecated
     *
     * @param float $amount
     * @param float $rate
     */
    public function convertByRate(float $amount, float $rate)
    {
        return bcmul($amount, $rate, $this->cryptoScale);
    }

    /**
     * @return null|Currency
     */
    public function getDefaultCurrency()
    {
        return $this->currencies->get(CurrencyRate::BTC);
    }

    /**
     * adds currency to collection
     *
     * @param string $name
     * @param string $code
     * @param string|null $icon
     * @param bool $visible
     */
    public function addCurrency(
        string $name,
        string $code,
        string $icon = null,
        bool $visible = true
    ) {
        $this->currencies->set(
            $code,
            (new Currency())
                ->setName($name)
                ->setCode($code)
                ->setIcon($icon)
                ->setVisible($visible)
                ->setRate($this->getBestForSale($code))
        );
    }
}