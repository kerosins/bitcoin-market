<?php
/**
 * @author kerosin
 */

namespace Payment;

use Payment\Exchange\BaseExchangeClient;
use Payment\DependencyInjection\ClientCompiler;
use Payment\DependencyInjection\PaymentExtension;
use Payment\Exchange\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class PaymentBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new ClientCompiler());
    }
}