<?php
/**
 * @author kerosin
 */

namespace Payment\DependencyInjection;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ClientCompiler implements CompilerPassInterface
{
    public const TAG = 'payment.exchange_client',
        TAG_ATTR_NAME = 'exchange_name',
        TAG_ATTR_ACTIVE = 'active';

    public function process(ContainerBuilder $container)
    {
        if (!$container->has(\Payment\Provider\ExchangeClientProvider::class)) {
            return;
        }

        $definition = $container->getDefinition(\Payment\Provider\ExchangeClientProvider::class);
        $taggedServices = $container->findTaggedServiceIds(self::TAG);

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attr) {
                if (!isset($attr[self::TAG_ATTR_NAME], $attr[self::TAG_ATTR_ACTIVE])) {
                    $message = sprintf(
                        'Attributes %s are required for tag %s in service %s',
                        implode(', ', [self::TAG_ATTR_ACTIVE, self::TAG_ATTR_NAME]),
                        self::TAG,
                        $id
                    );
                    throw new \RuntimeException($message);
                }

                if ($attr[self::TAG_ATTR_ACTIVE] === false) {
                    continue;
                }

                $clientDef = $container->getDefinition($id);
                $clientDef->addMethodCall('setName', [$attr[self::TAG_ATTR_NAME]]);
                $definition->addMethodCall('addExchangeClient', [new Reference($id)]);
            }
        }
    }
}