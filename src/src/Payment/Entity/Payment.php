<?php
/**
 * @author kerosin
 */

namespace Payment\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;
use Catalog\ORM\Entity\Order;

/**
 * Class Payment
 * @package Payment\Entity
 *
 * @ORM\Entity(repositoryClass="Payment\Repository\PaymentRepository")
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class Payment
{
    use BaseMapping;
    use UpdateTimestamps;

    /**
     * Constants of payments statuses
     */
    public const
        PAYMENT_NONE = 0,
        PAYMENT_PROCESS = 10,
        PAYMENT_SUCCEEDED = 20,
        PAYMENT_DECLINED = 30,
        PAYMENT_CANCELED = 40;

    public static $statusLabels = [
        self::PAYMENT_NONE => 'None',
        self::PAYMENT_PROCESS => 'Process',
        self::PAYMENT_SUCCEEDED => 'Success',
        self::PAYMENT_DECLINED => 'Declined',
        self::PAYMENT_CANCELED => 'Cancelled'
    ];

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $exchange;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $externalTransactionId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $account;

    /**
     * @ORM\Column(type="string")
     */
    private $currency;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", scale=5, nullable=true)
     */
    private $rate;

    /**
     * @ORM\Column(type="decimal", scale=7, precision=20)
     */
    private $amount;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $errMessage;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $status = 0;



    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $qr;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set externalTransactionId
     *
     * @param string $externalTransactionId
     *
     * @return Payment
     */
    public function setExternalTransactionId($externalTransactionId)
    {
        $this->externalTransactionId = $externalTransactionId;

        return $this;
    }

    /**
     * Get externalTransactionId
     *
     * @return string
     */
    public function getExternalTransactionId()
    {
        return $this->externalTransactionId;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Payment
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return Payment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set errMessage
     *
     * @param string $errMessage
     *
     * @return Payment
     */
    public function setErrMessage($errMessage)
    {
        $this->errMessage = $errMessage;

        return $this;
    }

    /**
     * Get errMessage
     *
     * @return string
     */
    public function getErrMessage()
    {
        return $this->errMessage;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Payment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Payment
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set exchange
     *
     * @param string $exchange
     *
     * @return Payment
     */
    public function setExchange(string $exchange = null)
    {
        $this->exchange = $exchange;

        return $this;
    }

    /**
     * Get exchange
     *
     * @return string
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * Set rate
     *
     * @param string $rate
     *
     * @return Payment
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return string
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Payment
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Payment
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getAccount(): ?string
    {
        return $this->account;
    }

    /**
     * @param string $account
     * @return Payment
     */
    public function setAccount(string $account): Payment
    {
        $this->account = $account;
        return $this;
    }

    /**
     * @return string
     */
    public function getQr(): ?string
    {
        return $this->qr;
    }

    /**
     * @param string $qr
     * @return Payment
     */
    public function setQr(string $qr): Payment
    {
        $this->qr = $qr;
        return $this;
    }

    /**
     * Gets a readable label of status
     *
     * @return mixed
     */
    public function getReadableStatus()
    {
        return self::$statusLabels[$this->status];
    }
}