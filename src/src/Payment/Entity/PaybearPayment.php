<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Payment\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;

/**
 * Class PaybearPayment
 * @package Payment\Entity
 *
 * @ORM\Entity()
 * @ORM\Table()
 */
class PaybearPayment
{
    use BaseMapping, UpdateTimestamps;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $invoice;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=20, scale=9)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $currency;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $block = [];

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $transaction = [];

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $confirmationNumber = 0;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isDone = false;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return PaybearPayment
     */
    public function setAddress(string $address): PaybearPayment
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getInvoice(): ?string
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     * @return PaybearPayment
     */
    public function setInvoice(string $invoice): PaybearPayment
    {
        $this->invoice = $invoice;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return PaybearPayment
     */
    public function setAmount(float $amount): PaybearPayment
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return PaybearPayment
     */
    public function setCurrency(string $currency): PaybearPayment
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return array
     */
    public function getBlock(): ?array
    {
        return $this->block;
    }

    /**
     * @param array $block
     * @return PaybearPayment
     */
    public function setBlock(array $block): PaybearPayment
    {
        $this->block = $block;
        return $this;
    }

    /**
     * @return array
     */
    public function getTransaction(): ?array
    {
        return $this->transaction;
    }

    /**
     * @param array $transaction
     * @return PaybearPayment
     */
    public function setTransaction(array $transaction): PaybearPayment
    {
        $this->transaction = $transaction;
        return $this;
    }

    /**
     * @return int
     */
    public function getConfirmationNumber(): ?int
    {
        return $this->confirmationNumber;
    }

    /**
     * @param int $confirmationNumber
     * @return PaybearPayment
     */
    public function setConfirmationNumber(int $confirmationNumber): PaybearPayment
    {
        $this->confirmationNumber = $confirmationNumber;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsDone(): ?bool
    {
        return $this->isDone;
    }

    /**
     * @param bool $isDone
     * @return PaybearPayment
     */
    public function setIsDone(bool $isDone = false): PaybearPayment
    {
        $this->isDone = $isDone;
        return $this;
    }
}