<?php
/**
 * @author kerosin
 */

namespace Payment\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;

/**
 * Class Currency
 * @package Currency\Entity
 *
 * @ORM\Entity(repositoryClass="Payment\Repository\CurrencyRepository")
 * @ORM\Table(name="currency_rate")
 * @ORM\HasLifecycleCallbacks()
 */
class CurrencyRate
{
    use BaseMapping;
    use UpdateTimestamps;

    public const RATE_SCALE = 7;

    public const BTC = 'BTC';
    public const BCH = 'BCH';
    public const ETH = 'ETH';
    public const LTC = 'LTC';
    public const DASH = 'DASH';
    public const BTG = 'BTG';
    public const ZBC = 'ZBC';

    /**
     * @ORM\Column(type="decimal", scale=7)
     */
    private $rate;

    /**
     * @ORM\Column(type="string")
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $exchange;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isBest = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive = true;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", scale=3)
     */
    private $difference = 0.000;

    /**
     * Set rate
     *
     * @param string $rate
     *
     * @return CurrencyRate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return string
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return CurrencyRate
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CurrencyRate
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return CurrencyRate
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set exchange
     *
     * @param string $exchange
     *
     * @return CurrencyRate
     */
    public function setExchange(string $exchange = null)
    {
        $this->exchange = $exchange;

        return $this;
    }

    /**
     * Get exchange
     *
     * @return string
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * Set isMax
     *
     * @param boolean $isBest
     *
     * @return CurrencyRate
     */
    public function setIsBest($isBest)
    {
        $this->isBest = $isBest;

        return $this;
    }

    /**
     * Get isMax
     *
     * @return boolean
     */
    public function getIsBest()
    {
        return $this->isBest;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return CurrencyRate
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set difference
     *
     * @param string $difference
     *
     * @return CurrencyRate
     */
    public function setDifference($difference)
    {
        $this->difference = $difference;

        return $this;
    }

    /**
     * Get difference
     *
     * @return string
     */
    public function getDifference()
    {
        return $this->difference;
    }
}
