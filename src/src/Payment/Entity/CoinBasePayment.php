<?php
/**
 * @author Kondaurov
 */

namespace Payment\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;


/**
 * Class CoinBasePayment
 * @package Payment\Entity
 *
 * @ORM\Entity(repositoryClass="Payment\Repository\CoinBasePaymentRepository")
 * @ORM\Table(name="coin_base_payment")
 */
class CoinBasePayment
{
    use BaseMapping, UpdateTimestamps;

    public const
        STATUS_NEW = 'NEW',
        STATUS_PENDING = 'PENDING',
        STATUS_COMPLETED = 'COMPLETED',
        STATUS_EXPIRED = 'EXPIRED',
        STATUS_UNRESOLVED = 'UNRESOLVED',
        STATUS_RESOLVED = 'RESOLVED',
        STATUS_CANCELED = 'CANCELED';

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $uid;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=20, scale=9)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isDone = false;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $status = self::STATUS_NEW;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getUid(): ?string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     *
     * @return CoinBasePayment
     */
    public function setUid(string $uid): CoinBasePayment
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return CoinBasePayment
     */
    public function setCode(string $code): CoinBasePayment
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     *
     * @return CoinBasePayment
     */
    public function setAmount(float $amount): CoinBasePayment
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     *
     * @return CoinBasePayment
     */
    public function setCurrency(string $currency): CoinBasePayment
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return CoinBasePayment
     */
    public function setAddress(string $address): CoinBasePayment
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsDone(): ?bool
    {
        return $this->isDone;
    }

    /**
     * @param bool $isDone
     *
     * @return CoinBasePayment
     */
    public function setIsDone(bool $isDone): CoinBasePayment
    {
        $this->isDone = $isDone;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return CoinBasePayment
     */
    public function setStatus(string $status): CoinBasePayment
    {
        $this->status = $status;
        return $this;
    }
}