<?php
/**
 * @author kerosin
 */

namespace Payment\Repository;

use Kerosin\Doctrine\ORM\EntityRepository;

/**
 * Class ExchangeRepository
 * @package Currency\Repository
 *
 * @method findByIsActive(bool $isActive)
 */
class ExchangeRepository extends EntityRepository
{

    public function isActiveByClientClass($clientClass)
    {
        $result = $this->createQueryBuilder('e')
            ->select('count(e)')
            ->andWhere('e.clientClass = :clientClass AND e.isActive = true')
            ->setParameter('clientClass', $clientClass)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return (bool)$result;
    }

    public function findByClients(array $clientClasses)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.isActive = true')
            ->andWhere('e.clientClass IN (:clientClasses)')
            ->setParameter('clientClasses', $clientClasses)
            ->getQuery()
            ->getResult();
    }
}