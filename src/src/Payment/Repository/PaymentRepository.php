<?php
/**
 * @author Kerosin
 */

namespace Payment\Repository;

use Kerosin\Doctrine\ORM\EntityRepository;
use Payment\Entity\Payment;

/**
 * Class PaymentRepository
 * @package Payment\Repository
 *
 * @method Payment[] findByStatus(int $status)
 */
class PaymentRepository extends EntityRepository
{
    /**
     * Finds payments with status none and they were created plus offset (in minutes) earlier than current time
     *
     * @param $offset
     *
     * @return Payment[]
     */
    public function findLaterThanCurrentTime($offset)
    {
        $offset *= 60;

        return $this->createQueryBuilder('p')
            ->andWhere("DATE_ADD(p.createdAt, :offset, 'second') < CURRENT_TIMESTAMP()")
            ->andWhere('p.status IN (:wait)')
            ->setParameters([
                'offset' => $offset,
                'wait' => [Payment::PAYMENT_NONE, Payment::PAYMENT_PROCESS]
            ])
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Finds payment by exchange's name and transaction's ID
     *
     * @param string $transaction
     * @param string $client
     *
     * @return Payment|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByTransactionIdAndExchange(string $transaction, string $client)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exchange = :exchange')
            ->andWhere('p.externalTransactionId = :transaction')
            ->setParameters([
                'exchange' => $client,
                'transaction' => $transaction
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }
}