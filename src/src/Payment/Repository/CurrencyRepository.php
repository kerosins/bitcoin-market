<?php
/**
 * @author kerosin
 */

namespace Payment\Repository;


use Payment\Entity\CurrencyRate;
use Kerosin\Doctrine\ORM\EntityRepository;

/**
 * Class CurrencyRepository
 * @package Currency\Repository
 *
 * @method findOneByCurrency(string $currency) : Payment\Entity\Currency|null
 */
class CurrencyRepository extends EntityRepository
{
    /**
     * @param $currency
     * @param $exchange
     *
     * @return CurrencyRate|null
     */
    public function findByCurrencyAndExchange($currency, $exchange)
    {
        $builder = $this->createQueryBuilder('c');
        $builder->andWhere('c.currency = :currency AND c.exchange = :exchange')
            ->setParameters([
                'currency' => $currency,
                'exchange' => $exchange
            ]);

        return $builder->getQuery()->getOneOrNullResult();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function preBuildIsActive()
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.isActive = true');
    }

    /**
     * Find active rates by currency code
     *
     * @param string $currency
     * @return array
     */
    public function findActiveByCurrency(string $currency)
    {
        return $this->preBuildIsActive()
            ->andWhere('c.currency = :currency')
            ->setParameter('currency', $currency)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find rates by currencies
     *
     * @param array $currencies
     *
     * @return CurrencyRate[]
     */
    public function findByCurrencies(array $currencies)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.currency IN (:currencies)')
            ->setParameter('currencies', $currencies)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Find best rate
     *
     * @param $currency
     *
     * @return CurrencyRate|null
     */
    public function findBestRateForCurrency($currency)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.currency = :currency')
            ->andWhere('c.isBest = :isBest')
            ->andWhere('c.isActive = :isActive')
            ->setParameters([
                'currency'  => $currency,
                'isActive'  => true,
                'isBest'    => true
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }
}