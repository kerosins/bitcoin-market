<?php
/**
 * @author kerosin
 */

namespace Payment\Exception;


use Throwable;

class UnknownExchangeClientException extends \RuntimeException
{
    /**
     * UnknownExchangeClientException constructor.
     * @param string $className - classname of client
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($className, $code = 0, Throwable $previous = null)
    {
        $message = "Class {$className} not exists or not ExchangeClient";
        parent::__construct($message, $code, $previous);
    }
}