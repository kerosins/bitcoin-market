<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Payment\Component\Exchange\Client;


use Payment\Exchange\BaseExchangeClient;
use Payment\Exchange\RateProviderInterface;
use Payment\Entity\CurrencyRate;

class BitfinexClient extends BaseExchangeClient implements RateProviderInterface
{
    protected $url = 'https://api.bitfinex.com/';

    public function getCurrencies(): array
    {
        return [
            CurrencyRate::BTC,
            CurrencyRate::BCH,
            CurrencyRate::ETH,
            CurrencyRate::LTC,
            CurrencyRate::BTG,
            CurrencyRate::DASH,
        ];
    }

    /**
     * Replace currency code to exchange currency code and revert
     *
     * @param $currencies
     * @return array
     */
    private function replaceCurrenciesCode($currencies, $fromExchange = true)
    {
        $localCurrencyCodes = [
            CurrencyRate::BCH => 'BCH'
        ];

        if ($fromExchange) {
            $localCurrencyCodes = array_flip($localCurrencyCodes);
        }

        return array_map(function ($currency) use ($localCurrencyCodes) {
            return $localCurrencyCodes[$currency] ?? $currency;
        }, $currencies);
    }

    public function getRate(array $currencies = []): array
    {
        $currencies = array_intersect($currencies, self::getCurrencies());
        $currencies = $this->replaceCurrenciesCode($currencies, false);

        $symbols = array_map(function ($currency) {
            return "t{$currency}USD";
        }, $currencies);

        $client = $this->getHttpClient();

        $response = $client->get('/v2/tickers', [
            'query' => [
                'symbols' => implode(',', $symbols)
            ]
        ]);

        if ($response->getStatusCode() !== 200) {
            return [];
        }

        $response = (string)$response->getBody();
        $response = json_decode($response, true);

        $codes = array_map(function ($item) {
            $code = str_replace(['t', 'USD'], '', $item[0]);
            return $code;
        }, $response);
        $codes = $this->replaceCurrenciesCode($codes);

        $rates = array_map(function ($item) {
            return bcdiv(1, $item[3], CurrencyRate::RATE_SCALE);
        }, $response);

        return array_combine($codes, $rates);
    }

}