<?php
/**
 * @author Kondaurov
 */

namespace Payment\Component\Exchange\Client;

use Billing\DependencyInjection\BillingServicesInjectionContract;
use Billing\DependencyInjection\BillingServicesInjectionContractTrait;
use GuzzleHttp\Client;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Payment\Entity\Payment;
use Payment\Exchange\PaymentProviderInterface;
use Payment\Exchange\RateProviderInterface;
use Payment\Entity\CurrencyRate;

class ZbcBilling implements
    RateProviderInterface,
    PaymentProviderInterface,
    BillingServicesInjectionContract,
    EntityManagerContract
{
    use BillingServicesInjectionContractTrait,
        EntityManagerContractTrait;

    private const ZBC_RATE_FACTOR = 1;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var int
     */
    private $scale;


    private $pairs = [
        'EUR' => 'eur-usd',//euro
        'JPY' => 'jpy-usd',//japanese currency
    ];

    public function __construct(string $apiKey, int $scale)
    {
        $this->apiKey = $apiKey;
        $this->scale = $scale;
    }

    public function getCurrencies(): array
    {
        return [ CurrencyRate::ZBC ];
    }

    /**
     * @inheritdoc
     *
     * @param array $currencies
     *
     * @return array
     */
    public function getRate(array $currencies = []): array
    {
        $httpClient = new Client();
        $rates = [1];

        foreach ($this->pairs as $pair) {
            $response = $httpClient->get("https://api.cryptonator.com/api/ticker/{$pair}");
            if ($response->getStatusCode() !== 200) {
                throw new \HttpException('API is unavailable');
            }

            $response = json_decode((string)$response->getBody(), true);
            if (json_last_error() !== JSON_ERROR_NONE
                || (!isset($response['ticker']) && !isset($response['ticker']['price']))
            ) {
                throw new \RuntimeException('Invalid json: ' . json_last_error_msg());
            }

            array_push($rates, $response['ticker']['price']);
        }

        $currencyFactor = bcdiv(1, count($rates), $this->scale);
        $zbcRate = array_reduce($rates, function ($carry, $rate) use ($currencyFactor) {
            $rate = (float)$rate;
            $rate = round($rate, $this->scale);
            return bcadd(
                $carry,
                bcmul($rate, $currencyFactor, $this->scale),
                $this->scale
            );
        });

        $zbcRate = bcdiv(1, $zbcRate, $this->scale);
        $zbcRate = bcmul($zbcRate, self::ZBC_RATE_FACTOR, $this->scale);

        return [
            CurrencyRate::ZBC => $zbcRate
        ];
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $transactionId
     *
     * @return bool|mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPaymentStatus(string $transactionId)
    {
        $isDone = $this->transactionManager->isVerifiedById($transactionId);
        if ($isDone) {
            return Payment::PAYMENT_SUCCEEDED;
        }

        return Payment::PAYMENT_PROCESS;
    }

    public function processPayment(Payment $payment): Payment
    {
        //todo should be implement later, mb
        return new Payment();
    }


}