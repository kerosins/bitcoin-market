<?php
/**
 * @author Kerosin
 */

namespace Payment\Component\Exchange\Client;


use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Payment\Component\PaymentEvent;
use Payment\Entity\Payment;
use Payment\Exchange\BaseExchangeClient;
use Payment\Exchange\PaymentProviderInterface;
use Payment\Entity\CurrencyRate;
use Payment\Provider\CurrencyProvider;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SimpleClient extends BaseExchangeClient implements PaymentProviderInterface, EntityManagerContract
{
    use EntityManagerContractTrait;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getCurrencies(): array
    {
        return [ CurrencyRate::BTC, CurrencyRate::ETH, CurrencyRate::BCH ];
    }

    /**
     * @inheritdoc
     */
    public function processPayment(Payment $payment): Payment
    {
        $payment->setAccount(md5(time()));
        $payment->setQr('/images/qr-code.gif');
        $payment->setExternalTransactionId(hash('sha256', time()));

        return $payment;
    }

    /**
     * @inheritdoc
     */
    public function getPaymentStatus(string $transactionId)
    {
        return Payment::PAYMENT_SUCCEEDED;
    }
}