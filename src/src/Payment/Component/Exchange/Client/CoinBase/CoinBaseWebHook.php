<?php
/**
 * @author Kondaurov
 */

namespace Payment\Component\Exchange\Client\CoinBase;


use Payment\Component\PaymentEvent;
use Payment\Entity\CoinBasePayment;
use Payment\Entity\Payment;
use Payment\Exception\PaymentException;
use Payment\Repository\CoinBasePaymentRepository;
use Payment\Repository\PaymentRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

class CoinBaseWebHook
{
    public const
        EVENT_CONFIRMED = 'charge:confirmed',
        EVENT_EXPIRED = 'charge:delayed'
    ;

    /**
     * @var CoinBasePaymentRepository
     */
    private $coinBasePaymentRepository;

    /**
     * @var string
     */
    private $webHookSecret;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var PaymentRepository
     */
    private $paymentRepository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var CoinBaseClient
     */
    private $coinBaseClient;

    public function __construct(
        CoinBasePaymentRepository $coinBasePaymentRepository,
        PaymentRepository $paymentRepository,
        EventDispatcherInterface $eventDispatcher,
        CoinBaseClient $coinBaseClient,
        LoggerInterface $logger,
        string $webHookSecret
    ) {
        $this->coinBasePaymentRepository = $coinBasePaymentRepository;
        $this->paymentRepository = $paymentRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
        $this->webHookSecret = $webHookSecret;
        $this->coinBaseClient = $coinBaseClient;
    }

    /**
     * @param Request $request
     */
    public function process(Request $request)
    {
        $rawData = $request->getContent();

        if (!$this->validateRequest($request)) {
            throw new PaymentException('Invalid CoinBase WebHook request');
        }

        $jsonData = json_decode($rawData, true);
        $event = $jsonData['event'];

        switch ($event['type']) {
            case self::EVENT_CONFIRMED:
                $this->onPaymentConfirm($event);
                break;
            case self::EVENT_EXPIRED:
                $this->onPaymentExpire($event);
                break;
        }


    }

    /**
     * @param Request $request
     */
    public function log(Request $request)
    {
        $rawData = $request->getContent();
        $headers = $request->headers->all();

        $this->logger->info('Coinbase Webhook Request', [
            'headers' => $headers,
            'raw' => $rawData,
            'decoded' => json_decode($rawData, true)
        ]);
    }

    /**
     * Handle "charge:confirmed" event
     *
     * @param array $event
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function onPaymentConfirm(array $event)
    {
        $uid = $event['data']['id'];
        $payment = $this->getPayment($uid);

        $payment
            ->setStatus(CoinBasePayment::STATUS_COMPLETED)
            ->setIsDone(true);
        $this->coinBasePaymentRepository->save($payment);
    }

    /**
     * Handle "charge:delayed" event
     *
     * @param array $event
     */
    private function onPaymentExpire(array $eventData)
    {
        $uid = $eventData['data']['id'];
        $payment = $this->getPayment($uid);

        $payment->setStatus(CoinBasePayment::STATUS_EXPIRED);
        $this->coinBasePaymentRepository->save($payment);

        //find origin payment and send
        /** @var Payment $originalPayment */
        $originalPayment = $this->paymentRepository->findOneBy([
            'externalTransactionId' => $uid,
            'exchange' => $this->coinBaseClient->getName()
        ]);

        if ($originalPayment) {
            $event = new PaymentEvent($originalPayment);
            $this->eventDispatcher->dispatch(PaymentEvent::EVENT_NAME_EXPIRE, $event);
        }
    }

    /**
     * Gets payment by uid
     *
     * @param string $uid
     * @return CoinBasePayment
     */
    private function getPayment(string $uid)
    {
        /** @var CoinBasePayment $payment */
        $payment = $this->coinBasePaymentRepository->findOneBy([
            'uid' => $uid
        ]);

        if (!$payment) {
            throw new PaymentException('Unknown CoinBase payment with uid: ' . $uid);
        }

        return $payment;
    }

    /**
     * Validates request
     *
     * @param Request $request
     * @return bool
     */
    private function validateRequest(Request $request): bool
    {
        $rawData = $request->getContent();
        $signature = $request->headers->get('X-CC-Webhook-Signature');

        $hash = hash_hmac('sha256', $rawData, $this->webHookSecret);

        return strcasecmp($signature, $hash) === 0;
    }
}