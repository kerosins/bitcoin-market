<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Payment\Component\Exchange\Client\CoinBase;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Payment\Entity\CoinBasePayment;
use Payment\Entity\CurrencyRate;
use Payment\Entity\Payment;
use Payment\Exchange\PaymentProviderInterface;
use Payment\Repository\CoinBasePaymentRepository;
use Payment\Service\QrGenerator;
use Psr\Log\LoggerInterface;

class CoinBaseClient implements PaymentProviderInterface
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $apiVersion = '2018-03-22';

    /**
     * @var string
     */
    private $apiUrl = 'https://api.commerce.coinbase.com';

    /**
     * @var int
     */
    private $cryptoCurrencyScale;

    /**
     * @var CoinBasePaymentRepository
     */
    private $coinBasePaymentRepository;

    /**
     * @var array
     */
    private $currencyMap = [
        CurrencyRate::BTC => 'bitcoin',
        CurrencyRate::BCH => 'bitcoincash',
        CurrencyRate::ETH => 'ethereum',
        CurrencyRate::LTC => 'litecoin'
    ];

    /**
     * @var QrGenerator
     */
    private $qrGenerator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        CoinBasePaymentRepository $coinBasePaymentRepository,
        QrGenerator $qrGenerator,
        LoggerInterface $logger,
        string $apiKey,
        int $cryptoCurrencyScale
    ) {
        $this->coinBasePaymentRepository = $coinBasePaymentRepository;
        $this->qrGenerator = $qrGenerator;
        $this->apiKey = $apiKey;
        $this->cryptoCurrencyScale = $cryptoCurrencyScale;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function getCurrencies(): array
    {
        return [
            CurrencyRate::BTC,
            CurrencyRate::BCH,
            CurrencyRate::ETH,
            CurrencyRate::LTC
        ];
    }

    /**
     * @inheritdoc
     *
     * @param string $name
     *
     * @return mixed|void
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritdoc
     *
     * @param Payment $payment
     *
     * @return Payment
     */
    public function processPayment(Payment $payment): Payment
    {
        $coinBasePayment = new CoinBasePayment();
        $amount = $payment->getAmount() * $payment->getRate();
        $amount = round($amount, $this->cryptoCurrencyScale);

        $coinBasePayment
            ->setAmount($amount)
            ->setCurrency($payment->getCurrency());


        $httpClient = new Client(['base_uri' => $this->apiUrl]);

        $response = $httpClient->post(
            '/charges',
            [
                RequestOptions::HTTP_ERRORS => false,
                RequestOptions::HEADERS => [
                    'X-CC-Api-Key' => $this->apiKey,
                    'X-CC-Version' => $this->apiVersion
                ],
                RequestOptions::JSON => [
                    'name' => 'Order Payment',
                    'description' => 'Order Payment',
                    'pricing_type' => 'fixed_price',
                    'local_price' => [
                        'amount' => $amount,
                        'currency' => $payment->getCurrency()
                    ]
                ]
            ]
        );

        $body = (string)$response->getBody();
        $dataJson = json_decode($body, true);

        if ($response->getStatusCode() !== 201) {
            $this->logger->error('CoinBase API Request error', $dataJson);
            throw new \RuntimeException();
        }

        $dataJson = $dataJson['data'];

        $address = $dataJson['addresses'][$this->currencyMap[$payment->getCurrency()]];

        $coinBasePayment
            ->setUid($dataJson['id'])
            ->setCode($dataJson['code'])
            ->setAddress($address)
            ->setStatus('NEW')
        ;

        $qrCode = $this->qrGenerator->generate(
            $coinBasePayment->getCurrency(),
            $coinBasePayment->getAddress(),
            $coinBasePayment->getAmount()
        );

        $payment
            ->setAccount($coinBasePayment->getAddress())
            ->setExternalTransactionId($coinBasePayment->getUid())
            ->setQr($qrCode);

        $this->coinBasePaymentRepository->save($coinBasePayment);

        return $payment;
    }

    /**
     * @inheritdoc
     *
     * @param string $transactionId
     *
     * @return mixed
     */
    public function getPaymentStatus(string $transactionId)
    {
        /** @var CoinBasePayment $payment */
        $payment = $this->coinBasePaymentRepository->findOneBy(['uid' => $transactionId]);
        if (!$payment) {
            return Payment::PAYMENT_NONE;
        }

        return $payment->getIsDone() ? Payment::PAYMENT_SUCCEEDED : Payment::PAYMENT_PROCESS;
    }

}