<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Payment\Component\Exchange\Client;


use GuzzleHttp\Client;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Payment\Entity\CurrencyRate;
use Payment\Entity\PaybearPayment;
use Payment\Entity\Payment;
use Payment\Exchange\PaymentProviderInterface;
use Payment\Exchange\RateProviderInterface;
use Payment\Service\QrGenerator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

/**
 * Integration with savvy.io
 *
 * Class PaybearClient
 * @package Payment\Component\Exchange\Client
 */
class PaybearClient implements PaymentProviderInterface, RateProviderInterface, EntityManagerContract
{
    use EntityManagerContractTrait;

    /**
     * Client name
     *
     * @var string
     */
    private $name;

    /**
     * Secret key
     *
     * @var string
     */
    private $secretKey;

    /**
     * Public key
     *
     * @var string
     */
    private $publicKey;

    /**
     * Payment fees
     *
     * @var array
     */
    private $fees = [];

    /**
     * Api url template
     *
     * @var string
     */
//    private $urlTemplate = 'https://api.paybear.io/v2/%s/payment/%s?token=%s';
    private $urlTemplate = 'https://api.savvy.io/v3/%s/payment/%s?token=%s';

//    private $rateUrlTemplate = 'https://api.paybear.io/v2/%s/exchange/usd/rate';
    private $rateUrlTemplate = 'https://api.savvy.io/v3/exchange/usd/rate';

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * Project hostname
     *
     * @var string
     */
    private $projectHost;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var QrGenerator
     */
    private $qrGenerator;

    /**
     * @var int
     */
    private $cryptoScale;

    public function __construct(
        RouterInterface $router,
        QrGenerator $qrGenerator,
        LoggerInterface $logger,
        int $cryptoScale,
        string $projectHost,
        string $secretKey,
        string $publicKey,
        array $fees
    ) {
        $this->router = $router;
        $this->qrGenerator = $qrGenerator;
        $this->projectHost = $projectHost;
        $this->logger = $logger;

        $this->cryptoScale = $cryptoScale;

        $this->secretKey = $secretKey;
        $this->publicKey = $publicKey;

        $this->fees = $fees;
    }

    /**
     * @return array
     */
    public function getCurrencies(): array
    {
        return [
            CurrencyRate::BTC,
            CurrencyRate::BCH,
            CurrencyRate::ETH,
            CurrencyRate::LTC,
            CurrencyRate::BTG,
            CurrencyRate::DASH
        ];
    }

    /**
     * @param string $name
     * @return $this|mixed
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritdoc
     *
     * @param Payment $payment
     *
     * @return Payment
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function processPayment(Payment $payment): Payment
    {
        $paybearPayment = new PaybearPayment();
        $amount = bcmul($payment->getAmount(), $payment->getRate(), 5);

        $paybearPayment
            ->setAmount($amount)
            ->setCurrency($payment->getCurrency());

        try {
            $paybearData = $this->sendCreatePaymentRequest($paybearPayment->getCurrency());
            $paybearPayment->setAddress($paybearData['address']);
            $paybearPayment->setInvoice($paybearData['invoice']);

            $qrCode = $this->qrGenerator->generate(
                $paybearPayment->getCurrency(),
                $paybearPayment->getAddress(),
                $paybearPayment->getAmount()
            );
        } catch (\Exception $e) {
            $this->logger->error('Paybear error', [
                'error' => $e->getMessage()
            ]);
            throw new \RuntimeException();
        }

        $this->entityManager->persist($paybearPayment);
        $this->entityManager->flush($paybearPayment);

        $payment
            ->setAccount($paybearPayment->getAddress())
            ->setExternalTransactionId($paybearPayment->getId())
            ->setQr($qrCode);

        return $payment;
    }

    /**
     * Checks payment status
     *
     * @param string $transactionId
     *
     * @return int|mixed
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getPaymentStatus(string $transactionId)
    {
        /** @var PaybearPayment $payment */
        $payment = $this->entityManager->find(PaybearPayment::class, $transactionId);
        return $payment->getIsDone() ? Payment::PAYMENT_SUCCEEDED : Payment::PAYMENT_PROCESS;
    }

    /**
     * Sends request to savvy.io
     *
     * @param string $currency
     * @return mixed
     */
    private function sendCreatePaymentRequest(string $currency)
    {
        $callback = $this->router->generate('paybear', [], UrlGeneratorInterface::ABSOLUTE_URL);

        $uri = sprintf(
            $this->urlTemplate,
            strtolower($currency),
            urlencode($callback),
            $this->secretKey
        );

        $client = new Client();
        $response = $client->get($uri, ['http_errors' => false]);

        $body = (string)$response->getBody();
        $body = json_decode($body, true);

        if ($response->getStatusCode() !== 201 || $body['success'] !== true) {
            throw new \RuntimeException(json_encode($body));
        }

        return $body['data'];
    }

    /**
     * Gets a list of rates
     *
     * @param array $currencies
     *
     * @return array
     */
    public function getRate(array $currencies = []): array
    {
        $currencies = array_uintersect(
            $currencies,
            $this->getCurrencies(),
            function ($a, $b) {
                return strcasecmp($a, $b);
            }
        );

        $client = new Client();
        $result = [];
        $res = $client->get($this->rateUrlTemplate, ['http_errors' => false]);
        if ($res->getStatusCode() !== 200) {
            $this->logger->error('Savvy request return ' . $res->getStatusCode());
            return [];
        }

        $body = json_decode((string)$res->getBody(), true);

        if (!isset($body['data'], $body['success'])) {
            $this->logger->error('Savvy change response', [
                'response' => $body
            ]);
            return [];
        }

        foreach ($body['data'] as $currency => $exchanges) {
            $currency = strtoupper($currency);
            if (!in_array($currency, $currencies)) {
                continue;
            }

            $rate = max($exchanges);
            $result[$currency] = bcdiv(1, $rate, $this->cryptoScale);
        }

        return $result;
    }

}