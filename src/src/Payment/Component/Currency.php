<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Payment\Component;


class Currency
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var bool
     */
    private $visible;

    /**
     * @var float
     */
    private $rate;

    /**
     * @var bool
     */
    private $isActive;

    private $isFiat = false;

    public function __toString()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Currency
     */
    public function setName(string $name): Currency
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Currency
     */
    public function setCode(string $code): Currency
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return Currency
     */
    public function setIcon(string $icon = null): Currency
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return bool
     */
    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    /**
     * @param string $visible
     * @return Currency
     */
    public function setVisible(bool $visible): Currency
    {
        $this->visible = $visible;
        return $this;
    }

    /**
     * @return float
     */
    public function getRate(): ?float
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     * @return Currency
     */
    public function setRate(float $rate): Currency
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return Currency
     */
    public function setIsActive(bool $isActive): Currency
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFiat(): ?bool
    {
        return $this->isFiat;
    }

    /**
     * @param bool $isFiat
     * @return Currency
     */
    public function setIsFiat(bool $isFiat): Currency
    {
        $this->isFiat = $isFiat;
        return $this;
    }


}