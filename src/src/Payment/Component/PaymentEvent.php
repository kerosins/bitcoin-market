<?php
/**
 * @author Kondaurov
 */

namespace Payment\Component;

use Payment\Entity\Payment;
use Symfony\Component\EventDispatcher\Event;

class PaymentEvent extends Event
{
    const EVENT_NAME_SUCCESS = 'payment.success';

    const EVENT_NAME_EXPIRE = 'payment.expire';

    /**
     * @var Payment
     */
    private $payment;

    /**
     * PaymentEvent constructor.
     *
     * @param Payment $payment
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return Payment
     */
    public function getPayment(): Payment
    {
        return $this->payment;
    }

    /**
     * @param Payment $payment
     */
    public function setPayment(Payment $payment): void
    {
        $this->payment = $payment;
    }
}