<?php
/**
 * @author Kondaurov
 */

namespace Payment\Command;


use Doctrine\Common\Persistence\ObjectManager;
use Kerosin\Component\ContainerAwareCommand;
use Payment\Component\PaymentEvent;
use Payment\Entity\Payment;
use Payment\Exchange\PaymentProviderInterface;
use Payment\Provider\ExchangeClientProvider;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PaymentVerifyProcessorCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('payment:verify_processor')
            ->setDescription('Run a verify processor');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = $this->getEntityManager();
        $paymentRepository = $entityManager->getRepository(Payment::class);
        $exchangeProvider = $this->getContainer()->get(ExchangeClientProvider::class);
        $dispatcher = $this->getContainer()->get('event_dispatcher');

        while (true) {
            try {
                $payments = $paymentRepository->findByStatus(Payment::PAYMENT_PROCESS);

                foreach ($payments as $payment) {
                    /** @var PaymentProviderInterface $exchange */
                    $exchange = $exchangeProvider->getExchangeByName($payment->getExchange());
                    if (!$exchange) {
                        throw new \RuntimeException("Exchange {$payment->getExchange()} not exist in system");
                    }
                    $status = $exchange->getPaymentStatus($payment->getExternalTransactionId());

                    switch ($status) {
                        case Payment::PAYMENT_SUCCEEDED;
                            $this->savePaymentStatus($entityManager, $payment, $status);

                            $event = new PaymentEvent();
                            $event->setPayment($payment);
                            $dispatcher->dispatch(PaymentEvent::EVENT_NAME_SUCCESS, $event);
                            break;
                    }
                }

                $entityManager->clear();
                sleep(30);
            } catch (\Exception $e) {
                $output->writeln('have error');
                $output->writeln($e->getMessage());
                continue;
            }
        }
    }

    private function savePaymentStatus(ObjectManager $entityManager, Payment $payment, int $status)
    {
        $payment->setStatus($status);
        $entityManager->persist($payment);
        $entityManager->flush($payment);
    }
}