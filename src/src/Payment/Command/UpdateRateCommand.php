<?php
/**
 * @author kerosin
 */

namespace Payment\Command;

use Doctrine\Common\Collections\ArrayCollection;
use Payment\Entity\CurrencyRate;
use Payment\Exchange\ClientMeta;
use Payment\Provider\CurrencyProvider;
use Payment\Provider\ExchangeCriteria;
use Payment\Exchange\RateProviderInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateRateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('payment:rate:update')
            ->setDescription('Update rate of currencies')
            ->addArgument('currency', InputArgument::OPTIONAL, 'List of codes separate by comma')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @todo реализовать поиск максимального, минимального курса
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (($currencies = $input->getArgument('currency')) !== null) {
            $currencies = explode(',', $currencies);
        } else {
            $currencies = [
                CurrencyRate::BCH,
                CurrencyRate::BTC,
                CurrencyRate::ETH,
                CurrencyRate::ZBC,
                CurrencyRate::BTG,
                CurrencyRate::DASH,
                CurrencyRate::LTC
            ];
        }

        /** @var CurrencyRate[] $updated */
        $updated = $this->getContainer()->get(CurrencyProvider::class)->updateRates($currencies);

        foreach ($updated as $item) {
            $message = "Update currency {$item->getCurrency()} with rate {$item->getRate()} from client {$item->getExchange()}, difference {$item->getDifference()}";
            $output->writeln($message);
        }

        /*$em->beginTransaction();

        try {
            $this->updateRates($clientsMeta, $output, $currencies);
            $em->flush();
            $em->commit();
        } catch (\Error|\Exception $e) {
            $em->rollback();
            $output->writeln('Update currency rates failed');
            var_dump($e); //todo для отладки, перенести потом в лог
            return;
        }*/

        $output->writeln('===== DONE');
    }
}