<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Payment\Command;

use Kerosin\Component\ContainerAwareCommand;
use Payment\Component\Exchange\Client\PaybearClient;
use Payment\Entity\Payment;
use Payment\Provider\CurrencyProvider;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DebugPaybearRequestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('payment:debug:paybear_request')
            ->addArgument('currency', InputArgument::REQUIRED, 'Currency of payment')
            ->addArgument('amount', InputArgument::REQUIRED, 'Amount of payment');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $paybearClient = $container->get(PaybearClient::class);

        $currenciesList = $paybearClient->getCurrencies();
        $currenciesList = array_map('strtolower', $currenciesList);
        $currency = strtolower($input->getArgument('currency'));

        if (!in_array($currency, $currenciesList)) {
            $output->writeln('Unknown currency');
        }

        $fakePayment = new Payment();
        $fakePayment
            ->setCurrency($currency)
            ->setAmount($input->getArgument('amount'))
            ->setRate($container->get(CurrencyProvider::class)->getBestForSale(strtoupper($currency)))
        ;

        $paybearClient->processPayment($fakePayment);

        $output->write('address: ');
        $output->writeln($fakePayment->getAccount());

        $output->write('amount: ');
        $output->writeln(bcmul($fakePayment->getAmount(), $fakePayment->getRate(), 5));

        $output->write('ext_tr: ');
        $output->writeln($fakePayment->getExternalTransactionId());
    }
}