<?php
/**
 * @author Kondaurov
 */

namespace Payment\Command;

use Kerosin\Component\ContainerAwareCommand;
use Payment\Component\PaymentEvent;
use Payment\Entity\Payment;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PaymentSuccessCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('payment:success')
            ->addArgument('payment_id', InputArgument::REQUIRED, 'Payment ID')
            ->setDescription('Complete a payment');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = $this->getEntityManager();
        $payment = $entityManager->find(Payment::class, $input->getArgument('payment_id'));
        if (!$payment) {
            $output->writeln('Payment not found');
            return;
        }

        $payment->setStatus(Payment::PAYMENT_SUCCEEDED);
        $entityManager->persist($payment);
        $entityManager->flush();

        $event = new PaymentEvent();
        $event->setPayment($payment);
        $this->getContainer()->get('event_dispatcher')->dispatch(PaymentEvent::EVENT_NAME_SUCCESS, $event);

        $output->writeln('Done');
    }
}