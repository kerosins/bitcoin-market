<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Payment\Command;


use Payment\Component\Exchange\Client\CoinBase\CoinBaseWebHook;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;

class DebugCoinbasePaymentCommand extends Command
{
    private $name = 'payment:coinbase:debug';

    /**
     * @var CoinBaseWebHook
     */
    private $coinBaseWebHook;

    /**
     * @var string
     */
    private $webHookSecret;

    public function __construct(
        CoinBaseWebHook $coinBaseWebHook,
        string $webHookSecret
    ) {
        parent::__construct();
        $this->coinBaseWebHook = $coinBaseWebHook;
        $this->webHookSecret = $webHookSecret;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName($this->name)
            ->addArgument('uid', InputArgument::REQUIRED, 'Payment uid')
            ->addArgument('event', InputArgument::REQUIRED, 'Type of event')
            ->setDescription('Debug coinbase webhook')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $request = $this->createRequest(
            $input->getArgument('uid'),
            $input->getArgument('event')
        );

        $this->coinBaseWebHook->process($request);
    }

    /**
     * @param string $uid
     * @param string $event
     * @return Request
     */
    private function createRequest(string $uid, string $event): Request
    {
        $requestDataPrototype = '{"attempt_number":1,"event":{"api_version":"2018-03-22","created_at":"2019-07-12T02:13:32Z","data":{"id":"265f925f-7be4-4bcf-b4bf-7ab1ff834063","code":"Z5ZT378T","name":"Order Payment","pricing":{"usdc":{"amount":"1.785286","currency":"USDC"},"local":{"amount":"0.00016000","currency":"BTC"},"bitcoin":{"amount":"0.00016000","currency":"BTC"},"ethereum":{"amount":"0.006692000","currency":"ETH"},"litecoin":{"amount":"0.01774638","currency":"LTC"},"bitcoincash":{"amount":"0.00533247","currency":"BCH"}},"metadata":{},"payments":[{"block":{"hash":"0000000000000000001c5fc21d6af2690f8048fa139d5bce14bd4e32899e6892","height":585001,"confirmations":0,"confirmations_required":1},"value":{"local":{"amount":"0.00016000","currency":"BTC"},"crypto":{"amount":"0.00016000","currency":"BTC"}},"status":"CONFIRMED","network":"bitcoin","detected_at":"2019-07-11T21:11:33Z","transaction_id":"2768267088af7fdb0704f9a1dfe16c68c184ece88c0ef1fc628202b9fc0ead94"}],"resource":"charge","timeline":[{"time":"2019-07-11T20:44:44Z","status":"NEW"},{"time":"2019-07-11T21:11:34Z","status":"PENDING","payment":{"network":"bitcoin","transaction_id":"2768267088af7fdb0704f9a1dfe16c68c184ece88c0ef1fc628202b9fc0ead94"}},{"time":"2019-07-12T02:13:32Z","status":"COMPLETED","payment":{"network":"bitcoin","transaction_id":"2768267088af7fdb0704f9a1dfe16c68c184ece88c0ef1fc628202b9fc0ead94"}}],"addresses":{"usdc":"0x2ea3901bb5ae2c6b580afea5200364128651398a","bitcoin":"13ycVEjVe25x19JpLSnMuaibSSJdicE7gG","ethereum":"0x74c28522e5700d4af1b4154a89bee057c65e875a","litecoin":"LdXA6n3QJEZdxF2GaDXUCXeWmgGq8C7Nbo","bitcoincash":"qr0s4yt6y25y0m4alv874lfjmvtum4alagnd6uj6s7"},"created_at":"2019-07-11T20:44:44Z","expires_at":"2019-07-11T21:44:44Z","hosted_url":"https://commerce.coinbase.com/charges/Z5ZT378T","description":"Order Payment","confirmed_at":"2019-07-12T02:13:32Z","pricing_type":"fixed_price","pwcb_enabled":false},"id":"2300b1d6-4eee-447f-941d-6e07954b056e","resource":"event","type":"charge:confirmed"},"id":"156e2972-538b-4bde-ba75-d1fa4ede5268","scheduled_for":"2019-07-12T02:13:32Z"}';
        $requestData = json_decode($requestDataPrototype, true);

        $requestData['event']['type'] = $event;
        $requestData['event']['data']['id'] = $uid;

        $rawContent = json_encode($requestData);

        $request = new Request(
            [],
            [],
            [],
            [],
            [],
            [],
            $rawContent
        );

        $request->headers->add([
            'X-CC-Webhook-Signature' => hash_hmac('sha256', $rawContent, $this->webHookSecret)
        ]);

        return $request;
    }
}