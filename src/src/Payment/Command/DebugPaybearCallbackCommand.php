<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Payment\Command;


use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Kerosin\Component\ContainerAwareCommand;

use Payment\Entity\PaybearPayment;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DebugPaybearCallbackCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('payment:debug:paybear_callback')
            ->addArgument('id', InputArgument::REQUIRED,  'Id of transaction', null)
            ->addOption('invoiceId', 'i', InputOption::VALUE_OPTIONAL, 'Invoice of transaction', null)
            ->addOption('confirmationNumber', 'c', InputOption::VALUE_OPTIONAL, 'Count of confirmation numbers', null)
            ->addOption('amount', 'a', InputOption::VALUE_OPTIONAL, 'Amount', null);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getEntityManager();
        $requestData = [
            'invoice' => '',
            'confirmations' => 0,
            'maxConfirmations' => 4,
            'blockchain' => 'eth',
            'block' => [
                'number' => 4316966,
                'hash' => '0xf80718e3021cc6c226a01ea69b98131cd9b03fa5a0cac1f2469cc32d0f09e110'
            ],
            'inTransaction' => [
                'hash' => '0x7e29e165d15ec1c6fc0b71eed944471308c10d0450fe7e768843241f944bdfde',
                'exp' => 18,
                'amount' => 21000000000000
            ]
        ];

        $payment = $em->find(PaybearPayment::class, $input->getArgument('id'));
        if (!$payment) {
            throw new \RuntimeException('Payment not found');
        }

        $requestData = array_merge($requestData, [
            'invoice' => $payment->getInvoice(),
            'blockchain' => strtolower($payment->getCurrency()),
            'inTransaction' => [
                'amount' => $payment->getAmount()
            ]
        ]);

        if (($invoice = $input->getOption('invoiceId')) !== null) {
            $requestData['invoice'] = $invoice;
        }

        if (($amount = $input->getOption('amount')) !== null) {
            $requestData['inTransaction']['amount'] = (float)$amount;
        }

        if (($cNumber = $input->getOption('confirmationNumber')) !== null) {
            $requestData['confirmations'] = $cNumber;
        }

        $router = $this->getContainer()->get('router');
        $url = $this->getContainer()->getParameter('project_host') . $router->generate('paybear');

        $guzzle = new Client();
        $response = $guzzle->post($url, [
            'http_errors' => false,
            RequestOptions::JSON => $requestData
        ]);

        $output->writeln('Response status code ' . $response->getStatusCode());

        if ($response->getStatusCode() !== 200) {
            $output->writeln((string)$response->getBody());
        }

        $output->writeln('Done');
    }
}