<?php
/**
 * @author Kerosin
 */

namespace Payment\Command;

use Kerosin\Component\ArrayHelper;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderStatus;
use Catalog\Order\Service\OrderManager;
use Payment\Entity\Payment;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ExpiredCommand
 * @package Payment\Command
 */
class ExpiredCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('payment:expire')
            ->setDescription('Find payments expired and update they')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $paymentRepo = $entityManager->getRepository(Payment::class);
        $orderRepo = $entityManager->getRepository(Order::class);

        $orderManager = $this->getContainer()->get(OrderManager::class);

        $timeout = $this->getContainer()->getParameter('payment_wait_timeout');

        $payments = $paymentRepo->findLaterThanCurrentTime($timeout);
        if (!$payments) {
            $output->writeln('<info>Payments not found</info>');
            return;
        }

        $output->writeln('Found ' . count($payments) . ' payments');
        $orders = $orderRepo->findByPayments($payments);

        foreach ($payments as $payment) {
            $entityManager->beginTransaction();
            try {
                /** @var Payment $payment */
                $payment->setStatus(Payment::PAYMENT_CANCELED);

                /** @var Order $order */
                $order = ArrayHelper::arraySearch($orders, function (Order $order) use ($payment) {
                    return $order->getPayment()->getId() === $payment->getId();
                });

                if ($order instanceof Order &&
                    $order->getCurrentStatus()->getType() === OrderStatus::PAYMENT_WAIT
                ) {
                    $orderManager->updateStatus($order, OrderStatus::PAYMENT_EXPIRED);
                }

                $entityManager->persist($payment);
                $entityManager->flush();

                $entityManager->commit();

            } catch (\Exception $e) {
                $output->writeln($e->getTraceAsString());
                $entityManager->rollback();
            }
        }

        $output->writeln('<info>Finish</info>');
    }
}