<?php
/**
 * @author Kondaurov
 */

namespace Payment\Service;

use chillerlan\QRCode\QRCode;
use Payment\Entity\CurrencyRate;

class QrGenerator
{
    private static $currencyMapping = [
        CurrencyRate::BTC => 'bitcoin'
    ];

    /**
     * Generates QR code for payment
     *
     * @param string $currency
     * @param string $address
     * @param float $amount
     *
     * @return string
     */
    public function generate(
        string $currency,
        string $address,
        float $amount
    ) {
        switch ($currency) {
            case CurrencyRate::BTC:
                return $this->generateForBitcoin($address, $amount);
            case CurrencyRate::ETH:
                return $this->generateFotEthereum($address, $amount);
            case CurrencyRate::BCH:
                return $this->generateForBitcoinCash($address);
            case CurrencyRate::BTG:
                return $this->generateForBitcoinCash($address);
            case CurrencyRate::LTC:
                return $this->generateForLtc($address, $amount);
        }

        return '';
    }

    /**
     * Generates QR for bitcoin payment
     *
     * @param string $address
     * @param float $amount
     *
     * @return mixed
     */
    private function generateForBitcoin(string $address, float $amount)
    {
        $string = sprintf(
            'bitcoin:%s?amount=%F',
            $address,
            $amount
        );

        return (new QRCode())->render($string);
    }

    /**
     * Generates QR for Ethereum Payment
     *
     * @param string $address
     * @param float $amount
     *
     * @return mixed
     */
    private function generateFotEthereum(string $address, float $amount)
    {
        $string = sprintf(
            'ethereum:%s?amount=%F',
            $address,
            $amount
        );

        return (new QRCode())->render($string);
    }

    /**
     * Generates QR for Bitcoin Cash or Bitcoin Gold
     *
     * @param string $address
     *
     * @return mixed
     */
    private function generateForBitcoinCash(string $address)
    {
        return (new QRCode())->render($address);
    }

    /**
     * @param string $address
     * @param float $amount
     *
     * @return mixed
     */
    private function generateForLtc(string $address, float $amount)
    {
        $string = sprintf('litecoin:%s?amount=%F', $address, $amount);
        return (new QRCode())->render($string);
    }
}