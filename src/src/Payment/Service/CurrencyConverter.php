<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Payment\Service;


use Payment\Component\Currency;
use Payment\Provider\CurrencyProvider;

class CurrencyConverter
{
    /**
     * @var CurrencyProvider
     */
    private $currencyProvider;

    /**
     * @var int
     */
    private $cryptoScale;

    /**
     * @var int
     */
    private $fiatScale;

    public function __construct(
        CurrencyProvider $currencyProvider,
        int $cryptoScale,
        int $fiatScale
    ) {
        $this->currencyProvider = $currencyProvider;
        $this->cryptoScale = $cryptoScale;
        $this->fiatScale = $fiatScale;
    }

    /**
     * Convert from One to Currency to Another
     *
     * @param float $value
     * @param Currency|string $from
     * @param Currency|string $to
     *
     * @return float $value
     */
    public function convert(float $value, $from, $to)
    {
        $from = $from instanceof Currency ? $from : $this->currencyProvider->getCurrenciesList()->get($from);
        $to = $to instanceof Currency ? $to : $this->currencyProvider->getCurrenciesList()->get($to);

        $result = $value / $from->getRate();
        $result = $result * $to->getRate();

        $result = round($result, ($to->isFiat() ? $this->fiatScale : $this->cryptoScale));

        return (float)$result;
    }
}