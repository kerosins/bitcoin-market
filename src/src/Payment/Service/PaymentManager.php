<?php
/**
 * @author Kerosin
 */

namespace Payment\Service;

use Catalog\ORM\Repository\OrderRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Payment\Exchange\BaseExchangeClient;
use Payment\Exchange\ClientMeta;
use Payment\Exchange\PaymentProviderInterface;
use Payment\Entity\PaymentTransaction;
use Payment\Entity\CurrencyRate;
use Payment\Entity\Payment;
use Payment\Exception\NotFoundExchangeException;
use Payment\Exception\PaymentManagerException;
use Payment\Provider\CurrencyProvider;
use Payment\Provider\ExchangeClientProvider;
use Payment\Provider\ExchangeCriteria;
use Payment\Repository\PaymentRepository;

/**
 * Class PaymentManager
 * @package Payment\Service
 */
class PaymentManager
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var ExchangeClientProvider
     */
    private $exchangeProvider;

    /**
     * @var CurrencyProvider
     */
    private $currencyProvider;

    /**
     * @var PaymentTimer
     */
    private $paymentTimer;

    /**
     * @var PaymentRepository
     */
    private $paymentRepository;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        ExchangeClientProvider $exchangeClientProvider,
        CurrencyProvider $currencyProvider,
        PaymentTimer $paymentTimer,
        PaymentRepository $paymentRepository,
        OrderRepository $orderRepository
    ) {
        $this->em = $entityManager;
        $this->exchangeProvider = $exchangeClientProvider;
        $this->currencyProvider = $currencyProvider;
        $this->paymentTimer = $paymentTimer;
        $this->paymentRepository = $paymentRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Create a new payment
     *
     * @param string $currency
     * @param float $amount
     * @param float $rate
     *
     * @return Payment
     */
    public function createPayment(string $currency, float $amount, $email = null)
    {
        $payment = new Payment();
        $payment->setCurrency($currency);
        $payment->setAmount($amount);
        $payment->setEmail($email);

        $payment->setRate($this->currencyProvider->getBestForSale($currency));
        $this->paymentPreProcess($payment);

        $this->em->persist($payment);
        $this->em->flush($payment);

        $this->paymentTimer->register($payment);

        return $payment;
    }

    /**
     * Creates payment from our inner transaction
     *
     * @param \Billing\Entity\Transaction $transaction
     *
     * @return Payment
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createPaymentFromBonusTransaction(\Billing\Entity\Transaction $transaction)
    {
        $payment = new Payment();
        $payment->setExternalTransactionId($transaction->getId());
        $payment->setCurrency(CurrencyRate::ZBC);
        $payment->setAmount($transaction->getAmount());
        $payment->setStatus(Payment::PAYMENT_NONE);


        $criteria = new ExchangeCriteria();
        $criteria->setCurrencies([CurrencyRate::ZBC])->setFeatures([PaymentProviderInterface::class]);
        /** @var ClientMeta $exchange */
        $exchange = $this->exchangeProvider->search($criteria)->first();
        $payment->setExchange($exchange->getClient()->getName());

        $this->em->persist($payment);
        $this->em->flush($payment);

        return $payment;
    }

    /**
     * Prerocess a payment, creates payment in external excange
     *
     * @param Payment $payment
     * @return void
     */
    public function paymentPreProcess(Payment $payment)
    {
        if (
            $payment->getStatus() !== Payment::PAYMENT_NONE ||
            $payment->getExternalTransactionId()
        ) {
            return;
        }

        //find exchange
        /** @var \Payment\Exchange\PaymentProviderInterface $exchange */
        if ($payment->getExchange() !== null) {
            $exchange = $this->exchangeProvider->getExchangeByName($payment->getExchange());
        } else {
            $exchange = $this->getExchange($payment->getCurrency());
        }

        if (!$exchange) {
            throw new NotFoundExchangeException("Payment by {$payment->getCurrency()} for the Order unavailable now");
        }

        $payment->setExchange($exchange->getName());
        $payment = $exchange->processPayment($payment);

        $this->em->persist($payment);
        $this->em->flush($payment);
    }

    /**
     * cancel a payment
     *
     * @param Payment $payment
     */
    public function cancelPayment(Payment $payment, $status = Payment::PAYMENT_CANCELED)
    {
        if (!in_array($status, [Payment::PAYMENT_CANCELED, Payment::PAYMENT_DECLINED])) {
            throw new PaymentManagerException('Invalid status for cancel payment');
        }

        $payment->setStatus($status);
        $this->em->persist($payment);
        $this->em->flush($payment);
    }

    /**
     * Re-create payment
     *
     * @param Payment $payment
     *
     * @throws PaymentManagerException
     */
    public function refreshPayment(Payment $payment)
    {
        if (in_array($payment->getStatus(), [Payment::PAYMENT_CANCELED, Payment::PAYMENT_DECLINED])) {
            throw new PaymentManagerException('Invalid status for refresh payment');
        }

        $this->paymentRepository->getConnection()->beginTransaction();

        try {
            $payment->setStatus(Payment::PAYMENT_CANCELED);

            $order = $this->orderRepository->findOneByPayment($payment);
            $newPayment = $this->createPayment(
                $payment->getCurrency(),
                $payment->getAmount(),
                $order->getDelivery()->getEmail()
            );
            $order->addPayment($newPayment);

            $this->paymentRepository->save($payment);
            $this->orderRepository->save($order);

            $this->paymentRepository->getConnection()->commit();
        } catch (\Exception $e) {
            $this->paymentRepository->getConnection()->rollBack();
            throw $e;
        }

        return $newPayment;
    }

    /**
     * Found payment exchange
     *
     * @param $currency
     * @return mixed|null
     */
    private function getExchange($currency)
    {
        $criteria = new ExchangeCriteria();
        $criteria->setCurrencies([$currency]);
        $criteria->setFeatures([PaymentProviderInterface::class]);

        $metaData = $this->exchangeProvider->search($criteria);

        if ($metaData->count() === 0) {
            return null;
        }

        return $metaData->first()->getClient();
    }

}