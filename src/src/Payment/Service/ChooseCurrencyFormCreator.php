<?php
/**
 * @author Kondaurov
 */

namespace Payment\Service;


use Billing\DependencyInjection\BillingServicesInjectionContract;
use Billing\DependencyInjection\BillingServicesInjectionContractTrait;
use Catalog\ORM\Entity\Order;
use Payment\Provider\CurrencyProvider;
use Shop\Form\SelectCurrencyOfPaymentType;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use User\Entity\User;

class ChooseCurrencyFormCreator implements BillingServicesInjectionContract
{
    use BillingServicesInjectionContractTrait;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var CurrencyProvider
     */
    private $currencyProvider;

    /**
     * @var Router
     */
    private $router;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        FormFactoryInterface $formFactory,
        CurrencyProvider $currencyProvider,
        RouterInterface $router
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->formFactory = $formFactory;
        $this->currencyProvider = $currencyProvider;
        $this->router = $router;
    }

    /**
     * Creates form
     *
     * @param Order $order
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    public function create(Order $order)
    {
        $token = $this->tokenStorage->getToken();
        $user = null;
        $billingAccount = null;

        if ($token) {
            $user = $token->getUser();
            if (!$user instanceof User) {
                $user = null;
            }
        }

        if ($user) {
            $billingAccount = $user->getBillingAccount();
        }

        $form = $this->formFactory->create(
            SelectCurrencyOfPaymentType::class,
            [
                'currency' => $this->currencyProvider->getCurrentCurrency()
            ],
            [
                'account' => $billingAccount,
                'order_amount' => $order->getTotalPrice()->getTotal(),
                'attr' => [
                    'class' => 'js-select-currency-of-payment',
                    'data-update-total-uri' => $this->router->generate(
                        'shop.payment.total_preview',
                        [ 'hash' => $order->getHash() ]
                    ),
                ],
            ]
        );

        return $form;
    }
}