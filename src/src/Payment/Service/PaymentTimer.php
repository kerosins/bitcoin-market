<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Payment\Service;

use Payment\Entity\Payment;
use Predis\Client;

class PaymentTimer
{
    /**
     * @var Client
     */
    private $redis;

    /**
     * @var int
     */
    private $ttl;

    /**
     * PaymentTimer constructor.
     *
     * @param Client $redis
     * @param int $ttl
     */
    public function __construct(Client $redis, int $ttl)
    {
        $this->redis = $redis;
        $this->ttl = $ttl;
    }

    /**
     * @param Payment $payment
     *
     * @throws \Exception
     */
    public function register(Payment $payment)
    {
        $this->redis->set(
            $this->buildKey($payment),
            '1',
            'EX',
            $this->ttl
        );
    }

    /**
     * @param Payment $payment
     *
     * @return int
     */
    public function isActual(Payment $payment)
    {
        return $this->redis->exists($this->buildKey($payment));
    }

    /**
     * Gets second until end of payment period
     *
     * @param Payment $payment
     */
    public function getTimeLeft(Payment $payment)
    {
        return $this->redis->ttl($this->buildKey($payment));
    }

    /**
     * Returns difference in seconds between $d and $d2
     *
     * @param \DateTime $d
     * @param \DateTime $d2
     * @return int
     */
    private function diff(\DateTime $d, \DateTime $d2)
    {
        return ((int)$d->format('U')) - ((int)$d2->format('U'));
    }

    /**
     * @param Payment $payment
     *
     * @return string
     */
    private function buildKey(Payment $payment): string
    {
        return "payment:timer:" . $payment->getId();
    }
}