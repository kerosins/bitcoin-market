<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Referral\Repository;

use Kerosin\Doctrine\ORM\EntityRepository;
use Referral\Entity\ReferralSession;

/**
 * Class ReferralSessionRepository
 * @package Referral\Repository
 *
 * @method ReferralSession[]|null findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferralSessionRepository extends EntityRepository
{
}