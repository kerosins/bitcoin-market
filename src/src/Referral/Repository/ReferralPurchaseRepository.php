<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Referral\Repository;

use Catalog\ORM\Entity\Order;
use Doctrine\ORM\Query;
use Kerosin\Doctrine\ORM\EntityRepository;
use Referral\Entity\ReferralPurchase;
use Referral\Entity\ReferralSession;

/**
 * Class ReferralPurchaseRepository
 * @package Referral\Repository
 *
 * @method ReferralPurchase|null findOneByOrder(Order|int $order)
 */
class ReferralPurchaseRepository extends EntityRepository
{
    /**
     * Checks if referral purchase exist by order
     *
     * @param $order
     *
     * @return bool
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function existByOrder($order): bool
    {
        return null !== $this->createQueryBuilder('rp')
            ->andWhere('rp.order = :order')
            ->setParameter('order', $order)
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    /**
     * @param ReferralSession|string $session
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countOfPurchasesBySession($session)
    {
        return $this->createQueryBuilder('rp')
            ->select('COUNT(rp.id)')
            ->andWhere('rp.referralSession = :session')
            ->setParameter('session', $session)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param $code
     *
     * @return Query
     */
    public function getQueryForCompletedByCode($code)
    {
        return $this->createQueryBuilder('p')
            ->where('p.referralProgram = :code and p.status = :status')
            ->setParameter('status', ReferralPurchase::COMPLETE)
            ->setParameter('code', $code)
            ->orderBy('p.purchaseDate', 'DESC')
            ->getQuery();
    }
}