<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Referral\Repository;

use Doctrine\ORM\Query;
use Kerosin\Doctrine\ORM\EntityRepository;
use Referral\Entity\ReferralWithdrawal;

/**
 * Class WithdrawalRepository
 * @package Referral\Repository
 *
 * @method ReferralWithdrawal|null findOneByCode(string $code)
 */
class WithdrawalRepository extends EntityRepository
{
    /**
     * @param $user
     * @return \Doctrine\ORM\Query
     */
    public function getWithdrawalsQueryByUser($user)
    {
        return $this->createQueryBuilder('rw')
            ->andWhere('rw.user = :user')
            ->setParameter('user', $user)
            ->orderBy('rw.updatedAt', 'DESC')
            ->getQuery();
    }

    public function getWithdrawalsQuery()
    {
        return $this->createQueryBuilder('rw')
            ->orderBy('rw.created_date');
    }

    /**
     * @param string $code
     */
    public function existByCode(string $code)
    {
        return null !== $this->createQueryBuilder('w')
            ->select('w.id')
            ->andWhere('w.code = :code')
            ->setParameter('code', $code)
            ->getQuery()
            ->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);
    }
}