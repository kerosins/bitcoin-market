<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Referral\Repository;


use Kerosin\Doctrine\ORM\EntityRepository;
use User\Entity\User;

class ReferralProgramRepository extends EntityRepository
{

    /**
     * Get code of referral program by user
     *
     * @param User|int $user
     * @return null|string
     */
    public function findCodeByUser($user): ?string
    {
        return $this->createQueryBuilder('rp')
            ->select('rp.code')
            ->where('rp.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }
}