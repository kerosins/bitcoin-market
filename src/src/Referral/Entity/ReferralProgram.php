<?php
/**
 * @author Kondaurov
 */

namespace Referral\Entity;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;
use Referral\Doctrine\DBAL\Type\ReferralSettings;
use User\Entity\User;

/**
 * Class Referral
 * @package Referral\Entity
 *
 * @ORM\Entity(repositoryClass="Referral\Repository\ReferralProgramRepository")
 * @ORM\Table(name="referral_program")
 * @ORM\HasLifecycleCallbacks()
 */
class ReferralProgram
{
    use UpdateTimestamps;

    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Referral\Doctrine\UidGenerator")
     */
    private $code;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User\Entity\User", cascade={"remove"}, inversedBy="referralProgram")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="ReferralPurchase", mappedBy="referralProgram")
     */
    private $purchases;

    /**
     * @var ReferralSettings
     *
     * @ORM\Column(type="referral_settings", name="referral_settings")
     */
    private $settings;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt = null)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return ReferralProgram
     */
    public function setCode(string $code): ReferralProgram
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return ReferralProgram
     */
    public function setUser(User $user): ReferralProgram
    {
        $this->user = $user;
        $this->user->setReferralProgram($this);
        return $this;
    }

    /**
     * @return ReferralSettings
     */
    public function getSettings(): ReferralSettings
    {
        return $this->settings;
    }

    /**
     * @param array $settings
     * @return ReferralProgram
     */
    public function setSettings(ReferralSettings $settings = null): ReferralProgram
    {
        $this->settings = $settings;
        return $this;
    }
}