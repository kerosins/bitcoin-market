<?php
/**
 * @author Kondaurov
 */

namespace Referral\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;
use User\Entity\User;

/**
 * Class ReferralWithdrawal
 * @package Referral\Entity
 *
 * @ORM\Entity(repositoryClass="Referral\Repository\WithdrawalRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ReferralWithdrawal
{
    use BaseMapping, UpdateTimestamps;

    public const
        COMPLETE = 0,
        PENDING = 1,
        PROCESS = 2,
        CANCELED = 3;

    public static $statusLabel = [
        self::COMPLETE => 'Completed',
        self::PENDING => 'Pending',
        self::PROCESS => 'In Process',
        self::CANCELED => 'Canceled'
    ];

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User", cascade={"remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $amount;


    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", scale=8)
     */
    private $rate;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $status = self::PENDING;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $transaction;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=20, scale=5)
     */
    private $balance;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return ReferralWithdrawal
     */
    public function setUser(User $user): ReferralWithdrawal
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     *
     * @return ReferralWithdrawal
     */
    public function setAmount($amount): ReferralWithdrawal
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return ReferralWithdrawal
     */
    public function setComment(string $comment): ReferralWithdrawal
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return ReferralWithdrawal
     */
    public function setStatus(int $status): ReferralWithdrawal
    {
        $this->status = $status;
        return $this;
    }

    public function getReadableStatus()
    {
        return isset(self::$statusLabel[$this->status]) ? self::$statusLabel[$this->status] : '';
    }

    /**
     * @return string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return ReferralWithdrawal
     */
    public function setCurrency(string $currency): ReferralWithdrawal
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return ReferralWithdrawal
     */
    public function setAddress(string $address): ReferralWithdrawal
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return float
     */
    public function getRate(): ?float
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     * @return ReferralWithdrawal
     */
    public function setRate(float $rate): ReferralWithdrawal
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     * @return string
     */
    public function getWillTake()
    {
        return bcmul($this->amount, $this->rate, 5);
    }

    /**
     * @return string
     */
    public function getTransaction(): ?string
    {
        return $this->transaction;
    }

    /**
     * @param string $transaction
     * @return ReferralWithdrawal
     */
    public function setTransaction(string $transaction): ReferralWithdrawal
    {
        $this->transaction = $transaction;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return ReferralWithdrawal
     */
    public function setCode(string $code): ReferralWithdrawal
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     * @return ReferralWithdrawal
     */
    public function setBalance(float $balance): ReferralWithdrawal
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @todo temp
     *
     * @return bool
     */
    public function canBeCanceledByUser()
    {
        return in_array($this->status, [self::PENDING, self::PROCESS]);
    }
}