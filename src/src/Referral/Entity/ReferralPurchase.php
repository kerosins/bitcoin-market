<?php
/**
 * @author Kondaurov
 */

namespace Referral\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Catalog\ORM\Entity\Order;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class Purchase
 * @package Referral\Entity
 *
 * @ORM\Entity(repositoryClass="Referral\Repository\ReferralPurchaseRepository")
 * @ORM\Table()
 */
class ReferralPurchase
{
    use BaseMapping;

    /**
     * Constants of the Statuses
     */
    public const
        PROCESS = 10,
        COMPLETE = 20,
        CANCEL = 30;

    public static $statusLabel = [
        self::PROCESS => 'Process',
        self::COMPLETE => 'Complete',
        self::CANCEL => 'Cancel'
    ];

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=20, scale=7, nullable=true)
     */
    private $amount;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $currency;

    /**
     * @var string
     * @ORM\Column(type="decimal", precision=20, scale=7, nullable=true)
     */
    private $rate;

    /**
     * @var ReferralProgram
     *
     * @ORM\ManyToOne(targetEntity="Referral\Entity\ReferralProgram", inversedBy="purchases", cascade={"remove"})
     * @ORM\JoinColumn(name="program_id", referencedColumnName="code", onDelete="CASCADE")
     */
    private $referralProgram;

    /**
     * @var ReferralSession
     * @ORM\ManyToOne(targetEntity="Referral\Entity\ReferralSession")
     * @ORM\JoinColumn(onDelete="SET NULL", nullable=true)
     */
    private $referralSession;

    /**
     * @var Order
     * @ORM\OneToOne(targetEntity="Catalog\ORM\Entity\Order", cascade={"remove"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $order;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    private $amountBonuses;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $purchaseDate;

    /**
     * var int
     *
     * @ORM\Column(type="integer")
     */
    private $status = self::PROCESS;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return float
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     *
     * @return ReferralPurchase
     */
    public function setAmount(float $amount): ReferralPurchase
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     *
     * @return ReferralPurchase
     */
    public function setCurrency(string $currency): ReferralPurchase
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getRate(): ?string
    {
        return $this->rate;
    }

    /**
     * @param string $rate
     *
     * @return ReferralPurchase
     */
    public function setRate(string $rate): ReferralPurchase
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return ReferralPurchase
     */
    public function setStatus(int $status): ReferralPurchase
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Returns formatted status
     *
     * @return string
     */
    public function getFormattedStatus(): string
    {
        return self::$statusLabel[$this->status] ?? 'Unknown';
    }

    /**
     * @return ReferralProgram
     */
    public function getReferralProgram(): ReferralProgram
    {
        return $this->referralProgram;
    }

    /**
     * @param ReferralProgram $referralProgram
     *
     * @return ReferralPurchase
     */
    public function setReferralProgram(ReferralProgram $referralProgram): ReferralPurchase
    {
        $this->referralProgram = $referralProgram;
        return $this;
    }

    /**
     * @return Order
     */
    public function getOrder(): ?Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     *
     * @return ReferralPurchase
     */
    public function setOrder(Order $order): ReferralPurchase
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmountBonuses(): ?float
    {
        return $this->amountBonuses;
    }

    /**
     * @param float $amountBonuses
     *
     * @return ReferralPurchase
     */
    public function setAmountBonuses(float $amountBonuses): ReferralPurchase
    {
        $this->amountBonuses = $amountBonuses;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPurchaseDate(): ?\DateTime
    {
        return $this->purchaseDate;
    }

    /**
     * @param \DateTime $purchaseDate
     *
     * @return ReferralPurchase
     */
    public function setPurchaseDate(\DateTime $purchaseDate): ReferralPurchase
    {
        $this->purchaseDate = $purchaseDate;
        return $this;
    }

    /**
     * @return ReferralSession
     */
    public function getReferralSession(): ?ReferralSession
    {
        return $this->referralSession;
    }

    /**
     * @param ReferralSession $referralSession
     * @return ReferralPurchase
     */
    public function setReferralSession(ReferralSession $referralSession): ReferralPurchase
    {
        $this->referralSession = $referralSession;
        return $this;
    }
}