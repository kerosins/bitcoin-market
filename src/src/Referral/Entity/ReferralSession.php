<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Referral\Entity;


use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\UpdateTimestamps;
use User\Entity\User;

/**
 * Class ReferralSession
 * @package Referral\Entity
 *
 * @ORM\Entity(repositoryClass="Referral\Repository\ReferralSessionRepository")
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class ReferralSession
{
    public const
        STATUS_OPEN = 0,
        STATUS_CLOSE = 1;

    use UpdateTimestamps;

    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Referral\Doctrine\UidGenerator")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var ReferralProgram
     * @ORM\ManyToOne(targetEntity="Referral\Entity\ReferralProgram")
     * @ORM\JoinColumn(referencedColumnName="code", onDelete="CASCADE")
     */
    private $referralProgram;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $client;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $status = self::STATUS_OPEN;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return ReferralSession
     */
    public function setId(string $id): ReferralSession
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return ReferralProgram
     */
    public function getReferralProgram(): ReferralProgram
    {
        return $this->referralProgram;
    }

    /**
     * @param ReferralProgram $referralProgram
     * @return ReferralSession
     */
    public function setReferralProgram(ReferralProgram $referralProgram): ReferralSession
    {
        $this->referralProgram = $referralProgram;
        return $this;
    }

    /**
     * @return User
     */
    public function getClient(): ?User
    {
        return $this->client;
    }

    /**
     * @param User $client
     * @return ReferralSession
     */
    public function setClient(User $client): ReferralSession
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return ReferralSession
     */
    public function setStatus(int $status): ReferralSession
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return ReferralSession
     */
    public function setCreatedAt(\DateTime $createdAt): ReferralSession
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}