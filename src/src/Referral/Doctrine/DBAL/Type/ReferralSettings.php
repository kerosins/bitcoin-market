<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Referral\Doctrine\DBAL\Type;


use Kerosin\Doctrine\DBAL\Types\JsonObjectType;

class ReferralSettings implements JsonObjectType
{
    public const
        ON_ORDER_DONE = 'onOrderDone',
        ON_PAYMENT_ACCEPT = 'onPaymentAccept';

    public const INFINITY_PURCHASES = 0;

    /**
     * Expire of cookie in days, if 0 - infinity
     *
     * @var int
     */
    private $cookieExpire;

    /**
     * Count of purchases until cookie dead, if 0 - infinity
     *
     * @var int
     */
    private $purchaseCount;

    /**
     * When purchase should be approved
     *
     * @var string
     */
    private $approveEvent = self::ON_ORDER_DONE;

    /**
     * @return int
     */
    public function getCookieExpire(): ?int
    {
        return $this->cookieExpire;
    }

    /**
     * @param int $cookieExpire
     * @return ReferralSettings
     */
    public function setCookieExpire(int $cookieExpire = null): ReferralSettings
    {
        $this->cookieExpire = $cookieExpire;
        return $this;
    }

    /**
     * @return int
     */
    public function getPurchaseCount(): ?int
    {
        return $this->purchaseCount;
    }

    /**
     * @param int $purchaseCount
     * @return ReferralSettings
     */
    public function setPurchaseCount(int $purchaseCount = null): ReferralSettings
    {
        $this->purchaseCount = $purchaseCount;
        return $this;
    }

    public static function fromArray(array $data)
    {
        $self = new static();
        $self
            ->setCookieExpire($data['cookie_expire'] ?? null)
            ->setPurchaseCount($data['purchase_count'] ?? null)
            ->setApproveEvent($data['approve_event'] ?? self::ON_ORDER_DONE);

        return $self;
    }

    /**
     * @return string
     */
    public function getApproveEvent(): ?string
    {
        return $this->approveEvent;
    }

    /**
     * @param string $approveEvent
     * @return ReferralSettings
     */
    public function setApproveEvent(string $approveEvent): ReferralSettings
    {
        $this->approveEvent = $approveEvent;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'cookie_expire' => $this->cookieExpire,
            'purchase_count' => $this->purchaseCount,
            'approve_event' => $this->approveEvent
        ];
    }
}