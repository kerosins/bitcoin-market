<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Referral\Doctrine\DBAL\Type;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;

class ReferralSettingsType extends JsonType
{

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'referral_settings';
    }

    /**
     * @inheritdoc
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $value = parent::convertToPHPValue($value, $platform);
        return ReferralSettings::fromArray((array)$value);
    }
}