<?php
/**
 * @author Kondaurov
 */

namespace Referral\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

class UidGenerator extends AbstractIdGenerator
{
    public function generate(EntityManager $em, $entity)
    {
        $entityName = $em->getClassMetadata(get_class($entity))->getName();

        while (true) {
            $uid = uniqid();

            $item = $em->find($entityName, $uid);
            if (!$item) {
                return $uid;
            }
        }

        throw new \RuntimeException("Id couldn't generated");
    }
}