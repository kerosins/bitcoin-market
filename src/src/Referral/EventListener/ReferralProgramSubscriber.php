<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Referral\EventListener;


use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Referral\Doctrine\DBAL\Type\ReferralSettings;
use Referral\Entity\ReferralProgram;

class ReferralProgramSubscriber implements EventSubscriber
{
    /**
     * @var int
     */
    private $defaultPurchasesCount;

    /**
     * @var int
     */
    private $defaultCookieExpire;

    public function __construct(int $defaultPurchaseCount, int $defaultCookieExpire)
    {
        $this->defaultPurchasesCount = $defaultPurchaseCount;
        $this->defaultCookieExpire = $defaultCookieExpire;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postLoad,
            Events::prePersist,
            Events::preUpdate
        ];
    }

    public function postLoad(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getObject();
        if (!$entity instanceof ReferralProgram) {
            return;
        }

        /** @var ReferralSettings $settings */
        $settings = $entity->getSettings();
        if (null === $settings->getPurchaseCount()) {
            $settings->setPurchaseCount($this->defaultPurchasesCount);
        }

        if (null === $settings->getCookieExpire()) {
            $settings->setCookieExpire($this->defaultCookieExpire);
        }

        $entity->setSettings($settings);
    }

    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $this->presetDefaultSettings($eventArgs);
    }

    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        $this->presetDefaultSettings($eventArgs);
    }

    /**
     * Preset default settings
     *
     * @param LifecycleEventArgs $eventArgs
     */
    public function presetDefaultSettings(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getObject();
        if (!$entity instanceof ReferralProgram) {
            return;
        }
        /** @var ReferralSettings $settings */
        $settings = $entity->getSettings();
        if ($this->defaultPurchasesCount === $settings->getPurchaseCount()) {
            $settings->setPurchaseCount(null);
        }

        if ($this->defaultCookieExpire === $settings->getCookieExpire()) {
            $settings->setCookieExpire(null);
        }

        $entity->setSettings($settings);
    }
}