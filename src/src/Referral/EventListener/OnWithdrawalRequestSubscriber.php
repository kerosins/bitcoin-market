<?php
/**
 * @author Kondaurov
 */

namespace Referral\EventListener;


use Kerosin\Mailer\CustomMailer;
use Referral\Entity\ReferralWithdrawal;
use Referral\Event\WithdrawalEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class OnWithdrawalRequestSubscriber
 * @package Referral\EventListener
 */
class OnWithdrawalRequestSubscriber implements EventSubscriberInterface
{
    /**
     * @var CustomMailer
     */
    private $mailer;

    public function __construct(CustomMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents()
    {
        return [WithdrawalEvent::EVENT_NAME => 'onWithdrawalRequest'];
    }

    /**
     * Sends notify when user makes request
     *
     * @param WithdrawalEvent $withdrawalEvent
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function onWithdrawalRequest(WithdrawalEvent $withdrawalEvent)
    {
        if ($withdrawalEvent->getStatus() !== ReferralWithdrawal::PENDING) {
            return;
        }

        $this->mailer->sendWithdrawalNotify($withdrawalEvent->getWithdrawal());
    }
}