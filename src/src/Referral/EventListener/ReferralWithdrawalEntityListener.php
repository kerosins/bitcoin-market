<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Referral\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Kerosin\DependencyInjection\Contract\EventDispatcherContract;
use Kerosin\DependencyInjection\Contract\EventDispatcherContractTrait;
use Referral\Event\WithdrawalEvent;
use Referral\Entity\ReferralWithdrawal;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ReferralWithdrawalEntityListener
{
    use EventDispatcherContractTrait;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->setEventDispatcher($dispatcher);
    }

    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $this->onChangeStatus($eventArgs);
    }

    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        $this->onChangeStatus($eventArgs);
    }

    public function onChangeStatus(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();

        if (!$entity instanceof ReferralWithdrawal) {
            return;
        }

        $withdrawalEvent = new WithdrawalEvent();
        $withdrawalEvent->setStatus($entity->getStatus())->setWithdrawal($entity);

        $this->eventDispatcher->dispatch(WithdrawalEvent::EVENT_NAME, $withdrawalEvent);
    }
}