<?php
/**
 * @author Kondaurov
 */

namespace Referral\EventListener;

use Catalog\Order\Event\OrderEvent;
use Catalog\Order\Event\OrderStatusEvent;
use Referral\Service\ReferralManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderDoneListener implements EventSubscriberInterface
{
    /**
     * @var ReferralManager
     */
    private $referralManager;

    public function __construct(ReferralManager $referralManager)
    {
        $this->referralManager = $referralManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            OrderEvent::ORDER_DONE => 'onOrderDone'
        ];
    }

    public function onOrderDone(OrderEvent $event)
    {
        $this->referralManager->approvePurchase($event->getOrder());
    }
}