<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Referral\Service;

use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use Psr\Log\LoggerInterface;
use Referral\Doctrine\DBAL\Type\ReferralSettings;
use Referral\Entity\ReferralProgram;
use Referral\Entity\ReferralSession;
use Referral\Repository\ReferralProgramRepository;
use Referral\Repository\ReferralSessionRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use User\Entity\User;

class ReferralSessionManager implements EventSubscriberInterface
{
    /**
     * @var ReferralSessionRepository
     */
    private $referralSessionRepository;

    /**
     * @var ReferralProgramRepository
     */
    private $referralProgramRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ReferralSession|null
     */
    private $referralSession;

    /**
     * @var string
     */
    private $sessionCookieName;

    private $isCookieMustDie = false;

    public function __construct(
        ReferralSessionRepository $referralSessionRepository,
        ReferralProgramRepository $referralProgramRepository,
        LoggerInterface $logger,
        string $sessionCookieName
    ) {
        $this->referralSessionRepository = $referralSessionRepository;
        $this->referralProgramRepository = $referralProgramRepository;
        $this->logger = $logger;
        $this->sessionCookieName = $sessionCookieName;
    }

    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::SECURITY_IMPLICIT_LOGIN => 'onLogin',
            SecurityEvents::INTERACTIVE_LOGIN => 'onLogin',
            KernelEvents::RESPONSE => 'onResponse'
        ];
    }

    /**
     * @param $event
     */
    public function onLogin($event)
    {
        if ($event instanceof UserEvent) {
            $user = $event->getUser();
            $request = $event->getRequest();
        } elseif ($event instanceof InteractiveLoginEvent) {
            $user = $event->getAuthenticationToken()->getUser();
            $request = $event->getRequest();
        } else {
            return;
        }

        $program = $this->extractProgramFromRequest($request);
        if (!$program) {
            return;
        }

        $session = $this->create($user, $program);

        if ($session) {
            $this->isCookieMustDie = true;
        }
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onResponse(FilterResponseEvent $event)
    {
        if (!$this->isCookieMustDie) {
            return;
        }

        $response = $event->getResponse();
        $response->headers->clearCookie($this->sessionCookieName);

        $event->setResponse($response);
    }

    /**
     * @param ReferralProgram $program
     * @param Response $response
     * @param User|null $user
     * @return bool
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function open(
        ReferralProgram $program,
        Response $response,
        User $user = null
    ) {
        if ($user) {
            $this->create($user, $program);
            return true;
        }

        $expire = new \DateTime();
        $expireInterval = $program->getSettings()->getCookieExpire() === 0 ? 365 : $program->getSettings()->getCookieExpire();
        $expire->add(new \DateInterval("P{$expireInterval}D"));

        $cookie = new Cookie($this->sessionCookieName, $program->getCode(), $expire);
        $response->headers->setCookie($cookie);

        return true;
    }
    /**
     * Gets opened session
     *
     * @param User $user
     * @return null|object|ReferralSession
     */
    public function get(User $user)
    {
        if (!$this->referralSession) {
            /** @var ReferralSession $referralSession */
            $referralSession = $this->referralSessionRepository->findOneBy([
                'client' => $user,
                'status' => ReferralSession::STATUS_OPEN
            ]);

            if (!$referralSession) {
                return null;
            }

            //checks expire
            $expireInterval = $referralSession->getReferralProgram()->getSettings()->getCookieExpire();
            if (ReferralSettings::INFINITY_PURCHASES !== $expireInterval) {
                $created = $referralSession->getCreatedAt()->add(new \DateInterval("P{$expireInterval}D"));
                if ($created < (new \DateTime())) {
                    $this->close($referralSession);
                    return null;
                }
            }

            $this->referralSession = $referralSession;
        }

        return $this->referralSession;
    }

    /**
     * Close session
     *
     * @param ReferralSession $session
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function close(ReferralSession $session)
    {
        $session->setStatus(ReferralSession::STATUS_CLOSE);
        $this->referralSessionRepository->save($session);
    }

    /**
     * @param Request $request
     *
     * @return ReferralProgram|null
     */
    private function extractProgramFromRequest(Request $request)
    {
        if (!$request->cookies->has($this->sessionCookieName)) {
            return null;
        }

        $code = $request->cookies->get($this->sessionCookieName);
        return $this->referralProgramRepository->findOneBy(['code' => $code]);
    }

    /**
     * Opens referral session
     *
     * @param User $user
     * @param ReferralProgram $referralProgram
     * @return bool|mixed|null|ReferralSession
     * @throws \Doctrine\DBAL\ConnectionException
     */
    private function create(User $user, ReferralProgram $referralProgram)
    {
        if ($user->getId() === $referralProgram->getUser()->getId()) {
            $this->isCookieMustDie = true;
            return false;
        }

        $connection = $this->referralSessionRepository->getConnection();
        $connection->beginTransaction();

        try {
            $sessions = $this->referralSessionRepository->findBy([
                'status' => ReferralSession::STATUS_OPEN,
                'client' => $user
            ]);

            $now = new \DateTime();
            $sessions = array_filter($sessions, function (ReferralSession $session) use ($referralProgram, $now) {
                if ($session->getReferralProgram()->getCode() !== $referralProgram->getCode()) {
                    $this->close($session);
                    return false;
                }

                $cookieExpire = $referralProgram->getSettings()->getCookieExpire();
                if (ReferralSettings::INFINITY_PURCHASES === $cookieExpire) {
                    return true;
                }

                $expiredAt = $session->getCreatedAt()->add(new \DateInterval("P{$cookieExpire}D"));
                if ($expiredAt < $now) {
                    $this->close($session);
                    return false;
                }

                return true;
            });

            if (count($sessions) > 1) {
                $this->logger->warning('More than one opened referral sessions', [
                    'sessions' => array_map(function (ReferralSession $session) { return $session->getId(); }, $sessions)
                ]);
                foreach ($sessions as $session) {
                    $this->close($session);
                }
            } elseif (count($sessions) === 1) {
                $this->referralSession = reset($sessions);
                $connection->commit();
                return $this->referralSession;
            }

            $this->referralSession = new ReferralSession();
            $this->referralSession->setClient($user)->setReferralProgram($referralProgram);
            $this->referralSessionRepository->save($this->referralSession);

            $this->logger->info(
                'Started referral session',
                [
                    'owner' => $this->referralSession->getReferralProgram()->getUser()->getId(),
                    'referral' => $this->referralSession->getClient()->getId(),
                    'id' => $this->referralSession->getId()
                ]
            );

            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            $this->logger->error('Failed opening referral session', [
                'exception' => [
                    'msg' => $e->getMessage(),
                    'trace' => $e->getTraceAsString()
                ]
            ]);

            return null;
        }

        return $this->referralSession;
    }
}