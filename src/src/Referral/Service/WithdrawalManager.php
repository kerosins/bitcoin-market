<?php
/**
 * @author Kondaurov
 */

namespace Referral\Service;

use Billing\DependencyInjection\BillingServicesInjectionContract;
use Billing\DependencyInjection\BillingServicesInjectionContractTrait;
use Billing\Entity\Transaction;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Psr\Log\LoggerInterface;
use Referral\Entity\ReferralWithdrawal;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use User\Entity\BalanceChangeLog;
use User\Event\BalanceEvent;

class WithdrawalManager implements
    EntityManagerContract,
    BillingServicesInjectionContract
{
    use EntityManagerContractTrait,
        BillingServicesInjectionContractTrait;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        LoggerInterface $logger,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->logger = $logger;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param ReferralWithdrawal $withdrawal
     *
     * @return bool
     */
    public function askWithdrawal(ReferralWithdrawal $withdrawal)
    {
        $this->entityManager->beginTransaction();

        try {
            $account = $withdrawal->getUser()->getBillingAccount();
            $systemAccount = $this->accountManager->getSystemAccount();

            $balance = $this->accountManager->balance($account);

            $transaction = $this->transactionManager->addHold(
                $account,
                $systemAccount,
                $withdrawal->getAmount(),
                'Withdrawal Request'
            );

            $withdrawal
                ->setTransaction($transaction->getId())
                ->setBalance($balance);
            $this->generateCode($withdrawal);

            $this->entityManager->persist($withdrawal);
            $this->entityManager->flush();

            $balanceEvent = new BalanceEvent(
                $transaction,
                $withdrawal->getUser(),
                BalanceChangeLog::TYPE_WITHDRAWAL,
                BalanceChangeLog::STATUS_WITHDRAWAL_REQUEST,
                'Your withdrawal request has been registered by the automatic system'
            );
            $balanceEvent->setWithdrawal($withdrawal);
            $this->eventDispatcher->dispatch(
                BalanceEvent::NAME,
                $balanceEvent
            );

            $this->entityManager->commit();

            $this->logger->info('Client asked withdrawal', [
                'user_id' => $withdrawal->getUser()->getId(),
                'amount' => $withdrawal->getAmount(),
                'currency' => $withdrawal->getCurrency()
            ]);
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            $this->logger->error('Error on client\'s withdrawal request', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);

            return false;
        }

        return true;
    }

    /**
     * @param ReferralWithdrawal $withdrawal
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function processWithdrawal(ReferralWithdrawal $withdrawal)
    {
        if ($withdrawal->getStatus() === ReferralWithdrawal::PROCESS) {
            return;
        }

        $transaction = $this->transactionManager->getTransaction($withdrawal->getTransaction());
        $withdrawal->setStatus(ReferralWithdrawal::PROCESS);

        $balanceEvent = new BalanceEvent(
            $transaction,
            $withdrawal->getUser(),
            BalanceChangeLog::TYPE_WITHDRAWAL,
            BalanceChangeLog::STATUS_WITHDRAWAL_ACCEPT,
            'Your withdrawal request is accepted. Please, wait for processing by managers.'
        );
        $balanceEvent->setWithdrawal($withdrawal);
        $this->eventDispatcher->dispatch(BalanceEvent::NAME, $balanceEvent);

        $this->entityManager->persist($withdrawal);
        $this->entityManager->flush($withdrawal);
    }

    /**
     * approves a withdrawal request
     *
     * @param ReferralWithdrawal $withdrawal
     *
     * @return bool
     */
    public function approveWithdrawal(ReferralWithdrawal $withdrawal)
    {
        $this->entityManager->beginTransaction();

        try {
            $withdrawal->setStatus(ReferralWithdrawal::COMPLETE);
            $isSuccess = $this->transactionManager->releaseTransaction($withdrawal->getTransaction());

            if (!$isSuccess) {
                throw new \Exception('Transaction could not be released');
            }
            $transaction = $this->entityManager->find(Transaction::class, $withdrawal->getTransaction());
            $this->entityManager->persist($transaction);

            $this->entityManager->persist($withdrawal);
            $this->entityManager->flush();

            $balanceEvent = new BalanceEvent(
                $transaction,
                $withdrawal->getUser(),
                BalanceChangeLog::TYPE_WITHDRAWAL,
                BalanceChangeLog::STATUS_WITHDRAWAL_PROCESS,
                'Your request for withdrawal of funds has been processed. Please, wait for funds to be deposited to your wallet.'
            );
            $balanceEvent->setWithdrawal($withdrawal);
            $this->eventDispatcher->dispatch(
                BalanceEvent::NAME,
                $balanceEvent
            );

            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->logger->error('Error catched on trying to approve withdrawal', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            $this->entityManager->rollback();
            return false;
        }

        return true;
    }

    /**
     * Cancels withdrawal
     *
     * @param ReferralWithdrawal $withdrawal
     *
     * @return bool
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function cancelWithdrawal(ReferralWithdrawal $withdrawal)
    {
        $this->entityManager->beginTransaction();

        try {
            $withdrawal->setStatus(ReferralWithdrawal::CANCELED);
            $isSuccess = $this->transactionManager->cancelTransaction($withdrawal->getTransaction());

            if (!$isSuccess) {
                throw new \Exception('Transaction could not be cancelled');
            }

            $transaction = $this->entityManager->find(Transaction::class, $withdrawal->getTransaction());
            $transaction->setDescription('Withdrawal request has been cancelled by Zenithbay Admin');
            $this->entityManager->persist($transaction);

            $this->entityManager->persist($withdrawal);
            $this->entityManager->flush();

            $balanceEvent = new BalanceEvent(
                $transaction,
                $withdrawal->getUser(),
                BalanceChangeLog::TYPE_WITHDRAWAL,
                BalanceChangeLog::STATUS_WITHDRAWAL_CANCEL,
                'Your withdrawal request has been cancelled'
            );
            $balanceEvent->setWithdrawal($withdrawal);
            $this->eventDispatcher->dispatch(
                BalanceEvent::NAME,
                $balanceEvent
            );

            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->logger->error('Error catched on trying to cancel withdrawal', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            $this->entityManager->rollback();
            return false;
        }


        return $isSuccess;
    }

    /**
     * todo rewrite to Generator in entity
     * Generates an unique code
     *
     * @param ReferralWithdrawal $withdrawal
     */
    public function generateCode(ReferralWithdrawal $withdrawal)
    {
        $code = strtoupper(uniqid('ZT'));
        $repo = $this->entityManager->getRepository(ReferralWithdrawal::class);
        $repo->existByCode($code);
        while ($repo->existByCode($code)) {
            $code = strtoupper(uniqid('ZT'));
        }

        $withdrawal->setCode($code);
    }
}