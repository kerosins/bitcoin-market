<?php
/**
 * @author Kondaurov
 */

namespace Referral\Service;

use function Kerosin\Helper\StringHelper\startsWith;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

class LinkGenerator
{
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * Generates a referral link
     *
     * @param $path
     * @param $code
     * @param bool $absolute
     *
     * @return string
     */
    public function generate($path, $code, $absolute = true)
    {
        $parsed = parse_url($path);
        $routeParams = ['code' => $code];

        $isIndexPage = !$parsed || !isset($parsed['path']) || (isset($parsed['path']) && '/' === $parsed['path']);

        if ($isIndexPage) {
            $route = 'shop.referrals.link_without_slash';
        } else {
            $path = trim($parsed['path'], '/');
            $route = 'shop.referrals.link';
            $routeParams['path'] = $path;
        }

        return $this->router->generate(
            $route,
            $routeParams,
            ($absolute ? UrlGeneratorInterface::ABSOLUTE_URL : UrlGeneratorInterface::ABSOLUTE_PATH)
        );
    }
}