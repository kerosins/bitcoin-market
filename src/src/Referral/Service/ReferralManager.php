<?php
/**
 * @author Kondaurov
 */

namespace Referral\Service;


use Billing\Entity\Account;
use Billing\Service\AccountManager;
use Billing\Service\TransactionManager;
use Catalog\Order\Component\TotalPrice;
use Catalog\Order\Service\TotalAmountCalculator;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderStatus;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Payment\Entity\CurrencyRate;
use Payment\Service\CurrencyConverter;
use Psr\Log\LoggerInterface;
use Referral\Doctrine\DBAL\Type\ReferralSettings;
use Referral\Entity\ReferralProgram;
use Referral\Entity\ReferralPurchase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use User\Entity\BalanceChangeLog;
use User\Entity\User;
use User\Event\BalanceEvent;

class ReferralManager implements EntityManagerContract
{
    use EntityManagerContractTrait;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * Name of cookie that contains referral id
     *
     * @var string
     */
    private $cookieName;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var AccountManager
     */
    private $accountManager;

    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var int
     */
    private $bonusPercent;

    /**
     * @var int
     */
    private $fiatScale;

    /**
     * @var CurrencyConverter
     */
    private $currencyConverter;

    /**
     * @var ReferralSessionManager
     */
    private $referralSessionManager;

    /**
     * @var TotalAmountCalculator
     */
    private $amountCalculator;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        RequestStack $requestStack,
        TokenStorageInterface $storage,
        ReferralSessionManager $referralSessionManager,
        AccountManager $accountManager,
        TransactionManager $transactionManager,
        LoggerInterface $logger,
        CurrencyConverter $currencyConverter,
        TotalAmountCalculator $amountCalculator,
        EventDispatcherInterface $eventDispatcher,
        string $cookieName,
        int $bonusPercent,
        int $fiatScale
    ) {
        $this->requestStack = $requestStack;
        $this->tokenStorage = $storage;
        $this->referralSessionManager = $referralSessionManager;
        $this->accountManager = $accountManager;
        $this->transactionManager = $transactionManager;
        $this->currencyConverter = $currencyConverter;
        $this->logger = $logger;
        $this->amountCalculator = $amountCalculator;
        $this->eventDispatcher = $eventDispatcher;

        $this->cookieName = $cookieName;
        $this->bonusPercent = $bonusPercent;
        $this->fiatScale = $fiatScale;
    }

    /**
     * Get referral code of current user
     *
     * @return null|string
     */
    public function getCurrentUserReferralCode(): ?string
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if (!$user) {
            return null;
        }

        return $this->entityManager->getRepository(ReferralProgram::class)->findCodeByUser($user);
    }

    /**
     * Register purchase in referral program if it's possible
     *
     * @param Order $order
     */
    public function registerPurchase(Order $order)
    {
        $session = $this->referralSessionManager->get($order->getUser());
        $purchaseRepository = $this->entityManager->getRepository(ReferralPurchase::class);
        if ($purchaseRepository->existByOrder($order) || !$session) {
            return;
        }

        $program = $session->getReferralProgram();
        //checks count of purchases in current session
        if ($program->getSettings()->getPurchaseCount() !== ReferralSettings::INFINITY_PURCHASES) {
            $count = $purchaseRepository->countOfPurchasesBySession($session);
            if ($count === $program->getSettings()->getPurchaseCount()) {
                $this->referralSessionManager->close($session);
                return;
            }
        }

        $totalPrice = $this->amountCalculator->calculatePriceByOrder($order);

        $purchase = (new ReferralPurchase())
            ->setOrder($order)
            ->setReferralProgram($program)
            ->setReferralSession($session)
            ->setAmount($totalPrice->getSubTotal());

        $this->logger->info(
            "Register order #{$order->getId()} as referral purchase",
            [
                'order' => $order->getId(),
                'program' => $program->getCode(),
            ]
        );

        $this->entityManager->persist($purchase);
        $this->entityManager->flush();
    }

    /**
     * Approves referral purchase and enrolls tokens
     *
     * @param Order $order
     */
    public function approvePurchase(Order $order)
    {
        /** @var ReferralPurchase|null $purchase */
        $purchase = $this->entityManager->getRepository(ReferralPurchase::class)->findOneByOrder($order);
        if (!$purchase) {
            return;
        }

        $program = $purchase->getReferralProgram();

        if (!$this->isPermitApprovePurchase($purchase, $order)) {
            return;
        }

        $this->entityManager->beginTransaction();
        $totalPrice = $this->amountCalculator->calculatePriceByOrder($order);

        try {
            $tokens = $this->calculateAmount($totalPrice);
            $purchase
                ->setCurrency($order->getCurrency())
                ->setRate($order->getMainPayment()->getRate())
                ->setPurchaseDate(new \DateTime())
                ->setStatus(ReferralPurchase::COMPLETE)
                ->setAmountBonuses($tokens);

            $user = $program->getUser();
            $account = $user->getBillingAccount();
            if (!$account) {
                $account = $this->accountManager->create();
                $user->setBillingAccount($account);
                $this->entityManager->persist($user);
            }

            $this->enrollTokens(
                $user,
                $account,
                $tokens
            );

            $this->logger->info('Token deposit by referral purchase was completed', [
                'amount' => $tokens,
                'purchase_id' => $purchase->getId()
            ]);

            $this->entityManager->persist($purchase);
            $this->entityManager->flush();

            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            return;
        }

        if ($program->getSettings()->getPurchaseCount() === ReferralSettings::INFINITY_PURCHASES) {
            return;
        }
    }

    /**
     * Checks if purchase is permit
     *
     * @param ReferralPurchase $purchase
     * @param Order $order
     *
     * @return bool
     */
    private function isPermitApprovePurchase(ReferralPurchase $purchase, Order $order)
    {
        $logContext = [
            'order' => $order->getId(),
            'program' => $purchase->getReferralProgram()->getCode(),
            'session' => $purchase->getReferralSession()->getId(),
        ];

        $this->logger->info('Check permissions for approve purchase', $logContext);

        if ($purchase->getStatus() === ReferralPurchase::COMPLETE) {
            $this->logger->info('Check Permissions. Failed. Purchase already has been completed', $logContext);
            return false;
        }

        $status = $order->getCurrentStatus()->getType();
        if (!in_array($status, [OrderStatus::PROCESS, OrderStatus::DONE])) {
            $this->logger->info('Check Permissions. Failed. Order yet does not done', $logContext);
            return false;
        }

        $programSettings = $purchase->getReferralProgram()->getSettings();
        $session = $purchase->getReferralSession();

        $approveOrderEvent = $programSettings->getApproveEvent();
        if ($approveOrderEvent === ReferralSettings::ON_ORDER_DONE && $status !== OrderStatus::DONE) {
            $this->logger->info('Check Permissions. Failed. Need another event', $logContext);
            return false;
        }

        $purchasesInSession = $this->entityManager->getRepository(ReferralPurchase::class)->countOfPurchasesBySession($session);
        if (ReferralSettings::INFINITY_PURCHASES !== $programSettings->getPurchaseCount()
            && $purchasesInSession > $programSettings->getPurchaseCount()
        ) {
            $this->logger->info('Check Permissions. Failed. Exceed count of purchases by one session', $logContext);
            return false;
        }

        $this->logger->info('Check Permissions. Success.', $logContext);

        return true;
    }

    /**
     * Calculates how many tokens need enroll to webmaster
     *
     * @param TotalPrice $price
     */
    private function calculateAmount(TotalPrice $price)
    {
        $multiplier = bcdiv($this->bonusPercent, 100, 2);
        $bonus = bcmul($price->getSubTotal(), $multiplier, $this->fiatScale);

        return $this->currencyConverter->convert($bonus, 'USD',CurrencyRate::ZBC);
    }

    /**
     * Enrolls tokens to web master
     *
     * @param User $user
     * @param Account $account
     * @param $amount
     */
    private function enrollTokens(User $user, Account $account, float $amount)
    {
        $description = 'ZBC deposit by referral program';
        $transaction = $this->transactionManager->add(
            $this->accountManager->getSystemAccount(),
            $account,
            $amount,
            $description
        );

        $event = new BalanceEvent(
            $transaction,
            $user,
            BalanceChangeLog::STATUS_DEPOSIT,
            BalanceChangeLog::STATUS_DEPOSIT,
            $description
        );
        $this->eventDispatcher->dispatch(BalanceEvent::NAME, $event);
    }
}