<?php
/**
 * @author Kondaurov
 */

namespace Referral\Service;


use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\DependencyInjection\Contract\LoggerContract;
use Kerosin\DependencyInjection\Contract\LoggerContractTrait;
use Referral\Doctrine\DBAL\Type\ReferralSettings;
use Referral\Entity\ReferralProgram;
use User\Entity\User;

class ReferralCreator implements EntityManagerContract, LoggerContract
{
    use EntityManagerContractTrait, LoggerContractTrait;

    /**
     * Create new referral program to User
     *
     * @param User $user
     * @param bool $flush - flush to DB
     *
     * @return ReferralProgram
     */
    public function attachReferralProgram(User $user, $flush = false)
    {
        $program = new ReferralProgram();
        $program->setUser($user);
        $program->setSettings(new ReferralSettings());

        if ($flush) {
            $this->entityManager->persist($program);
            $this->entityManager->flush();
        }

        return $program;
    }
}