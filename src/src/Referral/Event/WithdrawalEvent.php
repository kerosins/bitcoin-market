<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Referral\Event;

use Referral\Entity\ReferralWithdrawal;
use Symfony\Component\EventDispatcher\Event;

class WithdrawalEvent extends Event
{
    const EVENT_NAME = 'withdrawal.change_status';

    /**
     * @var ReferralWithdrawal
     */
    private $withdrawal;

    /**
     * @var int
     */
    private $status;

    /**
     * @return ReferralWithdrawal
     */
    public function getWithdrawal(): ?ReferralWithdrawal
    {
        return $this->withdrawal;
    }

    /**
     * @param ReferralWithdrawal $withdrawal
     * @return WithdrawalEvent
     */
    public function setWithdrawal(ReferralWithdrawal $withdrawal): WithdrawalEvent
    {
        $this->withdrawal = $withdrawal;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return WithdrawalEvent
     */
    public function setStatus(int $status): WithdrawalEvent
    {
        $this->status = $status;
        return $this;
    }
}