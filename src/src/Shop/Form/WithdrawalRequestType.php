<?php
/**
 * @author Kondaurov
 */

namespace Shop\Form;

use Billing\Entity\Account;
use Billing\Service\AccountManager;
use Payment\Component\Currency;
use Payment\Provider\CurrencyProvider;
use Referral\Entity\ReferralWithdrawal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Expression;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class WithdrawalRequestType extends AbstractType
{
    /**
     * @var CurrencyProvider
     */
    private $currencyProvider;

    /**
     * @var AccountManager
     */
    private $accountManager;

    public function __construct(CurrencyProvider $currencyProvider, AccountManager $accountManager)
    {
        $this->currencyProvider = $currencyProvider;
        $this->accountManager = $accountManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $currencies = [];
        $this->currencyProvider->getAllowedCurrenciesList()->forAll(
            function ($key, Currency $currency) use (&$currencies) {
                $currencies["{$currency->getName()} ({$currency->getCode()})"] = $key;

                return true;
            }
        );

        $builder
            ->add(
                'amount',
                TextType::class,
                [
                    'label' => 'The amount of bonuses to withdraw',
                    'constraints' => [
                        new NotBlank(),
                        new Callback([
                            'callback' => function ($value, ExecutionContextInterface $context) use ($options) {
                                $fValue = (string)(float)$value;
                                if (strcmp($value, $fValue) !== 0) {
                                    $context->addViolation('Amount must be numeric');
                                    return;
                                }

                                if ((float)$value > $this->accountManager->balance($options['account'])) {
                                    $context->addViolation('Insufficient funds');
                                }
                            }
                        ])
                    ]
                ]
            )
            ->add(
                'currency',
                ChoiceType::class,
                [
                    'choices' => $currencies,
                    'choice_attr' => function ($choiceValue, $key, $value) {
                        return [
                            'data-rate' => $this->currencyProvider->getBestForSale($value)
                        ];
                    },
                ]
            )
            ->add(
                'address',
                TextType::class,
                [
                    'constraints' => [
                        new NotBlank()
                    ]
                ]
            );

        $builder->get('amount')->addModelTransformer(
            new CallbackTransformer(
                function ($value) {
                    return str_replace(',', '.', $value);
                },
                function ($value) {
                    return str_replace(',', '.', $value);
                }
            )
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', ReferralWithdrawal::class);

        $resolver->setDefault('account', null);
        $resolver->setAllowedTypes('account', [Account::class, 'null']);
        $resolver->setRequired('account');
    }
}