<?php
/**
 * @author Kerosin
 */

namespace Shop\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Kerosin\Form\BaseForm;
use Shop\Entity\AddressObject;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContext;

class AddressType extends BaseForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('q', TextType::class)
            ->add('parent', TextType::class)
            ->add('country', TextType::class, [
                'constraints' => new NotBlank()
            ])
            ->add('type', TextType::class, [
                'constraints' => new NotBlank()
            ]);
    }

    public function validate($value, ExecutionContext $context)
    {
        if (!in_array($value['type'], [AddressObject::CITY_TYPE, AddressObject::STATE_TYPE])) {
            $context->buildViolation('Invalid type\'s value')
                ->atPath('type')
                ->addViolation();
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'method' => 'GET',
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}