<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Form;

use Doctrine\ORM\EntityRepository;
use Shop\Entity\Country;
use Shop\Entity\Feedback\AffiliateProgram;
use Shop\Entity\Feedback\ComplaintAgreements;
use Shop\Entity\Feedback\ComplaintIntellectualRights;
use Shop\Entity\Feedback\Feedback;
use Shop\Entity\Feedback\NotFoundProduct;
use Shop\Entity\Feedback\ProhibitedProducts;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;

class FeedbackFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dataClass = $options['data_class'];
        $fields = $this->getFieldsList($dataClass);

        foreach ($fields as $field) {
            $builder->add(...$field);
        }
    }

    /**
     * Gets a list of fields
     *
     * @param $dataClass
     *
     * @return array
     */
    private function getFieldsList($dataClass) {
        $fields = [
            Feedback::class => [
                [
                    'fullName',
                    TextType::class,
                    [
                        'label' => 'Your name',
                        'required' => false,
                        'constraints' => [
                            new NotBlank()
                        ]
                    ]
                ],
                [
                    'email',
                    EmailType::class,
                    [
                        'constraints' => [
                            new NotBlank(),
                            new Email()
                        ]
                    ]
                ],
                [
                    'comment',
                    TextareaType::class,
                    [
                        'constraints' => [
                            new NotBlank()
                        ]
                    ]
                ]
            ],
            NotFoundProduct::class => [
                [
                    'fullName',
                    TextType::class,
                    [
                        'label' => 'Your name',
                        'constraints' => [
                            new NotBlank()
                        ]
                    ]
                ],
                [
                    'email',
                    EmailType::class,
                    [
                        'constraints' => [
                            new NotBlank(),
                            new Email()
                        ]
                    ]
                ],
                [
                    'url',
                    TextType::class,
                    [
                        'label' => "The URL of the product you wish to buy",
                        'constraints' => [
                            new NotBlank(),
                        ]
                    ]
                ],
                [
                    'comment',
                    TextareaType::class,
                    [
                        'required' => false
                    ]
                ]
            ],
            ComplaintAgreements::class => [
                [
                    'fullName',
                    TextType::class,
                    [
                        'label' => 'Your name',
                        'constraints' => [
                            new NotBlank()
                        ]
                    ]
                ],
                [
                    'email',
                    EmailType::class,
                    [
                        'constraints' => [
                            new NotBlank(),
                            new Email()
                        ]
                    ]
                ],
                [
                    'url',
                    TextType::class,
                    [
                        'label' => "Product or Seller Url",
                        'constraints' => [
                            new NotBlank(),
                        ]
                    ]
                ],
                [
                    'comment',
                    TextareaType::class,
                    [
                        'constraints' => [
                            new NotBlank()
                        ]
                    ]
                ]
            ],
            AffiliateProgram::class => [
                [
                    'fullName',
                    TextType::class,
                    [
                        'label' => 'Your name',
                        'constraints' => [
                            new NotBlank()
                        ]
                    ]
                ],
                [
                    'email',
                    EmailType::class,
                    [
                        'constraints' => [
                            new NotBlank(),
                            new Email()
                        ]
                    ]
                ],
                [
                    'url',
                    TextType::class,
                    [
                        'label' => "Link to website (seller's page on off-site trading platforms) if exist",
                        'required' => false
                    ]
                ],
                [
                    'comment',
                    TextareaType::class,
                    [
                        'required' => false
                    ]
                ]
            ],
            ComplaintIntellectualRights::class => [
                [
                    'email',
                    EmailType::class,
                    [
                        'constraints' => [
                            new Email(),
                            new NotBlank()
                        ]
                    ]
                ],
                [
                    'url',
                    TextType::class,
                    [
                        'label' => "Product or Store Url",
                        'constraints' => [
                            new NotBlank()
                        ]
                    ]
                ],
                [
                    'comment',
                    TextareaType::class,
                    [
                        'constraints' => [
                            new NotBlank()
                        ]
                    ]
                ]
            ],
            ProhibitedProducts::class => [
                [
                    'url',
                    TextType::class,
                    [
                        'label' => "Product's or Seller's URL",
                        'constraints' => [
                            new NotBlank(),
                        ]
                    ]
                ],
                [
                    'kind',
                    ChoiceType::class,
                    [
                        'label' => 'What kind of prohibited product?',
                        'choices' => ProhibitedProducts::getProhibitedOptions(),
                        'constraints' => [
                            new NotBlank()
                        ]
                    ]
                ],
                [
                    'country',
                    EntityType::class,
                    [
                        'label' => 'Country in which the specified item is prohibited',
                        'class' => Country::class,
                        'choice_label' => 'title',
                        'query_builder' => function (EntityRepository $repository) {
                            return $repository->createQueryBuilder('c')->orderBy('c.title', 'ASC');
                        },
                        'constraints' => [
                            new NotBlank()
                        ]
                    ]
                ],
                [
                    'email',
                    EmailType::class,
                    [
                        'constraints' => [
                            new NotBlank(),
                            new Email()
                        ]
                    ]
                ],
                [
                    'comment',
                    TextareaType::class,
                    [
                        'required' => false
                    ]
                ]
            ]
        ];

        return $fields[$dataClass] ?? null;
    }
}