<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Form;

use Billing\Entity\Account;
use Billing\Service\AccountManager;
use Payment\Component\Currency;
use Payment\Entity\CurrencyRate;
use Payment\Provider\CurrencyProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class SelectCurrencyOfPaymentType extends AbstractType
{
    /**
     * @var AccountManager
     */
    private $accountManager;

    /**
     * @var CurrencyProvider
     */
    private $currencyProvider;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(
        AccountManager $accountManager,
        CurrencyProvider $currencyProvider,
        TranslatorInterface $translator
    ) {
        $this->accountManager = $accountManager;
        $this->currencyProvider = $currencyProvider;
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = [];
        $this->currencyProvider->getAllowedCurrenciesList()->forAll(function ($i, $currency) use (&$choices) {
            $choices[$currency->getName()] = $currency->getCode();

            return true;
        });

        $builder
            ->add(
                'currency',
                ChoiceType::class,
                [
                    'choices' => $choices,
                    'empty_data' => $this->currencyProvider->getCurrentCurrency()->getCode(),
                    'attr' => [
                        'class' => 'js-payment-currency'
                    ]
                ]
            )
            ->add('country', HiddenType::class, [ 'required' => false ]);

        if ($options['account']) {
            $balance = $this->accountManager->balance($options['account']);
            if ($balance > 0) {
                $bonusLabel = $this->translator->trans('You can use your bonuses for pay (choose amount)');
                $amount = $this->currencyProvider->convertToBestRate($options['order_amount'], CurrencyRate::ZBC);
                $builder->add(
                    'znt',
                    NumberType::class,
                    [
                        'label' => $bonusLabel,
                        'required' => false,
                        'constraints' => [
                            new Range([
                                'min' => 0,
                                'max' => $options['order_amount'],
                                'maxMessage' => 'Amount of bonuses should be less than total price of order'
                            ]),
                            new Callback([
                                'callback' => function ($value, ExecutionContextInterface $context) use ($options, $balance, $amount) {
                                    $message = $this->translator->trans('You dont have enough bonuses');
                                    $violation = $context->buildViolation($message);

                                    if (!$options['account'] && $value > 0) {
                                        $violation->addViolation();
                                        return;
                                    }

                                    if ($balance < $value) {
                                        $violation->addViolation();
                                        return;
                                    }
                                }
                            ])
                        ],
                        'attr' => [
                            'class' => 'js-payment-bonus',
                            'data-order-amount' => $options['order_amount'],
                            'data-balance' => $balance,
                        ]
                    ]
                );
            }
        }

    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('allow_extra_fields', true);

        $resolver->setDefault('account', null);
        $resolver->setAllowedTypes('account', [Account::class, 'null']);

        $resolver->setDefault('order_amount', null);
        $resolver->setAllowedTypes('order_amount', ['float', 'string']);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}