<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Form;

use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Shop\Entity\Newsletter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContext;
use User\Entity\User;

class SubscribeNewsletterFormType extends AbstractType implements EntityManagerContract
{
    use EntityManagerContractTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
            'required' => false,
            'constraints' => [
                new NotBlank(),
                new Email(),
                new Callback([
                    'callback' => function ($value, ExecutionContext $context) use ($options) {
                        $userRepo = $this->entityManager->getRepository(User::class);
                        $newsletterRepo = $this->entityManager->getRepository(Newsletter::class);
                        /** @var User|null $user */
                        $user = $options['user'];
                        if ($newsletterRepo->findOneBy(['email' => $value])) {
                            $context->addViolation('This email already exist in newsletter list');
                            return;
                        }

                        if (!$user && $userRepo->existUserWithEmail($value)) {
                            $context->addViolation('Account with specified email already exist, please sign in');
                            return;
                        }

                        if ($user
                            && $user->getEmail() !== $value
                            && $userRepo->existUserWithEmail($value)
                        ) {
                            $context->addViolation('Account with specified email belongs another user');
                            return;
                        }

                        if ($user && $newsletterRepo->findOneBy(['user' => $user])) {
                            $context->addViolation('This user already subscribed');
                            return;
                        }
                    }
                ])
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('user', null);
        $resolver->setAllowedTypes('user', ['null', User::class]);
    }
}