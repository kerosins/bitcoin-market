<?php
/**
 * @author Kerosin
 */

namespace Shop\Form;

use Catalog\ORM\Entity\FacetCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Kerosin\Form\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CatalogFilterType extends AbstractType
{
    /**
     * @var array
     */
    private $options;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->options = $options;
        $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'onPresetData']);
        $builder->addEventListener(FormEvents::SUBMIT, [$this, 'onSubmit']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'method' => 'get',
            'config_fields' => []
        ]);

        $resolver->setAllowedTypes('config_fields', 'array');
    }

    public function onPreSetData(FormEvent $event)
    {
        /** @var FormInterface $form */
        $form = $event->getForm();
        $data = $event->getData();

        foreach ($form as $name => $value) {
            $form->remove($name);
        }

        foreach ($this->options['config_fields'] as $name => $value) {
            $type = $value['type'];
            if ($type === FacetCategory::LIST) {
                $form->add(
                    $this->makeName($value),
                    ChoiceType::class,
                    [
                        'label' => $value['title'],
                        'choices' => $value['data'],
                        'multiple' => true,
                        'expanded' => true,
                        'required' => false
                    ]
                );
            } elseif ($type === FacetCategory::RANGE) {
                $form->add($this->makeName($value), RangeType::class, [
                    'label' => $value['title'],
                    'required' => false,
                    'attr' => [
                        'inline' => true
                    ]
                ]);
            } else if ($type === FacetCategory::BOOL) {
                $form->add(
                    $this->makeName($value),
                    ChoiceType::class,
                    [
                        'label' => $value['title'],
                        'choices' => [
                            'Unknown' => null,
                            'Yes' => 1,
                            'No' => 0
                        ],
                        'expanded' => true
                    ]
                );
            }
        }
    }

    public function onSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();
        //todo не самая лучшая проверка на пустые значения, неочевидная
        foreach ($data as $name => $value) {
            if ($value === null ||
                (is_array($value) && count($value) == 0)
            ) {
                unset($data[$name]);
                continue;
            }

            if (is_array($value) && array_key_exists('min', $value) && array_key_exists('max', $value)) {
                if (empty($value['min']) && empty($value['max'])) {
                    unset($data[$name]);
                }
            }
        }

        $event->setData($data);
    }

    private function makeName($field, $postfix = null)
    {
        if ($postfix) {
            $postfix = ':' . $postfix;
        }

        return "filter:${field['id']}{$postfix}";
    }

    public function getBlockPrefix()
    {
        return '';
    }
}