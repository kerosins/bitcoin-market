<?php
/**
 * @author Kerosin
 */

namespace Shop\Form;

use Doctrine\ORM\EntityRepository;
use Catalog\Order\Form\DeliveryInfoType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use User\Component\Validator\UniqueEmail;
use User\Entity\User;
use User\Entity\UserDeliveryInfo;

class OrderDeliveryType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('delivery', DeliveryInfoType::class, [
            'state_autocomplete_route' => 'shop.address.autocomlete',
            'city_autocomplete_route' => 'shop.address.autocomlete',
            'zip_autocomplete_route' => 'shop.address.zip',
        ]);

        if ($options['user']) {
            $builder->add('userDeliveryInfo', EntityType::class, [
                'class' => UserDeliveryInfo::class,
                'label' => 'Shipping information',
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => false,
                'placeholder' => 'Create a new shipping info',
                'required' => false,
                'empty_data' => null,
                'query_builder' => function (EntityRepository $repository) use ($options) {
                    return $repository->createQueryBuilder('ud')
                        ->andWhere('ud.user = :user')
                        ->setParameter('user', $options['user'])
                        ;
                },
                'choice_attr' => function (UserDeliveryInfo $model, $key, $index) {
                //fixme
                    $data = $model->toArray();
                    if ($model->getCountry()->getAllowAutocomplete()) {
                        $zipCode = $model->getZip();
                        $zip = $this->em
                            ->getRepository('ShopBundle:ZipCode')
                            ->findOneByCodeAndCountry($zipCode, $model->getCountry())
                        ;
                        $data['zip'] = [
                            'id' => $zip->getId(),
                            'title' => $zipCode
                        ];
                    }

                    return [
                        'data-info' => json_encode($data),
                        'data-id' => $model->getId(),
                    ];
                },
                'attr' => [
                    'class' => 'js-user-delivery-info'
                ]
            ]);
        } else {
            $builder->add('email', EmailType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Email(),
                    new UniqueEmail()
                ]
            ]);
            $builder->add('createNewAccount', CheckboxType::class, [
                'label' => 'Create an account for later use?',
                'required' => false
            ]);
        }

        $builder->add('comment', TextareaType::class, [
            'required' => false,
            'label' => 'Leave your comment for seller'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'user' => null
        ]);
        $resolver->setAllowedTypes('user', [User::class, 'null']);
    }
}