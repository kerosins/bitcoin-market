<?php
/**
 * @author Kondaurov
 */

namespace Shop\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Component\Image;
use Kerosin\Controller\BaseController;
use Kerosin\Filesystem\ImageHelper;
use Kerosin\Filesystem\ImageUploadStack;
use Kerosin\Form\Type\XhrFileType;
use Kerosin\Mailer\CustomMailer;
use Catalog\ORM\Entity\Dispute;
use Catalog\ORM\Entity\Message\BaseMessage;
use Catalog\ORM\Entity\Message\DisputeMessage;
use Catalog\ORM\Entity\Message\MessageContract;
use Catalog\ORM\Entity\Message\OrderMessage;
use Catalog\ORM\Entity\Order;
use Shop\Form\MessageFormType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ChatController extends BaseController
{
    /**
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @param string $type
     * @param MessageContract|object|int $target
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function sendAction(
        EntityManagerInterface $entityManager,
        Request $request,
        CustomMailer $customMailer,
        string $type,
        int $target
    ) {
        /** @var Dispute|Order $target */
        switch ($type) {
            case BaseMessage::TYPE_DISPUTE:
                $entity = new DisputeMessage();
                $target = $entityManager->find(Dispute::class, $target);
                break;
            case BaseMessage::TYPE_ORDER:
                $entity = new OrderMessage();
                $target = $entityManager->find(Order::class, $target);
                break;
            default:
                return $this->jsonException(new BadRequestHttpException('Unknown type of message'));
        }

        $form = $this->createForm(MessageFormType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $user = $this->getUser();

                $entity
                    ->setFrom($user)
                    ->setFromDisplayName($user->getUsername());

                $this->saveAttachments(
                    $entity,
                    $request->get('stack_id')
                );

                $target->addMessage($entity);
                $this->flushEntities([$target, $entity]);

                if (BaseMessage::TYPE_DISPUTE === $type) {
                    $customMailer->newMessageDisputeForModerators($target);
                } else if (BaseMessage::TYPE_ORDER === $type) {
                    $customMailer->sendNotifyToModerators($target);
                }

                $messages = $target->getMessages()->toArray();
                usort($messages, function (BaseMessage $message, BaseMessage $message2) {
                    return $message->getCreatedAt() <> $message2->getCreatedAt();
                });

                $messagesList = $this->renderView('@Shop/account/chat/message_list.part.twig', [
                    'messages' => $messages
                ]);

                return $this->json([
                    'messages' => $messagesList
                ], 201);
            } else {
                return $this->jsonException(new BadRequestHttpException('Invalid data'));
            }
        } else {
            return $this->jsonException(new BadRequestHttpException('Empty data'));
        }
    }

    /**
     * Registers
     *
     * @param ImageUploadStack $imageUploadStack
     * @param string $type
     *
     * @return mixed|string
     */
    public function tokenAction(ImageUploadStack $imageUploadStack, string $type)
    {
        return $this->json([
            'token' => $imageUploadStack->registerStack($type)
        ]);
    }

    /**
     * Uploads attachments
     *
     * @param ImageUploadStack $imageUploadQueue
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function attachmentAction(
        ImageUploadStack $imageUploadQueue,
        Request $request
    ) {
        $form = $this->createForm(XhrFileType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            /** @var UploadedFile $file */
            $file = $data['file'];
            try {
                $src = $imageUploadQueue->addToStack($file, $data['id'], $data['type']);
            } catch (\RuntimeException $e) {
                return $this->jsonException(new BadRequestHttpException('Server error when save file'));
            }

            return $this->json([ 'file' => $src ], 201);
        }

        return $this->jsonException(new BadRequestHttpException('Invalid request'));
    }

    /**
     *
     * Saves attachments
     *
     * @param BaseMessage $entity
     * @param string|null $stackId
     */
    private function saveAttachments(BaseMessage $entity, string $stackId = null)
    {
        if (!$entity->allowAttachments() || !$stackId) {
            return;
        }

        $imageStack = $this->get(ImageUploadStack::class);

        $attachments = $imageStack->getStack($stackId);
        $attachments = array_map(function ($src) {
            return (new Image($src));
        }, $attachments);

        $entity->setAttachments($attachments);
    }
}