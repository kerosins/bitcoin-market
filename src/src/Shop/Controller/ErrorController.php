<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Controller;

use Symfony\Bundle\TwigBundle\Controller\ExceptionController;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

class ErrorController extends ExceptionController
{
    protected function findTemplate(Request $request, $format, $code, $showException)
    {
        $name = $showException ? 'exception' : 'error';
        if ($showException && 'html' === $format) {
            $name = 'exception_full';
        }

        // For error pages, try to find a template for the specific HTTP status code and format
        if (!$showException) {
            $template = sprintf('@Shop/error/%s%s.%s.twig', $name, $code, $format);//
            if ($this->templateExists($template)) {
                return $template;
            }
        }

        // try to find a template for the given format
        $template = sprintf('@Shop/error/%s.%s.twig', $name, $format);
        if ($this->templateExists($template)) {
            return $template;
        }

        $originTemplate = '@Twig/Exception/%s.%s.twig';
        if ($this->templateExists($originTemplate)) {
            return $originTemplate;
        }

        // default to a generic HTML exception
        $request->setRequestFormat('html');
        if ($showException) {
            return '@Twig/Exception/exception_full.html.twig';
        }

        return sprintf('@Shop/Exception/%s.html.twig', $showException ? 'exception_full' : $name);
    }
}