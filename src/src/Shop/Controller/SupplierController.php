<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Controller;

use Kerosin\Controller\BaseController;
use Catalog\ORM\Entity\Category;
use Shop\Entity\Seller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SupplierController extends BaseController
{
    private $supplierRepo;

    private $categoryRepo;

    public function indexAction(int $supplierId, int $categoryId = null)
    {
        /** @var Seller $supplier */
        $supplier = $this->getSupplierRepo()->find($supplierId);
        if (!$supplier) {
            throw new NotFoundHttpException('Store not found');
        }

        $this->addBreadCrumb($supplier->getName(), $this->generateUrl('shop.supplier.index', ['supplierId' => $supplierId]));

        if ($categoryId) {
            $categoryRepo = $this->getCategoryRepo();
            $category = $categoryRepo->find($categoryId);
            if (!$category) {
                throw new NotFoundHttpException();
            }
        }
    }

    public function productAction(int $store, int $product)
    {

    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository
     */
    private function getSupplierRepo()
    {
        if (!$this->supplierRepo) {
            $this->supplierRepo = $this->getEm()->getRepository(Seller::class);
        }

        return $this->supplierRepo;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository|\Catalog\ORM\Repository\CategoryRepository
     */
    private function getCategoryRepo()
    {
        if (!$this->categoryRepo) {
            $this->categoryRepo = $this->getEm()->getRepository(Category::class);
        }

        return $this->categoryRepo;
    }
}