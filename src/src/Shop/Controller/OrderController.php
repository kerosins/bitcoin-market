<?php
/**
 * @author Kerosin
 */

namespace Shop\Controller;

use Billing\Service\TransactionManager;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Catalog\Import\Service\ProductAvailableProvider;
use Kerosin\Controller\BaseController;
use Kerosin\Mailer\CustomMailer;
use Catalog\Order\Component\DeliveryInfoTransmitter;
use Catalog\ORM\Entity\DeliveryInfo;
use Catalog\ORM\Entity\Message\BaseMessage;
use Catalog\ORM\Entity\Message\OrderMessage;
use Catalog\ORM\Entity\OrderDeliveryInfo;
use Catalog\Order\Exception\OrderManagerException;
use Catalog\ORM\Repository\OrderRepository;
use Catalog\Order\Service\OrderAccessManager;
use Catalog\Order\Service\OrderManager;
use Catalog\Order\Service\OrderProvider;
use Catalog\Order\Cart\ProductCart;
use Payment\Entity\CurrencyRate;
use Payment\Entity\Payment;
use Payment\Provider\CurrencyProvider;
use Payment\Service\ChooseCurrencyFormCreator;
use Payment\Service\PaymentManager;
use Referral\Service\ReferralManager;
use Catalog\Order\Cart\Item;
use Catalog\ORM\Entity\Order;
use Shop\Entity\OrderDelivery;
use Catalog\ORM\Entity\OrderPosition;
use Catalog\ORM\Entity\OrderStatus;
use Catalog\ORM\Entity\Product;
use Shop\Form\MessageFormType;
use Shop\Form\OrderDeliveryType;
use Shop\Form\SelectCurrencyOfPaymentType;
use Shop\Widget\ProductCartWidget;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use User\Entity\User;
use User\Entity\UserDeliveryInfo;
use User\Exception\UserManagerException;
use User\Service\UserCountry;
use User\Service\UserDataManager;

class OrderController extends BaseController
{
    /**
     * Checkout cart action
     *
     * @param Request $request
     * @param \Catalog\Order\Cart\ProductCart $cart
     * @param ProductCartWidget $cartWidget
     * @param OrderManager $orderManager
     * @param UserCountry $userCountry
     * @param UserDataManager $userDataManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deliveryAction(
        Request $request,
        ProductCart $cart,
        ProductCartWidget $cartWidget,
        OrderManager $orderManager,
        UserCountry $userCountry,
        UserDataManager $userDataManager,
        ChooseCurrencyFormCreator $chooseCurrencyFormCreator,
        TransactionManager $transactionManager,
        ReferralManager $referralManager,
        ProductAvailableProvider $availableProvider,
        CustomMailer $customMailer
    ) {
        if ($cart->get()->count() === 0) {
            return $this->redirectToRoute('homepage');
        }

        $entityManager = $this->getEm();

        /**
         * not show cart icon
         */
        $cartWidget->setIsShow(false);
        $user = $this->getUser();

        $order = $orderManager->prepareOrderFromCart($cart);
        $unavailableProducts = $availableProvider->checkOrder($order);

        if ($unavailableProducts) {
            $ids = array_map(function (Product $product) {
                return $product->getId();
            }, $unavailableProducts);

            return $this->redirectToRoute('shop.catalog.unavailable', [
                'p' => implode(',', $ids)
            ]);
        }

        if (($deliveryInfo = $order->getDelivery()) === null) {
            $deliveryInfo = new OrderDeliveryInfo();
            $deliveryInfo->setCountry($userCountry->get());
        }

        //choose currency form
        $chooseCurrencyForm = $chooseCurrencyFormCreator->create($order);
        $chooseCurrencyForm->handleRequest($request);

        //delivery form
        $form = $this->createForm(
            OrderDeliveryType::class,
            [ 'delivery' => $deliveryInfo ],
            [
                'user' => $user,
                'allow_extra_fields' => true
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() &&
            $form->isValid() &&
            $chooseCurrencyForm->isSubmitted() &&
            $chooseCurrencyForm->isValid()
        ) {
            $entityManager->beginTransaction();
            try {
                $data = $form->getViewData();
                /** @var OrderDeliveryInfo $deliveryInfo */
                $deliveryInfo = $data['delivery'];

                //create user if need
                if (!$user) {
                    $user = new User();
                    $user->setEmail($deliveryInfo->getEmail());
                    $user->setCountry($deliveryInfo->getCountry());
                    $user = $userDataManager->createHiddenAccountIfNotExists($user);

                    if (isset($data['createNewAccount'])) {
                        $userDataManager->enableHiddenUser($user);
                    }
                }
                //create a new user delivery info or transmit info from it
                if (!empty($data['userDeliveryInfo']) && $data['userDeliveryInfo'] instanceof UserDeliveryInfo) {
                    DeliveryInfoTransmitter::transmit($data['userDeliveryInfo'], $deliveryInfo);
                    $deliveryInfo->setEmail($user->getEmail());
                } else {
                    $deliveryInfo->setEmail($user->getEmail());
                    $userDataManager->createDeliveryInfoFromOrder($user, $deliveryInfo);
                }

                //creates order and attach customer browser data and payment
                $order = $orderManager->create($deliveryInfo, $cart, $user);
                $orderManager->attachUserBrowserData($order, $request);

                $chooseCurrencyData = $chooseCurrencyForm->getData();

                $payment = $orderManager->createPaymentForOrder(
                    $order,
                    $chooseCurrencyData['currency'],
                    $chooseCurrencyData['znt'] ?? 0
                );

                //attach message to chat between customer and 'seller'(moderator)
                $comment = trim($data['comment']);
                if (!empty($comment)) {
                    $message = new OrderMessage();
                    $message
                        ->setFrom($user)
                        ->setFromDisplayName($user->getUsername())
                        ->setMessage($comment);
                    $order->addMessage($message);
                    $this->flushEntities([$order, $message]);

                    $customMailer->sendNotifyToModerators($order);
                }

                $referralManager->registerPurchase($order);

                $entityManager->commit();
                //release cart and prepared order
                $cart->removeAll();
                $cart->flush();
                $orderManager->releasePreparedOrder();

                //if created only one payment with znt, then move it to process and redirect to status page
                if ($payment->getCurrency() === CurrencyRate::ZBC) {
                    $transactionManager->releaseTransaction($payment->getExternalTransactionId());
                    return $this->redirectToRoute('shop.order.status', ['hash' => $order->getHash()]);
                }

                return $this->redirectToRoute('shop.payment.index', ['hash' => $order->getHash()]);
            } catch (\Exception $e) {
                $entityManager->rollback();
                throw $e;
                //todo need log here
            }
        }

        return $this->render('@Shop/order/checkout/index.twig', [
            'form' => $form->createView(),
            'items' => $cart->get(),
            'total' => $cart->getTotalPrice(),
            'currencyForm' => $chooseCurrencyForm->createView()
        ]);
    }

    public function skipUnavailableAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            throw new BadRequestHttpException('This request allow only via POST');
        }
    }

    /**
     * @param OrderManager $orderManager
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cancelAction(OrderManager $orderManager, $hash)
    {
        $order = $this->getOrderRepository()->findOneByHash($hash);
        if (!$order) {
            throw new NotFoundHttpException();
        }

        if ($order->getUser()->isEnabled() &&
            $order->getUser()->getId() !== $this->getUser()->getId()
        ) {
            throw new AccessDeniedHttpException();
        }

        $orderManager->cancelByUser($order);

        return $this->render('@Shop/order/cancel.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function expiredAction()
    {
        return $this->render('@Shop/order/expired.twig');
    }

    /**
     * @todo подобный метод в AccountController
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction(OrderAccessManager $orderAccessManager, string $hash)
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('shop.account.orders.detail', ['hash' => $hash]);
        }

        $order = $this->getOrderRepository()->findOneByHash($hash);
        if (!$order) {
            throw new NotFoundHttpException();
        }

        if (!$orderAccessManager->canView($order)) {
            throw new AccessDeniedHttpException();
        }

        return $this->render('@Shop/order/detail.twig', ['order' => $order]);
    }

    /**
     * @param EntityManagerInterface $entityManager     *
     * @param string $hash
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function chatAction(EntityManagerInterface $entityManager, string $hash)
    {
        $order = $entityManager->getRepository(Order::class)->findOneByHash($hash);
        if (!$order) {
            throw new NotFoundHttpException();
        }

        $messages = $order->getMessages()->toArray();
        usort($messages, function (BaseMessage $message, BaseMessage $message2) {
            return $message->getCreatedAt() <> $message2->getCreatedAt();
        });

        $form = $this->createForm(MessageFormType::class, new OrderMessage());

        return $this->render('@Shop/order/chat.twig', [
            'messages' => $messages,
            'order' => $order,
            'form' => $form->createView(),
            'isAllowChat' => !in_array($order->getCurrentStatus()->getType(), [
                OrderStatus::CANCEL,
                OrderStatus::DONE
            ])
        ]);
    }

    /**
     * @return OrderRepository
     */
    private function getOrderRepository()
    {
        return $this->getEm()->getRepository(Order::class);
    }
}