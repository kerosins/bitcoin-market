<?php
/**
 * @author Kerosin
 */

namespace Shop\Controller;

use Kerosin\Controller\BaseController;
use Payment\Provider\CurrencyProvider;
use Shop\Entity\Country;
use Symfony\Component\HttpFoundation\Request;
use User\Service\UserCountry;

class SystemController extends BaseController
{
    /**
     * Change a currency action
     *
     * @param Request $request
     * @param CurrencyProvider $currencyProvider
     * @param $code
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeCurrencyAction(Request $request, CurrencyProvider $currencyProvider, $code)
    {
        $currencyProvider->setCurrentCurrency($code);
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Change a country action
     *
     * @param Country $selectedCountry
     */
    public function changeCountryAction(Country $selectedCountry)
    {
        $this->checkIfXHR();
        $userCountry = $this->container->get(UserCountry::class);
        $userCountry->set($selectedCountry);

        return $this->json([]);
    }
}