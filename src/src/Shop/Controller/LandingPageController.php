<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.11.2018
 * Time: 19:50
 */

namespace Shop\Controller;


use Kerosin\Controller\BaseController;

class LandingPageController extends BaseController
{
    public function sellerLandingAction()
    {
        return $this->render('@Shop/landing/seller-landing.twig');
    }
}