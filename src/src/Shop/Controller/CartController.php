<?php
/**
 * @author Kerosin
 */

namespace Shop\Controller;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;
use Catalog\Import\Service\ProductAvailableProvider;
use Kerosin\Controller\BaseController;
use Catalog\Order\Cart\Item;
use Catalog\ORM\Entity\Product;
use Shop\Exception\OnlyAjaxRequestException;
use Catalog\Order\Cart\ProductCart;
use Shop\Widget\ProductCartWidget;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Twig\Environment;

/**
 * Class CartController
 * @package Shop\Controller
 *
 * todo запросы к корзине переделать на RequestFilter
 */
class CartController extends BaseController
{
    /**
     * Add into Cart
     *
     * @param ObjectManager $em
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function addAction(
        ObjectManager $em,
        Request $request,
        ProductAvailableProvider $availableProvider
    ) {
        if (!$request->isXmlHttpRequest()) {
            throw new OnlyAjaxRequestException();
        }

        $id = $request->request->get('id');
        $product = $em->find(Product::class, $id);
        if (!$product) {
            throw new NotFoundHttpException('Product not found');
        }

        $isset = $availableProvider->checkProduct($product);
        if (!$isset) {
            return $this->jsonException(
                new HttpException(410, 'Product was gone'),
                [
                    'url' => $this->generateUrl('shop.catalog.unavailable', ['p' => $product->getId()])
                ]
            );
        }

        $cart = $this->container->get(\Catalog\Order\Cart\ProductCart::class);
        $success = $cart->add($product, 1);

        if (!$success) {
            return $this->json(['success' => false]);
        }

        $cart->flush();

        return $this->renderCartChangeResponse($cart, $id, false);
    }

    /**
     * Remove from cart
     *
     * @param ObjectManager $em
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function removeAction(ObjectManager $em, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new OnlyAjaxRequestException();
        }

        $id = $request->request->get('id');
        $quantity = $request->request->get('quantity');
        $product = $em->getRepository(Product::class)->find($id);
        if (!$product) {
            throw new NotFoundHttpException('Product not found');
        }

        $cart = $this->container->get(\Catalog\Order\Cart\ProductCart::class);
        $success = $cart->remove($product, $quantity);

        if (!$success) {
            return $this->json(['success' => false]);
        }

        $cart->flush();

        return $this->renderCartChangeResponse($cart, $id, true);
    }

    /**
     * @param ProductCart $cart
     * @param int $currentProductId
     * @param bool $isAdd
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function renderCartChangeResponse(ProductCart $cart, int $currentProductId, $isAdd = true)
    {
        $items = $cart->get();
        $twig = $this->container->get('twig');
        $preview = $twig->render('@Shop/widgets/cart/items_preview.twig', [
            'items' => $items,
            'count' => $items->count(),
            'totalPrice' => $cart->getTotalPrice()
        ]);

        return $this->json([
            'success' => true,
            'count' => $items->count(),
            'preview' => $preview,
            'button' => $twig->render('@Shop/widgets/cart/add_button.twig', [
                'isAdd' => $isAdd,
                'id' => $currentProductId
            ])
        ]);
    }

    /**
     * @param ProductCartWidget $cartWidget
     * @param ProductCart $cart
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(ProductCartWidget $cartWidget, ProductCart $cart, ProductAvailableProvider $availableProvider)
    {
        $cartWidget->setIsShow(false);

        $unavailableProducts = $availableProvider->checkCart($cart);
        if ($unavailableProducts) {
            $ids = array_map(function (Product $product) {
                return $product->getId();
            }, $unavailableProducts);

            return $this->redirectToRoute('shop.catalog.unavailable', [
                'p' => implode(',', $ids)
            ]);
        }

        $items = $cart->get();
        return $this->render('@Shop/cart/index.twig', [
            'items' => $this->applySortToItems($items),
            'totalPrice' => $cart->getTotalPrice()
        ]);
    }

    /**
     * Update a cart
     *
     * @param Request $request
     * @param \Catalog\Order\Cart\ProductCart $cart
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function updateAction(Request $request, ProductCart $cart, Environment $twig)
    {
        if ($this->checkIfXHR(false) && !$request->isMethod('post')) {
            throw new BadRequestHttpException();
        }

        $productId = $request->get('product');
        $qty = $request->get('quantity');

        $cart->updateQuantity($productId, $qty);
        $cart->flush();

        $cartRender = $twig->render('@Shop/cart/items.part.twig', [
            'items' => $this->applySortToItems($cart->get())
        ]);
        return $this->json([
            'cart' => $cartRender,
            'totalPrice' => $twig->render('@Shop/cart/total_price.part.twig', ['totalPrice' => $cart->getTotalPrice()])
        ]);
    }

    /**
     * sort items by total price
     *
     * @param Collection $collection
     * @return array
     */
    public function applySortToItems(Collection $collection)
    {
        $items = $collection->toArray();
        usort($items, function (Item $a, Item $b) {
            return $a->getTotalPrice() < $b->getTotalPrice();
        });

        return $items;
    }
}