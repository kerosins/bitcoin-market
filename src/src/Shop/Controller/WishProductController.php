<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Controller;


use Doctrine\Common\Persistence\ObjectManager;
use Kerosin\Controller\BaseController;
use Knp\Component\Pager\PaginatorInterface;
use Catalog\ORM\Entity\Product;
use Content\Navigation\Service\Breadcrumbs;
use Symfony\Component\HttpFoundation\Request;
use User\Service\WishProductManager;

class WishProductController extends BaseController
{
    public function indexAction(
        WishProductManager $wishProductManager,
        PaginatorInterface $paginator,
        Request $request,
        Breadcrumbs $breadcrumbs
    ) {
        $this->addBreadCrumbsFromArray([
            ['My Account', $this->generateUrl('shop.account.index')],
            ['Wishlist', $this->generateUrl('shop.account.wish.index')]
        ]);

        $list = $wishProductManager->getList();
        $list = $paginator->paginate($list, $request->get('page', 1));

        if ($this->checkIfXHR(false)) {
            return $this->json([
                'list' => $this->renderView('@Shop/account/wish/list.part.twig', [
                    'list' => $list
                ])
            ]);
        }

        return $this->render('@Shop/account/wish/index.twig', [
            'list' => $list
        ]);

    }

    public function addAction(WishProductManager $wishProductManager, Product $product)
    {
        $isAdded = $wishProductManager->add($product);

        if (!$this->checkIfXHR(false)) {
            $category = $product->getCategories()->first();

            return $this->redirectToRoute('shop.catalog.category', ['category' => $category->getId()]);
        }

        return $this->json([
            'button' => $this->renderView('@Shop/widgets/wish/button.twig', [
                'isAdd' => !$isAdded,
                'id' => $product->getId()
            ])
        ]);
    }

    public function dropAction(WishProductManager $wishProductManager, $product)
    {
        $wishProductManager->drop($product);
        return $this->json([
            'button' => $this->renderView('@Shop/widgets/wish/button.twig', [
                'isAdd' => true,
                'id' => $product
            ])
        ]);
    }

    public function dropAll()
    {

    }
}