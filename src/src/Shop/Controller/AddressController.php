<?php
/**
 * @author Kerosin
 */

namespace Shop\Controller;

use Doctrine\ORM\EntityManager;
use Kerosin\Controller\BaseController;
use Shop\Entity\AddressObject;
use Shop\Entity\ZipCode;
use Shop\Form\AddressType;
use Shop\Provider\AddressProvider;
use Shop\Provider\ZipProvider;
use Shop\Repository\AddressObjectRepository;
use Symfony\Component\HttpFoundation\Request;

class AddressController extends BaseController
{
    /**
     * @param AddressProvider $provider
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function autocompleteAction(AddressProvider $provider)
    {
        $this->checkIfXHR();

        $form = $this->createForm(AddressType::class);
        $form->handleRequest($this->getRequest());

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $provider
                ->setQ($formData['q'])
                ->setType($formData['type'])
                ->setParent($formData['parent'])
                ->setCountry($formData['country'])
            ;

            $data = $provider->search();
            $response = [];

            //prepare titles and structure
            foreach ($data as $item) {
                /** @var AddressObject $item */
                $title = $item->getTitle();

                if (AddressObject::STATE_TYPE == $formData['type']) {
                    $title .= " ({$item->getAltTitle()})";
                } else {
                    $title .= " ({$item->getParent()->getTitle()})";
                }

                $response[] = [
                    'id' => $item->getId(),
                    'text' => $title
                ];
            }

            return $this->json([
                'result' => $response,
                'total' => $data->getTotalItemCount(),
                'perPage' => $data->getItemNumberPerPage()
            ]);
        }

        $response = [];
        foreach ($form->getErrors(true) as $error) {
            $response[$error->getOrigin()->getName()] = $error->getMessage();
        }

        return $this->json($response, 400);
    }

    public function zipAction(ZipProvider $provider)
    {
        $this->checkIfXHR();
        $request = $this->getRequest();

        $provider
            ->setCity($request->get('city'))
            ->setQuery($request->get('q'))
            ->setCountry($request->get('country'))
        ;

        $provider->setResultTransformer(function (ZipCode $zipCode) {
            $county = $zipCode->getAddressObject()->getParent();
            $city = $zipCode->getAddressObject();

            return [
                'id' => $zipCode->getId(),
                'text' => $zipCode->getCode(),
                'city_id' => $city->getId(),
                'state_id' => $county->getParent()->getId(),
                'state_title' => $county->getParent()->getTitle(),
                'city_title' => "{$city->getTitle()} ({$county->getTitle()})",
            ];
        });
        $codes = $provider->search();

        //prepare response
        $response = [
            'result' => $codes->getItems(),
            'total' => $codes->getTotalItemCount(),
            'perPage' => $codes->getItemNumberPerPage()
        ];

        return $this->json($response);
    }
}