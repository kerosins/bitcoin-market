<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Controller;

use Billing\Service\TransactionManager;
use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Controller\BaseController;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderPosition;
use Catalog\ORM\Entity\OrderStatus;
use Catalog\Order\Exception\OrderManagerException;
use Catalog\Order\Service\OrderManager;
use Catalog\Order\Service\TotalAmountCalculator;
use Payment\Component\PaymentEvent;
use Payment\Entity\CurrencyRate;
use Payment\Entity\Payment;
use Payment\Provider\CurrencyProvider;
use Payment\Service\ChooseCurrencyFormCreator;
use Payment\Service\PaymentManager;
use Payment\Service\PaymentTimer;
use Referral\Service\ReferralManager;
use Shop\Entity\Country;
use Shop\Widget\CurrencyWidget;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use User\Service\UserCountry;

class PaymentController extends BaseController
{
    public function indexAction(
        CurrencyWidget $currencyWidget,
        PaymentTimer $paymentTimer,
        EventDispatcherInterface $eventDispatcher,
        $hash
    ) {
        $currencyWidget->setDisplayCurrencySelector(false);
        $order = $this->getEm()->getRepository(Order::class)->findOneByHash($hash);

        if (!$order) {
            throw new NotFoundHttpException('Order not found');
        }

        if ($order->getCurrentStatus()->getType() !== OrderStatus::PAYMENT_WAIT
            && $order->getDelivery() === null
        ) {
            throw new BadRequestHttpException('Order was canceled or already paid');
        }

        $realPayments = $order->getRealPayments();
        $paymentsCount = $realPayments->count();

        if ($paymentsCount == 0) {
            return $this->forward(
                self::class . '::withoutPaymentAction',
                ['order' => $order]);
        }

        //refresh payment if that expired
        $payment = $realPayments->first();
        if (!$paymentTimer->isActual($payment)) {
            $eventDispatcher->dispatch(
                PaymentEvent::EVENT_NAME_EXPIRE,
                new PaymentEvent($payment)
            );
            return $this->redirectToRoute(
                'shop.payment.index',
                [ 'hash' => $hash ]
            );
        }

        return $this->forward(
            self::class. '::withPaymentAction',
            ['order' => $order]
        );
    }

    /**
     * Cancel payment and return to select currency
     *
     * @param PaymentManager $paymentManager
     * @param TransactionManager $transactionManager
     * @param $hash
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws OrderManagerException
     */
    public function cancelPaymentAction(
        PaymentManager $paymentManager,
        TransactionManager $transactionManager,
        $hash
    ) {
        $em = $this->getEm();
        $order = $em->getRepository(Order::class)->findOneByHash($hash);
        if (!$order) {
            throw new NotFoundHttpException('Order not found');
        }

        if ($order->getCurrentStatus()->getType() !== OrderStatus::PAYMENT_WAIT) {
            throw new OrderManagerException('Invalid status for order');
        }

        $em->beginTransaction();
        try {

            foreach ($order->getPayment() as $payment) {
                /** @var Payment $payment */
                $paymentManager->cancelPayment($payment);

                if ($payment->getCurrency() === CurrencyRate::ZBC) {
                    $transactionManager->cancelTransaction($payment->getExternalTransactionId());
                }
            }

            $em->commit();
        } catch (\Exception $e) {
            $this->container->get('logger.public')->error(
                $e->getMessage(),
                [
                    'trace' => $e->getTraceAsString()
                ]
            );
            $em->rollback();
            throw new BadRequestHttpException('Cancel of payment failed');
        }

        return $this->redirectToRoute('shop.payment.index', ['hash' => $hash]);
    }

    /**
     * Displays form for select currency
     *
     * @param Order $order
     *
     * @return Response
     */
    public function withoutPaymentAction(
        Request $request,
        OrderManager $orderManager,
        ReferralManager $referralManager,
        TransactionManager $transactionManager,
        Order $order,
        ChooseCurrencyFormCreator $chooseCurrencyFormCreator
    ) {
        $referralManager->registerPurchase($order);

        $chooseForm = $chooseCurrencyFormCreator->create($order);
        $chooseForm->handleRequest($request);

        if ($chooseForm->isSubmitted() && $chooseForm->isValid()) {
            //todo it code uses in OrderController
            $data = $chooseForm->getData();
            $payment = $orderManager->createPaymentForOrder($order, $data['currency'], $data['znt'] ?? 0);

            if ($payment) {
                //if created only one payment with znt, then move it to process and redirect to status page
                if ($payment->getCurrency() === CurrencyRate::ZBC) {
                    $transactionManager->releaseTransaction($payment->getExternalTransactionId());
                    return $this->redirectToRoute('shop.order.status', ['hash' => $order->getHash()]);
                }


                if ($payment) {
                    return $this->redirectToRoute('shop.payment.index', ['hash' => $order->getHash()]);
                }
            } else {
                $chooseForm->addError(new FormError('Something went wrong'));
            }

        }

        return $this->render('@Shop/payment/select_currency.twig', [
            'order' => $order,
            'chooseForm' => $chooseForm->createView()
        ]);
    }

    /**
     * Displays bill and qr
     */
    public function withPaymentAction(
        Order $order,
        PaymentTimer $paymentTimer
    ) {
        if ($order->getCurrentStatus()->getType() !== OrderStatus::PAYMENT_WAIT) {
            throw new \RuntimeException('Invalid status of Order');
        }

        $businessLogger = $this->container->get('monolog.logger.business');
        $targetPayment = $order->getRealPayments();

        if (!$targetPayment->count()) {
            $businessLogger->error('Order without payment');
            throw new BadRequestHttpException();
        }

        $payment = $targetPayment->first();

        return $this->render('@Shop/payment/index.twig', [
            'order' => $order,
            'payment' => $payment,
            'paymentTimeLeft' => $paymentTimer->getTimeLeft($payment)
        ]);
    }

    /**
     * @param PaymentTimer $paymentTimer
     * @param OrderManager $orderManager
     * @param $hash
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function approveAction(PaymentTimer $paymentTimer, OrderManager $orderManager, $hash)
    {
        $entityManager = $this->getEm();
        $order = $entityManager->getRepository(Order::class)->findOneByHash($hash);
        $logger = $this->container->get('monolog.logger.business');

        if (!$order) {
            throw new NotFoundHttpException('Order not found');
        }

        if ($order->getCurrentStatus()->getType() !== OrderStatus::PAYMENT_WAIT) {
            throw new BadRequestHttpException('Invalid Order');
        }

        $entityManager->beginTransaction();

        try {
            $orderManager->updateStatus($order, OrderStatus::PAYMENT_PROCESS);
            $payments = $order->getPayment();
            foreach ($payments as $payment) {
                /** @var Payment $payment */
                $payment->setStatus(Payment::PAYMENT_PROCESS);
                $entityManager->persist($payment);
            }

            $entityManager->flush();
            $entityManager->commit();
        } catch (\Exception $e) {
            $logger->error($e->getMessage(), [
                'trace' => $e->getTraceAsString()
            ]);
            $entityManager->rollback();
            throw new BadRequestHttpException('Payment not approved');
        }

        return $this->redirectToRoute('shop.order.status', ['hash' => $hash]);
    }

    /**
     * Gets a preview total price
     *
     * @param Request $request
     * @param OrderManager $manager
     * @param string $hash
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function totalPreviewAction(
        Request $request,
        CurrencyProvider $currencyProvider,
        ChooseCurrencyFormCreator $formCreator,
        TotalAmountCalculator $calculator,
        UserCountry $userCountry,
        EntityManagerInterface $entityManager,
        string $hash
    ) {
        if (!$request->isXmlHttpRequest() || !$request->isMethod('post')) {
            return $this->json(['message' => 'Bad Request'], 400);
        }

        $order = $this->getEm()->getRepository(Order::class)->findOneByHash($hash);
        if (!$order) {
            return $this->json(['message' => 'Order not found'], 404);
        }

        $form = $formCreator->create($order);
        $form->handleRequest($request);

        $currency = 0;
        $bonus = 0;

        if (!$form->isSubmitted()) {
            return $this->jsonException(new BadRequestHttpException());
        }

        if (!$form->isValid()) {
            $formErrors = $form->getErrors(true);
            $errors = [];

            foreach ($formErrors as $key => $error) {
                /** @var FormError $error */
                $errors[$error->getOrigin()->getName()][] = $error->getMessage();
            }

            return $this->json(['errors' => $errors], 400);
        }

        $data = $form->getData();
        if (isset($data['currency'])) {
            $currency = $data['currency'];
        }

        if (isset($data['znt'])) {
            $bonus = $data['znt'];
        }

        if (!empty($data['country'])) {
            $country = $entityManager->find(Country::class, $data['country']);
        }

        if (empty($country)) {
            $country = $userCountry->getDefaultCountry();
        }


        $order->getOrderProducts()->map(function (OrderPosition $position) use ($calculator, $country) {
            $tax = $calculator->calculatePrice($position->getProduct(), $country);
            $position->setShipmentTax($tax->getDeliveryTax())->setPublicPrice($tax->getSubTotal());
        });

        $total = $order->getTotalPrice();
        try {
            $total->sub($currencyProvider->convertToUsd($bonus, CurrencyRate::ZBC));
        } catch (\Exception $e) {}

        $total = array_map(function ($value) use ($currencyProvider, $currency) {
            return $currencyProvider->convertToBestRate($value, $currency);
        }, $total->toArray());

        return $this->json($total);
    }
}