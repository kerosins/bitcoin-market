<?php
/**
 * Catalog class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Shop\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Catalog\ORM\Entity\Category;
use Elastic\Entity\Category as ElasticCategory;
use Elastic\Repository\ElasticCategoryRepository;
use Kerosin\Controller\BaseController;
use Catalog\ORM\Entity\Product;
use Content\ORM\Entity\Slug;
use Content\SlugRoute\Service\SlugRouter;
use Shop\Entity\Seller;
use Shop\Provider\CategoryProvider;
use Shop\Provider\ProductProvider;
use Shop\Provider\SuggestionsProvider;
use Shop\Service\ActiveSeller;
use Content\Seo\Service\SeoDataCollector;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CatalogController extends BaseController
{
    /**
     * Page of category
     *
     * @param ObjectManager $em
     * @param Request $request
     * @param CategoryProvider
     * @param $category
     * @return Response
     */
    public function categoryAction(
        EntityManagerInterface $em,
        Request $request,
        ElasticCategoryRepository $elasticCategoryRepository,
        SlugRouter $routeGenerator,
        ActiveSeller $activeSeller,
        ProductProvider $provider,
        $category
    ) {
        $elasticCategory = $elasticCategoryRepository->findById($category);
        $parents = $elasticCategory->getParents();
        $seller = $activeSeller->get();

        if ($seller) {
            $provider->setSeller($seller->getId());
            $this->addBreadCrumb($seller->getName(), $this->generateUrl('shop.catalog.seller', ['seller' => $seller->getId()]));
        }

        foreach ($parents as $item) {
            /** @var ElasticCategory $item */
            $this->addBreadCrumb(
                $item->getTitle(),
                $routeGenerator->generate(Slug::TARGET_CATEGORY, $item->getId())
            );
        }
        $this->addBreadCrumb(
            $elasticCategory->getTitle(),
            $routeGenerator->generate(Slug::TARGET_CATEGORY, $elasticCategory->getId())
        );

        //init filters
        $filters = $this->container->get(\Shop\Service\ProductFilter::class);
        $filters->setCategory($category);
        $filters->buildFormView();

        //configures provider
        $category = $em->find(Category::class, $category);
        $searchCategories = (array)$category->getId();
        $children = $elasticCategory->getChildren()->map(function (ElasticCategory $category) {
            return $category->getId();
        })->toArray();
        $searchCategories = array_merge($searchCategories, $children);
        $provider->setCategories($searchCategories);

        if (!empty($request->get('q'))) {
            $provider->setTitle($request->get('q'));
        }

        $provider
            ->setParameters($filters->getFilterData())
            ->setPerPage($request->get('per-page', 24));

        //applies sort
        $provider->setSort($request->get('sorting', null));
        $provider->setDirection($request->get('direct', 'asc'));

        //seo
        $this->container->get(SeoDataCollector::class)->setProvider($category);

        return $this->render('@Shop/catalog/products_container.twig', [
            'products' => $provider->search(),
            'sorting' => $provider->getSortingParams()
        ]);
    }

    public function sellerAction(
        ProductProvider $provider,
        Request $request,
        Seller $seller
    ) {
        $provider->setSeller($seller->getId());

        $this->addBreadCrumb($seller->getName(), $this->generateUrl('shop.catalog.seller', ['seller' => $seller->getId()]));
        $provider->setPerPage($request->get('per-page', 24));

        //applies sort
        $provider->setSort($request->get('sorting', null));
        $provider->setDirection($request->get('direct', 'asc'));

        return $this->render('@Shop/catalog/products_container.twig', [
            'products' => $provider->search(),
            'sorting' => $provider->getSortingParams()
        ]);
    }

    /**
     * It's a detail page
     *
     * @param EntityManager $em
     * @param $id
     * @return Response
     */
    public function productAction(
        EntityManagerInterface $em,
        SlugRouter $routeGenerator,
        ActiveSeller $activeSeller,
        Product $product
    ) {
        $seller = $activeSeller->get();
        if ($seller) {
            if ($product->getSeller()->getId() !== $seller->getId()) {
                throw new NotFoundHttpException();
            }

            $this->addBreadCrumb($seller->getName(), $this->generateUrl('shop.catalog.seller', ['seller' => $seller->getId()]));
        }

        /** @var \Catalog\ORM\Entity\Category $mainCategory */
        $mainCategory = $product->getCategories()->last();
        $parents = $em->getRepository(Category::class)->findAllParents($mainCategory->getId());
        foreach ($parents as $parent) {
            /** @var \Catalog\ORM\Entity\Category $parent */
            $this->addBreadCrumb($parent->getTitle(), $routeGenerator->generate(Slug::TARGET_CATEGORY, $parent->getId()));
        }

        $this->container->get(SeoDataCollector::class)->setProvider($product);
        return $this->render('@Shop/catalog/detail.twig', [
            'product' => $product,
            'category' => $mainCategory
        ]);
    }

    /**
     * It's a search action
     *
     * @param Request $request
     * @param $query
     * @return Response
     */
    public function searchAction(
        Request $request,
        SessionInterface $session,
        $query
    ) {
        $this->addBreadCrumb('Search: ' . $query, $this->generateUrl('shop.catalog.search', ['query' => $query]));

        $provider = $this->container->get(ProductProvider::class);
        $provider->setTitle($query);

        $provider->setPerPage($request->get('per-page', 24));

        //applies sort
        $provider->setSort($request->get('sorting', null));
        $provider->setDirection($request->get('direct', 'asc'));

        $products = $provider->search();

        if ($request->isXmlHttpRequest()) {
            return $this->render('@Shop/catalog/products_list.twig', [
                'products' => $products
            ]);
        }

        //save search query into log
        $hashedQuery = 'search_' . md5($query);
        $hasSearchQuery = $session->get($hashedQuery, false);
        if (!$hasSearchQuery) {
            $logger = $this->get('monolog.logger.search');
            $headers = [];
            foreach ($request->headers->getIterator() as $name => $value) {
                $headers[$name] = reset($value);
            }
            $logger->info(
                'Search query: ' . $query,
                [
                    'query' => $query,
                    'headers' => $headers
                ]
            );
            $session->set($hashedQuery, true);
        }

        return $this->render('@Shop/catalog/search_products_container.twig', [
            'products' => $products,
            'query' => $query,
            'sorting' => $provider->getSortingParams(),
            'foundCategories' => $provider->getFoundCategories()
        ]);
    }

    /**
     * Action for autocomplete search
     * @todo reformat response if need
     *
     * @param Request $request
     * @param $query - query term
     * @return JsonResponse
     */
    public function searchAutoCompleteAction(Request $request, SuggestionsProvider $suggestionsProvider)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new HttpException(400, 'This request must be via Ajax');
        }

        $query = $request->get('term');
        $suggestionsProvider->setQuery($query);

        $result = $suggestionsProvider->search();
        return $this->json([
            'suggestions' => $result
        ]);
    }

    public function unavailableAction(Request $request, EntityManagerInterface $entityManager)
    {
        $this->addBreadCrumb('Products are unavailable now', '');

        $maxCountOfProducts = 15;
        $ids = $request->get('p');
        $ids = explode(',', $ids);
        $ids = array_map(function ($id) {
            return (int)trim($id);
        }, $ids);

        if (count($ids) > $maxCountOfProducts) {
            $ids = array_slice($ids, 0, $maxCountOfProducts);
        }

        $products = $entityManager->getRepository(Product::class)->findUnavailableProducts($ids);

        return $this->render('@Shop/catalog/unavailable.twig', [
            'products' => $products
        ]);
    }
}