<?php
/**
 * @author Kondaurov
 */

namespace Shop\Controller;

use Billing\Entity\Transaction;
use Billing\Service\AccountManager;
use Billing\Service\TransactionManager;
use Cp\Provider\BillingAccountProvider;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Kerosin\Component\ArrayHelper;
use Kerosin\Controller\BaseController;
use Knp\Component\Pager\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Payment\Component\Currency;
use Payment\Entity\CurrencyRate;
use Payment\Provider\CurrencyProvider;
use Payment\Service\CurrencyConverter;
use Referral\Entity\ReferralProgram;
use Referral\Entity\ReferralPurchase;
use Referral\Entity\ReferralWithdrawal;
use Referral\Service\LinkGenerator;
use Referral\Service\ReferralCreator;
use Referral\Service\ReferralManager;
use Referral\Service\ReferralSessionManager;
use Referral\Service\WithdrawalManager;
use Shop\Form\WithdrawalRequestType;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use User\Entity\BalanceChangeLog;
use User\Entity\User;
use User\Event\BalanceEvent;

class ReferralController extends BaseController
{
    /**
     * Display code and purchases
     *
     * @param ReferralCreator $referralCreator
     * @param EntityManager $entityManager
     * @param Paginator $paginator
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(
        ReferralCreator $referralCreator,
        EntityManagerInterface $entityManager,
        PaginatorInterface $paginator,
        Request $request,
        AccountManager $accountManager,
        LinkGenerator $linkGenerator,
        CurrencyProvider $currencyProvider,
        CurrencyConverter $converter
    ) {
        $this->addBreadCrumbsFromArray([
            ['My Account', $this->generateUrl('shop.account.index')],
            ['Referral Program', $this->generateUrl('shop.referrals.index')]
        ]);

        $user = $this->getUser();
        $referralProgram = $entityManager->getRepository(ReferralProgram::class)->findOneBy(['user' => $user]);

        if (!$referralProgram) {
            $referralProgram = $referralCreator->attachReferralProgram($user, true);
        }

        $historyQuery = $entityManager->getRepository(BalanceChangeLog::class)
            ->createByUserQuery($user);

        //fixme lost _fragment
        $historyItems = $paginator->paginate(
            $historyQuery,
            $request->get('ph', 1),
            20,
            [
                PaginatorInterface::PAGE_PARAMETER_NAME => 'ph'
            ]
        );

        if (($account = $user->getBillingAccount()) === null) {
            $account = $accountManager->create();
            $user->setBillingAccount($account);

            $entityManager->persist($user);
            $entityManager->flush();
        }
        $balance = $accountManager->balance($account);

        $zbcAmounts = [];
        if ($balance) {
            $zbcAmounts = array_map(function (Currency $currency) use ($balance, $converter) {
                return [
                    'name' => $currency->getName(),
                    'amount' => $converter->convert($balance, CurrencyRate::ZBC, $currency)
                ];

            }, $currencyProvider->getAllowedCurrenciesList()->toArray());
        }

        return $this->render('@Shop/account/referral/index.twig', [
            'program' => $referralProgram,
            'historyItems' => $historyItems,
            'balance' => $balance,
            'zbcAmounts' => $zbcAmounts,
            'mainDeepLink' => $linkGenerator->generate('/', $referralProgram->getCode())
        ]);
    }

    /**
     * @param Request $request
     * @param CurrencyProvider $currencyProvider
     * @param WithdrawalManager $withdrawalManager
     * @param AccountManager $accountManager
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function withdrawRequestAction(
        Request $request,
        CurrencyProvider $currencyProvider,
        WithdrawalManager $withdrawalManager,
        AccountManager $accountManager
    ) {
        $withdrawal = new ReferralWithdrawal();
        $user = $this->getUser();
        $form = $this->createForm(
            WithdrawalRequestType::class,
            $withdrawal,
            [
                'account' => $user->getBillingAccount()
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $withdrawal->setUser($user);
            $withdrawal->setRate(
                $currencyProvider->getBestForSale($withdrawal->getCurrency())
            );

            $isSuccess = $withdrawalManager->askWithdrawal($withdrawal);

            if (!$isSuccess) {
                $error = new FormError('Internal error, you can try later');
                $form->addError($error);
            } else {
                return $this->json([
                    'success' => true,
                    'redirectTo' => $this->generateUrl('shop.referrals.index', [
                        '_fragment' => 'history'
                    ])
                ]);
            }
        }

        $balance = $accountManager->balance($user->getBillingAccount());

        return $this->json([
            'modal' => $this->renderView('@Shop/account/referral/withdraw/withdraw_modal.part.twig', [
                'balance' => $balance,
                'form' => $form->createView()
            ])
        ]);
    }

    /**
     * todo rewrite
     *
     * @param EntityManagerInterface $entityManager
     * @param string $operationCode
     */
    public function cancelWithdrawalAction(
        TransactionManager $manager,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        string $operationCode
    ) {
        $withdrawRepo = $entityManager->getRepository(ReferralWithdrawal::class);

        $withdraw = $withdrawRepo->findOneByCode($operationCode);
        if (!$withdraw
            || !$withdraw->canBeCanceledByUser()
            || $withdraw->getUser()->getId() !== $this->getUser()->getId()
        ) {
            throw new AccessDeniedHttpException("Cannot allow cancel this operation {$operationCode}");
        }

        $entityManager->beginTransaction();
        try {
            $transaction = $manager->getTransaction($withdraw->getTransaction());
            if (!$transaction) {
                throw new NotFoundHttpException("Operation {$withdraw->getTransaction()} not found");
            }
            $manager->cancelTransaction($transaction);
            $withdraw->setStatus(ReferralWithdrawal::CANCELED);

            $entityManager->persist($withdraw);
            $entityManager->flush();

            $balanceEvent = new BalanceEvent(
                $transaction,
                $this->getUser(),
                BalanceChangeLog::TYPE_WITHDRAWAL,
                BalanceChangeLog::STATUS_WITHDRAWAL_CANCEL,
                'Withdrawal cancelled by user'
            );
            $eventDispatcher->dispatch(BalanceEvent::NAME, $balanceEvent);
            $entityManager->commit();
        } catch (\Exception $e) {
            $entityManager->rollback();
            throw $e;
        }

        return $this->redirectToRoute('shop.referrals.index', [
            '_fragment' => 'history'
        ]);
    }

    /**
     * @param Request $request
     * @param LinkGenerator $generator
     * @param ReferralManager $referralManager
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function generateLinksAction(
        Request $request,
        LinkGenerator $generator,
        ReferralManager $referralManager
    ) {
        if (!$request->isXmlHttpRequest() && !$request->isMethod('post')) {
            return $this->json(['message' => 'Request must be via POST and XHR'], 400);
        }

        $links = $request->get('links');
        if (!$links) {
            return $this->json(['message' => 'Parameter "links" is required'], 400);
        }

        $code = $referralManager->getCurrentUserReferralCode();
        if (!$code) {
            return $this->json(['message' => 'Referral Program is empty'], 400);
        }

        $links = explode(PHP_EOL, $links);
        $links = array_map('trim', $links);//fixme
        $links = ArrayHelper::arrayClean($links);
        $links = array_map(function ($path) use ($generator, $code) {
            return $generator->generate($path, $code);
        }, $links);

        return $this->json(['links' => $links]);
    }

    /**
     * @param EntityManager $entityManager
     * @param ReferralManager $manager
     * @param $code
     * @param $path
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectAction(
        ObjectManager $entityManager,
        ReferralSessionManager $referralSessionManager,
        Request $request,
        $code,
        $path
    ) {
        if ('/' !== $path) {
            $path = '/' . $path;
        }
        $response = $this->redirect($path, 301);
        $program = $entityManager
            ->getRepository(ReferralProgram::class)
            ->findOneBy(['code' => $code]);
        if (!$program) {
            throw new NotFoundHttpException('Unknown referral program');
        }

        $user = $this->getUser();
        $referralSessionManager->open($program, $response, $user instanceof User ? $user : null);

        return $response;
    }
}