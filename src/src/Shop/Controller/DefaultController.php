<?php

namespace Shop\Controller;

use Background\WebSocket\Message;
use Billing\Service\AccountManager;
use Catalog\Import\Component\ProductUpdateEvent;
use Catalog\ORM\Entity\Product;
use Kerosin\Controller\BaseController;
use Payment\Entity\CurrencyRate;
use Payment\Service\ExchangeClients;
use Payment\Service\QrGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Shop\Entity\Newsletter;
use Shop\Form\SubscribeNewsletterFormType;
use Shop\Provider\SuggestionsProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class DefaultController extends BaseController
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        // replace this example code with whatever you need
        return $this->render('@Shop/home/home.twig', [
            'user' => $this->getUser(),
        ]);
    }

    /**
     * Handlers subscription
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function subscribeAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(SubscribeNewsletterFormType::class, [], [
            'user' => $user
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                return $this->json(
                    [
                        'errors' => $this->convertFormErrorsToArray($form)
                    ],
                    400
                );
            }

            $email = $form->getData()['email'];
            $subscription = new Newsletter();
            $subscription->setEmail($email)->setUser($user);

            $this->flushEntities($subscription);
        } else {
            $this->jsonException(new BadRequestHttpException());
        }

        return $this->json('success', 201);
    }

    public function ajaxErrorAction($code)
    {
        if (!in_array($code, [400, 500])) {
            $code = 400;
        }

        return $this->json(['key' => 'some content'], $code);
    }
}