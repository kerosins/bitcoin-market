<?php
/**
 * @author kerosin
 */

namespace Shop\Controller;

use Catalog\Order\Service\TotalAmountCalculator;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Controller\ResettingController;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Kerosin\Controller\BaseController;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Catalog\ORM\Entity\Order;
use Catalog\Order\Service\OrderAccessManager;
use Catalog\Order\Service\OrderManager;
use Catalog\Order\Service\OrderProvider;
use Catalog\ORM\Entity\OrderStatus;
use Shop\Entity\ZipCode;
use Content\Navigation\Service\Breadcrumbs;
use Symfony\Component\ExpressionLanguage\Tests\Node\Obj;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use User\Entity\UserDeliveryInfo;
use User\Form\UserDeliveryInfoType;
use User\Service\UserCountry;

class AccountController extends BaseController
{
    /**
     * Displays a dashboard
     *
     * @param ObjectManager $entityManager
     * @param OrderProvider $orderProvider
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(
        ObjectManager $entityManager,
        \Catalog\Order\Service\OrderProvider $orderProvider
    ) {
        $this->addBreadCrumb('My Account', $this->generateUrl('shop.account.index'));

        $infoList = $entityManager
            ->getRepository(UserDeliveryInfo::class)
            ->buildFindByUserQuery($this->getUser())
            ->setMaxResults(2)
            ->getResult()
        ;

        $orderProvider
            ->setClient($this->getUser())
            ->setStatus(OrderProvider::$visibleStatuses)
            ->setPerPage(3)
            ->setSortColumn('paymentDate');

        $userInfo = $this->getUser();

        return $this->render('@Shop/account/dashboard/index.twig', [
            'infoList' => $infoList,
            'orders' => $orderProvider->search(),
            'userInfo' => $userInfo
        ]);
    }

    /**
     * Displays list of addresses
     *
     * @param ObjectManager $entityManager
     * @param PaginatorInterface $paginator
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addressAction(
        ObjectManager $entityManager,
        PaginatorInterface $paginator,
        Request $request
    ) {
        $this->addBreadCrumbsFromArray([
            ['My Account', $this->generateUrl('shop.account.index')],
            ['Address Book', $this->generateUrl('shop.account.address_book.index')]
        ]);

        $addressesQuery = $entityManager
            ->getRepository(UserDeliveryInfo::class)
            ->buildFindByUserQuery($this->getUser())
        ;
        $addresses = $paginator->paginate($addressesQuery, $request->get('page', 1));

        return $this->render('@Shop/account/address/index.twig', [
            'addresses' => $addresses
        ]);
    }

    /**
     * Creates or updates an address
     *
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param Request $request
     * @param null $deliveryInfo - id parameter in request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createUpdateInfoAction(
        ObjectManager $entityManager,
        Request $request,
        UserCountry $userCountry,
        UserDeliveryInfo $deliveryInfo = null
    ) {
        if (!$deliveryInfo) {
            $entity = new UserDeliveryInfo();
            $entity->setCountry($userCountry->get());
        } else {
            $entity = $deliveryInfo;
        }

        $isCreate = null === $deliveryInfo;

        $this->addBreadCrumbsFromArray([
            ['My Account', $this->generateUrl('shop.account.index')],
            ['Address Book', $this->generateUrl('shop.account.address_book.index')],
            [($isCreate ? 'Create' : 'Edit'), '']
        ]);

        $form = $this->createForm(UserDeliveryInfoType::class, $entity, [
            'state_autocomplete_route' => 'shop.address.autocomlete',
            'city_autocomplete_route' => 'shop.address.autocomlete',
            'zip_autocomplete_route' => 'shop.address.zip'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity->setUser($this->getUser());

            if ($entity->getZip() instanceof ZipCode) {
                $entity->setZip($entity->getZip()->getZip());
            }

            $entityManager->persist($entity);
            $entityManager->flush($entity);

            return $this->redirectToRoute('shop.account.address_book.index');
        }

        return $this->render('@Shop/account/address/form.twig', [
            'isCreate' => $isCreate,
            'form' => $form->createView()
        ]);
    }

    /**
     * Removes an address
     *
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeInfoAction(ObjectManager $entityManager, $id)
    {
        $entity = $this->getDeliveryInfo($id);
        if (!$entity) {
            throw new NotFoundHttpException();
        }

        $entityManager->remove($entity);
        $entityManager->flush();

        return $this->redirectToRoute('shop.account.address_book.index');
    }

    /**
     * Displays list of orders
     *
     * @param OrderProvider $provider
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function orderListAction(OrderProvider $provider)
    {
        $this->addBreadCrumbsFromArray([
            ['My Account', $this->generateUrl('shop.account.index')],
            ['Orders', $this->generateUrl('shop.account.orders.index')]
        ]);

        $provider->setClient($this->getUser());
        $provider->setStatus(OrderProvider::$visibleStatuses);

        $orders = $provider->search();

        return $this->render('@Shop/account/orders/index.twig', [
            'orders' => $orders
        ]);
    }

    /**
     * @todo подобный метод в OrderController
     *
     * @param EntityManagerInterface $entityManager
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function orderDetailAction(
        EntityManagerInterface $entityManager,
        OrderAccessManager $orderAccessManager,
        TotalAmountCalculator $amountCalculator,
        string $hash
    ) {
        $order = $entityManager->getRepository(Order::class)->findOneByHash($hash);
        if (!$order) {
            throw new NotFoundHttpException();
        }

        if (!$orderAccessManager->canView($order)) {
            throw new AccessDeniedHttpException();
        }

        $this->addBreadCrumbsFromArray([
            ['My Account', $this->generateUrl('shop.account.index')],
            ['Orders', $this->generateUrl('shop.account.orders.index')],
            ['Order ' . $order->getCode(), $this->generateUrl('shop.account.orders.detail', ['hash' => $hash])]
        ]);

        return $this->render('@Shop/account/orders/detail.twig', [
            'order' => $order,
            'totalPrice' => $amountCalculator->calculatePriceByOrder($order)
        ]);
    }

    /**
     * Accepts order
     *
     * @param OrderManager $orderManager
     * @param EntityManagerInterface $entityManager
     * @param $hash
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function acceptOrderAction(OrderManager $orderManager, EntityManagerInterface $entityManager, $hash)
    {
        $order = $entityManager->getRepository(Order::class)->findOneByHash($hash);
        if (!$order) {
            throw new NotFoundHttpException('Order not found');
        }

        $isAccepted = $orderManager->accept($order, $this->getUser());
        if (!$isAccepted) {
            throw new BadRequestHttpException('Order could not accepted.');
        }

        return $this->redirectToRoute('shop.account.orders.index');
    }

    /**
     * sends request for reset password
     *
     * @param TokenGeneratorInterface $tokenGenerator
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resetPasswordAction(TokenGeneratorInterface $tokenGenerator)
    {
        $this->checkIfXHR();

        $mailer = $this->get('fos_user.mailer.public');
        $manager = $this->get('fos_user.user_manager');
        $user = $this->getUser();

        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $mailer->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $manager->updateUser($user);

        return $this->render('@Shop/account/success_send_resetting_email.part.twig');
    }

    /**
     * Gets an address
     *
     * @param $id
     * @return null|UserDeliveryInfo
     */
    private function getDeliveryInfo($id)
    {
        return $this->getEm()->getRepository('UserBundle:UserDeliveryInfo')->find($id);
    }
}