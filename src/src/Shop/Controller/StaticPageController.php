<?php
/**
 * Created by PhpStorm.
 * User: kerosin
 * Date: 07.04.2018
 * Time: 3:17
 */

namespace Shop\Controller;


use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Controller\BaseController;
use Content\ORM\Entity\Slug;
use Content\ORM\Entity\StaticPage;
use Content\Seo\Service\SeoDataCollector;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class StaticPageController extends BaseController
{
    public function indexAction(SeoDataCollector $collector, StaticPage $staticPage = null)
    {
        $collector->setProvider($staticPage);
        return $this->render('@Shop/static-page/index.twig', [
            'page' => $staticPage
        ]);
    }

    public function slugAction(EntityManagerInterface $entityManager, SeoDataCollector $collector, string $slug)
    {
        $slugItem = $entityManager->getRepository(Slug::class)->findOneBySlugAndType($slug, Slug::TARGET_STATIC_PAGE);
        if (!$slugItem) {
            throw new NotFoundHttpException();
        }
        $staticPage = $entityManager->find(StaticPage::class, $slugItem->getTargetId());

        $collector->setProvider($staticPage);
        return $this->render('@Shop/static-page/index.twig', [
            'page' => $staticPage
        ]);
    }
}