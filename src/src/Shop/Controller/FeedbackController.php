<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Controller;


use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Controller\BaseController;
use Shop\Entity\Feedback\AffiliateProgram;
use Shop\Entity\Feedback\BaseFeedback;
use Shop\Entity\Feedback\ComplaintAgreements;
use Shop\Entity\Feedback\ComplaintIntellectualRights;
use Shop\Entity\Feedback\Feedback;
use Shop\Entity\Feedback\NotFoundProduct;
use Shop\Entity\Feedback\ProhibitedProducts;
use Shop\Form\FeedbackFormType;
use Symfony\Component\HttpFoundation\Request;
use User\Entity\User;

/**
 * Class FeedbackController
 * @package Shop\Controller
 *
 * Todo move select entity to service
 */
class FeedbackController extends BaseController
{
    public const
        TYPE_FEEDBACK = 'feedback',
        TYPE_NOT_FOUND_PRODUCT = 'notFoundProduct',
        TYPE_AGREEMENT = 'agreement',
        TYPE_INTELLECTUAL_RIGHTS = 'intellectualRights',
        TYPE_AFFILIATE = 'affiliate',
        TYPE_PROHIBITED = 'prohibited';

    private static $entityMap = [
        self::TYPE_FEEDBACK => Feedback::class,
        self::TYPE_NOT_FOUND_PRODUCT => NotFoundProduct::class,
        self::TYPE_AGREEMENT => ComplaintAgreements::class,
        self::TYPE_INTELLECTUAL_RIGHTS => ComplaintIntellectualRights::class,
        self::TYPE_AFFILIATE => AffiliateProgram::class,
        self::TYPE_PROHIBITED => ProhibitedProducts::class
    ];

    public function indexAction(EntityManagerInterface $entityManager, Request $request, string $type)
    {
        $this->checkIfXHR();
        $entity = $this->createFeedbackEntity($type);

        if ($this->getUser() instanceof User) {
            $entity->setEmail($this->getUser()->getEmailCanonical());
        }

        $form = $this->createForm(FeedbackFormType::class, $entity, [
            'action' => $this->generateUrl('shop.feedback', ['type' => $type]),
            'data_class' => self::$entityMap[$type]
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->json([
                'submitted' => true,
                'modal' => $this->renderView('@Shop/feedback/success.twig', [
                    'type' => $type
                ])
            ]);
        }

        return $this->json([
            'submitted' => false,
            'modal' => $this->renderView('@Shop/feedback/form.twig', [
                'form' => $form->createView(),
                'title' => $this->getFormTitle($type)
            ])
        ]);
    }

    private function getFormTitle($type)
    {
        switch ($type) {
            case self::TYPE_FEEDBACK:
                return 'Feedback';
            case self::TYPE_NOT_FOUND_PRODUCT:
                return 'Did not find what you were looking for? Write a letter to us!';
            case self::TYPE_AGREEMENT:
                return 'Submit a report. If you consider that the seller is violating the rules of trade, you can send a report to a moderator.';
            case self::TYPE_INTELLECTUAL_RIGHTS:
                return 'Report intellectual property rights violation';
            case self::TYPE_AFFILIATE:
                return 'Become a seller';
            case self::TYPE_PROHIBITED:
                return 'Report restricted or Prohibited items';
        }

        return '';
    }

    /**
     * @param $type
     * @return BaseFeedback
     */
    private function createFeedbackEntity($type)
    {
        $entityClass = self::$entityMap[$type] ?? '';
        if (!$entityClass) {
            throw new \RuntimeException('Unknown Type "' . $type . '" of Feedback');
        }
        return new $entityClass;
    }
}