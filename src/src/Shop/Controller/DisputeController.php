<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Controller;

use Elasticsearch\Common\Exceptions\Forbidden403Exception;
use Kerosin\Controller\BaseController;
use Kerosin\Filesystem\ImageHelper;
use Kerosin\Filesystem\ImageUploadStack;
use Kerosin\Mailer\CustomMailer;
use Catalog\ORM\Entity\Dispute;
use Catalog\ORM\Entity\Message\BaseMessage;
use Catalog\ORM\Entity\Message\DisputeMessage;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderPosition;
use Catalog\Order\Service\DisputeManager;
use Shop\Form\MessageFormType;
use Symfony\Component\HttpFoundation\Session\Session;

class DisputeController extends BaseController
{
    /**
     * @param DisputeManager $disputeManager
     * @param int|OrderPosition $orderPosition
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(
        DisputeManager $disputeManager,
        string $hash,
        OrderPosition $position
    ) {
//        $orderPosition = $this->getEm()->find(OrderPosition::class, $orderPosition);
        $dispute = $disputeManager->open($position, $this->getUser());
        return $this->redirectToRoute('shop.account.order.dispute.view', [
            'hash' => $dispute->getOrderPosition()->getOrder()->getHash(),
            'position' => $dispute->getOrderPosition()->getId()
        ]);
    }

    /**
     * @param DisputeManager $disputeManager
     * @param string $hash
     * @param OrderPosition $position
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws Forbidden403Exception
     */
    public function viewAction(
        DisputeManager $disputeManager,
        string $hash,
        OrderPosition $position
    ) {
        $dispute = $position->getDisputes()->first();

        if (!$disputeManager->canView($dispute, $this->getUser())) {
            throw new Forbidden403Exception('You can\'t view this dispute');
        }

        $order = $position->getOrder();
        $this->addBreadCrumbsFromArray([
            ['My Account', $this->generateUrl('shop.account.index')],
            ['Orders', $this->generateUrl('shop.account.orders.index')],
            ['Order #' . $order->getCode(), $this->generateUrl('shop.account.orders.detail', ['hash' => $order->getHash()])],
            ['Dispute', $this->generateUrl(
                'shop.account.order.dispute.view',
                [
                    'hash' => $order->getHash(),
                    'position' => $position->getId()
                ]
            )]
        ]);

        $form = null;
        $fileStackId = null;

        $isAllowChat = $disputeManager->isAllowChat($dispute);
        if ($isAllowChat) {
            $msg = new DisputeMessage();
            $form = $this->createForm(MessageFormType::class, $msg);
        }

        $messages = $dispute->getMessages()->toArray();
        usort($messages, function (BaseMessage $message, BaseMessage $message2) {
            return $message->getCreatedAt() <> $message2->getCreatedAt();
        });

        return $this->render('@Shop/account/dispute/view.twig', [
            'dispute' => $dispute,
            'messages' => $messages,
            'isAllowChat' => $isAllowChat,
            'fileStackId' => $fileStackId,
            'form' => $isAllowChat ? $form->createView() : $form
        ]);
    }

    /**
     * Cancels dispute
     *
     * @param DisputeManager $disputeManager
     * @param string $hash
     * @param OrderPosition|int $position
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function cancelAction(
        DisputeManager $disputeManager,
        string $hash,
        OrderPosition $position
    ) {
        /** @var Dispute $dispute */
        $position = $this->getEm()->find(OrderPosition::class, $position);
        $dispute = $position->getDisputes()->first();

        if (!$disputeManager->closeDisputeByUser($dispute, $this->getUser())) {
            throw new \RuntimeException();
        }

        return $this->redirectToRoute('shop.account.order.dispute.view', [
            'hash' => $dispute->getOrderPosition()->getOrder()->getHash(),
            'dispute' => $dispute->getOrderPosition()->getId()
        ]);
    }
}