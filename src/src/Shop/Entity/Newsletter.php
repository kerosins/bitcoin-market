<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use User\Entity\User;

/**
 * Class Newsletter
 * @package Shop\Entity
 *
 * @ORM\Entity()
 * @ORM\Table()
 */
class Newsletter
{
    use BaseMapping;

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(name="user_id", nullable=true)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $email;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Newsletter
     */
    public function setUser(User $user = null): Newsletter
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Newsletter
     */
    public function setEmail(string $email): Newsletter
    {
        $this->email = $email;
        return $this;
    }
}