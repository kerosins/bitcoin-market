<?php
/**
 * @author Kondaurov
 */

namespace Shop\Entity\Feedback;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;

/**
 * Class BaseFeedback
 * @package Shop\Entity\Feedback
 *
 * @ORM\Entity()
 * @ORM\Table(name="feedback")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "feedback" = "Shop\Entity\Feedback\Feedback",
 *     "not_found_product" = "Shop\Entity\Feedback\NotFoundProduct",
 *     "complaint_agreements" = "Shop\Entity\Feedback\ComplaintAgreements",
 *     "affiliate_program" = "Shop\Entity\Feedback\AffiliateProgram",
 *     "complaint_intellectual_rights" = "Shop\Entity\Feedback\ComplaintIntellectualRights",
 *     "prohibited_products" = "Shop\Entity\Feedback\ProhibitedProducts"
 * })
 */
abstract class BaseFeedback
{
    use BaseMapping;

    public const
        TYPE_FEEDBACK = 'feedback',
        TYPE_NOT_FOUND_PRODUCT = 'not_found_product',
        TYPE_COMPLAINT_AGREEMENTS = 'complaint_agreements',
        TYPE_AFFILIATE_PROGRAM = 'affiliate_program',
        TYPE_COMPLAINT_INTELLECTUAL_RIGHTS = 'complaint_intellectual_rights',
        TYPE_PROHIBITED_PRODUCTS = 'prohibited_products';

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $answerSubject;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $answer;

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return BaseFeedback
     */
    public function setEmail(string $email): BaseFeedback
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return BaseFeedback
     */
    public function setComment(string $comment): BaseFeedback
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswerSubject(): ?string
    {
        return $this->answerSubject;
    }

    /**
     * @param string $answerSubject
     * @return BaseFeedback
     */
    public function setAnswerSubject(string $answerSubject): BaseFeedback
    {
        $this->answerSubject = $answerSubject;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     * @return BaseFeedback
     */
    public function setAnswer(string $answer): BaseFeedback
    {
        $this->answer = $answer;
        return $this;
    }

    abstract public function getReadableType();
}