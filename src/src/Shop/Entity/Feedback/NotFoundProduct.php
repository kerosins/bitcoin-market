<?php
/**
 * @author Kondaurov
 */

namespace Shop\Entity\Feedback;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class NotFoundProduct
 * @package Shop\Entity\Feedback
 *
 * @ORM\Entity()
 */
class NotFoundProduct extends BaseFeedback
{
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @return string
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     *
     * @return NotFoundProduct
     */
    public function setFullName(string $fullName): NotFoundProduct
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return NotFoundProduct
     */
    public function setUrl(string $url): NotFoundProduct
    {
        $this->url = $url;
        return $this;
    }

    public function getReadableType()
    {
        return 'Did not find what you were looking for? Write a letter to us!';
    }
}