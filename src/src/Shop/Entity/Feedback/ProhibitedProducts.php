<?php
/**
 * @author Kondaurov
 */

namespace Shop\Entity\Feedback;
use Doctrine\ORM\Mapping as ORM;
use Shop\Entity\Country;

/**
 * Class ProhibitedProducts
 * @package Shop\Entity\Feedback
 *
 * @ORM\Entity()
 */
class ProhibitedProducts extends BaseFeedback
{
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $kind;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Shop\Entity\Country")
     * @ORM\JoinColumn(name="country_id")
     */
    private $country;

    /**
     * @return string
     */
    public function getKind(): ?string
    {
        return $this->kind;
    }

    /**
     * @param string $kind
     *
     * @return ProhibitedProducts
     */
    public function setKind(string $kind): ProhibitedProducts
    {
        $this->kind = $kind;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return ProhibitedProducts
     */
    public function setUrl(string $url): ProhibitedProducts
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return Country
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * @param Country $country
     *
     * @return ProhibitedProducts
     */
    public function setCountry(Country $country): ProhibitedProducts
    {
        $this->country = $country;
        return $this;
    }

    public static function getProhibitedOptions()
    {
        return [
            'Offensive Weapons & Offensive Materials' => 'Offensive Weapons & Offensive Materials',
            'Illicit Drugs, Precursors and Drug Paraphernalia' => 'Illicit Drugs, Precursors and Drug Paraphernalia',
            'Flammable, Explosive & Hazardous Chemicals' => 'Flammable, Explosive & Hazardous Chemicals',
            'Obscene and Adult Materials' => 'Obscene and Adult Materials',
            'Medical Drugs and Devices' => 'Medical Drugs and Devices',
            'Human Parts & Remains and Protected Flora and Fauna' => 'Human Parts & Remains and Protected Flora and Fauna',
            'Software, Tools and Devices Used for Illegal Purposes' => 'Software, Tools and Devices Used for Illegal Purposes',
            'Other Prohibited Products' => 'Other Prohibited Products'
        ];
    }

    public function getReadableType()
    {
        return 'Report restricted or Prohibited items';
    }

}