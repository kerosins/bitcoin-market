<?php
/**
 * @author Kondaurov
 */

namespace Shop\Entity\Feedback;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class AffiliateProgram
 * @package Shop\Entity\Feedback
 *
 * @ORM\Entity()
 */
class AffiliateProgram extends BaseFeedback
{
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @return string
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     *
     * @return AffiliateProgram
     */
    public function setFullName(string $fullName): AffiliateProgram
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return AffiliateProgram
     */
    public function setUrl(string $url): AffiliateProgram
    {
        $this->url = $url;
        return $this;
    }

    public function getReadableType()
    {
        return 'Affiliate Program';
    }


}