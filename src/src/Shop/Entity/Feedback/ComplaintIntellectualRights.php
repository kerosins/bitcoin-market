<?php
/**
 * @author Kondaurov
 */

namespace Shop\Entity\Feedback;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ComplaintIntellectualRights
 * @package Shop\Entity\Feedback
 *
 * @ORM\Entity()
 */
class ComplaintIntellectualRights extends BaseFeedback
{

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     *
     * @return ComplaintIntellectualRights
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function getReadableType()
    {
        return 'Report intellectual property rights violation';
    }
}