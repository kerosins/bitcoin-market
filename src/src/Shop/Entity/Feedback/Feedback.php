<?php
/**
 * @author Kondaurov
 */

namespace Shop\Entity\Feedback;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Feedback
 * @package Shop\Entity\Feedback
 *
 * @ORM\Entity()
 */
class Feedback extends BaseFeedback
{
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $fullName;

    /**
     * @return string
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     *
     * @return Feedback
     */
    public function setFullName(string $fullName): Feedback
    {
        $this->fullName = $fullName;
        return $this;
    }

    public function getReadableType()
    {
        return 'Feedback';
    }
}