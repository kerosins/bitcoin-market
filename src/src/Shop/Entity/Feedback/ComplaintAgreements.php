<?php
/**
 * @author Kondaurov
 */

namespace Shop\Entity\Feedback;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ComplaintAgreements
 * @package Shop\Entity\Feedback
 *
 * @ORM\Entity()
 */
class ComplaintAgreements extends BaseFeedback
{
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @return string
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     *
     * @return ComplaintAgreements
     */
    public function setFullName(string $fullName): ComplaintAgreements
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return ComplaintAgreements
     */
    public function setUrl(string $url): ComplaintAgreements
    {
        $this->url = $url;
        return $this;
    }

    public function getReadableType()
    {
        return 'Complaint about the Seller';
    }
}