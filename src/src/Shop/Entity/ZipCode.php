<?php
/**
 * @author kerosin
 */

namespace Shop\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ZipCode
 * @package Shop\Entity
 *
 * @ORM\Entity(repositoryClass="Shop\Repository\ZipRepository")
 * @ORM\Table(name="zip_code")
 */
class ZipCode
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="Shop\Entity\AddressObject", inversedBy="zipCodes")
     * @ORM\JoinColumn(name="address_object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $addressObject;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Shop\Entity\Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $country;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return ZipCode
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set addressObject
     *
     * @param \Shop\Entity\AddressObject $addressObject
     *
     * @return ZipCode
     */
    public function setAddressObject(\Shop\Entity\AddressObject $addressObject = null)
    {
        $this->addressObject = $addressObject;

        return $this;
    }

    /**
     * Get addressObject
     *
     * @return \Shop\Entity\AddressObject
     */
    public function getAddressObject()
    {
        return $this->addressObject;
    }

    /**
     * Set country
     *
     * @param \Shop\Entity\Country $country
     *
     * @return ZipCode
     */
    public function setCountry(\Shop\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Shop\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}
