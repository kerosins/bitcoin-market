<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;

/**
 * Class Country
 * @package Shop\Entity
 *
 * @ORM\Entity()
 */
class Country
{
    use BaseMapping;
    use UpdateTimestamps;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="image", nullable=true)
     */
    private $flag;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="boolean")
     */
    private $allowAutocomplete = false;

    /**
     * @ORM\OneToMany(targetEntity="Shop\Entity\AddressObject", mappedBy="country")
     */
    private $childrenObjects;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $aliExpressCode;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Country
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Country
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Country
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set flag
     *
     * @param string $flag
     *
     * @return Country
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return string
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Add childrenObject
     *
     * @param \Shop\Entity\AddressObject $childrenObject
     *
     * @return Country
     */
    public function addChildrenObject(\Shop\Entity\AddressObject $childrenObject)
    {
        $this->childrenObjects[] = $childrenObject;

        return $this;
    }

    /**
     * Remove childrenObject
     *
     * @param \Shop\Entity\AddressObject $childrenObject
     */
    public function removeChildrenObject(\Shop\Entity\AddressObject $childrenObject)
    {
        $this->childrenObjects->removeElement($childrenObject);
    }

    /**
     * Get childrenObjects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildrenObjects()
    {
        return $this->childrenObjects;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set allowAutocomplete
     *
     * @param boolean $allowAutocomplete
     *
     * @return Country
     */
    public function setAllowAutocomplete($allowAutocomplete)
    {
        $this->allowAutocomplete = $allowAutocomplete;

        return $this;
    }

    /**
     * Get allowAutocomplete
     *
     * @return boolean
     */
    public function getAllowAutocomplete()
    {
        return $this->allowAutocomplete;
    }

    /**
     * @return string
     */
    public function getAliExpressCode(): ?string
    {
        return $this->aliExpressCode;
    }

    /**
     * @param string $aliExpressCode
     *
     * @return Country
     */
    public function setAliExpressCode(string $aliExpressCode = null)
    {
        $this->aliExpressCode = $aliExpressCode;
        return $this;
    }
}
