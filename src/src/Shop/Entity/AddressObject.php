<?php
/**
 * @author kerosin
 */

namespace Shop\Entity;

use Cp\Provider\AddressObjectProvider;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;

/**
 * Class AddressObject
 * @package Shop\Entity
 *
 * @ORM\Entity(repositoryClass="Shop\Repository\AddressObjectRepository")
 * @ORM\Table(name="address_object")
 */
class AddressObject
{
    use BaseMapping;

    const COUNTRY_TYPE = 1;
    const STATE_TYPE = 2;
    const COUNTY_TYPE = 3;
    const CITY_TYPE = 4;

    public static $typeLabels = [
        self::COUNTRY_TYPE => 'Country',
        self::STATE_TYPE => 'State',
        self::COUNTY_TYPE => 'County',
        self::CITY_TYPE => 'City'
    ];

    /**
     * @ORM\Column(type="string")
     */
    private $title;
    /**
     * @ORM\ManyToOne(targetEntity="Shop\Entity\AddressObject", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\Column(type="string", name="alt_title")
     */
    private $altTitle;

    /**
     * @ORM\ManyToOne(targetEntity="Shop\Entity\Country", inversedBy="childrenObjects")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $country;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $parentId;
    /**
     * @ORM\OneToMany(targetEntity="Shop\Entity\AddressObject", mappedBy="parent", cascade={"remove"})
     */
    private $children;

    /**
     * @ORM\OneToMany(targetEntity="Shop\Entity\ZipCode", mappedBy="addressObject", cascade={"remove"})
     */
    private $zipCodes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->zipCodes = new ArrayCollection();
    }

    public static function getChoices()
    {
        return array_flip(self::$typeLabels);
    }

    public function getTypeLabel()
    {
        return self::$typeLabels[$this->getType()];
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return AddressObject
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return AddressObject
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     *
     * @return AddressObject
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return AddressObject
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get parent
     *
     * @return \Shop\Entity\AddressObject
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set parent
     *
     * @param \Shop\Entity\AddressObject $parent
     *
     * @return AddressObject
     */
    public function setParent(\Shop\Entity\AddressObject $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Add child
     *
     * @param \Shop\Entity\AddressObject $child
     *
     * @return AddressObject
     */
    public function addChild(\Shop\Entity\AddressObject $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Shop\Entity\AddressObject $child
     */
    public function removeChild(\Shop\Entity\AddressObject $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set altTitle
     *
     * @param string $altTitle
     *
     * @return AddressObject
     */
    public function setAltTitle($altTitle)
    {
        $this->altTitle = $altTitle;

        return $this;
    }

    /**
     * Get altTitle
     *
     * @return string
     */
    public function getAltTitle()
    {
        return $this->altTitle;
    }

    /**
     * Add zipCode
     *
     * @param \Shop\Entity\ZipCode $zipCode
     *
     * @return AddressObject
     */
    public function addZipCode(\Shop\Entity\ZipCode $zipCode)
    {
        $this->zipCodes[] = $zipCode;
        $zipCode->setAddressObject($this);

        return $this;
    }

    /**
     * Remove zipCode
     *
     * @param \Shop\Entity\ZipCode $zipCode
     */
    public function removeZipCode(\Shop\Entity\ZipCode $zipCode)
    {
        $this->zipCodes->removeElement($zipCode);
    }

    /**
     * Get zipCodes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZipCodes()
    {
        return $this->zipCodes;
    }

    /**
     * Return a readable type of address
     *
     * @return mixed
     */
    public function getReadableType()
    {
        $types = AddressObjectProvider::typesList();
        $types = array_flip($types);
        return $types[$this->type];
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * Set country
     *
     * @param \Shop\Entity\Country $country
     *
     * @return AddressObject
     */
    public function setCountry(\Shop\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Shop\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}
