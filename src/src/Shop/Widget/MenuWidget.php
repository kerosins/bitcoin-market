<?php
/**
 * Menu class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Shop\Widget;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Elastic\Repository\ElasticCategoryRepository;
use Kerosin\Component\Widget;
use Kerosin\DependencyInjection\Contract\TaggedCacheContract;
use Kerosin\DependencyInjection\Contract\TaggedCacheContractTrait;
use Content\Navigation\Provider\MenuProvider;
use Catalog\ORM\Entity\Category;
use Shop\Provider\CategoryProvider;
use Catalog\ORM\Repository\CategoryRepository;
use Shop\Service\ActiveSeller;
use Symfony\Component\HttpFoundation\RequestStack;

class MenuWidget extends Widget implements TaggedCacheContract
{
    use TaggedCacheContractTrait;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('topMenu', [$this, 'topMenu'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction(
                'mainCategoryMenu',
                [$this, 'renderMainCategoryMenu'],
                ['is_safe' => ['html']]
            ),
            new \Twig_SimpleFunction(
                'sidebarCategoryMenu',
                [$this, 'renderSidebarCategoryMenu'],
                ['is_safe' => ['html']]
            ),
            new \Twig_SimpleFunction(
                'accountMenu',
                [$this, 'renderAccountMenu'],
                ['is_safe' => ['html']]
            )
        ];
    }

    /**
     * Render top menu
     *
     * @return string
     */
    public function topMenu()
    {
        return $this->twig()->render('@Shop/widgets/menu/top.twig');
    }

    public function renderMainCategoryMenu()
    {
        $categoryProvider = $this->container->get(CategoryProvider::class);
        $simpleList = $categoryProvider->getSimpleList(2, true);

        return $this->render('@Shop/widgets/menu/main_category.twig', [
            'categories' => $simpleList ?? []
        ]);
    }

    /**
     * Render a sidebar's menu of categories
     *
     * @param int $parent
     */
    public function renderSidebarCategoryMenu()
    {
        $seller = $this->container->get(ActiveSeller::class)->get();
        /** @var \Catalog\ORM\Repository\CategoryRepository $repository */
        $request = $this->request();
        $categoryId = $request->get('category');
        /** @var \Catalog\ORM\Entity\Category[]|\Catalog\ORM\Entity\Category $categories */
        if ($seller) {
            $categories = $this->container->get(ElasticCategoryRepository::class)->findBySellerAndParent($seller->getId(), $categoryId);
        } else {
            $categories = $this->container->get(ElasticCategoryRepository::class)->findByParentAndHasProducts($categoryId);
        }

        if (count($categories) == 0) {
            return '';
        }

        return $this->twig()->render(
            '@Shop/widgets/menu/sidebar_category.twig',
            ['categories' => $categories]
        );
    }

    public function renderStoreCategoryMenu()
    {

    }

    public function renderAccountMenu()
    {
        $items = $this->container->get(MenuProvider::class)->getItems('account');
        return $this->render('@Shop/widgets/menu/account_menu.twig', [
            'items' => $items
        ]);
    }

}