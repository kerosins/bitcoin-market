<?php
/**
 * BreadcrubmsWidget class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Shop\Widget;

use Kerosin\Component\Widget;
use Content\Navigation\Service\Breadcrumbs;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BreadcrumbsWidget extends Widget
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('breadcrumbs', [$this, 'renderBreadcrumbs'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('titleBreadcrumbs', [$this, 'renderTitleBreadcrumbs'], ['is_safe' => ['html']])
        ];
    }

    public function renderBreadcrumbs()
    {
        $crumbs = $this->container->get(Breadcrumbs::class)->get();
        return $this->render('@Shop/widgets/breadcrumbs/catalog.twig', [
            'crumbs' => $crumbs
        ]);
    }

    public function renderTitleBreadcrumbs(bool $reverse = true)
    {
        $crumbs = $this->container->get(Breadcrumbs::class)->get();
        $crumbs = array_column($crumbs, 'title');
        array_unshift($crumbs, 'Zenithbay');

        if ($reverse) {
            $crumbs = array_reverse($crumbs);
        }

        return implode(' - ', $crumbs);
    }
}