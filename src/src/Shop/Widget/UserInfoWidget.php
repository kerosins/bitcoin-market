<?php
/**
 * UserInfoWidget class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Shop\Widget;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use User\Entity\User;

class UserInfoWidget extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    public function __construct(\Twig_Environment $twig, TokenStorageInterface $tokenStorage)
    {
        $this->twig = $twig;
        $this->tokenStorage = $tokenStorage;
    }

    public function getGlobals()
    {
        $user = null;
        $token = $this->tokenStorage->getToken();
        if ($token) {
            $user = $this->tokenStorage->getToken()->getUser();
        }
        return [
            'user' => $user instanceof User ? $user : null
        ];
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('userInfo', [$this, 'getUserInfo'], ['is_safe' => ['html']]),
        ];
    }

    public function getUserInfo()
    {
        return $this->twig->render('@Shop/widgets/user_info/user_info.twig', [
            'user' => $this->tokenStorage->getToken()->getUser()
        ]);
    }
}