<?php
/**
 * @author Kerosin
 */

namespace Shop\Widget;

use Kerosin\Component\Widget;
use Catalog\Order\Cart\ProductCart;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ProductCartWidget extends Widget
{

    /**
     * @var ProductCart
     */
    private $cart;

    /**
     * Show icon
     *
     * @var bool
     */
    private $isShow = true;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->cart = $this->container->get(\Catalog\Order\Cart\ProductCart::class);
        $this->requestStack = $this->container->get('request_stack');
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('cartIcon', [$this, 'renderCartIcon'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('addToCartButton', [$this, 'renderAddButton'], ['is_safe' => ['html']])
        ];
    }

    /**
     * @param $isShow
     * @return $this
     */
    public function setIsShow($isShow)
    {
        $this->isShow = $isShow;
        return $this;
    }

    /**
     * render button for add to cart
     *
     * @param $id
     * @return string
     */
    public function renderAddButton($id)
    {
        return $this->render('@Shop/widgets/cart/add_button.twig', [
            'id' => $id,
            'isAdd' => $this->cart->get($id) === null
        ]);
    }

    /**
     * Render icon on top panel
     *
     * @return string
     */
    public function renderCartIcon()
    {
        if (!$this->isShow) {
            return '';
        }

        $total = $this->cart->getTotalPrice();

        return $this->render('@Shop/widgets/cart/icon.twig', [
            'count' => $this->cart->get()->count(),
            'items' => $this->cart->get(),
            'totalPrice' => $total
        ]);
    }
}