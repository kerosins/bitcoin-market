<?php
/**
 * @author Kerosin
 */

namespace Shop\Widget;

use Kerosin\Component\Widget;
use User\Service\UserCountry;

class CountryWidget extends Widget
{
    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('countryChangeList', [$this, 'renderCountryChangeList'], ['is_safe' => ['html']])
        ];
    }

    /**
     * @return string
     */
    public function renderCountryChangeList()
    {
        $repo = $this->em()->getRepository('ShopBundle:Country');
        $selectedCountry = $this->container->get(UserCountry::class);

        return $this->render('@Shop/widgets/country/selector.twig', [
            'countries' => $repo->findAll(),
            'selected' => $selectedCountry->get()
        ]);
    }
}