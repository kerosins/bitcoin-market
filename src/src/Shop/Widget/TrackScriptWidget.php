<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07.09.2018
 * Time: 19:21
 */

namespace Shop\Widget;


use Kerosin\Component\Widget;

class TrackScriptWidget extends Widget
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('googleAnalyticsTrackScript', [$this, 'renderGoogleAnalyticsTrackScript'], ['is_safe' => ['html']])
        ];
    }

    public function renderGoogleAnalyticsTrackScript()
    {
        if ($this->container->getParameter('kernel.debug')) {
            return '';
        }

        return $this->render('@Shop/widgets/track_script/google_analytics.twig');
    }
}