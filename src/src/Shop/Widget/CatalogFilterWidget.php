<?php
/**
 * @author Kerosin
 */

namespace Shop\Widget;

use Kerosin\Component\Widget;
use Shop\Exception\UnknownCategoryException;
use Shop\Service\ProductFilter;

class CatalogFilterWidget extends Widget
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('priceFilter', [$this, 'renderPriceFilter'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('catalogFilter', [$this, 'renderFilter'], ['is_safe' => ['html']])
        ];
    }

    public function renderFilter()
    {
        try {
            $productFilter = $this->container->get(\Shop\Service\ProductFilter::class);
            $form = $productFilter->buildFormView();
        } catch (UnknownCategoryException $e) {
            return '';
        }

        return $this->render(
            '@shop/widgets/catalog/filter.twig',
            [
                'form' => $form
            ]
        );
    }

    public function renderPriceFilter()
    {

    }
}