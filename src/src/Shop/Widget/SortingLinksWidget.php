<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Widget;

use Kerosin\Component\Widget;

class SortingLinksWidget extends Widget
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('sorting_links', [$this, 'renderSortingLinks'], ['is_safe' => ['html']])
        ];
    }

    public function renderSortingLinks($params, $view = null)
    {
        if (!$view) {
            $view = '@Shop/widgets/sorting_links/products.twig';
        }

        $currentSorting = array_filter($params['keys'], function ($item) {
            return $item['is_active'];
        });

        if (!$currentSorting) {
            $currentSorting = reset($params['keys']);
        } else {
            $currentSorting = reset($currentSorting);
        }
        $currentSorting = $currentSorting['key'];
        $params['current_sorting'] = $currentSorting;

        $request = $this->container->get('request_stack')->getMasterRequest();
        if (!$request) {
            $request = $this->container->get('request_stack')->getCurrentRequest();
        }

        $params['route'] = $request->get('_route');
        $params['query_params'] = array_merge($request->query->all(), $request->get('_route_params'));

        return $this->render($view, $params);
    }
}