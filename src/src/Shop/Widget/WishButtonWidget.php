<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Widget;

use Kerosin\Component\Widget;
use User\Service\WishProductManager;

class WishButtonWidget extends Widget
{
    /**
     * @var WishProductManager
     */
    private $wishManager;

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('wishButton', [$this, 'renderWishButton'], ['is_safe' => ['html']])
        ];
    }

    private function getWishManager()
    {
        if (!$this->wishManager) {
            $this->wishManager = $this->container->get(WishProductManager::class);
        }

        return $this->wishManager;
    }

    public function renderWishButton($productId, bool $isAdd = null)
    {
        if (!$isAdd) {
            $isAdd = true;

            if ($this->user()) {
                $isAdd = !$this->getWishManager()->has($productId);
            }
        }

        return $this->render('@Shop/widgets/wish/button.twig', [
            'isAdd' => $isAdd,
            'id' => $productId
        ]);
    }
}