<?php
/**
 * @author Kondaurov
 */

namespace Shop\Widget;

use Kerosin\Component\Widget;
use Shop\Form\SubscribeNewsletterFormType;
use User\Entity\User;

class NewsletterWidget extends Widget
{
    public function getFunctions()
    {
        return [
            new \Twig_Function('subscribeNewsLetterForm', [$this, 'renderSubscribeNewsLetterForm'], ['is_safe' => ['html']])
        ];
    }

    public function renderSubscribeNewsLetterForm()
    {
        $formFactory = $this->container->get('form.factory');
        $form = $formFactory->create(SubscribeNewsletterFormType::class);

        return $this->render('@Shop/widgets/newsletter/subscribe-form.twig', [
            'form' => $form->createView()
        ]);
    }
}