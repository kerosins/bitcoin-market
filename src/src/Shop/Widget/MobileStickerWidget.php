<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Widget;


use Kerosin\Component\Widget;

class MobileStickerWidget extends Widget
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('mobileSticker', [$this, 'renderMobileSticker'], ['is_safe' => ['html']])
        ];
    }

    /**
     * It only for testing mobile detector
     *
     * @return string
     */
    public function renderMobileSticker()
    {
        $detector = $this->container->get('mobile_detect.mobile_detector.public');

        $type = 'Desktop';
        $device = '';
        if ($detector->isMobile()) {
            $type = 'Mobile';
            $devices = forward_static_call([$detector, 'getPhoneDevices']);
            $device = 'Unknown Phone';
            foreach ($devices as $key => $match) {
                if ($detector->{'is' . $key}()) {
                    $device = $key;
                    break;
                }
            }
        }

        if ($detector->isTablet()) {
            $type = 'Tablet';
            $devices = forward_static_call([$detector, 'getTabletDevices']);
            $device = 'Unknown Tablet';
            foreach ($devices as $key => $match) {
                if ($detector->{'is' . $key}()) {
                    $device = $key;
                    break;
                }
            }
        }


        return $this->render('@Shop/widgets/mobile_sticker/sticker.twig', [
            'type' => $type,
            'device' => $device
        ]);
    }
}