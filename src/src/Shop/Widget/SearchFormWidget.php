<?php
/**
 * @author Kerosin
 */

namespace Shop\Widget;

use Kerosin\Component\Widget;
use Shop\Form\SearchType;

class SearchFormWidget extends Widget
{
    protected $functions = ['searchForm'];

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('searchForm', [$this, 'searchForm'], ['is_safe' => ['html']])
        ];
    }

    public function searchForm()
    {
        $formFactory = $this->container->get('form.factory');
        $form = $formFactory->create(
            SearchType::class,
            [
                'query' => $this->request()->get(
                    'query',
                    $this->request()->get('q')
                )
            ],
            [
                'action' => $this->container->get('router')->generate('shop.catalog.search')
            ]
        );
        $form->handleRequest($this->request());

        return $this->render('@Shop/widgets/search_form/default.twig', [
            'form' => $form->createView()
        ]);
    }
}