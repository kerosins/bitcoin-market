<?php
/**
 * @author Kondaurov
 */

namespace Shop\Widget;

use Kerosin\Component\Widget;
use Content\ORM\Entity\ItemBlock;
use Content\ItemBlock\BlockType\AutomaticProducts\AutomaticProductsBlock;
use Content\ItemBlock\BlockType\BannerList\BannerListBlock;
use Content\ItemBlock\BlockType\Category\CategoryBlock;
use Content\ItemBlock\BlockType\ManualProducts\ManualProductsBlock;
use Content\ItemBlock\BlockType\ConfigurableProducts\ConfigurableProductsBlock;
use Content\ItemBlock\ItemBlockManager;

class ItemBlockListWidget extends Widget
{
    private static $viewMap = [
        AutomaticProductsBlock::NAME => 'products.twig',
        ManualProductsBlock::NAME => 'products.twig',
        CategoryBlock::NAME => 'categories.twig',
        BannerListBlock::NAME => 'banners.twig',
        ConfigurableProductsBlock::NAME => 'products.twig'
    ];

    /**
     * @var ItemBlockManager
     */
    private $itemBlockManager;

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('itemBlockList', [$this, 'renderItemBlockList'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('itemBlock', [$this, 'renderItemBlock'], ['is_safe' => ['html']])
        ];
    }

    /**
     * Renders list of blocks of items, which stored in DB
     *
     * @return string
     * @throws \Twig_Error_Runtime
     */
    public function renderItemBlockList(string $area)
    {
        $itemBlockManager = $this->getItemBlockManager();

        if (!array_key_exists($area, ItemBlock::$areaLabels)) {
            throw new \Twig_Error_Runtime('Unknown Area');
        }

        $blocks = $this->em()->getRepository(ItemBlock::class)->findBy(
            [ 'area' => $area, 'enable' => true ],
            ['weight' => 'ASC']
        );

        $result = '';

        foreach ($blocks as $block) {
            $items = $itemBlockManager->findItemsViaBlock($block);

            if (!isset(self::$viewMap[$block->getType()])) {
                throw new \Twig_Error_Runtime('Missing view file for type ' . $block->getType());
            }

            $result .= $this->render(
                '@Shop/widgets/item_block/types/' . self::$viewMap[$block->getType()],
                [
                    'title' => $block->getTitle(),
                    'items' => $items,
                    'params' => $block->getParams()
                ]
            );
        }

        return $result;
    }

    /**
     * Render specified block of items
     *
     * @param string $type - type of block
     * @param array $params - parameters
     *
     * @return string
     */
    public function renderItemBlock(string $type, array $params = [])
    {
        $itemBlockManager = $this->getItemBlockManager();
        $itemBlock = $itemBlockManager->getBlockType($type);

        if (!$itemBlock) {
            throw new \RuntimeException('Unknown type of block');
        }

        $title = isset($params['title']) ? $params['title'] : $itemBlock->getTitle();
        $items = $itemBlock->findItems($params);

        if (!$items) {
            return '';
        }

        return $this->render('@Shop/widgets/item_block/types/' . self::$viewMap[$type],
            [
                'title' => $title,
                'items' => $items,
                'params' => $params
            ]
        );
    }

    /**
     * @return ItemBlockManager
     */
    private function getItemBlockManager()
    {
        if (!$this->itemBlockManager) {
            $this->itemBlockManager = $this->container->get(ItemBlockManager::class);
        }

        return $this->itemBlockManager;
    }
}