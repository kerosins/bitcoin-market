<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Widget;

use Kerosin\Component\Widget;
use Shop\Service\ActiveSeller;

class SellerWidget extends Widget implements \Twig_Extension_GlobalsInterface
{
    public function getGlobals()
    {
        return [
            'activeSeller' => $this->container->get(ActiveSeller::class)->get()
        ];
    }
}