<?php
/**
 * @author Kerosin
 */

namespace Shop\Widget;

use Kerosin\Component\ArrayHelper;
use Kerosin\Component\Widget;
use Payment\Component\Currency;
use Payment\Provider\CurrencyProvider;
use Payment\Service\CurrencyConverter;

class CurrencyWidget extends Widget implements \Twig_Extension_GlobalsInterface
{
    /**
     * @var CurrencyProvider
     */
    private $provider;

    /**
     * @var CurrencyConverter
     */
    private $converter;

    /**
     * @var bool
     */
    private $displayCurrencySelector = true;

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('currencySelector', [$this, 'currencySelector'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('currencyWrap', [$this, 'currencyWrap']),
            new \Twig_SimpleFunction('currencyIconsList', [$this, 'renderCurrencyIconsList'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('currencyRate', [$this, 'renderCurrencyRate']),
        ];
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('convert', [$this, 'convert']),
            new \Twig_SimpleFilter('convertTo', [$this, 'convertTo'])
        ];
    }

    public function getGlobals()
    {
        $currencyProvider = $this->container->get(CurrencyProvider::class);
        return [
            'currency' => $currencyProvider->getCurrentCurrency()
        ];
    }

    /**
     * @deprecated
     *
     * @param float $price
     * @return string
     */
    public function convert($price, $currency = null, $rate = null)
    {
        $toCurrency = $this->getProvider()->getCurrentCurrency();

        if ($currency || $rate) {
            if ($currency && !$currency instanceof Currency) {
                $toCurrency = $this->getProvider()->getCurrenciesList()->get($currency);
            } elseif (!$currency) {
                $toCurrency = new Currency();
            }

            $rate && $toCurrency->setRate($rate);
        }

        return $this->convertTo($price, $toCurrency);
    }

    /**
     * @param string|float $value
     * @param null|string|Currency $toCurrency
     * @param null|string|Currency $fromCurrency
     */
    public function convertTo(float $value, $toCurrency = null, $fromCurrency = 'USD')
    {
        $currenciesList = $this->getProvider()->getCurrenciesList();
        $fromCurrency = $fromCurrency instanceof Currency ? $fromCurrency : $currenciesList->get($fromCurrency);

        if ($toCurrency) {
            $toCurrency = $toCurrency instanceof Currency ? $toCurrency : $currenciesList->get($toCurrency);
        } else {
            $toCurrency = $this->getProvider()->getCurrentCurrency();
        }

        return $this->getConverter()->convert($value, $fromCurrency, $toCurrency);
    }

    /**
     * Renders currency selector
     *
     * @return string
     */
    public function currencySelector()
    {
        $currencyProvider = $this->container->get(\Payment\Provider\CurrencyProvider::class);
        return $this->render('@Shop/widgets/currency/currencySelector.twig', [
            'currencies' => $currencyProvider->getAllowedCurrenciesList(),
            'activeCurrency' => $currencyProvider->getCurrentCurrency(),
            'display' => $this->displayCurrencySelector,
        ]);
    }

    /**
     * Gets a Currency Provider
     *
     * @return CurrencyProvider
     */
    private function getProvider()
    {
        if (!$this->provider) {
            $this->provider = $this->container->get(CurrencyProvider::class);
        }

        return $this->provider;
    }

    /**
     * Gets a Converter
     *
     * @return CurrencyConverter
     */
    private function getConverter()
    {
        if (!$this->converter) {
            $this->converter = $this->container->get(CurrencyConverter::class);
        }

        return $this->converter;
    }

    /**
     * @param bool $display
     * @return $this
     */
    public function setDisplayCurrencySelector(bool $display)
    {
        $this->displayCurrencySelector = $display;
        return $this;
    }

    /**
     * todo it's dirty hack
     *
     * @param Currency|string $currency
     *
     * @return Currency
     */
    public function currencyWrap($currency)
    {
        $provider = $this->getProvider();

        if (is_string($currency)) {
            return $provider->getCurrenciesList()->get($currency);
        }
        /** @var Currency $systemCurrency */
        $systemCurrency = $provider->getCurrenciesList()->get($currency->getCode());

        $currency
            ->setName($systemCurrency->getName())
            ->setIcon($systemCurrency->getIcon())
        ;

        if (!$currency->getRate()) {
            $currency->setRate($systemCurrency->getRate());
        }

        return $currency;
    }

    public function renderCurrencyIconsList()
    {
        $provider = $this->container->get(CurrencyProvider::class);

        return $this->render('@Shop/widgets/currency/icons_list.twig', [
            'currencies' => $provider->getAllowedCurrenciesList()
        ]);
    }

    public function renderCurrencyRate(string $code)
    {
        $provider = $this->container->get(CurrencyProvider::class);
        /** @var Currency $currency */
        $currency = $provider->getCurrenciesList()->filter(function (Currency $currency) use ($code) {
            return $currency->getCode() === $code;
        })->first();

        $rate = $currency->getRate();
        return bcdiv(1, $rate, $this->container->getParameter('fiat_currency_scale'));
    }
}