<?php
/**
 * @author Kerosin
 */

namespace Shop\Exception;


use Throwable;

class UnknownCartHashException extends \RuntimeException
{

    public function __construct($message = "Unknown hash of cart", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}