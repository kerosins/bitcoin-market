<?php
/**
 * @author Kerosin
 */

namespace Shop\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

class OnlyAjaxRequestException extends HttpException
{
    public function __construct(
        $statusCode = 400,
        $message = 'This action requested only via Ajax',
        \Exception $previous = null,
        array $headers = array(),
        $code = 0
    ) {
        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }

}