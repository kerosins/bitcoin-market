<?php
/**
 * @author Kondaurov
 */

namespace Shop\Repository;

use Kerosin\Doctrine\ORM\EntityRepository;

class SellerRepository extends EntityRepository
{
    public function findByNames(array $names)
    {
        $names = array_map('strtolower', $names);

        return $this->createQueryBuilder('s')
            ->andWhere('lower(s.name) IN (:names)')
            ->setParameter('names', $names)
            ->getQuery()
            ->getResult();
    }
}