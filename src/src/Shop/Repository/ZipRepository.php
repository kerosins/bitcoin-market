<?php
/**
 * @author kerosin
 */

namespace Shop\Repository;


use Kerosin\Doctrine\ORM\EntityRepository;
use Shop\Entity\Country;

class ZipRepository extends EntityRepository
{

    public function findZipsByTitleAndAddresses($code, array $addresses = null)
    {
        $builder = $this->createQueryBuilder('z')->setMaxResults(20);

        $builder->andWhere('CAST_TO_TEXT(z.zip) LIKE :code')
            ->setParameter('code', "%{$code}%");

        if ($addresses) {
            $builder->andWhere('z.addressObject IN (:addresses)')
                ->setParameter('addresses', $addresses);
        }

        return $builder->getQuery()->getResult();
    }

    /**
     * Find Zip by code and country
     *
     * @param string $code
     * @param Country|int $country
     */
    public function findOneByCodeAndCountry($code, $country)
    {
        return $this->createQueryBuilder('z')
            ->andWhere('z.code = :code and z.country = :country')
            ->setParameters([
                'code' => $code,
                'country' => $country
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }
}