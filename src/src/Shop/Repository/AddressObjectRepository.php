<?php

namespace Shop\Repository;

use Shop\Entity\AddressObject;
use Kerosin\Doctrine\ORM\EntityRepository;

/**
 * AddressObjectRepository
 *
 * @method findOneByAltTitle(string $title)
 * @method findByType(int $type)
 * @method findByParent($parent)
 */
class AddressObjectRepository extends EntityRepository
{
    /**
     * @param $type
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function createBuilderWithType($type)
    {
        return $this->createQueryBuilder('ao')
            ->andWhere('ao.type = :type')
            ->setParameter('type', $type);
    }

    /**
     * @param $code
     * @return AddressObject|null
     */
    public function findStateByAltCode($code)
    {
        $builder = $this->createBuilderWithType(AddressObject::STATE_TYPE);
        return $builder->andWhere('ao.altTitle = :altTitle')
            ->setParameter('altTitle', $code)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function deleteAllByState(AddressObject $state)
    {
        $this->createQueryBuilder('ao')
            ->delete('ShopBundle:AddressObject', 'ao')
            ->andWhere('ao.parent = :parent')
            ->setParameter('parent', $state)
            ->getQuery()
            ->execute();
    }

    /**
     * @param string $title
     * @param int $type
     * @param array|null $parents
     * @return array
     */
    public function findByLikeTitleAndType(string $title, int $type, array $parents = null)
    {
        $builder = $this->createBuilderWithType($type)
            ->andWhere('ao.title LIKE :title')
            ->setParameter('title', "%{$title}%")
            ->setMaxResults(20)
        ;

        if ($parents) {
            $builder->andWhere('ao.parent IN (:parent)')->setParameter('parent', $parents);
        }

        return $builder->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function findAllStates()
    {
        return $this->createBuilderWithType(AddressObject::STATE_TYPE)->getQuery()->getResult();
    }
}
