<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Provider;

use Content\ORM\Entity\Slug;
use Content\SlugRoute\Service\SlugRouter;
use Elastica\Aggregation\Terms;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\Match;
use Elastica\Result;
use Elastica\SearchableInterface;
use Elastica\Suggest;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Catalog\ORM\Entity\Category;
use Symfony\Component\Routing\RouterInterface;

class SuggestionsProvider implements EntityManagerContract
{
    use EntityManagerContractTrait;

    /**
     * @var string
     */
    private $query;

    /**
     * @var \Elastica\Type
     */
    private $elasticIndex;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var int
     */
    private $countCategoriesInResult;

    /**
     * @var int
     */
    private $sizeOfResult;

    /**
     * @var SlugRouter
     */
    private $slugRouter;

    public function __construct(
        RouterInterface $router,
        SlugRouter $slugRouter,
        SearchableInterface $elasticIndex,
        int $countCategoriesInResult = 3,
        int $sizeOfResult = 10
    ) {
        $this->router = $router;
        $this->slugRouter = $slugRouter;
        $this->elasticIndex = $elasticIndex;
        $this->countCategoriesInResult = $countCategoriesInResult;
        $this->sizeOfResult = $sizeOfResult;
    }

    /**
     * @return array
     */
    public function search(): array
    {
        //build query
        $query = new Query();

        $boolQuery = new BoolQuery();
        $query->setQuery($boolQuery);

        $suggest = new Suggest();
        $suggest->setParam(
            'suggest',
            [
                'text' => $this->query,
                'term' => [
                    'field' => 'title',
                    'suggest_mode' => 'popular',
                    'analyzer' => 'simple'
                ]
            ]
        );
        $query->setSuggest($suggest);

        $fieldQuery = new Match();
        $fieldQuery->setFieldQuery('title', $this->query);
//        $fieldQuery->setFieldFuzziness('title', 2);
        $boolQuery->addMust($fieldQuery);

        $agg = new Terms('categories');
        $agg->setField('categories.id');
        $query->addAggregation($agg);

        $query->setSize(($this->sizeOfResult - $this->countCategoriesInResult));
        $query->setHighlight([
            'fields' => [
                'title' => new \StdClass
            ]
        ]);

        $elasticResult = $this->elasticIndex->search($query);
        $suggestions = [];

        $results = $elasticResult->getResults();
        if (!$results) {
            return $suggestions;
        }

        //prepare result
        //main
        $suggestions[] = [
            'id' => null,
            'title' => $this->query,
            'highlights' => "<em>{$this->query}</em>",
            'url' => $this->router->generate('shop.catalog.search', [
                'query' => $this->query
            ])
        ];

        //categories
        $aggregations = $elasticResult->getAggregation('categories');
        $categories = array_map(function ($bucket) {
            return $bucket['key'];
        }, $aggregations['buckets']);
        $categories = array_slice($categories, 0, $this->countCategoriesInResult);
        $categories = $this->entityManager->getRepository(Category::class)->findByInIds($categories);

        foreach ($categories as $category) {
            /** @var \Catalog\ORM\Entity\Category $category */
//            $this->slugRouter->generate()
            $suggestions[] = [
                'id' => $category->getId(),
                'type' => 'category',
                'title' => "{$this->query} in {$category->getTitle()}",
                'highlights' => "<em>{$this->query}</em> in {$category->getTitle()}",
                'url' => $this->slugRouter->generate(
                    Slug::TARGET_CATEGORY,
                        $category->getId(),
                        [ 'q' => $this->query ]
                )
            ];
        }

        //elastic suggestions
        $elasticSuggestions = $elasticResult->getSuggests()['suggest'];
        $elasticSuggestions = array_filter($elasticSuggestions, function ($suggest) {
            return $this->query !== $suggest['text'];
        });

        $currentSize = $this->sizeOfResult - count($suggestions);
        $suggestions = array_merge($suggestions, array_map(function ($suggest) {
            $text = $suggest['text'];
            return [
                'title' => $text,
                'type' => 'suggestion',
                'highlights' => "<em>{$text}</em>",
                'url' => $this->router->generate('shop.catalog.search', ['query' => $text])
            ];
        }, array_slice($elasticSuggestions, 0, $currentSize)));

        //products
        $productsSize = $this->sizeOfResult - count($suggestions);

        $suggestions = array_merge(
            $suggestions,
            array_map(function (Result $result) {
                $data = $result->getData();
                $highlights = $result->getHighlights();
                return [
                    'id' => $result->getId(),
                    'type' => 'product',
                    'title' => $data['title'],
                    'highlights' => $highlights['title'][0],
                    'url' => $this->slugRouter->generate(Slug::TARGET_PRODUCT, $result->getId())
                ];
            }, array_slice($results, 0, $productsSize))
        );

        $suggestions[] = [
            'title' => $this->query,
            'type' => 'all-results',
            'url' => $this->router->generate('shop.catalog.search', ['query' => $this->query])
        ];

        return $suggestions;
    }

    /**
     * @return string
     */
    public function getQuery(): ?string
    {
        return $this->query;
    }

    /**
     * @param string $query
     * @return SuggestionsProvider
     */
    public function setQuery(string $query): SuggestionsProvider
    {
        $this->query = htmlspecialchars($query);
        return $this;
    }
}