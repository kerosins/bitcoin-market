<?php
/**
 * ProductProvider class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Shop\Provider;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\QueryBuilder;
use Elastica\Aggregation\Terms;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\Match;
use Elastica\Query\Term;
use FOS\ElasticaBundle\Finder\FinderInterface;
use FOS\ElasticaBundle\Finder\PaginatedFinderInterface;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;
use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Payment\Provider\CurrencyProvider;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\Product;
use Shop\Entity\Seller;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class ProductProvider extends BaseProvider
{
    /**
     * @var array
     */
    private $categories;

    /**
     * array of product parameters where key is id
     *
     * @var array
     */
    private $parameters = [];

    /**
     * @var bool
     */
    private $onlyWithPrice = false;

    /**
     * Query for search by title
     *
     * @var string
     */
    private $title;

    /**
     * Founded categories
     *
     * @var array
     */
    private $foundCategories = [];

    /**
     * @var string
     */
    private $sort;

    /**
     * @var string
     */
    private $direction = 'asc';

    /**
     * @var CurrencyProvider
     */
    private $currencyProvider;

    /**
     * @var RepositoryManagerInterface
     */
    private $repositoryManager;

    /**
     * @var int
     */
    private $seller;

    public function __construct(
        ObjectManager $entityManager,
        PaginatorInterface $paginator,
        RequestStack $requestStack,
        CurrencyProvider $currencyProvider,
        RepositoryManagerInterface $repositoryManager
    ) {
        $this->currencyProvider = $currencyProvider;
        $this->repositoryManager = $repositoryManager;
        parent::__construct($entityManager, $paginator, $requestStack);
    }

    /**
     * @param bool $onlyWithPrice
     */
    public function setOnlyWithPrice(bool $onlyWithPrice)
    {
        $this->onlyWithPrice = $onlyWithPrice;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     *
     * @return self
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param array $categories
     */
    public function setCategories(array $categories)
    {
        $this->categories = $categories;
    }

    /**
     * @param $query
     * @return $this
     */
    public function setTitle($query)
    {
        $this->title = $query;
        return $this;
    }

    public function setSeller(int $seller)
    {
        $this->seller = $seller;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function search() : PaginationInterface
    {

        $elasticProductRepository = $this->repositoryManager->getRepository(Product::class);
        $query = $this->buildQuery();

        /**
         * todo rewrite it
         */
        if ($this->sort) {
            $sortingMap = $this->getSortingMap();
            $mapItem = isset($sortingMap[$this->sort]) ? $sortingMap[$this->sort] : reset($sortingMap);
            $query->addSort([
                $mapItem['column'] => ['order' => $this->direction]
            ]);
        }

        $results = $elasticProductRepository->createPaginatorAdapter($query);

        $aggregations = $results->getAggregations();
        if (!empty($aggregations['categories'])) {
            $repo = $this->em->getRepository(Category::class);
            $ids = array_column($aggregations['categories']['buckets'], 'key');
            $this->foundCategories = $repo->findByInIds($ids);
        }

        return $this->buildPaginationResult($results);
    }

    /**
     * Build query to elasticsearch
     */
    private function buildQuery() : Query
    {
        $boolQuery = new BoolQuery();
        $query = new Query();
        $query->setQuery($boolQuery);

        $availableTerm = new Term();
        $availableTerm->setTerm('status', Product::STATUS_ACTIVE);
        $boolQuery->addMust($availableTerm);

        if ($this->categories) {
            $categoryTerm = new Query\Terms();
            $categoryTerm->setTerms('categories.id', $this->categories);
            $boolQuery->addMust($categoryTerm);
        } else {
            $agg = new Terms('categories');
            $agg->setField('categories.id');
            $query->addAggregation($agg);
        }

        if ($this->title) {
            $titleMatch = new Match();
            $titleMatch->setFieldQuery('title', $this->title);
            $boolQuery->addMust($titleMatch);
        }

        if ($this->seller) {
            $boolQuery->addMust((new Term())->setTerm('supplier.id', $this->seller));
        }

        if ($this->parameters) {
            $this->makeSearchConditionByProductParameters($boolQuery);
        }

        return $query;
    }

    /**
     * Make query for Elasticsearch
     *
     * @param QueryBuilder $builder
     */
    private function makeSearchConditionByProductParameters(BoolQuery $outerQuery)
    {
        foreach ($this->parameters as $id => $value) {
            $facetTerm = new Term();
            $facetTerm->setTerm('parameters.facet.id', $id);

            if (is_array($value)) {
                //make condition for range type of facet
                if (array_key_exists('min', $value) && array_key_exists('max', $value)) {
                    $andInner = new BoolQuery();
                    $valueRange = new Query\Range();

                    $range = [];
                    if (!empty($value['min'])) {
                        $range['gte'] = $value['min'];
                    }

                    if (!empty($value['max'])) {
                        $range['lte'] = $value['max'];
                    }


                    $column = ('price' === $id ? 'publicPrice' : $id);


                    if ('price' !== $id) {
                        $andInner->addMust($facetTerm);
                    } else {
                        $range = array_map(function ($value) {
                            return $this->currencyProvider->convertToUsd(
                                $value,
                                $this->currencyProvider->getCurrentCurrency()
                            );
                        }, $range);
                    }

                    $valueRange->setParam($column, $range);
                    $andInner->addMust($valueRange);

                    $outerQuery->addMust($andInner);
                } else {
                    //make condition for list type of facet
                    $orOuter = new BoolQuery();
                    foreach ($value as $item) {
                        $andInner = new BoolQuery();

                        $valueTerm = new Term();
                        $valueTerm->setTerm('parameters.value', $item);

                        $andInner->addMust($facetTerm);
                        $andInner->addMust($valueTerm);

                        $orOuter->addShould($andInner);
                    }
                    $outerQuery->addMust($orOuter);
                }
            } else {
                //make condition for bool type
                $andInner = new BoolQuery();

                $valueTerm = new Term();
                $valueTerm->setTerm('parameters.value', $value);

                $andInner->addMust($facetTerm);
                $andInner->addMust($valueTerm);

                $outerQuery->addMust($andInner);
            }
        }
    }

    /**
     * Get all categories where products were found
     *
     * @return array
     */
    public function getFoundCategories()
    {
        return $this->foundCategories;
    }

    /**
     * @todo нужно разобраться с запросом
     * Get titles for autocomplete
     */
    public function autocomplete()
    {
        $query = $this->buildQuery();

        return array_map(function (Product $product) {
            return [
                'id' => $product->getId(),
                'title' => $product->getTitle()
            ];
        }, $this->finder->find($query));
    }

    public function getSortingMap(): ?array
    {
        return [
            'title' => [
                'column' => 'title.keyword',
                'title' => 'Name'
            ],
            'price' => [
                'column' => 'publicPrice',
                'title' => 'Price'
            ]
        ];
    }

    /**
     * @return string
     */
    public function getSort(): ?string
    {
        return $this->sort;
    }

    /**
     * @param string $sort
     * @return ProductProvider
     */
    public function setSort(string $sort = null): ProductProvider
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return string
     */
    public function getDirection(): ?string
    {
        return $this->direction;
    }

    /**
     * @param string $direction
     * @return ProductProvider
     */
    public function setDirection(string $direction = null): ProductProvider
    {
        $direction = strtolower($direction);
        if ($direction && in_array($direction, ['asc', 'desc'])) {
            $this->direction = $direction;
        } else {
            $this->direction = 'asc';
        }
        return $this;
    }

    /**
     * fixme
     */
    public function getSortingParams()
    {
        $sortingKeys = array_keys($this->getSortingMap());

        return [
            'keys' => array_map(function ($key, $params) {
                return [
                    'key' => $key,
                    'is_active' => $key === strtolower($this->sort),
                    'title' => $params['title']
                ];
            }, $sortingKeys, $this->getSortingMap()),
            'currentDirection' => $this->direction,
            'direction' => $this->direction === 'asc' ? 'desc' : 'asc'
        ];
    }
}