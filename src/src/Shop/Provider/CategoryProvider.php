<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Provider;

use Doctrine\ORM\Query;
use Elastic\Repository\ElasticCategoryRepository;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\DependencyInjection\Contract\TaggedCacheContract;
use Kerosin\DependencyInjection\Contract\TaggedCacheContractTrait;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\Product;
use Symfony\Component\Cache\CacheItem;
use Elastic\Entity\Category as ElasticCategory;

/**
 * Class CategoryProvider
 * @package Shop\Provider
 *
 * todo rewrite with use elastic
 */
class CategoryProvider implements EntityManagerContract, TaggedCacheContract
{
    use EntityManagerContractTrait, TaggedCacheContractTrait;

    public const CACHE_TAG = 'categories_with_products_tree';
    private $allCategoriesCacheKey = 'categories.all_categories';
    private $allChildrenCacheKey = 'all_children.category.%d';

    /**
     * @var ElasticCategoryRepository
     */
    private $categoryRepository;

    public function __construct(ElasticCategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Gets list of categories with products
     *
     * @return array|mixed|\Catalog\ORM\Entity\Category[]|CacheItem
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getAllCategoriesWithProducts()
    {
        $categories = $this->taggedCache->getItem($this->allCategoriesCacheKey);
        if ($categories->isHit()) {
            return $categories->get();
        }

        $categories = $this->buildTree();

        return $categories;
    }

    /**
     * Gets category with products
     *
     * @param $id
     *
     * @return \Catalog\ORM\Entity\Category|null
     */
    public function getCategoryWithProducts($id = null)
    {
        $categories = $this->getAllCategoriesWithProducts();
        if (!$id) {
            return array_filter($categories, function (Category $category) {
                return $category->getParentId() === null;
            });
        }

        return isset($categories[$id]) ? $categories[$id] : null;
    }

    /**
     * builds tree of categories
     *
     * @return array|\Catalog\ORM\Entity\Category[]
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function buildTree()
    {
        $allCategories = $this->entityManager->getRepository(Category::class)->findBy(['enabled' => true]);
        $productRepo = $this->entityManager->getRepository(Product::class);

        /** @var \Catalog\ORM\Entity\Category[] $indexed */
        $indexed = [];
        foreach ($allCategories as $category) {
            /** @var \Catalog\ORM\Entity\Category $category */
            $issetProducts = $productRepo->existByCategory($category);
            $category->setIssetProducts($issetProducts);
            $indexed[$category->getId()] = $category;
        }

        //clean from empty categories
        foreach ($indexed as $id => $category) {
            if ($category->getParent() !== null && !$category->issetProducts()) {
                $indexed[$category->getParentId()]->removeChild($category);
                if ($category->getChildren()->count() === 0) {
                    unset($indexed[$id]);
                }
            }
        }

        $indexed = array_filter($indexed, function (Category $category) {
            return $category->getChildren()->count() > 0 || $category->issetProducts();
        });

        /**
         * Calculates depth of category
         *
         * @param \Catalog\ORM\Entity\Category $item
         *
         * @return int
         */
        $getDepth = function (Category $item) {
            $depth = 0;

            $parent = $item->getParent();
            while (true) {
                if ($parent === null) {
                    return $depth;
                }

                $depth++;
                $parent = $parent->getParent();
            }

            return $depth;
        };

        $maxDepth = 0;
        $indexed = array_map(function (Category $category) use ($getDepth, &$maxDepth) {
            $depth = $getDepth($category);
            $category->setDepth($depth);
            $maxDepth = $maxDepth < $depth ? $depth : $maxDepth;
            return $category;
        }, $indexed);

        //drops lost children
        $indexed = array_filter($indexed, function (Category $category) use ($indexed) {
            return $category->getParentId() === null || isset($indexed[$category->getParentId()]);
        });

        $allChildren = [];

        //all children
        while ($maxDepth != 0) {
            $cats = array_filter($indexed, function (Category $category) use ($maxDepth) {
                return $category->getDepth() == $maxDepth;
            });

            foreach ($cats as $k => $category) {
                if (!$category->getParentId()) {
                    continue;
                }

                if (!isset($allChildren[$category->getParentId()])) {
                    $allChildren[$category->getParentId()] = [];
                }

                $allChildren[$category->getParentId()][] = $category->getId();

                if (isset($allChildren[$category->getId()])) {
                    $options = [$allChildren[$category->getParentId()], $allChildren[$category->getId()]];
                    $allChildren[$category->getParentId()] = array_merge(...$options);
                }

                unset($cats[$k]);
            }

            $maxDepth--;
        }

        //save to cache
        foreach ($allChildren as $id => $children) {
            $allChildrenCache = $this->taggedCache->getItem(sprintf($this->allChildrenCacheKey, $id));
            $allChildrenCache->set($children);
            $allChildrenCache->tag(self::CACHE_TAG);

            $this->taggedCache->save($allChildrenCache);

            unset($allChildren[$id]);
        }

        $cacheItem = $this->taggedCache->getItem($this->allCategoriesCacheKey);
        $cacheItem->set($indexed);
        $cacheItem->tag(self::CACHE_TAG);

        $this->taggedCache->save($cacheItem);

        return $indexed;
    }

    /**
     * @todo if children are exist, but don't return, then warm up cache or refresh that
     *
     * Gets all children
     *
     * @param $parentId
     * @return null
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getAllChildren($parentId)
    {
        $cacheItem = $this->taggedCache->getItem(sprintf($this->allChildrenCacheKey, $parentId));
        if (!$cacheItem->get()) {
            return null;
        }

        return $cacheItem->get();
    }

    /**
     * Gets a simple list of categories
     *
     * @param int $depth - max depth of list, starts from 1
     * @param bool $hasProducts
     *
     * @return array
     */
    public function getSimpleList(int $depth = null, bool $hasProducts = false)
    {
        $key = 'categories.simpleList';
        $method = 'findAllByFirstParent';
        if ($hasProducts) {
            $key .= '.hasProducts';
            $method = 'findByParentAndHasProducts';
        }

        if ($depth) {
            $key .= '.d' . $depth;
        }

        $cachedList = $this->taggedCache->getItem($key);

        if ($cachedList->isHit()) {
            return $cachedList->get();
        }

        $get = function ($parentId = null, $currentDepth) use ($method, $depth, &$get) {
            if ($depth && $currentDepth > $depth) {
                return [];
            }

            $categories = $this->categoryRepository->$method($parentId);
            $list = [];
            foreach ($categories as $category) {
                $list[] = [$category];
                $list[] = $get($category->getId(), ($currentDepth + 1));
            }

            if (count($list)) {
                $list = array_merge(...$list);
            }
            return $list;
        };

        $list = $get(null, 1);

        $list = array_map(function (ElasticCategory $category) {
            return [
                'id' => $category->getId(),
                'title' => $category->getTitle(),
                'depth' => $category->getDepth(),
                'parent' => $category->getFirstParent() ? $category->getFirstParent()->getId() : null,
                'fullPath' => $category->getFullPath()
            ];
        }, $list);

        $cachedList->set($list);
        $cachedList->tag([self::CACHE_TAG]);
        $this->taggedCache->save($cachedList);

        return $cachedList->get();
    }
}