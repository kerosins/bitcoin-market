<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Provider;

use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;

class ZipProvider extends BaseProvider
{
    /**
     * @var int
     */
    private $city;

    /**
     * @var int
     */
    private $country;

    /**
     * @var string
     */
    private $q;

    /**
     * @inheritdoc
     */
    public function search(): PaginationInterface
    {
        $repo = $this->em->getRepository('ShopBundle:ZipCode');

        $builder = $repo->createQueryBuilder('zc');
        if ($this->city && 'null' !== $this->city) {
            $builder->andWhere('zc.addressObject = :city')->setParameter('city', $this->city);
        }

        if ($this->country && 'null' !== $this->country) {
            $builder->andWhere('zc.country = :country')->setParameter('country', $this->country);
        }

        if ($this->q) {
            $builder->andWhere('lower(zc.code) like :code')->setParameter('code', '%' . strtolower($this->q) .'%');
        }

        return $this->buildPaginationResult($builder->getQuery());
    }

    /**
     * @param int $city
     * @return ZipProvider
     */
    public function setCity($city = null): ZipProvider
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @param int $country
     * @return ZipProvider
     */
    public function setCountry($country = null): ZipProvider
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @param string $q
     * @return ZipProvider
     */
    public function setQuery($q = null): ZipProvider
    {
        $this->q = $q;
        return $this;
    }


}