<?php
/**
 * @author Kerosin
 */

namespace Shop\Provider;

use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Shop\Entity\AddressObject;
use Shop\Entity\Country;

class AddressProvider extends BaseProvider
{
    /**
     * @var int
     */
    private $country;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $q;

    /**
     * @var Country|int
     */
    private $parent;

    /**
     * @return PaginationInterface
     */
    public function search(): PaginationInterface
    {
        $builder = $this->em->getRepository('ShopBundle:AddressObject')->createQueryBuilder('a');

        if ($this->country) {
            $builder->andWhere('a.country = :country')
                    ->setParameter('country', $this->country);
        }

        if ($this->type) {
            $builder->andWhere('a.type = :type')
                    ->setParameter('type', $this->type);
        }

        if ($this->q) {
            $q = strtolower($this->q);
            $builder->andWhere('lower(a.title) LIKE :title')
                    ->setParameter('title', "%{$q}%");
        }

        if ($this->parent) {
            if (AddressObject::CITY_TYPE == $this->type) {
                $counties = $this->em
                    ->getRepository('ShopBundle:AddressObject')
                    ->findByParent($this->parent);

                $builder->andWhere('a.parent IN (:parents)')
                        ->setParameter('parents', $counties);
            } else {
                $builder->andWhere('a.parent = :parent')
                    ->setParameter('parent', $this->parent);
            }
        }

        return $this->buildPaginationResult($builder->getQuery());
    }

    /**
     * @param string $type
     *
     * @return static
     */
    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param string $q
     *
     * @return static
     */
    public function setQ(string $q = null)
    {
        $this->q = $q;

        return $this;
    }

    /**
     * @param Country|int $country
     * @return $this
     */
    public function setCountry($country)
    {
        if ($country instanceof Country) {
            $country = $country->getId();
        }

        $this->country = $country;

        return $this;
    }

    /**
     * @param int|Country $parent
     * @return AddressProvider
     */
    public function setParent($parent)
    {
        if ($parent instanceof Country) {
            $parent = $parent->getId();
        }

        $this->parent = $parent;
        return $this;
    }


}