<?php
/**
 * @author Kerosin
 */

namespace Shop\Service;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Catalog\ORM\Entity\AssemblyFacet;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\FacetCategory;
use Shop\Exception\UnknownCategoryException;
use Shop\Form\CatalogFilterType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Filter in category storage
 *
 * Class ProductFilter
 * @package Shop\Service
 *
 * todo rearrange it
 */
class ProductFilter
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var FormBuilderInterface
     */
    private $factory;

    /**
     * @var Category
     */
    private $category;

    /**
     * @var CatalogFilterType
     */
    private $form;

    function __construct(ObjectManager $manager, RequestStack $requestStack, FormFactory $factory)
    {
        $this->em = $manager;
        $this->requestStack = $requestStack;
        $this->factory = $factory;
    }

    /**
     * @param Category|int $category
     */
    public function setCategory($category)
    {
        if (is_int($category)) {
            $category = $this->em->getRepository(\Catalog\ORM\Entity\Category::class)->find($category);
        }

        $this->category = $category;
        return $this;
    }

    private function getForm()
    {
        if (!$this->form) {
            if (!$this->category) {
                throw new UnknownCategoryException;
            }

            $assembly = $this->em->getRepository(AssemblyFacet::class)->findByCategory($this->category);
            $data = array_map(function(AssemblyFacet $item) {
                return $item->getData();
            }, $assembly);

            array_unshift($data, [
                'id' => 'price',
                'type' => FacetCategory::RANGE,
                'title' => 'Price'
            ]);

            $form = $this->factory->create(CatalogFilterType::class, [], [ 'config_fields' => $data ]);
            $form->handleRequest($this->requestStack->getCurrentRequest());

            $this->form = $form;
        }

        return $this->form;
    }


    /**
     * build FormView filters
     * @return FormView
     */
    public function buildFormView()
    {
        return $this->getForm()->createView();
    }

    /**
     * @return array
     */
    public function getFilterData()
    {
        $data = $this->getForm()->getData();
        $ids = array_keys($data);
        $ids = array_map(function ($key) {
            return preg_replace('/^(filter:)(price|[0-9]*)(.*)/', '$2', $key);
        }, $ids);

        return array_combine($ids, array_values($data));
    }
}