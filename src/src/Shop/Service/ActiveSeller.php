<?php
/**
 * @author Kondaurov
 */

namespace Shop\Service;


use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Shop\Entity\Seller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class ActiveSeller implements EntityManagerContract
{
    use EntityManagerContractTrait;

    /**
     * @var string
     */
    private $sellerParameter;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Seller
     */
    private $seller;

    public function __construct(
        RequestStack $requestStack,
        string $sellerParameter
    ) {
        $this->sellerParameter = $sellerParameter;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @return Seller|null
     */
    public function get()
    {
        if (!$this->seller) {
            $seller = $this->request->get($this->sellerParameter);
            if (!$seller) {
                return null;
            }

            $this->seller = $this->entityManager->find(Seller::class, $seller);
        }

        return $this->seller;
    }
}