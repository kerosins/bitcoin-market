<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Shop\Command;


use Kerosin\Component\ContainerAwareCommand;
use Shop\Provider\CategoryProvider;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CacheWarmCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('shop:cache:warm');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $output->writeln('Warm up list of categories');
        $container->get(CategoryProvider::class)->buildTree();

        $output->writeln('Done');
    }
}