<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Widget;


use Content\Navigation\Service\Breadcrumbs;
use Kerosin\Component\Widget;

class CpBreadcrumbs extends Widget
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('cpBreadcrumbs', [$this, 'renderCpBreadcrumbs'], ['is_safe' => ['html']])
        ];
    }

    public function renderCpBreadcrumbs()
    {
        $breadcrumbs = $this->container->get(Breadcrumbs::class)->get();
        if (!$breadcrumbs) {
            return '';
        }

        return $this->render('@Cp/widget/breadcrumbs.twig', ['breadcrumbs' => $breadcrumbs]);
    }
}