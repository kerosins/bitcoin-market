<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Widget;


use Catalog\ORM\Entity\Order;
use Kerosin\Component\Widget;
use Catalog\ORM\Entity\OrderStatus;
use Catalog\Order\Service\OrderManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OrderWidget extends Widget
{
    private $orderManager;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->orderManager = $container->get(OrderManager::class);
    }

    /**
     * @inheritdoc
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('statusCssClass', [$this, 'statusCssClassFilter']),
            new \Twig_SimpleFilter('isCriticalOrderLifeTime', [$this, 'isCriticalOrderLifeTimeFilter']),
            new \Twig_SimpleFilter('orderCanAccept', [$this, 'orderCanAccept'])
        ];
    }

    /**
     * Provide css class for status of order
     *
     * @param $status
     * @return mixed
     */
    public function statusCssClassFilter($status)
    {
        switch ($status) {
            //todo add another classes for all statuses
            case OrderStatus::PROCESS:
                return 'order-status-processing';
            default:
                return 'order-status-default';
        }
    }

    /**
     * Diff in days between present date and current
     *
     * @param \DateTime $dateEnd
     * @return bool
     */
    public function isCriticalOrderLifeTimeFilter(\DateTime $dateEnd = null)
    {
        if (!$dateEnd) {
            return false;
        }
        return $this->orderManager->isCriticalIntervalUntilEnd($dateEnd);
    }

    public function orderCanAccept(Order $order)
    {
        return $order->getCurrentStatus()->getType() === OrderStatus::SHIPPING;
    }
}