<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Widget;

use Kerosin\Component\Widget;

class ButtonBlockWidget extends Widget
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('ButtonBlock', [$this, 'renderButtonBlock'], ['is_safe' => ['html']])
        ];
    }

    /**
     * @param array $buttons - list of buttons, where key is a button ID
     * Structure
     * - $title
     * - $buttonClass
     * - $route
     * - $routeParams
     * - $icon
     */
    public function renderButtonBlock(array $buttons)
    {
        //set predefine options for some buttons
        foreach ($buttons as $key => &$button) {
            switch ($key) {
                case 'edit':
                    $button['icon'] = 'pencil';
                    $button['buttonClass'] .= ' btn btn-primary';
                    break;
                case 'delete':
                    $button['icon'] = 'trash';
                    $button['buttonClass'] .= ' btn btn-danger';
                    break;
            }
        }

        return $this->render('@Cp/widget/buttonBlock.twig', [
            'buttons' => $buttons
        ]);
    }
}