<?php
/**
 * @author Kondaurov
 */

namespace Cp\Widget;


use Kerosin\Component\Widget;

class SortWidget extends Widget
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('sortPath', [$this, 'renderSortPath'])
        ];
    }

    public function renderSortPath(string $route, string $sortBy, int $currentDirection)
    {
        $request = $this->request();
        $router = $this->container->get('router');

        $params = $request->query->all();
        $params['sortBy'] = $sortBy;
        $params['direction'] = $currentDirection === SORT_ASC ? SORT_DESC : SORT_ASC;

        return $router->generate($route, $params);
    }

}