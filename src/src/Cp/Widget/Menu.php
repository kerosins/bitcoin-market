<?php
/**
 * @author Kondaurov
 */

namespace Cp\Widget;

use Kerosin\Component\Widget;
use function Kerosin\Helper\StringHelper\endsWith;
use Content\Navigation\Provider\MenuProvider;
use Symfony\Component\Yaml\Yaml;

class Menu extends Widget
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('sidebarMenu', [$this, 'renderSidebarMenu'], ['is_safe' => ['html']])
        ];
    }

    public function renderSidebarMenu()
    {
        $items = $this->container->get(MenuProvider::class)->getItems('controlPanel');
        return $this->twig()->render('@Cp/widget/sidebar_menu.twig', [
            'menu' => $items
        ]);
    }
}