<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Provider;

use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Kerosin\Entity\BusinessLog;
use Knp\Component\Pager\Pagination\PaginationInterface;

class LogProvider extends BaseProvider
{
    /**
     * @var string
     */
    private $level;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var string
     */
    private $message;

    public function search() : PaginationInterface
    {
        $builder = $this->em->getRepository(BusinessLog::class)->createQueryBuilder('l');

        $builder->orderBy('l.createdAt', 'DESC');

        if ($this->message) {
            $builder->andWhere('lower(l.message) LIKE lower(:message)')
                    ->setParameter('message', '%' . $this->message . '%');
        }

        if ($this->channel) {
            $builder
                ->andWhere('l.channel = :channel')
                ->setParameter('channel', $this->channel);
        }

        if ($this->level) {
            $level = array_map('strtolower', $this->level);
            $builder
                ->andWhere('lower(l.levelName) IN (:level)')
                ->setParameter('level', $level);
        }

        return $this->buildPaginationResult($builder->getQuery());
    }

    /**
     * @return mixed
     */
    public function getChannel(): ?string
    {
        return $this->channel;
    }

    /**
     * @param mixed $channel
     * @return LogProvider
     */
    public function setChannel($channel): LogProvider
    {
        $this->channel = $channel;
        return $this;
    }

    /**
     * @return string
     */
    public function getLevel(): ?array
    {
        return $this->level;
    }

    /**
     * @param string $level
     * @return LogProvider
     */
    public function setLevel(array $level = null): LogProvider
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return LogProvider
     */
    public function setMessage(string $message): LogProvider
    {
        $this->message = $message;
        return $this;
    }


}