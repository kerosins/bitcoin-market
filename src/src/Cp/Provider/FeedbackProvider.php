<?php
/**
 * @author Kondaurov
 */

namespace Cp\Provider;


use Doctrine\ORM\Query;
use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Shop\Entity\Feedback\BaseFeedback;
use Shop\Entity\Feedback\Feedback;

class FeedbackProvider extends BaseProvider
{
    private $id;

    private $type;

    private $dateFrom;

    private $dateTo;

    public function search(): PaginationInterface
    {
        $builder = $this->em->getRepository(BaseFeedback::class)->createQueryBuilder('f');
        $builder->orderBy('f.createdAt', 'DESC');


        return $this->buildPaginationResult($builder->getQuery()->setHydrationMode(Query::HYDRATE_ARRAY));
    }
}