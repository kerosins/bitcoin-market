<?php
/**
 * @author Kerosin
 */

namespace Cp\Provider;

use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Shop\Entity\AddressObject;

class AddressObjectProvider extends BaseProvider
{
    private $title;

    private $type;

    public function search(): PaginationInterface
    {
        $repo = $this->em->getRepository('ShopBundle:AddressObject');
        $builder = $repo->createQueryBuilder('a');

        if ($this->type) {
            $builder->andWhere('a.type = :type')
                    ->setParameter('type', $this->type);
        }

        if ($this->title) {
            $builder->andWhere('LOWER(a.title) LIKE LOWER(:title)')
                    ->setParameter('title',"%{$this->title}%");
        }

        return $this->buildPaginationResult($builder->getQuery());
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return AddressObjectProvider
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return AddressObjectProvider
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public static function typesList()
    {
        return [
            'Country' => AddressObject::COUNTRY_TYPE,
            'State' => AddressObject::STATE_TYPE,
            'County' => AddressObject::COUNTY_TYPE,
            'City' => AddressObject::CITY_TYPE
        ];
    }
}