<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Provider;

use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Referral\Entity\ReferralPurchase;
use User\Entity\User;

class ReferralProvider extends BaseProvider
{
    /**
     * @var \DateTime[]
     */
    private $date;

    /**
     * @var string
     */
    private $status;

    /**
     * @var float[]
     */
    private $amount;

    /**
     * @var User
     */
    private $user;

    public function search(): PaginationInterface
    {
        $builder = $this->em->getRepository(ReferralPurchase::class)->createQueryBuilder('rp');
        $builder->orderBy('rp.createdAt', 'DESC');

        if (isset($this->date['begin'])) {
            $builder
                ->andWhere('rp.purchaseDate >= :begin')
                ->setParameter('begin', $this->date['begin']);
        }

        if (isset($this->date['end'])) {
            $builder
                ->andWhere('rp.purchaseDate <= :end')
                ->setParameter('end', $this->date['end']);
        }

        if ($this->status) {
            $builder
                ->andWhere('rp.status = :status')
                ->setParameter('status', $this->status);
        }

        if ($this->user) {
            $builder
                ->innerJoin('rp.referralProgram', 'p')
                ->andWhere('p.user = :user')
                ->setParameter('user', $this->user);
        }

        if (isset($this->amount['min'])) {
            $builder
                ->andWhere('rp.amount >= :amount_min')
                ->setParameter('amount_min', $this->amount['min']);
        }

        if (isset($this->amount['max'])) {
            $builder
                ->andWhere('rp.amount <= :amount_max')
                ->setParameter('amount_max', $this->amount['max']);
        }

        return $this->buildPaginationResult($builder->getQuery());
    }

    /**
     * @return \DateTime[]
     */
    public function getDate(): ?array
    {
        return $this->date;
    }

    /**
     * @param \DateTime[] $date
     *
     * @return ReferralProvider
     */
    public function setDate(array $date): ReferralProvider
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return ReferralProvider
     */
    public function setStatus(string $status): ReferralProvider
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return float[]|null
     */
    public function getAmount(): ?array
    {
        return $this->amount;
    }

    /**
     * @param float[] $amount
     *
     * @return ReferralProvider
     */
    public function setAmount(array $amount): ReferralProvider
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return ReferralProvider
     */
    public function setUser(User $user): ReferralProvider
    {
        $this->user = $user;
        return $this;
    }
}