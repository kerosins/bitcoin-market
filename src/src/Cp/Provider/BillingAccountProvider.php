<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Provider;

use Billing\Entity\Account;
use Doctrine\ORM\Query\Expr\Join;
use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;
use User\Entity\User;

class BillingAccountProvider extends BaseProvider
{
    /**
     * @var User
     */
    private $user;

    public function search(): PaginationInterface
    {
        $builder = $this->em->getRepository(Account::class)->createQueryBuilder('a');
        $builder->orderBy('a.createdAt', 'ASC');

        if ($this->user) {
            $builder->andWhere('a.id = :account')->setParameter('account', $this->user->getBillingAccount());
        }

        return $this->buildPaginationResult($builder->getQuery());
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     *
     * @return BillingAccountProvider
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return array
     */
    public function getBalance(): ?array
    {
        return $this->balance;
    }

    /**
     * @param array $balance
     *
     * @return BillingAccountProvider
     */
    public function setBalance(array $balance = null): BillingAccountProvider
    {
        $this->balance = $balance;
        return $this;
    }
}