<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Provider;

use Doctrine\Common\Persistence\ObjectManager;
use Elastic\Paginator\ExtRawPaginatorAdapter;
use Elastica\Query;
use Elastica\SearchableInterface;
use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ElasticProductsProvider
 * @package Cp\Provider
 *
 * todo curl -XPUT "http://localhost:9200/catalog/_settings" -d '{ "index" : { "max_result_window" : 1000000 } }'
 */
class ElasticProductsProvider extends BaseProvider
{
    private $id;

    private $title;

    private $productIndex;

    public function __construct(
        ObjectManager $entityManager,
        PaginatorInterface $paginator,
        RequestStack $requestStack,
        SearchableInterface $productIndex
    ) {
        parent::__construct($entityManager, $paginator, $requestStack);
        $this->productIndex = $productIndex;
    }

    public function search(): PaginationInterface
    {
        $query = new Query();
        $bool = new Query\BoolQuery();

        $query->setQuery($bool);

        if ($this->id) {
            $term = new Query\Term();
            $term->setTerm('id', $this->id);
            $bool->addMust($term);
        }

        if ($this->title) {
            $match = new Query\Match();
            $match->setField('title', $this->title);
            $bool->addMust($match);
        }

        $rawPaginatorAdapter = new ExtRawPaginatorAdapter($this->productIndex, $query, []);
        return $this->buildPaginationResult($rawPaginatorAdapter);
    }
}