<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Provider;


use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Referral\Entity\ReferralProgram;
use User\Entity\User;

class ReferralProgramProvider extends BaseProvider
{
    /**
     * @var User
     */
    private $user;

    public function search(): PaginationInterface
    {
        $builder = $this->em->getRepository(ReferralProgram::class)->createQueryBuilder('rp');
        $builder->orderBy('rp.createdAt', 'DESC');

        if ($this->user) {
            $builder->andWhere('rp.user = :user')->setParameter('user', $this->user);
        }

        return $this->buildPaginationResult($builder->getQuery());
    }
}