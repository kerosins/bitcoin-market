<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Provider;

use Billing\Entity\Transaction;
use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;
use User\Entity\User;

class BillingTransactionProvider extends BaseProvider
{
    /**
     * @var string
     */
    private $transaction;

    /**
     * dirty hack for accounts without user
     *
     * @var int
     */
    private $account;

    /**
     * @var User
     */
    private $fromAccount;

    /**
     * @var User
     */
    private $toAccount;

    /**
     * @var array
     */
    private $amount;

    /**
     * @var \DateTime[]
     */
    private $date;

    /**
     * @var int
     */
    private $status;

    public function search(): PaginationInterface
    {
        $builder = $this->em->getRepository(Transaction::class)->createQueryBuilder('t');
        $builder->orderBy('t.createdAt', 'DESC');

        if ($this->transaction) {
            $builder
                ->andWhere('LOWER(t.id) LIKE :transactionId')
                ->setParameter('transactionId', '%' . strtolower($this->transaction) . '%');
        }

        //from|to
        if (
            (
                $this->fromAccount &&
                $this->toAccount &&
                $this->fromAccount instanceof User &&
                $this->fromAccount->getId() === $this->toAccount->getId()
            ) ||
            $this->account
        ) {
            $id = $this->account ?? $this->fromAccount->getBillingAccount()->getId();
            $builder->andWhere('t.fromAccount = :account OR t.toAccount = :account')->setParameter('account', $id);
        } else {
            if ($this->fromAccount) {
                $builder
                    ->andWhere('t.fromAccount = :fromAccount')
                    ->setParameter('fromAccount', $this->fromAccount->getBillingAccount()->getId());
            }

            if ($this->toAccount) {
                $builder
                    ->andWhere('t.toAccount = :toAccount')
                    ->setParameter('toAccount', $this->fromAccount->getBillingAccount()->getId());
            }
        }

        if ($this->amount) {
            if (isset($this->amount['min'])) {
                $builder->andWhere('t.amount >= :amountMin')->setParameter('amountMin', $this->amount['min']);
            }

            if (isset($this->amount['max'])) {
                $builder->andWhere('t.amount <= :amountMax')->setParameter('amountMax', $this->amount['max']);
            }
        }

        if ($this->date) {
            if (isset($this->date['begin']) && $this->date['begin'] !== false) {
                $builder
                    ->andWhere('t.createdAt >= :dateBegin')
                    ->setParameter('dateBegin', $this->date['begin']->format('Y-m-d 00:00:00'));
            }

            if (isset($this->date['end']) && $this->date['end'] !== false) {
                $builder
                    ->andWhere('t.createdAt <= :dateEnd')
                    ->setParameter('dateEnd', $this->date['end']->format('Y-m-d 23:59:59'));
            }
        }

        if ($this->status) {
            $builder
                ->andWhere('t.status IN (:status)')
                ->setParameter('status', $this->status);
        }

        return $this->buildPaginationResult($builder->getQuery());
    }

    /**
     * @return mixed
     */
    public function getFromAccount()
    {
        return $this->fromAccount;
    }

    /**
     * @param mixed $fromAccount
     */
    public function setFromAccount($fromAccount): void
    {
        $this->fromAccount = $fromAccount;
    }

    /**
     * @return mixed
     */
    public function getToAccount()
    {
        return $this->toAccount;
    }

    /**
     * @param mixed $toAccount
     */
    public function setToAccount($toAccount): void
    {
        $this->toAccount = $toAccount;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param mixed $transaction
     *
     * @return BillingTransactionProvider
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param mixed $account
     *
     * @return BillingTransactionProvider
     */
    public function setAccount($account)
    {
        $this->account = $account;
        return $this;
    }


}