<?php
/**
 * @author Kondaurov
 */

namespace Cp\Provider;

use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Referral\Entity\ReferralWithdrawal;

class WithdrawalProvider extends BaseProvider
{
    /**
     * @var int
     */
    private $status;

    public function search(): PaginationInterface
    {
        $builder = $this->em->getRepository(ReferralWithdrawal::class)->createQueryBuilder('w');
        $builder->orderBy('w.createdAt', 'DESC');

        if ($this->status) {
            $builder->andWhere('w.status = :status')->setParameter('status', $this->status);
        }

        return $this->buildPaginationResult($builder->getQuery());
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return WithdrawalProvider
     */
    public function setStatus(int $status): WithdrawalProvider
    {
        $this->status = $status;
        return $this;
    }
}