<?php
/**
 * @author Kondaurov
 */

namespace Cp\Provider;

use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\Product;
use Kerosin\Doctrine\ORM\Doctrine\BaseProvider;
use Knp\Component\Pager\Pagination\PaginationInterface;

class ProductProvider extends BaseProvider
{
    /**
     * @var int
     */
    private $productId;

    /**
     * @var Category
     */
    private $category;

    /**
     * @var string
     */
    private $title;

    /**
     * @var array
     */
    private $price = [];

    /**
     * @return PaginationInterface
     */
    public function search(): PaginationInterface
    {
        $builder = $this->em->getRepository(Product::class)->createQueryBuilder('p');

        if ($this->productId) {
            $builder->andWhere('p.id = :id')
                ->setParameter('id', (int)$this->productId);
        }

        if ($this->title) {
            $builder
                ->andWhere('lower(p.title) LIKE lower(:title)')
                ->setParameter('title', "%{$this->title}%")
            ;
        }

        if ($this->price) {
            $builder->innerJoin('p.price', 'pp');
            if (!empty($this->price['min'])) {
                $builder
                    ->andWhere('pp.value >= :valueMin')
                    ->setParameter('valueMin', $this->price['min']);
            }

            if (!empty($this->price['max'])) {
                $builder
                    ->andWhere('pp.value <= :valueMax')
                    ->setParameter('valueMax', $this->price['max']);
            }
        }

        if ($this->category) {
            $builder
                ->innerJoin('p.categories', 'category')
                ->andWhere('category = :category')
                ->setParameter('category', $this->category);
        }

        return $this->buildPaginationResult($builder->getQuery());
    }

    /**
     * @return Category
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     *
     * @return ProductProvider
     */
    public function setCategory(Category $category = null): ProductProvider
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return ProductProvider
     */
    public function setTitle(string $title = ''): ProductProvider
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return array
     */
    public function getPrice(): ?array
    {
        return $this->price;
    }

    /**
     * @param array $price
     *
     * @return ProductProvider
     */
    public function setPrice(array $price = []): ProductProvider
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getProductId(): ?int
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     *
     * @return ProductProvider
     */
    public function setProductId(int $productId): ProductProvider
    {
        $this->productId = $productId;
        return $this;
    }
}