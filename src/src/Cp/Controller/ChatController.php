<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Controller;

use Cp\Form\DisputeMessageType;
use Cp\Form\MessageType;
use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Controller\BaseController;
use Catalog\ORM\Entity\Dispute;
use Catalog\ORM\Entity\Message\BaseMessage;
use Catalog\ORM\Entity\Message\OrderMessage;
use Catalog\ORM\Entity\Order;
use Catalog\Order\Service\DisputeManager;
use Catalog\Order\Service\OrderManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides chat to order and dispute in Control Panel
 *
 * Class ChatController
 * @package Cp\Controller
 */
class ChatController extends BaseController
{
    /**
     * Provides chat
     *
     * @param EntityManagerInterface $entityManager
     * @param OrderManager $orderManager
     * @param DisputeManager $disputeManager
     * @param string $type
     * @param int $target
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function renderAction(
        EntityManagerInterface $entityManager,
        OrderManager $orderManager,
        DisputeManager $disputeManager,
        string $type,
        int $target
    ) {
        $viewVars = [
            'allowChat' => false,
            'type' => $type
        ];
        $mainEntity = null;

        switch ($type) {
            case BaseMessage::TYPE_ORDER:
                $mainEntity = $entityManager->find(Order::class, $target);
                $viewVars['allowChat'] = $orderManager->isAllowChat($mainEntity);
                break;
            case BaseMessage::TYPE_DISPUTE:
                $mainEntity = $entityManager->find(Dispute::class, $target);
                $viewVars['allowChat'] = $disputeManager->isAllowChat($mainEntity);
                break;
        }

        try {
            $this->buildChatForm($type, $viewVars);
        } catch (\Exception $e) {
            return $this->jsonException($e);
        }

        $viewVars['chat'] = $mainEntity->getMessages();

        return $this->json([
            'chat' => $this->render('@Cp/chat/list.part.twig', $viewVars)
        ]);
    }

    /**
     * Adds message to chat
     *
     * @param string $type
     * @param int $target
     */
    public function addAction(Request $request, string $type, int $target)
    {
        $this->checkIfXHR();

        if (BaseMessage::TYPE_ORDER === $type) {
            $form = $this->createForm(MessageType::class, new OrderMessage());
        } elseif (BaseMessage::TYPE_DISPUTE === $type) {
            $form = $this->createForm(DisputeMessageType::class);
        } else {
            return $this->jsonException(new BadRequestHttpException('Unknown type'));
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //todo implement save
            return $this->json([], 201);
        } else {
            return $this->json([
                'errors' => $this->convertFormErrorsToArray($form)
            ], 400);
        }
    }

    private function buildChatForm($type, &$viewVars)
    {
        if (!$viewVars['allowChat']) {
            $viewVars['chatForm'] = null;
            return;
        }

        if (BaseMessage::TYPE_ORDER === $type) {
            $form = $this->createForm(MessageType::class, new OrderMessage());
        } elseif (BaseMessage::TYPE_DISPUTE === $type) {
            $form = $this->createForm(DisputeMessageType::class);
        } else {
            throw new BadRequestHttpException();
        }

        $viewVars['chatForm'] = $form;
    }
}