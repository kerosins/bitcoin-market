<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Controller;


use Cp\Form\StaticPageForm;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Kerosin\Controller\BaseController;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Content\ORM\Entity\Slug;
use Content\ORM\Entity\StaticPage;
use Content\SlugRoute\Service\SlugCreator;
use Content\Service\AliasRouteManager;
use Symfony\Component\HttpFoundation\Request;

class StaticPageController extends BaseController
{
    /**
     * @param ObjectManager $entityManager
     * @param Paginator $paginator
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request)
    {
        $orderQuery = $entityManager->getRepository(StaticPage::class)->createQueryBuilder('sp')->getQuery();

        $paginated = $paginator->paginate(
            $orderQuery,
            $request->get('page', 1)
        );
        $items = $paginated->getItems();

        //fucking manual hydration for aliases
        $ids = array_map(function (StaticPage $page) {
            return $page->getId();
        }, $items);

        $aliases = $entityManager->getRepository(Slug::class)->findBy([
            'targetId' => $ids,
            'target' => Slug::TARGET_STATIC_PAGE
        ], ['weight' => 'DESC']);

        $items = array_map(function (StaticPage $page) use ($aliases) {
            $page->setSlugs(array_filter($aliases, function (Slug $alias) use ($page) {
                return $alias->getTargetId() === $page->getId();
            }));

            return $page;
        }, $items);

        $paginated->setItems($items);

        return $this->render('@Cp/static-page/index.twig', [
            'pages' => $paginated
        ]);
    }


    /**
     * @param EntityManager $entityManager
     * @param Request $request
     * @param \Content\ORM\Entity\StaticPage|null $staticPage
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createEditAction(
        EntityManagerInterface $entityManager,
        Request $request,
        SlugCreator $slugCreator,
        StaticPage $staticPage = null
    ) {
        $isCreate = false;

        $slugRepository = $entityManager->getRepository(Slug::class);
        if (!$staticPage) {
            $isCreate = true;
            $staticPage = new StaticPage();
        } else {
            //found aliases
            $slugs = $slugRepository->findByTargetObject($staticPage);
            $staticPage->setSlugs($slugs);
        }

        $form = $this->createForm(StaticPageForm::class, $staticPage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->flushEntities($staticPage);
            $slugRepository->saveCollection($staticPage, $staticPage->getSlugs());
            $slugCreator->generateIfNotExists($staticPage);

            return $this->redirectToRoute('cp.static_page.index');
        }

        return $this->render('@Cp/static-page/form.twig', [
            'staticPageForm' => $form->createView(),
            'isCreate' => $isCreate
        ]);
    }

    public function deleteAction(EntityManagerInterface $entityManager, StaticPage $staticPage)
    {
        $entityManager->remove($staticPage);
        $entityManager->flush();

        return $this->redirectToRoute('cp.static_page.index');
    }
}