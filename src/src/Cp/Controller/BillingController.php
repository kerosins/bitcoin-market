<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Controller;


use Billing\Entity\Account;
use Billing\Service\AccountManager;
use Cp\Form\Filter\BillingAccountFilter;
use Cp\Form\Filter\BillingTransactionFilter;
use Cp\Provider\BillingAccountProvider;
use Cp\Provider\BillingTransactionProvider;
use Kerosin\Component\AutocompleteTransformer;
use Kerosin\Controller\BaseController;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use User\Entity\User;

class BillingController extends BaseController
{
    public function accountAction(BillingAccountProvider $provider, AccountManager $accountManager, Request $request)
    {
        $filter = $this->createForm(BillingAccountFilter::class, $provider);
        $filter->handleRequest($request);

        $userRepository = $this->getEm()->getRepository(User::class);
        $provider->setResultTransformer(function (Account $account) use ($userRepository, $accountManager) {
             $account->setUser($userRepository->findOneBy(['billingAccount' => $account]));
             $account->setBalance($accountManager->balance($account));
             return $account;
        });

        return $this->render('@Cp/billing/accounts.twig', [
            'accounts' => $provider->search(),
            'filter' => $filter->createView()
        ]);
    }

    public function transactionAction(BillingTransactionProvider $provider, Request $request)
    {
        $filter = $this->createForm(BillingTransactionFilter::class, $provider);
        $filter->handleRequest($request);

        return $this->render('@Cp/billing/transactions.twig', [
            'transactions' => $provider->search(),
            'filter' => $filter->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param PaginatorInterface $paginator
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function accountAutocompleteAction(Request $request, PaginatorInterface $paginator)
    {
        $builder = $this->getEm()->getRepository(User::class)->createQueryBuilder('u');
        $builder->andWhere('u.billingAccount IS NOT NULL');

        $q = $request->get('q');
        if ($q) {
            $builder
                ->andWhere('LOWER(u.username) LIKE :username')
                ->setParameter('username', strtolower($q));
        }

        return $this->json(
            AutocompleteTransformer::fromPaginatedResult(
                $paginator->paginate(
                    $builder->getQuery(),
                    $request->get('page', 1)
                ),
                function (User $user) {
                    return $user->getBillingAccount()->getId();
                },
                'username'
            )
        );
    }
}