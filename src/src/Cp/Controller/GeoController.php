<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Controller;

use Cp\Form\CountryType;
use Kerosin\Controller\BaseController;
use Shop\Entity\Country;

class GeoController extends BaseController
{
    public function countryListAction()
    {
        $query = $this->getEm()
            ->getRepository('ShopBundle:Country')
            ->createQueryBuilder('c')
            ->getQuery();

        return $this->render('@Cp/geo/country/list.twig', [
            'countries' => $this->paginate($query)
        ]);
    }

    public function countryEditAction(Country $country = null)
    {
        $isCreate = ($country === null);

        if (!$country) {
            $country = new Country();
        }

        $form = $this->createForm(CountryType::class, $country);
        $form->handleRequest($this->getRequest());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getEm()->persist($country);
            $this->getEm()->flush($country);

            return $this->redirectToRoute('cp.geo.country.index');
        }

        return $this->render('@Cp/geo/country/form.twig', [
            'isCreate' => $isCreate,
            'form' => $form->createView()
        ]);
    }

    public function countryDeleteAction(Country $country)
    {
        $this->getEm()->remove($country);
        $this->getEm()->flush();
        return $this->redirectToRoute('cp.geo.country.index');
    }
}