<?php
/**
 * CategoryController class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Cp\Controller;

use Cp\Form\CategoryType;
use Cp\Form\FacetCategoryType;
use Cp\Form\MappingCategoryAliasType;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Controller\BaseController;
use Content\ORM\Entity\Slug;
use Content\SlugRoute\Service\SlugCreator;
use Content\Service\AliasRouteManager;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\FacetCategory;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends BaseController
{
    public function indexAction(ObjectManager $em, $category = null)
    {
        $this->addBreadCrumb('Categories', $this->generateUrl('cp.catalog.category.index'));

        $entityRepository = $em->getRepository(\Catalog\ORM\Entity\Category::class);
        $categories = $entityRepository->findCategories($category);

        $context = [
            'categories' => $categories
        ];

        if ($category) {
            $current = $entityRepository->findAllParents($category);
            $context['parents'] = $current;
        }

        return $this->render('@Cp/category/index.twig', $context);
    }

    /**
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param \Content\SlugRoute\Service\SlugCreator $slugCreator
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createEditAction(
        EntityManagerInterface $em,
        SlugCreator $slugCreator,
        Request $request,
        $id = null
    ) {
        $this->addBreadCrumbsFromArray([
            ['Categories', $this->generateUrl('cp.catalog.category.index')],
            [($id ? 'Update' : 'Create'), '#']
        ]);

        $isCreate = false;
        $slugRepository = $em->getRepository(Slug::class);
        if ($id) {
            $category = $em->getRepository(\Catalog\ORM\Entity\Category::class)->find($id);
            if (!$category) {
                throw new NotFoundHttpException();
            }

            $oldAliases = $slugRepository->findByTargetObject($category);
            $category->setSlugs($oldAliases);
        } else {
            $isCreate = true;
            $category = new Category();

            $predefine = $request->get('predefine');
            if ($predefine) {
                $category->setParent($em->getRepository(\Catalog\ORM\Entity\Category::class)->find($predefine));
            }
        }

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($category);
            $em->flush();

            $slugRepository->saveCollection($category, $category->getSlugs());
            $slugCreator->generateIfNotExists($category);

            $parent = null;
            if ($category->getParent()) {
                $parent = $category->getParent()->getId();
            }

            return $this->redirectToRoute('cp.catalog.category.index', ['category' => $parent]);
        }

        //create a form for the Mapping Tab
        $aliasesEntity = $category->getMappingAlias();
        $aliasesForm = $this->createForm(MappingCategoryAliasType::class, $aliasesEntity, [
            'action' => $this->generateUrl('cp.catalog.category.addMappingAlias', [
                'category' => $category->getId()
            ])
        ]);

        return $this->render('@Cp/category/createEdit.twig', [
            'form' => $form->createView(),
            'category' => $category,
            'aliasesForm' => $aliasesForm->createView(),
            'isCreate' => $isCreate
        ]);
    }

    public function deleteAction(ObjectManager $entityManager, $id)
    {
        $category = $entityManager->getRepository(\Catalog\ORM\Entity\Category::class)->find($id);
        $entityManager->remove($category);
        $entityManager->flush();

        return $this->redirectToRoute('cp.catalog.category.index', ['category' => $category->getParentId()]);
    }

    public function facetAction(ObjectManager $entityManager, Request $request, $id)
    {
        $category = $entityManager->getRepository(\Catalog\ORM\Entity\Category::class)->find($id);
        if (!$category) {
            throw new NotFoundHttpException('Category not found');
        }

        $form = $this->createFormBuilder($category, ['data_class' => Category::class]);
        $form->add('facets', CollectionType::class, [
            'label' => false,
            'entry_type' => FacetCategoryType::class,
            'allow_add' => true,
            'allow_delete' => true,
        ]);

        $removalFacets = [];
        foreach ($category->getFacets() as $facet) {
            $removalFacets[] = $facet;
        }

        $form = $form->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //remove
            $facets = $category->getFacets();

            $removalFacets = array_filter(
                $removalFacets,
                function (FacetCategory $facetCategory) use ($facets) {
                    foreach ($facets as $facet) {
                        if ($facet->getId() == $facetCategory->getId()) {
                            return false;
                        }
                    }

                    return true;
                }
            );

            foreach ($removalFacets as $facet) {
                /** @var \Catalog\ORM\Entity\FacetCategory $facet */
                $facet->removeCategory($category);
                if (count($facet->getCategories()) == 0) {
                    $entityManager->remove($facet);
                } else {
                    $entityManager->persist($facet);
                }
            }

            //save
            foreach ($facets as $facet) {
                /** @var \Catalog\ORM\Entity\FacetCategory $facet */
                $facet->addCategory($category);
                $entityManager->persist($facet);
            }

            $entityManager->persist($category);
            $entityManager->flush();

            return $this->redirectToRoute(
                'cp.catalog.category.index',
                ['category' => $category->getParent() ? $category->getParent()->getId() : null]
            );
        }

        return $this->render('@Cp/category/facet_form.twig', [
            'category' => $category,
            'form' => $form->createView()
        ]);
    }

    public function assemblyViewAction(ObjectManager $manager)
    {
        return $this->render('@Cp/category/assembly_view.twig', [
            'categories' => $manager->getRepository(\Catalog\ORM\Entity\Category::class)->findBy(
                [ 'parent' => null ],
                [ 'order' => 'DESC' ]
            )
        ]);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function assemblyAction($id)
    {
        $this->container->get(\Cp\Service\AssemblyFacet::class)->assembly($id);

        $response = new JsonResponse();
        $response->headers->set('Content-Type', 'application/json');
        $response->setData(['success' => true]);

        return $response;
    }

    /**
     * Update mapping alias action
     *
     * @param \Catalog\ORM\Entity\Category $category
     *
     * @return JsonResponse
     */
    public function addMappingAliasAction(Category $category = null)
    {
        $this->checkIfXHR();

        $alias = $category->getMappingAlias();
        $form = $this->createForm(MappingCategoryAliasType::class, $alias);
        $form->handleRequest($this->getRequest());

        $response = [];

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getEm()->persist($alias);
            $this->getEm()->flush();

            $response = [
                'success' => true,
                'message' => 'Aliases has been saved'
            ];
        } elseif ($form->getErrors(true)) {
            $errors = $form->getErrors(true);

            $response = [
                'success' => false,
                'message' => (string)$errors //todo fix it
            ];
        }

        return $this->json($response);
    }

    public function toggleEnableAction(EntityManagerInterface $entityManager, Request $request, Category $category)
    {
        $category->setEnabled(!$category->getEnabled());
        $entityManager->persist($category);
        $entityManager->flush();

        return $this->redirect($request->headers->get('referer'));
    }
}