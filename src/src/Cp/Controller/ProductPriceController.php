<?php
/**
 * @author kerosin
 */

namespace Cp\Controller;


use Catalog\ORM\Entity\Product;
use Cp\Form\ProductFilter;
use Cp\Form\ProductPriceType;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Kerosin\Controller\BaseController;
use Catalog\ORM\Entity\ProductPrice;
use Shop\Provider\ProductProvider;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class ProductPriceController extends BaseController
{

    public function indexAction(Request $request)
    {
        $provider = $this->container->get('cp.provider.product_provider');
        $filter = $this->createForm(ProductFilter::class, $provider);
        $filter->handleRequest($request);

        return $this->render('@Cp/product/price/index.twig', [
            'products' => $provider->search(),
            'filter' => $filter->createView()
        ]);
    }

    public function createEditAction(ObjectManager $manager, Request $request, $id = null, $product = null)
    {
        $this->checkIfXHR();

        if ($id) {
            $entity = $manager->getRepository(ProductPrice::class)->find($id);
        } else {
            $entity = new ProductPrice();
            $entity->setProduct(
                $manager->getRepository(Product::class)->find($product)
            );
        }

        $form = $this->createForm(ProductPriceType::class, $entity);
        $form->handleRequest($request);

        $statusCode = 400;//is bad request
        $formOpen = false;

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $manager->persist($entity);
                $manager->flush();
                $statusCode = 200;
            } else {
                $formOpen = true;
            }
        }

        return $this->json([
            'row' => $this->renderView('@Cp/product/price/row.twig', [
                'product' => $entity->getProduct(),
                'form' => $form->createView(),
                'formOpen' => $formOpen
            ])
        ], $statusCode);
    }

    public function deleteAction(ObjectManager $manager, $id)
    {
        $price = $manager->getRepository(ProductPrice::class)->find($id);

        if (!$price) {
            throw new Exception('Price of product not found');
        }

        $manager->remove($price);
        $manager->flush();

        return $this->redirectToRoute('cp.catalog.product.price');
    }

}