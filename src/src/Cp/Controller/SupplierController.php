<?php
/**
 * @author kerosin
 */

namespace Cp\Controller;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Kerosin\Controller\BaseController;
use Shop\Entity\Seller;
use Shop\Form\SupplierType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SupplierController extends BaseController
{
    public function indexAction(ObjectManager $em)
    {
        $this->addBreadCrumb('Sellers', $this->generateUrl('cp.supplier.index'));

        $suppliers = $em->getRepository(Seller::class)->findAll();
        return $this->render('@Cp/supplier/index.twig', [
            'suppliers' => $suppliers
        ]);
    }

    public function createUpdateAction(ObjectManager $em, $id = null)
    {
        $this->addBreadCrumbsFromArray([
            ['Sellers', $this->generateUrl('cp.supplier.index')],
            [($id ? 'Update' : 'Create'), '#']
        ]);

        if ($id) {
            $supplier = $em->getRepository(Seller::class)->find($id);
            if (!$supplier) {
                throw new NotFoundHttpException();
            }
        } else {
            $supplier = new Seller();
        }

        $form = $this->createForm(SupplierType::class, $supplier);
        $form->handleRequest($this->getRequest());

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($supplier);
            $em->flush();

            return $this->redirectToRoute('cp.supplier.index');
        }

        return $this->render('@Cp/supplier/createUpdate.twig', [
            'isCreate' => !(bool)$id,
            'form' => $form->createView()
        ]);
    }

    public function removeAction(ObjectManager $em, $id)
    {
        $supplier = $em->getRepository(Seller::class)->find($id);
        if (!$supplier) {
            throw new NotFoundHttpException();
        }

        $em->remove($supplier);
        $em->flush();

        return $this->redirectToRoute('cp.supplier.index');
    }
}