<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Controller;

use Cp\Provider\ElasticProductsProvider;
use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Controller\BaseController;
use Catalog\ORM\Entity\Product;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;

/**
 * Class ElasticController
 * @package Cp\Controller
 *
 * todo implement
 */
class ElasticController extends BaseController
{
    public function indexAction(
        ElasticProductsProvider $elasticProductsProvider,
        EntityManagerInterface $entityManager
    ) {

        $cache = $this->get('app.cache');
        $countFromCache = $cache->getItem('count_products');

        if (($countProducts = $countFromCache->get()) === null) {
            $countProducts = $entityManager->getRepository(Product::class)->getCount();
            $countFromCache->set($countProducts);

            $cache->save($countFromCache);
            $countFromCache->expiresAfter(60 * 5);
        }

        return $this->render('@Cp/elastic/index.twig', [
            'elasticProducts' => $elasticProductsProvider->search(),
            'countProducts' => $countProducts
        ]);
    }
}