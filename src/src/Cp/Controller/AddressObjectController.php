<?php
/**
 * @author Kerosin
 */

namespace Cp\Controller;

use Cp\Form\AddressObjectFilterType;
use Cp\Form\AddressObjectType;
use Cp\Provider\AddressObjectProvider;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Kerosin\Controller\BaseController;
use Shop\Entity\AddressObject;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AddressObjectController extends BaseController
{
    public function indexAction(AddressObjectProvider $provider, Request $request)
    {
        $filter = $this->createForm(AddressObjectFilterType::class, $provider);
        $filter->handleRequest($request);

        return $this->render('@cp/address/index.twig', [
            'filter' => $filter->createView(),
            'addresses' => $provider->search()
        ]);
    }

    public function createEditAction(ObjectManager $entityManager, Request $request, $id = null)
    {
        if (!$id) {
            $entity = new AddressObject();
        } else {
            $entity = $entityManager->find('ShopBundle:AddressObject', $id);
        }

        $form = $this->createForm(AddressObjectType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirectToRoute('cp.address.index');
        }

        return $this->render('@cp/address/form.twig', [
            'form' => $form->createView(),
            'isEdit' => $id !== null
        ]);
    }

    public function autocompleteAction(AddressObjectProvider $provider, Request $request)
    {
        $type = $request->get('type');

        $provider->setType($type - 1)
                ->setTitle($request->get('q'))
        ;

        $addresses = $provider->search();

        $result = [];

        foreach ($addresses as $address) {
            $result[] = ['text' => $address->getTitle(), 'id' => $address->getId()];
        }

        return $this->json(['result' => $result]);
    }
}