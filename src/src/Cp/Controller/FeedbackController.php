<?php
/**
 * @author Kondaurov
 */

namespace Cp\Controller;


use Cp\Form\ReplyFeedbackType;
use Cp\Provider\FeedbackProvider;
use Kerosin\Controller\BaseController;
use Kerosin\Mailer\CustomMailer;
use Shop\Entity\Feedback\BaseFeedback;
use Symfony\Component\HttpFoundation\Request;

class FeedbackController extends BaseController
{
    public function indexAction(FeedbackProvider $feedbackProvider)
    {
        $this->addBreadCrumbsFromArray([
            ['Dashboard', $this->generateUrl('cp.home')],
            ['Feedback', '']
        ]);

        return $this->render('@Cp/feedback/index.twig', [
            'feedbackItems' => $feedbackProvider->search()
        ]);
    }

    /**
     * Displays feedback and provide form for write answer
     *
     * @param CustomMailer $customMailer
     * @param Request $request
     * @param BaseFeedback $feedback
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function replyAction(CustomMailer $customMailer, Request $request, BaseFeedback $feedback)
    {
        $enableFeedbackReply = !(bool)$feedback->getAnswer();

        if (!$feedback->getAnswerSubject()) {
            $feedback->setAnswerSubject('Re: ' . $feedback->getReadableType());
        }

        $replyForm = $this->createForm(
            ReplyFeedbackType::class,
            $feedback,
            [
                'disabled' => !$enableFeedbackReply
            ]
        );
        $replyForm->handleRequest($request);

        if ($replyForm->isSubmitted()
            && $replyForm->isValid()
            && $enableFeedbackReply
            && (bool)$feedback->getAnswer()
        ) {
            $customMailer->sendFeedbackReply($feedback);
            $this->flushEntities($feedback);

            return $this->redirectToRoute('cp.feedback.index');
        }

        return $this->render('@Cp/feedback/reply.twig', [
            'feedback' => $feedback,
            'enableForm' => $enableFeedbackReply,
            'replyForm' => $replyForm->createView()
        ]);
    }
}