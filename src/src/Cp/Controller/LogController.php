<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Controller;

use Cp\Form\Filter\LogFilter;
use Cp\Provider\LogProvider;
use Kerosin\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

class LogController extends BaseController
{
    public function indexAction(LogProvider $provider, Request $request)
    {
        $filter = $this->createForm(LogFilter::class, $provider);
        $filter->handleRequest($request);

        $logItems = $provider->search();

        return $this->render('@Cp/log/index.twig', [
            'logItems' => $logItems,
            'filter' => $filter->createView()
        ]);
    }
}