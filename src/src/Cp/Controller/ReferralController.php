<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Controller;

use Cp\Form\ProcessWithdrawalType;
use Cp\Form\Filter\ReferralPurchaseFilter;
use Cp\Form\Filter\WithdrawalFilter;
use Cp\Form\ReferralProgramSettingsType;
use Cp\Provider\ReferralProgramProvider;
use Cp\Provider\ReferralProvider;
use Cp\Provider\WithdrawalProvider;
use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Controller\BaseController;
use Referral\Entity\ReferralProgram;
use Referral\Entity\ReferralWithdrawal;
use Referral\Service\WithdrawalManager;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ReferralController extends BaseController
{
    public function indexAction()
    {
        return $this->render('@Cp/referral/index.twig');
    }

    /**
     * List of programs
     */
    public function programAction(ReferralProgramProvider $provider)
    {
        $this->addBreadCrumbsFromArray([
            ['Referrals', $this->generateUrl('cp.referral.index')],
            ['Programs', '#']
        ]);

        $pagination = $provider->search();
        return $this->render('@Cp/referral/programs.twig', [
            'programs' => $pagination
        ]);
    }

    /**
     * Edit settings of program
     */
    public function programSettingsEditAction(Request $request, ReferralProgram $program)
    {
        $settings = clone $program->getSettings();
        $form = $this->createForm(ReferralProgramSettingsType::class, $settings);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $program
                ->setSettings($settings)
                ->setUpdatedAt(new \DateTime()); //hack for update json

            $this->flushEntities($program);
            return $this->redirectToRoute('cp.referral.programs');
        }

        $this->addBreadCrumbsFromArray([
            ['Referrals', $this->generateUrl('cp.referral.index')],
            ['Programs', $this->generateUrl('cp.referral.programs')],
            ['Settings', $this->generateUrl('cp.referral.programs.settings_edit', ['program' => $program->getCode()])]
        ]);

        return $this->render('@Cp/referral/program_settings_edit.twig', [
            'settingsForm' => $form->createView(),
            'program' => $program
        ]);
    }

    /**
     * List of purchases
     *
     * @param ReferralProvider $provider
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function purchasesAction(ReferralProvider $provider, Request $request)
    {
        $filter = $this->createForm(ReferralPurchaseFilter::class, $provider);
        $filter->handleRequest($request);

        return $this->render('@Cp/referral/purchases.twig', [
            'purchases' => $provider->search(),
            'filter' => $filter->createView()
        ]);
    }

    /**
     * List of withdrawals
     *
     * @param WithdrawalProvider $provider
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function withdrawalsAction(WithdrawalProvider $provider, Request $request)
    {
        $filter = $this->createForm(WithdrawalFilter::class, $provider);
        $filter->handleRequest($request);

        return $this->render('@Cp/referral/withdrawals.twig', [
            'filter' => $filter->createView(),
            'withdrawals' => $provider->search()
        ]);
    }

    /**
     * @param WithdrawalManager $withdrawalManager
     * @param Request $request
     * @param ReferralWithdrawal $withdrawal
     */
    public function processWithdrawalAction(
        WithdrawalManager $withdrawalManager,
        Request $request,
        ReferralWithdrawal $withdrawal
    ) {
        $canProcess =  !in_array($withdrawal->getStatus(), [ReferralWithdrawal::CANCELED, ReferralWithdrawal::COMPLETE]);
        if ($canProcess) {
            $withdrawalManager->processWithdrawal($withdrawal);
        }

        $decisionForm = $this->createForm(ProcessWithdrawalType::class, ['withdrawal' => $withdrawal->getId()]);
        $decisionForm->handleRequest($request);

        if ($canProcess && $decisionForm->isSubmitted() && $decisionForm->isValid()) {
            $data = $decisionForm->getData();

            if (!empty($data['comment'])) {
                $withdrawal->setComment($data['comment']);
            }

            $decision = $data['decision'];

            if ($data['withdrawal'] != $withdrawal->getId()) {
                $decisionForm->addError(new FormError('Invalid withdrawal'));
            } else {
                $isSuccess = true;
                if ($decision == ReferralWithdrawal::COMPLETE) {
                    $isSuccess = $withdrawalManager->approveWithdrawal($withdrawal);
                } else if ($decision) {
                    $withdrawalManager->cancelWithdrawal($withdrawal);
                }

                if ($isSuccess) {
                    return $this->redirectToRoute('cp.referral.withdrawals');
                }

                $decisionForm->addError(new FormError('Internal error, try later'));
            }
        }

        return $this->render('@Cp/referral/withdrawal_detail.twig', [
            'canProcess' => !in_array($withdrawal->getStatus(), [ReferralWithdrawal::CANCELED, ReferralWithdrawal::COMPLETE]),
            'withdrawal' => $withdrawal,
            'decisionForm' => $decisionForm->createView()
        ]);
    }
}