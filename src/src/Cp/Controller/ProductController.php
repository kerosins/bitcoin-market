<?php
/**
 * ProductController class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Cp\Controller;

use Catalog\Import\AliExpress\Exception\RequestException;
use Catalog\Import\AliExpress\Product\AliExpressProductProvider;
use Catalog\Import\AliExpress\Product\SimpleSaver;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\FacetListValue;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Entity\Source;
use Content\ORM\Entity\Slug;
use Content\SlugRoute\Service\SlugCreator;
use Cp\Form\Filter\ProductFilter;
use Cp\Form\ImportProductForm;
use Cp\Form\ProductCopyType;
use Cp\Form\ProductType;
use Cp\Provider\ProductProvider;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Controller\BaseController;
use Kerosin\Filesystem\ImageHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductController extends BaseController
{
    /**
     * List of products
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(ProductProvider $provider, Request $request)
    {
        $this->addBreadCrumb('Products', $this->generateUrl('cp.catalog.product.index'));

        $form = $this->createForm(ProductFilter::class, $provider);
        $form->handleRequest($request);

        return $this->render('@Cp/product/index.twig', [
            'products' => $provider->search(),
            'filter' => $form->createView()
        ]);
    }

    /**
     * Edit product
     *
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @param \Content\SlugRoute\Service\SlugCreator $slugCreator
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createEditAction(
        EntityManagerInterface $entityManager,
        Request $request,
        SlugCreator $slugCreator,
        ImageHelper $loader,
        $id = null
    ) {
        $this->addBreadCrumbsFromArray([
            ['Products', $this->generateUrl('cp.catalog.product.index')],
            [$id ? 'Update' : 'Create', '#']
        ]);

        $repository = $entityManager->getRepository(Product::class);
        if (!$id) {
            $product = new Product();
        } else {
            $product = $repository->find($id);
            if (!$product) {
                throw new NotFoundHttpException();
            }

            $slugs = $entityManager->getRepository(Slug::class)->findByTargetObject($product);
            $product->setSlugs($slugs);
        }

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mainImage = $product->getMainImage();

            if ($mainImage) {
                $mainImage = $loader->saveImage($product->getMainImage(), ImageHelper::PRODUCT_TYPE);
                $product->setMainImage($mainImage);
            }

            $images = $product->getImages();
            if ($images) {
                $images = $loader->saveMany($product->getImages(), ImageHelper::PRODUCT_TYPE);
            }

            $product->setImages($images);
            foreach ($product->getParameters() as $parameter) {
                $entityManager->persist($parameter);
            }

            if (($price = $product->getPrice()) !== null) {
                $price->setProduct($product);
                $entityManager->persist($price);
            }

            //if insert set source manual
            if (!$product->getId()) {
                $product->setExternalSource(Source::EXTERNAL_SOURCE_ALIEXPRESS)
                    ->setSource(Source::MANUAL);
            }

            $entityManager->persist($product);
            $entityManager->flush();

            $entityManager->getRepository(Slug::class)->saveCollection($product, $product->getSlugs());
            $slugCreator->generateIfNotExists($product);

            return $this->redirectToRoute('cp.catalog.product.index');
        }

        return $this->render('@Cp/product/form.twig', [
            'formView' => $form->createView()]
        );
    }


    /**
     * remove product
     *
     * @param EntityManager $entityManager
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(ObjectManager $entityManager, $id)
    {
        $product = $entityManager->getRepository(Product::class)->find($id);
        if (!$product) {
            throw new NotFoundHttpException();
        }

        $product->setStatus(Product::STATUS_DELETED);
        $entityManager->flush();

        return $this->redirectToRoute('cp.catalog.product.index');
    }


    /**
     * Autocomplete parameter values
     *
     * @param EntityManager $entityManager
     * @param Request $request
     * @return JsonResponse
     */
    public function facetValueAction(ObjectManager $entityManager, Request $request)
    {
        $term = $request->get('q');
        $response = new JsonResponse();
        $response->headers->set('Content-Type', 'application/json');

        $repo = $entityManager->getRepository('ShopBundle:FacetListValue');
        $values = $repo->createQueryBuilder('fv')
            ->andWhere('fv.title LIKE :q')
            ->setParameter('q', "%{$term}%")
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;

        $values = array_map(function (FacetListValue $value) {
            return [
                'id' => $value->getId(),
                'text' => $value->getTitle()
            ];
        }, $values);

        array_unshift($values, ['id' => '-1', 'text' => 'Remove attribute']);

        $response->setData([
            'result' => $values
        ]);

        return $response;
    }

    /**
     * Copy Product
     *
     * @param EntityManager $em
     * @param Request $request
     * @param $id
     */
    public function copyAction(ObjectManager $em, Request $request, $id)
    {
        $product = $em->getRepository(Product::class)->find($id);

        if (!$product) {
            throw new NotFoundHttpException('Product not found');
        }

        $newProduct = $this->container->get(\Kerosin\Doctrine\ORM\CopyEntity::class)->copy($product);

        $form = $this->createForm(ProductCopyType::class, $newProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $parameters = $product->getParameters();
            foreach ($parameters as $parameter) {
                /** @var \Catalog\ORM\Entity\ProductParameter $parameter */
                $parameter = clone $parameter;
                $parameter->setProduct($newProduct);
                $newProduct->addParameter($parameter);
                $em->persist($parameter);
            }

            $em->persist($newProduct);
            $em->flush();

            return $this->redirectToRoute('cp.catalog.product.index');
        }

        return $this->render('@Cp/product/copy_form.twig', [
            'product' => $product,
            'form' => $form->createView()
        ]);
    }


    public function importAction(
        AliExpressProductProvider $aliExpressProductProvider,
        SimpleSaver $saver,
        Request $request
    ) {
        $this->addBreadCrumbsFromArray([
            ['Products', $this->generateUrl('cp.catalog.product.index')],
            ['Import Product', $this->generateUrl('cp.catalog.product.import')]
        ]);

        $form = $this->createForm(ImportProductForm::class);
        $form->handleRequest($request);
        $error = null;

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $aliProduct = $aliExpressProductProvider->getOne(
                    $form->getData()['source']
                );

                if ($aliProduct) {
                    $productId = $saver->saveOne($aliProduct);
                    return $this->redirectToRoute('cp.catalog.product.edit', ['id' => $productId]);
                } else {
                    $error = 'Unknown product';
                }
            } catch (RequestException $e) {
                $error = $e->getMessage();
            }
        }

        return $this->render('@Cp/product/import.twig', [
            'importForm' => $form->createView(),
            'error' => $error
        ]);
    }

//    public function importAction(
//        EntityManagerInterface $entityManager,
//        AliExpressProductProvider $productProvider,
//        SlugCreator $slugCreator,
//        Request $request
//    ) {
//        $this->addBreadCrumbsFromArray([
//            ['Products', $this->generateUrl('cp.catalog.product.index')],
//            ['Import Product', $this->generateUrl('cp.catalog.product.import')]
//        ]);
//
//        $form = $this->createForm(ImportProductForm::class);
//        $form->handleRequest($request);
//        $aliProduct = '';
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $data = $form->getData();
//            $id = $data['source'];
//
//            $aliProduct = $productProvider->provideFullInfo($id);
//
//            //creates product and relations
//            if ($form->get('saveProduct')->isClicked() || !$data['preview']) {
//                $entityManager->beginTransaction();
//                try {
//                    $title = $aliProduct['title'];
//                    $product = (new Product())
//                        ->setTitle($title)
//                        ->setDescription($aliProduct['description'])
//                        ->setShortDescription($aliProduct['shortDescription'])
//                        ->setMetaDescription($title)
//                        ->setMetaTitle($title)
//                        ->setMetaKeywords($title)
//                        ->setExternalLink($aliProduct['url'])
//                        ->setExternalSource(Source::EXTERNAL_SOURCE_ALIEXPRESS)
//                        ->setSource(Source::MANUAL);
//
//                    //main image
//                    if ($aliProduct['mainImage']) {
//                        $mainImage = new Image(
//                            null,
//                            $title,
//                            $title,
//                            $aliProduct['mainImage']
//                        );
//                        $product->setMainImage($mainImage);
//                    }
//
//                    $images = array_map(function ($url) use ($title) {
//                        return new Image(
//                            null,
//                            $title,
//                            $title,
//                            $url
//                        );
//                    }, $aliProduct['images']);
//                    $product->setImages($images);
//
//                    //create price
//                    $price = (new ProductPrice())
//                        ->setValue($aliProduct['price'])
//                        ->setScale($this->getParameter('catalog.price.scale'))
//                        ->setProduct($product);
//
//                    $this->flushEntities([
//                        $product,
//                        $price
//                    ]);
//                    $slugCreator->generate($product);
//
//                    $entityManager->commit();
//
//                    return $this->redirectToRoute('cp.catalog.product.edit', ['id' => $product->getId()]);
//                } catch (\Exception $e) {
//                    $entityManager->rollback();
//                }
//            }
//        }
//
//        $formData = $form->getData();
//
//        return $this->render('@Cp/product/import.twig', [
//            'valid' => $form->isValid(),
//            'importForm' => $form->createView(),
//            'aliProduct' => $aliProduct,
//            'formData' => $formData
//        ]);
//    }

    public function autocompleteAction(EntityManagerInterface $entityManager, Request $request, ImageHelper $imageHelper)
    {
        $productRepo = $entityManager->getRepository(Product::class);
        $term = $request->get('term');

        $products = $productRepo->findByLikeTitleOrId($term);
        $products = array_map(function (Product $product) use ($imageHelper) {
            return [
                'id' => $product->getId(),
                'title' => $product->getTitle(),
                'image' => $imageHelper->getPath($product->getMainImage()),
                'categories' => $product->getCategories()->map(function (Category $category) {
                    return ['title' => $category->getTitle()];
                })->toArray(),
                'price' => $product->getPrice()->getValue(),
                'publicPrice' => $product->getPublicPrice(),
            ];
        }, $products);

        return $this->json([
            'products' => $products
        ]);
    }
}