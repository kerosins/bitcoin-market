<?php
/**
 * @author kerosin
 */

namespace Cp\Controller;

use Catalog\Order\Event\OrderEvent;
use Catalog\Order\Event\OrderStatusEvent;
use Cp\Form\DisputeMessageType;
use Cp\Form\Filter\OrderFilterType;
use Cp\Form\OrderProductType;
use Cp\Form\OrderStatusType;
use Doctrine\ORM\EntityManager;
use Kerosin\Controller\BaseController;
use Kerosin\Mailer\CustomMailer;
use Catalog\ORM\Entity\Dispute;
use Catalog\ORM\Entity\Message\DisputeMessage;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderPosition;
use Catalog\ORM\Entity\OrderStatus;
use Catalog\Order\Service\DisputeManager;
use Catalog\Order\Service\OrderManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Manage orders
 *
 * Class OrderController
 * @package Cp\Controller
 */
class OrderController extends BaseController
{
    /**
     * List of orders action
     * @param \Catalog\Order\Service\OrderProvider $provider
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(\Catalog\Order\Service\OrderProvider $provider)
    {
        //preset default values
        /*$provider->setStatus([
            OrderStatus::PAYMENT_PROCESS,
            OrderStatus::PROCESS,
            OrderStatus::SHIPPING
        ]);*/
        $this->addBreadCrumb('Orders', $this->generateUrl('cp.order.index'));

        $filter = $this->createForm(OrderFilterType::class, $provider);
        $filter->handleRequest($this->getRequest());

        return $this->render('@Cp/order/index.twig', [
            'orders' => $provider->search(),
            'form' => $filter->createView()
        ]);
    }

    /**
     * Work with order
     *
     * @param $id
     * @param EntityManager $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function processAction(Order $order)
    {
        $this->addBreadCrumbsFromArray([
            ['Orders', $this->generateUrl('cp.order.index')],
            ['Process Order #' . $order->getId(), '#']
        ]);
        return $this->render('@Cp/order/process.twig', [
            'order' => $order
        ]);
    }

    /**
     * Cancel an order
     *
     * @param OrderManager $orderManager
     * @param Order $order
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function cancelAction(OrderManager $orderManager, Order $order)
    {
        $orderManager->updateStatus($order, OrderStatus::CANCEL);
        return $this->redirectToRoute('cp.order.index');
    }

    /**
     * Update a status action
     *
     * @param Order $order
     */
    public function updateStatusAction(
        OrderManager $orderManager,
        EventDispatcherInterface $eventDispatcher,
        Order $order
    ) {
        $this->checkIfXHR();

        $form = $this->createForm(OrderStatusType::class, $order);
        $form->handleRequest($this->getRequest());

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $status = $order->getCurrentStatus();
                $status->setComment(
                    sprintf(
                        'Status manually changed by %s (%d)',
                        $this->getUser()->getUsername(),
                        $this->getUser()->getId()
                    )
                );

                $this->flushEntities([
                    $order,
                    $order->getCurrentStatus()
                ]);

                //sent event for send mails
                $statusEvent = new OrderStatusEvent();
                $statusEvent->setOrder($order)->setNewStatus($order->getCurrentStatus()->getType());
                $eventDispatcher->dispatch(OrderStatusEvent::EVENT_NAME, $statusEvent);

                //dispatch event for check referral purchase
                if ($order->getCurrentStatus()->getType() === OrderStatus::DONE) {
                    $doneEvent = new OrderEvent($order);
                    $eventDispatcher->dispatch(OrderEvent::ORDER_DONE, $doneEvent);
                }

                return $this->json([
                    'block' => $this->renderView('@Cp/order/overview_tab/statuses_block.twig', [
                        'order' => $order
                    ])
                ]);
            } else {
                return $this->json([
                    'form' => $this->renderView('@Cp/order/overview_tab/statuses_form.twig', [
                        'form' => $form->createView()
                    ])
                ],400);
            }
        }

        return $this->render('@Cp/order/overview_tab/statuses_form.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Update a comment of the Order or the Product of Order
     *
     * @param $id
     * @param string $type
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function updateCommentAction($id, $type = 'order')
    {
        $this->checkIfXHR();

        $message = $this->getRequest()->get('message');

        if ($message === null) {
            throw new BadRequestHttpException('Message is required');
        }

        /** @var Order|OrderPosition $entity */
        if ($type == 'product') {
            $entity = $this->getEm()->getRepository(OrderPosition::class)->find($id);
        } elseif ($type == 'order') {
            $entity = $this->getEm()->getRepository(Order::class)->find($id);
        } else {
            throw new BadRequestHttpException('Unknown type of comment');
        }

        if (!$entity) {
            throw new NotFoundHttpException("Entity #{$id} not found");
        }

        $entity->setComment($message);
        $this->getEm()->persist($entity);
        $this->getEm()->flush();

        return $this->json([
            'comment' => $entity->getComment()
        ]);
    }

    /**
     * @param OrderPosition $orderProduct
     */
    public function updateProductAction(OrderPosition $orderProduct)
    {
        $this->checkIfXHR();
        $request = $this->getRequest();

        $form = $this->createForm(OrderProductType::class, $orderProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->flushEntities($orderProduct);

                return $this->json([
                    'block' => $this->renderView('@Cp/order/cart_tab/product.twig', [
                        'item' => $orderProduct
                    ])
                ]);
            } else {
                return $this->json([
                    'form' => $this->renderView('@Cp/order/cart_tab/product_form.twig', [
                        'orderProduct' => $orderProduct,
                        'form' => $form->createView()
                    ])
                ], 400);
            }
        }

        return $this->render('@Cp/order/cart_tab/product_form.twig', [
            'orderProduct' => $orderProduct,
            'form' => $form->createView()
        ]);
    }

    /**
     * Disputes
     *
     * @param Request $request
     * @param DisputeManager $disputeManager
     * @param Dispute $dispute
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function disputeAction(
        Request $request,
        DisputeManager $disputeManager,
        CustomMailer $customMailer,
        Dispute $dispute
    ) {
        $messages = $dispute->getMessages()->toArray();
        usort($messages, function (DisputeMessage $a, DisputeMessage $b) {
            return $a->getCreatedAt() <> $b->getCreatedAt();
        });

        $message = new DisputeMessage();

        $form = $this->createForm(DisputeMessageType::class, ['message' => $message], ['dispute' => $dispute]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $userTo = $dispute->getOrderPosition()->getOrder()->getUser();
            $message
                ->setFrom($this->getUser())
                ->setFromDisplayName(
                    $dispute->getOrderPosition()->getProduct()->getSellerName()
                )
                ->setTo($userTo)
                ->setToDisplayName($userTo->getUsername())
            ;

            $dispute->addMessage($message);

            //check action
            //todo need move this logic to DisputeManager
            switch ($data['action']) {
                case Dispute::STATUS_DECLINE:
                    $dispute->setStatus(Dispute::STATUS_DECLINE);
                    $customMailer->declineDispute($dispute);
                    break;
                case Dispute::STATUS_PARTIAL_REFUND:
                    $dispute
                        ->setStatus(Dispute::STATUS_PARTIAL_REFUND)
                        ->setRefundAmount($data['amount'])
                        ->setIsProcessingRefund(true)
                    ;
                    break;
                case Dispute::STATUS_FULL_REFUND:
                    $dispute
                        ->setStatus(Dispute::STATUS_FULL_REFUND)
                        ->setRefundAmount($data['amount'])
                        ->setIsProcessingRefund(true)
                    ;
                    break;
                default:
                    $customMailer->newMessageDispute($dispute);
                    break;
            }

            $this->flushEntities([$dispute, $message]);
            //refreshes form
            return $this->redirectToRoute('cp.order.dispute', ['dispute' => $dispute->getId()]);
        }

        return $this->render('@Cp/order/dispute.twig', [
            'dispute' => $dispute,
            'messages' => $messages,
            'isAllowChat' => $disputeManager->isAllowChat($dispute),
            'disputeForm' => $form->createView()
        ]);
    }

    /**
     * @param Dispute $dispute
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function refundCompleteAction(CustomMailer $customMailer, Dispute $dispute)
    {
        if (!$dispute->getIsProcessingRefund()
            || !in_array($dispute->getStatus(), [Dispute::STATUS_FULL_REFUND, Dispute::STATUS_PARTIAL_REFUND])
        ) {
            throw new \RuntimeException();
        }

        $customMailer->orderRefund($dispute);

        $dispute->setIsProcessingRefund(false);
        $this->flushEntities($dispute);

        return $this->redirectToRoute('cp.order.dispute', ['dispute' => $dispute->getId()]);
    }
}