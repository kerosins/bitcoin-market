<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Controller\BaseController;
use Content\ORM\Entity\ItemBlock;
use Content\ItemBlock\ItemBlockForm;
use Content\ItemBlock\ItemBlockParameters;
use Knp\Component\Pager\PaginatorInterface;
use Catalog\ORM\Entity\Category;
use Symfony\Component\HttpFoundation\Request;

class ItemBlockController extends BaseController
{

    public function indexAction(EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request)
    {
        $this->addBreadCrumbsFromArray([
            ['Item Blocks', $this->generateUrl('cp.catalog.item_block.index')]
        ]);

        $itemBlocks = $entityManager->getRepository(ItemBlock::class)->createQueryBuilder('i')->getQuery();
        $itemBlocks = $paginator->paginate($itemBlocks, $request->get('page', 1));
        return $this->render('@Cp/item_block/index.twig', [
            'itemBlocks' => $itemBlocks
        ]);
    }


    public function createUpdateAction(EntityManagerInterface $entityManager, Request $request, ItemBlock $itemBlock = null)
    {
        $this->addBreadCrumbsFromArray([
            ['Item Blocks', $this->generateUrl('cp.catalog.item_block.index')],
            [(!$itemBlock ? 'Create' : 'Update'), '#']
        ]);

        if (!$itemBlock) {
            $itemBlock = new ItemBlock();
        }

        $checkIfXHR = $this->checkIfXHR(false);
        if ($checkIfXHR && ($type = $request->get('type')) !== null) {
            $itemBlock->setParams([]);
            $itemBlock->setType($type);
        }

        $form = $this->createForm(ItemBlockForm::class, $itemBlock);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $itemBlock = $form->getData();
            $params = $itemBlock->getParams();
            if ($params instanceof ItemBlockParameters) {
                $itemBlock->setParams($params->toArray());
            }

            $entityManager->persist($itemBlock);
            $entityManager->flush();

            return $this->redirectToRoute('cp.catalog.item_block.index');
        }

        if ($checkIfXHR) {
            return $this->json([
                'html' => $this->renderView('@Cp/item_block/form.part.twig', [
                    'itemBlockForm' => $form->createView()
                ])
            ]);
        }

        return $this->render('@Cp/item_block/form_wrap.twig', [
            'isCreate' => $itemBlock->getId() === null,
            'itemBlockForm' => $form->createView()
        ]);
    }

    public function deleteAction(EntityManagerInterface $entityManager, ItemBlock $itemBlock)
    {
        $entityManager->remove($itemBlock);
        $entityManager->flush();

        return $this->redirectToRoute('cp.catalog.item_block.index');
    }
}