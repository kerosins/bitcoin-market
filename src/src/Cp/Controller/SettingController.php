<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Controller;

use Cp\Form\SettingForm;
use Doctrine\ORM\EntityManagerInterface;
use Kerosin\Controller\BaseController;
use Kerosin\Doctrine\ORM\EntityRepository;
use Kerosin\Entity\SystemSetting;
use Kerosin\SystemSettings\SettingsManager;
use Kerosin\SystemSettings\SettingValueConverter;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SettingController extends BaseController
{

    public function indexAction(PaginatorInterface $paginator, Request $request)
    {
        /** @var EntityRepository $repository */
        $repository = $this->get('kerosin.system_setting.repository');
        $query = $repository->createQueryBuilder('ss')->getQuery();

        $paginated = $paginator->paginate($query, $request->get('page', 1), $request->get('per-page', 20));

        return $this->render('@Cp/setting/index.twig', [
            'settings' => $paginated
        ]);
    }

    public function updateAction(
        EntityManagerInterface $entityManager,
        Request $request,
        SettingsManager $sm
    ) {
        $this->checkIfXHR();

        $setting = $entityManager->find(SystemSetting::class, $request->get('setting'));
        if (!$setting) {
            return $this->jsonException(new NotFoundHttpException());
        }

        $settingForm = $this->createForm(SettingForm::class,
            [ 'value' => $setting->getValue() ],
            [
                'type' => $setting->getType(),
                'label' => $setting->getTitle()
            ]
        );
        $settingForm->handleRequest($request);

        if ($settingForm->isSubmitted() && $settingForm->isValid()) {
            $data = $settingForm->getData();
            $setting->setValue($data['value']);
            $sm->save($setting);

            return $this->json(null);
        }

        return $this->json([
            'html' => $this->renderView('@Cp/setting/form.part.twig', [
                'settingForm' => $settingForm->createView(),
                'currentSettingName' => $setting->getName()
            ])
        ]);
    }
}