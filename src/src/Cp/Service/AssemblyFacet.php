<?php
/**
 * @author Kerosin
 */

namespace Cp\Service;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Kerosin\Doctrine\ORM\BatchQueryProvider;
use function Kerosin\Helper\array_push_unique;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\FacetCategory;
use Catalog\ORM\Entity\FacetListValue;
use Catalog\ORM\Entity\ProductParameter;
use Catalog\ORM\Repository\AssemblyFacetRepository;

class AssemblyFacet
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var BatchQueryProvider
     */
    private $provider;

    public function __construct(ObjectManager $manager, BatchQueryProvider $provider)
    {
        $this->em = $manager;
        $this->provider = $provider;
    }

    /**
     * @param Category|int $category
     */
    public function assembly($category)
    {
        if (!$category instanceof Category) {
            $category = $this->em->getRepository(\Catalog\ORM\Entity\Category::class)->find($category);
        }

        $facets = $this->em->getRepository(FacetCategory::class)
            ->createQueryBuilder('fc')
            ->innerJoin('fc.categories', 'c')
            ->andWhere('c.id = :category')
            ->setParameter('category', $category->getId())
            ->getQuery()
            ->getResult()
        ;

        $facets = new ArrayCollection($facets);

        $this->em->getConnection()->beginTransaction();

        /** @var AssemblyFacetRepository $assemblyRepo */
        $assemblyRepo = $this->em->getRepository('ShopBundle:AssemblyFacet');
        $oldFacets = $assemblyRepo->findByCategory($category);
        try {
            $assemblies = [];
            $facets->forAll(function ($k, FacetCategory $facetCategory) use ($category, $assemblyRepo, &$assemblies) {
                $assembly = $assemblyRepo->findByFacetAndCategory($facetCategory, $category);
                $data = [
                    'title' => $facetCategory->getTitle(),
                    'type' => $facetCategory->getType(),
                    'id' => $facetCategory->getId(),
                    'data' => null
                ];
                if (!$assembly) {
                    $assembly = new \Catalog\ORM\Entity\AssemblyFacet();
                    $assembly->setCategory($category)->setFacet($facetCategory);
                }
                switch ($facetCategory->getType()) {
                    case FacetCategory::LIST:
                        $data['data'] = $this->assemblyListType($facetCategory, $category);
                        break;
                    case FacetCategory::RANGE:
                        $data['data'] = $this->assemblyRangeType($facetCategory, $category);
                        break;
                }

                $assembly->setData($data);
                $this->em->persist($assembly);
                array_push($assemblies, $assembly);

                return true;
            });

            //remove not exist facets
            foreach ($oldFacets as $key => $facet) {
                foreach ($assemblies as $assembly) {
                    if ($assembly->getId() === $facet->getId()) {
                        unset($oldFacets[$key]);
                        continue;
                    }
                }
            }

            foreach ($oldFacets as $facet) {
                $this->em->remove($facet);
            }

            $this->em->flush();
            $this->em->getConnection()->commit();
        } catch (\Exception|\ErrorException $e) {
            var_dump($e);
            $this->em->getConnection()->rollBack();
        }
    }

    /**
     * @param \Catalog\ORM\Entity\FacetCategory $facet
     * @param $category
     *
     * @return array
     */
    private function assemblyListType(FacetCategory $facet, $category)
    {
        $builder = $this->em
            ->getRepository(ProductParameter::class)
            ->findByFacetAndCategory($facet, $category, true);

        $list = [];

        foreach ($this->provider->provideBatchResult($builder) as $parameters) {
            foreach ($parameters as $parameter) {
                /** @var \Catalog\ORM\Entity\ProductParameter $parameter */
                array_push_unique($list, $parameter->getValue());
            }
        }

        $parameterNames = $this->em->getRepository('ShopBundle:FacetListValue')
            ->createQueryBuilder('flv')
            ->andWhere('flv.id  IN (:ids)')
            ->setParameter('ids', $list)
            ->getQuery()
            ->getResult();

        $list = [];
        foreach ($parameterNames as $parameterName) {
            /** @var FacetListValue $parameterName */
            $list[$parameterName->getTitle()] = $parameterName->getId();
        }

        return $list;
    }

    private function assemblyRangeType(FacetCategory $facet, $category)
    {
        $builder = $this->em
            ->getRepository(ProductParameter::class)
            ->findByFacetAndCategory($facet, $category, true);

        $builder->select('min(pp.value), max(pp.value)');

        $result = $builder->getQuery()->getScalarResult();
        $result = $result[0];

        return [
            'min' => $result[1], 'max' => $result[2]
        ];
    }
}