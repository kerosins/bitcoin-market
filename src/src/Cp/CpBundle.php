<?php
/**
 * CpBundle class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Cp;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CpBundle extends Bundle
{

}