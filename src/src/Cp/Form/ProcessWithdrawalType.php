<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Form;

use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\Form\BaseForm;
use Referral\Entity\ReferralWithdrawal;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ProcessWithdrawalType extends BaseForm implements EntityManagerContract
{
    use EntityManagerContractTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('withdrawal', HiddenType::class, [
                'constraints' => [
                    new Callback([
                        'callback' => function ($value, ExecutionContextInterface $context) {
                            $withdrawal = $this->entityManager->find(ReferralWithdrawal::class, $value);
                            if (!$withdrawal) {
                                $context->addViolation('Not found withdrawal');
                                return;
                            }

                            if ($withdrawal->getStatus() !== ReferralWithdrawal::PROCESS) {
                                $context->addViolation('This withdrawal already processed');
                            }
                        }
                    ])
                ]
            ])
            ->add('decision', HiddenType::class, [
                'constraints' => [
                    new Callback([
                        'callback' => function ($value, ExecutionContextInterface $context) {
                            if (!in_array($value, [ ReferralWithdrawal::COMPLETE, ReferralWithdrawal::CANCELED ])) {
                                $context->addViolation('Invalid decision value');
                            }
                        }
                    ])
                ]
            ])
            ->add('comment', TextareaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
    }
}