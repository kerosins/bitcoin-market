<?php
/**
 * @author Kondaurov
 */

namespace Cp\Form\Type;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Kerosin\Form\Type\AutoCompleteType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use User\Entity\User;

class AccountType extends AutoCompleteType
{
    private $router;

    public function __construct(ObjectManager $entityManager, RouterInterface $router)
    {
        parent::__construct($entityManager);
        $this->router = $router;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'ajax_url' => $this->router->generate('cp.billing.account_autocomplete'),
            'value_attr' => function (User $user) {
                return $user->getBillingAccount() ? $user->getBillingAccount()->getId() : null;
            },
            'entity_search_function' => function (EntityRepository $entityRepository, $value) {
                return $entityRepository->findOneBy(['billingAccount' => $value]);
            },
            'text_attr' => 'username',
            'entity_class' => User::class,
            'attr' => [
                'placeholder' => 'Select Account'
            ],
            'required' => false
        ]);
    }
}