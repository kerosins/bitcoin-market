<?php
/**
 * @author Kerosin
 */

namespace Cp\Form;

use Kerosin\Form\BaseForm;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderStatus;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Context\ExecutionContext;

class OrderStatusType extends BaseForm
{
    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('currentStatus', ChoiceType::class, [
            'label' => 'New Status',
            'choices' => array_flip(OrderStatus::$statusString)
        ]);

        $builder->get('currentStatus')->addModelTransformer(new CallbackTransformer(
            [$this, 'toForm'],
            [$this, 'toEntity']
        ));
    }

    /**
     * Convert entity to integer
     *
     * @param $entity
     * @return int
     */
    public function toForm($entity)
    {
        return $entity instanceof OrderStatus ? $entity->getType() : OrderStatus::PROCESS;
    }

    /**
     * Convert form value to entity
     *
     * @param $value
     * @return OrderStatus
     */
    public function toEntity($value)
    {
        $entity = new OrderStatus();
        $entity->setType($value);
        return $entity;
    }

    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', Order::class);
    }

    /**
     * @param Order $value
     * @param ExecutionContext $context
     */
    public function validate($value, ExecutionContext $context)
    {
        //todo debug it
//        $currentStatus = $value->getCurrentStatus();
//
//        $history = $value->getHistoryStatuses()->toArray();
//        usort($history, function (OrderStatus $a, OrderStatus $b) {
//             return $a->getCreatedAt() <> $b->getCreatedAt();
//        });
//
//        reset($history);
//        $lastStatus = current($history);
//        if ($lastStatus->getType() === $currentStatus->getType()) {
//            $context->buildViolation('It status is current')
//                ->atPath('currentStatus')
//                ->addViolation();
//        }
    }
}