<?php
/**
 * ProductType class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Cp\Form;

use Kerosin\Form\DataTransformer\ImageFileTransformer;
use Kerosin\Filesystem\ImageHelper;
use Kerosin\Form\Type\ImageType;
use Kerosin\Form\Type\ImageUrlType;
use Kerosin\Form\Type\RouteAliasType;
use Kerosin\Form\Type\TreeType;
use Content\ORM\Entity\Slug;
use Content\SlugRoute\Form\SlugType;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductType extends AbstractType
{
    protected $transformer;

    public function __construct(ImageFileTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('mainImage', ImageUrlType::class)
            ->add('categories',TreeType::class, [
                'class' => Category::class,
                'orderFields' => ['order' => 'desc'],
                'choice_label' => 'title',
                'multiple' => true
            ])
            ->add('images', CollectionType::class, [
                'entry_type' => ImageUrlType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('sort')
            ->add('price', ProductPriceType::class)
            ->add('parameters', ProductParameterCollectionType::class, [
                'product' => $builder->getData()
            ])
            ->add('metaTitle', TextType::class, ['required' => false])
            ->add('metaDescription', TextType::class, ['required' => false])
            ->add('metaKeywords', TextType::class, ['required' => false])

            ->add('slugs', CollectionType::class, [
                'label' => 'Url slugs',
                'entry_type' => \Content\SlugRoute\Form\SlugType::class,
                'entry_options' => [
                    'target' => Slug::TARGET_PRODUCT
                ],
                'allow_add' => true,
                'allow_delete' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'shop_product';
    }
}