<?php
/**
 * ProductParameterType class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Cp\Form;

use Cp\Form\EventListener\ProductParameterFormSubscriber;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Catalog\ORM\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductParameterCollectionType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $subscriber = new ProductParameterFormSubscriber($this->entityManager, $options);
        $builder->addEventSubscriber($subscriber);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'product' => null
        ]);
        $resolver->setAllowedTypes('product', [Product::class, 'null']);
    }

    public function getBlockPrefix()
    {
        return 'product_parameter_collection';
    }
}