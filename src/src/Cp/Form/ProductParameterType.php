<?php
/**
 * @author kerosin
 */

namespace Cp\Form;


use Kerosin\Form\Type\AutoCompleteType;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\FacetCategory;
use Catalog\ORM\Entity\FacetListValue;
use Catalog\ORM\Entity\ProductParameter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductParameterType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'title',
            TextType::class,
            ['mapped' => false, 'disabled' => true]
        );

        $builder->add('value');
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();
            /** @var \Catalog\ORM\Entity\ProductParameter $data */
            $data = $event->getData();

            if ($data->getFacet()->getType() == FacetCategory::BOOL) {
                $form->add('value', ChoiceType::class, [
                    'choices' => [
                        'Select choice' => null,
                        'Yes' => 1,
                        'No' => 0
                    ]
                ]);
            } else if ($data->getFacet()->getType() == FacetCategory::LIST) {
                $form->add('value', ParameterValueType::class, [
                    'label' => false
                ]);
            } else {
                $form->add('value', TextType::class, ['required' => false]);
            }

        });

        //Prepare title for view
        $builder->get('title')->addViewTransformer(new CallbackTransformer(
            function () use ($builder) {
                /** @var FacetCategory $facet */
                $facet = $builder->getData()->getFacet();
                $categories = $facet->getCategories()->map(function (Category $category) {
                    return $category->getTitle();
                });
                $title = sprintf('%s (from %s)', $facet->getTitle(), join(",", $categories->toArray()));

                return $title;
            },
            function () {

            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => ProductParameter::class]);
    }

    public function getBlockPrefix()
    {
        return 'product_parameter';
    }
}