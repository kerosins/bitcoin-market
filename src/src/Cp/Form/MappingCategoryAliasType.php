<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Form;

use Catalog\ORM\Entity\CategoryAlias;
use Kerosin\Form\BaseForm;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContext;

class MappingCategoryAliasType extends BaseForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('aliases', TextareaType::class, [
            'error_bubbling' => true
        ]);

        $builder->get('aliases')
            ->addModelTransformer(new CallbackTransformer(
                [$this, 'toForm'],
                [$this, 'toEntity']
            ))
        ;

        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'preSubmit']);
    }

    public function toForm($value)
    {
        $value = str_replace(CategoryAlias::DELIMITER, PHP_EOL, $value);
        $value = trim($value, CategoryAlias::DELIMITER);

        return $value;
    }

    public function toEntity($value)
    {
        $value = explode(PHP_EOL, $value);
        $value = array_map('trim', $value);

        return CategoryAlias::DELIMITER . implode(CategoryAlias::DELIMITER, $value) . CategoryAlias::DELIMITER;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', CategoryAlias::class);
    }

    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
        $aliases = isset($data['aliases']) ? $data['aliases'] : null;

        if ($aliases && preg_match('/(\|)/', $aliases) > 0) {
//            $form->remove('aliases');
            $form->get('aliases')
                 ->addError(new FormError(CategoryAlias::DELIMITER . ' is reserved char'))
             ;
             $t = 1;
        }
    }

}