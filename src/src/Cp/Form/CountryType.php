<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Form;


use Shop\Entity\Country;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CountryType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('title');
        $builder->add('allowAutocomplete', CheckboxType::class, [
            'label' => 'Allow autocomplete for the Regions'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Country::class);
    }
}