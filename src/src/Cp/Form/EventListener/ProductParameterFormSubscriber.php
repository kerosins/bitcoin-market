<?php
/**
 * ParameterFormListener class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Cp\Form\EventListener;

use Cp\Form\ProductParameterType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\FacetCategory;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Entity\ProductParameter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Exception\RuntimeException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class ProductParameterFormSubscriber implements EventSubscriberInterface
{
    private $options;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(ObjectManager $entityManager, array $options = [])
    {
        $this->options = $options;
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
            FormEvents::SUBMIT => ['onSubmit', 50]
        ];
    }

    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

//        var_dump($data);

        if (null === $data) {
            $data = array();
        }

        if (!is_array($data) && !($data instanceof \Traversable && $data instanceof \ArrayAccess)) {
            throw new UnexpectedTypeException($data, 'array or (\Traversable and \ArrayAccess)');
        }

        // First remove all rows
        foreach ($form as $name => $child) {
            $form->remove($name);
        }

        // get category facets

        /** @var \Catalog\ORM\Entity\Product $product */
        $product = $this->options['product'];
        $categories = $product->getCategories()->map(function (Category $category) {
            return $category->getId();
        });

        $repo = $this->entityManager->getRepository(FacetCategory::class);

        $builder = $repo->createQueryBuilder('fc');

        $facetCategories = $builder->innerJoin('fc.categories', 'c')
            ->andWhere('c.id IN (:categories)')
            ->setParameter('categories', $categories)
            ->orderBy('fc.sort', 'desc')
            ->getQuery()
            ->getResult();

        $data = new ArrayCollection();

        $productParameters = $product->getParameters();
        //build all rows
        foreach ($facetCategories as $name => $facetCategory) {
            /** @var FacetCategory $facetCategory */

            $parameter = $productParameters->filter(function (ProductParameter $parameter) use ($facetCategory) {
                return $parameter->getFacet()->getId() == $facetCategory->getId();
            });

            if ($parameter->count() == 1) {
                $parameter = $parameter->first();
            } else {
                $parameter = new ProductParameter();
                $parameter->setProduct($product)->setFacet($facetCategory);
            }

            $data->add($parameter);

            $form->add($name, ProductParameterType::class, [
                'property_path' => "[{$name}]",
                'data' => $parameter,
                'label' => false
            ]);
        }

        $event->setData($data);
    }

    public function preSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        if (!is_array($data)) {
            $data = [];
        }

        foreach ($form as $name => $value) {
            if (empty($data[$name])) {
                $form->remove($name);
            }
        }
    }

    public function onSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        // At this point, $data is an array or an array-like object that already contains the
        // new entries, which were added by the data mapper. The data mapper ignores existing
        // entries, so we need to manually unset removed entries in the collection.

        if (null === $data) {
            $data = array();
        }

        if (!is_array($data) && !($data instanceof \Traversable && $data instanceof \ArrayAccess)) {
            throw new UnexpectedTypeException($data, 'array or (\Traversable and \ArrayAccess)');
        }

        $previousData = $event->getForm()->getData();
        foreach ($form as $name => $child) {
            $isNew = !isset($previousData[$name]);

            // $isNew can only be true if allowAdd is true, so we don't
            // need to check allowAdd again
            if ($data[$name]->getValue() === null) {
                unset($data[$name]);
                $form->remove($name);
            }
        }

        // The data mapper only adds, but does not remove items, so do this
        // here
        $toDelete = array();

        foreach ($data as $name => $child) {
            if (!$form->has($name)) {
                $toDelete[] = $name;
            }
        }

        foreach ($toDelete as $name) {
            unset($data[$name]);
        }

        $event->setData($data);
    }


}