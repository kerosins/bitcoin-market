<?php

namespace Cp\Form;

use Kerosin\Form\Type\TreeType;
use Content\ORM\Entity\Slug;
use Content\SlugRoute\Form\SlugType;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Repository\CategoryRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Category $entity */
        $entity = $builder->getData();
        $builder
            ->add('title')
            ->add('parent', TreeType::class, [
                'class' => Category::class,
                'query_builder' => function (CategoryRepository $er) use ($entity) {
                    $builder = $er->createQueryBuilder('c');
                    if ($entity->getId()) {
                        $builder->andWhere('c.id != :id')->setParameter('id', $entity->getId());
                    }

                    $builder->orderBy('c.order', 'desc');

                    return $builder;
                },
                'placeholder' => 'Select parent',
                'empty_data' => null,
                'required' => false,
                'choice_label' => 'title',
            ])
            ->add('order')

            ->add('metaTitle', TextType::class, ['required' => false])
            ->add('metaDescription', TextType::class, ['required' => false])
            ->add('metaKeywords', TextType::class, ['required' => false])

            ->add('slugs', CollectionType::class, [
                'label' => 'Uri aliases',
                'entry_type' => \Content\SlugRoute\Form\SlugType::class,
                'entry_options' => [
                    'target' => Slug::TARGET_CATEGORY
                ],
                'allow_add' => true,
                'allow_delete' => true
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Category::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'shop_category';
    }


}
