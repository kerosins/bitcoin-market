<?php
/**
 * ProductFilter class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Cp\Form;

use Shop\Provider\ProductProvider;
use Kerosin\Form\Type\TreeType;
use Catalog\ORM\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductFilter extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('category', TreeType::class, [
            'class' => Category::class,
            'orderFields' => ['order' => 'desc'],
            'choice_label' => 'title',
        ])
            ->setMethod('GET')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => ProductProvider::class]);
    }

    public function getBlockPrefix()
    {
        return 'cp_product_filter';
    }
}