<?php
/**
 * @author Kerosin
 */

namespace Cp\Form;


use Cp\Provider\AddressObjectProvider;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Kerosin\Form\Type\AutoCompleteType;
use Kerosin\Form\Type\SimpleEntityType;
use Shop\Entity\AddressObject;
use Shop\Entity\ZipCode;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressObjectType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(ObjectManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('type', ChoiceType::class, [
                'choices' => AddressObjectProvider::typesList()
            ])
            ->add('parent', SimpleEntityType::class, [
                'repository' => $this->em->getRepository('ShopBundle:AddressObject'),
                'attr' => [
                    'class' => 'address-form-parent-object'
                ]
            ])
            ->add('zipCodes', CollectionType::class, [
                'entry_type' => SimpleEntityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_options' => [
                    'repository' => $this->em->getRepository('ShopBundle:ZipCode'),
                    'isTextInputType' => true,
                    'choice_label' => 'zip',
                    'choice_value' => 'zip',
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => AddressObject::class]);
    }

    public function getBlockPrefix()
    {
        return 'address_object_form';
    }
}