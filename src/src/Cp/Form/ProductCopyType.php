<?php
/**
 * @author kerosin
 */

namespace Cp\Form;


use Kerosin\Form\Type\TreeType;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductCopyType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('categories',TreeType::class, [
                'class' => Category::class,
                'orderFields' => ['order' => 'desc'],
                'choice_label' => 'title',
                'multiple' => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Product::class);
    }
}