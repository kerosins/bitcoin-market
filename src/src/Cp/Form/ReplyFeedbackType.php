<?php
/**
 * @author Kondaurov
 */

namespace Cp\Form;


use Shop\Entity\Feedback\BaseFeedback;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReplyFeedbackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('answerSubject', TextType::class, [
                'label' => 'Subject of Reply',
                'disabled' => $options['disabled']
            ])
            ->add('answer', TextareaType::class, [
                'label' => 'Reply',
                'disabled' => $options['disabled']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', BaseFeedback::class);
    }
}