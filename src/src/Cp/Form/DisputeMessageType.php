<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Form;


use Kerosin\Form\BaseForm;
use Catalog\ORM\Entity\Dispute;
use Payment\Provider\CurrencyProvider;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Context\ExecutionContext;

class DisputeMessageType extends BaseForm
{
    /**
     * @var CurrencyProvider
     */
    private $currencyProvider;

    public function __construct(CurrencyProvider $currencyProvider)
    {
        $this->currencyProvider = $currencyProvider;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Dispute $dispute */
        $dispute = $options['dispute'];

        $maxRefundAmount = $dispute->getOrderPosition()->getTotalPublicPrice();

        $maxRefundAmount = $this->currencyProvider->convertByRate(
            $maxRefundAmount,
            $dispute->getOrderPosition()->getOrder()->getRealPayments()->first()->getRate()
        );

        $builder
            ->add('message', MessageType::class)
            ->add('amount', TextType::class, [
                'label' => 'Refund amount (max: ' . $maxRefundAmount . ' ' . $dispute->getCurrency() . ')',
                'required' => false,
                'constraints' => [
                    new Range([
                        'min' => 0,
                        'max' => $maxRefundAmount
                    ])
                ]
            ])
            ->add('action', ChoiceType::class, [
                'placeholder' => 'Select action',
                'required' => false,
                'choices' => [
                    'Decline' => Dispute::STATUS_DECLINE,
                    'Full Refund' => Dispute::STATUS_FULL_REFUND,
                    'Partial Refund' => Dispute::STATUS_PARTIAL_REFUND
                ],
                'choice_attr' => function ($choiceValue) {
                    return [
                        'data-display-amount' => (int)in_array(
                            $choiceValue,
                            [
                                Dispute::STATUS_FULL_REFUND,
                                Dispute::STATUS_PARTIAL_REFUND
                            ]
                        )
                    ];
                }
            ])
        ;
    }

    public function validate($data, ExecutionContext $context)
    {
        $action = $data['action'];
        if (in_array($action, [
                Dispute::STATUS_FULL_REFUND,
                Dispute::STATUS_PARTIAL_REFUND
            ])
            && empty($data['amount'])
        ) {
            $context
                ->buildViolation('Amount is required')
                ->atPath('amount')
                ->addViolation();
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('dispute', null);
        $resolver->addAllowedTypes('dispute', Dispute::class);
        $resolver->setRequired('dispute');

        parent::configureOptions($resolver);
    }
}