<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Form;


use Kerosin\Entity\SystemSetting;
use Kerosin\Form\BaseForm;
use Kerosin\Form\DataTransformer\CallbackWithOptionsTransformer;
use Kerosin\SystemSettings\SettingValueConverter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingForm extends BaseForm
{
    /**
     * @var SettingValueConverter
     */
    private $valueConverter;

    public function __construct(SettingValueConverter $valueConverter)
    {
        $this->valueConverter = $valueConverter;
    }

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $fieldType = $options['type'];
        $fieldOptions = [
            'label' => $options['label']
        ];

        switch ($fieldType) {
            case SystemSetting::TYPE_BOOL:
                $fieldClass = ChoiceType::class;
                $fieldOptions['choices'] = [
                    'Yes' => true,
                    'No' => false
                ];
                $fieldOptions['multiple'] = false;
                $fieldOptions['expanded'] = true;
                break;
            default:
                $fieldClass = TextType::class;
                break;
        }

        $builder->add('value', $fieldClass, $fieldOptions);

        $builder->get('value')->addModelTransformer(
            new CallbackWithOptionsTransformer(
                function ($value, $options) {
                    return $this->valueConverter->toPhp($value, $options['type']);
                },
                function ($value, $options) {
                    return $this->valueConverter->toDb($value, $options['type']);
                },
                $options
            )
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('type', SystemSetting::TYPE_STRING);
        $resolver->setAllowedTypes('type', 'string');
        $resolver->setRequired('type');

        parent::configureOptions($resolver);
    }
}