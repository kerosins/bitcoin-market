<?php
/**
 * @author Kondaurov
 */

namespace Cp\Form\Filter;

use Cp\Provider\ProductProvider;
use Kerosin\Form\BaseFormFilter;
use Kerosin\Form\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductFilter extends BaseFormFilter
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('productId', TextType::class, [
                'required' => false
            ])
            ->add('title', TextType::class, [
                'required' => false
            ])
            ->add('price', RangeType::class, [
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', ProductProvider::class);
    }
}