<?php
/**
 * @author Kondaurov
 */

namespace Cp\Form\Filter;


use Cp\Form\Type\AccountType;
use Kerosin\Form\BaseFormFilter;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;

class BillingAccountFilter extends BaseFormFilter
{
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', AccountType::class);
    }
}