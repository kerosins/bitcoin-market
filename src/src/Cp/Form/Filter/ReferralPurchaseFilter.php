<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Form\Filter;


use Cp\Provider\ReferralProvider;
use Kerosin\Form\BaseFormFilter;
use Kerosin\Form\Type\AutoCompleteType;
use Kerosin\Form\Type\DateRangeType;
use Kerosin\Form\Type\RangeType;
use Referral\Entity\ReferralPurchase;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use User\Entity\User;
use User\Form\Type\UserAutocompleteType;
use User\Form\UserType;

class ReferralPurchaseFilter extends BaseFormFilter
{

    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', UserAutocompleteType::class, [
                'required' => false
            ])
            ->add('date', DateRangeType::class, [
                'label' => 'Purchase Date'
            ])
            ->add('amount', RangeType::class)
            ->add('status', ChoiceType::class, [
                'choices' => array_flip(ReferralPurchase::$statusLabel)
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', ReferralProvider::class);
    }
}