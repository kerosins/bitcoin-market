<?php
/**
 * @author Kondaurov
 */

namespace Cp\Form\Filter;


use Billing\Entity\Transaction;
use Cp\Form\Type\AccountType;
use Kerosin\Form\BaseFormFilter;
use Kerosin\Form\Type\DateRangeType;
use Kerosin\Form\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class BillingTransactionFilter extends BaseFormFilter
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('account', HiddenType::class, [
                'required' => false
            ])
            ->add('transaction', TextType::class, [
                'required' => false
            ])
            ->add('fromAccount', AccountType::class)
            ->add('toAccount', AccountType::class)
            ->add('amount', RangeType::class, [
                'required' => false
            ])
            ->add('date', DateRangeType::class, [
                'required' => false
            ])
            ->add('status', ChoiceType::class, [
                'required' => false,
                'multiple' => true,
                'choices' => array_flip(Transaction::$statusString)
            ]);
    }
}