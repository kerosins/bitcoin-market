<?php
/**
 * @author Kondaurov
 */

namespace Cp\Form\Filter;

use Cp\Provider\WithdrawalProvider;
use Kerosin\Form\BaseFormFilter;
use Referral\Entity\ReferralWithdrawal;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WithdrawalFilter extends BaseFormFilter
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('status', ChoiceType::class, [
            'choices' => array_flip(ReferralWithdrawal::$statusLabel)
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', WithdrawalProvider::class);
    }
}