<?php
/**
 * @author Kerosin
 */

namespace Cp\Form\Filter;

use Kerosin\Form\BaseForm;
use Kerosin\Form\BaseFormFilter;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderStatus;
use Catalog\Order\Service\OrderProvider;
use Payment\Entity\Payment;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use User\Form\Type\UserAutocompleteType;

class OrderFilterType extends BaseFormFilter
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('client', UserAutocompleteType::class, [
                'required' => false
            ])
            ->add('code', TextType::class, [
                'required' => false
            ])
            ->add('status', ChoiceType::class, [
                'label' => 'Order Status',
                'required' => false,
                'multiple' => true,
                'choices' => self::getStatusOptions(),
            ])
            ->add('hasDispute', CheckboxType::class, [
                'label' => 'Has opened dispute',
                'required' => false
            ])
        ;
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => OrderProvider::class
        ]);
    }

    public static function getStatusOptions()
    {
        $statuses = OrderStatus::$statusString;
        return array_flip($statuses);
    }
}