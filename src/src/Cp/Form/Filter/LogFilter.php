<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Form\Filter;

use Cp\Provider\LogProvider;
use Kerosin\Entity\BusinessLog;
use Kerosin\Form\BaseFormFilter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LogFilter extends BaseFormFilter
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message', TextType::class, [
                'required' => false
            ])
            ->add('level', ChoiceType::class, [
                'required' => false,
                'expanded' => true,
                'multiple' => true,
                'choices' => [
                    self::getLevelChoices()
                ]
            ])
            ->add('channel', ChoiceType::class, [
                'required' => false,
                'choices' => self::getChannelChoices()
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => LogProvider::class
        ]);
    }

    /**
     * Returns choices for channel select
     *
     * @return array
     */
    public static function getChannelChoices()
    {
        return [
            'Background' => BusinessLog::CHANNEL_BACKGROUND,
            'Catalog\Import' => BusinessLog::CHANNEL_IMPORT,
            'Business' => BusinessLog::CHANNEL_BUSINESS,
            'Search' => BusinessLog::CHANNEL_SEARCH,
            'Referral Program' => BusinessLog::CHANNEL_REFERRAL
        ];
    }

    public static function getLevelChoices()
    {
        return [
            'Info' => 'info',
            'Debug' => 'debug',
            'Critical' => 'critical',
            'Warning' => 'warning',
            'Error' => 'error'
        ];
    }
}