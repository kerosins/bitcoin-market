<?php
/**
 * Created by PhpStorm.
 * User: kerosin
 * Date: 06.04.2018
 * Time: 2:54
 */

namespace Cp\Form;

use Kerosin\Form\BaseForm;
use Kerosin\Form\DataTransformer\Filter\TrimFilter;
use Content\ORM\Entity\Slug;
use Content\ORM\Entity\StaticPage;
use Content\Seo\Form\MetaTagsFieldsHelper;
use Content\SlugRoute\Form\SlugType;
use function Sodium\add;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class StaticPageForm extends BaseForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('body', TextareaType::class, [
                'constraints' => [
                    new NotBlank()
                ]
            ]);
        //seo meta
        MetaTagsFieldsHelper::addFields($builder);

        //route aliases
        $builder->add('slugs', CollectionType::class, [
            'label' => 'Uri slugs',
            'required' => true,
            'entry_type' => \Content\SlugRoute\Form\SlugType::class,
            'entry_options' => [
                'target' => Slug::TARGET_STATIC_PAGE
            ],
            'allow_add' => true,
            'allow_delete' => true
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StaticPage::class,
            'allow_extra_fields' => true
        ]);

        parent::configureOptions($resolver);
    }
}