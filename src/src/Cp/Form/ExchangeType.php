<?php
/**
 * @author Kerosin
 */

namespace Cp\Form;

use Payment\Entity\Exchange;
use Payment\Form\ClientType;
use function Sodium\add;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExchangeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('clientClass', ClientType::class)
            ->add('isActive');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Exchange::class]);
    }
}