<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Form;


use Kerosin\Form\BaseForm;
use Referral\Doctrine\DBAL\Type\ReferralSettings;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReferralProgramSettingsType extends BaseForm
{
    private $defaultExpire;
    private $defaultCount;

    public function __construct(int $defaultExpire, int $defaultCount)
    {
        $this->defaultExpire = $defaultExpire;
        $this->defaultCount = $defaultCount;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('purchaseCount', NumberType::class, [
                'label' => 'Count of purchases (0 - Infinity, Default - ' . $this->defaultCount . ')'
            ])
            ->add('cookieExpire', NumberType::class, [
                'label' => 'Expire of "Ref" cookie in days (0 - Infinity, Default - ' . $this->defaultExpire . ' days)'
            ])
            ->add('approveEvent', ChoiceType::class, [
                'label' => 'When purchase with this program should be approved?',
                'choices' => [
                    'Order was done' => ReferralSettings::ON_ORDER_DONE,
                    'Payment was accepted'  => ReferralSettings::ON_PAYMENT_ACCEPT
                ]
            ])
        ;

        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data_class', ReferralSettings::class);
    }
}