<?php
/**
 * @author kerosin
 */

namespace Cp\Form;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Kerosin\Form\Type\AutoCompleteType;
use Catalog\ORM\Entity\FacetListValue;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ParameterValueType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(ObjectManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('value', AutoCompleteType::class, [
            'ajax_url' => '/cp/facet-autocomplete',
            'entity_class' => FacetListValue::class,
            'text_attr' => 'title',
            'required' => false
        ]);

        $builder->add('createNewValue', TextType::class, [
            'label' => 'or create new value',
            'required' => false
        ]);

        $builder->addModelTransformer(new CallbackTransformer(
            [$this, 'normalizeDataValue'],
            [$this, 'deNormalizeDataValue']
        ));
    }

    public function getBlockPrefix()
    {
        return 'product_parameter_value';
    }

    public function normalizeDataValue($value)
    {
        return [
            'value' => $value,
            'createNewValue' => null
        ];
    }

    public function deNormalizeDataValue($value)
    {
        if (!empty($value['createNewValue'])) {
            $entity = $this->em
                ->getRepository('ShopBundle:FacetListValue')
                ->createIfNotExistsByTitle($value['createNewValue'])
            ;

            return $entity->getId();
        }

        return $value['value'];
    }
}