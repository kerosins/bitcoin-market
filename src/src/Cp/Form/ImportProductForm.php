<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Cp\Form;


use Catalog\ORM\Entity\ExternalProductsMap;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Entity\Source;
use Doctrine\ORM\QueryBuilder;
use Kerosin\Component\Validator\ExistRecord;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ImportProductForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('source', TextType::class, [
                'label' => "AliExpress's ID of The Product",
                'constraints' => [
                    new ExistRecord([
                        'entity' => Product::class,
                        'attribute' => 'externalId',
                        'extendQuery' => function (QueryBuilder $builder) {
                            $builder->andWhere('e.externalSource = :source')
                                ->setParameter('source', Source::EXTERNAL_SOURCE_ALIEXPRESS);
                        }
                    ])
                ]
            ]);
    }
}