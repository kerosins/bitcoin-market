<?php
/**
 * @author Kerosin
 */

namespace Cp\Form;

use Kerosin\Form\BaseForm;
use Catalog\ORM\Entity\OrderPosition;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Context\ExecutionContext;

class OrderProductType extends BaseForm
{
    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', ChoiceType::class, [
                'choices' => array_flip(OrderPosition::$statusString)
            ])
            ->add('trackCode', TextType::class, [
                'required' => false
            ])
            ->add('comment', TextType::class, [
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('data_class', OrderPosition::class);
    }

    /**
     * @param OrderPosition $entity
     * @param ExecutionContext $context
     */
    public function validate($entity, ExecutionContext $context)
    {
        if ($entity->getStatus() === OrderPosition::STATUS_SHIPPING &&
            !$entity->getTrackCode()
        ) {
            $context
                ->buildViolation('Track Code is requred')
                ->atPath('trackCode')
                ->addViolation();
        }
    }
}