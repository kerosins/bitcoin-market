<?php
/**
 * @author Kerosin
 */

namespace Background\Exception;

class WorkerNotFoundException extends \Exception
{
}