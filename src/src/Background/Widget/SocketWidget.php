<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Background\Widget;

use Background\WebSocket\Server;
use Kerosin\Component\Widget;

class SocketWidget extends Widget implements \Twig_Extension_GlobalsInterface
{
    public function getGlobals()
    {
        return [
            'socketId' => $this->container->get('session')->getId()
        ];
    }
}