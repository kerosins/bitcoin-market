<?php
namespace Background\Worker\Output;

use Symfony\Component\Console\Output\StreamOutput;

/**
 * @author Kerosin
 */
class WorkerOutput extends StreamOutput
{
    /**
     * Worker log file
     *
     * WorkerOutput constructor.
     * @param string $file
     */
    public function __construct(string $file)
    {
        parent::__construct(
            fopen($file, 'a'),
            self::VERBOSITY_NORMAL,
            null,
            null
        );
    }

    public function write($messages, $newline = false, $options = self::OUTPUT_NORMAL)
    {
        $messages = (array)$messages;

        parent::write(
            array_map(function ($message) {
                return sprintf(
                    '%s - %s',
                    date('Y-m-d H:i:s'),
                    $message
                );
            }, $messages),
            $newline,
            $options
        );
    }
}