<?php
/**
 * WorkerManager class file.
 */

namespace Background\Worker;

use Doctrine\Common\Collections\ArrayCollection;
use Psr\Log\LoggerInterface;
use React\ChildProcess\Process;
use React\EventLoop\ExtEventLoop;
use React\EventLoop\Factory;
use React\EventLoop\LibEventLoop;
use React\EventLoop\LibEvLoop;
use React\EventLoop\StreamSelectLoop;
use React\Promise\Deferred;
use React\Promise\Promise;
use React\Promise\RejectedPromise;
use React\Socket\ConnectionInterface;
use React\Socket\Server;
use React\Socket\TcpConnector;

/**
 * Todo debug wait for complete that worker
 *
 * Run a worker server and receive message from background
 *
 * Class WorkerServer
 * @package Background\Worker
 */
class WorkerServer
{
    /**
     * @var string
     */
    private $serverAddress;

    private $taskWorkers = [];

    /**
     * @var ExtEventLoop|LibEventLoop|LibEvLoop|StreamSelectLoop
     */
    private $loop;

    /**
     * @var Server
     */
    private $socketServer;

    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Heap of counters of running workers
     *
     * @var array
     */
    private $countRunningWorkers = [];

    private const
        MAP_WORKER_CLASS = 'workerClass',
        MAP_WORKER_LIMIT = 'workerLimit';

    /**
     * WorkerManager constructor.
     * @param string $server
     */
    public function __construct(
        LoggerInterface $logger,
        string $server,
        string $rootDir,
        int $limitOfWorkers = 2
    ) {
        $this->serverAddress = $server;
        $this->rootDir = $rootDir;
        $this->taskWorkers = new ArrayCollection();
        $this->logger = $logger;
    }

    /**
     * initialize event loop, and socket server
     */
    public function run()
    {
        $this->loop = Factory::create();
        $this->socketServer = new Server($this->serverAddress, $this->loop);

        $this->logger->info('Run a server. It\'s ready for tasks');

        $this->socketServer->on('connection', function (ConnectionInterface $connection) {
            //receive message
            $connection->on('data', function ($task) use ($connection) {
                $task = json_decode($task, true);
                if (
                    json_last_error() !== JSON_ERROR_NONE ||
                    !$task ||
                    !isset($task['id'])
                ) {
                    $this->logger->warning('Receive an invalid message', [
                        'Socket message' => $task
                    ]);
                    $connection->close();
                    return;
                }

                $workerId = $task['id'];

                if (!isset($this->taskWorkers[$workerId])) {
                    $this->logger->error("Worker with name {$workerId} not found");
                    $connection->close();
                    return;
                }

                $wait = (isset($task['wait']) && $task['wait'] === true);
                $isClose = false;
                if (!$wait) {
                    $connection->close();
                    $isClose = true;
                }

                $this->logger->info("Receive Task", [
                    'Task Id' => $task['id'],
                    'Task Data' => $task['data']
                ]);

                $this
                    ->checkForFreeWorker($workerId)
                    ->then(
                        function () use ($task) {
                            return $this->onReceiveTask($task['id'], $task['data']);
                        }
                    )
                    ->then(
                        function ($result) use ($connection, $isClose, $workerId) {
                            $this->countRunningWorkers[$workerId]--;
                            if (!$isClose) {
                                $connection->write(json_encode($result));
                            }
                        }
                    );
            });

            $connection->on('error', function (\Exception $e) {
                $this->logger->critical('Socket error:' . $e->getMessage());
            });
        });

        $this->loop->run();
    }

    public function checkForFreeWorker($id)
    {
        $deferred = new Deferred();
        $limit = $this->getLimitOfWorkersById($id);
        if (!isset($this->countRunningWorkers[$id])) {
            $this->countRunningWorkers[$id] = 0;
        }

        if ($this->countRunningWorkers[$id] < $limit) {
            $this->countRunningWorkers[$id]++;
            $deferred->resolve();
        } else {
            $this->loop->addPeriodicTimer(1, function ($timer) use ($deferred, $limit, $id) {
                if ($this->countRunningWorkers[$id] < $limit) {
                    $this->countRunningWorkers[$id]++;
                    $deferred->resolve();
                    $this->loop->cancelTimer($timer);
                }
            });
        }
        return $deferred->promise();
    }

    /**
     * @param string $taskId
     * @param array $taskData
     *
     * @return Promise
     */
    public function onReceiveTask(string $taskId, array $taskData = [])
    {
        //create a promise
        $deferred = new Deferred();

        $worker = array_filter($this->taskWorkers->getKeys(), function ($name) use ($taskId) {
            return $name == $taskId;
        });

        $worker = count($worker) > 0 ? reset($worker) : null;
        if (!$worker) {
            $this->logger->error("Worker with name {$taskId} not found");
            $deferred->reject();
            return $deferred->promise();
        }

        $this->logger->info(
            "Start processing task with name {$worker}",
            [
                'Task Data' => $taskData
            ]
        );

        $dataArg = json_encode($taskData);
        $dataArg = escapeshellarg($dataArg);

        $cmd = "php {$this->rootDir}/../bin/console background:task {$worker} {$dataArg}";
        $process = new Process($cmd);
        $this->logger->info($cmd);
        $process->start($this->loop);

        $workerOutput = '';
        $process->stdout->on('data', function ($chunk) use (&$workerOutput) {
            $workerOutput .= (string)$chunk;
        });
        $process->stderr->on('data', function ($chunk) use (&$workerOutput) {
            $workerOutput .= (string)$chunk;
        });

        $process->on('exit', function ($exitCode) use ($worker, &$workerOutput, $deferred) {
            $deferred->resolve([
                'code' => $exitCode,
                'output' => trim($workerOutput)
            ]);
            $this->logger->info("Worker '{$worker}' with a Task processing finished with code {$exitCode}", [
                'Worker Output' => trim($workerOutput)
            ]);
        });

        return $deferred->promise();
    }

    /**
     * @param string $name
     * @param TaskWorkerInterface $worker
     */
    public function addTaskWorker(string $name, TaskWorkerInterface $worker, int $limit = 1)
    {
        $this->taskWorkers->set($name, [
            self::MAP_WORKER_CLASS => $worker,
            self::MAP_WORKER_LIMIT => $limit
        ]);
    }

    /**
     * @deprecated
     *
     * @param string $id
     * @param array $taskData
     *
     * @return Promise
     */
    public function addTask(string $id, array $taskData, $wait = false)
    {
        $loop = Factory::create();
        $connector = new TcpConnector($loop);

        $message = json_encode(['id' => $id, 'data' => $taskData, 'wait' => $wait]);

        $promise = $connector->connect($this->serverAddress)
            ->then(
                function (ConnectionInterface $connection) use ($message, $wait) {
                    $connection->write($message . PHP_EOL);
                    $deferred = new Deferred();

                    if ($wait) {//wait for task output
                        $connection->on('data', function ($data) use ($deferred, $connection) {
                            $data = json_decode($data, true);
                            $deferred->resolve($data);
                            $connection->end();
                        });

                        $connection->on('error', function () use ($deferred) {
                            $deferred->reject();
                        });
                    } else {
                        $deferred->resolve();
                        $connection->end();
                    }

                    return $deferred->promise();
                },
                function (\Exception $err) {
                    $this->logger->critical('Cannot connect to task server');
                    return new RejectedPromise($err->getMessage());
                }
            );
        $loop->run();

        return $promise;
    }

    /**
     * @param $id
     * @return TaskWorkerInterface
     */
    public function getWorker($id)
    {
        $worker = $this->taskWorkers->get($id);
        return $worker ? $worker[self::MAP_WORKER_CLASS] : null;
    }

    /**
     * Returns count of workers which may run in same time
     *
     * @param $id
     */
    private function getLimitOfWorkersById($id)
    {
        $worker = $this->taskWorkers->get($id);
        return $worker ? $worker[self::MAP_WORKER_LIMIT] : null;
    }
}