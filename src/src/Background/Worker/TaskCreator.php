<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Background\Worker;

use React\Promise\Deferred;
use React\Promise\PromiseInterface;
use React\Socket\ConnectionInterface;
use Psr\Log\LoggerInterface;
use React\EventLoop\Factory;
use React\EventLoop\LoopInterface;
use React\Socket\TcpConnector;

class TaskCreator
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $workerServerAddress;

    public function __construct(LoggerInterface $logger, string $workerServerAddress)
    {
        $this->logger = $logger;
        $this->workerServerAddress = $workerServerAddress;
    }

    private function createConnector(LoopInterface $loop)
    {
        $connector = new TcpConnector($loop);
        return $connector->connect($this->workerServerAddress);
    }

    /**
     * Add task to background job and nowait when it has been completed
     *
     * @param string $id - id of task
     * @param array $data - params of task
     */
    public function addTask(string $id, array $data = [])
    {
        $loop = Factory::create();
        $message = json_encode([
            'id' => $id,
            'data' => $data,
            'wait' => false
        ]);
        $this->createConnector($loop)
            ->then(
                function (ConnectionInterface $connection) use ($message) {
                    $this->logger->info('Sent message to worker server', [
                        'message' => json_decode($message, true),
                        'server' => $this->workerServerAddress
                    ]);
                    $connection->write($message . PHP_EOL);
                    $connection->end();
                },
                function (\Exception $exception) {
                    $this->logger->critical($exception->getMessage(), [
                        'trace' => $exception->getTraceAsString()
                    ]);
                }
            );

        $loop->run();
    }

    /**
     * add task and return promise for wait it
     *
     * @param LoopInterface $loop
     * @param string $id
     * @param array $data
     * @return PromiseInterface
     */
    public function addTaskWithPromise(LoopInterface $loop, string $id, array $data = []): PromiseInterface
    {
        $message = json_encode([
            'id' => $id,
            'data' => $data,
            'wait' => true
        ]);

        $deferred = new Deferred();

        $this->createConnector($loop)
            ->then(
                function (ConnectionInterface $connection) use ($deferred, $message) {
                    $this->logger->info('Sent message to worker server', [
                        'message' => json_decode($message, true),
                        'server' => $this->workerServerAddress
                    ]);
                    $connection->write($message . PHP_EOL);

                    $connection->on('data', function ($data) use ($deferred, $connection) {
                        $data = json_decode($data, true);
                        $deferred->resolve($data);
                        $connection->end();
                    });

                    $connection->on('error', function () use ($deferred) {
                        $deferred->reject();
                    });
                },
                function (\Exception $exception) {
                    $this->logger->critical($exception->getMessage(), [
                        'trace' => $exception->getTraceAsString()
                    ]);
                }
            );

        return $deferred->promise();
    }
}