<?php
/**
 * @author Kondaurov
 */

namespace Background\Worker;

interface TaskWorkerInterface
{
    public function runJob(array $data);
}