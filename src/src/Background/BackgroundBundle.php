<?php
/**
 * @author Kerosin
 */

namespace Background;

use Background\DependencyInjection\BackgroundConfigurationExtension;
use Background\DependencyInjection\WorkerCompiler;
use Background\Worker\TaskWorkerInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class BackgroundBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container
            ->registerForAutoconfiguration(TaskWorkerInterface::class)
            ->addTag(WorkerCompiler::TAG)
        ;
        $container
            ->addCompilerPass(new WorkerCompiler());
    }

    public function getContainerExtension()
    {
        return new BackgroundConfigurationExtension();
    }
}