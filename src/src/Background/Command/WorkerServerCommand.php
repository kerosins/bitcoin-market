<?php
/**
 * @author Kerosin
 */

namespace Background\Command;


use Background\Entity\ProgressModel;
use Background\Exception\WorkerNotFoundException;
use Background\Exception\WorkerRuntimeException;
use Background\TaskTransportableInterface;
use Background\Worker\WorkableInterface;
use Background\Worker\WorkerServer;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WorkerServerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('background:worker:server')
            ->setDescription('Run a worker server')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $server = $this->getContainer()->get(WorkerServer::class);
        $server->run();
    }
}