<?php
/**
 * TaskCommand class file.
 */

namespace Background\Command;

use Background\Worker\TaskWorkerInterface;
use Background\Worker\WorkerServer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TaskCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('background:task')
            ->addArgument('worker', InputArgument::REQUIRED, 'Worker class')
            ->addArgument('data', InputArgument::REQUIRED, 'Task data')
            ->setDescription('Run a task')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var TaskWorkerInterface $worker */
        $worker = $input->getArgument('worker');
        $worker = $this->getContainer()->get(WorkerServer::class)->getWorker($worker);
        $data = $input->getArgument('data');
        $data = json_decode($data, true);

        $result = $worker->runJob($data);
        $output->writeln(json_encode($result));
    }
}