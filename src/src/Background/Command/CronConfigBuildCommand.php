<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Background\Command;


use Kerosin\Component\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Parser;

class CronConfigBuildCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('background:cron:build')
            ->setDescription('Builds a configuration for Crontab');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $parser = new Parser();
        $container = $this->getContainer();

        $confPath = $container->getParameter('zenith.fs.config_dir') . '/packages/background/cron.conf.yml';
        $logsDir = $container->getParameter('zenith.background.logs_dir') . '/cron';
        $destination = $container->getParameter('zenith.fs.main_dir');

        $config = $parser->parseFile($confPath);
        $content = '';

        foreach ($config['jobs'] as $job) {
            $logFile = str_replace(':', '_', $job['command']) . '.txt';
            $content .= "{$job['schedule']} (cd {$destination} && ./btshop.sh {$job['command']}) >> {$logsDir}/{$logFile}" . PHP_EOL;
        }

        $output->writeln('Generated following content' . PHP_EOL);
        $output->writeln($content);

        $file = new \SplFileObject($destination . '/zenith.crontab', 'w+');
        $file->fwrite($content);
    }
}