<?php
/**
 * ExampleWorker class file.
 */

namespace Background\Command\Worker;

use Background\Worker\TaskWorkerInterface;

class ExampleWorker implements TaskWorkerInterface
{
    public function runJob(array $data)
    {
        sleep(4);
        return $data;
    }
}