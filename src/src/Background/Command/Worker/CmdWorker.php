<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Background\Command\Worker;
use Background\Exception\WorkerRuntimeException;
use Background\Worker\TaskWorkerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class CmdWorker
 * @package Background\Command\Worker
 *
 * Execute command as worker
 * params:
 *  - cmd - name of command
 *  - params - params of command
 */
class CmdWorker implements TaskWorkerInterface
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var Application
     */
    private $application;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
        $this->application = new Application($kernel);
    }

    public function runJob(array $data)
    {
        $cmd = $data['cmd'] ?? null;
        $params = $data['params'] ?? [];

        if (!$cmd && !$params) {
            throw new WorkerRuntimeException('Input data for background job is incorrect');
        }

        //build input and output
        $params['command'] = $cmd;
        $input = new ArrayInput($params);
        $output = new BufferedOutput();

        $cmd = $this->application->find($cmd);

        //run and return result
        $code = $cmd->run($input, $output);
        if ($code !== 0) {
            echo 2222;
        }
        return $output->fetch();
    }
}