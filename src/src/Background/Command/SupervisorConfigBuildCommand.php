<?php
/**
 * SupervisorConfigBuildCommand class file.
 */

namespace Background\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Parser;

class SupervisorConfigBuildCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('background:supervisor:build')
            ->setDescription('Build a configuration file for supervisor');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rootDir = $this->getContainer()->getParameter('kernel.root_dir');
        $logsDir = dirname($this->getContainer()->getParameter('kernel.logs_dir')) . '/workers/logs';
        $user = $this->getContainer()->getParameter('supervisor.user');

        $destinationDir = dirname($rootDir, 2);
        $projectDir = dirname($rootDir, 1);

        $parser = new Parser();

        $config = $parser->parse(
            file_get_contents($rootDir . '/config/packages/background/supervisor.conf.yml')
        );

        $outputFile = [];
        foreach ($config as $program => $params) {
            $outputBlock = '';
            $keys = array_keys($params);
            if (array_diff(['directory', 'command'], $keys)) {
                throw new \Exception('Keys "directory", "command" are required for ' . $program);
            }

            $outputBlock .= "[program:$program]" . PHP_EOL;
            $outputBlock .= "user={$user}" . PHP_EOL;

            $directory = $projectDir;
            if ($params['directory']) {
                $directory .=  '/' . $params['directory'];
            }
            unset($params['directory']);

            foreach ($params as $name => $value) {
                $outputBlock .= "{$name}={$value}" . PHP_EOL;
            }

            $outputBlock .= "directory={$directory}" . PHP_EOL;
            $outputBlock .= "stdout_logfile={$logsDir}/{$program}.log.txt" . PHP_EOL;
            $outputBlock .= "stderr_logfile={$logsDir}/{$program}.error.txt" . PHP_EOL;

            $outputFile[] = $outputBlock;
        }

        $destination = new \SplFileObject($destinationDir . '/supervisor.conf', 'w+');
        $destination->fwrite(implode(PHP_EOL, $outputFile));
    }
}