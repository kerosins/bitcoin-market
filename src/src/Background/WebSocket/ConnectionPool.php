<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Background\WebSocket;

use Kerosin\Component\SessionSerializer;
use Predis\Client;
use Swoole\WebSocket\Server;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use User\Entity\User;

class ConnectionPool
{
    /**
     * Redis client
     *
     * @var Client
     */
    private $redis;

    private $poolKey = 'websocket:connection:pool';

    private $searchByUserIdKeyPattern = 'websocket:connection:user:';
    private $searchBySessionKeyPattern = 'websocket:connection:session:';
    private $searchByDescriptorPattern = 'websocket:connection:descriptor:';
    private $descriptorPoolPattern = 'websocket:connection:descriptor_pool:';

    /**
     * Cookien name which contain socket token
     *
     * @var string
     */
    private $cookieName = 'socketToken';

    /**
     * @var Session
     */
    private $session;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    public function __construct(
        Client $client,
        Session $session,
        TokenStorage $tokenStorage
    ) {
        $this->redis = $client;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response = $event->getResponse();

        $userId = null;

//        if ($this->tokenStorage->getToken()->getUser()) {
//            $userId = $this->tokenStorage->getToken()->getUser()->getId();
//        }

        $token = $this->register($this->session->getId(), $userId);
        //set cookie
        $cookie = new Cookie(
            $this->cookieName,
            $token,
            0,
            '/',
            null,
            false,
            false
        );
        $response->headers->setCookie($cookie);
    }

    /**
     * @param $sessionId
     * @param null $userId
     *
     * @return string - token
     */
    private function register($sessionId, $userId = null)
    {
        $token = hash('crc32b', $sessionId . $userId);

        $data = $this->getDataByToken($token);
        if ($data && !empty($data['fd'])) {
            return $token;
        }

        $this->redis->multi();
        $this->redis->hset($this->poolKey, $token, json_encode([
            'session' => $sessionId,
            'user' => $userId,
            'isActive' => false
        ]));

        $this->redis->set($this->searchBySessionKeyPattern . $sessionId, $token);
        if ($userId) {
            $this->redis->set($this->searchByUserIdKeyPattern . $userId, $token);
        }

        $this->redis->exec();

        return $token;
    }

    /**
     * Update a connection descriptor
     *
     * @param $token
     * @param $descriptor
     *
     * @return bool - true if success update, false otherwise
     */
    public function update($token, $descriptor, $isDelete = true)
    {
        $data = $this->getDataByToken($token);
        if (!$data) {
            return false;
        }

        $isActive = true;

        $descriptorPoolKey = $this->descriptorPoolPattern . $token;
        if ($isDelete) {
            $this->redis->srem($descriptorPoolKey, $descriptor);
            $members = $this->redis->smembers($descriptor);
            $this->redis->del([ $this->searchByDescriptorPattern . $descriptor ]);
            if (count($members) === 0) {
                $isActive = false;
                $this->redis->del([$descriptorPoolKey]);
            }
        } else {
            $this->redis->sadd($descriptorPoolKey, [$descriptor]);
            $this->redis->set($this->searchByDescriptorPattern . $descriptor, $token);
        }

        $data['isActive'] = $isActive;

        $this->redis->hset($this->poolKey, $token, json_encode($data));
        return true;
    }

    /**
     * Drop descriptor when user has been disconnected
     *
     * @param $descriptor
     */
    public function dropByDescriptor($descriptor)
    {
        $token = $this->redis->get($this->searchByDescriptorPattern . $descriptor);
        $this->update($token, $descriptor, true);
    }

    /**
     * @param $token
     * @return bool|array
     */
    public function getDataByToken($token)
    {
        $data = $this->redis->hget($this->poolKey, $token);
        if (!$data) {
            return false;
        }

        $data = json_decode($data, true);
        if (json_last_error() !== JSON_ERROR_NONE) {//corrupted data
            return false;
        }

        return $data;
    }

    public function getTokenByUserId($userId)
    {
        return $this->redis->get($this->searchByUserIdKeyPattern . $userId);
    }

    public function getTokenBySession($session)
    {
        return $this->redis->get($this->searchBySessionKeyPattern . $session);
    }
}