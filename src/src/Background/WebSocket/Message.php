<?php
/**
 * Message class file.
 */

namespace Background\WebSocket;

use GuzzleHttp\RequestOptions;
use Kerosin\DependencyInjection\Contract\LoggerContract;
use Kerosin\DependencyInjection\Contract\LoggerContractTrait;
use Predis\Client;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Todo хз, насколько нормально, возможно в дальнейшем доведу до ума
 * Class Message
 * @package Background\WebSocket
 */
class Message implements LoggerContract
{
    use LoggerContractTrait;

    public const TYPE_DEFAULT = 'message';

    /**
     * @var ConnectionPool
     */
    private $pool;

    /**
     * @var int
     */
    private $ttl;

    /**
     * @var string
     */
    private $socketServer;

    /**
     * @var Client
     */
    private $redis;

    /**
     * @var Session
     */
    private $session;

    private $deferredKeyPattern = 'websocket:deferred:token:';

    /**
     * Message constructor.
     * @param ConnectionPool $pool
     * @param int $ttl - ttl of message in seconds
     */
    public function __construct(
        ConnectionPool $pool,
        Client $redis,
        Session $session,
        string $socketServer,
        int $ttl
    ) {
        $this->pool = $pool;
        $this->ttl = $ttl;
        $this->session = $session;
        $this->redis = $redis;
        $this->socketServer = $socketServer;
    }

    /**
     * Get a single deferred message (current implementation)
     *
     * @param $token
     * @return string
     */
    public function getDeferredMessages($token)
    {
        $message = $this->redis->get($this->deferredKeyPattern . $token);
        $this->redis->del([$this->deferredKeyPattern . $token]);

        return $message;
    }

    /**
     * Push message uses User's Id
     *
     * @param $userId
     * @param string $type
     * @param array $payload
     */
    public function pushToUserId($userId, string $type, array $payload = [])
    {
        $token = $this->pool->getTokenByUserId($userId);
        if (!$token) {
            return;
        }
        $this->pushToToken($token, $type, $payload);
    }

    /**
     * Push message to current user
     *
     * @param string $type
     * @param array $payload
     */
    public function pushToCurrentUser(string $type = self::TYPE_DEFAULT, array $payload = [])
    {
        $token = $this->pool->getTokenBySession($this->session->getId());
        if (!$token) {
            return;
        }
        $this->pushToToken($token, $type, $payload);
    }

    /**
     * Push message uses WebSocket Token
     * todo add log
     *
     * @param $token
     * @param string $type
     * @param array $payload
     */
    public function pushToToken($token, string $type, array $payload = [])
    {
        $stream = @stream_socket_client($this->socketServer, $errno, $errstr);

        if (!$stream) {
            $this->logger->error("Websocket message transmission error. Error number: {$errno}, error message: ${errstr}");
            return;
        }

        fwrite($stream, json_encode([
            'token' => $token,
            'type' => $type,
            'message' => $payload
        ]));

        fclose($stream);
    }
}