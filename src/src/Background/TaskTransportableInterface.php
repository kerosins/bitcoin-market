<?php
/**
 * @author Kerosin
 */

namespace Background;

use Background\Entity\ProgressModel;

interface TaskTransportableInterface
{
    /**
     * @param string $workerId
     * @return bool
     */
    public function subscribe(string $workerId);

    /**
     * @return ProgressModel
     */
    public function getTask();

    /**
     * @param $worker
     * @param array $data
     * @return mixed
     * @internal param ProgressModel $progressModel
     */
    public function send($worker, array $data);

    public function fail(ProgressModel $progressModel, $message);

    public function success(ProgressModel $progressModel, $result);
}