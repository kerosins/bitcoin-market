<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Background\DependencyInjection;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class BackgroundConfigurationExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $logsDir = $container->getParameter('kernel.logs_dir');
        $logsDir = $logsDir . '/background';

        $container->setParameter('background.logs_dir', $logsDir);
        $container->setParameter('zenith.background.logs_dir', $logsDir);
    }
}