<?php
/**
 * @author kerosin
 */

namespace Background\DependencyInjection;


use Background\Worker\WorkerServer;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class WorkerCompiler implements CompilerPassInterface
{
    public const TAG = 'background.task_worker';

    /**
     * Add workers
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(WorkerServer::class)) {
            return;
        }

        $definition = $container->getDefinition(WorkerServer::class);
        $workers = $container->findTaggedServiceIds(self::TAG);

        foreach ($workers as $worker => $tags) {
            foreach ($tags as $tag => $attributes) {
                if (!isset($attributes['id'])) {
                    continue;
                }

                $limit = $attributes['limit'] ?? 1;

                $definition->addMethodCall('addTaskWorker', [
                    $attributes['id'],
                    new Reference($worker),
                    $limit
                ]);
            }
        }
    }

}