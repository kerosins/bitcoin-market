<?php
/**
 * Created by PhpStorm.
 * User: kerosin
 * Date: 07.04.2018
 * Time: 3:22
 */

namespace Content\SlugRoute\Form;

use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\Form\DataTransformer\CallbackWithOptionsTransformer;
use Kerosin\Form\DataTransformer\Filter\TrimFilter;
use Content\ORM\Entity\Slug;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class SlugType extends AbstractType implements EntityManagerContract
{
    use EntityManagerContractTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug', TextType::class, [
                'label' => 'Alias URI (without leading or ending /)',
                'attr' => [
                    'autocomplete' => 'Off'
                ]
            ])
            ->add('isMain')
            ->add('weight')
        ;

        $builder->get('slug')->addModelTransformer(new TrimFilter(" \t\n\r\0\x0B/"));

        $builder
            ->addModelTransformer(new CallbackWithOptionsTransformer(
                [$this, 'toForm'],
                [$this, 'toEntity'],
                $options
            ));
    }

    public function uniqueAlias(Slug $pageAlias, ExecutionContextInterface $context, $payload)
    {
        $repo = $this->entityManager->getRepository(Slug::class);

        if ($repo->existBySlug($pageAlias->getSlug(), $pageAlias->getTarget(), $pageAlias->getId())) {
            $context->buildViolation("Slug for {$pageAlias->getTarget()} already exist, please change that one")
                ->atPath('slug')
                ->addViolation();
        }
    }

    /**
     * Transform value to form view
     *
     * @param $value
     *
     * @return mixed
     */
    public function toForm($value)
    {
        return $value;
    }

    /**
     * Transform value to entity
     *
     * @param $value
     * @param $options
     *
     * @return Slug
     */
    public function toEntity($value, $options)
    {
        if (!$value) {
            return $value;
        }

        if ($value instanceof Slug) {
            $value->setTarget($options['target']);
        }

        return $value;
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Slug::class,
            'target' => null,
            'constraints' => [
                new Callback([
                    'callback' => [$this, 'uniqueAlias']
                ])
            ]
        ]);

        $resolver->setAllowedTypes('target', 'string');
        $resolver->setAllowedValues('target', array_keys(Slug::$targetLabel));
    }
}