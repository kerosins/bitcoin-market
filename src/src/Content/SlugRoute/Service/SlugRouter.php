<?php
/**
 * @author Kondaurov
 */

namespace Content\SlugRoute\Service;


use Content\SlugRoute\Sluggable;
use Content\SlugRoute\Component\SlugRoute;
use Doctrine\Common\Collections\ArrayCollection;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\DependencyInjection\Contract\TaggedCacheContract;
use Kerosin\DependencyInjection\Contract\TaggedCacheContractTrait;
use Content\ORM\Entity\Slug;
use Content\ORM\Repository\SlugRepository;
use Shop\Service\ActiveSeller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

class SlugRouter implements EntityManagerContract, TaggedCacheContract
{
    use EntityManagerContractTrait, TaggedCacheContractTrait;

    /**
     * @var SlugRepository
     */
    private $slugRepository;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var array
     */
    private $routeMap = [];

    /**
     * @var string
     */
    private $slugParameter;

    /**
     * @var string
     */
    private $routeSlugSuffix;

    /**
     * @var ActiveSeller
     */
    private $activeSeller;

    /**
     * @var string
     */
    private $routeSellerSuffix;

    /**
     * @var string
     */
    private $sellerParameter;

    public const
        IGNORE_SELLER_PARAMETER = '_ignoreSeller',
        ABSOLUTE_PARAMETER = '_absolute';

    private static $reservedParamNames = [self::ABSOLUTE_PARAMETER, self::IGNORE_SELLER_PARAMETER];

    public function __construct(
        RouterInterface $router,
        ActiveSeller $activeSeller,
        string $slugParameter,
        string $routeSlugSuffix,
        string $sellerParameter,
        string $routeSellerSuffix,
        array $routeMap
    ) {
        $this->router = $router;
        $this->activeSeller = $activeSeller;
        $this->routeMap = new ArrayCollection();
        foreach ($routeMap as $value) {
            $this->routeMap->set($value['type'], $value);
        }

        $this->slugParameter = $slugParameter;
        $this->routeSlugSuffix = $routeSlugSuffix;
        $this->sellerParameter = $sellerParameter;
        $this->routeSellerSuffix = $routeSellerSuffix;

        $this->routeMap = new ArrayCollection($this->buildRouteMap($routeMap));
    }

    /**
     * Generates path
     *
     * @param string $type
     * @param int $id
     * @param bool $absolute
     * @param bool $ignoreSeller
     *
     * @return string
     */
    public function generate(
        string $type,
        int $id = null,
        array $params = []
    ) {
        if (!$this->hasType($type)) {
            throw new \RuntimeException('Unknown slug type ' . $type);
        }

        $absolute = $params[self::ABSOLUTE_PARAMETER] ?? false;
        $ignoreSeller = $params[self::IGNORE_SELLER_PARAMETER] ?? false;

        foreach (self::$reservedParamNames as $paramName) {
            unset($params[$paramName]);
        }

        $referenceType = $absolute ? RouterInterface::ABSOLUTE_URL : RouterInterface::ABSOLUTE_PATH;
        /** @var SlugRoute $slugRoute */
        $slugRoute = $this->getByType($type);
        /** @var \Content\ORM\Entity\Slug $slug */
        $slug = $this->getMainSlugByIdAndType($type, $id);

        if (!$slug || !$slugRoute->getAliasRoute()) {
            if (!$id) {
                throw new \RuntimeException('Identificator for main route is required');
            }

            $params[$slugRoute->getIdentifierParameter()] = $id;

            return $this->router->generate(
                $slugRoute->getMainRouteName(),
                $params,
                $referenceType
            );
        }

        //if active seller, then generates url for it
        $params[$this->slugParameter] = $slug;
        $seller = $this->activeSeller->get();
        if (!$ignoreSeller && $seller && $slugRoute->getSellerRoute()) {
            $params[$this->sellerParameter] = $seller->getId();
            $params[$slugRoute->getIdentifierParameter()] = $id;

            return $this->router->generate($slugRoute->getSellerRouteName(), $params, $referenceType);
        }

        if ($slugRoute->getAliasRoute()->getRequirement($slugRoute->getIdentifierParameter())) {
            $params[$slugRoute->getIdentifierParameter()] = $id;
        }

        return $this->router->generate($slugRoute->getAliasRouteName(), $params, $referenceType);
    }

    /**
     * Generates path
     *
     * @param Sluggable $sluggable
     * @param bool $absolute
     *
     * @return string
     */
    public function generateFromObject(Sluggable $sluggable, array $params = [])
    {
        return $this->generate($sluggable->getSlugType(), $sluggable->getIdentifier(), $params);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|null
     */
    public function resolveRequest(Request $request)
    {
        $route = $request->get('_route');
        $slugRoute = $this->routeMap->filter(function (SlugRoute $slugRoute) use ($route) {
            return $slugRoute->matchRouteName($route);
        });
        if ($slugRoute->count() === 0) {
            return null;
        }
        /** @var SlugRoute $slugRoute */
        $slugRoute = $slugRoute->first();

        if ($slugRoute->isMainRoute($route)) {
            return null;
        }

        $isMainSlug = $this->getSlugRepository()->isMainSlug(
            $request->get($this->slugParameter),
            $slugRoute->getType(),
            $request->get($slugRoute->getIdentifierParameter())
        );

        if ($isMainSlug) {
            return null;
        }

        return new RedirectResponse(
            $this->generate(
                $slugRoute->getType(),
                $request->get($slugRoute->getIdentifierParameter()),
                $request->request->all()
            ),
            301
        );
    }

    /**
     * @param string $routeName
     *
     * @return bool
     */
    public function hasRouteName(string $routeName)
    {
        return $this->routeMap->exists(function ($index, SlugRoute $slugRoute) use ($routeName) {
            return $slugRoute->matchRouteName($routeName);
        });
    }

    /**
     * @param string $type
     * @param int $id
     */
    private function getMainSlugByIdAndType(string $type, int $id = null)
    {
        $cacheItem = $this->taggedCache->getItem("main_slug_{$type}_{$id}");
        if (!$cacheItem->isHit()) {
            /** @var Slug $slug */
            $slug = $this->getSlugRepository()->getMainSlugByIdAndType($type, $id);
            if (!$slug) {
                return null;
            }
            $cacheItem->set($slug->getSlug());
            $cacheItem->tag(["main_slug_{$type}_{$slug->getTargetId()}"]);
            $this->taggedCache->save($cacheItem);
        }

        return $cacheItem->get();
    }

    /**
     * @param string $type
     *
     * @return null|SlugRoute
     */
    private function getByType(string $type)
    {
        $slugRoute = $this->routeMap->filter(function (SlugRoute $slugRoute) use ($type) {
            return $slugRoute->getType() === $type;
        });
        if ($slugRoute->count() === 0) {
            return null;
        }
        return $slugRoute->first();
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    private function hasType(string $type)
    {
        return $this->routeMap->exists(function ($index, SlugRoute $slugRoute) use ($type) {
            return $slugRoute->getType() === $type;
        });
    }

    /**
     * @param $routeMap
     *
     * @return array
     */
    private function buildRouteMap($routeMap)
    {
        return array_map(function ($route) {
            $main = $this->router->getRouteCollection()->get($route['route']);
            $aliasRouteName = "{$route['route']}.{$this->routeSlugSuffix}";
            $alias = $this->router->getRouteCollection()->get($aliasRouteName);
            $sellerRouteName = "{$route['route']}.{$this->routeSellerSuffix}";
            $seller = $this->router->getRouteCollection()->get($sellerRouteName);

            return (new SlugRoute())
                ->setMainRoute($main)
                ->setMainRouteName($route['route'])
                ->setAliasRoute($alias)
                ->setAliasRouteName($aliasRouteName)
                ->setSellerRoute($seller)
                ->setSellerRouteName($sellerRouteName)
                ->setType($route['type'])
                ->setIdentifierParameter($route['identifierParam'])
                ;
        }, $routeMap);
    }

    /**
     * @return \Content\ORM\Repository\SlugRepository
     */
    private function getSlugRepository()
    {
        if (!$this->slugRepository) {
            $this->slugRepository = $this->entityManager->getRepository(Slug::class);
        }

        return $this->slugRepository;
    }
}