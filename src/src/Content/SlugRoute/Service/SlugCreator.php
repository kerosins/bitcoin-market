<?php
/**
 * @author Kondaurov
 */

namespace Content\SlugRoute\Service;


use Content\SlugRoute\Sluggable;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Content\SlugRoute\Component\Slugger;
use Content\ORM\Entity\Slug;

class SlugCreator implements EntityManagerContract
{
    use EntityManagerContractTrait;

    /**
     * Generates slug from target object
     *
     * @param \Content\SlugRoute\Sluggable $sluggable
     */
    public function generate(Sluggable $sluggable, $isMain = true, $flush = true)
    {
        $method = "get{$sluggable->getSlugAttribute()}";
        $slugStr = Slugger::slugify($sluggable->$method());

        $slug = new Slug();
        $slug
            ->setSlug($slugStr)
            ->setTarget($sluggable->getSlugType())
            ->setWeight(0)
            ->setIsMain($isMain)
            ->setTargetId($sluggable->getIdentifier());

        $slugRepository = $this->entityManager->getRepository(Slug::class);

        $counter = 1;
        while ($slugRepository->existBySlug($slug->getSlug(), $sluggable->getSlugType())) {
            $slug->setSlug("{$slugStr}-{$counter}");
            $counter++;
        }

        if ($flush) {
            $slugRepository->save($slug);
        }

        return $slug;
    }

    /**
     * @param \Content\SlugRoute\Sluggable $sluggable
     *
     * @return null|\Content\ORM\Entity\Slug
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function generateIfNotExists(Sluggable $sluggable, $isMain = true, $flush = true)
    {
        $exist = $this
            ->entityManager
            ->getRepository(Slug::class)
            ->existByTarget($sluggable->getSlugType(), $sluggable->getIdentifier());
        return !$exist ? $this->generate($sluggable, $isMain, $flush) : null;
    }
}