<?php
/**
 * @author Kondaurov
 */

namespace Content\SlugRoute\Widget;


use Kerosin\Component\Widget;
use Content\SlugRoute\Sluggable;
use Content\SlugRoute\Service\SlugRouter;

class SlugWidget extends Widget
{
    /**
     * @var \Content\SlugRoute\Service\SlugRouter
     */
    private $generator;

    public function getFunctions()
    {
        return [
            new \Twig_Function('slugPath', [$this, 'slugPath'])
        ];
    }

    public function getFilters()
    {
        return [
            new \Twig_Filter('slug', [$this, 'slugPathFromEntity'])
        ];
    }

    /**
     * @param string $type
     * @param int $id
     * @param array $params
     */
    public function slugPath(string $type, int $id, array $params = [])
    {
        $params = array_merge($params, [
            SlugRouter::ABSOLUTE_PARAMETER => $params[SlugRouter::ABSOLUTE_PARAMETER] ?? false,
            SlugRouter::IGNORE_SELLER_PARAMETER => $params[SlugRouter::IGNORE_SELLER_PARAMETER] ?? false
        ]);
        return $this->getRouteGenerator()->generate($type, $id, $params);
    }

    /**
     * @param Sluggable $sluggable
     * @param array $params
     */
    public function slugPathFromSluggable(Sluggable $sluggable, array $params = [])
    {
        $params = array_merge($params, [
            SlugRouter::ABSOLUTE_PARAMETER => $params[SlugRouter::ABSOLUTE_PARAMETER] ?? false,
            SlugRouter::IGNORE_SELLER_PARAMETER => $params[SlugRouter::IGNORE_SELLER_PARAMETER] ?? false
        ]);
        return $this->getRouteGenerator()->generateFromObject($sluggable, $params);
    }

    /**
     * @return SlugRouter
     */
    private function getRouteGenerator()
    {
        if (!$this->generator) {
            $this->generator = $this->container->get(SlugRouter::class);
        }

        return $this->generator;
    }
}