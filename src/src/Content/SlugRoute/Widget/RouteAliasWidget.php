<?php
/**
 * Created by PhpStorm.
 * User: kerosin
 * Date: 10.04.2018
 * Time: 0:47
 */

namespace Content\SlugRoute\Widget;

use Kerosin\Component\Widget;
use Content\SlugRoute\Service\SlugRouter;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class RouteAliasWidget extends Widget
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var SlugRouter
     */
    private $slugRouter;

    /**
     * @return object|\Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    private function getRouter()
    {
        if (!$this->router) {
            $this->router = $this->container->get('router');
        }
        return $this->router;
    }

    private function getSlugRouter()
    {
        if (!$this->slugRouter) {
            $this->slugRouter = $this->container->get(SlugRouter::class);
        }

        return $this->slugRouter;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('pageAlias', [$this, 'pageAlias']),
            new \Twig_SimpleFunction('relativePageAlias', [$this, 'relativePageAlias']),
        ];
    }

    /**
     * @param string $type
     * @param $targetId
     * @param null $position
     */
    public function pageAlias(string $type, int $targetId, $ignoreSeller = false)
    {
        return $this->getSlugRouter()->generate($type, $targetId, [
            SlugRouter::IGNORE_SELLER_PARAMETER => $ignoreSeller
        ]);
    }

    public function relativePageAlias(string $type, int $targetId, $ignoreSeller = false)
    {
        return $this->getSlugRouter()->generate(
            $type,
            $targetId,
            [
                SlugRouter::ABSOLUTE_PARAMETER => false,
                SlugRouter::IGNORE_SELLER_PARAMETER => $ignoreSeller
            ]
        );
    }
}