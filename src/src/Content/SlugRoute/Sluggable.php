<?php
/**
 * @author Kondaurov
 */

namespace Content\SlugRoute;


use Doctrine\Common\Collections\Collection;
use Content\ORM\Entity\Slug;

interface Sluggable
{
    /**
     * Get object entity type
     *
     * @return string
     */
    public function getSlugType(): string;

    /**
     * Get slug attribute
     *
     * @return string
     */
    public function getSlugAttribute(): string;

    /**
     * Get object identifier
     *
     * @return int
     */
    public function getIdentifier(): int;

    /**
     * Get array of slugs
     *
     * @return \Content\ORM\Entity\Slug[]|Collection
     */
    public function getSlugs();

    /**
     * @param \Content\ORM\Entity\Slug[]|Collection $slugs
     *
     * @return mixed
     */
    public function setSlugs($slugs);

    /**
     * @return mixed
     */
    public function addSlug(Slug $slug);
}