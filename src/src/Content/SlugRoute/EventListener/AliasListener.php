<?php
/**
 * Created by PhpStorm.
 * User: kerosin
 * Date: 11.04.2018
 * Time: 1:15
 */

namespace Content\SlugRoute\EventListener;

use Content\SlugRoute\Service\SlugRouter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\RouterInterface;

class AliasListener
{
    /**
     * @var \Symfony\Component\Routing\Router
     */
    private $router;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var \Content\SlugRoute\Service\SlugRouter
     */
    private $slugRouter;

    public function __construct(
        ContainerInterface $container,
        SlugRouter $slugRouter,
        RouterInterface $router
    ) {
        $this->container = $container;
        $this->router = $router;
        $this->slugRouter = $slugRouter;
    }

    /**
     * On kernel.request handler
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->getRequestType() !== HttpKernelInterface::MASTER_REQUEST) {
            return;
        }

        try {
            $redirect = $this->slugRouter->resolveRequest($event->getRequest());
        } catch (\RuntimeException $e) {
            throw new NotFoundHttpException();
        }

        if ($redirect) {
            $event->setResponse($redirect);
        }
    }
}