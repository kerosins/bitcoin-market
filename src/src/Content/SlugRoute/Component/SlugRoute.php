<?php
/**
 * @author Kondaurov
 */

namespace Content\SlugRoute\Component;


use Symfony\Component\Routing\Route;

class SlugRoute
{
    /**
     * @var Route
     */
    private $mainRoute;

    /**
     * @var string
     */
    private $mainRouteName;

    /**
     * @var Route
     */
    private $aliasRoute;

    /**
     * @var string
     */
    private $aliasRouteName;

    /**
     * @var Route
     */
    private $sellerRoute;

    /**
     * @var string
     */
    private $sellerRouteName;

    /**
     * @var string
     */
    private $identifierParameter;

    /**
     * @var string
     */
    private $type;

    /**
     * @return Route
     */
    public function getMainRoute(): ?Route
    {
        return $this->mainRoute;
    }

    /**
     * @param Route $mainRoute
     *
     * @return SlugRoute
     */
    public function setMainRoute(Route $mainRoute = null): SlugRoute
    {
        $this->mainRoute = $mainRoute;
        return $this;
    }

    /**
     * @return Route
     */
    public function getAliasRoute(): ?Route
    {
        return $this->aliasRoute;
    }

    /**
     * @param Route $aliasRoute
     *
     * @return SlugRoute
     */
    public function setAliasRoute(Route $aliasRoute = null): SlugRoute
    {
        $this->aliasRoute = $aliasRoute;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdentifierParameter(): ?string
    {
        return $this->identifierParameter;
    }

    /**
     * @param string $identifierParameter
     *
     * @return SlugRoute
     */
    public function setIdentifierParameter(string $identifierParameter): SlugRoute
    {
        $this->identifierParameter = $identifierParameter;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return SlugRoute
     */
    public function setType(string $type): SlugRoute
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainRouteName(): ?string
    {
        return $this->mainRouteName;
    }

    /**
     * @param string $mainRouteName
     *
     * @return SlugRoute
     */
    public function setMainRouteName(string $mainRouteName): SlugRoute
    {
        $this->mainRouteName = $mainRouteName;
        return $this;
    }

    /**
     * @return string
     */
    public function getAliasRouteName(): ?string
    {
        return $this->aliasRouteName;
    }

    /**
     * @param string $aliasRouteName
     *
     * @return SlugRoute
     */
    public function setAliasRouteName(string $aliasRouteName): SlugRoute
    {
        $this->aliasRouteName = $aliasRouteName;
        return $this;
    }

    /**
     * @return Route
     */
    public function getSellerRoute(): ?Route
    {
        return $this->sellerRoute;
    }

    /**
     * @param Route $sellerRoute
     *
     * @return SlugRoute
     */
    public function setSellerRoute(Route $sellerRoute = null): SlugRoute
    {
        $this->sellerRoute = $sellerRoute;
        return $this;
    }

    /**
     * @return string
     */
    public function getSellerRouteName(): ?string
    {
        return $this->sellerRouteName;
    }

    /**
     * @param string $sellerRouteName
     *
     * @return SlugRoute
     */
    public function setSellerRouteName(string $sellerRouteName = null): SlugRoute
    {
        $this->sellerRouteName = $sellerRouteName;
        return $this;
    }

    public function isMainRoute(string $routeName)
    {
        return $this->mainRouteName === $routeName;
    }

    /**
     * @param string $routeName
     *
     * @return bool
     */
    public function matchRouteName(string $routeName): bool
    {
        return $this->mainRouteName === $routeName || ($this->aliasRoute && $this->aliasRouteName === $routeName);
    }
}