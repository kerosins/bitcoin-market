<?php
/**
 * Created by PhpStorm.
 * User: kerosin
 * Date: 10.04.2018
 * Time: 23:54
 */

namespace Content\SlugRoute\Component;


class Slugger
{
    /**
     * Slugify the string
     *
     * @param string $string
     *
     * @return string
     */
    public static function slugify(string $string): string
    {
        $string = strtolower($string);
        $string = preg_replace('/[^a-z0-9\s]/', '', $string);
        //replace more than twice space
        $string = preg_replace('/(\s){2,}/', ' ', $string);

        $string = preg_replace('/\s/', '-', $string);

        return $string;
    }
}