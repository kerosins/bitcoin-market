<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ORM\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Content\ItemBlock\ItemBlockParameters;

/**
 * Class ItemBlock
 * @package Kerosin\Entity
 *
 * @ORM\Entity()
 * @ORM\Table()
 */
class ItemBlock
{
    use BaseMapping;

    const
        AREA_MAIN_PAGE_1 = 'mainPage1',
        AREA_MAIN_PAGE_2 = 'mainPage2';

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @var array
     *
     * @ORM\Column(type="json", options={"jsonb": true})
     */
    private $params = [];

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $enable = true;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $displayInMain = true;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $weight = 0;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $area;

    public static $areaLabels = [
        self::AREA_MAIN_PAGE_1 => 'Main Page 1 (Top)',
        self::AREA_MAIN_PAGE_2 => 'Main Page 2 (Bottom)'
    ];

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return ItemBlock
     */
    public function setTitle(string $title): ItemBlock
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ItemBlock
     */
    public function setType(string $type): ItemBlock
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return array|ItemBlockParameters
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param mixed $params
     * @return ItemBlock
     */
    public function setParams($params): ItemBlock
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @return bool
     */
    public function getEnable(): ?bool
    {
        return $this->enable;
    }

    /**
     * @param bool $enable
     * @return ItemBlock
     */
    public function setEnable(bool $enable): ItemBlock
    {
        $this->enable = $enable;
        return $this;
    }

    /**
     * @return bool
     */
    public function getDisplayInMain(): ?bool
    {
        return $this->displayInMain;
    }

    /**
     * @param bool $displayInMain
     * @return ItemBlock
     */
    public function setDisplayInMain(bool $displayInMain): ItemBlock
    {
        $this->displayInMain = $displayInMain;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     *
     * @return ItemBlock
     */
    public function setWeight(int $weight): ItemBlock
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return string
     */
    public function getArea(): ?string
    {
        return $this->area;
    }

    /**
     * @param string $area
     * @return ItemBlock
     */
    public function setArea(string $area): ItemBlock
    {
        $this->area = $area;
        return $this;
    }

    public function getReadableArea()
    {
        return array_key_exists($this->area, self::$areaLabels) ? self::$areaLabels[$this->area] : 'Unknown';
    }
}