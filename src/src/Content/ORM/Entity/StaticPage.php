<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ORM\Entity;

use Content\ORM\Entity\Slug;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;
use Kerosin\Doctrine\ORM\MetaTagsMapping;
use Kerosin\Doctrine\ORM\UpdateTimestamps;
use Content\Seo\SeoDataProviderInterface;
use Content\SlugRoute\Sluggable;

/**
 * Class StaticPage
 * @package Content\ORM\Entity
 *
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class StaticPage implements SeoDataProviderInterface, Sluggable
{
    use BaseMapping;
    use UpdateTimestamps;
    use MetaTagsMapping;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $body;

    /**
     * @var Collection
     */
    private $slugs;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return StaticPage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return StaticPage
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    public function getSlugType(): string
    {
        return Slug::TARGET_STATIC_PAGE;
    }

    public function getSlugAttribute(): string
    {
        return 'title';
    }

    public function getIdentifier(): int
    {
        return $this->id;
    }

    public function getSlugs()
    {
        return $this->slugs;
    }

    public function setSlugs($slugs)
    {
        $this->slugs = new ArrayCollection($slugs);
    }

    public function addSlug(Slug $slug)
    {
        if (!$this->slugs instanceof Collection) {
            $this->slugs = new ArrayCollection();
        }

        $this->slugs->add($slug);
        return $this;
    }


}