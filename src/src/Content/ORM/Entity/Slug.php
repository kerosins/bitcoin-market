<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ORM\Entity;
use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\BaseMapping;

/**
 * Class Slug
 * @package Shop\Entity
 *
 * @ORM\Entity(repositoryClass="Content\ORM\Repository\SlugRepository")
 * @ORM\Table(
 *     indexes={
 *         @ORM\Index(name="idx_slug", columns={"slug"}),
 *         @ORM\Index(name="idx_slug_target_target_id", columns={"target", "target_id"})
 *     }
 * )
 */
class Slug
{
    use BaseMapping;

    /**
     * target's constants
     */
    public const
        TARGET_STATIC_PAGE = 'staticPage',
        TARGET_PRODUCT = 'product',
        TARGET_CATEGORY = 'category';

    public static $targetLabel = [
        self::TARGET_STATIC_PAGE => 'Static Page',
        self::TARGET_PRODUCT => 'Product',
        self::TARGET_CATEGORY => 'Category'
    ];

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1000)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $target;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $targetId;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $weight;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isMain = false;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Set target
     *
     * @param string $target
     *
     * @return Slug
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set targetId
     *
     * @param integer $targetId
     *
     * @return Slug
     */
    public function setTargetId($targetId)
    {
        $this->targetId = $targetId;

        return $this;
    }

    /**
     * Get targetId
     *
     * @return integer
     */
    public function getTargetId()
    {
        return $this->targetId;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Slug
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set uri
     *
     * @param string $slug
     *
     * @return Slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return bool
     */
    public function isMain(): ?bool
    {
        return $this->isMain;
    }

    /**
     * @param bool $isMain
     */
    public function setIsMain(bool $isMain)
    {
        $this->isMain = $isMain;
        return $this;
    }


}
