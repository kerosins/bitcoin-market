<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ORM\Repository;


use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Query;
use Kerosin\Doctrine\ORM\EntityRepository;
use Content\ORM\Entity\Slug;
use Content\SlugRoute\Sluggable;

/**
 * Class PageAliasRepository
 * @package Seo\Repository
 *
 * @method Slug findOneByUri(string $uri)
 */
class SlugRepository extends EntityRepository
{
    /**
     * Find aliases by target object
     *
     * @param Sluggable $target
     *
     * @return \Content\ORM\Entity\Slug[]|null
     */
    public function findByTargetObject(Sluggable $sluggable, $hydration = Query::HYDRATE_OBJECT)
    {
        return $this->findByTargetAndTargetId($sluggable->getSlugType(), $sluggable->getIdentifier(), $hydration);
    }

    /**
     * @param string $slug
     * @param string $type
     * @param int $hydration
     *
     * @return Slug|null
     */
    public function findOneBySlugAndType(string $slug, string $type, $hydration = Query::HYDRATE_OBJECT)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('lower(s.slug) = lower(:slug) AND s.target = :target')
            ->setParameters([
                'slug' => $slug,
                'target' => $type
            ])
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult($hydration);
    }

    /**
     * Check if alias with specified uri already exist
     *
     * @param string $slug
     * @param string $type
     * @param int|null $exceptId
     *
     * @return bool
     */
    public function existBySlug(string $slug, string $type, int $exceptId = null)
    {
        $builder = $this->createQueryBuilder('s')
            ->andWhere('s.slug = :slug and s.target = :type')
            ->setParameters([
                'slug' => $slug,
                'type' => $type
            ])
        ;

        if ($exceptId !== null) {
            $builder->andWhere('s.id != :except')->setParameter('except', $exceptId);
        }

        return null !== $builder
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    /**
     * @param $target
     * @param $targetId
     * @param int $hydration
     *
     * @return array
     */
    public function findByTargetAndTargetId($target, $targetId, $hydration = Query::HYDRATE_ARRAY)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.target = :target')
            ->andWhere('s.targetId = :targetId')
            ->setParameters([
                'target' => $target,
                'targetId' => $targetId
            ])
            ->orderBy('s.weight', 'DESC')
            ->getQuery()
            ->getResult($hydration);
    }

    /**
     * Find list of main aliases
     *
     * @return \Content\ORM\Entity\Slug[]
     */
    public function findMainAliases()
    {
        return $this->createQueryBuilder('pa')
            ->andWhere('pa.isMain = :is_main')
            ->setParameter('is_main', true)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $target
     * @param int $targetId
     *
     * @throws
     *
     * @return \Content\ORM\Entity\Slug|array|null
     */
    public function findMainAliasByTargetAndTargetId(string $target, int $targetId, $hydration = Query::HYDRATE_ARRAY)
    {
        return $this->createQueryBuilder('pa')
            ->andWhere('pa.target = :target AND pa.targetId = :target_id AND pa.isMain = :is_main')
            ->setParameters([
                'target' => $target,
                'target_id' => $targetId,
                'is_main' => true
            ])
            ->getQuery()
            ->setHydrationMode($hydration)
            ->getOneOrNullResult();
    }

    /**
     * Find main alias by uri
     *
     * @param string $uri
     *
     * @return null|Slug
     */
    public function findMainAliasByUrl(string $uri): ?Slug
    {
        return $this->createQueryBuilder('pa')
                ->andWhere('pa.uri = :uri AND pa.isMain = :is_main')
                ->setParameters([
                    'uri' => $uri,
                    'is_main' => true
                ])
                ->getQuery()
                ->getOneOrNullResult();
    }

    /**
     * Check if entity exist by target and target id
     *
     * @param string $target
     * @param int $targetId
     *
     * @return bool
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function existByTarget(string $target, int $targetId)
    {
        return null !== $this->createQueryBuilder('s')
            ->andWhere('s.target = :target')
            ->andWhere('s.targetId = :targetId')
            ->setParameters([
                'target' => $target,
                'targetId' => $targetId
            ])
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $type
     * @param int $id
     *
     * @return string|null
     */
    public function getMainSlugByIdAndType(string $type, int $id = null)
    {
        $qb = $this->createQueryBuilder('s')
            ->andWhere('s.isMain = :isMain AND s.target = :target')
            ->setParameters([
                'isMain' => true,
                'target' => $type
            ]);

        if ($id) {
            $qb->andWhere('s.targetId = :targetId')->setParameter('targetId', $id);
        }

        return $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $slug
     * @param string $type
     * @param int|null $id
     *
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isMainSlug(string $slug, string $type, int $id = null)
    {
        $qb = $this->createQueryBuilder('s');

        $qb->andWhere('lower(s.slug) = lower(:slug) and s.isMain = :isMain and s.target = :type')
            ->setParameters([
                'slug' => $slug,
                'isMain' => true,
                'type' => $type
            ]);

        if ($id) {
            $qb->andWhere('s.targetId = :targetId')->setParameter('targetId', $id);
        }

        return $qb->setMaxResults(1)->getQuery()->getOneOrNullResult(Query::HYDRATE_ARRAY) !== null;
    }

    /**
     * @param \Content\ORM\Entity\Slug[]|Collection $slugs
     */
    public function saveCollection(Sluggable $sluggable, $slugs)
    {
        $slugs = is_array($slugs) ? $slugs : $slugs->toArray();
        $entityManager = $this->getEntityManager();
        $entityManager->beginTransaction();

        try {
            $oldSlugs = $this->findByTargetObject($sluggable);
            $removeSlugs = array_udiff($oldSlugs, $slugs, function (Slug $slug1, Slug $slug2) {
                return $slug1->getId() <=> $slug2->getId();
            });

            foreach ($removeSlugs as $removeSlug) {
                $entityManager->remove($removeSlug);
            }

            foreach ($slugs as $slug) {
                $slug->setTargetId($sluggable->getIdentifier());
                $entityManager->persist($slug);
            }
            $entityManager->flush();
            $entityManager->commit();
        } catch (\Exception $e) {
            $entityManager->rollback();
            return false;
        }

        return true;
    }
}