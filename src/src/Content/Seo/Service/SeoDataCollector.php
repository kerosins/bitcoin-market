<?php
/**
 * @author Kerosin
 */

namespace Content\Seo\Service;


use Content\Seo\SeoDataProviderInterface;

class SeoDataCollector
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $keywords;

    /**
     * @var string
     */
    private $description;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return SeoDataCollector
     */
    public function setTitle(string $title): SeoDataCollector
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     * @return SeoDataCollector
     */
    public function setKeywords(string $keywords): SeoDataCollector
    {
        $this->keywords = $keywords;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return SeoDataCollector
     */
    public function setDescription(string $description): SeoDataCollector
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param SeoDataProviderInterface $provider
     * @return $this
     */
    public function setProvider(SeoDataProviderInterface $provider)
    {
        $this->title = $provider->getMetaTitle();
        $this->description = $provider->getMetaDescription();
        $this->keywords = $provider->getMetaKeywords();

        return $this;
    }
}