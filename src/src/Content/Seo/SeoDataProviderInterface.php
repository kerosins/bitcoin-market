<?php
/**
 * @author Kerosin
 */

namespace Content\Seo;

interface SeoDataProviderInterface
{
    /**
     * @return string
     */
    public function getMetaTitle();

    /**
     * @return string
     */
    public function getMetaKeywords();

    /**
     * @return string
     */
    public function getMetaDescription();
}