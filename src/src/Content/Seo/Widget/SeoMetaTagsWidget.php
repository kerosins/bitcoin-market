<?php
/**
 * @author Kerosin
 */

namespace Content\Seo\Widget;

use Kerosin\Component\Widget;
use Content\Seo\Service\SeoDataCollector;

class SeoMetaTagsWidget extends Widget implements \Twig_Extension_GlobalsInterface
{

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('seoMetaTags', [$this, 'renderMetaTags'], ['is_safe' => ['html']]),
        ];
    }

    public function getGlobals()
    {
        return [
            'seoTitle' => $this->container->get(SeoDataCollector::class)->getTitle() ?? ''
        ];
    }

    public function renderMetaTags()
    {
        $collector = $this->container->get(SeoDataCollector::class);

        $html = '';

        if ($collector->getTitle()) {
            $html = "<meta name='title' content='{$collector->getTitle()}'>";
        }

        if ($collector->getKeywords()) {
            $html .= "<meta name='keywords' content='{$collector->getKeywords()}'>" . PHP_EOL;
        }

        if ($collector->getDescription()) {
            $html .= "<meta name='description' content='{$collector->getDescription()}'>" . PHP_EOL;
        }

        return $html;
    }
}