<?php
/**
 * Created by PhpStorm.
 * User: kerosin
 * Date: 07.04.2018
 * Time: 3:40
 */

namespace Content\Seo\Form;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class MetaTagsFieldsHelper
{
    /**
     * Add are seo metadata fields
     *
     * @param FormBuilderInterface $builder
     */
    public static function addFields(FormBuilderInterface $builder)
    {
        $builder
            ->add('metaTitle', TextType::class, [
                'required' => false
            ])
            ->add('metaKeywords', TextType::class, [
                'required' => false
            ])
            ->add('metaDescription', TextType::class, [
                'required' => false
            ]);
    }
}