<?php
/**
 * Breadcrumbs class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Content\Navigation\Service;

class Breadcrumbs
{
    private $breadcrumbs = [];

    public function add($title, $url)
    {
        $items = [];
        if (is_array($title)) {
            $items = $title;
        } else {
            $items[] = [
                'title' => $title,
                'url' => $url
            ];
        }

        foreach ($items as $item) {
            $this->breadcrumbs[] = $item;
        }
    }

    public function addArray(array $crumbs = [])
    {
        foreach ($crumbs as $crumb) {
            $this->add($crumb['title'], $crumb['url']);
        }
    }

    public function get()
    {
        return $this->breadcrumbs;
    }
}