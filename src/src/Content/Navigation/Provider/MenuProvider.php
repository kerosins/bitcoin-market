<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\Navigation\Provider;


use function Kerosin\Helper\StringHelper\endsWith;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Yaml\Yaml;

class MenuProvider
{
    private $requestStack;

    private $menuConfigurationPaths = [];

    private $menuItems = [];

    public function __construct(RequestStack $requestStack, array $menuConfigurationPaths = [])
    {
        $this->requestStack = $requestStack;
        $this->menuConfigurationPaths = $menuConfigurationPaths;
    }

    /**
     * Gets items of menu
     *
     * @param string $menuName
     */
    public function getItems(string $menuName)
    {
        if (isset($this->menuItems[$menuName])) {
            return $this->menuItems[$menuName];
        }

        $path = $this->menuConfigurationPaths[$menuName] ?? null;

        if (!$path) {
            throw new \RuntimeException("Undefined menu: {$menuName}");
        }

        $path = $this->menuConfigurationPaths[$menuName];
        if (!file_exists($path)) {
            throw new \RuntimeException("Configuration file for menu '{$menuName}' not found in path {$path}");
        }

        $menuConfig = file_get_contents($path);
        $menuConfig = Yaml::parse($menuConfig);

        $currentRequest = $this->requestStack->getCurrentRequest();

        $currentRoute = $currentRequest->get('_route');

        $checkIfActive = function ($menuRoute, $currentRoute) {
            if (endsWith($menuRoute, 'index')) { //is index route for section|CRUD
                $currentRoute = (substr($currentRoute, 0, strrpos($currentRoute, '.'))) . '.index';
            }

            return ($currentRoute === $menuRoute);
        };

        $findActiveItem = function ($menu) use ($currentRoute, &$findActiveItem, $checkIfActive) {
            $result = [];
            $active = false;

            foreach ($menu as $key => $item) {
                $isActive = false;
                if (!empty($item['route'])) {
                    $isActive = $checkIfActive($item['route'], $currentRoute);
                    if ($isActive) {
                        $active = $isActive;
                    }
                }

                if (isset($item['children'])) {
                    list ($children, $isChildActive) = $findActiveItem($item['children']);
                    if (!$isActive) {
                        $isActive = $isChildActive;
                    }
                    $item['children'] = $children;
                }

                $item['active'] = $isActive;

                $result[$key] = $item;
            }

            return [$result, $active];
        };

        $items = $findActiveItem($menuConfig['items'])[0];

        $this->menuItems[$menuName] = $items;
        return $this->menuItems[$menuName];
    }
}