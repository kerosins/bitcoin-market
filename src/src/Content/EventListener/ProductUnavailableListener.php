<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\EventListener;


use Catalog\Import\Component\ProductUpdateEvent;
use Content\ItemBlock\BlockType\ManualProducts\ManualProductsBlock;
use Content\ItemBlock\BlockType\ManualProducts\Parameters;
use Content\ORM\Entity\ItemBlock;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProductUnavailableListener implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            ProductUpdateEvent::EVENT_NAME_AVAILABLE => 'onUnavailableProduct'
        ];
    }

    public function onUnavailableProduct(ProductUpdateEvent $event)
    {
        $product = $event->getProduct();
        if ($product->isActive()) {
            return;
        }

        $sql = <<<SQL
    SELECT * FROM item_block ib WHERE ib.type = :type AND ib.params->'products' ! :product
SQL;

        $rsm = new ResultSetMappingBuilder($this->entityManager);
        $rsm->addRootEntityFromClassMetadata(ItemBlock::class, 'ib');
        /** @var ItemBlock[] $blocks */
        $blocks = $this->entityManager
            ->createNativeQuery($sql, $rsm)
            ->setParameters(['type' => ManualProductsBlock::NAME, 'product' => $product])
            ->getResult()
        ;

        $this->entityManager->beginTransaction();

        try {
            foreach ($blocks as $block) {
                $params = Parameters::fromArray($block->getParams());
                $products = array_filter($params->getProducts(), function ($id) use ($product) {
                    return $id != $product->getId();
                });
                $params->setProducts($products);
                $block->setParams($params->toArray());

                $this->entityManager->persist($block);
            }

            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
        }
    }

}