<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock;

abstract class ItemBlockParameters
{
    private $title;

    public static function fromArray(array $parameters = [])
    {
        $self = new static();

        foreach ($parameters as $name => $value) {
            $methodName = 'set' . $name;
            if (method_exists($self, $methodName)) {
                $self->$methodName($value);
            } else {
                throw new \UnexpectedValueException('Unknown parameter "' . $name . '"');
            }
        }

        return $self;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return ItemBlockParameters
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * builds a cache key from params
     *
     * @return string
     */
    public function getCacheKey()
    {
        $params = $this->toArray();
        $params = json_encode($params, true);
        $md5 = md5($params);

        return "item_block_{$md5}";
    }

    abstract public function toArray() : array;
}