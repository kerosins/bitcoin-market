<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock;

use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Content\ORM\Entity\ItemBlock;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ItemBlockForm extends AbstractType implements EntityManagerContract
{
    use EntityManagerContractTrait;

    /**
     * @var ItemBlockManager
     */
    private $itemBlockManager;

    public function __construct(ItemBlockManager $itemBlockManager)
    {
        $this->itemBlockManager = $itemBlockManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $areas = ItemBlock::$areaLabels;
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('type', ChoiceType::class, [
                'choices' => $this->itemBlockManager->getTypeOptions(),
                'placeholder' => 'Select Type of Block'
            ])
            ->add('area', ChoiceType::class, [
                'choices' => array_flip($areas)
            ])
            ->add('weight')
            ->add('enable');

        $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'addParamsField']);
        $builder->get('type')->addEventListener(FormEvents::POST_SUBMIT, [$this, 'addParamsField']);
        $builder->addModelTransformer(new CallbackTransformer(
            [$this, 'toForm'],
            [$this, 'toEntity']
        ));
//        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'preSubmit']);

//        $builder->addViewTransformer(new CallbackTransformer(
//            function ($value) {
//                return $value;
//            },
//            function ($value) {
//                $t = 1;
//                return $value;
//            }
//        ));
    }

    public function toForm(ItemBlock $itemBlock)
    {
        $type = $itemBlock->getType();
        $blockType = $this->itemBlockManager->getBlockType($type);
        if (!$blockType) {
            return;
        }

        $itemBlock->setParams($blockType->populateParams($itemBlock->getParams()));
        return $itemBlock;
    }

    public function toEntity(ItemBlock $formData)
    {
        $params = $formData->getParams();
        if ($params instanceof ItemBlockParameters) {
            $formData->setParams($params->toArray());
        }
        return $formData;
    }

    /**
     * 'form.pre_set_data' handler
     *
     * @param FormEvent $event
     */
    public function addParamsField(FormEvent $event)
    {
        /** @var ItemBlock $data */
        $data = $event->getData();
        $form = $event->getForm();

        if ($data instanceof ItemBlock) {
            $type = $data->getType();
        }  else {
            $type = $data;
            $form = $form->getParent();
        }
        $blockType = $this->itemBlockManager->getBlockType($type);

        if (!$blockType) {
            return;
        }

        $childForm = $blockType->getForm();
        $form->add('params', $childForm);
    }

    /*public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();

        $type = $data['type'];
        $blockType = $this->itemBlockManager->getBlockType($type);
        if (!$blockType) {
            return;
        }

        $form = $event->getForm();
        $form->add('params', $blockType->getForm());
    }*/

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', ItemBlock::class);
//        $resolver->setDefault('allow_extra_fields', true);
    }
}