<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock\BlockType\BannerList;

use Content\ItemBlock\ItemBlockParameters;

class Parameters extends ItemBlockParameters
{
    private $banners = [];

    public function toArray(): array
    {
        $banners = array_map(function ($item) {
            return [
                'image' => $item['image']->getSrc(),
                'title' => $item['title'],
                'url' => $item['url'],
                'weight' => $item['weight']
            ];
        }, $this->banners);

        usort($banners, function ($a, $b) {
            return $a['weight'] <=> $b['weight'];
        });

        return ['banners' => $banners];
    }

    /**
     * @return array
     */
    public function getBanners(): ?array
    {
        return $this->banners;
    }

    /**
     * @param array $banners
     * @return Parameters
     */
    public function setBanners(array $banners): Parameters
    {
        $this->banners = $banners;
        return $this;
    }
}