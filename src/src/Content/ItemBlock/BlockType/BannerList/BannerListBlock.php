<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock\BlockType\BannerList;

use Content\ItemBlock\BlockType\BannerList\Form\Form;
use Content\ItemBlock\ItemBlockParameters;
use Content\ItemBlock\ItemBlockInterface;

class BannerListBlock implements ItemBlockInterface
{
    public const NAME = 'banner_list';

    public function getTitle(): string
    {
        return 'List of Banners';
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getForm(): string
    {
        return Form::class;
    }

    /**
     * @param array $params
     *
     * @return ItemBlockParameters|Parameters
     */
    public function populateParams(array $params): ItemBlockParameters
    {
        return Parameters::fromArray($params);
    }

    /**
     * @param array|ItemBlockParameters $params
     * @return array
     */
    public function findItems($params): array
    {
        return $this->populateParams($params)->getBanners();
    }

    public function isPersistedBlock(): bool
    {
        return true;
    }
}