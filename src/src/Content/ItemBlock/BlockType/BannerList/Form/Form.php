<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock\BlockType\BannerList\Form;


use Content\ItemBlock\BlockType\BannerList\Parameters;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Form extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('banners', CollectionType::class, [
            'entry_type' => BannerItemType::class,
            'allow_add' => true,
            'allow_delete' => true
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Parameters::class);
    }
}