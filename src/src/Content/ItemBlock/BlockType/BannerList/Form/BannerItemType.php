<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock\BlockType\BannerList\Form;


use Kerosin\Component\Image;
use Kerosin\Filesystem\ImageHelper;
use Kerosin\Form\Type\ImageUrlType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;

class BannerItemType extends AbstractType
{
    /**
     * @var ImageHelper
     */
    private $imageHelper;

    public function __construct(ImageHelper $imageHelper)
    {
        $this->imageHelper = $imageHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', ImageUrlType::class, [
                'extended' => false
            ])
            ->add('title', TextType::class, [
                'required' => false,
            ])
            ->add('url', TextType::class, [
                'required' => false
            ])
            ->add('weight', IntegerType::class);

        $builder->get('image')->addModelTransformer(new CallbackTransformer(
            function ($value) {
                return $value ? new Image($value) : '';
            },
            function ($value) {
                if ($value instanceof Image && $value->getFile()) {
                    $this->imageHelper->saveImage($value, ImageHelper::BANNER_TYPE);
                }
                return $value;
            }
        ));
    }

    public function getBlockPrefix()
    {
        return 'banners_type_item';
    }
}