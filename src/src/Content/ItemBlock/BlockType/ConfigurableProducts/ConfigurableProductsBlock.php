<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock\BlockType\ConfigurableProducts;


use Catalog\ORM\Entity\Product;
use Elastica\Query;
use FOS\ElasticaBundle\Manager\RepositoryManager;
use Kerosin\DependencyInjection\Contract\TaggedCacheContract;
use Kerosin\DependencyInjection\Contract\TaggedCacheContractTrait;
use Content\ItemBlock\ItemBlockInterface;
use Content\ItemBlock\ItemBlockManager;
use Content\ItemBlock\ItemBlockParameters;

class ConfigurableProductsBlock implements ItemBlockInterface, TaggedCacheContract
{
    use TaggedCacheContractTrait;

    public const NAME = 'configurableProducts';
    /**
     * @var int
     */
    private $fiatCurrencyScale;

    /**
     * @var int
     */
    private $expireBlock;

    /**
     * @var int
     */
    private $productsInBlock;

    /**
     * @var int
     */
    private $productsInQuery;

    /**
     * @var RepositoryManager
     */
    private $repositoryManager;

    public function __construct(
        RepositoryManager $repositoryManager,
        int $fiatCurrencyScale,
        int $expireBlock,
        int $productsInBlock,
        int $productsInQuery
    ) {
        $this->repositoryManager = $repositoryManager;
        $this->fiatCurrencyScale = $fiatCurrencyScale;
        $this->expireBlock = $expireBlock;
        $this->productsInBlock = $productsInBlock;
        $this->productsInQuery = $productsInQuery;
    }

    public function isPersistedBlock(): bool
    {
        return false;
    }

    public function getTitle(): string
    {
        return "Configurable Products";
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getForm(): string
    {
        return '';
    }

    /**
     * @inheritdoc
     *
     * @param array $params
     * @return Parameters
     */
    public function populateParams(array $params): ItemBlockParameters
    {
        return Parameters::fromArray($params);
    }

    /**
     * @inheritdoc
     *
     * @param array|Parameters $params
     * @return array
     */
    public function findItems($params): array
    {
        $hash = md5(serialize($params));
        $key = 'configurable_products_' . $hash;

        $cacheItem = $this->taggedCache->getItem($key);
        if (!$cacheItem->isHit()) {
            $data = $this->findProducts($params);
            $cacheItem->set($data);
            $cacheItem->expiresAfter($this->expireBlock);
            $cacheItem->tag([ItemBlockManager::CACHE_TAG]);

            $this->taggedCache->save($cacheItem);
        }

        $data = $cacheItem->get();
        shuffle($data);

        return array_slice($data, 0,$this->productsInBlock);
    }

    private function findProducts($params)
    {
        $parameters = Parameters::fromArray($params);

        $query = new Query();
        $boolQuery = new Query\BoolQuery();
        $query->setQuery($boolQuery);

        $boolQuery->addMust((new Query\Term())->setTerm('status', Product::STATUS_ACTIVE));

        if ($parameters->getSupplier() !== null) {
            $boolQuery->addMust((new Query\Term())->setTerm('supplier.id', $parameters->getSupplier()));
        }

        if ($parameters->getCategory()) {
            $boolQuery->addMust((new Query\Term())->setTerm('categories.id', $parameters->getCategory()));
        }

        if (($price = $parameters->getPrice()) !== null) {
            $priceDiapason = $parameters->getPriceDiapason();
            $diapasonValue = bcdiv($price, $priceDiapason, $this->fiatCurrencyScale);

            $boolQuery->addMust(
                new Query\Range('publicPrice', [
                    'gte' => bcsub($price, $diapasonValue, $this->fiatCurrencyScale),
                    'lte' => bcadd($price, $diapasonValue, $this->fiatCurrencyScale)
                ])
            );
        }

        if ($parameters->getExceptProducts()) {
            $boolQuery->addMustNot(new Query\Terms('_id', $parameters->getExceptProducts()));
        }

        $repository = $this->repositoryManager->getRepository('catalog/product');
        return $repository->find($query, $this->productsInQuery);
    }
}