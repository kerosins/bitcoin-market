<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock\BlockType\ConfigurableProducts;


use Content\ItemBlock\ItemBlockParameters;
use Catalog\ORM\Entity\Category;
use Shop\Entity\Seller;

class Parameters extends ItemBlockParameters
{
    /**
     * Supplier
     *
     * @var int
     */
    private $supplier;

    /**
     * Category
     *
     * @var int
     */
    private $category;

    /**
     * Diapasone of price in percents
     *
     * @var int
     */
    private $priceDiapason = 10;

    /**
     * @var float
     */
    private $price;

    /**
     * @var int[]
     */
    private $exceptProducts;

    public function toArray(): array
    {
        return [
            'supplier' => $this->supplier,
            'category' => $this->category,
            'priceDiapason' => $this->priceDiapason,
            'price' => $this->price
        ];
    }

    /**
     * @return int
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param int|Seller $supplierId
     *
     * @return Parameters
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier instanceof Seller ? $supplier->getId() : $supplier;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param int|\Catalog\ORM\Entity\Category $category
     *
     * @return Parameters
     */
    public function setCategory($category)
    {
        $this->category = $category instanceof Category ? $category->getId() : $category;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriceDiapason(): ?int
    {
        return $this->priceDiapason;
    }

    /**
     * @param int $priceDiapason
     * @return Parameters
     */
    public function setPriceDiapason(int $priceDiapason)
    {
        $this->priceDiapason = $priceDiapason;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Parameters
     */
    public function setPrice(float $price): Parameters
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int[]
     */
    public function getExceptProducts(): ?array
    {
        return $this->exceptProducts;
    }

    /**
     * @param int[] $exceptProducts
     * @return Parameters
     */
    public function setExceptProducts(array $exceptProducts): Parameters
    {
        $this->exceptProducts = $exceptProducts;
        return $this;
    }
}