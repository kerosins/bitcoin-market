<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock\BlockType\AutomaticProducts;


use Elastica\Query;
use FOS\ElasticaBundle\Finder\FinderInterface;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Content\ItemBlock\BlockType\AutomaticProducts\Form\Form;
use Content\ItemBlock\ItemBlockParameters;
use Content\ItemBlock\ItemBlockInterface;
use Catalog\ORM\Entity\Product;

class AutomaticProductsBlock implements ItemBlockInterface
{
    public const NAME = 'automatic_products';

    /**
     * @var FinderInterface
     */
    private $finder;

    public function __construct(FinderInterface $finder)
    {
        $this->finder = $finder;
    }

    public function getTitle(): string
    {
        return 'Automatic generated list of products';
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getForm(): string
    {
        return Form::class;
    }

    /**
     * @param array $params
     *
     * @return ItemBlockParameters|Parameters
     */
    public function populateParams(array $params): ItemBlockParameters
    {
        return Parameters::fromArray($params);
    }

    public function findItems($params): array
    {
        $parameters = $this->populateParams($params);

        $boolQuery = new Query\BoolQuery();
        $query = new Query();
        $query->setQuery($boolQuery);

        $boolQuery->addMust(new Query\Terms('categories.id', $parameters->getCategories()));
        $boolQuery->addMust((new Query\Term())->setTerm('status', Product::STATUS_ACTIVE));
        $boolQuery->addMust(
            (new Query\Range('amountOrders', ['gte' => $parameters->getLimit()]))
        );

        $query->setSize($parameters->getSize());

        return $this->finder->find($query);
    }

    public function isPersistedBlock(): bool
    {
        return true;
    }
}