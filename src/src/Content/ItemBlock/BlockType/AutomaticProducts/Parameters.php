<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock\BlockType\AutomaticProducts;


use Content\ItemBlock\ItemBlockParameters;
use Catalog\ORM\Entity\Category;

class Parameters extends ItemBlockParameters
{
    /**
     * @var array
     */
    private $categories = [];

    /**
     * @var string
     */
    private $orderBy;

    /**
     * Min value of field
     *
     * @var int
     */
    private $limit;

    /**
     * Count of products
     *
     * @var int
     */
    private $size;

    public function toArray(): array
    {
        return [
            'categories' => $this->categories,
            'orderBy' => $this->orderBy,
            'limit' => $this->limit ?? 0,
            'size' => $this->size ?? 0
        ];
    }

    public static function getOrderByOptions()
    {
        return [
            'Amount of Orders' => 'amountOrders'
        ];
    }

    /**
     * @return array
     */
    public function getCategories(): ?array
    {
        return $this->categories;
    }

    /**
     * @param array $categories
     * @return Parameters
     */
    public function setCategories(array $categories): Parameters
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderBy(): ?string
    {
        return $this->orderBy;
    }

    /**
     * @param string $orderBy
     * @return Parameters
     */
    public function setOrderBy(string $orderBy): Parameters
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return Parameters
     */
    public function setLimit(int $limit = 0): Parameters
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return int
     */
    public function getSize(): ?int
    {
        return $this->size;
    }

    /**
     * @param int $size
     *
     * @return Parameters
     */
    public function setSize(int $size): Parameters
    {
        $this->size = $size;
        return $this;
    }
}