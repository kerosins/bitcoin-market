<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock\BlockType\AutomaticProducts\Form;

use Kerosin\Form\Type\CategoryTreeType;
use Content\ItemBlock\BlockType\AutomaticProducts\Parameters;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Form extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categories',CategoryTreeType::class)
            ->add('orderBy', ChoiceType::class, [
                'choices' => Parameters::getOrderByOptions()
            ])
            ->add('limit', NumberType::class, [
                'label' => 'Min value of field'
            ])
            ->add('size', NumberType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Parameters::class);
    }
}