<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock\BlockType\ManualProducts\Form;

use Content\ItemBlock\BlockType\ManualProducts\Parameters;
use Content\ItemBlock\BlockType\ManualProducts\Form\ProductSearchField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

class Form extends AbstractType
{
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('products', ProductSearchField::class, [
            'label' => 'List of Products',
            'search_uri' => $this->router->generate('cp.catalog.product.autocomplete')
        ]);

//        $builder->addViewTransformer(new CallbackTransformer(
//            function ($value) {
//                return $value;
//            },
//            function ($value) {
//                return $value;
//            }
//        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Parameters::class);
    }
}