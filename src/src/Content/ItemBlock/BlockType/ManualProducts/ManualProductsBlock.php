<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock\BlockType\ManualProducts;


use Catalog\ORM\Entity\Product;
use Content\ItemBlock\BlockType\ManualProducts\Form\Form;
use Content\ItemBlock\ItemBlockInterface;
use Content\ItemBlock\ItemBlockParameters;
use Content\ORM\Entity\ItemBlock;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface;

class ManualProductsBlock implements
    ItemBlockInterface,
    CacheWarmerInterface
{
    public const NAME = 'manual_products';

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TagAwareAdapterInterface
     */
    private $tagAwareAdapter;

    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        TagAwareAdapterInterface $tagAwareAdapter
    ) {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->tagAwareAdapter = $tagAwareAdapter;
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Manual List of Products';
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getForm(): string
    {
        return Form::class;
    }

    /**
     * @param array $params
     * @return ItemBlockParameters|Parameters
     */
    public function populateParams(array $params) : ItemBlockParameters
    {
        return Parameters::fromArray($params);
    }

    /**
     * @inheritdoc
     *
     * @param array|ItemBlockParameters $params
     * @return array
     */
    public function findItems($params): array
    {
        $params = $this->populateParams($params);

        $cacheItem = $this->tagAwareAdapter->getItem($params->getCacheKey());
        if (!$cacheItem->isHit()) {
            $products = $this->getItems($params);
            $cacheItem->set($products);
            $this->tagAwareAdapter->save($cacheItem);
        }

        return $cacheItem->get();

    }

    /**
     * Gets items from DB
     *
     * @param Parameters $parameters
     *
     * @return array|Product[]|ItemBlock[]
     */
    private function getItems(Parameters $parameters)
    {
        $data = $parameters->getProducts();
        $products = $this->entityManager->getRepository(Product::class)->findBy([
            'id' => array_column($data, 'id'),
            'available' => true
        ]);


        $sortArray = array_combine(array_column($data, 'id'), array_column($data, 'position'));
        usort($products, function (Product $a, Product $b) use ($sortArray) {
            return $sortArray[$a->getId()] <=> $sortArray[$b->getId()];
        });

        return $products;
    }

    /**
     * @inheritdoc
     *
     * @return bool
     */
    public function isPersistedBlock(): bool
    {
        return true;
    }

    /**
     * @return false
     */
    public function isOptional()
    {
        return false;
    }

    public function warmUp($cacheDir)
    {
        $blocks = $this->entityManager
            ->getRepository(ItemBlock::class)
            ->findBy([
                'type' => self::NAME,
                'enable' => true
            ]);

        foreach ($blocks as $block) {
            $parameters = $this->populateParams($block->getParams());
            $products = $this->getItems($parameters);

            $cacheItem = $this->tagAwareAdapter->getItem($parameters->getCacheKey());
            $cacheItem->set($products);
            $this->tagAwareAdapter->save($cacheItem);
        }
    }
}