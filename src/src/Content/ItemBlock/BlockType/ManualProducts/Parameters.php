<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock\BlockType\ManualProducts;

use Content\ItemBlock\ItemBlockParameters;

class Parameters extends ItemBlockParameters
{
    /**
     * array of ids
     *
     * @var array
     */
    private $products = [];

    public function toArray(): array
    {
        $counter = 0;
        return [
            'products' => array_map(function ($id) use (&$counter) {
                return [
                    'id' => $id,
                    'position' => $counter++
                ];
            }, $this->products)
        ];
    }

    /**
     * @return array
     */
    public function getProducts(): ?array
    {
        return $this->products;
    }

    /**
     * @param array $products
     * @return Parameters
     */
    public function setProducts(array $products): Parameters
    {
        $this->products = $products;
        return $this;
    }
}