<?php
/**
 * @author Kondaurov
 */

namespace Content\ItemBlock\BlockType\Category;

use Content\ItemBlock\ItemBlockParameters;

class Parameters extends ItemBlockParameters
{
    private $categories;

    public function toArray(): array
    {
        $categories = array_map(function ($item) {
            return [
                'category' => $item['category'],
                'image' => $item['image']->getSrc(),
                'weight' => $item['weight']
            ];
        }, $this->categories);

        usort($categories, function ($a, $b) {
            return $a['weight'] <=> $b['weight'];
        });

        return ['categories' => $categories];
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     *
     * @return Parameters
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
        return $this;
    }
}