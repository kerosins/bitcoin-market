<?php
/**
 * @author Kondaurov
 */

namespace Content\ItemBlock\BlockType\Category;


use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Content\ItemBlock\BlockType\Category\Form\Form;
use Content\ItemBlock\ItemBlockParameters;
use Content\ItemBlock\ItemBlockInterface;
use Catalog\ORM\Entity\Category;

class CategoryBlock implements ItemBlockInterface, EntityManagerContract
{
    use EntityManagerContractTrait;

    public const NAME = 'categories';

    public function getTitle(): string
    {
        return 'Categories';
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getForm(): string
    {
        return Form::class;
    }

    /**
     * @param array $params
     *
     * @return ItemBlockParameters|Parameters
     */
    public function populateParams(array $params): ItemBlockParameters
    {
        return Parameters::fromArray($params);
    }

    public function findItems($params): array
    {
        $parameters = !$params instanceof ItemBlockParameters ? $this->populateParams($params) : $params;
        $indexed = [];

        foreach ($parameters->getCategories() as $item) {
            $indexed[$item['category']] = [
                'id' => $item['category'],
                'image' => $item['image']
            ];
        }

        $ids = array_column($indexed, 'id');
        $categories = $this->entityManager->getRepository(Category::class)->findBy([
            'id' => $ids,
            'enabled' => true
        ]);

        $result = array_map(function (Category $category) use ($indexed) {
            return [
                'category' => $category,
                'image' => $indexed[$category->getId()]['image']
            ];
        }, $categories);

        return $result;
    }


    public function isPersistedBlock(): bool
    {
        return true;
    }

}