<?php
/**
 * @author Kondaurov
 */

namespace Content\ItemBlock\BlockType\Category\Form;

use Kerosin\Component\Image;
use Kerosin\Filesystem\ImageHelper;
use Kerosin\Form\Type\CategoryListType;
use Kerosin\Form\Type\ImageType;
use Kerosin\Form\Type\ImageUrlType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CategoryItemType extends AbstractType
{
    /**
     * @var ImageHelper
     */
    private $imageHelper;

    public function __construct(ImageHelper $imageHelper)
    {
        $this->imageHelper = $imageHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', CategoryListType::class)
            ->add('image', ImageUrlType::class, [
                'extended' => false
            ])
            ->add('weight', NumberType::class)
        ;


        $builder->get('image')->addModelTransformer(new CallbackTransformer(
            function ($value) {
                return $value ? new Image($value) : '';
            },
            function ($value) {
                if ($value instanceof Image && $value->getFile()) {
                    $this->imageHelper->saveImage($value, ImageHelper::BANNER_TYPE);
                }
                return $value;
            }
        ));
    }

    public function getBlockPrefix()
    {
        return 'category_item_type';
    }
}