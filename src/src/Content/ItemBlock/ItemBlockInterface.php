<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock;

interface ItemBlockInterface
{
    /**
     * Marks block for persist in database
     *
     * @return bool
     */
    public function isPersistedBlock() : bool;

    /**
     * Gets a readable name
     *
     * @return string
     */
    public function getTitle() : string;

    /**
     * Gets name
     *
     * @return string
     */
    public function getName() : string;

    /**
     * Gets a form type
     *
     * @param array $data
     * @return string
     */
    public function getForm() : string;

    /**
     * Populates array of params to DTO
     *
     * @param array $params
     * @return ItemBlockParameters
     */
    public function populateParams(array $params) : ItemBlockParameters;


    /**
     * Gets items
     *
     * @param array|ItemBlockParameters $params
     * @return array
     */
    public function findItems($params) : array;
}