<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Content\ItemBlock;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Kerosin\DependencyInjection\Contract\TaggedCacheContract;
use Kerosin\DependencyInjection\Contract\TaggedCacheContractTrait;
use Content\ORM\Entity\ItemBlock;

/**
 * todo move it
 *
 * Class ItemBlockManager
 * @package Kerosin\ItemBlock
 */
class ItemBlockManager implements TaggedCacheContract
{
    use TaggedCacheContractTrait;

    public const CACHE_TAG = 'item_blocks';

    /**
     * @var Collection
     */
    private $types = [];

    public function __construct()
    {
        $this->types = new ArrayCollection();
    }

    /**
     * Gets array of items
     *
     * @param \Content\ORM\Entity\ItemBlock $itemBlock
     */
    public function findItemsViaBlock(ItemBlock $itemBlock)
    {
        $itemBlockType = $this->getBlockType($itemBlock->getType());
        if (!$itemBlockType) {
            throw new \RuntimeException('Unknown type of block');
        }

        return $itemBlockType->findItems($itemBlock->getParams());
    }

    /**
     * @param $type
     * @return ItemBlockInterface|null
     */
    public function getBlockType($type)
    {
        $itemBlockType = $this->types->filter(function (ItemBlockInterface $itemBlockType) use ($type) {
            return $itemBlockType->getName() === $type;
        });

        return $itemBlockType->count() ? $itemBlockType->first() : null;
    }

    /**
     * @param ItemBlockInterface $blockType
     *
     * @return $this
     */
    public function addBlockType(ItemBlockInterface $blockType)
    {
        $this->types->add($blockType);
        return $this;
    }

    /**
     * @return array
     */
    public function getTypeOptions()
    {
        $options = [];

        $types = $this->types->filter(function (ItemBlockInterface $type) {
             return $type->isPersistedBlock();
        });

        foreach ($types as $type) {
            /** @var ItemBlockInterface $type */
            $options[$type->getTitle()] = $type->getName();
        }

        return $options;
    }

    /**
     * Gets list of items from cache
     *
     * @param \Content\ORM\Entity\ItemBlock $itemBlock
     *
     * @return \Symfony\Component\Cache\CacheItem
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function getItemsFromCache(ItemBlock $itemBlock)
    {
        $key = sprintf(
            'item_block_%s_%d',
            $itemBlock->getType(),
            $itemBlock->getId()
        );
        return $this->taggedCache->getItem($key);
    }
}