<?php
/**
 * @author Kondaurov
 */

namespace Elastic\Entity;


class Seller
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Seller
     */
    public function setId(int $id): Seller
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Seller
     */
    public function setTitle(string $title): Seller
    {
        $this->title = $title;
        return $this;
    }
}