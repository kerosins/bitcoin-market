<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Elastic\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Elastic\Entity\Category\ChildCategory;

class ElasticProduct
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var ArrayCollection|ChildCategory[]
     */
    private $categories;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $mainImage;

    /**
     * @var float
     */
    private $publicPrice;

    /**
     * @var bool
     */
    private $available;

    /**
     * @var int
     */
    private $amountOrders;

    /**
     * @var Seller
     */
    private $seller;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ElasticProduct
     */
    public function setId(int $id): ElasticProduct
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return ElasticProduct
     */
    public function setTitle(string $title): ElasticProduct
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return ArrayCollection|ChildCategory[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param ArrayCollection|ChildCategory[] $categories
     * @return ElasticProduct
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return ElasticProduct
     */
    public function setDescription(string $description): ElasticProduct
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainImage(): ?string
    {
        return $this->mainImage;
    }

    /**
     * @param string $mainImage
     * @return ElasticProduct
     */
    public function setMainImage(string $mainImage): ElasticProduct
    {
        $this->mainImage = $mainImage;
        return $this;
    }

    /**
     * @return float
     */
    public function getPublicPrice(): ?float
    {
        return $this->publicPrice;
    }

    /**
     * @param float $publicPrice
     * @return ElasticProduct
     */
    public function setPublicPrice(float $publicPrice): ElasticProduct
    {
        $this->publicPrice = $publicPrice;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailable(): ?bool
    {
        return $this->available;
    }

    /**
     * @param bool $available
     * @return ElasticProduct
     */
    public function setAvailable(bool $available): ElasticProduct
    {
        $this->available = $available;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmountOrders(): ?int
    {
        return $this->amountOrders;
    }

    /**
     * @param int $amountOrders
     * @return ElasticProduct
     */
    public function setAmountOrders(int $amountOrders): ElasticProduct
    {
        $this->amountOrders = $amountOrders;
        return $this;
    }

    /**
     * @return \Elastic\Entity\Seller
     */
    public function getSeller(): ?\Elastic\Entity\Seller
    {
        return $this->seller;
    }

    /**
     * @param \Elastic\Entity\Seller $seller
     * @return ElasticProduct
     */
    public function setSeller(\Elastic\Entity\Seller $seller): ElasticProduct
    {
        $this->seller = $seller;
        return $this;
    }
}