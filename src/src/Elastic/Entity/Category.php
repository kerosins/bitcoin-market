<?php
/**
 * @author Kondaurov
 */

namespace Elastic\Entity;

use Doctrine\Common\Collections\Collection;

class Category
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var Category|null
     */
    private $firstParent;

    /**
     * @var Collection|Category[]
     */
    private $parents;

    /**
     * @var Collection|Category[]
     */
    private $children;

    /**
     * @var Collection|Seller[]
     */
    private $sellers;

    /**
     * @var int
     */
    private $depth;

    /**
     * @var bool
     */
    private $enabled;

    /**
     * @var string[]
     */
    private $fullPath;

    /**
     * @var int
     */
    private $weight;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Category
     */
    public function setId(int $id): Category
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Category
     */
    public function setTitle(string $title): Category
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getParents(): ?Collection
    {
        return $this->parents;
    }

    /**
     * @param Collection $parents
     *
     * @return Category
     */
    public function setParents(Collection $parents): Category
    {
        $this->parents = $parents;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getChildren(): ?Collection
    {
        return $this->children;
    }

    /**
     * @param Collection $children
     *
     * @return Category
     */
    public function setChildren(Collection $children): Category
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getSellers(): ?Collection
    {
        return $this->sellers;
    }

    /**
     * @param Collection $sellers
     *
     * @return Category
     */
    public function setSellers(Collection $sellers): Category
    {
        $this->sellers = $sellers;
        return $this;
    }

    /**
     * @return int
     */
    public function getDepth(): ?int
    {
        return $this->depth;
    }

    /**
     * @param int $depth
     *
     * @return Category
     */
    public function setDepth(int $depth): Category
    {
        $this->depth = $depth;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     *
     * @return Category
     */
    public function setEnabled(bool $enabled): Category
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getFullPath(): ?array
    {
        return $this->fullPath;
    }

    /**
     * @param string[] $fullPath
     *
     * @return Category
     */
    public function setFullPath(array $fullPath): Category
    {
        $this->fullPath = $fullPath;
        return $this;
    }

    /**
     * @return Category|null
     */
    public function getFirstParent(): ?Category
    {
        return $this->firstParent;
    }

    /**
     * @param Category|null $firstParent
     *
     * @return Category
     */
    public function setFirstParent(?Category $firstParent): Category
    {
        $this->firstParent = $firstParent;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     *
     * @return Category
     */
    public function setWeight(?int $weight): Category
    {
        $this->weight = $weight;
        return $this;
    }
}