<?php
/**
 * @author Kondaurov
 */

namespace Elastic\EventListener;

use Elastic\Pager\RedisPagerProvider;
use Elastica\Client;
use Elasticsearch\Endpoints\Indices\Settings\Put;
use FOS\ElasticaBundle\Event\TypePopulateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\KernelEvent;

class PopulateTypeSubscriber implements EventSubscriberInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var RedisPagerProvider
     */
    private $pagerProvider;

    public function __construct(Client $client, RedisPagerProvider $pagerProvider)
    {
        $this->client = $client;
        $this->pagerProvider = $pagerProvider;
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            TypePopulateEvent::PRE_TYPE_POPULATE => 'preTypePopulate',
            TypePopulateEvent::POST_TYPE_POPULATE => 'postTypePopulate',
            TypePopulateEvent::POST_TYPE_POPULATE => 'clearRedisPager',
        ];
    }

    /**
     * @param TypePopulateEvent $event
     */
    public function preTypePopulate(TypePopulateEvent $event)
    {
        $index = $event->getIndex();
        $type = $event->getType();

        $putSetting = new Put();
        $putSetting
            ->setType($type)
            ->setIndex($index)
            ->setBody([
                'index' => [
                    'refresh_interval' => '-1',
                ]
            ]);

        $this->client->requestEndpoint($putSetting);
    }

    /**
     * @param TypePopulateEvent $event
     */
    public function postTypePopulate(TypePopulateEvent $event)
    {
        $index = $event->getIndex();
        $type = $event->getType();

        $putSetting = new Put();
        $putSetting
            ->setType($type)
            ->setIndex($index)
            ->setBody([
                'index' => [
                    'refresh_interval' => '1s',
                ]
            ]);

        $this->client->requestEndpoint($putSetting);
    }

    /**
     * @param TypePopulateEvent $event
     */
    public function clearRedisPager(TypePopulateEvent $event)
    {
        $this->pagerProvider->clearByOptions($event->getIndex(), $event->getType(), $event->getOptions());
    }
}