<?php
/**
 * @author Kerosin
 */

namespace Elastic\EventListener;


use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use FOS\ElasticaBundle\Persister\ObjectPersisterInterface;
use FOS\ElasticaBundle\Provider\IndexableInterface;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Entity\ProductParameter;
use Catalog\ORM\Entity\ProductPrice;

class ProductIndexerSubscriber implements EventSubscriber
{
    /**
     * @var ProductListener
     */
    private $listener;

    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
            'postFlush'
        ];
    }

    /**
     * @var ObjectPersisterInterface
     */
    private $persister;

    /**
     * @var IndexableInterface
     */
    private $indexable;

    /**
     * @var array
     */
    private $config;

    /**
     * @var \Catalog\ORM\Entity\Product|null
     */
    private $indexableEntity;

    public function __construct(
        ObjectPersisterInterface $persister,
        IndexableInterface $indexable,
        ProductListener $productListener,
        $config
    ) {
        $this->listener = $productListener;
        $this->persister = $persister;
        $this->indexable = $indexable;
        $this->config    = $config;
    }

    /**
     * Magic method
     *
     * @param $name
     * @param $arguments
     */
    public function __call($name, $arguments)
    {
        if ($this->listener->isDisabled()) {
            return;
        }

        if ('postFlush' === $name) {
            if ($this->indexableEntity) {
                $this->persister->replaceOne($this->indexableEntity);
                $this->indexableEntity = null;
            }
        }

        if (in_array($name, ['postPersist', 'postUpdate', 'preRemove'])) {
            /** @var LifecycleEventArgs $event */
            $event = reset($arguments);
            $entity = $event->getEntity();

            $instance = get_class($entity);

            $isInsert = 'postPersist' === $name;
            switch ($instance) {
                case ProductPrice::class:
                    $this->processProductPrice($entity, $isInsert);
                    break;
                case ProductParameter::class:
                    $this->processProductParameter($entity, $isInsert);
                    break;

            }
        }
    }

    /**
     * Update product when updated product price
     *
     * @param ProductPrice $entity
     * @param $isInsert
     */
    private function processProductPrice(ProductPrice $entity, $isInsert)
    {
        $indexableEntity = $entity->getProduct();
        if ($this->persister->handlesObject($indexableEntity) && $this->isIndexable($indexableEntity)) {
            if ($isInsert) {
                $indexableEntity->setPrice($entity);
            }

            $this->indexableEntity = $indexableEntity;
        }
    }

    private function processProductParameter(ProductParameter $entity, $isInsert)
    {
        $indexableEntity = $entity->getProduct();
        if ($this->persister->handlesObject($indexableEntity) && $this->isIndexable($indexableEntity)) {
            if ($isInsert) {
                $indexableEntity->addParameter($entity);
            }

            $this->indexableEntity = $indexableEntity;
        }
    }

    /**
     * @param $entity
     * @return bool
     */
    private function isIndexable($entity)
    {
        return $this->indexable->isObjectIndexable(
            $this->config['index'],
            $this->config['type'],
            $entity
        );
    }
}