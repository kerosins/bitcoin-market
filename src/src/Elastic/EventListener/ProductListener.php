<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Elastic\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use FOS\ElasticaBundle\Doctrine\Listener;
use FOS\ElasticaBundle\Persister\ObjectPersisterInterface;
use FOS\ElasticaBundle\Provider\IndexableInterface;
use Psr\Log\LoggerInterface;

class ProductListener extends Listener
{
    private $isEnabledRuntime = true;

    /**
     * Disable runtime index updating
     */
    public function runtimePopulateDisable()
    {
        $this->isEnabledRuntime = false;
    }

    /**
     * Enable runtime index updating
     */
    public function runtimePopulateEnable()
    {
        $this->isEnabledRuntime = true;
    }

    /**
     * @return bool
     */
    public function isDisabled()
    {
        return !$this->isEnabledRuntime;
    }

    /**
     * @inheritdoc
     */
    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        if ($this->isEnabledRuntime) {
            parent::postPersist($eventArgs);
        }
    }

    /**
     * @inheritdoc
     */
    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        if ($this->isEnabledRuntime) {
            parent::postUpdate($eventArgs);
        }
    }

    /**
     * @inheritdoc
     */
    public function preRemove(LifecycleEventArgs $eventArgs)
    {
        if ($this->isEnabledRuntime) {
            parent::preRemove($eventArgs); // TODO: Change the autogenerated stub
        }
    }

    /**
     * @inheritdoc
     */
    public function postFlush()
    {
        if ($this->isEnabledRuntime) {
            parent::postFlush(); // TODO: Change the autogenerated stub
        }
    }
}