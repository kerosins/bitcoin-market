<?php
/**
 * @author Kondaurov
 */

namespace Elastic\EventListener;


use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Elastica\Document;
use FOS\ElasticaBundle\Event\TransformEvent;
use FOS\ElasticaBundle\Event\TypePopulateEvent;
use Shop\Entity\Seller;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CategoryPropertiesListener implements EventSubscriberInterface
{

    public const INDEX = 'category';

    public const TYPE = 'main';

    /**
     * Disabled|Enabled transformation
     *
     * @var bool
     */
    private $transformationEnabled = true;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function postTransform(TransformEvent $event)
    {
        $entity = $event->getObject();
        if (!$entity instanceof Category || !$this->transformationEnabled) {
            return;
        }

        $document = $event->getDocument();
        //set parents
        $parents = [];
        $parent = $entity->getParent();
        $depth = 1;
        $path = [];

        if ($parent) {
            $document->set('firstParent', [
                'id' => $entity->getParent()->getId(),
                'title' => $entity->getParent()->getTitle()
            ]);
        }

        while ($parent !== null) {
            $parents[] = ['id' => $parent->getId(), 'title' => $parent->getTitle()];
            $path[] = $parent->getTitle();
            $parent = $parent->getParent();
            $depth++;
        }

        $path[] = $entity->getTitle();
        /** @var Document $document */
        $children = $this->assemblyChildren($entity, ($depth + 1));
        $document
            ->set('fullPath', $path)
            ->set('parents', $parents)
            ->set('children', $children)
            ->set('depth', $depth);

        if ('cli' === PHP_SAPI) {//collected only in console
            $ids = array_column($children, 'id');
            $ids[] = $entity->getId();

            $document
                ->set('sellers', $this->getSellers($ids))
                ->set('hasProducts', $this->getProductRepository()->existByCategory($ids));
            ;
        }

        $event->setDocument($document);
    }

    public static function getSubscribedEvents()
    {
        return [
            TransformEvent::POST_TRANSFORM => 'postTransform',
            TypePopulateEvent::PRE_TYPE_POPULATE => 'preTypePopulate',
            TypePopulateEvent::POST_TYPE_POPULATE => 'postTypePopulate'
        ];
    }

    /**
     * @return array
     */
    private function assemblyChildren(Category $category, $depth = 1)
    {
        $children = [];
        foreach ($category->getChildren() as $child) {
            /** @var Category $child */
            $children[] = [
                'id' => $child->getId(),
                'title' => $child->getTitle(),
                'parent' => $category->getId(),
                'depth' => $depth
            ];

            $children = array_merge($children, $this->assemblyChildren($child, ($depth + 1)));
        }

        return $children;
    }

    /**
     * Disables transformation if indexing another type
     *
     * @param TypePopulateEvent $event
     */
    public function preTypePopulate(TypePopulateEvent $event)
    {
        $this->transformationEnabled = self::INDEX === $event->getIndex() && self::TYPE === $event->getType();
    }

    public function postTypePopulate(TypePopulateEvent $event)
    {
        $this->transformationEnabled = true;
    }

    private function getSellers($categories)
    {
        $expr = $this->entityManager->getExpressionBuilder();
        $qb = $this->entityManager->createQueryBuilder();

        $qb
            ->select(['s.id', 's.name title'])
            ->from(Seller::class, 's')
            ->where(
                $expr->in(
                    's.id',
                    $this->entityManager->createQueryBuilder()
                        ->select('p.supplierId')
                        ->from(Product::class, 'p')
                        ->innerJoin('p.categories', 'c')
                        ->where('c IN (:categories)')
                        ->getDQL()
                )
            )
            ->setParameter('categories', $categories);
        ;

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @return ProductRepository
     */
    private function getProductRepository()
    {
        if (!$this->productRepository) {
            $this->productRepository = $this->entityManager->getRepository(Product::class);
        }

        return $this->productRepository;
    }

}