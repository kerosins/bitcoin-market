<?php
/**
 * @author Kondaurov
 */

namespace Elastic\QueryBuilderHelper;


use Elastica\Query\BoolQuery;
use Elastica\Query\Simple;
use Elastica\Query\Term;

/**
 * Class CategoryQueryBuilderHelper
 * @package Elastic\QueryBuilderHelper
 */
class CategoryQueryBuilderHelper
{
    /**
     * Builds has products term
     *
     * @return Term
     */
    public static function hasProducts()
    {
        $query = new Term();
        $query->setTerm('hasProducts', true);

        return $query;
    }

    public static function byEnabled()
    {
        $query = new Term();
        $query->setTerm('enabled', true);

        return $query;
    }

    public static function byFirstParent(BoolQuery $boolQuery, int $parent = null)
    {
        $query = new Term();
        $query->setTerm('firstParent.id', $parent);

        if ($parent) {
            $boolQuery->addMust((new Term())->setTerm('firstParent.id', $parent));
        } else {
            $boolQuery->addMustNot(CategoryQueryBuilderHelper::byExistsFirstParent());
        }

        return $query;
    }

    public static function byExistsFirstParent()
    {
        return new Simple([
            'exists' => ['field' => 'firstParent.id']
        ]);
    }
}