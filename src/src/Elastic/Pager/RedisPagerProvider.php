<?php
/**
 * @author Kondaurov
 */

namespace Elastic\Pager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use FOS\ElasticaBundle\Doctrine\RegisterListenersService;
use FOS\ElasticaBundle\Provider\PagerProviderInterface;
use Kerosin\Doctrine\ORM\EntityRepository;
use Predis\Client;

class RedisPagerProvider implements PagerProviderInterface
{
    public const REGISTERED_PAGERS_KEY = 'zb:pagers:registered';

    private const
        OPTION_INDEX = 'index',
        OPTION_TYPE = 'type',
        OPTION_MAX_PER_PAGE = 'max_per_page',
        OPTION_FIRST_PAGE = 'first_page',
        OPTION_QUERY_BUILDER_METHOD = 'query_builder_method';

    /**
     * @var Client
     */
    private $redis;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var RegisterListenersService
     */
    private $registerListenersService;

    /**
     * @var array
     */
    private $baseOptions;

    /**
     * @var string
     */
    private $objectClass;

    public function __construct(
        Client $redis,
        EntityManagerInterface $entityManager,
        RegisterListenersService $registerListenersService,
        string $objectClass,
        array $baseOptions
    ) {
        $this->redis = $redis;
        $this->entityManager = $entityManager;
        $this->registerListenersService = $registerListenersService;
        $this->baseOptions = $baseOptions;
        $this->objectClass = $objectClass;
    }

    /**
     * @param array $options
     *
     * @return \FOS\ElasticaBundle\Provider\PagerInterface
     */
    public function provide(array $options = [])
    {
        $options = array_replace($this->baseOptions, $options);

        $pagerOptions = new RedisPagerOptions();
        $pagerOptions
            ->setMaxPerPage($options[self::OPTION_MAX_PER_PAGE] ?? 100)
            ->setFirstPage($options[self::OPTION_FIRST_PAGE] ?? 1)
            ->setIndex($options[self::OPTION_INDEX])
            ->setType($options[self::OPTION_TYPE]);
        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository($this->objectClass);
        $redisPager = new RedisPager($repository, $this->redis, $pagerOptions);

        $prefix = RedisPager::getRedisPrefix($pagerOptions);
        $this->redis->sadd(self::REGISTERED_PAGERS_KEY, [$prefix]);

        $queryBuilderMethod = $options[self::OPTION_QUERY_BUILDER_METHOD] ?? 'createQueryBuilder';
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = call_user_func([$repository, $queryBuilderMethod], 'a');

        $countQuery = clone $queryBuilder;
        $count = $countQuery
            ->select('COUNT(a.id)')
            ->getQuery()
            ->setHydrationMode(Query::HYDRATE_SINGLE_SCALAR)
            ->getOneOrNullResult();

        $queryBuilder->select('a.id');
        $query = $queryBuilder->getQuery();
        $query->setHydrationMode(Query::HYDRATE_SCALAR);

        $i = 0;
        $pack = [];
        $page = $pagerOptions->getFirstPage();
        foreach ($query->iterate() as $result) {
            $result = reset($result);
            $pack[] = $result['id'];

            $i++;
            if ($i >= $pagerOptions->getMaxPerPage()) {
                $this->redis->set($prefix . ':' . $page, implode(',', $pack));
                $pack = [];
                $page++;
                $i = 0;
            }
        }
        $page++;
        $this->redis->set($prefix . ':' . $page, implode(',', $pack));

        $redisPager->setTotal($count)->setPages($page);

        $this->registerListenersService->register($this->entityManager, $redisPager, $options);
        return $redisPager;
    }

    /**
     * Deletes data from redis about pager
     *
     * @param string $index
     * @param string $type
     * @param array $options
     */
    public function clearByOptions(string $index, string $type, array $options)
    {
        $pagerOptions = new RedisPagerOptions();
        $pagerOptions
            ->setIndex($index)
            ->setType($type)
            ->setFirstPage($options[self::OPTION_FIRST_PAGE] ?? 1)
            ->setMaxPerPage($options[self::OPTION_MAX_PER_PAGE] ?? 100);

        $prefix = RedisPager::getRedisPrefix($pagerOptions);
        $registeredPagers = $this->redis->smembers(self::REGISTERED_PAGERS_KEY);

        if (!in_array($prefix, $registeredPagers)) {
            return;
        }

        $keys = $this->redis->keys("{$prefix}:*");

        $transaction = $this->redis->transaction();
        foreach ($keys as $key) {
            $transaction->del($key);
        }
        $transaction->srem(self::REGISTERED_PAGERS_KEY, $prefix);
        $transaction->exec();
    }
}