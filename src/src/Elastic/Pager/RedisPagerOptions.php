<?php
/**
 * @author Kondaurov
 */

namespace Elastic\Pager;


class RedisPagerOptions
{
    /**
     * @var int
     */
    private $firstPage;

    /**
     * @var int
     */
    private $maxPerPage;

    /**
     * @var string
     */
    private $index;

    /**
     * @var string
     */
    private $type;

    /**
     * @return int
     */
    public function getFirstPage(): ?int
    {
        return $this->firstPage;
    }

    /**
     * @param int $firstPage
     *
     * @return RedisPagerOptions
     */
    public function setFirstPage(int $firstPage): RedisPagerOptions
    {
        $this->firstPage = $firstPage;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxPerPage(): ?int
    {
        return $this->maxPerPage;
    }

    /**
     * @param int $maxPerPage
     *
     * @return RedisPagerOptions
     */
    public function setMaxPerPage(int $maxPerPage): RedisPagerOptions
    {
        $this->maxPerPage = $maxPerPage;
        return $this;
    }

    /**
     * @return string
     */
    public function getIndex(): ?string
    {
        return $this->index;
    }

    /**
     * @param string $index
     *
     * @return RedisPagerOptions
     */
    public function setIndex(string $index): RedisPagerOptions
    {
        $this->index = $index;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return RedisPagerOptions
     */
    public function setType(string $type): RedisPagerOptions
    {
        $this->type = $type;
        return $this;
    }
}