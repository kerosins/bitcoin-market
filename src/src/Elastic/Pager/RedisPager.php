<?php
/**
 * @author Kondaurov
 */

namespace Elastic\Pager;

use Doctrine\ORM\EntityRepository;
use FOS\ElasticaBundle\Provider\PagerInterface;
use Predis\Client;

class RedisPager implements PagerInterface
{
    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * @var RedisPagerOptions
     */
    private $options;

    /**
     * @var int
     */
    private $total;

    /**
     * @var int
     */
    private $pages;

    /**
     * @var int
     */
    private $currentPage;

    /**
     * @var int
     */
    private $maxPerPage;

    /**
     * @var Client
     */
    private $redis;

    /**
     * @var array
     */
    private $currentResults;

    public function __construct(
        EntityRepository $repository,
        Client $redis,
        RedisPagerOptions $options
    ) {
        $this->repository = $repository;
        $this->redis = $redis;
        $this->options = $options;
        $this->setCurrentPage($options->getFirstPage());
        $this->setMaxPerPage($options->getMaxPerPage());
    }

    /**
     * @return int
     */
    public function getNbResults()
    {
        return $this->total;
    }

    /**
     * @return int
     */
    public function getNbPages()
    {
        return $this->pages;
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @param int $page
     */
    public function setCurrentPage($page)
    {
        $this->currentPage = $page;
        $this->currentResults = null;
    }

    /**
     * @return int
     */
    public function getMaxPerPage()
    {
        return $this->maxPerPage;
    }

    /**
     * @param int $perPage
     */
    public function setMaxPerPage($perPage)
    {
        $this->maxPerPage = $perPage;
    }

    /**
     * @return array
     */
    public function getCurrentPageResults()
    {
        if ($this->currentResults) {
            return $this->currentResults;
        }

        $prefix = self::getRedisPrefix($this->options);
        $page = $this->getCurrentPage();

        $item = $this->redis->get($prefix . ':' . $page);
        if (!$item) {
            return [];
        }

        $ids = explode(',', $item);
        $this->currentResults = $this->repository->findBy(['id' => $ids]);

        return $this->currentResults;
    }

    /**
     * @return string
     */
    public static function getRedisPrefix(RedisPagerOptions $options)
    {
        $prefix = 'zb:elastic:pager:';
        $prefix .= implode(':', [
            $options->getIndex(),
            $options->getType(),
            $options->getMaxPerPage(),
            $options->getFirstPage()
        ]);
        return $prefix;
    }

    /**
     * @param int $total
     *
     * @return RedisPager
     */
    public function setTotal(int $total): RedisPager
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @param int $pages
     *
     * @return RedisPager
     */
    public function setPages(int $pages): RedisPager
    {
        $this->pages = $pages;
        return $this;
    }
}