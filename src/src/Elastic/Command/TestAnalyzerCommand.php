<?php
namespace Elastic\Command;
/**
 * @Author Dmitry Kondaurov
 */

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestAnalyzerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('elastic:analyze')
            ->addArgument('analyzerPath', InputArgument::OPTIONAL, 'path to analyzer\'s configuration')
            ->setDescription('Testing an analyzer')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $client = new Client();
        $testUri = "http://{$container->getParameter('elastic_host')}:9200/_analyze";

        $json = file_get_contents($container->getParameter('data_dir') . '/elastic/analyzer_example.json');

        $output->writeln('Run analyze with following parameters:');
        $output->writeln($json);

        $response = $client->post($testUri, [
            RequestOptions::JSON => json_decode($json)
        ]);

        //validate and pretty-print response
        $body = (string)$response->getBody();
        $body = json_decode($body, true);

        $output->writeln('Analyzed with following tokens:');

        foreach ($body['tokens'] as $item) {
            $output->writeln($item['token']);
        }
    }
}