<?php
/**
 * @author Kondaurov
 */

namespace Elastic\Repository;


use Elastic\Entity\Category;
use Elastic\QueryBuilderHelper\CategoryQueryBuilderHelper;
use Elastica\Query;
use FOS\ElasticaBundle\Finder\PaginatedFinderInterface;
use FOS\ElasticaBundle\Repository;

class ElasticCategoryRepository extends Repository
{
    public function __construct(PaginatedFinderInterface $finder)
    {
        parent::__construct($finder);
    }

    /**
     * @param string $title
     *
     * @return array
     */
    public function findByTitle($title)
    {
        $boolQuery = new Query\BoolQuery();
        $query = new Query();
        $query->setQuery($boolQuery);

        $term = new Query\Term();
        $term->setTerm('title.keyword', $title);
        $boolQuery->addMust($term);

        return $this->find($query);
    }

    /**
     * Finds by seller and parent
     *
     * @param int $seller
     * @param int|null $parent
     */
    public function findBySellerAndParent(int $seller, int $parent = null)
    {
        $boolQuery = new Query\BoolQuery();
        $query = new Query();
        $query->setQuery($boolQuery);

        $boolQuery->addMust((new Query\Term())->setTerm('sellers.id', $seller));
        $boolQuery->addMust(CategoryQueryBuilderHelper::byEnabled());
        CategoryQueryBuilderHelper::byFirstParent($boolQuery, $parent);

        $query->setSort([
            'weight' => 'asc',
            'title.keyword' => 'asc'
        ]);

        return $this->find($query, 5000);
    }

    /**
     * Finds by seller
     *
     * @param int $seller
     *
     * @return array
     */
    public function findBySeller(int $seller)
    {
        $boolQuery = new Query\BoolQuery();
        $query = new Query();
        $query->setQuery($boolQuery);

        $boolQuery->addMust(CategoryQueryBuilderHelper::byEnabled());
        $boolQuery->addMust((new Query\Term())->setTerm('sellers.id', $seller));

        return $this->find($query, 5000);
    }

    /**
     * @param int $id
     *
     * @return \Elastic\Entity\Category|null
     */
    public function findById(int $id)
    {
        $docs = $this->findByAttributeValue('_id', $id);
        return reset($docs);
    }

    /**
     * Finds by ids
     *
     * @param array $ids
     *
     * @return array
     */
    public function findByIds(array $ids)
    {
        $boolQuery = new Query\BoolQuery();
        $query = new Query();
        $query->setQuery($boolQuery);

        $attributeTerm = new Query\Terms();
        $attributeTerm->setTerms('_id', $ids);
        $boolQuery->addMust($attributeTerm);

        return $this->find($query);
    }

    /**
     * Finds by attribute
     *
     * @param $attribute
     * @param $value
     *
     * @return array
     */
    public function findByAttributeValue($attribute, $value)
    {
        $boolQuery = new Query\BoolQuery();
        $query = new Query();
        $query->setQuery($boolQuery);

        $attributeTerm = new Query\Term();
        $attributeTerm->setTerm($attribute, $value);
        $boolQuery->addMust($attributeTerm);

        return $this->find($query);
    }

    /**
     * Find categories with products and parent
     *
     * @param int|null $parent
     */
    public function findByParentAndHasProducts(int $parent = null)
    {
        $boolQuery = new Query\BoolQuery();
        $query = new Query();
        $query->setQuery($boolQuery);

        $boolQuery
            ->addMust(CategoryQueryBuilderHelper::byEnabled())
            ->addMust(CategoryQueryBuilderHelper::hasProducts())
        ;

        CategoryQueryBuilderHelper::byFirstParent($boolQuery, $parent);

        $query->setSort([
            'weight' => 'asc',
            'title.keyword' => 'asc'
        ]);

        return $this->find($query, 5000);
    }

    public function findAllByFirstParent(int $parent = null)
    {
        $boolQuery = new Query\BoolQuery();
        $query = new Query();
        $query->setQuery($boolQuery);

        CategoryQueryBuilderHelper::byFirstParent($boolQuery, $parent);

        $query->setSort([
            'weight' => 'asc',
            'title.keyword' => 'asc'
        ]);

        return $this->find($query, 5000);
    }
}