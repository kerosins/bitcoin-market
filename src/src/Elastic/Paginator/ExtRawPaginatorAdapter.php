<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Elastic\Paginator;


use FOS\ElasticaBundle\Paginator\RawPaginatorAdapter;

class ExtRawPaginatorAdapter extends RawPaginatorAdapter
{
    /**
     * {@inheritdoc}
     */
    public function getResults($offset, $itemCountPerPage)
    {
        return new ExtRawPartialResults($this->getElasticaResults($offset, $itemCountPerPage));
    }
}