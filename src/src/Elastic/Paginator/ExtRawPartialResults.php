<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Elastic\Paginator;


use Elastica\Result;
use FOS\ElasticaBundle\Paginator\RawPartialResults;

class ExtRawPartialResults extends RawPartialResults
{
    /**
     * @inheritdoc
     */
    public function toArray()
    {
        return array_map(function (Result $result) {
            $source = $result->getSource();
            $source['id'] = $result->getId();
            return $source;
        }, $this->resultSet->getResults());
    }
}