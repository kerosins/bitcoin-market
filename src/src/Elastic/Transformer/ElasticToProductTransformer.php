<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Elastic\Transformer;


use Doctrine\Common\Collections\ArrayCollection;
use Elastic\Entity\Category;
use Elastic\Entity\ElasticProduct;
use Elastic\Entity\Seller;
use Elastica\Result;
use FOS\ElasticaBundle\Transformer\ElasticaToModelTransformerInterface;

class ElasticToProductTransformer implements ElasticaToModelTransformerInterface
{
    public function transform(array $elasticaObjects)
    {
        return array_map([$this, 'toModel'], $elasticaObjects);
    }

    public function hybridTransform(array $elasticaObjects)
    {
        return $this->transform($elasticaObjects);
    }

    public function getObjectClass()
    {
        return ElasticProduct::class;
    }

    public function getIdentifierField()
    {
        return 'id';
    }

    private function toModel(Result $document)
    {
        $modelClass = ElasticProduct::class;
        /** @var ElasticProduct $model */
        $model = new $modelClass;

        $data = $document->getData();
        $model->setId($document->getId());

        $seller = new Seller();
//        $seller->setId($data['supplier']['id']);

        $categories = new ArrayCollection(
            array_map(function ($category) {
                return (new Category())
                    ->setId($category['id'])
                    ->setTitle($category['title']);
            }, $data['categories'])
        );

        $model
            ->setTitle($data['title'])
            ->setDescription($data['description'])
            ->setPublicPrice($data['publicPrice'])
            ->setSeller($seller)
            ->setCategories($categories)
            ->setAvailable($data['available'])
            ->setMainImage($data['mainImage'])
        ;

        return $model;
    }
}