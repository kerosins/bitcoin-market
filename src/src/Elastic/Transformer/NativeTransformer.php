<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Elastic\Transformer;

use Elastica\Result;
use FOS\ElasticaBundle\Transformer\ElasticaToModelTransformerInterface;

class NativeTransformer implements ElasticaToModelTransformerInterface
{
    public function transform(array $elasticaObjects)
    {
        return $elasticaObjects;
    }

    public function hybridTransform(array $elasticaObjects)
    {
        return $elasticaObjects;
    }

    public function getObjectClass()
    {
        return Result::class;
    }

    public function getIdentifierField()
    {
        return '_id';
    }

}