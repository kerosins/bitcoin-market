<?php
/**
 * @author Kondaurov
 */

namespace Elastic\Transformer;


use Doctrine\Common\Collections\ArrayCollection;
use Elastic\Entity\Category\ChildCategory;
use Elastic\Entity\Category;
use Elastic\Entity\Seller;
use Elastica\Result;
use FOS\ElasticaBundle\Transformer\ElasticaToModelTransformerInterface;

class ElasticaToCategoryTransformer implements ElasticaToModelTransformerInterface
{
    public function transform(array $elasticaObjects)
    {
        return array_map([$this, 'toCategoryEntity'], $elasticaObjects);
    }

    public function hybridTransform(array $elasticaObjects)
    {
        $objects = $this->transform($elasticaObjects);
        return $objects;
    }

    public function getObjectClass()
    {
        return Category::class;
    }

    public function getIdentifierField()
    {
        return 'id';
    }

    private function toCategoryEntity(Result $document)
    {
        $objectName = $this->getObjectClass();
        /** @var Category $entity */
        $entity = new $objectName;
        $data = $document->getData();

        $entity
            ->setId($document->getId())
            ->setTitle($data['title'])
            ->setEnabled($data['enabled'])
            ->setDepth($data['depth'])
            ->setFullPath($data['fullPath'])
            ->setWeight($data['weight'])
        ;

        if (!empty($data['firstParent'])) {
            $entity->setFirstParent(
                (new Category())
                    ->setId($data['firstParent']['id'])
                    ->setTitle($data['firstParent']['title'])
                    ->setDepth(($data['depth'] - 1))
            );
        }

        //set parents
        $parents = new ArrayCollection();
        foreach ($data['parents'] as $elasticParent) {
            $parents->set(
                $elasticParent['id'],
                (new Category())
                    ->setId($elasticParent['id'])
                    ->setTitle($elasticParent['title'])
            );
        }
        $entity->setParents($parents);

        //set children
        $children = new ArrayCollection();
        foreach ($data['children'] as $elasticChildren) {
            $children->set(
                $elasticChildren['id'],
                (new Category())
                    ->setId($elasticChildren['id'])
                    ->setTitle($elasticChildren['title'])
                    ->setFirstParent((new Category())->setId($elasticChildren['parent']))
                    ->setDepth($elasticChildren['depth'])
            );
        }

        $entity->setChildren($children);

        //set sellers
        $sellers = new ArrayCollection();
        foreach ($data['sellers'] as $elasticSeller) {
            $sellers->set(
                $elasticSeller['id'],
                (new Seller())
                    ->setId($elasticSeller['id'])
                    ->setTitle($elasticSeller['title'])
            );
        }
        $entity->setSellers($sellers);

        return $entity;
    }
}