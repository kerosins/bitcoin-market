<?php
/**
 * @author Kondaurov
 */

namespace Kerosin\DependencyInjection\Contract;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @deprecated DO NOT USE
 *
 * Interface EventDispatcherContract
 * @package Kerosin\DependencyInjection\Contract
 */
interface EventDispatcherContract
{
    /**
     * @param EventDispatcherInterface $eventDispatcher
     *
     * @return mixed
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher);

}