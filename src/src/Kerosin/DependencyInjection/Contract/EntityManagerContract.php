<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\DependencyInjection\Contract;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Add tag for autoconfigure service with EntityManager dependency
 *
 * Interface EntityManagerContract
 * @package Kerosin\DependencyInjection\Contract
 */
interface EntityManagerContract
{
    /**
     * Set a default entity manager
     *
     * @param ObjectManager $entityManager
     * @return mixed
     */
    public function setEntityManager(ObjectManager $entityManager);
}