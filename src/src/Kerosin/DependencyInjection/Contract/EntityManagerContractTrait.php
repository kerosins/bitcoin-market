<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\DependencyInjection\Contract;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Simple implementation for EntityManagerContract
 *
 * Trait EntityManagerContractTrait
 * @package Kerosin\DependencyInjection\Contract
 */
trait EntityManagerContractTrait
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param ObjectManager $entityManager
     * @return $this
     */
    public function setEntityManager(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }
}