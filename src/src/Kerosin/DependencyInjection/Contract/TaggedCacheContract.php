<?php
/**
 * @author Kondaurov
 */

namespace Kerosin\DependencyInjection\Contract;

use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;

interface TaggedCacheContract
{
    public function setTaggedCache(TagAwareAdapterInterface $adapter);
}