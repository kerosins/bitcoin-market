<?php
/**
 * @author Kondaurov
 */

namespace Kerosin\DependencyInjection\Contract;


use Symfony\Component\EventDispatcher\EventDispatcherInterface;

trait EventDispatcherContractTrait
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     *
     * @return mixed
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
        return $this;
    }

    /**
     * @return null|EventDispatcherInterface
     */
    public function getEventDispatcher(): ?EventDispatcherInterface
    {
        return $this->eventDispatcher;
    }
}