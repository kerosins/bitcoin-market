<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\DependencyInjection\Contract;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Register definitions for service contracts
 *
 * Class Compiler
 * @package Kerosin\DependencyInjection\Contract
 */
class Compiler implements CompilerPassInterface
{
    //tags
    public const
        LOGGER = 'logger',
        ENTITY_MANAGER = 'entity_manager',
        TAGGED_CACHE = 'tagged_cache',
        EVENT_DISPATCHER = 'event_dispatcher';

    public const
        MAPPING_TAG = 'tag',
        MAPPING_METHOD = 'method',
        MAPPING_SERVICE = 'service',
        MAPPING_CONTRACT_INTERFACE = 'contract'
    ;

    private static function autowiringDefinitions()
    {
        return [
            self::LOGGER => [
                self::MAPPING_TAG => 'logger_contract',
                self::MAPPING_METHOD => 'setLogger',
                self::MAPPING_SERVICE => 'logger',
                self::MAPPING_CONTRACT_INTERFACE => LoggerContract::class
            ],
            self::ENTITY_MANAGER => [
                self::MAPPING_TAG => 'entity_manager_contract',
                self::MAPPING_METHOD => 'setEntityManager',
                self::MAPPING_SERVICE => 'doctrine.orm.entity_manager',
                self::MAPPING_CONTRACT_INTERFACE => EntityManagerContract::class
            ],
            self::TAGGED_CACHE => [
                self::MAPPING_TAG => 'tagged_cache_contract',
                self::MAPPING_METHOD => 'setTaggedCache',
                self::MAPPING_SERVICE => 'app.cache',
                self::MAPPING_CONTRACT_INTERFACE => TaggedCacheContract::class
            ],
            self::EVENT_DISPATCHER => [
                self::MAPPING_TAG => 'event_dispatcher_contract',
                self::MAPPING_METHOD => 'setEventDispatcher',
                self::MAPPING_SERVICE => 'event_dispatcher',
                self::MAPPING_CONTRACT_INTERFACE => EventDispatcherInterface::class
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function process(ContainerBuilder $container)
    {
        foreach (self::autowiringDefinitions() as $autowiringDefinition) {
            [
                self::MAPPING_TAG => $tag,
                self::MAPPING_METHOD => $method,
                self::MAPPING_SERVICE => $service,
                self::MAPPING_CONTRACT_INTERFACE => $contract
            ] = $autowiringDefinition;
            $this->processTag($container, $tag, $method, $service, $contract);
        }
    }

    /**
     * Processing a tag for autowire and apply service contract
     *
     * @param ContainerBuilder $container
     * @param string $tag
     * @param string $method
     * @param string $injectedServiceId
     * @param string $contactInterface
     */
    private function processTag(
        ContainerBuilder $container,
        string $tag,
        string $method,
        string $injectedServiceId,
        string $contactInterface
    ) {
        if (!$container->has($injectedServiceId)) {
            return;
        }

        $services = $container->findTaggedServiceIds($tag);
        foreach ($services as $service => $tagName) {
            $def = $container->getDefinition($service);
            $def->addMethodCall($method, [new Reference($injectedServiceId)]);
        }
    }

    /**
     * @inheritdoc
     */
    public static function registerAutoconfiguration(ContainerBuilder $builder)
    {
        $mapping = self::autowiringDefinitions();
        foreach ($mapping as $item) {
            $builder
                ->registerForAutoconfiguration($item[self::MAPPING_CONTRACT_INTERFACE])
                ->addTag($item[self::MAPPING_TAG]);
        }
    }

}