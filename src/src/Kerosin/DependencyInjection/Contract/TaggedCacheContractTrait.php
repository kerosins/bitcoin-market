<?php
/**
 * TaggedCacheContractTrait trait file.
 */

namespace Kerosin\DependencyInjection\Contract;

use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;

trait TaggedCacheContractTrait
{
    /**
     * @var TagAwareAdapterInterface
     */
    private $taggedCache;

    public function setTaggedCache(TagAwareAdapterInterface $adapter)
    {
        $this->taggedCache = $adapter;
        return $this;
    }
}