<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\DependencyInjection\Contract;
use Psr\Log\LoggerInterface;

/**
 * Indicate what class accept LoggerInterface
 * Interface LoggerContract
 * @package Kerosin\DependencyInjection\Contract
 */
interface LoggerContract
{
    public function setLogger(LoggerInterface $logger);
}