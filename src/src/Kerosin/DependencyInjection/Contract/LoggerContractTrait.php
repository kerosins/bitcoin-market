<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\DependencyInjection\Contract;
use Psr\Log\LoggerInterface;

/**
 * Simple implement a logger contract
 * Trait LoggerContractTrait
 * @package Kerosin\DependencyInjection\Contract
 */
trait LoggerContractTrait
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Set a logger service
     *
     * @param LoggerInterface $logger
     * @return $this
     */
    public function setLogger(LoggerInterface $logger) {
        $this->logger = $logger;
        return $this;
    }
}