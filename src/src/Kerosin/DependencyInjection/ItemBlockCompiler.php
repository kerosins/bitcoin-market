<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\DependencyInjection;


use Content\ItemBlock\ItemBlockManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ItemBlockCompiler implements CompilerPassInterface
{
    public const TAG = 'item_block';

    public function process(ContainerBuilder $container)
    {
        if (!$container->has(ItemBlockManager::class)) {
            return;
        }

        $services = $container->findTaggedServiceIds(self::TAG);
        $itemBlockManagerDef = $container->getDefinition(ItemBlockManager::class);

        foreach ($services as $service => $tag) {
            $itemBlockManagerDef->addMethodCall('addBlockType', [new Reference($service)]);
        }
    }
}