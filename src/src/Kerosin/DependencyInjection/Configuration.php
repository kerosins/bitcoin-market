<?php
/**
 * KerosinConfiguration class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Kerosin\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $root = $treeBuilder->root('kerosin');

        $root
            ->children()
                ->arrayNode('image_helper')
                    ->children()
                        ->scalarNode('path')->end()
                        ->scalarNode('public_path')->end()
                        ->integerNode('depth')->defaultValue(2)->end()
                        ->integerNode('part_length')->defaultValue(2)->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }

}