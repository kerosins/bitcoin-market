<?php
/**
 * @author Kerosin
 */

namespace Kerosin\DependencyInjection;

use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class KerosinExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config')
        );

        $loader->load('services.yml');

        $processor = new Processor();
        $configuration = new Configuration();

        $config = $processor->processConfiguration($configuration, $configs);

        $container->setParameter('kerosin', $config);
        $this->setParametersToContainer($container);

    }

    private function setParametersToContainer(ContainerBuilder $containerBuilder)
    {
        $rootDir = $containerBuilder->getParameter('kernel.root_dir');
        $mainDir = dirname($rootDir, 2);

        //bc, deprecate names
        $containerBuilder->setParameter('kerosin.main_dir', $mainDir);
        $containerBuilder->setParameter('kernel.config_dir', $rootDir . '/config');
        $containerBuilder->setParameter('kernel.routing_config_dir', $rootDir . '/routing');
        $containerBuilder->setParameter('kernel.is_cli', php_sapi_name() === 'cli');

        $containerBuilder->setParameter('zenith.fs.data_dir', realpath($mainDir . '/data'));
        $containerBuilder->setParameter('zenith.fs.main_dir', $mainDir);
        $containerBuilder->setParameter('zenith.fs.config_dir',$rootDir . '/config');
        $containerBuilder->setParameter('zenith.fs.routing_config_dir', $rootDir . '/routing');
        $containerBuilder->setParameter('zenith.cli_environment', php_sapi_name() === 'cli');
    }
}