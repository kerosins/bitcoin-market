<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Entity;


use Doctrine\ORM\Mapping as ORM;
use Kerosin\Doctrine\ORM\UpdateTimestamps;

/**
 * Class SystemSettings
 * @package Kerosin\Entity
 *
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class SystemSetting
{
    use UpdateTimestamps;

    /**
     * Defined settings
     */
    public const
        SETTINGS_MAINTENANCE = 'maintenance',
        CHECK_PRODUCT_AVAILABLE = 'check_product_available';

    /**
     * Types of value
     */
    public const
        TYPE_STRING = 'string',
        TYPE_INT = 'int',
        TYPE_BOOL = 'bool';

    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $enabled = true;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return SystemSetting
     */
    public function setName(string $name): SystemSetting
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return SystemSetting
     */
    public function setValue(string $value): SystemSetting
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return SystemSetting
     */
    public function setCreatedAt(\DateTime $createdAt): SystemSetting
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return SystemSetting
     */
    public function setTitle(string $title): SystemSetting
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return SystemSetting
     */
    public function setType(string $type): SystemSetting
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * @return bool
     */
    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return SystemSetting
     */
    public function setEnabled(bool $enabled): SystemSetting
    {
        $this->enabled = $enabled;
        return $this;
    }
}