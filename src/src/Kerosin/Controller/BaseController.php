<?php
/**
 * Controller class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Kerosin\Controller;

use Doctrine\ORM\Query;
use Catalog\Order\Cart\ProductCart;
use Content\Navigation\Service\Breadcrumbs;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * Class BaseController
 * @package Kerosin\Controller
 *
 *  @method \User\Entity\User getUser()
 */
class BaseController extends \Symfony\Bundle\FrameworkBundle\Controller\Controller
{
    /**
     * @var \Content\Navigation\Service\Breadcrumbs
     */
    private $breadcrumbs;

    /**
     * @return null|Request
     */
    protected function getRequest()
    {
        return $this->container->get('request_stack')->getCurrentRequest();
    }

    /**
     * @return \Doctrine\ORM\EntityManager|object
     */
    protected function getEm()
    {
        return $this->container->get('doctrine.orm.entity_manager');
    }

    /**
     * @inheritdoc
     */
    protected function json($data, int $status = 200, array $headers = array(), array $context = array()): JsonResponse
    {
        if (is_array($data)) {
            $data['status_code'] = $status;
        }

        $response = parent::json($data, $status, $headers, $context);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Paginate a query
     *
     * @param Query $query
     * @param int $limit
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    protected function paginate(Query $query, $limit = 10)
    {
        $request = $this->getRequest();

        return $this->container->get('knp_paginator')->paginate(
            $query,
            $request->query->getInt('page', 1),
            $limit
        );
    }

    /**
     * Check that request doing via XHR (Ajax)
     * @param bool $throwException
     * @return bool
     */
    protected function checkIfXHR($throwException = true)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        if ($throwException && !$isAjax) {
            throw new BadRequestHttpException('Request must be does via XHR');
        }

        return $isAjax;
    }

    /**
     * Shortcut
     *
     * @param object|object[] $entities
     */
    protected function flushEntities($entities)
    {
        $em = $this->getEm();
        if (is_array($entities)) {
            foreach ($entities as $entity) {
                $em->persist($entity);
            }
        } else {
            $em->persist($entities);
        }

        $em->flush($entities);
    }

    /**
     * Shortcut
     *
     * @param $title
     * @param $url
     */
    protected function addBreadCrumb($title, $url)
    {
        if (!$this->breadcrumbs) {
            $this->breadcrumbs = $this->container->get(Breadcrumbs::class);
        }

        $this->breadcrumbs->add($title, $url);
    }

    /**
     * Shortcut
     *
     * @param array $breadCrumbs
     */
    protected function addBreadCrumbsFromArray(array $breadCrumbs)
    {
        foreach ($breadCrumbs as $crumb) {
            $this->addBreadCrumb(...$crumb);
        }
    }

    /**
     * Transforms exception to json and sends that
     *
     * @param \Exception $exception
     * @param array $extra
     * @return JsonResponse
     */
    protected function jsonException(\Exception $exception, array $extra = [])
    {
        $statusCode = 500;
        if ($exception instanceof HttpExceptionInterface) {
            $statusCode = $exception->getStatusCode();
        }

        $response = [
            'message' => $exception->getMessage()
        ];

        return $this->json(array_merge($response, $extra), $statusCode);
    }

    /**
     * Builds simple array from Form errors
     *
     * todo implement it
     *
     * @param FormInterface $form
     */
    protected function convertFormErrorsToArray(FormInterface $form) : array
    {
        return [];
    }
}