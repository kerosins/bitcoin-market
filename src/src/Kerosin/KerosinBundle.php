<?php
/**
 * KerosinBundle class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Kerosin;

use Kerosin\DependencyInjection\ItemBlockCompiler;
use Kerosin\DependencyInjection\KerosinExtension;
use Content\ItemBlock\ItemBlockInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class KerosinBundle extends Bundle
{

    /**
     * @inheritdoc
     */
    public function getContainerExtension()
    {
        return new KerosinExtension();
    }

    /**
     * @inheritdoc
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new \Kerosin\DependencyInjection\Contract\Compiler());
        \Kerosin\DependencyInjection\Contract\Compiler::registerAutoconfiguration($container);

        $container->addCompilerPass(new ItemBlockCompiler());
        $container->registerForAutoconfiguration(ItemBlockInterface::class)
            ->addTag(ItemBlockCompiler::TAG);
    }
}