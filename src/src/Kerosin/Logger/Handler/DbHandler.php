<?php
/**
 * @author Kondaurov
 */

namespace Kerosin\Logger\Handler;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Kerosin\Entity\BusinessLog;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

class DbHandler extends AbstractProcessingHandler
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct($level = Logger::DEBUG, $bubble = true, EntityManagerInterface $entityManager)
    {
        parent::__construct($level, $bubble);
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $record
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function write(array $record)
    {
        $em = $this->entityManager;

        $logEntry = new BusinessLog();

        $logEntry
            ->setMessage($record['message'])
            ->setLevel($record['level'])
            ->setLevelName($record['level_name'])
            ->setExtra($record['extra'])
            ->setChannel($record['channel'])
            ->setContext($record['context']);

        $em->persist($logEntry);
        $em->flush($logEntry);
        $em->detach($logEntry);
    }

}