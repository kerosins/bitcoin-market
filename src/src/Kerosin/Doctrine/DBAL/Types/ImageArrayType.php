<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Doctrine\DBAL\Types;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Kerosin\Component\Image;

class ImageArrayType extends ImageType
{
    public const TYPE = 'image_array';

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return array|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value === '') {
            return null;
        }

        $value = (is_resource($value)) ? stream_get_contents($value) : $value;
        $value = json_decode($value, true);

        $result = [];

        foreach ($value as $item) {
            if (is_array($item)) {
                $result[] = Image::createFromArray($item);
            }
        }

        return $result;
    }
}