<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Doctrine\DBAL\Types;


interface JsonObjectType extends \JsonSerializable
{
    /**
     * Creates object from array
     *
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data);
}