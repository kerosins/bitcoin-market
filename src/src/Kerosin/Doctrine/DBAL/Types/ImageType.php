<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonArrayType;
use Kerosin\Component\Image;

class ImageType extends JsonArrayType
{
    /**
     * @inheritdoc
     *
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return Image|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $value = parent::convertToPHPValue($value, $platform);
        return is_array($value) ? Image::createFromArray($value) : null;
    }

    public function getName()
    {
        return 'image';
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}