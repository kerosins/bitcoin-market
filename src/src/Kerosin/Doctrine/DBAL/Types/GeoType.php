<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Doctrine\DBAL\Types;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonArrayType;
use Kerosin\Component\GeoObject;

class GeoType extends JsonArrayType
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'geo_object';
    }

    /**
     * @inheritdoc
     *
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return GeoObject|null
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $value = parent::convertToPHPValue($value, $platform);
        return is_array($value) ? GeoObject::fromArray($value) : null;
    }
}