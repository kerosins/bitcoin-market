<?php
/**
 * @author kerosin
 */

namespace Kerosin\Doctrine\ORM;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class MUST BE have HasLifeCycleCallback Annotation
 *
 * Trait UpdateTimestamps
 * @package Kerosin\Doctrine\ORM
 */
trait UpdateTimestamps
{
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateTimestamp()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt = null)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

}