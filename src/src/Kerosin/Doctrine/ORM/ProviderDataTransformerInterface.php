<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Doctrine\ORM;


interface ProviderDataTransformerInterface
{
    /**
     * @param mixed $data
     * @return mixed
     */
    public function transform($data);
}