<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Doctrine\ORM;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

/**
 * @todo Need implement
 *
 * Class BatchQueryProvider
 * @package Kerosin\Doctrine\ORM
 */
class BatchQueryProvider
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(ObjectManager $manager)
    {
        $this->em = $manager;
    }

    /**
     * Provide batch with inner join
     *
     * @param $query
     * @param $size
     */
    public function provideBatchResult(QueryBuilder $query, $size = 20)
    {
        $offset = 0;
        $maxResults = $query->getMaxResults();

        while (true) {
            $results = $query->setFirstResult($offset)->setMaxResults($size)->getQuery()->getResult();
            $results = new ArrayCollection($results);

            if ($results->count() == 0) { //todo реализовать если в билдере установлен msxResults
                break;
            }

            $offset += $size;
            yield $results;
        }
    }
}