<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Doctrine\ORM;

use Doctrine\ORM\Mapping as ORM;

/**
 * Implement mapping for Soft Delete
 *
 * Trait SoftDelete
 * @package Kerosin\Component
 */
trait SoftDelete
{
    /**
     * @ORM\Column(type="boolean")
     */
    private $deleted = false;

    /**
     * @var bool
     */
    private $forceDelete = false;

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param $deleted
     * @return $this
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @param $force
     * @return $this
     */
    public function setForceDelete($force)
    {
        $this->forceDelete = $force;
        return $this;
    }

    /**
     * @return bool
     */
    public function getForceDelete()
    {
        return $this->forceDelete;
    }
}