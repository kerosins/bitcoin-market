<?php
/**
 * EntityRepository class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Kerosin\Doctrine\ORM;

use Doctrine\ORM\EntityRepository as EM;
use Doctrine\ORM\QueryBuilder;

class EntityRepository extends EM
{

    /**
     * @var string
     */
    protected $alias;

    /**
     * @var QueryBuilder
     */
    protected $builder;

    /**
     * @return \Doctrine\DBAL\Connection
     */
    public function getConnection()
    {
        return parent::getEntityManager()->getConnection();
    }

    public function createBuilder($alias)
    {
        $this->alias = $alias;
        $this->builder = $this->createQueryBuilder($alias);
    }

    /**
     * @return array
     */
    public function fetchAll()
    {
        return $this->builder->getQuery()->getArrayResult();
    }

    /**
     * Saves entity to DB
     *
     * @param object $entity
     * @param bool $flush
     *
     * @return static
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($entity, bool $flush = true)
    {
        $this->getEntityManager()->persist($entity);
        if ($flush) {
            $this->getEntityManager()->flush($entity);
        }

        return $this;
    }
}