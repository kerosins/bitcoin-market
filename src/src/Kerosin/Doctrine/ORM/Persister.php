<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Doctrine\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Elastic\EventListener\ProductListener;
use FOS\ElasticaBundle\Persister\Event\Events;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * This class must be uses when inserted|updated a big data to database
 *
 * Class Persister
 * @package Kerosin\Doctrine\ORM
 */
class Persister
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * List of entities names which must be detached after flush
     *
     * @var array
     */
    private $entitiesForDetach = [];

    /**
     * List of entities for detach
     *
     * @var array
     */
    private $entities = [];

    /**
     * @var ProductListener
     */
    private $productListener;

    public function __construct(ObjectManager $entityManager, ProductListener $productListener)
    {
        $this->em = $entityManager;
        $this->productListener = $productListener;
    }

    /**
     * Disables log of doctrine
     */
    public function disableDoctrineLogger()
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    }

    /**
     * Disabled auto populate to Elasticsearch
     */
    public function disableElasticPopulate()
    {
        $this->productListener->runtimePopulateDisable();
    }

    /**
     * Registers an entity class, that must be detached
     *
     * @param $className
     * @return $this
     */
    public function registerEntityForDetach($className)
    {
        if (is_string($className)) {
            $className = [$className];
        }

        $this->entitiesForDetach = array_merge($className, $this->entitiesForDetach);

        return $this;
    }

    /**
     * Persist a entity
     *
     * @param object|array $entity
     * @return $this
     */
    public function persist($entity)
    {
        if (is_array($entity)) {
            foreach ($entity as $value) {
                $this->persist($value);
            }

            return $this;
        }

        $entityName = get_class($entity);
        if (in_array($entityName, $this->entitiesForDetach)) {
            $this->entities[] = $entity;
        }

        $this->em->persist($entity);

        return $this;
    }

    /**
     * Flush data to db
     *
     * @return $this
     */
    public function flush(bool $clear = true)
    {
        $this->em->flush();
        if ($clear) {
            $this->clear();
        }
        return $this;
    }

    /**
     * Clear Doctrine and memory, they leaks:((
     */
    public function clear()
    {
        foreach ($this->entities as $key => $entity) {
            $this->em->getUnitOfWork()->removeFromIdentityMap($entity);
            $this->em->detach($entity);
            unset($this->entities[$key]);
        }

        foreach ($this->entitiesForDetach as $entityName) {
            $this->em->clear($entityName);
        }
        $this->entities = [];

        gc_collect_cycles();
    }
}