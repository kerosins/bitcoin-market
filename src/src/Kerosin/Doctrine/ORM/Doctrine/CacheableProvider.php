<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Doctrine\ORM\Doctrine;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class CacheableProvider extends BaseProvider
{
    protected $cache;

    public function __construct(
        ObjectManager $entityManager,
        Paginator $paginator,
        RequestStack $requestStack,
        TagAwareAdapter $cache
    ) {
        parent::__construct($entityManager, $paginator, $requestStack);
    }

    /**
     * Create a cache key
     *
     * @return mixed
     */
    abstract public function buildCacheKey();

    /**
     * Create a cache tag
     *
     * @return mixed
     */
    abstract public function getTag();

    private function getFromCache()
    {

    }

    private function setToCache()
    {

    }
}