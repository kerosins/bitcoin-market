<?php
/**
 * BaseProvider class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Kerosin\Doctrine\ORM\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use FOS\ElasticaBundle\Paginator\PaginatorAdapterInterface;
use Kerosin\Doctrine\ORM\ProviderDataTransformerInterface;
use Knp\Component\Pager\Pagination\AbstractPagination;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class BaseProvider
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var Paginator
     */
    protected $paginator;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var callable
     */
    protected $resultTransformer;

    /**
     * An query variable
     *
     * @var string
     */
    protected $pageQueryParam = 'page';

    /**
     * Count results on page
     *
     * @var int
     */
    protected $perPage = 10;


    public function __construct(
        EntityManagerInterface $entityManager,
        PaginatorInterface $paginator,
        RequestStack $requestStack
    ) {
        $this->em = $entityManager;
        $this->paginator = $paginator;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * set search result transformer
     * @param $transformer
     */
    public function setResultTransformer($transformer)
    {
        if (!is_callable($transformer) && !$transformer instanceof ProviderDataTransformerInterface) {
            throw new \InvalidArgumentException('Transformer must be callable or instanceof ProviderDataTransformerInterface');
        }

        $this->resultTransformer = $transformer;
    }

    /**
     * @param Query|PaginatorAdapterInterface $query
     * @param int $limit
     *
     * @return PaginationInterface
     */
    protected function buildPaginationResult($query)
    {
        $result = $this->paginator->paginate(
            $query,
            $this->request->get($this->pageQueryParam, 1),
            $this->perPage
        );
        $result = $this->applyTransformer($result);

        return $result;
    }

    /**
     * Apply transformer to items
     *
     * @param PaginationInterface|AbstractPagination $data
     * @return PaginationInterface
     */
    final protected function applyTransformer(PaginationInterface $data)
    {
        if (!$this->resultTransformer) {
            return $data;
        }

        $items = array_map(function ($item) {
            if (is_callable($this->resultTransformer)) {
                $item = call_user_func($this->resultTransformer, $item);
            } else if ($this->resultTransformer instanceof ProviderDataTransformerInterface) {
                $item = $this->resultTransformer->transform($item);
            }

            return $item;
        }, iterator_to_array($data));

        $data->setItems($items);

        return $data;
    }

    /**
     * Set per page value
     *
     * @param int $perPage
     * @return static
     */
    public function setPerPage(int $perPage = 10)
    {
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @return PaginationInterface
     */
    abstract public function search() : PaginationInterface;
}