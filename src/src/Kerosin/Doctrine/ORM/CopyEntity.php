<?php
/**
 * @author kerosin
 */

namespace Kerosin\Doctrine\ORM;


use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class CopyEntity
{

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(ObjectManager $manager)
    {
        $this->em = $manager;
    }

    /**
     * Copy entity
     *
     * @return object
     */
    public function copy($entity)
    {
        $className = get_class($entity);
        $accessor = new PropertyAccessor();

        $metaData = $this->em->getClassMetadata($className);
        $copiedEntity = new $className;

        foreach ($metaData->getColumnNames() as $columnName) {
            if (!$accessor->isWritable($entity, $columnName)) {
                continue;
            }

            $value = $accessor->getValue($entity, $columnName);
            if ($value instanceof Collection) {
                foreach ($value as $item) {
                    $copiedEntity->{'add' . $columnName}($item);
                }
            } else {
                $accessor->setValue($copiedEntity, $columnName, $value);
            }
        }

        return $copiedEntity;
    }

}