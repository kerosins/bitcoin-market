<?php
/**
 * @author Kondaurov
 */

namespace Kerosin\Doctrine\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Kerosin\DependencyInjection\Contract\TaggedCacheContract;
use Kerosin\DependencyInjection\Contract\TaggedCacheContractTrait;
use Catalog\ORM\Entity\Category;
use Shop\Provider\CategoryProvider;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;

class CategoryListener
{
    use TaggedCacheContractTrait;

    public function __construct(TagAwareAdapterInterface $adapter)
    {
        $this->setTaggedCache($adapter);
    }

    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $this->updateEnableProperty($eventArgs);
    }

    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        $this->updateEnableProperty($eventArgs);
    }

    public function preRemove(LifecycleEventArgs $eventArgs)
    {
        $this->updateEnableProperty($eventArgs);
    }

    /**
     * Updates children property "enable"
     *
     * @param LifecycleEventArgs $eventArgs
     *
     * @throws \Exception
     */
    private function updateEnableProperty(LifecycleEventArgs $eventArgs)
    {
        $entityManager = $eventArgs->getEntityManager();
        $entity = $eventArgs->getEntity();
        if (!$entity instanceof Category) {
            return;
        }
        $this->taggedCache->invalidateTags([CategoryProvider::CACHE_TAG]);

        $uow = $entityManager->getUnitOfWork();
        $changeSet = $uow->getEntityChangeSet($entity);

        if (!isset($changeSet['enabled'])) {
            return;
        }

        $entityManager->beginTransaction();

        try {

            [$oldValue, $newValue] = $changeSet['enabled'];

            $children = $entity->getChildren();

            //updates children and parent
            foreach ($children as $child) {
                /** @var Category $child */
                if ($child->getEnabled() !== $newValue) {
                    $child->setEnabled($newValue);
                    $entityManager->persist($child);
                }
            }

            $entityManager->flush();
            $entityManager->commit();
        } catch (\Exception $e) {
            $entityManager->rollback();
            throw $e;
        }
    }
}