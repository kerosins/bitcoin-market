<?php
/**
 * FacetCreateListener class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Kerosin\Doctrine\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\FacetCategory;

class FacetCreateListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof FacetCategory) {
            return;
        }

        $categories = $entity->getCategories();
        $entityManager = $args->getEntityManager();

        foreach ($categories as $category) {
            $childrenCategories = $category->getChildren();
            foreach ($childrenCategories as $child) {
                /** @var FacetCategory $clone */
                /** @var Category $child */
                $entity->addCategory($child);
                $child->addFacet($entity);
            }
        }
    }
}