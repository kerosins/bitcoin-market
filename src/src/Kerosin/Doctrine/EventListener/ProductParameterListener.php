<?php
/**
 * @author kerosin
 */

namespace Kerosin\Doctrine\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use Catalog\ORM\Entity\ProductParameter;

class ProductParameterListener
{
    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();
        if (!$entity instanceof ProductParameter) {
            return;
        }

        if ($entity->getValue() === null) {
            $entity->setValue(0);
            $entity->markToDelete();
        }
    }

    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();
        if (!$entity instanceof ProductParameter) {
            return;
        }

        if ($entity->needDelete()) {
            $eventArgs->getEntityManager()->remove($entity);
        }
    }
}