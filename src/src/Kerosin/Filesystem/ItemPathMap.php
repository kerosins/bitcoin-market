<?php
/**
 * @author Kondaurov
 */

namespace Kerosin\Filesystem;

/**
 * DTO for ImageHelper
 *
 * Class ItemPathMap
 * @package Kerosin\Filesystem
 */
class ItemPathMap
{
    /**
     * @var string
     */
    private $serverPath;

    /**
     * @var string
     */
    private $publicPath;

    /**
     * @var string
     */
    private $type;

    public function __construct(string $type, string $serverPath, string $publicPath)
    {
        $this->type = $type;
        $this->serverPath = $serverPath;
        $this->publicPath = $publicPath;
    }

    /**
     * @return string
     */
    public function getServerPath(): ?string
    {
        return $this->serverPath;
    }

    /**
     * @return string
     */
    public function getPublicPath(): ?string
    {
        return $this->publicPath;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }
}