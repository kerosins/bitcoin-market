<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Filesystem;

use Kerosin\Component\Image;
use Kerosin\DependencyInjection\Contract\TaggedCacheContract;
use Kerosin\DependencyInjection\Contract\TaggedCacheContractTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUploadStack implements TaggedCacheContract
{
    use TaggedCacheContractTrait;

    /**
     * @var TaggedCacheContract
     */
    private $imageHelper;

    /**
     * @var int
     */
    private $ttlStack;

    public function __construct(ImageHelper $imageHelper, int $ttlStack = 1800)
    {
        $this->imageHelper = $imageHelper;
        $this->ttlStack = $ttlStack;
    }

    /**
     * Registers stack for upload queue
     *
     * @param $type
     * @return mixed|string
     */
    public function registerStack($type)
    {
        $stackId = uniqid($type, false);

        if ($this->isRegisteredStack($stackId)) {
            $stackId = $this->registerStack($type);
        }

        $stackFlag = $this->taggedCache->getItem('registered_' . $stackId);
        $stackFlag->set(true);
        $stackFlag->expiresAt($this->getTtl());
        $stackFlag->tag($stackId);
        $this->taggedCache->save($stackFlag);

        return $stackId;
    }

    /**
     * Save file and add to queue
     *
     * @param UploadedFile $file
     * @param string $stackId
     * @param string $type
     * @return string
     *
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \RuntimeException
     */
    public function addToStack(UploadedFile $file, string $stackId, string $type)
    {
        if (!$this->isRegisteredStack($stackId)) {
            throw new \RuntimeException('Unknown file queue');
        }

        $stackDataItem = $this->taggedCache->getItem('file_stack_' . $stackId);
        $data = $stackDataItem->isHit() ? $stackDataItem->get() : [];

        $src = $this->imageHelper->saveUploadedFile($file, $type);
        $data[] = $src;

        $stackDataItem->set($data);
        $stackDataItem->expiresAt($this->getTtl());
        $stackDataItem->tag($stackId);
        $this->taggedCache->save($stackDataItem);

        return $src;
    }

    /**
     * Checks if stack is register
     *
     * @param string $stackId
     * @return bool
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function isRegisteredStack(string $stackId) : bool
    {
        return $this->taggedCache->hasItem('registered_' . $stackId);
    }

    /**
     * @param string $stackId
     * @param bool $clearStack
     *
     * @return string[]|array
     */
    public function getStack(string $stackId, bool $clearStack = true)
    {
        if (!$this->isRegisteredStack($stackId)) {
            return [];
        }

        $stackDataItem = $this->taggedCache->getItem('file_stack_' . $stackId);
        $data = $stackDataItem->get() ?? [];

        if ($clearStack) {
            $this->taggedCache->invalidateTags([$stackId]);
        }

        return $data;
    }

    /**
     * @return \DateTime
     */
    private function getTtl()
    {
        $dateTime = new \DateTime();
        $dateTime->add(new \DateInterval('PT' . $this->ttlStack . 'S'));

        return $dateTime;
    }
}