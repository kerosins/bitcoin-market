<?php
/**
 * ImageWidget class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Kerosin\Filesystem;

class ImageWidget extends \Twig_Extension
{
    /**
     * @var ImageHelper
     */
    private $imageHelper;

    public function __construct(ImageHelper $imageHelper)
    {
        $this->imageHelper = $imageHelper;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('image_path', [$this->imageHelper, 'getPath'])
        ];
    }

    public function getFilters()
    {
        return [
            new \Twig_Filter('replace_image_host', [$this->imageHelper, 'replaceImageHostInString'])
        ];
    }
}