<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Filesystem;


class LdjFile
{
    /**
     * @var \SplFileObject
     */
    private $file;

    /**
     * @var string
     */
    private $path;

    public function __construct(string $filePath, string $mode = 'r')
    {
        $this->path = $filePath;
        $this->file = new \SplFileObject($filePath, $mode);
    }

    /**
     * Reads line and returns json object that converted to array
     */
    public function read()
    {
        $this->file->setFlags(\SplFileObject::SKIP_EMPTY | \SplFileObject::DROP_NEW_LINE);
        while (!$this->file->eof()) {
            $row = $this->file->current();
            if (!$row) {
                break;
            }

            $decoded = json_decode($row, true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new \RuntimeException(sprintf('Invalid json object in line %d, %s', $this->file->key(), $row));
            }

            yield $decoded;

            $this->file->next();
        }
    }

    /**
     * Writes json to file
     *
     * @param mixed $data
     */
    public function write($data)
    {
        $this->file->fwrite(json_encode($data) . PHP_EOL);
    }
}