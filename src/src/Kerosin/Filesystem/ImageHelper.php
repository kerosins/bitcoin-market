<?php
/**
 * FileHelper class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Kerosin\Filesystem;

use Doctrine\Common\Collections\ArrayCollection;
use Kerosin\Component\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageHelper
{
    public const PRODUCT_TYPE = 'product';
    public const BANNER_TYPE = 'banner';
    public const QR_TYPE = 'qr';
    public const DISPUTE = 'dispute';

    /**
     * Map between remote cdn and our
     *
     * @var array
     */
    private $cdnMap = [
        'https://ae01.alicdn.com' => 'https://ae01.alicdn.com'
    ];

    /**
     * @var int
     */
    private $depth;

    /**
     * @var int
     */
    private $partLength;

    /**
     * @var ArrayCollection
     */
    private $pathMap;

    public function __construct(
        int $depth,
        int $partLength,
        array $cdnMap,
        array $pathMap
    ) {
        $this->cdnMap = $cdnMap;

        $this->depth = $depth;
        $this->partLength = $partLength;

        $this->pathMap = new ArrayCollection();
        foreach ($pathMap as $type => $paths) {
            $this->pathMap->set($type, new ItemPathMap($type, $paths['server'], $paths['public']));
        }
    }

    /**
     * @param UploadedFile $file
     * @param string $type
     *
     * @return string
     */
    public function saveUploadedFile(UploadedFile $file, string $type)
    {
        /** @var ItemPathMap $pathItem */
        $pathItem = $this->pathMap->get($type);
        if (!$pathItem) {
            throw new \RuntimeException('Unknown type: ' . $type);
        }

        $path = $pathItem->getServerPath();
        $depth = $this->depth;
        $partLen = $this->partLength;

        $hash = md5_file($file->getPathname());
        $fileName = substr($hash, $partLen * $depth);
        $start = 0;

        while ($depth > 0) {
            $path .= '/' . substr($hash, $start, $partLen);
            $start += $partLen;
            $depth--;
        }

        $extension = $file->guessExtension();
        $fileName .= '.' . $extension;
        $file->move($path, $fileName);

        return $type . '_' . $hash . '.' . $extension;
    }

    /**
     * @param Image $image
     * @param $type
     *
     * @return Image
     */
    public function saveImage(Image $image, $type)
    {
        $file = $image->getFile();
        if ($file instanceof UploadedFile) {
            $src = $this->saveUploadedFile($image->getFile(), $type);
            $image->setSrc($src);
        }
        return $image;
    }

    /**
     * @param UploadedFile[] $files
     * @param string $type
     *
     * @return string[]
     */
    public function saveMany(array $files, string $type)
    {
        return array_map(function ($file) use ($type) {
            return $file instanceof Image ? $this->saveImage($file, $type) : $this->saveUploadedFile($file, $type);
        }, $files);
    }

    /**
     * get a web path to image
     *
     * @param $fileName
     * @return string
     */
    public function getPath($fileName)
    {
        if ($fileName instanceof Image) {
            $url = $fileName->getUrl();
            if ($url) {
                return $this->resolveUrl($url);
            } else {
                $fileName = $fileName->getSrc();
            }
        }

        if (!$fileName) {
            return '';
        }

        [$type, $hash] = explode('_', $fileName);

        /** @var ItemPathMap $pathItem */
        $pathItem = $this->pathMap->get($type);
        if (!$pathItem) {
            throw new \RuntimeException('Unknown type: ' . $type);
        }

        $depth = $this->depth;
        $partLen = $this->partLength;

        $path = $pathItem->getPublicPath();

        $fileName = substr($hash, $depth * $partLen);
        $start = 0;

        while ($depth > 0) {
            $path .= '/' . substr($hash, $start, $partLen);
            $start += $partLen;
            $depth--;
        }


        return "{$path}/{$fileName}";
    }

    /**
     * load file from uri
     * @param $uri
     */
    public function loadFromUri($uri)
    {
        $tmpFilePath = tempnam('/tmp', 'img');
        $tmpFile = fopen($tmpFilePath, 'a');
        fwrite($tmpFile, file_get_contents($uri));

        $originalName = explode('/', $uri);
        $originalName = end($originalName);

        return new UploadedFile(
            $tmpFilePath,
            $originalName,
            null,
            null,
            null,
            true
        );
    }

    /**
     * Change aliexpress image host, to our cdn|directory
     *
     * @param $url
     *
     * @return resolved url
     */
    private function resolveUrl($url): string
    {
        $urlComponents = parse_url($url);
        $remoteHost = "{$urlComponents['scheme']}://{$urlComponents['host']}";

        $ourHost = $this->cdnMap[$remoteHost] ?? null;

        if ($ourHost) {
            $query = isset($urlComponents['query']) ? '?' . $urlComponents['query'] : '';
            $fragment = isset($urlComponents['fragment']) ? '#' . $urlComponents['fragment'] : '';
            return $ourHost . $urlComponents['path'] . $query . $fragment;
        }

        return $url;
    }

    /**
     * @param string $string
     */
    public function replaceImageHostInString(string $string)
    {
        return str_replace(array_keys($this->cdnMap), array_values($this->cdnMap), $string);
    }
}