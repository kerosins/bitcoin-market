<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Component;


class RandomString
{
    public const CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    public static function generate($length = 10)
    {
        $result = '';
        $charsLen = strlen(self::CHARS) - 1;

        while ($length--) {
            $result .= self::CHARS[rand(0, $charsLen)];
        }

        return $result;
    }
}