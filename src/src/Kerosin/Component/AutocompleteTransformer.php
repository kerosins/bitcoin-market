<?php
/**
 * @author Kondaurov
 */

namespace Kerosin\Component;


use Knp\Component\Pager\Pagination\PaginationInterface;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class AutocompleteTransformer
{
    private static $accessor;

    /**
     * Create array for response for autocomplete from array of items
     *
     * @param $array
     * @param string|callable $uidKey
     * @param string|callable $textKey
     *
     * @return array
     */
    public static function fromArray($array, $uidKey, $textKey)
    {
        $result = array_map(function ($item) use ($uidKey, $textKey) {
            return [
                'id' => self::getValue($item, $uidKey),
                'text' => self::getValue($item, $textKey)
            ];
        }, $array);

        return [
            'result' => $result,
            'total' => count($array)
        ];
    }

    /**
     * Create array for response for autocomplete from paginated result
     *
     * @param $paginated
     * @param string|callable $uidKey
     * @param string|callable $textKey
     *
     * @return array
     */
    public static function fromPaginatedResult(PaginationInterface $paginated, $uidKey, $textKey)
    {
        $result = [];

        foreach ($paginated as $item) {
            $result[] = [
                'id' => self::getValue($item, $uidKey),
                'text' => self::getValue($item, $textKey)
            ];
        }

        return [
            'result' => $result,
            'total' => $paginated->getTotalItemCount()
        ];
    }

    /**
     * @param $item
     * @param $textKey
     *
     * @return string
     */
    private static function getValue($item, $keyOrCallable): string
    {
        if (is_callable($keyOrCallable)) {
            return call_user_func($keyOrCallable, $item);
        } else {
            return self::getAccessor()->getValue($item, (is_object($item) ? $keyOrCallable : "[{$keyOrCallable}]"));
        }
    }

    private static function getAccessor()
    {
        if (!self::$accessor) {
            self::$accessor = new PropertyAccessor();
        }

        return self::$accessor;
    }
}