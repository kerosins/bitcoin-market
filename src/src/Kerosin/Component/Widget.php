<?php
/**
 * Widget class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Kerosin\Component;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use User\Entity\User;

class Widget extends \Twig_Extension implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var User
     */
    private $user;

    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
        $this->init();
    }

    /**
     * Init method
     */
    protected function init()
    {
    }

    /**
     * Get Entity Manager
     *
     * @return EntityManager
     */
    protected function em()
    {
        return $this->container->get('doctrine.orm.entity_manager');
    }

    /**
     * Get Request
     *
     * @return null|\Symfony\Component\HttpFoundation\Request
     */
    protected function request()
    {
        return $this->container->get('request_stack')->getCurrentRequest();
    }

    /**
     * @return object|\Twig\Environment
     */
    protected function twig()
    {
        return $this->container->get('twig');
    }

    /**
     * Gets current user
     *
     * @return mixed|null|User
     */
    protected function user()
    {
        $token = $this->container->get('security.token_storage')->getToken();
        if (!$token) {
            return null;
        }

        if (!$this->user) {
            $this->user = $token->getUser();
        }

        return $this->user;
    }

    /**
     * @param $view
     * @param array|object $params
     * @return string
     */
    protected function render($view, $params = [])
    {
        return $this->twig()->render($view, $params);
    }
}