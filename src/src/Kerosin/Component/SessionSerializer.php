<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Component;

/**
 * This uses when need encode|decode another session
 *
 * Class SessionSerializer
 * @package Kerosin\Component
 */
class SessionSerializer
{
    /**
     * Serialize session data
     *
     * @param array $data
     * @return string
     */
    public static function encode(array $data): string
    {
        if (empty($data)) {
            return '';
        }
        $encodedData = '';
        foreach ($data as $key => $value) {
            $encodedData .= $key . '|' . serialize($value);
        }
        return $encodedData;
    }

    /**
     * Unserialize session data
     *
     * @param string $data
     * @return array
     */
    public static function decode(string $data) : array
    {
        if ('' === $data) {
            return [];
        }
        preg_match_all('/(^|;|\})(\w+)\|/i', $data, $matchesarray, PREG_OFFSET_CAPTURE);
        $decodedData = [];
        $lastOffset = null;
        $currentKey = '';
        foreach ($matchesarray[2] as $value) {
            $offset = $value[1];
            if (null !== $lastOffset) {
                $valueText = substr($data, $lastOffset, $offset - $lastOffset);

                $decodedData[$currentKey] = unserialize($valueText);
            }
            $currentKey = $value[0];
            $lastOffset = $offset + strlen($currentKey) + 1;
        }
        $valueText = substr($data, $lastOffset);

        $decodedData[$currentKey] = unserialize($valueText);
        return $decodedData;
    }
}