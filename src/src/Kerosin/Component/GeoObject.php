<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Component;

class GeoObject implements \JsonSerializable
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @param $array
     * @return GeoObject
     */
    public static function fromArray($array)
    {
        $instance = new self();
        $instance
            ->setId(isset($array['id']) ? $array['id'] : null)
            ->setTitle(isset($array['title']) ? $array['title'] : null);

        return $instance;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'title' => $this->title
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return GeoObject
     */
    public function setId(int $id = null): GeoObject
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return GeoObject
     */
    public function setTitle(string $title = null): GeoObject
    {
        $this->title = $title;
        return $this;
    }
}