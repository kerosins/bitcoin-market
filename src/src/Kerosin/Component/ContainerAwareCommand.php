<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Component;

use Doctrine\ORM\EntityManager;

class ContainerAwareCommand extends \Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * Get an Entity Manager
     *
     * @return EntityManager|object
     */
    protected function getEntityManager()
    {
        if (!$this->entityManager) {
            $this->entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        }

        return $this->entityManager;
    }
}