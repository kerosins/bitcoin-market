<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Component\Validator;

use Symfony\Component\Validator\Constraint;

class OnlyLetters extends Constraint
{
    public $message = 'String can only contain letters';
}