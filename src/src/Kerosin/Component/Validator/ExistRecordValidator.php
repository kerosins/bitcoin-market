<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Component\Validator;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ExistRecordValidator extends ConstraintValidator
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param mixed $value
     * @param Constraint|ExistRecord $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $builder = $this->entityManager->createQueryBuilder();
        $builder
            ->select("e")
            ->from($constraint->entity, 'e')
            ->andWhere("e.{$constraint->attribute} = :value")
            ->setParameter('value', $value)
            ->setMaxResults(1);

        if ($constraint->extendQuery && is_callable($constraint->extendQuery)) {
            call_user_func($constraint->extendQuery, $builder);
        }

        $entity = $builder->getQuery()->getOneOrNullResult();
        if ($entity) {
            $this->context->addViolation($constraint->message);
        }
    }
}