<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Component\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class OnlyLettersValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!preg_match('/^[a-zA-Z]+$/', $value)) {
            $this->context
                ->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}