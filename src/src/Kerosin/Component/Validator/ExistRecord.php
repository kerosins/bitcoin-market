<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Component\Validator;


use Symfony\Component\Validator\Constraint;

class ExistRecord extends Constraint
{
    public $message = 'Record already exist in DB';

    /**
     * Target Entity
     *
     * @var string
     */
    public $entity;

    /**
     * Entity attribute
     *
     * @var string
     */
    public $attribute;

    /**
     * Extend query
     *
     * @var callable
     */
    public $extendQuery;
}