<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Component;

use Kerosin\Filesystem\ImageHelper;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Image implements \JsonSerializable
{
    /**
     * @var null|string
     */
    private $src;

    /**
     * @var null|string
     */
    private $title;

    /**
     * @var null|string
     */
    private $alt;

    /**
     * @var null|File
     */
    private $file;

    /**
     * @var string|null
     */
    private $url;

    /**
     * Image constructor.
     * @param string|null $src
     * @param string|null $title
     * @param string|null $alt
     */
    public function __construct(
        string $src = null,
        string $title = null,
        string $alt = null,
        string $url = null
    ) {
        $this->src = $src;
        $this->title = $title;
        $this->alt = $alt;
        $this->url = $url;
    }

    /**
     * @param array $data
     * @return Image
     * @throws \Exception
     */
    public static function createFromArray(array $data)
    {
        return new self(
            !empty($data['src']) ? $data['src'] : null,
            !empty($data['title']) ? $data['title'] : null,
            !empty($data['alt']) ? $data['alt'] : null,
            !empty($data['url']) ? $data['url'] : null
        );
    }

    /**
     * @return mixed
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @param mixed $src
     * @return Image
     */
    public function setSrc($src)
    {
        $this->src = $src;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Image
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * @param mixed $alt
     * @return Image
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
        return $this;
    }

    public function __toString()
    {
        return $this->src ? $this->src : '';
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'src' => $this->src,
            'title' => $this->title,
            'alt' => $this->alt,
            'url' => $this->url
        ];
    }

    /**
     * @return null|File|UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param $file
     */
    public function setFile($file = null)
    {
        $this->file = $file;
    }

    /**
     * @return null|string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param null|string $url
     * @return Image
     */
    public function setUrl(?string $url): Image
    {
        $this->url = $url;
        return $this;
    }
}