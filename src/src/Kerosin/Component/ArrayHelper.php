<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Component;


class ArrayHelper
{
    /**
     * finds and returns element in array uses user defined function
     *
     * @param iterable $haystack
     * @param callable $function - must return true if element was found
     *
     * @return mixed
     */
    public static function arraySearch(iterable $haystack, callable $function)
    {
        foreach ($haystack as $item) {
            $result = call_user_func($function, $item);
            if ($result === true) {
                return $item;
            }
        }

        return null;
    }

    /**
     * Cleans array from empty values, with not save values
     *
     * @param array $array
     *
     * @return array
     */
    public static function arrayClean($array)
    {
        $result = [];
        foreach ($array as $item) {
            if ((bool)$item) {
                array_push($result, $item);
            }
        }

        return $result;
    }
}