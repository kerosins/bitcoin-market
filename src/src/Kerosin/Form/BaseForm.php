<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContext;

class BaseForm extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('constraints', new Callback([
            'callback' => [$this, 'validate']
        ]));
    }

    /**
     * !important You must call parent::configureOptions method in configureOptions, what would it method has been worked
     * !important this method applies always after data transformation, use FormEvents::PRE_SUBMIT if you'll need non-transformed data
     *
     * @param mixed $value
     * @param ExecutionContext $context
     */
    public function validate($value, ExecutionContext $context)
    {
    }
}