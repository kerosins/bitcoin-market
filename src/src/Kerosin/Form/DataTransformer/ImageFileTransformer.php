<?php
/**
 * FileTransformer class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Kerosin\Form\DataTransformer;

use Kerosin\Filesystem\ImageHelper;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\HttpFoundation\File\File;

class ImageFileTransformer implements DataTransformerInterface
{
    /**
     * @var ImageHelper
     */
    private $imageHelper;

    public function __construct(ImageHelper $imageHelper)
    {
        $this->imageHelper = $imageHelper;
    }

    public function transform($value)
    {
        if ($value) {
            if (is_array($value)) {
                return array_map([$this, 'doTransform'], $value);
            } else {
                return $this->doTransform($value);
            }
        }

        return '';
    }

    public function doTransform($value)
    {
        return new File($this->imageHelper->getPath($value), false);
    }

    public function reverseTransform($value)
    {
        return $value;
    }

}