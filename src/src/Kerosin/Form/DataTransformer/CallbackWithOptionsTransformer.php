<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class CallbackWithOptionsTransformer implements DataTransformerInterface
{
    private $options = [];

    /**
     * @var callable
     */
    private $transform;

    /**
     * @var callable
     */
    private $reverseTransform;

    public function __construct(callable $transform, callable $reverseTransform, array $options = [])
    {
        $this->options = $options;
        $this->transform = $transform;
        $this->reverseTransform = $reverseTransform;
    }

    public function transform($data)
    {
        return call_user_func($this->transform, $data, $this->options);
    }

    public function reverseTransform($data)
    {
        return call_user_func($this->reverseTransform, $data, $this->options);
    }
}