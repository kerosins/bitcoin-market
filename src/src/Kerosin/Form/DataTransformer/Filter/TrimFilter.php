<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Form\DataTransformer\Filter;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Trim a string value
 *
 * Class TrimFilter
 * @package Kerosin\Form\DataTransformer\Filter
 */
class TrimFilter implements DataTransformerInterface
{
    private $chars = " \t\n\r\0\x0B";

    public function __construct(string $chars = null)
    {
        if ($chars) {
            $this->chars = $chars;
        }
    }

    /**
     * @inheritdoc
     */
    public function transform($value)
    {
        return $value;
    }

    /**
     * @inheritdoc
     */
    public function reverseTransform($value)
    {
        return trim($value, $this->chars);
    }


}