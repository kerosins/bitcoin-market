<?php
/**
 * AutoCompleteTransformer class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Kerosin\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ArrayToStringTransformer implements DataTransformerInterface
{
    /**
     * Delimiter
     *
     * @var string
     */
    private $delimiter;

    public function __construct($delimiter)
    {
        $this->delimiter = $delimiter;
    }

    /**
     * To
     * @param array $value
     */
    public function transform($value)
    {
        return join($this->delimiter, $value);
    }

    /**
     * From
     * @param string $value
     */
    public function reverseTransform($value)
    {
        if (!$this->delimiter) {
            return [$value];
        }

        return explode($this->delimiter, $value);
    }

}