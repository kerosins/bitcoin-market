<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RangeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('min', TextType::class, [
                'attr' => [
                    'class' => 'input-sm range-input'
                ],
                'required' => $options['required']
            ])
            ->add('max', TextType::class, [
                'attr' => [
                    'class' => 'input-sm range-input'
                ],
                'required' => $options['required']
            ]);

        $builder->addModelTransformer(
            new CallbackTransformer(
                [$this, 'transform'],
                [$this, 'revertTransform']
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'range_field';
    }

    public function transform($value)
    {
        $result = [
            'min' => null,
            'max' => null
        ];

        if (!is_array($value)) {
            return $result;
        }

        if (array_key_exists('from', $value) || array_key_exists('to', $value)) {
            if (array_key_exists('from', $value)) {
                $result['min'] = $value['from'];
            }

            if (array_key_exists('to', $value)) {
                $result['max'] = $value['to'];
            }
        } elseif (array_key_exists('min', $value) && array_key_exists('max', $value)) {
            $result['min'] = $value['min'];
            $result['max'] = $value['max'];
        } else {
            $result['min'] = reset($value);
            $result['max'] = next($value);
        }

        return $result;
    }

    public function revertTransform($value)
    {
        return $value;
    }
}