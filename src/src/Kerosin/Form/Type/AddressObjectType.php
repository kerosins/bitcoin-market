<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Form\Type;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Kerosin\Component\GeoObject;
use Kerosin\Form\DataTransformer\CallbackWithOptionsTransformer;
use Shop\Entity\Country;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Shop\Entity\AddressObject;
use Shop\Entity\ZipCode;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Provide form field for address object or zip code entities
 *
 * Class AddressObjectType
 * @package Kerosin\Form\Type
 */
class AddressObjectType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * @inheritdoc
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', HiddenType::class, ['required' => false]);
        $builder->add('title', AutoCompleteType::class, [
            'required' => false,
            'label' => false,
            'entity_class' => AddressObject::class,
            'ajax_url' => $options['autocomplete_url'],
            'attr' => $options['attr']
        ]);

        $builder->addModelTransformer(new CallbackWithOptionsTransformer(
            [$this, 'toForm'],
            [$this, 'toEntity'],
            $options
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'country' => null,
            'autocomplete_url' => null,
//            'data_class' => GeoObject::class,
        ]);

        $resolver->setAllowedTypes('country', [Country::class, 'null']);
        $resolver->setAllowedTypes('autocomplete_url', 'string');
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
    }

    /**
     * @param GeoObject $value
     * @return array|null
     */
    public function toForm($value = null, $options = [])
    {
        if (!$value) {
            return null;
        }

        if ($value instanceof AddressObject) {
            $title = $this->em->getRepository('ShopBundle:AddressObject')->find($value->getId());
        } else {
            $title = $value->getTitle();
        }

        return [
            'id' => $value->getId(),
            'title' => $title
        ];
    }

    public function toEntity($value, $options)
    {
        if ($value['title'] instanceof AddressObject) {
            $value = $value['title'];
            return GeoObject::fromArray([
                'id' => $value->getId(),
                'title' => $value->getTitle()
            ]);
        } else if ($value['title'] == null) {
            return GeoObject::fromArray([]);
        }

        return GeoObject::fromArray($value);
    }

    public function getBlockPrefix()
    {
        return 'address_object';
    }
}