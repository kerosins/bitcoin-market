<?php
/**
 * @author Kondaurov
 */

namespace Kerosin\Form\Type;

use Kerosin\Form\DataTransformer\CallbackWithOptionsTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DateRangeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('begin', DatePickerType::class, [
                'required' => $options['required']
            ])
            ->add('end', DatePickerType::class, [
                'required' => $options['required']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('date_format', 'Y.m.d');
        $resolver->addAllowedTypes('date_format', 'string');
    }
}