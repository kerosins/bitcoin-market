<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Kerosin\Filesystem\ImageHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;

class XhrFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, [
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('id', HiddenType::class, [
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('type', TextType::class, [
                'constraints' => [
                    new Choice([
                        'choices' => [
                            ImageHelper::DISPUTE
                        ]
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('csrf_protection', false);
    }
}