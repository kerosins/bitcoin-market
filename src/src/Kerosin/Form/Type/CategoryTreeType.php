<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Form\Type;

use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Catalog\ORM\Entity\Category;
use Shop\Provider\CategoryProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryTreeType extends AbstractType implements EntityManagerContract
{
    use EntityManagerContractTrait;

    /**
     * @var CategoryProvider
     */
    private $categoryProvider;

    public function __construct(CategoryProvider $categoryProvider)
    {
        $this->categoryProvider = $categoryProvider;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new CallbackTransformer(
            function ($values) {
                $values = (array)$values;
                return $this->entityManager->getRepository(Category::class)->findBy([
                    'id' => $values
                ]);
            },
            function ($values) {
                return array_map(function (Category $category) {
                    return $category->getId();
                }, $values);
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'multiple' => true,
            'expanded' => true,
            'choices' => $this->categoryProvider->getAllCategoriesWithProducts(),
            'choice_label' => function (Category $category) {
                return str_repeat('-', $category->getDepth()) . ' ' . $category->getTitle();
            },
            'choice_value' => function (Category $category) {
                return $category->getId();
            },
            'choice_attr' => function (Category $category) {
                return [
                    'data-id' => $category->getId(),
                    'data-parent-id' => $category->getParentId(),
                    'data-title' => strtolower($category->getTitle())
                ];
            },
            'allow_extra_fields' => true
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

    public function getBlockPrefix()
    {
        return 'category_tree';
    }
}