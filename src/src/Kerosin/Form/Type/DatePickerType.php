<?php
/**
 * @author Kondaurov
 */

namespace Kerosin\Form\Type;

use Kerosin\Form\DataTransformer\CallbackWithOptionsTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DatePickerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new CallbackWithOptionsTransformer(
            [$this, 'toForm'],
            [$this, 'toEntity'],
            $options
        ));

//        $builder->setAttribute('class', 'js-datepicker');
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
        $view->vars['attr']['class'] = 'js-datepicker';

        //convert to ui's date format
        $format = $options['format'];
        $uiFormat = '';
        $chars = [
            'Y' => 'yyyy',
            'm' => 'mm',
            'd' => 'dd'
        ];
        for ($i = 0; $i < strlen($format); $i++) {
            $uiFormat .= isset($chars[$format[$i]]) ? $chars[$format[$i]] : $format[$i];
        }

        $view->vars['attr']['data-format'] = $uiFormat;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('format', 'd-m-Y');
        $resolver->setAllowedTypes('format', 'string');
    }

    /**
     * Convert to form
     *
     * @param $value
     * @param $options
     *
     * @return string
     */
    public function toForm($value, $options)
    {
        if (!$value instanceof \DateTime) {
            return $value;
        }

        return $value->format($options['format']);
    }

    /**
     * Convert to entity
     *
     * @param $value
     * @param $options
     *
     * @return \DateTime
     */
    public function toEntity($value, $options)
    {
        if ($value instanceof \DateTime) {
            return $value;
        }

        return \DateTime::createFromFormat($options['format'], $value);
    }

    public function getParent()
    {
        return TextType::class;
    }
}