<?php
/**
 * AutoCompleteType class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Kerosin\Form\Type;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Kerosin\Form\DataTransformer\CallbackWithOptionsTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class AutoCompleteType
 * @package Kerosin\Form\Type
 *
 * todo describe about 'value_attr' and 'entity_search_function' options
 */
class AutoCompleteType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(ObjectManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer(new CallbackWithOptionsTransformer(
            [$this, 'toForm'],
            [$this, 'toEntity'],
            $options
        ));
    }
    /**
     * @inheritdoc
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'ajax_url' => '',
            'entity_class' => '',
            'min_length' => 2,
            'can_create' => false,

            'value_attr' => 'id',
            'value_transformer' => null,
            'entity_search_function' => null,

            'text_attr' => 'title',
            'js_prepare_query' => null
        ]);

        $resolver->setAllowedTypes('entity_class', ['string']);
        $resolver->setAllowedTypes('text_attr', ['string', 'callable']);
        $resolver->setAllowedTypes('value_attr', ['string', 'callable']);
        $resolver->setAllowedTypes('entity_search_function', ['null', 'callable']);
        $resolver->setAllowedTypes('ajax_url', ['string']);
        $resolver->setAllowedTypes('can_create', ['bool']);
        $resolver->setAllowedTypes('min_length', ['integer']);
        $resolver->setAllowedTypes('js_prepare_query', ['string', 'null']);

        $resolver->setRequired(['ajax_url', 'entity_class']);
    }

    /**
     * @inheritdoc
     *
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['url'] = $options['ajax_url'];
        $view->vars['canCreate'] = $options['can_create'];
        $view->vars['attr']['data-min-length'] = $options['min_length'];

        if ($options['js_prepare_query']) {
            $view->vars['attr']['data-data-handler'] = $options['js_prepare_query'];
        }
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getParent()
    {
        return TextType::class;
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'autocomplete';
    }

    /**
     * Transform to form
     *
     * @param $value
     * @param array $options
     *
     * @throws UnexpectedTypeException
     *
     * @return array|null
     */
    public function toForm($value, array $options)
    {
        if (null === $value) {
            return null;
        }

        if (!is_object($value) || !$value instanceof $options['entity_class']) {
            throw new UnexpectedTypeException($value, $options['entity_class']);
        }

        $accessor = new PropertyAccessor();

        $textOption = $options['text_attr'];
        if (is_callable($textOption)) {
            $text = call_user_func($textOption, $value);
        } else {
            $text = $accessor->getValue($value, $textOption);
        }

        $idOption = $options['value_attr'];
        if (is_callable($idOption)) {
            $id = call_user_func($idOption, $value);
        } else {
            $id = $accessor->getValue($value, $idOption);
        }

        return [
            'id' => $id,
            'title' => $text
        ];
    }

    /**
     * Transform to model
     *
     * @param $value
     * @param array $options
     *
     * @throws EntityNotFoundException
     *
     * @return object|null
     */
    public function toEntity($value, array $options)
    {
        if (in_array($value, [null, 'null', '-1'])) {
            return null;
        }

        $entityRepository = $this->em->getRepository($options['entity_class']);
        if (is_callable($options['value_attr'])) {
            $entity = call_user_func($options['entity_search_function'], $entityRepository, $value);
        } else {
            $entity = $entityRepository->findOneBy([$options['value_attr'] => $value]);
        }


        if (!$entity) {
            throw EntityNotFoundException::fromClassNameAndIdentifier($options['entity_class'], $value);
        }

        return $entity;
    }
}