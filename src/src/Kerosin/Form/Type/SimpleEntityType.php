<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Form\Type;

use Kerosin\Doctrine\ORM\EntityRepository;
use Kerosin\Form\DataTransformer\CallbackWithOptionsTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Todo remove it
 * @deprecated
 *
 * Class SimpleEntityType
 * @package Kerosin\Form\Type
 */
class SimpleEntityType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new CallbackWithOptionsTransformer(
            [$this, 'toForm'],
            [$this, 'toModel'],
            $options
        );

        $builder->addViewTransformer($transformer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
                ->setDefaults([
                    'repository' => null,
                    'choice_label' => null,
                    'choice_value' => null,
                    'isTextInputType' => false
                ])
                ->setAllowedTypes('repository', [EntityRepository::class])
                ->setAllowedTypes('isTextInputType', 'boolean')
                ->setAllowedTypes('choice_label', ['null', 'string', 'callable'])
                ->setAllowedTypes('choice_value', ['null', 'string', 'callable'])
                ->setRequired(['repository']);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['isTextInput'] = $options['isTextInputType'] ?? false;
    }

    /**
     * Transform value to form
     *
     * @param $value
     * @param $options
     * @return array
     */
    public function toForm($value, $options)
    {
        if (!$value) {
            return [];
        }

        $accessor = new PropertyAccessor();
        $choiceLabel = $accessor->getValue($options, '[choice_label]');
        $choiceValue = $accessor->getValue($options, '[choice_value]');

        $label = '';
        $formValue = '';

        if (null === $choiceLabel) {
            $label = (string)$value;
        } elseif (is_string($choiceLabel)) {
            $label = $accessor->getValue($value, $choiceLabel);
        } elseif (is_callable($choiceLabel)) {
            $label = call_user_func($choiceLabel, $value);
        }

        if ($options['isTextInputType']) {
            return $label;
        }

        if (null === $choiceValue) {
            $formValue = $accessor->getValue($value, 'id');
        } elseif (is_string($choiceValue)) {
            $formValue = $accessor->getValue($value, $choiceValue);
        } elseif (is_callable($choiceValue)) {
            $formValue = call_user_func($choiceValue, $value);
        }

        return [
            'text' => $label,
            'value' => $formValue
        ];
    }

    /**
     * Transform value to model
     *
     * @param $value
     * @param $options
     * @return null|object
     */
    public function toModel($value, $options)
    {
        if (!$value) {
            return null;
        }

        /** @var EntityRepository $repo */
        $repo = $options['repository'];

        $entity = $repo->find($value);

        if (!$entity && $options['isTextTypeInput']) {
            $entityClass = $repo->getClassName();

            $entity = new $entityClass;
            $accessor = new PropertyAccessor();

            $accessor->setValue($entity, $options['choice_label'], $value);
        }


        return $repo->find($value);
    }

    public function getBlockPrefix()
    {
        return 'simple_entity_field';
    }
}