<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Form\Type;


use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class RouteAliasType
 * @package Kerosin\Form\Type
 * @deprecated
 */
class RouteAliasType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', HiddenType::class);
        $builder->add('alias', TextType::class);
        $builder->addModelTransformer(new CallbackTransformer(
            [$this, 'toForm'],
            [$this, 'toModel']
        ));
    }

    /**
     * Transform a model to form
     *
     * @param $value
     * @return array
     */
    public function toForm($value)
    {
        if ($value instanceof RouteAlias) {
            return [
                'id' => $value->getId(),
                'alias' => $value->getAlias()
            ];
        }

        return [];
    }

    /**
     * Transform a data to entity
     *
     * @param $value
     * @return null
     */
    public function toModel($value)
    {
        if (!$value || empty($value['alias'])) {
            return null;
        }

        if (!empty($value['id'])) {
            $entity = $this->em->getRepository('ShopBundle:RouteAlias')->find($value['id']);
            if (!$entity) {
                $entity = new RouteAlias();
                $entity->setAlias($value['alias']);
            }
        } else {
            $entity = new RouteAlias();
            $entity->setAlias($value['alias']);
        }

        return $entity;
    }

    /**
     * @inheritdoc
     */
    public function getBlockPrefix()
    {
        return 'route_alias_field';
    }
}