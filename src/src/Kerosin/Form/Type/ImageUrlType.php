<?php
/**
 * ImageUrlType class file.
 *
 * @author Virtual Frameworks LLC <post@virtualhealth.com>
 * @link http://www.virtualhealth.com/
 * @copyright Copyright &copy; 2011-2017 Virtual Frameworks LLC
 */

namespace Kerosin\Form\Type;

use Kerosin\Component\Image;
use Kerosin\Filesystem\ImageHelper;
use Kerosin\Form\DataTransformer\CallbackWithOptionsTransformer;
use Kerosin\Form\DataTransformer\ImageFileTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageUrlType extends AbstractType
{
    /**
     * @var ImageFileTransformer
     */
    private $transformer;

    /**
     * @var ImageHelper
     */
    private $helper;

    private $options = [];

    /**
     * ImageType constructor.
     * @param ImageFileTransformer $transformer
     */
    public function __construct(ImageFileTransformer $transformer, ImageHelper $helper)
    {
        $this->transformer = $transformer;
        $this->helper = $helper;
    }

    /**
     * Todo перенести трасформер в отдельный класс
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->options = $options;
        $builder->add('src', HiddenType::class, [
            'required' => false
        ]);
        $builder->add('file', ImageType::class, [
            'required' => false
        ]);

        if ($options['extended']) {
            $builder->add('url', TextType::class, [
                'label' => 'Url to image',
                'required' => false
            ]);
            $builder->add('title', TextType::class, [
                'required' => false
            ]);
            $builder->add('alt', TextType::class, [
                'required' => false
            ]);
        }

        $builder->addModelTransformer(new CallbackWithOptionsTransformer(
            [$this, 'toForm'],
            [$this, 'toModel'],
            $options
        ), true);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['extended'] = $options['extended'];
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('multiple', false);
        $resolver->setAllowedTypes('multiple', 'bool');

        $resolver->setDefault('extended', true);
        $resolver->setAllowedTypes('extended', 'bool');
    }

    /**
     * @param null|Image $data
     * @param array $options
     */
    public function toForm($data, array $options)
    {
        $formData = [
            'src' => '',
            'file' => ''
        ];

        if ($options['extended']) {
            $formData['title'] = '';
            $formData['alt'] = '';
            $formData['url'] = '';
        }

        if (!$data) {
            return $formData;
        }

        $formData['src'] = $data->getSrc();

        if ($options['extended']) {
            $formData['title'] = $data->getTitle();
            $formData['alt'] = $data->getAlt();
            $formData['url'] = $data->getUrl();
        }

        return $formData;
    }

    /**
     * @param $data
     * @param array $options
     */
    public function toModel($data, array $options)
    {
        $image = new Image();

        if ($options['extended'] && !empty($data['url'])) {
            $file = $this->helper->loadFromUri($data['url']);
            $image->setFile($file);
        } elseif (!empty($data['file'])) {
            $image->setFile($data['file']);
        }

        if ($options['extended']) {
            $image->setTitle($data['title'])->setAlt($data['alt']);
        }

        $image
            ->setSrc($data['src'])
            ->setUrl($data['url'] ?? null);
        return $image;
    }

    public function getBlockPrefix()
    {
        return 'image_with_url';
    }

    public function transformModel($value)
    {
        return [
            'file' => $value,
            'url' => ''
        ];
    }
}