<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BaseFormFilter
 * @package Kerosin\Form
 *
 * Sets pre defined parameters for filters
 */
class BaseFormFilter extends BaseForm
{
    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'method' => 'get',
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getBlockPrefix()
    {
        return '';
    }
}