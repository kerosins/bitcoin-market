<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Helper;

function array_push_unique(&$array, $value)
{
    if (!in_array($value, $array)) {
        $array[] = $value;
    }
}