<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Helper\StringHelper;

/**
 * String $haystack ends with $needle
 *
 * @param $haystack
 * @param $needle
 * @return bool
 */
function endsWith(string $haystack, string $needle) : bool
{
    $needleLength = mb_strlen($needle);
    if ($needleLength === 0) {
        return true;
    }

    return (bool)(mb_substr($haystack, -$needleLength) === $needle);
}

/**
 *
 * String $haystack start with $needle
 *
 * @param string $haystack
 * @param string $needle
 * @return bool
 */
function startsWith(string $haystack, string $needle) : bool
{
    $needleLength = mb_strlen($needle);
    if ($needleLength === 0) {
        return true;
    }

    return (bool)(mb_substr($haystack, 0, $needleLength) === $needle);
}