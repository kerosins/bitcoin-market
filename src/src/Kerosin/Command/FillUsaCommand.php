<?php
/**
 * @author kerosin
 */

namespace Kerosin\Command;


use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use PHPUnit\Framework\Constraint\Count;
use Shop\Entity\AddressObject;
use Shop\Entity\Country;
use Shop\Entity\ZipCode;
use Shop\Repository\AddressObjectRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class FillUsaCommand extends ContainerAwareCommand
{
    private const ZIP_CODES_URI = 'http://gomashup.com/';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Filesystem
     */
    private $fs;

    private $dir;

    /**
     * @var Country
     */
    private $usa;

    protected function configure()
    {
        $this->setName('address:fill:usa')
            ->setDescription('Fill states, county, city and zip codes');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->dir = $this->getContainer()->getParameter('data_dir') . '/geo';

        $this->fs = new Filesystem();

        $output->writeln('==== LOAD STATES');
        $this->em->beginTransaction();
        $usa = $this->em->getRepository('ShopBundle:Country')->findOneBy(['title' => 'United States']);
        if (!$usa) {
            $usa = new Country();
            $usa
                ->setTitle('United States')
                ->setCode('USA')
                ->setAllowAutocomplete(true);

            $this->em->persist($usa);
        }

        $this->usa = $usa;

        try {
            $result = $this->loadStates();
            $this->em->flush();
            $this->em->commit();
            $output->writeln('Result');
            $output->writeln('Created: ' . $result['created']);
            $output->writeln('Existed:' . $result['exist']);
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            $this->em->rollback();
        }
        $output->writeln('==== LOAD STATES END');

        $output->writeln('==== LOAD CITY AND ZIP');

        $this->em->beginTransaction();
        try {
            $this->loadCityAndZip($output);
            $this->em->commit();
        } catch (\Exception $e) {
            $output->writeln($e->getMessage() . ':' . $e->getLine());
            $this->em->rollback();
        }

        $output->writeln('==== LOAD CITY AND ZIP END');
    }

    private function loadStates()
    {
        $stateFile = $this->dir . '/../usa_states.json';
        $stateFile = realpath($stateFile);
        /** @var AddressObjectRepository $repo */
        $repo = $this->em->getRepository('ShopBundle:AddressObject');
        $result = [
            'created' => 0,
            'exist' => 0
        ];

        if ($this->fs->exists($stateFile)) {
            $states = file_get_contents($stateFile);
            $states = json_decode($states, true);

            foreach ($states as $short => $full) {
                var_dump($states);
                $address = $repo->findStateByAltCode($short);
                if ($address) {
                    $result['exist']++;
                    continue;
                }

                $address = new AddressObject();
                $address->setTitle($full);
                $address->setType(AddressObject::STATE_TYPE);
                $address->setAltTitle($short);
                $address->setCountry($this->usa);

                $result['created']++;
                $this->em->persist($address);
            }
        }

        return $result;
    }

    private function loadCityAndZip(OutputInterface $output)
    {
        /** @var AddressObjectRepository $addressRepo */
        $addressRepo = $this->em->getRepository('ShopBundle:AddressObject');
        $zipRepo = $this->em->getRepository('ShopBundle:ZipCode');

        /** @var AddressObject[] $states */
        $states = $addressRepo->findByType(AddressObject::STATE_TYPE);

        $client = new Client(['base_uri' => self::ZIP_CODES_URI]);

        $renderResult = function ($title, $count) use ($output) {
            $output->writeln('Created:');
            foreach ($count as $name => $i) {
                $output->writeln("{$name}: {$i}");
            }

            $output->writeln('---- load city|zip for ' . $title . ' end');
        };

        $clearedEntities = [];
        foreach ($states as $state) {


            $addressRepo->deleteAllByState($state);
            $output->writeln('---- load city|zip for ' . $state->getTitle());
            $code  = $state->getAltTitle();

            $items = $this->loadZipFromFile($client, $code);

            $count = [
                'county' => 0,
                'city' => 0,
                'zip codes' => 0
            ];

            foreach ($items as $countyTitle => $cities) {
                $countyTitle = strtolower($countyTitle);
                $county = new AddressObject();
                $county
                    ->setTitle(ucfirst($countyTitle))
                    ->setAltTitle($countyTitle)
                    ->setType(AddressObject::COUNTY_TYPE)
                    ->setCountry($this->usa)
                    ->setParent($state);

                $count['county']++;
                $this->em->persist($county);
                $this->em->persist($state);
                $clearedEntities[] = $county;

                foreach ($cities as $cityTitle => $zip) {
                    $cityTitle = strtolower($cityTitle);
                    $city = new AddressObject();
                    $city
                        ->setTitle(ucfirst($cityTitle))
                        ->setAltTitle($cityTitle)
                        ->setType(AddressObject::CITY_TYPE)
                        ->setCountry($this->usa)
                        ->setParent($county);

                    $this->em->persist($city);
                    $clearedEntities[] = $city;

                    foreach ($zip as $code) {
                        $zipEntity = new ZipCode();
                        $zipEntity->setCode($code);
                        $zipEntity->setCountry($this->usa);

                        $city->addZipCode($zipEntity);

                        $this->em->persist($zipEntity);
                        $clearedEntities[] = $zipEntity;
                        $count['zip codes']++;
                    }

                    $count['city']++;
                }

                $this->em->flush();

                foreach ($clearedEntities as $entity) {
                    $this->em->detach($entity);
                }
            }

            $renderResult($state->getTitle(), $count);
        }
    }

    /**
     * @return array
     */
    private function loadZipFromFile(Client $client, $code)
    {
        $fileName = "{$this->dir}/zip_{$code}.json";

        if ($this->fs->exists($fileName)) {
            return json_decode(file_get_contents($fileName), true);
        } else {
            $accessor = new PropertyAccessor();

            $url = 'json.php?fds=geo/usa/zipcode/state/' . $code;
            $res = $client->get($url);

            $res = (string)$res->getBody();
            $res = trim($res, '()');
            $res = json_decode($res, true)['result'];

            $items = [];
            foreach ($res as $key => $item) {
                $county = !empty($item['County']) ? $item['County'] : $item['City'];
                $accessor->setValue($items, "[{$county}][{$item['City']}][{$key}]", $item['Zipcode']);
            }

            file_put_contents($fileName, json_encode($items, JSON_PRETTY_PRINT));

            return $items;
        }
    }
}