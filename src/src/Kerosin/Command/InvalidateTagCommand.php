<?php
/**
 * @author Kondaurov
 */

namespace Kerosin\Command;


use Kerosin\Component\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InvalidateTagCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('cache:invalidate_tag')
            ->addArgument('tag', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('app.cache')->invalidateTags([$input->getArgument('tag')]);
    }
}