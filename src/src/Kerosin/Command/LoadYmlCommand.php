<?php
/**
 * @author Kerosin
 */

namespace Kerosin\Command;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Kerosin\Component\Image;
use Kerosin\Filesystem\ImageHelper;
use Catalog\ORM\Entity\Category;
use Catalog\ORM\Entity\Product;
use Catalog\ORM\Entity\ProductPrice;
use Shop\Entity\Seller;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadYmlCommand extends ContainerAwareCommand
{
    private const INSERT_SIZE = 100;

    /**
     * @var Category[]
     */
    private $categories = [];

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * List of suppliers
     *
     * @var Seller[]
     */
    private $suppliers;

    /**
     * @var ImageHelper
     */
    private $imageHelper;

    private $forceImages = false;

    private $countPersisted = 0;

    private $beginTimer;

    private $ent = [];

    protected function configure()
    {
        $this->setName('shop:load_yml')
            ->addArgument('filename', InputArgument::OPTIONAL, 'Path to xml')
            ->addArgument('forceImages', InputArgument::OPTIONAL, 'Force loading images', false)
            ->setDescription('Load are goods from yml file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $this->output = $output;
        $this->imageHelper = $container->get('kerosin.filesystem.image_helper');
        $this->forceImages = $input->getArgument('forceImages');

        $fileName = $input->getArgument('filename');

        if (!$fileName) {
            $fileName = $container->getParameter('data_dir');
            $fileName .= DIRECTORY_SEPARATOR . 'goods/example_goods.xml';
        }

        if (!file_exists($fileName)) {
            $output->writeln('Yml not found in ' . $fileName);
        }

        $fileName = realpath($fileName);

        $output->writeln('Begin load from ' . $fileName);

        $xml = new \XMLReader();
        $xml->open($fileName);

        $beginTimer = microtime(true);
        try {
            $this->loadSuppliers();
            $this->loadCategories($xml);
            $this->loadGoods($xml);

        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            $output->writeln('End a document in' . (microtime(true) - $beginTimer));
        }

        $output->writeln('End a document in' . (microtime(true) - $beginTimer));
    }

    /**
     * read goods from xml
     * @param \XMLReader $reader
     */
    private function loadGoods(\XMLReader $reader)
    {
        $this->output->writeln('Begin goods');
        $offer = [];
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        try {
            $this->beginTimer = microtime(true);
            while ($reader->read()) {
                if (\XMLReader::ELEMENT === $reader->nodeType && 'offer' === $reader->name) {
                    $id = $reader->getAttribute('id');
                    $offer = $this->readChildElements($reader, 'offer');
                    $offer['id'] = $id;
                }

                if (\XMLReader::END_ELEMENT === $reader->nodeType && 'offer' === $reader->name) {
                    $this->saveGoods($offer, $em);
                }
            }
        } catch (\Exception $e) {
            $em->flush();
            throw $e;
        }

        $this->output->writeln('End goods');
    }

    /**
     * save good to db
     * @param array $item
     */
    private function saveGoods(array $item, ObjectManager $em)
    {
        if (($this->countPersisted % self::INSERT_SIZE) === 0) {
            $em->flush();

            foreach ($this->ent as $ent) {
                $em->detach($ent);
                unset($ent);
            }
            $this->ent = [];
            gc_collect_cycles();

            $em->clear(ProductPrice::class);
            $em->clear(Product::class);

            $this->output->writeln('Flushed ' . self::INSERT_SIZE . ' goods to DB in ' . (microtime(true) - $this->beginTimer));
            $this->output->writeln(memory_get_usage(true));
            $this->beginTimer = microtime(true);
        }

        $product = new Product();

        $product->setTitle($item['title']);

        $mainImage = new Image();
        $mainImage->setUrl($item['image']);

        if ($this->forceImages) {
            $imgFile = $this->imageHelper->loadFromUri($item['image']);
            $mainImage->setFile($imgFile);
            $this->imageHelper->saveImage($mainImage, ImageHelper::PRODUCT_TYPE);
        }

        $mainImage->setTitle($item['title'])->setAlt($item['title']);
        $product->setMainImage($mainImage);

        $url = explode('&ulp=', $item['url']);
        $url = $url[1];
        $product->setExternalLink($url);

        $productPrice = new ProductPrice();
        $productPrice->setValue($item['price']);
        $productPrice->setScale(20);
        $product->setPrice($productPrice);
        $em->persist($productPrice);

        $product->addCategory($this->categories[$item['categoryId']]);
        $product->setSeller($this->getSupplier());

        $em->persist($product);

        $this->ent[] = $product;
        $this->ent[] = $productPrice;

        $this->countPersisted++;
    }

    /**
     * Load array of categories from xml
     * @param \XMLReader $reader
     */
    private function loadCategories(\XMLReader $reader)
    {
        $categories = [];
        while ($reader->read()) {
            if ($reader->nodeType === \XMLReader::ELEMENT && $reader->name === 'categories') {
                $this->output->writeln('Begin categories');
            }

            if ($reader->nodeType === \XMLReader::ELEMENT && $reader->name === 'category') {
                $categories[$reader->getAttribute('id')] = $reader->getAttribute('en');
            }

            if ($reader->nodeType === \XMLReader::END_ELEMENT && $reader->name === 'categories') {
                $this->output->writeln('End Categories');
                $em = $this->getContainer()->get('doctrine.orm.entity_manager');
                $categoryRepo = $em->getRepository(\Catalog\ORM\Entity\Category::class);

                $em->beginTransaction();

                try {
                    $this->categories = array_map(function ($item) use ($em, $categoryRepo) {
                        $category = $categoryRepo->findByTitleNonCaseSensitive($item);
                        if (!$category) {
                            $category = new Category();
                            $category
                                ->setTitle($item)
                                ->setMetaTitle($item)
                                ->setMetaDescription($item);
                            $em->persist($category);
                        }

                        return $category;
                    }, $categories);
                    $em->flush();
                    $em->commit();
                } catch (\Exception $e) {
                    $this->output->writeln('Load categories has been failed');
                    $this->output->writeln((string)$e);
                    $em->rollback();
                    throw $e;
                }
                return;
            }
        }
    }

    /**
     * Read scalar values from child elements
     * @param \XMLReader $reader
     *
     * @return array
     */
    private function readChildElements(\XMLReader $reader, $endElementName)
    {
        $data = [];

        try {
            while ($reader->read()) {
                if (\XMLReader::ELEMENT === $reader->nodeType) {
                    $key = $reader->name;
                    $reader->read();
                    $value = $reader->value;

                    $data[$key] = $value;
                }

                if (\XMLReader::END_ELEMENT === $reader->nodeType && $reader->name === $endElementName) {
                    return $data;
                }
            }
        } catch (\Exception $e) {
            throw new \ErrorException('End of a document');
        }
    }

    /**
     * Get a random supplier, create him if it's need
     *
     * @return Seller
     * @throws \Exception
     */
    private function getSupplier()
    {
        $size = count($this->suppliers) - 1;

        return $this->suppliers[rand(0, $size)];
    }

    /**
     * Load suppliers from task
     * @throws \Exception
     */
    private function loadSuppliers()
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $suppliers = $em
            ->getRepository(Seller::class)
            ->findAll()
        ;

        if ($suppliers) {
            $this->suppliers = $suppliers;
            return;
        }

        $dataFilePath = $this->getContainer()->getParameter('data_dir');
        $dataFilePath .= DIRECTORY_SEPARATOR . 'goods/suppliers.csv';

        $dataFile = new \SplFileObject($dataFilePath);
        $dataFile->setFlags(\SplFileObject::READ_CSV | \SplFileObject::SKIP_EMPTY);
        $dataFile->setCsvControl(";");

        $em->beginTransaction();

        try {
            $item = [];
            while (!$dataFile->eof()) {
                list($item['id'], $item['title']) = $dataFile->fgetcsv();

                if ($item['id'] === 'id') {
                    continue;
                }

                $supplier = new Seller();
                $supplier
                        ->setName(trim($item['title']))
                        ->setExternalId($item['id']);

                $em->persist($supplier);

                $suppliers[] = $supplier;
            }

            $this->suppliers = $suppliers;
            $em->flush($supplier);
            $em->commit();
        } catch (\Exception $e) {
            $this->output->writeln('Failed save suppliers');
            $this->output->writeln($e->getMessage());
            $em->rollback();
            throw $e;
        }

    }
}