<?php
/**
 * @author Kondaurov
 */

namespace Kerosin\Command;

use Kerosin\Component\ContainerAwareCommand;
use Kerosin\Mailer\CustomMailer;
use Catalog\ORM\Entity\Order;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('email:order:send')
            ->addArgument('type', InputArgument::REQUIRED, 'Type of email')
            ->addArgument('order', InputArgument::REQUIRED, 'Order\'s ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $order = $this->getEntityManager()->find(Order::class, $input->getArgument('order'));

        if (!$order) {
            $output->writeln('Order not found');
            exit(1);
        }

        $mailer = $this->getContainer()->get(CustomMailer::class);

        switch ($input->getArgument('type')) {
            case 'received':
                $mailer->orderReceived($order);
                break;
            case 'process':
                $mailer->orderProcess($order);
                break;
            case 'shipped':
                $mailer->orderShipped($order);
                break;
        }

        $output->writeln('Mail has been sent');
    }
}