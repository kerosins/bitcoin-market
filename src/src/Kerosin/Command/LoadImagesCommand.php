<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Command;


use Kerosin\Filesystem\ImageHelper;
use Catalog\ORM\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadImagesCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this->setName('shop:load_images')
            ->setDescription('Load images from remote source');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $imageHelper = $this->getContainer()->get('kerosin.filesystem.image_helper');
        $repo = $em->getRepository(Product::class);
        $products = $repo->findByUnloadedImage();

        $iterator = $products->iterate();
        $startTimer = microtime(true);
        foreach ($iterator as $i => $row) {
            /** @var Product $product */
            $product = $row[0];
            $mainImage = $product->getMainImage();

            $imageFile = $imageHelper->loadFromUri($mainImage->getUrl());
            $src = $imageHelper->saveUploadedFile($imageFile, ImageHelper::PRODUCT_TYPE);
            $mainImage->setSrc($src);

            $repo->updateMainImageById($product->getId(), $mainImage);
            $em->detach($product);

            if (($i % 50) === 0) {
                $output->writeln('load 50 images in ' . (microtime(true) - $startTimer));
                $startTimer = microtime(true);
            }

            if (($i % 500) === 0) {
                gc_collect_cycles();
                $output->writeln('Clear memory');
            }
        }
    }
}