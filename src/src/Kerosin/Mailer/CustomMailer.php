<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\Mailer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Kerosin\DependencyInjection\Contract\EntityManagerContract;
use Kerosin\DependencyInjection\Contract\EntityManagerContractTrait;
use Catalog\ORM\Entity\Dispute;
use Catalog\ORM\Entity\Order;
use Catalog\ORM\Entity\OrderStatus;
use Referral\Entity\ReferralWithdrawal;
use Shop\Entity\Feedback\BaseFeedback;
use Symfony\Component\Routing\RouterInterface;
use User\Entity\BalanceChangeLog;
use User\Entity\User;

/**
 * this class contains predefined calls for send email
 *
 * @todo rewrite
 *
 * Class Mailer
 * @package Kerosin\Mailer
 */
class CustomMailer implements EntityManagerContract
{

    use EntityManagerContractTrait;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var \Twig\Environment
     */
    private $twig;

    /**
     * @var string
     */
    private $senderEmail;

    /**
     * @var string
     */
    private $senderName;

    public function __construct(
        \Swift_Mailer $mailer,
        RouterInterface $router,
        \Twig\Environment $twig,
        string $senderEmail,
        string $senderName
    ) {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->twig = $twig;

        $this->senderEmail = $senderEmail;
        $this->senderName = $senderName;
    }

    public function newUser(User $user)
    {
        $this->send(
            'Account created on the site ZenithBay',
            $user->getEmail(),
            $this->twig->render('@Kerosin/mail/new_user.twig', [
                'user' => $user
            ])
        );
    }


    /**
     * EMAILS RELATED WITH ORDERS
     */

    /**
     * Send when order received
     *
     * @param Order $order
     */
    public function orderReceived(Order $order)
    {
        $this->send(
            'Your order ' . $order->getCode() . ' at ZenithBay has been successfully placed!',
            $order->getDelivery()->getEmail(),
            $this->twig->render('@Kerosin/mail/order/customer/received.twig', [
                'order' => $order
            ])
        );
    }

    /**
     * Send when payment received
     *
     * @param Order $order
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function orderProcess(Order $order)
    {
        $this->send(
            'Your order ' . $order->getCode() . ' at ZenithBay has been successfully paid-up',
            $order->getDelivery()->getEmail(),
            $this->twig->render('@Kerosin/mail/order/customer/process.twig', [
                'order' => $order
            ])
        );
    }

    /**
     * Send when order changes status to 'Shipping'
     *
     * @param Order $order
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function orderShipped(Order $order)
    {
        $this->send(
            'Your order ' . $order->getCode() . ' at ZenithBay has been shipped and now is in transit',
            $order->getDelivery()->getEmail(),
            $this->twig->render('@Kerosin/mail/order/customer/shipped.twig', [
                'order' => $order
            ])
        );
    }

    /**
     * Remind user, that its should be accepts order
     *
     * @param Order $order
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function orderRemind(Order $order)
    {
        $this->send(
            'Your order ' . $order->getCode() . ' at ZenithBay has its Buyer protection period expired on ' . $order->getEolDate()->format('m/d/Y'),
            $order->getDelivery()->getEmail(),
            $this->twig->render('@Kerosin/mail/order/customer/remind.twig', [
                'order' => $order
            ])
        );
    }

    /**
     * Sends mail when order was expired and didn't accept
     *
     * @param Order $order
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function orderExpirationEnd(Order $order)
    {
        $this->send(
            'Buyer protection for your order ' . $order->getCode() . ' at ZenithBay has come to a close ' . $order->getEolDate()->format('m/d/Y'),
            $order->getDelivery()->getEmail(),
            $this->twig->render(
                '@Kerosin/mail/order/customer/expire_end.twig'
            )
        );
    }

    /**
     * Send notify about order to admins
     *
     * @param Order $order
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendNotifyEmailToModeratorAboutOrder(Order $order)
    {
        $admins = $this->getModerators();

        foreach ($admins as $admin) {
            $this->send(
                'You have received a new order at ZenithBay',
                $admin->getEmailCanonical(),
                $this->twig->render('@Kerosin/mail/order/moderator/order_process.twig', [
                    'order' => $order,
                    'manager' => $admin
                ])
            );
        }
    }

    public function sendNotifyToModeratorAboutEol(Order $order)
    {
        $admins = $this->getModerators();

        foreach ($admins as $admin) {
            $this->send(
                'Order yet doesn\'t process',
                $admin->getEmailCanonical(),
                $this->twig->render('@Kerosin/mail/order/moderator/order_remind.twig', [
                    'order' => $order,
                    'manager' => $admin
                ])
            );
        }
    }

    /**
     * Send notify when order was canceled
     *
     * @param Order $order
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function orderCancel(Order $order)
    {
        $admins = $this->getModerators();

        foreach ($admins as $admin) {
            $this->send(
                'Order ' . $order->getCode() . ' has been canceled',
                $admin->getEmailCanonical(),
                $this->twig->render('@Kerosin/mail/order/moderator/order_cancel.twig', [
                    'order' => $order
                ])
            );
        }
    }

    /**
     * Sends mail to customer when he's order has been changed status
     *
     * @param Order $order
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function changeOrderStatus(Order $order)
    {
        $this->send(
            'Your order ' . $order->getCode() . ' at ZenithBay has updated its status',
            $order->getDelivery()->getEmail(),
            $this->twig->render('@Kerosin/mail/order/customer/status_change.twig', [
                'order' => $order
            ])
        );
    }

    public function sendNotify(Order $order)
    {
        $this->send(
            'Your have a new message about order ' . $order->getCode() . ' on ZenithBay',
            $order->getDelivery()->getEmail(),
            $this->twig->render('@Kerosin/mail/order/customer/message.twig', [
                'order' => $order,
                'user' => $order->getUser()
            ])
        );
    }

    public function sendNotifyToModerators(Order $order)
    {
        $this->sendToModerators(
            'User left a new message to order ' . $order->getCode() . ' on ZenithBay',
            $this->twig->render('@Kerosin/mail/order/moderator/message.twig', [
                'order' => $order,
                'fromUser' => $order->getUser()
            ])
        );
    }

    public function sendFeedbackReply(BaseFeedback $feedback)
    {
        $this->mailer->send(
            (new \Swift_Message($feedback->getAnswerSubject()))
                ->setFrom($this->senderEmail, $this->senderName)
                ->setTo($feedback->getEmail())
                ->setBody(
                    $this->twig->render('@Kerosin/mail/feedback_reply.twig', [
                        'feedback' => $feedback
                    ]),
                    'text/html'
                )
        );
    }

    public function orderRefund(Dispute $dispute)
    {
        $full = $dispute->getStatus() === Dispute::STATUS_FULL_REFUND;
        $order = $dispute->getOrderPosition()->getOrder();
        $subject = sprintf(
            'Position %s in order %s at ZenithBay has been %s refunded',
            $dispute->getOrderPosition()->getProduct()->getTitle(),
            $order->getCode(),
            ($full ? 'fully' : 'partially')
        );

        $this->send(
            $subject,
            $order->getDelivery()->getEmail(),
            $this->twig->render('@Kerosin/mail/order/customer/refund.twig', [
                'order' => $order,
                'full' => $full
            ])
        );
    }

    /**
     * Disputes
     */

    /**
     * Sends email to customer about opened dispute
     *
     * @param Dispute $dispute
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function disputeOpen(Dispute $dispute)
    {
        $order = $dispute->getOrderPosition()->getOrder();
        $this->send(
            'You have opened a dispute on ZenithBay',
            $order->getDelivery()->getEmail(),
            $this->twig->render('@Kerosin/mail/dispute/customer/open.twig', [
                'dispute' => $dispute,
                'order' => $order
            ])
        );
    }

    /**
     * Send email to moderators about opened dispute
     *
     * @param Dispute $dispute
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function disputeOpenModerator(Dispute $dispute)
    {
        $subject = 'The user has opened the dispute.';
        $order = $dispute->getOrderPosition()->getOrder();
        $body = $this->twig->render('@Kerosin/mail/dispute/moderator/open.twig', [
            'dispute' => $dispute,
            'order' => $order
        ]);

        foreach ($this->getModerators() as $moderator) {
            $this->send(
                $subject,
                $moderator->getEmail(),
                $body
            );
        }
    }

    /**
     * Sends email to customer
     *
     * @param Dispute $dispute
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function disputeCancelByUser(Dispute $dispute)
    {
        $order = $dispute->getOrderPosition()->getOrder();
        $this->send(
            'You have closed the dispute on ZenithBay',
            $order->getDelivery()->getEmail(),
            $this->twig->render('@Kerosin/mail/dispute/customer/cancel.twig', [
                'dispute' => $dispute,
                'order' => $order
            ])
        );
    }

    /**
     * Sends emails to moderators
     *
     * @param Dispute $dispute
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function disputeCancelByUserToModerators(Dispute $dispute)
    {
        $subject = 'The user has closed the dispute.';
        $order = $dispute->getOrderPosition()->getOrder();

        $body = $this->twig->render('@Kerosin/mail/dispute/moderator/cancel.twig', [
            'dispute' => $dispute,
            'order' => $order
        ]);

        foreach ($this->getModerators() as $moderator) {
            $this->send(
                $subject,
                $moderator->getEmail(),
                $body
            );
        }
    }

    /**
     * Sends email to customer about decline dispute
     *
     * @param Dispute $dispute
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function declineDispute(Dispute $dispute)
    {
        $order = $dispute->getOrderPosition()->getOrder();
        $this->send(
            'Your dispute on ZenithBay is dismissed',
            $order->getDelivery()->getEmail(),
            $this->twig->render('@Kerosin/mail/dispute/customer/decline.twig', [
                'dispute' => $dispute,
                'order' => $order
            ])
        );
    }

    public function newMessageDispute(Dispute $dispute)
    {
        $order = $dispute->getOrderPosition()->getOrder();
        $this->send(
            'Dispute updates on ZenithBay',
            $order->getDelivery()->getEmail(),
            $this->twig->render('@Kerosin/mail/dispute/customer/notify.twig', [
                'dispute' => $dispute,
                'order' => $order
            ])
        );
    }

    public function newMessageDisputeForModerators(Dispute $dispute)
    {
        $subject = 'The user left a new message in the dispute.';
        $order = $dispute->getOrderPosition()->getOrder();

        $body = $this->twig->render('@Kerosin/mail/dispute/moderator/notify.twig', [
            'dispute' => $dispute,
            'order' => $order
        ]);

        foreach ($this->getModerators() as $moderator) {
            $this->send(
                $subject,
                $moderator->getEmail(),
                $body
            );
        }
    }

    /**
     * Sends email notify about withdrawal request
     *
     * @param ReferralWithdrawal $referralWithdrawal
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendWithdrawalNotify(ReferralWithdrawal $referralWithdrawal)
    {
        $body = $this->twig->render('@Kerosin/mail/withdrawal/moderator/request.twig', [
            'withdrawal' => $referralWithdrawal
        ]);
        $this->sendToModerators('Withdrawal Request', $body);
    }

    /**
     * sends simple email
     *
     * @param string $subject
     * @param string $to
     * @param string $body
     */
    private function send(string $subject, string $to, string $body)
    {
        $this->mailer->send(
            (new \Swift_Message($subject))
            ->setFrom($this->senderEmail, $this->senderName)
            ->setTo($to)
            ->setBody($body, 'text/html')
        );
    }

    /**
     * Send mail to all moderators
     */
    private function sendToModerators(string $subject, string $body)
    {
        $moderators = $this->getModerators();
        foreach ($moderators as $moderator) {
            $this->send($subject, $moderator->getEmail(), $body);
        }
    }

    /**
     * Gets are moderators
     *
     * @return User[]
     */
    private function getModerators()
    {
        $moderators = $this->entityManager->getRepository(User::class)->findByRole(User::ROLE_ADMIN);
        $moderators = array_filter($moderators, function (User $user) {
            return !$user->hasRole(User::ROLE_SUPER_ADMIN);
        });
        return $moderators;
    }
}