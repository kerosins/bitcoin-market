<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\SystemSettings;

use Kerosin\Doctrine\ORM\EntityRepository;
use Kerosin\Entity\SystemSetting;
use Kerosin\SystemSettings\Exception\SystemSettingException;
use Psr\Cache\CacheItemInterface;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;
use Symfony\Component\HttpKernel\CacheClearer\CacheClearerInterface;
use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface;

class SettingsManager implements CacheWarmerInterface, CacheClearerInterface
{
    private const CACHE_TAG = 'system_settings';

    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * @var TagAwareAdapterInterface
     */
    private $tagAwareAdapter;

    /**
     * @var SettingValueConverter
     */
    private $converter;

    public function __construct(
        EntityRepository $entityRepository,
        TagAwareAdapterInterface $tagAwareAdapter,
        SettingValueConverter $converter
    ) {
        $this->entityRepository = $entityRepository;
        $this->tagAwareAdapter = $tagAwareAdapter;
        $this->converter = $converter;
    }

    /**
     * Gets a setting value by name
     *
     * @param $name
     */
    public function getValue($name)
    {
        $cacheItem = $this->tagAwareAdapter->getItem($this->buildCacheKey($name));
        if (!$cacheItem->isHit()) {
            /** @var SystemSetting $setting */
            $setting = $this->entityRepository->findOneBy(['name' => $name]);
            if (!$setting) {
                throw new SystemSettingException("Unknown setting with name {$name}");
            }

            $cacheItem = $this->buildCacheItem($setting);
        }

        return $cacheItem->get();
    }

    /**
     * @param string $name
     * @param $value
     *
     * @throws SystemSettingException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function setValue(string $name, $value)
    {
        /** @var SystemSetting $setting */
        $setting = $this->entityRepository->find($name);
        if (!$setting || !$setting->getEnabled()) {
            throw new \RuntimeException("System setting {$name} not found or is not enabled");
        }
        $value = $this->converter->toDb($value, $setting->getType());
        $setting->setValue($value);
        $this->save($setting);
    }

    /**
     * @param SystemSetting $setting
     *
     * @throws SystemSettingException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function save(SystemSetting $setting)
    {
        $this->entityRepository->save($setting);
        $cacheItem = $this->buildCacheItem($setting);
        $this->tagAwareAdapter->save($cacheItem);
    }

    /**
     * Conversion string value to specific type
     *
     * @param string $value
     * @param string $type
     *
     * @return mixed
     *
     * @throws SystemSettingException
     */
    public function conversionValueToPHP(string $value, string $type)
    {
        switch ($type) {
            case SystemSetting::TYPE_STRING:
                return $value;
            case SystemSetting::TYPE_INT:
                return (int)$value;
            case SystemSetting::TYPE_BOOL:
                return (bool)$value;
        }

        throw new SystemSettingException('Unknown type of value');
    }

    /**
     * builds a cache key
     *
     * @param string $name
     * @return string
     */
    private function buildCacheKey(string $name)
    {
        return 'system_setting_' . $name;
    }

    /**
     * @param SystemSetting $setting
     * @return \Symfony\Component\Cache\CacheItem
     * @throws SystemSettingException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function buildCacheItem(SystemSetting $setting): CacheItemInterface
    {
        $cacheItem = $this->tagAwareAdapter->getItem($this->buildCacheKey($setting->getName()));
        $cacheItem->set($this->converter->toPhp($setting->getValue(), $setting->getType()));
        $cacheItem->tag(self::CACHE_TAG);

        return $cacheItem;
    }

    /**
     * BELLOW ARE CACHE WARMER AND CLEARER METHODS
     */

    /**
     * @inheritdoc
     *
     * @return bool
     */
    public function isOptional()
    {
        return false;
    }

    /**
     * @inheritdoc
     *
     * @param string $cacheDir
     * @throws SystemSettingException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function warmUp($cacheDir)
    {
        /** @var SystemSetting[] $settings */
        $settings = $this->entityRepository->findBy([
            'enabled' => true
        ]);

        foreach ($settings as $setting) {
            $cacheItem = $this->buildCacheItem($setting);
            $this->tagAwareAdapter->saveDeferred($cacheItem);
        }

        $this->tagAwareAdapter->commit();
    }

    /**
     * @inheritdoc
     *
     * @param string $cacheDir
     */
    public function clear($cacheDir)
    {
        $this->tagAwareAdapter->invalidateTags([self::CACHE_TAG]);
    }
}