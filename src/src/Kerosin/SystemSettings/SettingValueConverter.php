<?php
/**
 * @Author Dmitry Kondaurov
 */

namespace Kerosin\SystemSettings;


class SettingValueConverter
{
    public const
        STRING = 'string',
        INT = 'int',
        BOOL = 'bool';

    /**
     * @param mixed $value
     * @param string $type
     *
     * @return string
     */
    public function toDb($value, string $type)
    {
        switch ($type) {
            case self::BOOL:
                return (bool)$value ? 't' : 'f';
            default:
                return (string)$value;
        }
    }

    /**
     * @param string $value
     * @param string $type
     *
     * @return mixed
     */
    public function toPhp(string $value, string $type)
    {
        switch ($type) {
            case self::INT:
                return (int)$value;
            case self::BOOL:
                return $value === 't';
            default:
                return $value;
        }
    }
}