module.exports = {
    //admin
    adminMain: [ './Admin/main.js' ],
    adminOrdersPage: ['./Admin/orders.js'],
    adminWithdrawals: ['./Admin/withdrawals.js'],

    //orders
    checkout: [
        './Shop/order/checkout.js',
        './Shop/order/payment.js'
    ],

    //chat
    chat: './Shop/my/chat.js',

    catalog: './Shop/catalog/index.js',
    shopMain: [ './Shop/main.js' ],
    referrals: './Shop/my/referral.js',
    zenithTheme: './ZenithMainTheme/index.js',

    //landings
    landings: './Shop/landings/index.js',

    //vendor libs
    vendors: './vendors/index.js'
};