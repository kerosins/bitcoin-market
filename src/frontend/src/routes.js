export default {
    addCart: '/cart/add',
    removeFromCart: '/cart/remove',
    updateCheckout: '/cart/update?type=checkout',
    autocompleteCity: '/address/autocomplete',
    autocompleteZip: '/address/zip',
    search: '/catalog/search'
};