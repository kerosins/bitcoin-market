//js libs
import 'bluebird';
import 'popper.js';
//import plugins
import 'jquery-ui/ui/widgets/autocomplete';
import 'jquery-ui/ui/widgets/sortable';
import 'select2/dist/js/select2.full';
import 'bootstrap-datepicker';
import 'jquery.appear';
import 'jquery.easing';
import 'jquery.cookie';
import 'jquery-lazyload';
import 'owl.carousel';
import 'magnific-popup';
import '@zeitiger/elevatezoom';

import 'bootstrap';
import 'common/bootstrap';
//styles
import 'vendors/bootstrap/dist/css/bootstrap.css'
import 'font-awesome/css/font-awesome.css';
import '@fortawesome/fontawesome-free/css/regular.css';
import '@fortawesome/fontawesome-free/css/solid.css';
import '@fortawesome/fontawesome-free/css/fontawesome.css';
import 'select2/dist/css/select2.css';
import 'bootstrap-datepicker/dist/css/bootstrap-datepicker.css';
import 'magnific-popup/dist/magnific-popup.css';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'jquery-ui/themes/base/autocomplete.css';

//is temp, for old markup
import 'Shop/style/main.less';
import 'common/style/cstyle.less';

window.internalJQuery = jQuery;
window.internal$ = jQuery;