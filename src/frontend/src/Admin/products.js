(function ($, window, document) {
    $(function () {
        //prices
        $(document).on('click', '.js-price-edit-btn', function (e) {
            e.preventDefault();

            const $row = $(this).closest('.js-price-row');
            togglePriceForm($row.data('item-id'));
        });

        $(document).on('click', '.js-price-save__cancel', function (e) {
            e.preventDefault();

            const $row = $(this).closest('.js-price-row');
            togglePriceForm($row.data('item-id'), false);
        });

        $(document).on('click', '.js-price-save', function (e) {
            e.preventDefault();

            const $this = $(this);
            const $row = $this.closest('.js-price-row');
            //build query

            const data = {
                scale: $row.find('.js-price-item__form input[name="scale"]').val(),
                value: $row.find('.js-price-item__form input[name="value"]').val()
            };

            $.post($this.attr('href'), data)
                .then(function (response) {
                    $row.replaceWith(response.row);
                })
                .catch(function (err) {
                    $row.replaceWith(err.responseJSON.row);
                })
            ;
        });
    });

    function togglePriceForm(productId, showForm = true) {
        const $row = $(`.js-price-row[data-item-id="${productId}"]`);
        //find elements
        //item elements
        const $itemElements = $('.js-price-item, .js-price-edit-btn, .js-price-remove', $row);
        //form elements
        const $formElements = $('.js-price-item__form, .js-price-save, .js-price-save__cancel', $row);

        $itemElements.toggleClass('hide', showForm);
        $formElements.toggleClass('hide', !showForm);
    }
} (window.internalJQuery, window, document));