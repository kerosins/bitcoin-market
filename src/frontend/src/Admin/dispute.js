import 'Admin/styles/dispute.less';

/**
 * chat and disputes
 */
(function ($, document, window) {
    $(function () {
        const $document = $(document);

        //scroll messages list
        (function () {
            const $messageList = $document.find('.js-message-list');
            const $lastMessage = $messageList.find('.js-chat-message:last');

            if (!$messageList.length) {
                return;
            }

            $messageList.animate({
                scrollTop: $lastMessage.offset().top
            });

            $messageList.magnificPopup({
                delegate: '.js-zoom',
                type: 'image',
                gallery:{
                    enabled: true
                },
                callbacks: {
                    elementParse: function($item) {
                        $item.src = $item.el.data('src');
                    }
                }
            });
        } ());


        //displays amount
        const toggleAmount = function () {
            const $this = $('.js-dispute-chat-form-action-selector');
            const $selectedOption = $this.find('option:selected');
            const $amount = $('.js-dispute-chat-form-amount-field');

            $amount.toggleClass('hidden', !($selectedOption.data('displayAmount') == 1));
        };

        $document.on('change', '.js-dispute-chat-form-action-selector', toggleAmount);
        toggleAmount();

        $();

    });
} (window.internalJQuery, document, window));