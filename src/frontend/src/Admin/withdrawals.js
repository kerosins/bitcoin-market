(function ($, document, window) {
    $(function () {
        const $document = $(document);

        $document.on('click', '.js-withdrawal-form-send', function () {
            const $form = $('.js-withdrawal-form');
            const $decisionInput = $form.find('.js-withdrawal-form-decision');
            const $this = $(this);

            $decisionInput.val($this.data('action'));
            $form.trigger('submit');
        });
    });
} (window.internalJQuery, document, window));