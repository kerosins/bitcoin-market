$(function () {
    let $body = $('body');

    $body.on('click', '#assembly_all', function (e) {
        const $this = $(this);
        const isChecked = $this.is(':checked');

        let checkboxes = $this.closest('#assembly-form').find('.category-tree-list .assembly-checkbox');

        checkboxes.each((i, element) => {
            $(element).prop('checked', isChecked);
            $(element).trigger('change');
        });
    });

    $body.on('dblclick', '.category-tree-list .assembly-checkbox', function (e) {
        const $this = $(this);
        const isChecked = !$this.is(':checked');

        $this.prop('checked', isChecked);
        let checkboxes = $this.closest('li').find('.category-tree-list .assembly-checkbox');

        checkboxes.each((i, element) => {
            $(element).prop('checked', isChecked);
            $(element).trigger('change');
        });

        $this.trigger('change');
    });

    let poolNames = [];

    $body.on('change', '.assembly-checkbox:not(#assembly_all)', function () {
        const $this = $(this);
        const isChecked = $this.is(':checked');
        const name = $this.closest('div').find('label').text();

        if (poolNames.indexOf(name) === -1) {
            if (isChecked) {
                poolNames.push(name);
            }
        } else {
            if (!isChecked) {
                delete poolNames[poolNames.indexOf(name)];
            }
        }

        renderAssembly();
    });

    $body.on('click', '#assembly', function () {
        if (!poolNames.length) {
            return;
        }
        clearProgress();
        $this = $(this);
        let $container = $('.assembly-execution');
        let categories = $('.assembly-checkbox:not(#assembly_all):checked');

        (new Promise(resolve => resolve(categories)))
            .each((category, index, length) => {
                return sendRequest($(category).val())
                    .then(() => updateProgress($(category), index + 1, length))
                    ;
            }, {concurrency: 1})
            .then(() => {
                $('#assembly_all').prop('checked', false);
                $('button.hide-progress').removeClass('hidden');
            });
        // $this.hide();
        $container.removeClass('hidden');
    });

    $body.on('click', 'button.hide-progress', clearProgress);

    function sendRequest(categoryId) {
        return new Promise(resolve => {
            $.ajax({
                url: `/cp/catalog/category/assembly/${categoryId}`,
                method: 'GET',
                dataType: 'json',
                success: () => {
                    resolve();
                }
            });
        })
    }

    $container = $('.selected-categories');
    function renderAssembly() {
        $container.empty();
        for (let name of poolNames) {
            if (name !== undefined) {
                $container.append(`<li>${name}</li>`);
            }
        }
    }

    function clearProgress() {
        let percent = 0;
        $('button.hide-progress').addClass('hidden');
        $('.progress-bar').css({'width': `${percent}%`});
        $('.assembly-execution .percent').text(percent);
        $('.assembly-execution').addClass('hidden');
    }

    function updateProgress(category, current, total) {
        return new Promise(resolve => {
            let percent = Math.floor(current * 100 / total);
            $('.progress-bar').css({'width': `${percent}%`});
            $('.assembly-execution .percent').text(percent);

            $(`#assembly_category_${category.val()}`).prop('checked', false).trigger('change');

            resolve();
        });

    }
});