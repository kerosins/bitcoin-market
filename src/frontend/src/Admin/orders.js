import 'Admin/styles/orders.less';

(function ($, document) {
    $(function () {//dom ready

        //show hide products of orders
        $('.js-toggle-products').on('click', function (e) {
            e.preventDefault();

            const $this = $(this);
            const $icon = $this.find('.glyphicon');
            const $container = $(`.js-order-products-container[data-order-id="${$this.data('id')}"]`);

            if ($container.hasClass('hide')) {
                $icon.removeClass('glyphicon-eye-open');
                $icon.addClass('glyphicon-eye-close');

                $container.removeClass('hide');
            } else {
                $icon.removeClass('glyphicon-eye-close');
                $icon.addClass('glyphicon-eye-open');

                $container.addClass('hide');
            }
        });

        //comments of order
        $('.js-order-comment').html(
            commentToHtml($('.js-order-comment').text())
        );

        $('.js-update-comment').on('click', function (e) {
            toggleCommentElements(false);
        });

        $('.js-comment-cancel-save').on('click', function (e) {
            toggleCommentElements(true);
        });

        $('.js-comment-save').on('click', function (e) {
            const $textarea = $('.js-order-comment-textarea textarea');
            const $commentContainer = $('.js-order-comment');

            if ($textarea.val() === $commentContainer.text()) {
                $(document).trigger('alertModal.btshop', {
                    message: 'Text is equal previous comment'
                });

                return;
            }

            $.post($(this).data('href'), { message: $textarea.val() })
                .then(function (response) {
                    $commentContainer.html(commentToHtml(response.comment));
                    toggleCommentElements(true);
                })
                .catch(function () {
                     $(document).trigger('aleerModal.btshop', {
                         message: 'Comment couldn\'t saved'
                     });
                });
        });

        //replace line break to <br>
        function commentToHtml(text) {
            return text.replace(/(?:\r\n|\r|\n)/g, '<br />');
        }

        //show|hide form or comment text
        function toggleCommentElements(displayComment = true)
        {
            if (displayComment) {
                $('.js-order-comment, .js-update-comment').removeClass('hide');
                $('.js-order-comment-textarea, .js-comment-save, .js-comment-cancel-save').addClass('hide');
            } else {
                $('.js-order-comment, .js-update-comment').addClass('hide');
                $('.js-order-comment-textarea, .js-comment-save, .js-comment-cancel-save').removeClass('hide');
            }
        }

        //statuses of order
        $(document).on('loaded.ajax', '.js-call-update-status', function (e, data) {
            const $modal = data.modal;
            const $link = $(this);

            $('.js-send-status', $modal).on('click', function () {
                const $this = $(this);
                console.log($this.prop('classList'));
                if ($this.hasClass('disabled')) {
                    return false;
                }

                const $form = $this.closest('form');
                $this.addClass('disabled');

                $.post($link.data('url'), $form.serialize())
                    .then(response => {
                        $('.js-statuses-history-container').html(response.block);
                        $modal.modal('hide');
                    })
                    .catch(function (err) {
                        let $newModalContent = $(err.responseJSON.form).find('.modal-body').html();
                        $modal.find('.modal-body').html($newModalContent);
                        $this.removeClass('disabled');
                    })
                ;
            });
        });

        //process product
        $(document).on('click', '.js-call-product-form', function () {
            const $this = $(this);
            if ($this.hasClass('disabled')) {
                return false;
            }
            $this.addClass('disabled');
            const $formContainer = $(`.js-product-form[data-item-id=${$this.data('item-id')}]`);

            $.get($this.data('url')).then(function (response) {
                $formContainer.html(response);
                $formContainer.removeClass('hide');
                $this.removeClass('disabled');
            });
        });

        $(document).on('click', '.js-cancel-update-product', function () {
            $(this).closest('.js-product-form').empty();
        });

        $(document).on('click', '.js-send-product-form', function () {
             const $this = $(this);
            if ($this.hasClass('disabled')) {
                return false;
            }
            $this.addClass('disabled');

            const $formContainer = $this.closest('.js-product-form');
            const $form = $this.closest('form');

            $.post($form.attr('action'), $form.serialize())
                .then(function (response) {
                    console.log(`.js-product-info[data-item-id=${$this.data('item-id')}]`);
                    $(`.js-product-info[data-item-id=${$this.data('item-id')}]`).html(response.block);
                    $formContainer.empty();
                    $formContainer.addClass('hide');
                })
                .catch(function (err) {
                    $formContainer.html(err.responseJSON.form);
                })
            ;
        });
    });
} (window.internalJQuery, document));