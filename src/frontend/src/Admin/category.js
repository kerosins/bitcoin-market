import 'Admin/styles/category.less';

(function ($) {
    $(function () {
        let busy = false;

        $('.send-mapping-category-aliases').on('click', function (e) {
            const $form = $(this).closest('form');
            const $this = $(this);

            if (busy) {
                return;
            }
            busy = true;

            $this.trigger('blur');

            $.ajax({
                url: $form.prop('action'),
                method: 'POST',
                data: $form.serialize(),
                dataType: 'json'
            }).then(function (response) {
                busy = false;

                let $message = `<div class="save-message ${ response.success ? 'success' : 'error' }">${response.message}</div>`
                $message = $($message);

                $form.prepend($message);

                setTimeout(() => {
                    $message.fadeOut({
                        complete() {
                            $message.remove();
                        }
                    })
                }, 5000);
            });


        });
    });
} (window.internalJQuery));
