import 'common/bootstrap';
import 'common/tabs';
import 'common/modals.js';
import './assembly_form';
import './category';
import './products';
import './item_block';
import './dispute';
import './system_settings';
import CommonFields from 'common/form_fields';

import 'Admin/styles/main.less';

(function ($, document) {
    $(function () {
        const $document = $(document);

        //toggle log extra data visibility
        $document.on('click', '.js-display-log-detail', function () {
            const $this = $(this);
            const $row = $this.closest('.js-log-item');
            const $details = $(`.js-log-item-detail[data-id=${$row.data('id')}]`);

            $details.toggleClass('hide', !$details.hasClass('hide'));

            return false;
        })

    });
    //common code here
} (window.internalJQuery, document));