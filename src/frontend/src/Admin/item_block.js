import Mutex from 'common/mutex';
import spinner from 'common/spinner';

(function ($, document, window) {
    'use strict';

    function initFields(type) {
        const $context = $('.js-parameters-container');
        switch (type) {
            case 'manual_products':
                initManualProducts($context);
                break;
            case 'automatic_products':
                initAutomaticProducts($context);
        }
    }

    /**
     * inits manual products type of block
     * @param $context
     */
    function initManualProducts($context) {
        const $hiddenInput = $context.find('.js-manual-product-field');
        const $searchInput = $context.find('.js-manual-product-search-field');
        const $searchItemsContainer = $context.find('.js-search-container');
        const $productItemsContainer = $context.find('.js-items-container');
        const template = $context.find('#js-manual-product-search-result-item-template').text();

        $productItemsContainer.sortable({
            items: '.js-sortable-item'
        });

        $productItemsContainer.on('sortstop', (event, ui) => {
            const $items = $productItemsContainer.find('.js-product-item');
            arrayToValue($items.map((i, item) => $(item).data('id')).toArray());
        });

        $searchInput.autocomplete({
            delay: 1000,
            minLength: 3,
            source(req, res) {
                const response = $.get({
                    url: $searchInput.data('url'),
                    data: { ...req },
                    dataType: 'json'
                });

                spinner.start($searchItemsContainer);
                $searchInput.attr('readonly', true);

                response.then(result => {
                    res();
                    $searchItemsContainer.empty();

                    $searchItemsContainer.toggleClass('items-container', result.products.length);
                    if (!result.products.length) {
                        $searchItemsContainer.text('Nothing found')
                    }

                    result.products.forEach((product, i) => {
                        const $itemBlock = template
                            .replace(/__id__/g, product.id)
                            .replace(/__title__/g, product.title)
                            .replace(/__image__/g, product.image)
                            .replace(/__category__/g, product.categories.map(item => item.title).join(', '))
                            .replace(/__inner_price__/g, product.price)
                            .replace(/__public_price__/g, product.publicPrice);

                        $searchItemsContainer.append($itemBlock);
                    });
                }).always(() => {
                    spinner.stop($searchItemsContainer);
                    $searchInput.attr('readonly', null);
                });
            }
        });

        function idsToArray() {
            return $hiddenInput.val().split(',')
                .map(v => parseInt(v.trim(), 10))
                .filter(v => !!v);
        }

        function arrayToValue(ids = []) {
            $hiddenInput.val(ids.join(','));
        }

        $context
            .off('click', '.js-add-product')
            .on('click', '.js-add-product', function () {
                const $item = $(this).closest('.js-product-item');
                const id = $item.data('id');
                let values = idsToArray();

                if (values.indexOf(id) !== -1) {
                    $item.remove();
                    return;
                }

                values.push(id);
                arrayToValue(values);

                $item.find('.js-add-product').remove();
                $item.find('.js-drop-product').removeClass('hidden');
                $item.appendTo($productItemsContainer);

                $item.addClass('js-sortable-item');

                if (!$productItemsContainer.hasClass('items-container')) {
                    $productItemsContainer.addClass('items-container');
                }
            })
            .off('click', '.js-drop-product')
            .on('click', '.js-drop-product', function () {
                console.log(1);

                const $item = $(this).closest('.js-product-item');
                const id = parseInt($item.data('id'), 10);
                let values = idsToArray();

                values = values.filter(value => value !== id);
                arrayToValue(values);
                if (!values.length) {
                    $productItemsContainer.removeClass('items-container');
                }

                $item.remove();
            })
    }

    /**
     * inits automatic products type of block
     * @param $context
     */
    function initAutomaticProducts($context) {
        const $categoryFilter = $context.find('.js-category-tree-filter');
        const $categoryList = $context.find('.js-category-tree-field');

        function displayParent($context, $option) {
            if (!$option.data('parentId')) {
                return;
            }

            const $parent = $context.find(`input[data-id=${$option.data('parentId')}]`);
            $parent.closest('.checkbox').show();
            displayParent($context, $parent);
        }

        /**
         * filter
         * @param term
         */
        function find(term) {
            term = term.toLowerCase().trim();
            let $inputs = $categoryList.find('input');
            $inputs.closest('.checkbox').show();

            if (!term.length) {
                return;
            }

            const $shownInputs = $inputs.filter(function (i, input) {
                const $input = $(input);
                let title = $input.data('title').toLowerCase().trim();

                return title.indexOf(term) !== -1;
            });

            $inputs.closest('.checkbox').hide();
            $shownInputs.closest('.checkbox').show();

            $shownInputs.each(function (i, input) {
                displayParent($categoryList, $(input));
            });
        }

        $categoryFilter.autocomplete({
            minLength: 0,
            delay: 300,
            source(req, res) {
                const term = req.term;
                find(term);
                res();
            }
        });

        $categoryList
            .off('change', 'input')
            .on('change', 'input', function () {
                const $this = $(this);
                const $children = $categoryList.find(`input[data-parent-id=${$this.data('id')}]`);
                if (!$children.length) {
                    return;
                }

                const isChecked = $this.prop('checked');

                $children.each((i, input) => {
                    const $input = $(input);
                    $input.prop('checked', isChecked);
                    $input.trigger('change');
                });
            });
    }

    $(function () {
        const $document = $(document);

        const typeMutex = new Mutex();
        typeMutex
            .onAcquire(() => {
                $('.js-type-item-block-field').attr('disabled', 'disabled');
            })
            .onRelease(() => {
                $('.js-type-item-block-field').attr('disabled', null);
            });

        initFields($('.js-type-item-block-field').val());

        $document.on('change', '.js-type-item-block-field', function () {
            const $this = $(this);
            const $formContainer = $('.js-item-block-container');

            if (!typeMutex.acquire()) {
                return;
            }

            if (!$this.val()) {
                $('.js-parameters-container').empty();
                typeMutex.release();
                return;
            }

            const response = $.get({
                url: $this.data('url'),
                data: {
                    [$this.data('param')]: $this.val()
                },
                dataType: 'json'
            });

            response.then((res) => {
                $formContainer.html(res.html);
                initFields($this.val());
                typeMutex.release();
            });
        });
    });
} (window.internalJQuery, document, window));