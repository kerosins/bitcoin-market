import modals from 'common/modals'

(function ($, document, window) {

    $(function () {
        let $modal;

        $('.js-setting-edit-modal').on('click', function () {
            const $this = $(this);

            $.get($this.attr('href'), {setting: $this.data('setting')})
                .then(
                    response => {
                        $modal = modals.initModal($(response.html));
                    },
                    err => {
                        modals.alert('Something went wrong, try later');
                    }
                );

            return false;
        });

        $(document).on('submit', '.js-setting-form', function () {
            if (!$modal) {
                return;
            }

            const $this = $(this);
            const data = {};

            $this.serializeArray().forEach(item => {
                data[item.name] = item.value;
            });

            $.post($this.attr('action'), data).then(
                () => {
                    $modal.modal('hide');
                    document.location.reload();
                },
                () => {
                    $modal.modal('hide');
                    modals.alert('Something went wrong, try later');
                }
            );

            return false;
        });
    });

})(window.internalJQuery, document, window);