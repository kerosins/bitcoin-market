(function ($) {
    window.dataForStateSelect = function(params) {
        const $stateField = $('select.delivery-info-state-field');
        const $countryField = $('select.delivery-info-country-field');

        return {
            type: $stateField.data('type'),
            page: params.page || 1,
            q: params.term,
            country: $countryField.val()
        };
    };

    window.dataForCitySelect = function(params) {
        const $stateField = $('select.delivery-info-state-field');
        const $cityField = $('select.delivery-info-city-field');
        const $countryField = $('select.delivery-info-country-field');

        return {
            type: $cityField.data('type'),
            page: params.page || 1,
            q: params.term,
            parent: $stateField.val() | '',
            country: $countryField.val()
        };
    };

    window.dataForZipSelect = function(params) {
        const $cityField = $('select.delivery-info-city-field');
        const $countryField = $('select.delivery-info-country-field');

        return {
            page: params.page || 1,
            q: params.term,
            country: $countryField.val(),
            city: $cityField.val() || ''
        };
    };

    $(function () {
        const $stateField = $('select.delivery-info-state-field');
        const $cityField = $('select.delivery-info-city-field');
        const $zipField = $('select.delivery-info-zip-field');
        const $countryField = $('.delivery-info-country-field');
        const $deliveryForm = $('.js-delivery-form');
        const $document = $(document);

        toggleAutocomplete($('option:selected', $countryField));

        $countryField.on('change', function (e) {
            toggleAutocomplete($('option:selected', $countryField));
        });

        //preset user delivery
        $('.js-user-delivery-info input[type="radio"]').on('click', function (e) {
            onSelectUserInfo($(this));
        });

        function onSelectUserInfo($input) {
            const info = $input.data('info');

            if (info === undefined) {
                //enable fields
                $deliveryForm.find('input[type="text"], select').each((i, element) => {
                    $(element).val(null);
                    $(element).attr('readonly', null);
                });
            } else {
                //set values and disable fields
                Object.keys(info).map(key => {
                    const $input = $(`[name$="[${key}]"]`);
                    const value = info[key];

                    if ($input.length) {
                        switch ($input.prop('tagName')) {
                            case 'INPUT':
                                $input.val(value);
                                break;
                            case 'SELECT':
                                $input.val(value.id);
                                $input.trigger('change');
                        }
                        $input.attr('readonly', true);
                    }

                    //manually set state|city|zip
                    let addressKeys = ['state', 'city', 'zip'];
                    if (addressKeys.indexOf(key) !== -1) {
                        if (value.id === null || value.id === undefined) {
                            const $simple = $(`[name$="[${key}Simple]"]`);
                            $simple.val(value.title || value);
                            $simple.attr('readonly', true);
                        } else {
                            const $autocomplete = $(`[name*="[${key}Autocomplete]"]`);
                            $autocomplete.append(
                                new Option(value.title, value.id, true, true)
                            );
                            $autocomplete.trigger('change');
                            $autocomplete.attr('readonly', true);
                        }
                    }
                });
            }
        }

        //show|hide fields
        function toggleAutocomplete($selectedValue) {
            let autocomplete = !!$selectedValue.data('autocomplete') || 0;
            let autocompleteFields = $('[data-field-type="autocomplete"]').closest('.form-group');
            let simpleFields = $('[data-field-type="simple"]').closest('.form-group');

            if (autocomplete) {
                autocompleteFields.show();
                simpleFields.hide();
            } else {
                autocompleteFields.hide();
                simpleFields.show();
            }
        }

        //init autocomplete event handlers
        (function () {
            if (!$stateField.length || !$cityField.length) {
                return null;
            }

            $stateField
                .on('select2:select select2:unselect', (e) => {
                    $cityField.val(null);
                    $cityField.trigger('change');

                    $zipField.val(null);
                    $zipField.trigger('change');
                })
            ;

            $cityField.on('select2:select select2:unselect', () => {
                $zipField.val('null');
                $cityField.trigger('change');
            });

            $zipField.on('select2:select', (e) => {
                const data = e.params.data;

                $stateField
                    .append(new Option(data.state_title, data.state_id, true, true))
                    .trigger('change');

                $cityField
                    .append(new Option(data.city_title, data.city_id, true, true))
                    .trigger('change');
            })
        })();

        $document.on('change', '.js-checkout-form-country-field', function () {
            $document.trigger('checkout-change-country');
        });

        $document.on('click', '.js-checkout-form-place-order-btn', function () {
            $('.js-delivery-form').trigger('submit');
        });

        $document.on('submit', '.js-delivery-form', function () {
            const $form = $(this);
            const $chooseCurrencyForm = $('.js-select-currency-of-payment');

            if ($chooseCurrencyForm.length) {
                if ($chooseCurrencyForm.hasClass('has-error')) {
                    return false;
                }

                const $input = (name, value) => {
                    return $(`<input type="hidden" name="${name}" value="${value}">`);
                };

                $chooseCurrencyForm.find('select, input').each(
                    function (i, element) {
                        const $element = $(element);
                        $form.append($input(
                            $element.attr('name'),
                            $element.val()
                        ));
                    }
                );
            }
        });
    });
} (window.internalJQuery));