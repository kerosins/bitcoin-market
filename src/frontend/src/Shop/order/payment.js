import Mutex from 'common/mutex';
import Spinner from 'common/spinner';

(function ($, document, window) {

    $(function () {
        updateTotal();

        const $document = $(document);
        $document.on('change', '.js-payment-currency', function () {
            updateTotal();
        });

        let timer;
        $document.on('keyup', '.js-payment-bonus', function () {
            const $input = $(this);
            let value = parseInt($input.val(), 10);
            if (isNaN(value) || value === 0) {
                $input.val(0);
                updateTotal();
                return;
            }

            $input.val(value);

            if (timer) {
                clearTimeout(timer);
            }
            timer = setTimeout(() => {
                updateTotal();
            }, 700);
        });

        $document.on('checkout-change-country', function () {
            updateTotal();
        });

        const $paymentContainer = $('.js-payment-container');
        if ($paymentContainer.length) {
            setTimeout(
                () => {
                    window.location.reload(true);
                },
                ($paymentContainer.data('paymentTimeLeft') + 2) * 1000
            );
        }
    });

    let previewMutex = new Mutex();
    let currentCountry = null;
    let currentCurrency = null;
    const disabledElements = '.js-payment-currency, .js-payment-bonus, .js-checkout-form-place-order-btn, .js-checkout-form-country-field';
    previewMutex.onAcquire(function () {
        $(disabledElements).attr('disabled', true);

    });
    previewMutex.onRelease(function () {
        $(disabledElements).attr('disabled', null);
    });

    function updateTotal() {
        if (!previewMutex.acquire()) {
            return;
        }

        const $form = $('.js-select-currency-of-payment');
        const $currencyField = $form.find('.js-payment-currency');
        const $bonusField = $form.find('.js-payment-bonus');
        const $countryField = $('.js-checkout-form-country-field');

        const country = $countryField.length ? $countryField.val() : null;
        //break recalculate if country did not change
        if ($countryField) {
            if (country === currentCountry && currentCurrency === $currencyField.val()) {
                previewMutex.release();
                return;
            } else {
                currentCurrency = $currencyField.val();
                currentCountry = country;
            }
        }

        const $amountContainers = new Map();
        $amountContainers.set('total', $('.js-total-amount-value'));
        $amountContainers.set('sub_total', $('.js-sub-total-amount-value'));
        $amountContainers.set('delivery_tax', $('.js-delivery-tax-amount-value'));

        if (!$form && !$currencyField.length) {
            return;
        }

        $form.removeClass('has-error');
        $form.find('.js-error-container, .invalid-feedback').remove();

        for (let $container of $amountContainers.values()) {
            $container.removeClass('error');
            $container.empty();
            Spinner.start($container, {
                scale: 0.35,
                top: '12px',
                left: '-30px',
                position: 'relative'
            });
        }

        $.post(
            $form.data('update-total-uri'),
            {
                '_token': $form.find('#_token').val(),
                'currency': $currencyField.val(),
                'znt': $bonusField.length ? ($bonusField.val() || 0) : 0,
                country
            }
        ).then(
            function (response) {
                const currency = $currencyField.val().toUpperCase();
                for (let [key, $container] of $amountContainers.entries()) {
                    $container.text(`${response[key]} ${currency}`)
                }

                previewMutex.release();
            },
            function (err) {
                const response = err.responseJSON;

                $form.addClass('has-error');

                if (response.errors !== undefined) {
                    Object.keys(response.errors).forEach(key => {
                        const errors = response.errors[key];
                        const $fieldCnt = $form.find(`[name="${key}"]`).closest('.form-group');

                        if (!$fieldCnt.length) {
                            return;
                        }

                        let $errorCnt = errors.map(error => `<li class="error">${error}</li>`).join('');
                        $errorCnt = `<ul class="js-error-container error-container">${$errorCnt}</ul>`;

                        $fieldCnt.append($errorCnt);
                    });
                }

                for (let $container of $amountContainers.values()) {
                    $container.addClass('error');
                    $container.text('Sorry, we have some error');
                }

                previewMutex.release();
            }
        );
    }

} (window.internalJQuery, document, window));