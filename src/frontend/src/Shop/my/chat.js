import plupload from 'plupload/js/plupload.dev';
import 'Shop/style/chat.less';
import Spinner from 'common/spinner';
import Mutex from 'common/mutex';
import Modal from 'common/modals'


;(function ($) {
    class Chat {
        selectors = {
            //common
            form: '.js-chat-message-form',
            sendBtn: '.js-chat-message-send-btn',
            messageField: '.js-chat-message-message-field',
            messagesList: '.js-chat-messages-list-cnt',
            //attachments
            attachmentsListCnt: '.js-chat-message-attachments-list',
            attachmentsInputField: '.js-chat-attachments-input-field',
            attachmentPreviewItemCnt: '.js-chat-attachments-preview-item-cnt',
            attachmentsAddBtn: '.js-chat-message-attachments-add-btn',
            attachmentDropBtn: '.js-chat-message-attachment-drop-btn',
            attachmentZoomBtn: '.js-chat-message-attachment-zoom-btn',
            //templates
            previewItemTemplate: '#js-chat-message-preview-item-template'
        };
        $context = null;
        $elements = {};

        options = {
            maxImages: 5,
            enableAttachments: true
        };

        uploader = null;
        token = null;
        mutex = new Mutex();

        constructor($context, options = {}) {
            this.$context = $context;
            this.options = $.extend({}, true, this.options, options);

            this.cacheElements();
            this.initListeners();

            this.mutex
                .onAcquire(() => {
                    Spinner.start(this.$elements.$sendBtn, {
                        scale: 0.6,
                        position: 'relative',
                        top: '10px'
                    });
                    this.$elements.$sendBtn.addClass('disabled');
                    if (this.$elements.$attachmentsAddBtn) {
                        this.$elements.$attachmentsAddBtn.addClass('disabled');
                    }
                    this.$elements.$messageField.attr('readonly', true);
                })
                .onRelease(() => {
                    Spinner.stop(this.$elements.$sendBtn);
                    this.$elements.$sendBtn.removeClass('disabled');
                    if (this.$elements.$attachmentsAddBtn) {
                        this.$elements.$attachmentsAddBtn.removeClass('disabled');
                    }
                    this.$elements.$messageField.attr('readonly', null);
                });
        }

        cacheElements() {
            const selectors = this.selectors;
            const $ctx = this.$context;

            this.$elements = {
                $form: $ctx.find(selectors.form),
                $messageField: $ctx.find(selectors.messageField),
                $sendBtn: $ctx.find(selectors.sendBtn),
                $messagesList: $ctx.find(selectors.messagesList)
            };

            this.initZoom(this.$elements.$messagesList);

            if (this.options.enableAttachments) {
                this.$elements.$attachmentsListCnt = $ctx.find(selectors.attachmentsListCnt);
                this.$elements.$attachmentsInputField = $ctx.find(selectors.attachmentsInputField);
                this.$elements.$attachmentsAddBtn = $ctx.find(selectors.attachmentsAddBtn);
                this.$elements.$previewItemTemplate = $ctx.find(selectors.previewItemTemplate);
            }
        }

        initListeners() {
            if (this.options.enableAttachments) {
                this.initFileUpload();
            }

            this.$elements.$form.on('submit', this.onSubmitForm.bind(this));
        }

        /**
         * Init upload of attachments
         */
        initFileUpload() {
            this.uploader = new plupload.Uploader({
                browse_button: this.$elements.$attachmentsAddBtn.get(0),
                url: this.$elements.$attachmentsAddBtn.data('url'),
                runtimes: 'html5,html4',
                filters: {
                    max_file_size: '2mb',
                    mime_types: [
                        { title: "Image files", extensions: "jpg,jpeg,gif,png" },
                    ]
                },
                init: {
                    FilesAdded: this.onAddAttachment.bind(this),
                    UploadComplete: this.onUploadComplete.bind(this)
                },
                multipart_params: {
                    'xhr_file[id]': '',
                    'xhr_file[type]': 'dispute'
                },
                file_data_name: 'xhr_file[file]'
            });
            this.uploader.init();

            this.$context.on(
                'click',
                this.selectors.attachmentDropBtn,
                this.onDropAttachment.bind(this)
            )
        }

        /**
         * Listens 'FilesAdded' event
         *
         * @param up
         * @param files
         */
        onAddAttachment(up, files) {
            const onload = (e, pluploadFile) => {
                let template = this.$elements.$previewItemTemplate.text();

                const $item = $(
                    template
                        .replace(/__src__/g, e.target.result)
                        .replace(/__id__/g, pluploadFile.id)
                );

                $item.magnificPopup({
                    delegate: this.selectors.attachmentZoomBtn,
                    type: 'image',
                    gallery:{
                        enabled: true
                    },
                    callbacks: {
                        elementParse: function($item) {
                            $item.src = e.target.result;
                        }
                    }
                });

                $item.appendTo(this.$elements.$attachmentsListCnt);
            };

            for (let file of files) {
                const reader = new FileReader();
                reader.onload = e => onload(e, file);
                reader.readAsDataURL(file.getNative());
            }
        }

        /**
         * Listens click on drop button
         *
         * @param e
         */
        onDropAttachment(e) {
            const $target = $(e.target);
            const $previewContainer = $target.closest(this.selectors.attachmentPreviewItemCnt);

            this.uploader.removeFile($previewContainer.data('fileId'));
            $previewContainer.remove();
        }

        onUploadComplete(up, files) {
            up.refresh();
            this.onSubmitWithoutAttachments();
        }

        /**
         * Listens submit form event
         *
         * @param e
         * @returns {boolean}
         */
        onSubmitForm(e) {
            if (!this.mutex.acquire()) {
                return false;
            }

            // ajaxLoader.start(this.$elements.$form);
            if (!this.options.enableAttachments || !this.uploader.files.length) {
                return this.onSubmitWithoutAttachments(e);
            }

            $.get(this.$elements.$attachmentsAddBtn.data('tokenUrl')).then((res) => {
                this.token = res.token;
                // console.log(this.uploader, this.uploader._settings);
                this.uploader.settings.multipart_params['xhr_file[id]'] = this.token;
                this.uploader.start();
            });

            return false;
        }

        /**
         * Submits form without attachments
         *
         * @param e
         * @returns {boolean}
         */
        onSubmitWithoutAttachments(e) {

            let data = this.$elements.$form.serializeArray();
            if (this.token) {
                data.push({
                    name: 'stack_id',
                    value: this.token
                });
            }

            const res = $.post(
                this.$elements.$form.attr('action'),
                data
            );

            res
                .then((response) => {
                // ajaxLoader.stop(this.$elements.$form);
                    this.token = null;
                    this.$elements.$attachmentsListCnt.empty();
                    this.$elements.$messageField.val('');

                    const $newContainer = $(response.messages);
                    this.$elements.$messagesList.replaceWith($newContainer);
                    this.$elements.$messagesList = $newContainer;
                    this.initZoom(this.$elements.$messagesList);
                })
                .catch(() => {
                    Modal.alert('Something went wrong');
                    this.$elements.$messageField.empty();
                })
                .always(() => {
                    this.mutex.release();
                })
            ;

            return false;
        }

        initZoom($ctx) {
            $ctx.magnificPopup({
                delegate: this.selectors.attachmentZoomBtn,
                type: 'image',
                gallery:{
                    enabled: true
                },
                callbacks: {
                    elementParse: function($item) {
                        $item.src = $item.el.data('image');
                    }
                }
            });
        }
    }

    $(function () {
        const $chatContainer = $('.js-chat-container');

        const chat = new Chat($chatContainer, {
            enableAttachments: $chatContainer.data('allowAttachments')
        });
    });
} (window.internalJQuery));