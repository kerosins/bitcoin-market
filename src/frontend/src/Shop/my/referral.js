import Modal from 'common/modals';
import Mutex from 'common/mutex';

(function ($, document, window) {

    //calculate amount of coins
    function calculateAmount() {
        const $amountField = $('.js-withdrawal-amount');
        const $currencySelector = $('.js-withdrawal-currency');

        if (!$amountField.length || !$currencySelector.length) {
            if (IS_DEV) {
                console.error('One or more of required elements not found');
            }

            return;
        }

        let amountBonuses = parseFloat($amountField.val().replace(',', '.')).toFixed(2);
        if (isNaN(amountBonuses)) {
            if (IS_DEV) {
                console.error('Unknown count of bonuses');
            }
            amountBonuses = 0;
        }

        console.log(amountBonuses);

        let $selectedCurrencyOption = $currencySelector.find('option:selected');
        if (!$selectedCurrencyOption.length) {
            $selectedCurrencyOption = $currencySelector.find('option:first');
        }

        let rate = $selectedCurrencyOption.data('rate');
        rate = parseFloat(rate);

        if (isNaN(rate)) {
            rate = 1;
        }

        let result = amountBonuses * rate;

        $('.js-withdrawal-result').val(`${result.toFixed(5)} ${$selectedCurrencyOption.val()}`);
    }

    $(function () {
        const $document = $(document);
        const $errorContainer = $('.js-generator-error');
        const $sendBtn = $('.js-referral-links-btn');
        const $resultContainer = $('.js-links-generator-result');
        const $resultTextarea = $('.js-links-generator-result-textarea');
        const $linksTextarea = $('.js-referral-links-textarea');

        const showError = (text) => {
            $errorContainer.text(text);
            $errorContainer.removeClass('hide');
        };
        const hideError = () => {
            $errorContainer.empty();
            $errorContainer.addClass('hide');
        };

        let isSent = false;
        $document.on('click', '.js-referral-links-btn', function () {
            hideError();
            const $this = $(this);

            if (isSent) {
                return;
            }

            const links = $linksTextarea.val();
            if (!links.length) {
                showError('Links is empty');
                return;
            }

            isSent = true;
            $sendBtn.text($sendBtn.data('process-text'));

            $.post($this.data('href'), { links })
                .then(response => {
                    isSent = false;
                    $sendBtn.text($sendBtn.data('normal-text'));
                    $resultContainer.removeClass('hide');
                    $resultTextarea.val(response.links.join('\r\n'));
                })
                .catch(err => {
                    isSent = false;
                    $sendBtn.text($sendBtn.data('normal-text'));
                    if (err.status === 500) {
                        showError('Server Error');
                    } else {
                        showError(err.responseJSON.message);
                    }
                });
        });

        $document.on('click', '.js-referral-links-clear', function () {
            $resultContainer.addClass('hide');
            $resultTextarea.val('');
            $linksTextarea.val('');
            isSent = false;
        });

        $document.on('click', '.js-links-to-clipboard', function () {
            $resultTextarea.select();
            document.execCommand('copy');
            window.getSelection().removeAllRanges();
        });

        const requestModalMutex = new Mutex();

        //withdraw request
        $document.on('click', '.js-withdraw-request', function () {
            if (!requestModalMutex.acquire()) {
                return;
            }

            const response = $.ajax({
                url: $(this).data('url'),
                method: 'GET',
                dataType: 'json'
            });

            response.then(response => {
                Modal.initModal($(response.modal));
                requestModalMutex.release();
            });

            response.catch((err) => {
                if (IS_DEV) {
                    console.error(err);
                }

                requestModalMutex.release();
            });

            return false;
        });

        const submitWithdrawalMutex = new Mutex();
        //send withdraw request
        $document.on('click', '.js-send-withdrawal-request', function () {
            if (!submitWithdrawalMutex.acquire()) {
                return false;
            }

            const $form = $('.js-withdrawal-form');
            const $this = $(this);

            const response = $.ajax({
                url: $form.attr('action'),
                method: $form.attr('method'),
                data: $form.serialize(),
                dataType: 'json'
            });
            response.then(response => {
                const $modal = $this.closest('.js-modal');
                if (response.success !== undefined && response.success === true) {
                    document.location.href = response.redirectTo;
                    document.location.reload(true);
                } else if (response.modal !== undefined) {
                    const $responseModal = $(response.modal).find('.js-withdrawal-form');
                    $form.replaceWith($responseModal);
                    calculateAmount()
                    // console.log($form, $responseModal);
                }

                submitWithdrawalMutex.release();
            });

            response.catch((err) => {
                if (IS_DEV) {
                    console.error(err);
                }

                submitWithdrawalMutex.release();
            });
        });

        $document.on('change', '.js-withdrawal-currency', calculateAmount);
        $document.on('keyup', '.js-withdrawal-amount', calculateAmount);
    });

} (window.internalJQuery, document, window));