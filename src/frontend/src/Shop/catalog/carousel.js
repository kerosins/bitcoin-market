//initializes carousels in catalog
export default (function ($, document, window) {
    return {
        init() {
            const $carousels = $('.js-owl-carousel');

            $carousels.each((i, carousel) => {
                const $carousel = $(carousel);
                if ($carousel.data('carousel-init')) {
                    return true;
                }

                $carousel.owlCarousel({
                    items: $carousel.data('size') || 4,
                    autoHeight: true,
                    nav: $carousel.data('nav') || false,
                    dots: $carousel.data('dots') || false,
                    margin: $carousel.data('margin') || 0,
                    navText: $carousel.data('navText') || [],
                    loop: true,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 3
                        },
                        900: {
                            items: 4
                        }
                    }
                });
            });
        }
    };
} (window.internalJQuery, document, window));