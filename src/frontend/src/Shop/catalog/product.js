(function ($, document, window) {
    $(function () {
        const $gallery = $('.js-product-images-carousel');
        const $productZoom = $('.js-product-zoom');

        /*
        $productZoom.elevateZoom({
            responsive: true,
            cursor: "default"
        });
        */

        $gallery
            .owlCarousel({
                margin: 8,
                items: 4,
                nav: true,
                center: false,
                dots: false,
                autoplay: false
            })
            .on('click', '.owl-item', function(e) {
                //const ez = $productZoom.data('elevateZoom');
                const $targetLink = $(this).find('a');
                const smallImg = $targetLink.data('image');
                const bigImg = $targetLink.data('zoomImage');

                // Sync data-zoom-image for gallery
                $productZoom.data('zoomImage', bigImg);
                // prevent default
                $.magnificPopup.close();

                //ez.swaptheimage(smallImg, bigImg);
                $productZoom.data('zoomImage', bigImg);
                $productZoom.attr('src', bigImg);
            });

        // Set up gallery on click
        $gallery.magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery:{
                enabled: true
            },
            callbacks: {
                elementParse: function(item) {
                    item.src = item.el.data('zoomImage');
                },
                buildControls: function() {
                    this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                }
            }
        });

        // Zoom image when click on icon
        $('.js-product-img-zoom').on('click', function(e) {
            const current = $productZoom.data('zoomImage');

            $gallery.find('.owl-item').each(function() {
                const $this = $(this);
                if( current == $this.find('.js-product-gallery-item').data('zoomImage') ) {
                    return $gallery.magnificPopup('open', $this.index());
                }
            });

            e.preventDefault();
        });

        // No def events for links
        $gallery.find('a').on('click', function (e) {
            e.preventDefault();
        });
    });
} (window.internalJQuery, document, window));