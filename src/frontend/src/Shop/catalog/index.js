import 'Shop/catalog/cart';
import 'Shop/catalog/wish';
import 'Shop/catalog/product';
import carousel from 'Shop/catalog/carousel';

(function ($, document, window) {

    function initSearchSuggestions($searchInput) {
        let qResponse;

        $searchInput.autocomplete({
            classes: {
                'ui-autocomplete': 'main-search-suggestions-container'
            },
            source: (req, res) => {
                if (qResponse && qResponse.readyState === 1) {
                    qResponse.abort();
                }

                qResponse = $.ajax({
                    'url': $searchInput.data('suggestionsUrl'),
                    'data': { ...req },
                    'dataType': 'json'
                });

                qResponse
                    .then(xhr => {
                        res(
                            xhr.suggestions.map(item => ({
                                value: item.title,
                                ...item
                            }) )
                        )
                    })
                    .catch(err => {
                        if (IS_DEV) {
                            console.error(err);
                        }
                        res([]);
                    })
                ;
            },
            select: (e, ui) => {
                document.location.href = ui.item.url;
            },
            minLength: 3,
            delay: 500
        });

        //customize render items
        const autocomplete =  $searchInput.data("ui-autocomplete") || $searchInput.data("autocomplete");
        autocomplete._renderItem = function(ul, item) {
            return $("<li></li>", {
                'class': item.type
            })
                .data("item.autocomplete", item)
                .append(item.type !== 'all-results' ? item.highlights : 'All Results')
                .appendTo(ul);
        };

        //focus on search input
        $searchInput.on('focus', function () {
            if ($searchInput.val().length && $('.ui-autocomplete .ui-menu-item').length) {
                $('.ui-autocomplete').show();
            }
        });
    }

    $(function () {
        const $searchInput = $('.js-catalog-search-input');
        const $document = $(document);

        initSearchSuggestions($searchInput);

        //search input
        $document.on('submit', '.js-search-form', function () {
            const $this = $(this);
            const $searchField = $this.find('.js-catalog-search-input');

            if (!$searchField.val()) {
                return false;
            }

            window.location.href = $this.attr('action') + '/' + encodeURIComponent($searchField.val());

            return false;
        });

        //sorting link
        $document.on('change', '.js-products-sorting-link-selector', function () {
            const $this = $(this);
            if (!$this.val().length) {
                return;
            }

            document.location.href = $this.val();
        });

        carousel.init();
    });

} (window.internalJQuery, document, window));