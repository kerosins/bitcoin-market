import Mutex from 'common/mutex';
// import modal from 'common/modals';
import Spinner from 'common/spinner';

(function ($, window, document) {
    const $document = $(document);
    const $previewContainer = $('.js-preview-container');

    /**
     * request to actions with cart
     */
    function cartRequest(url, id, quantity = 0) {
        let data = { id };
        if (quantity > 0) {
            data.quantity = quantity;
        }

        return $.ajax({
            url,
            method: 'POST',
            data
        });
    }

    /**
     * Update cart icon
     * @param data
     */
    function updateCart(data) {
        $('.js-cart-qty').text(data.count);
        $previewContainer.html(data.preview);

        let event = new CustomEvent('update_checkout');
        document.dispatchEvent(event);
    }

    const cartWidgetMutex = new Mutex();
    //listeners
    //btn
    $document.on('click', '.js-cart-add, .js-cart-delete', function (e) {
        const $this = $(this);

        if (!cartWidgetMutex.acquire()) {
            return false;
        }

        const $productCard = $this.closest('.js-product-card');
        Spinner.start($productCard);
        $productCard.addClass('locked');

        $this.text('Processing...');

        cartRequest($this.attr('href'), $this.data('id'))
            .then(data => {
                updateCart(data);
                $this.replaceWith(data.button);
            })
            .catch(args => {
                if (args.status === 410) {
                    window.location.href = args.responseJSON.url;
                }
            })
            .always(() => {
                cartWidgetMutex.release();
                $productCard.removeClass('locked');
                Spinner.stop($productCard);
            });

        return false;
    });

    //icon in cart
    $document.on('click', '.js-cart-delete-icon', function () {
        const $this = $(this);

        if (cartWidgetMutex.acquire()) {
            return false;
        }

        //try find button
        const $dropBtn = $(`.js-cart-delete[data-id="${$this.data('id')}"]`);
        if ($dropBtn.length) {
            $dropBtn.text('Processing...');
        }

        cartRequest($this.attr('href'), $this.data('id'))
            .then(data => {
                updateCart(data);
                if ($dropBtn.length) {
                    $dropBtn.replaceWith(data.button);
                }

                cartWidgetMutex.release();
            })
            .catch(args => {
                console.error(args);
                cartWidgetMutex.release();
            });

        return false;
    });

    //cart page
    function recalculateCart (product, quantity) {
        const $itemsCnt = $('.js-cart-page-items-container');
        const $totalPriceCnt = $('.js-cart-page-total-price-container');

        const request = $.ajax({
            url: $itemsCnt.data('update-uri'),
            data: { product, quantity },
            method: 'POST',
            dataType: 'json'
        });

        return new Promise((resolve, reject) => {
            request.then(
                function (response) {
                    $itemsCnt.replaceWith(response.cart);
                    $totalPriceCnt.replaceWith(response.totalPrice);
                    resolve(response);
                },
                function (err) {
                    console.log(err);
                    reject();
                }
            );
        });
    }

    $document.on('click', '.js-cart-item-quantity-change', function (e) {

        const $this = $(this);
        const $actionCnt = $this.closest('.js-cart-item-quantity-actions-container');

        const action = $this.data('action');
        let quantity = parseInt($actionCnt.find('.js-cart-item-quantity-input').val(), 10);
        if (action === 'dec') {
            quantity--;
        } else {
            quantity++;
        }

        recalculateCart($actionCnt.data('id'), quantity);
    });

    $document.on('click', '.js-cart-item-remove', function () {
        recalculateCart($(this).data('id'), 0).finally(() => cartPageMutex.release());
    });

    $document.on('click', '.js-cart-items-clear', function () {
        const $items = $('.js-cart-item-quantity-actions-container').toArray();
        Promise
            .map($items, item => {
                return recalculateCart($(item).data('id'), 0);
            })
            .map(response => {
                const $itemsCnt = $('.js-cart-page-items-container');
                const $totalPriceCnt = $('.js-cart-page-total-price-container');

                $itemsCnt.replaceWith(response.cart);
                $totalPriceCnt.replaceWith(response.totalPrice);
            });
    });

    //manual update
    $document.on('keypress', '.js-cart-item-quantity-input', function (e) {
        const $this = $(this);
        const $actionCnt = $this.closest('.js-cart-item-quantity-actions-container');
        const $input = $actionCnt.find('.js-cart-item-quantity-input');

        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39) ||
            //Allow numbers and numbers + shift key
            ((e.shiftKey && (e.keyCode >= 48 && e.keyCode <= 57)) || (e.keyCode >= 96 && e.keyCode <= 105))) {
            // let it happen, don't do anything
            return;
        } else {
            if (!/[0-9]/.test(e.key)) {
                return false;
            }
        }
    });

    $document.on('input', '.js-cart-item-quantity-input', function (e) {
        const $this = $(this);
        const $actionCnt = $this.closest('.js-cart-item-quantity-actions-container');
        const $input = $this;

        let quantity = parseInt($this.val(), 10);

        if (isNaN(quantity)) {
            quantity = 0;
            $this.val(0);
        }

        if ( quantity != 0 ) {
            recalculateCart(
                $actionCnt.data('id'),
                quantity || 0
            );
        }
    });

} (internalJQuery, window, document));