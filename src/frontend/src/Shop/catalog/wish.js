import BitUser from 'common/user';
import Mutex from 'common/mutex'

(function ($, document) {
    const wishMutex = new Mutex();
    const WishList = {
        onUpdateWishList($button) {
            return new Promise((resolve, reject) => {
                if (!wishMutex.acquire()) {
                    reject(new Error());
                    return;
                }

                if (!BitUser.isAuth()) {
                    reject(new Error());
                    return;
                }

                $button.find('i').removeClass('fa-heart fa-trash').addClass('fa-clock-o');


                const response = $.ajax({
                    url: $button.attr('href'),
                    dataType: 'json'
                });

                response
                    .then(function (response) {
                        wishMutex.release();
                        resolve(response);

                    })
                    .catch(function (err) {
                        if (IS_DEV) {
                            console.error(err);
                        }
                    });
            });
        }
    };

    $(function () { //dom ready

        const $document = $(document);
        $document
            .on('click', '.js-wish-product', function (e) {//btn in catalog
                e.preventDefault();
                const $button = $(this);

                WishList
                    .onUpdateWishList($button)
                    .then(function (response) {
                        $button.replaceWith(response.button)
                    })
                    .catch(() => {});
            })
            .on('click', '.js-wish-page_drop-wish', function (e) {//btn in Wishlist Page
                e.preventDefault();
                const $button = $(this);
                const $wishListCnt = $button.closest('.js-wish-list-container');

                WishList
                    .onUpdateWishList($button)
                    .then(function () {
                        return $.ajax({
                            url: $wishListCnt.data('url')
                        });
                    })
                    .then(function (response) {
                        $wishListCnt.html(response.list);
                    })
                    .catch(() => {});
            })
        ;


    });
} (window.internalJQuery, document));