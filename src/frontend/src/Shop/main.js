import './style/main.less';
import modals from 'common/modals.js';
import Mutex from 'common/mutex';
import 'common/tabs';
import 'common/form_fields';
import BitUser from 'common/user';

(function ($, window, document) {
    $(function () {
        //confirm a delete
        const $document = $(document);

        let $feedbackModal;
        function sendFeedback(url, data = '')
        {
            const response = $.post({
                url,
                data,
                dataType: 'json'
            });

            response.then(res => modals.initModal($(res.modal)));
            response.catch(err => window.console.error(err));
        }

        //redirect to login
        $document.on('click', '.js-login-require', function () {
            if (!BitUser.isAuth()) {
                window.location.href = BitUser.getLoginUrl();
                return false;
            }
        });

        //collapsible blocks
        $document.on('click', '.js-collapsible .panel-box-title', function () {
            $(this).parent().toggleClass('collapsed');
        });

        //copy wallet address to clipboard
        $document.on('click', '.js-payment-receiver-address-copy-clipboard', function() {
            const $temp = $('<input>');
            $('body').append($temp);
            $temp.val( $('.js-payment-receiver-address-value').val() ).select();
            document.execCommand("copy");
            $temp.remove();
        });

        //copy payment amount to clipboard
        $document.on('click', '.js-payment-amount-copy-clipboard', function() {
            const $temp = $('<input>');
            $('body').append($temp);
            $temp.val( $('.js-payment-amount-value').val() ).select();
            document.execCommand("copy");
            $temp.remove();
        });

        //select a country
        $('.js-select-country ').on('click', function (e) {
            e.preventDefault();

            $.ajax({
                method: 'GET',
                url: $(this).prop('href')
            }).then(() => {
                $('.js-selected-country').text($(this).text())
            });
        });

        //sends feedback
        $document.on('click', '.js-feedback-form-display-btn', function () {
            const $this = $(this);
            const url = $this.attr('href') || $this.data('url');
            if (!url) {
                return false;
            }

            const response = $.post({
                url,
                dataType: 'json'
            });

            response.then(res => modals.initModal($(res.modal)));
            response.catch(err => window.console.error(err));
            return false;
        });

        $document.on('submit', '.js-feedback-form', function () {
            const $this = $(this);
            const $container = $this.find('.js-feedback-form-fields-container');
            const $modal = $this.closest('.js-modal');

            const response = $.post({
                url: $this.attr('action'),
                data: $this.serialize(),
                dataType: 'json'
            });

            $container.html('Processing...');
            response.then(res => {
                const $newModal = $(res.modal);
                if (res.submitted) {
                    $modal.modal('hide');
                    modals.initModal($newModal);
                    return;
                }
                $container.replaceWith($newModal.find('.js-feedback-form-fields-container'));
            });

            return false;
        });

        //write user timezone to cookie
        let timezoneOffset = new Date().getTimezoneOffset();
        timezoneOffset = timezoneOffset === 0 ? 0 : - timezoneOffset;

        document.cookie = `timezoneOffset=${timezoneOffset};`;

        //newsletter subscribe
        const subscribeMutex = new Mutex();
        $document.on('submit', '.js-newsletter-subscribe-form', function () {
             const $form = $(this);
             const request = $.post($form.attr('action'), $form.serialize());

             if (!subscribeMutex.acquire()) {
                 return false;
             }

             request
                 .then(res => {
                     $form.find('.js-newsletter-subscribe-form-email-field').val('');
                     modals.notify({message: $form.data('successText')});
                 })
                 .catch(err => {
                     modals.alert('Something goes wrong');
                 })
                 .always(() => {
                     subscribeMutex.release();
                 });

             return false;
        });

        //socket
        // socket.subscribe(function (message) {
        //     console.log(message);
        // });
    });
} (window.internalJQuery, window, document));