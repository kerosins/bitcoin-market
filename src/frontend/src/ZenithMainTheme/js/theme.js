(function ($, document, window) {
    // Theme
    window.theme = {};

// Theme Common Functions
    window.theme.fn = {

        getOptions: function(opts) {

            if (typeof(opts) == 'object') {

                return opts;

            } else if (typeof(opts) == 'string') {

                try {
                    return JSON.parse(opts.replace(/'/g,'"').replace(';',''));
                } catch(e) {
                    return {};
                }

            } else {

                return {};

            }

        }

    };

// Animate
    (function(theme, $) {

        theme = theme || {};

        var instanceName = '__animate';

        var PluginAnimate = function($el, opts) {
            return this.initialize($el, opts);
        };

        PluginAnimate.defaults = {
            accX: 0,
            accY: -150,
            delay: 1,
            duration: '1s'
        };

        PluginAnimate.prototype = {
            initialize: function($el, opts) {
                if ($el.data(instanceName)) {
                    return this;
                }

                this.$el = $el;

                this
                    .setData()
                    .setOptions(opts)
                    .build();

                return this;
            },

            setData: function() {
                this.$el.data(instanceName, this);

                return this;
            },

            setOptions: function(opts) {
                this.options = $.extend(true, {}, PluginAnimate.defaults, opts, {
                    wrapper: this.$el
                });

                return this;
            },

            build: function() {
                var self = this,
                    $el = this.options.wrapper,
                    delay = 0,
                    duration = '1s';

                $el.addClass('appear-animation animated');

                if (!$('html').hasClass('no-csstransitions') && $(window).width() > 767) {

                    $el.appear(function() {

                        $el.one('animation:show', function(ev) {
                            delay = ($el.attr('data-appear-animation-delay') ? $el.attr('data-appear-animation-delay') : self.options.delay);
                            duration = ($el.attr('data-appear-animation-duration') ? $el.attr('data-appear-animation-duration') : self.options.duration);

                            if (duration != '1s') {
                                $el.css('animation-duration', duration);
                            }

                            setTimeout(function() {
                                $el.addClass($el.attr('data-appear-animation') + ' appear-animation-visible');
                            }, delay);
                        });

                        $el.trigger('animation:show');

                    }, {
                        accX: self.options.accX,
                        accY: self.options.accY
                    });

                } else {

                    $el.addClass('appear-animation-visible');

                }

                return this;
            }
        };

        // expose to scope
        $.extend(theme, {
            PluginAnimate: PluginAnimate
        });

        // jquery plugin
        $.fn.themePluginAnimate = function(opts) {
            return this.map(function() {
                var $this = $(this);

                if ($this.data(instanceName)) {
                    return $this.data(instanceName);
                } else {
                    return new PluginAnimate($this, opts);
                }

            });
        };

    }).apply(this, [window.theme, jQuery]);

// Scroll to Top
    (function(theme, $) {

        theme = theme || {};

        $.extend(theme, {

            PluginScrollToTop: {

                defaults: {
                    wrapper: $('body'),
                    offset: 150,
                    buttonClass: 'scroll-to-top',
                    iconClass: 'fa fa-chevron-up',
                    delay: 1000,
                    visibleMobile: false,
                    label: false,
                    easing: 'easeOutBack'
                },

                initialize: function(opts) {
                    initialized = true;

                    this
                        .setOptions(opts)
                        .build()
                        .events();

                    return this;
                },

                setOptions: function(opts) {
                    this.options = $.extend(true, {}, this.defaults, opts);

                    return this;
                },

                build: function() {
                    var self = this,
                        $el;

                    // Base HTML Markup
                    $el = $('<a />')
                        .addClass(self.options.buttonClass)
                        .attr({
                            'href': '#',
                        })
                        .append(
                            $('<i />')
                                .addClass(self.options.iconClass)
                        );

                    // Visible Mobile
                    if (!self.options.visibleMobile) {
                        $el.addClass('hidden-mobile');
                    }

                    // Label
                    if (self.options.label) {
                        $el.append(
                            $('<span />').html(self.options.label)
                        );
                    }

                    this.options.wrapper.append($el);

                    this.$el = $el;

                    return this;
                },

                events: function() {
                    var self = this,
                        _isScrolling = false;

                    // Click Element Action
                    self.$el.on('click', function(e) {
                        e.preventDefault();
                        $('body, html').animate({
                            scrollTop: 0
                        }, self.options.delay, self.options.easing);
                        return false;
                    });

                    // Show/Hide Button on Window Scroll event.
                    $(window).scroll(function() {

                        if (!_isScrolling) {

                            _isScrolling = true;

                            if ($(window).scrollTop() > self.options.offset) {

                                self.$el.stop(true, true).addClass('visible');
                                _isScrolling = false;

                            } else {

                                self.$el.stop(true, true).removeClass('visible');
                                _isScrolling = false;

                            }

                        }

                    });

                    return this;
                }

            }

        });

    }).apply(this, [window.theme, window.internalJQuery]);

// Sticky
    (function(theme, $) {

        theme = theme || {};

        var instanceName = '__sticky';

        var PluginSticky = function($el, opts) {
            return this.initialize($el, opts);
        };

        PluginSticky.defaults = {
            minWidth: 991,
            activeClass: 'sticky-active'
        };

        PluginSticky.prototype = {
            initialize: function($el, opts) {
                if ( $el.data( instanceName ) ) {
                    return this;
                }

                this.$el = $el;

                this
                    .setData()
                    .setOptions(opts)
                    .build();

                return this;
            },

            setData: function() {
                this.$el.data(instanceName, this);

                return this;
            },

            setOptions: function(opts) {
                this.options = $.extend(true, {}, PluginSticky.defaults, opts, {
                    wrapper: this.$el
                });

                return this;
            },

            build: function() {
                if (!($.isFunction($.fn.pin))) {
                    return this;
                }

                var self = this,
                    $window = $(window);

                self.options.wrapper.pin(self.options);

                $window.afterResize(function() {
                    self.options.wrapper.removeAttr('style').removeData('pin');
                    self.options.wrapper.pin(self.options);
                    $window.trigger('scroll');
                });

                return this;
            }
        };

        // expose to scope
        $.extend(theme, {
            PluginSticky: PluginSticky
        });

        // jquery plugin
        $.fn.themePluginSticky = function(opts) {
            return this.map(function() {
                var $this = $(this);

                if ($this.data(instanceName)) {
                    return $this.data(instanceName);
                } else {
                    return new PluginSticky($this, opts);
                }

            });
        }

    }).apply(this, [ window.theme, jQuery ]);

// Validation
    (function(theme, $) {

        theme = theme || {};

        $.extend(theme, {

            PluginValidation: {

                defaults: {
                    validator: {
                        highlight: function(element) {
                            $(element)
                                .parent()
                                .removeClass('has-success')
                                .addClass('has-error');
                        },
                        success: function(element) {
                            $(element)
                                .parent()
                                .removeClass('has-error')
                                .addClass('has-success')
                                .find('label.error')
                                .remove();
                        },
                        errorPlacement: function(error, element) {
                            if (element.attr('type') == 'radio' || element.attr('type') == 'checkbox') {
                                error.appendTo(element.parent().parent());
                            } else {
                                error.insertAfter(element);
                            }
                        }
                    },
                    validateCaptchaURL: 'php/contact-form-verify-captcha.php',
                    refreshCaptchaURL: 'php/contact-form-refresh-captcha.php'
                },

                initialize: function(opts) {
                    initialized = true;

                    this
                        .setOptions(opts)
                        .build();

                    return this;
                },

                setOptions: function(opts) {
                    this.options = $.extend(true, {}, this.defaults, opts);

                    return this;
                },

                build: function() {
                    var self = this;

                    if (!($.isFunction($.validator))) {
                        return this;
                    }

                    self.addMethods();
                    self.setMessageGroups();

                    $.validator.setDefaults(self.options.validator);

                    return this;
                },

                addMethods: function() {
                    var self = this;

                    $.validator.addMethod('captcha', function(value, element, params) {
                        var captchaValid = false;

                        $.ajax({
                            url: self.options.validateCaptchaURL,
                            type: 'POST',
                            async: false,
                            dataType: 'json',
                            data: {
                                captcha: $.trim(value)
                            },
                            success: function(data) {
                                if (data.response == 'success') {
                                    captchaValid = true;
                                }
                            }
                        });

                        if (captchaValid) {
                            return true;
                        }

                    }, '');

                    // Refresh Captcha
                    $('#refreshCaptcha').on('click', function(e) {
                        e.preventDefault();
                        $.get(self.options.refreshCaptchaURL, function(url) {
                            $('#captcha-image').attr('src', url);
                        });
                    });

                },

                setMessageGroups: function() {

                    $('.checkbox-group[data-msg-required], .radio-group[data-msg-required]').each(function() {
                        var message = $(this).data('msg-required');
                        $(this).find('input').attr('data-msg-required', message);
                    });

                }

            }

        });

    }).apply(this, [window.theme, jQuery]);

// Account
    (function(theme, $) {

        theme = theme || {};

        var initialized = false;

        $.extend(theme, {

            Account: {

                defaults: {
                    wrapper: $('#headerAccount')
                },

                initialize: function($wrapper, opts) {
                    if (initialized) {
                        return this;
                    }

                    initialized = true;
                    this.$wrapper = ($wrapper || this.defaults.wrapper);

                    this
                        .setOptions(opts)
                        .events();

                    return this;
                },

                setOptions: function(opts) {
                    this.options = $.extend(true, {}, this.defaults, opts, theme.fn.getOptions(this.$wrapper.data('plugin-options')));

                    return this;
                },

                events: function() {
                    var self = this;

                    self.$wrapper.find('input').on('focus', function() {
                        self.$wrapper.addClass('open');

                        $(document).mouseup(function(e) {
                            if (!self.$wrapper.is(e.target) && self.$wrapper.has(e.target).length === 0) {
                                self.$wrapper.removeClass('open');
                            }
                        });
                    });

                    $('#headerSignUp').on('click', function(e) {
                        e.preventDefault();
                        self.$wrapper.addClass('signup').removeClass('signin').removeClass('recover');
                        self.$wrapper.find('.signup-form input:first').focus();
                    });

                    $('#headerSignIn').on('click', function(e) {
                        e.preventDefault();
                        self.$wrapper.addClass('signin').removeClass('signup').removeClass('recover');
                        self.$wrapper.find('.signin-form input:first').focus();
                    });

                    $('#headerRecover').on('click', function(e) {
                        e.preventDefault();
                        self.$wrapper.addClass('recover').removeClass('signup').removeClass('signin');
                        self.$wrapper.find('.recover-form input:first').focus();
                    });

                    $('#headerRecoverCancel').on('click', function(e) {
                        e.preventDefault();
                        self.$wrapper.addClass('signin').removeClass('signup').removeClass('recover');
                        self.$wrapper.find('.signin-form input:first').focus();
                    });
                }

            }

        });

    }).apply(this, [window.theme, jQuery]);

// Nav
    (function(theme, $) {

        theme = theme || {};

        var initialized = false;

        $.extend(theme, {

            Nav: {

                defaults: {
                    wrapper: $('#mainNav'),
                    scrollDelay: 600,
                    scrollAnimation: 'easeOutQuad'
                },

                initialize: function($wrapper, opts) {
                    if (initialized) {
                        return this;
                    }

                    initialized = true;
                    this.$wrapper = ($wrapper || this.defaults.wrapper);

                    this
                        .setOptions(opts)
                        .build()
                        .events();

                    return this;
                },

                setOptions: function(opts) {
                    this.options = $.extend(true, {}, this.defaults, opts, theme.fn.getOptions(this.$wrapper.data('plugin-options')));

                    return this;
                },

                build: function() {
                    var self = this,
                        $html = $('html'),
                        $header = $('#header'),
                        thumbInfoPreview;

                    // Add Arrows
                    // $header.find('.dropdown-toggle, .dropdown-submenu > a').append($('<i />').addClass('fa fa-caret-down'));

                    // Preview Thumbs
                    self.$wrapper.find('a[data-thumb-preview]').each(function() {
                        thumbInfoPreview = $('<span />').addClass('thumb-info thumb-info-preview')
                            .append($('<span />').addClass('thumb-info-wrapper')
                                .append($('<span />').addClass('thumb-info-image').css('background-image', 'url(' + $(this).data('thumb-preview') + ')')
                                )
                            );

                        $(this).append(thumbInfoPreview);
                    });

                    // Side Header Right (Reverse Dropdown)
                    if($html.hasClass('side-header-right')) {
                        $header.find('.dropdown').addClass('dropdown-reverse');
                    }

                    // Reverse
                    self.checkReverse = function() {

                        self.$wrapper.find('.dropdown-reverse-auto').removeClass('dropdown-reverse dropdown-reverse-auto');

                        self.$wrapper.find('.dropdown-submenu:not(.manual)').each(function() {
                            if(!$(this).find('.dropdown-menu').visible( false, true, 'horizontal' )  ) {
                                $(this).parents('.dropdown').addClass('dropdown-reverse dropdown-reverse-auto');
                            }
                        });
                    }

                    self.checkReverse();

                    $(window).afterResize(function() {
                        self.checkReverse();
                    });

                    return this;
                },

                events: function() {
                    var self    = this,
                        $html   = $('html'),
                        $header = $('#header'),
                        $window = $(window);

                    $header.find('a[href="#"]').on('click', function(e) {
                        e.preventDefault();
                    });

                    // Mobile Arrows
                    $header.find('.dropdown-toggle[href="#"], .dropdown-submenu a[href="#"], .dropdown-toggle[href!="#"] .fa-caret-down, .dropdown-submenu a[href!="#"] .fa-caret-down').on('click', function(e) {
                        e.preventDefault();
                        if ($window.width() < 992) {
                            $(this).closest('li').toggleClass('opened');
                        }
                    });

                    // Touch Devices with normal resolutions
                    if('ontouchstart' in document.documentElement) {
                        $header.find('.dropdown-toggle:not([href="#"]), .dropdown-submenu > a:not([href="#"])')
                            .on('touchstart click', function(e) {
                                if($window.width() > 991) {

                                    e.stopPropagation();
                                    e.preventDefault();

                                    if(e.handled !== true) {

                                        var li = $(this).closest('li');

                                        if(li.hasClass('tapped')) {
                                            location.href = $(this).attr('href');
                                        }

                                        li.addClass('tapped');

                                        e.handled = true;
                                    } else {
                                        return false;
                                    }

                                    return false;

                                }
                            })
                            .on('blur', function(e) {
                                $(this).closest('li').removeClass('tapped');
                            });

                    }

                    // Collapse Nav
                    $header.find('[data-collapse-nav]').on('click', function(e) {
                        $(this).parents('.collapse').removeClass('in');
                    });

                    // Anchors Position
                    $('[data-hash]').each(function() {

                        var target = $(this).attr('href'),
                            offset = ($(this).is("[data-hash-offset]") ? $(this).data('hash-offset') : 0);

                        if($(target).get(0)) {
                            $(this).on('click', function(e) {
                                e.preventDefault();

                                // Close Collapse if Opened
                                $(this).parents('.collapse.in').removeClass('in');

                                self.scrollToTarget(target, offset);

                                return;
                            });
                        }

                    });

                    // Flex header
                    var isIE9        = $html.hasClass('ie9'),
                        isHeaderFlex = $header.hasClass('header-flex')
                    isEdge       = /Edge/.test(navigator.userAgent);

                    if( isIE9 && isHeaderFlex && $window.width() > 991 ) {
                        headerNavHeight =

                            $header.find('.header-nav').css({
                                height: ( $header.find('.header-top').get(0) ) ? ( $header.find('.header-body').height() - $header.find('.header-top').outerHeight() ) + 4 : $header.find('.header-body').height()
                            });

                        $header.find('.header-nav-main nav > ul > li > a').css({
                            'line-height': $('.header-nav-main nav > ul > li > a').height() + 'px'
                        });

                        $header.find('.header-nav .header-social-icons').css({
                            'line-height': $header.find('.header-nav').height() + 'px'
                        });
                    }

                    if( isEdge ) {
                        $('#header.header-flex .header-nav-main nav > ul > li.dropdown .dropdown-menu').css({
                            'margin-top' : '-1px'
                        });
                    }

                    return this;
                },

                scrollToTarget: function(target, offset) {
                    var self = this;

                    $('body').addClass('scrolling');

                    $('html, body').animate({
                        scrollTop: $(target).offset().top - offset
                    }, self.options.scrollDelay, self.options.scrollAnimation, function() {
                        $('body').removeClass('scrolling');
                    });

                    return this;

                }

            }

        });

    }).apply(this, [window.theme, jQuery]);

} (window.internalJQuery, document, window));
