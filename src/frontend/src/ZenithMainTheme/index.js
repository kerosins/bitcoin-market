import 'ZenithMainTheme/styles/theme.css';
import 'ZenithMainTheme/styles/theme-elements.css';
import 'ZenithMainTheme/styles/theme-blog.css';
import 'ZenithMainTheme/styles/theme-shop.css';

import 'ZenithMainTheme/styles/fonts/geometria.css';
import 'ZenithMainTheme/styles/skin.css';
import 'ZenithMainTheme/styles/main.less';
import 'ZenithMainTheme/styles/products.less';
import 'ZenithMainTheme/styles/elements.less';

import 'ZenithMainTheme/js/common';
import 'ZenithMainTheme/js/theme';
import 'ZenithMainTheme/js/main';
import 'ZenithMainTheme/js/theme.init';