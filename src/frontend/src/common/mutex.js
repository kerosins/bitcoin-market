class Mutex {
    constructor() {
        this.mutex = false;
        this.listeners = {
            onAcquire: [],
            onRelease: []
        };
    }

    release() {
        this.mutex = false;
        for (let i in this.listeners.onRelease) {
            this.listeners.onRelease[i].call();
        }
    }

    acquire() {
        if (this.mutex) {
            return false;
        }

        this.mutex = true;
        for (let i in this.listeners.onAcquire) {
            this.listeners.onAcquire[i].call();
        }
        return true;
    }

    /**
     * just checks
     *
     * @returns {boolean}
     */
    isAcquire() {
        return this.mutex;
    }

    onAcquire(callback) {
        this.listeners.onAcquire.push(callback);
        return this;
    }

    onRelease(callback) {
        this.listeners.onRelease.push(callback);
        return this;
    }
}

export default Mutex;