export default (function ($, document) {
    const Modal = {
        /**
         * Displays a modal window
         *
         * @param $modal
         * @return {*}
         */
        initModal($modal) {
            const hideModal = () => {
                $modal.modal('hide');
                $modal.remove();
            };

            $('body').append($modal);

            try {
                $modal.modal();
            } catch (e) {
                console.error(e);
            }

            $modal.find('.js-close-modal').on('click', () => $modal.modal('hide'));
            $modal.on('hidden.bs.modal', () => {
                hideModal();
            });

            return $modal;
        },

        /**
         * Displays a confirmation dialog
         *
         * @param title
         * @param message
         * @param confirmBtnText
         * @return {Promise<any>}
         */
        confirmModal({title = null, message = null, confirmBtnText = null, cancelBtnText = null}) {
            let $templateCnt = $('#js-confirm-modal-template');

            if (!$templateCnt.length) {
                throw new Error('Template for confirm modal not found');
            }

            const $confirmContent = $($templateCnt.text());
            if (title) {
                $confirmContent.find('.js-confirm-modal-header').html(title);
            }

            if (message) {
                $confirmContent.find('.js-confirm-modal-body').html(message);
            }

            if (confirmBtnText) {
                $confirmContent.find('.js-confirm-btn').text(confirmBtnText);
            }

            if (cancelBtnText) {
                $confirmContent.find('.js-cancel-btn').text(cancelBtnText);
            }

            const $modal = Modal.initModal($confirmContent);

            return new Promise((resolve, reject) => {
                let isConfirmed = false;

                $('.js-confirm-btn', $confirmContent).on('click', () => {
                    isConfirmed = true;
                    $modal.modal('hide');
                });

                $modal.on('hidden.bs.modal', () => {
                    if (isConfirmed) {
                        resolve();
                    } else {
                        reject(new Error('canceled'));
                    }
                });
            });
        },

        alert(message) {
            const $modalTemplate = $('#js-alert-template');
            const $alertModal = $($modalTemplate.text());

            $alertModal.find('.js-alert-modal-body').text(message);

            Modal.initModal($alertModal);

            if (!$modalTemplate.length) {
                throw new Error('Template for alert has not present');
            }
        },

        notify({message, header = ''}) {
            const $template = $('#js-notify-template');
            if (!$template.length) {
                throw new Error('Template for notify modal not found');
            }

            let $notifyModal = $($template.text());

            if (header) {
                $notifyModal.find('.js-notify-modal-header').text(header);
            }

            $notifyModal.find('.js-notify-modal-body').text(message);

            Modal.initModal($notifyModal);
        }
    };

    $(function () {
        const $body = $('body');
        const $document = $(document);

        $document.on('alertModal.btshop', function (e, data) {
            Modal.alert(data.message);
        });

        //displays ajax content in modal
        $document.on('click', '[data-toggle="ajax-modal"]', function (e) {
            e.preventDefault();

            const $this = $(this);
            let url = $this.attr('href') || $this.data('url');

            if (!url) {
                throw new Error('Url must be required for ajax-modal');
            }

            $.ajax({
                url,
                dataType: 'html'
            }).then((html) => {
                const $modal = Modal.initModal($(html));
                console.log($modal);
                $this.trigger('loaded.ajax', {'modal': $modal});
            });
        });

        $document.on('click', '.js-link-require-confirm', function (e) {
            const $this = $(this);
            const uri = $this.attr('href') || $this.data('href');

            if (uri === undefined) {
                throw new Error('Link with confirmation require an url');
            }

            e.preventDefault();

            const params = {
                message: $this.data('confirm-message') || null,
                title: $this.data('confirm-title') || null,
                confirmBtnText: $this.data('confirm-button') || null,
                cancelBtnText: $this.data('confirm-cancel-button') || null
            };

            Modal.confirmModal(params)
                .then(() => {
                    window.location.href = uri;
                })
                .catch(() => {
                    if (IS_DEV) {
                        console.log('Confirm was canceled')
                    }
                });
        });
    });

    return Modal;
} (window.internalJQuery, document));