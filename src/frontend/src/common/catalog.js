/**
 * Replace products
 *
 * @param html
 */
export function productsReplace(html) {
    const $container = $('.product__list');

    $container.fadeTo('fast', 0);
    $container.html(html);
    $container.fadeTo('fast', 1);
}