export default (function ($) {
    let isAuth;
    let loginUrl;

    const BitUser = {
        isAuth() {
            return isAuth;
        },

        getLoginUrl() {
            return loginUrl;
        }
    };

    $(function () {
        let $body = $('body');
        isAuth = $body.data('authenticated') !== undefined;
        loginUrl = $body.data('login-url') || '';

        //remove they
        $body.attr('data-authenticated', null);
        $body.attr('data-login-url', null);
    });

    return BitUser;
} (window.internalJQuery));