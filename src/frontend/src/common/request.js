/**
 * @deprecated
 * @param options
 * @return {Promise}
 */
export function request(options) {
    return new Promise((resolve, reject) => {
        options = {
            dataType: 'json',
            ...options,
            success: (data) => {
                resolve(data);
            },
            error: (xhr, status, err) => {
                reject({
                    xhr,
                    status,
                    err
                });
            }
        };

        $.ajax(options);
    });
}