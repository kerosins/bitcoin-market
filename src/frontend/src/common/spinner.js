import 'common/style/ajax_loader.less';
import { Spinner } from 'spin.js'
import 'spin.js/spin.css';

export default (function ($, document, window) {
    return {
        start($element, opts = {}) {
            if (!$element.length) {
                return;
            }

            let dataSet = $element.data();
            let optionsSet = Object.keys(dataSet).filter(k => k.indexOf('spinner') === 0);

            for (let i in optionsSet) {
                let spinOptionName = optionsSet[i].replace('spinner', '').toLowerCase();
                opts[spinOptionName] = dataSet[optionsSet[i]];
            }

            const spin = new Spinner(opts);
            spin.spin($element.get(0));
            $element.data('spinner', spin);
        },

        stop($element) {
            if ($element.data('spinner')) {
                $element.data('spinner').stop();
                $element.data('spinner', null);
            }
        }
    };
} (window.internalJQuery, document, window));