import IoClient from 'socket.io-client'

function getCookie(name) {
    const matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

const client = new IoClient(WEB_SOCKET_URI, {
    query: { 'token': getCookie('socketToken') }
});

export default client;