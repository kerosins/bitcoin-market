import 'select2';
import 'select2/dist/css/select2.css';

export default (function ($, window, document) {
    $(function () {
        let $body = $('body');
        const $document = $(document);

        $('.entity-filter__toggle').on('click', function (e) {
            e.preventDefault();
            let $btn = $(this);
            let $container = $btn.closest('.entity-filter').find('.entity-filter__form');

            if ($container.hasClass('hidden')) {
                $container.removeClass('hidden');
                $btn.text('Hide');
            } else {
                $container.addClass('hidden');
                $btn.text('Show')
            }
        });

        $body.on('click', '.collection-element-add', function (e) {
            let $container = $(this).closest('.collection-container');
            let prototype = $container.data('prototype');

            let $row = $('<div>', {'class': 'form-inline collection-element'});

            if ($container.data('element-class')) {
                $row.addClass($container.data('element-class'));
            }

            let name = $container.find('.collection-element').length + 1;

            prototype = prototype.replace(/__name__/g, name);
            $row.html(prototype);

            $(this).before($row);
            e.preventDefault();
        });

        $body.on('click', '.collection-element-delete', function (e) {
            e.preventDefault();
            $(this).closest('.collection-element').remove();
        });

        $('.js-auto-complete').each((i, e) => {
            let $element = $(e);
            let $select = $element.find('select');
            let data = $select.data('data-handler') || (params => ({ q: params.term, page: (params.page || 1) }));

            let options = {
                width: '200px',
                ajax: {
                    url: $element.data('url'),
                    dataType: 'json',
                    delay: 250,
                    data: eval(data),
                    processResults: (data, params) => {
                        params.page = params.page || 1;

                        let result = {
                            results: data.result,
                            pagination: null
                        };

                        if (data.total && data.perPage) {
                            result.pagination = {
                                more: (params.page * data.perPage) < data.total
                            }
                        }

                        return result;
                    },
                    minimumInputLength: $element.data('min-length') || 2
                }
            };

            if ($select.attr('placeholder')) {
                options.placeholder = $select.attr('placeholder');
                options.allowClear = true;
            }

            $select.select2(options);
        });

        $('.js-datepicker').each((i, e) => {
            const $element = $(e);

            $element.datepicker({
                format: $element.data('format')
            });
        });

        function toJsDateFormat(format) {
            const chars = {
                'Y': 'yyyy',
                'm': 'mm',
                'd': 'dd'
            };

            let newFormat = '';

            for (let i = 0; i < format.length; i++) {
                if (chars[format[i]] !== undefined) {
                    newFormat += chars[format[i]];
                }
            }

            return newFormat;
        }
    });

    return {};
} (window.internalJQuery, window, document));