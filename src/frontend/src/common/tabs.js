import 'common/style/tabs.less';

(function ($, document) {
    $(function () {
        initializeTabs($('.nav-tabs'));


        $(document).on('click', '.js-tab-link', function () {
            const id = $(this).attr('href') || $(this).data('href');
            const $tab = $(`.nav-tabs a[href='${id}']`);

            if ($tab.length) {
                $tab.tab('show');
            }

            return false;
        })
    });

    function initializeTabs($tabs) {
        if (!$tabs.length) {
            return null;
        }

        if (document.location.hash) {
            $tabs.find('li').removeClass('active');
            $tabs.find(`a[href="${document.location.hash}"]`).trigger('click');
        }

        $tabs.find('a').on('shown.bs.tab', function (e) {
            let href = $(this).attr('href');

            if (document.location.hash !== href) {
                document.location.hash = href;
            }

            return false;
        })
    }

} (window.internalJQuery, document));