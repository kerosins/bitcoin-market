const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const entries = require('./webpack.entries');
const GoogleFontsPlugin = require('google-fonts-webpack-plugin');

const isProd = process.argv.filter(item => item == '-p').length > 0;

let config = {
    context: path.resolve(__dirname, 'src'),
    entry: {...entries},
    resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
            Shop: path.resolve(__dirname, 'src/Shop'),
            Admin: path.resolve(__dirname, 'src/Admin'),
            routes: path.resolve(__dirname, 'src/routes.js'),
            common: path.resolve(__dirname, 'src/common'),
            ZenithMainTheme: path.resolve(__dirname, 'src/ZenithMainTheme'),
            vendors: path.resolve(__dirname, 'src/vendors')
        }
    },
    output: {
        path: path.resolve(__dirname, '../web/assets'),
        filename: '[name].js',
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                'react',
                                ['es2015', {"modules": false}],
                                'stage-0'
                            ],
                            plugins: ['transform-decorators-legacy']
                        }
                    }
                ]
            },
            {
                test: /plupload/,
                parser: {
                    amd: false
                }
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader?sourceMap"
                })
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ["css-loader?sourceMap", "less-loader?sourceMap"]
                })
            },
            {
                test: /\.(jpg|jpeg|gif|giff|png|ico)$/,
                use: 'file-loader'
            },
            { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: "url-loader?limit=10000&mimetype=application/font-woff" },
            { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: "file-loader" }
        ]
    },
    plugins: [
        new ExtractTextPlugin('[name].css'),
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',
            'window.jQuery': 'jquery',
            'window.$': 'jquery',
            jqueryProvide: ['./jqueryBootstrap', 'default'],
            Promise: 'bluebird',
            moment: 'moment',
        }),
        new webpack.DefinePlugin({
            WEB_SOCKET_URI: JSON.stringify((isProd ? 'ws://ws.zenitbay.com' : 'ws://ws.bitshop.local')),
            IS_DEV: JSON.stringify((!isProd))
        }),
        // new GoogleFontsPlugin({
        //     fonts: [
        //         { family: 'Open Sans', variants: ['300', '400', '600', '700', '800'] },
        //         { family: 'Shadows Into Light' }
        //     ]
        // })
    ],
    devtool: isProd ? false : 'source-map'
};

module.exports = config;