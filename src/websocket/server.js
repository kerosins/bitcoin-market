const config = require('./config');
const app = require('express')();

const http = require('http').createServer(app);
const io = require('socket.io')(http);

io.on('connection', function (socket) {
    const token = socket.handshake.query.token;
    if (token === undefined) {
        socket.disconnect();
        return;
    }

    socket.join(token);
});

//start listening
http.listen(config.ports.frontend, function () {
    console.log('Start listening a frontend port');
});

//listen tcp and receive messages from backend
const tcp = require('net').createServer(function (socket) {
    socket.pipe(socket);
    socket.on('data', function (data) {
        try {
            data = JSON.parse(data);
            io.to(data.token).emit(data.type, data.message);
            socket.write('Ok\r\n');
        } catch (err) {
            socket.write('It a bad message, try again\r\n');
        }
    });

    socket.on('error', function (err) {});
});

const stop = function () {
    http.close(function () {
        console.log('Stop frontend');
    });
    tcp.close(function () {
        console.log('Stop listen backend');
    });
};

process.on('SIGTERM', stop);
process.on('SIGINT', stop);

tcp.listen(config.ports.backend, '127.0.0.1');