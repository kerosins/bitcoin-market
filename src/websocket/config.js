const path = require('path');
const fs = require('fs');

let config = {
    redis: {
        host: '127.0.0.1',
        port: '6379'
    },
    ports: {
        frontend: 2000,
        backend: 2001
    }
};

const isDev = process.env.NODE_ENV === 'dev';

let devConfig = {};
if (isDev && fs.existsSync(path.resolve(__dirname + '/config.dev.js'))) {
    devConfig = require('config.dev');
}

module.exports = Object.assign({}, config, devConfig);